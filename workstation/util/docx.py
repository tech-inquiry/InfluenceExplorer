#
# Copyright (c) 2023 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import io
import re
import requests
import xml.etree.ElementTree as ElementTree
import zipfile

namespace_map = {
    'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'
}


def qualified_name(tag):
    """
    Cf. https://github.com/python-openxml/python-docx/
    """
    prefix, root = tag.split(':')
    uri = namespace_map[prefix]
    return '{{{}}}{}'.format(uri, root)


def get_text_from_xml(xml):
    """
    Cf. https://github.com/python-openxml/python-docx/
    """
    text = ''
    for child in ElementTree.fromstring(xml).iter():
        if child.tag == qualified_name('w:t'):
            text += child.text if child.text is not None else ''
        elif child.tag == qualified_name('w:tab'):
            text += '\t'
        elif child.tag in (qualified_name('w:br'), qualified_name('w:cr')):
            text += '\n'
        elif child.tag == qualified_name("w:p"):
            text += '\n\n'
    return text


def fill_header_text(zipped_doc, texts):
    text = ''
    for filename in zipped_doc.namelist():
        if re.match('word/header[0-9]*.xml', filename):
            text += get_text_from_xml(zipped_doc.read(filename))
    if text:
        texts['header'] = text


def fill_main_text(zipped_doc, texts):
    texts['main'] = get_text_from_xml(zipped_doc.read('word/document.xml'))


def fill_footer_text(zipped_doc, texts):
    text = ''
    for filename in zipped_doc.namelist():
        if re.match('word/footer[0-9]*.xml', filename):
            text += get_text_from_xml(zipped_doc.read(filename))
    if text:
        texts['footer'] = text


# Note that the 'docx' object can be an io.BytesIO object too.
def get_text(docx, verbose=False):
    zipped_doc = zipfile.ZipFile(docx)
    if verbose:
        print('Files: {}'.format(zipped_doc.namelist()))

    texts = {}
    fill_header_text(zipped_doc, texts)
    fill_main_text(zipped_doc, texts)
    fill_footer_text(zipped_doc, texts)

    zipped_doc.close()

    return texts


def get_text_from_url(url, verbose=False):
    response = requests.get(url)
    return get_text(io.BytesIO(response.content), verbose=verbose)
