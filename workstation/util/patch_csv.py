#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# A utility for patching improperly formatted CSVs with custom logic.
#
import csv
import json
import pandas as pd


def get_header_components(filename, sep=','):
    '''Returns the list of labels from a CSV header.'''
    with open(filename, 'r') as infile:
        line = infile.readline()
        return line.rstrip('\n').split(sep)


def tokenize_line(infile,
                  sep,
                  quotechar,
                  doublequote,
                  escapechar,
                  assume_always_quoted=False,
                  assume_never_quoted=False,
                  verbose=True):
    '''Tokenize a line of a CSV by reading from a file input stream.

    Returns:
       The components from this line (or None) and whether the EOF was reached.
    '''
    line = ''
    tokens = []
    state = {'in_escape': False, 'in_quote': False, 'token': ''}
    is_eof = False
    while True:
        char = infile.read(1)
        if not char:
            is_eof = True
            break
        line += char
        pos = infile.tell()

        if state['in_escape']:
            state['token'] += char
            state['in_escape'] = False
        elif state['in_quote']:
            if char == quotechar:
                next_char = infile.read(1)
                next_pos = infile.tell()
                if doublequote and nextchar == quote_char:
                    # We are starting a double-quote.
                    state['in_escape'] = True
                    state['token'] += quote_char
                else:
                    # Close quotes should end the token. If not, correct for
                    # the missing escape on the quote.
                    if not next_char or next_char == '\n':
                        state['in_quote'] = False
                    elif next_char == sep:
                        if assume_always_quoted:
                            next_next_char = infile.read(1)
                            if not next_next_char or next_next_char == '\n':
                                state['in_quote'] = False
                            elif next_next_char == quotechar:
                                state['in_quote'] = False
                            else:
                                if verbose:
                                    print(
                                        'WARNING: Dynamically escaping quote in CSV line (with next next char {}):\n{}'
                                        .format(next_next_char, line))
                                state['token'] += char
                        else:
                            state['in_quote'] = False
                    else:
                        if verbose:
                            print(
                                'WARNING: Dynamically escaping quote in CSV line (with next char {}):\n{}'
                                .format(next_char, line))
                        state['token'] += char
                    infile.seek(pos)
            else:
                state['token'] += char
        elif char == quotechar:
            if assume_never_quoted:
                # Assume the quote wasn't escaped.
                state['token'] += char
            else:
                if state['token']:
                    # A quote cannot be opened in the middle of a token.
                    state['token'] += char
                else:
                    state['in_quote'] = True
        elif char == sep:
            tokens.append(state['token'].rstrip())
            state['in_escape'] = False
            state['in_quote'] = False
            state['in_token'] = False
            state['token'] = ''
        elif char == '\n':
            break
        else:
            state['token'] += char
    tokens.append(state['token'].rstrip())

    return tokens, is_eof


def patch_line(infile,
               line_number,
               header,
               corruptions,
               sep='\t',
               quotechar='"',
               doublequote=False,
               escapechar='\\',
               assume_always_quoted=False,
               assume_never_quoted=False,
               patch_components=None,
               verbose=True):
    '''Patches a line from a TSV.

    Args:
      infile: The opened filestream offset to the current CSV line.
      line_number: The line number of the 'line' string.
      header: The list of CSV header labels.
      corruptions: The dict of corruption statistics.
      sep: The original CSV separator character.
      quotechar: The quotation character for the CSV. 
      doublequote: Whether doublequoting should be used.
      escapechar: The escape character for the CSV.
      assume_always_quoted: Whether we can assume the tokens are always quoted.
      assume_never_quoted: Whether we can assume the tokens are never quoted.
      patch_components: The optional function for returning the patched 
          component (or None) from the original list of components.
      verbose: Whether to print patching information.

    Returns:
       The components from this line (or None) and whether the EOF was reached.
    '''
    components, is_eof = tokenize_line(infile, sep, quotechar, doublequote,
                                       escapechar, assume_always_quoted,
                                       assume_never_quoted)
    if patch_components is not None:
        components = patch_components(components,
                                      header=header,
                                      verbose=verbose,
                                      line_number=line_number)
    if components is None:
        corruptions['total'] += 1
        return None, is_eof

    if len(components) != len(header):
        corruptions['total'] += 1
        corruptions['uncaught'] += 1
        if verbose:
            print('Line {} had {} components:'.format(line_number,
                                                      len(components)))
        for i, component in enumerate(components):
            if component:
                label = header[i] if i < len(header) else 'OUT_OF_BOUNDS'
                if verbose:
                    print('  {}, {}: {}'.format(i, label, component))
        return None, is_eof

    return components, is_eof


def patch(orig_csv,
          output_csv,
          output_json,
          sep=',',
          quotechar='"',
          doublequote=False,
          escapechar='\\',
          quoting=csv.QUOTE_ALL,
          assume_always_quoted=False,
          assume_never_quoted=False,
          patch_components=None,
          preprocess_line=None,
          preprocessed_csv='preprocessed.csv',
          verbose=True):
    '''Undo corruptions in a CSV with a custom per-line fixing function.

    Args:
      orig_csv: The path to the original CSV.
      output_csv: The path to the patched output CSV.
      output_json: The path to the patched output JSON.
      sep: The separator token of the CSVs.
      quotechar: The quotation character of the CSV.
      doublequote: Whether double-quoting should be used to escape quotes.
      escapechar: The escape character to use to escape quotes.
      quoting: The CSV quotation style.
      assume_always_quoted: Whether we can assume the tokens are always quoted.
      assume_never_quoted: Whether we can assume the tokens are never quoted.
      patch_components: An optional function for patching each line's
          components. It should map an array of strings to an array of strings,
          or None if the patching failed.
      preprocess_line: An optional function for preprocessing each line.  It
          should map a string to a string.
      preprocessed_csv: If a preprocessing function is supplied, so to should
          be a temporary file to store the preprocessed data into.
      verbose: Whether adjustment information should be printed.
    '''
    if preprocess_line is not None:
        with open(orig_csv) as infile:
            lines = infile.readlines()
            if len(lines):
                output_lines = [lines[0]]
                for line in lines[1:]:
                    output_lines.append(preprocess_line(line))
                with open(preprocessed_csv, 'w') as outfile:
                    outfile.writelines(output_lines)
            else:
                print('WARNING: Empty line list from {}'.format(orig_csv))
                print('lines: {}'.format(lines))
        in_csv = preprocessed_csv
    else:
        in_csv = orig_csv

    header = get_header_components(in_csv, sep=sep)
    if verbose:
        print(header)

    corruptions = {'total': 0, 'uncaught': 0}
    with open(in_csv) as infile, open(output_csv, 'w',
                                      newline='') as outcsv, open(
                                          output_json, 'w') as outjson:
        csv_writer = csv.DictWriter(outcsv,
                                    fieldnames=header,
                                    delimiter=sep,
                                    quotechar=quotechar,
                                    quoting=quoting)
        csv_writer.writeheader()
        outjson.write('[\n')
        wroteJSON = False

        line_number = 0
        header_line = infile.readline()
        while True:
            line_number += 1
            components, is_eof = patch_line(
                infile,
                line_number,
                header,
                corruptions,
                sep=sep,
                quotechar=quotechar,
                assume_always_quoted=assume_always_quoted,
                assume_never_quoted=assume_never_quoted,
                patch_components=patch_components,
                verbose=verbose)
            if components:
                line_data = {}
                for i in range(len(header)):
                    line_data[header[i]] = components[i]
                csv_writer.writerow(line_data)
                if wroteJSON:
                    outjson.write(',\n')
                outjson.write(json.dumps(line_data, indent=1))
                wroteJSON = True
            if is_eof:
                break
        outjson.write('\n]\n')

    if verbose:
        print('Number of corrupted rows: {} ({} uncaught)'.format(
            corruptions['total'], corruptions['uncaught']))
