#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import argparse


# A utility to simplify boolean argparse members.
def str2bool(v):
    TRUE_VALUES = ['yes', 'true', 't', 'y', '1']
    FALSE_VALUES = ['no', 'false', 'f', 'n', '0']

    if isinstance(v, bool):
        return v
    if v.lower() in TRUE_VALUES:
        return True
    elif v.lower() in FALSE_VALUES:
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
