#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import requests


# After construction, the result can be used as a replacement for 'requests'
def get_session(host='127.0.0.1', port='9050', protocol='socks5'):
    session = requests.session()
    # Tor uses the 9050 port as the default socks port
    session.proxies = {'http':  '{}://{}:{}'.format(protocol, host, port),
                       'https': '{}://{}:{}'.format(protocol, host, port)}
    return session
