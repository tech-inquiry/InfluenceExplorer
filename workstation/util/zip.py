#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Utilities for manipulating tar.gz and .zip files.
#
import pathlib
import requests
import tarfile
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from io import BytesIO
from urllib.request import urlopen
from zipfile import ZipFile

import util.firefox

ZIP_FORMATS = ['TGZ', 'ZIP', 'DETECT']


def file_suffixes(file_location):
    return pathlib.Path(file_location).suffixes


def download_and_unpack(compressed_url,
                        dest_dir,
                        files_to_extract=None,
                        compression_format='DETECT',
                        use_selenium=False,
                        download_dir=None,
                        downloaded_file=None,
                        download_recheck_seconds=0.75,
                        max_download_seconds=1200,
                        headless=True,
                        gecko_path=None,
                        quit_browser=False,
                        num_sleep_seconds=5,
                        verbose=False,
                        verify=False):
    '''Unpacks a compressed file located at a specified URL.

    Args:
      compressed_url: The URL containing the compressed file.
      dest_dir: The directory to unpack the files into.
      files_to_extract: An optional list of member files to unpack. If left 
          None, then all files are unpacked.
      compression_format: The compression format.
      use_selenium: Whether Selenium should be use to script a browser
          interaction to download the file.
      download_dir: The directory to download the archive into. If None,
          it will be set to dest_dir.
      downloaded_file: The filename which will be downloaded by Selenium.
          If left as None, it will be inferred from the end of the URL.
      download_recheck_seconds: The number of seconds to wait between each
          poll for whether the file finished downloading.
      max_download_seconds: The maximum number of seconds to wait for the
          download to finish.
      headless: Whether the browser should avoid any GUI.
      gecko_path: The path to the Selenium firefox driver.
      quit_browser: Whether the browser should formally be destroyed.
      num_sleep_seconds: The number of seconds to sleep after a file is
          ostensibly downloaded before closing the browser.
      verbose: Whether progress information should be printed.
      verify: Whether the HTTPS request should demand verification.
    '''
    if compression_format == 'DETECT':
        suffixes = file_suffixes(compressed_url)
        if '.tgz' in suffixes or ('.tar' in suffixes and '.gz' in suffixes):
            if verbose:
                print('Detected TGZ compression')
            compression_format = 'TGZ'
        elif '.zip' in suffixes:
            if verbose:
                print('Detected ZIP compression')
            compression_format = 'ZIP'
        else:
            print('ERROR: Could not deduce compression format from {}'.format(
                extension))
            return

    if use_selenium:
        try:
            if not download_dir:
                download_dir = dest_dir
            browser = util.firefox.get_browser(headless=headless,
                                               download_dir=download_dir)
            if verbose:
                print('Downloading {}'.format(compressed_url))
            browser.get(compressed_url)

            if not downloaded_file:
                downloaded_file = compressed_url.split('/')[-1]
            file_path = '{}/{}'.format(downlod_dir, downloaded_file)
            if verbose:
                print('Monitoring for download of {}'.format(file_path))
            completed = util.firefox.watch_download(
                file_path,
                download_recheck_seconds=download_recheck_seconds,
                max_download_seconds=max_download_seconds,
                verbose=verbose)
            if verbose:
                if completed:
                    print('Finished download of {}'.format(pattern))
                else:
                    print('WARNING: Download of {} did not complete in time.'.
                          format(pattern))

            # Due to a bug in which attempting to quit the browser results in
            # the program hanging, we have added this workaround.
            if quit_browser:
                browser.close()
                if verbose:
                    print('Closed browser session')
                browser.quit()
                if verbose:
                    print('Quit browser session')
            else:
                # We are attempting to ensure the download completes.
                print('Sleeping for {} seconds to help ensure download completes.'.format(num_sleep_seconds))
                time.sleep(num_sleep_seconds)

            if compression_format == 'ZIP':
                with ZipFile(file_path, mode='r') as zip_file:
                    if files_to_extract is None:
                        files_to_extract = zip_file.namelist()
                    for target in files_to_extract:
                        zip_file.extract(target, dest_dir)
            elif compression_format == 'TGZ':
                with tarfile.open(file_path, 'r|gz') as tar:
                    tar.extractall(path=dest_dir)
        except Exception as e:
            print(e)
    else:
        try:
            if compression_format == 'ZIP':
                resp = requests.get(compressed_url, verify=verify)
                with ZipFile(BytesIO(resp.content)) as zip_file:
                    if files_to_extract is None:
                        files_to_extract = zip_file.namelist()
                    for target in files_to_extract:
                        zip_file.extract(target, dest_dir)
            elif compression_format == 'TGZ':
                resp = requests.get(compressed_url, stream=True, verify=verify)
                with tarfile.open(fileobj=resp.raw, mode='r|gz') as tar:
                    tar.extractall(path=dest_dir)
        except Exception as e:
            print(e)
            print(resp.content)
