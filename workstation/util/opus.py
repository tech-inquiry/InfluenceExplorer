#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Utilities for language translation training and inference on top of OPUS,
# 'the open parallel corpus'. We specifically make use of the 'CC Aligned'
# OPUS dataset to train transformer-based models using OpenNMT which are then
# converted using the ctranslate2 package to accelerate inferencing.
#
import ctranslate2
import glob
import json
import nltk
import numpy as np
import os
import re
import time

from tqdm import tqdm

import util.zip

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')

TRAIN_PORTION = 0.7
TEST_PORTION = 0.001
VALIDATION_PORTION = 0.001

# The maximum number of characters in an input sentence that we will keep.
INPUT_MAX_CHARACTERS = 1000

# Configure the translation inference.
TRANSLATION_REPLACE_UNKNOWNS = True
TRANSLATION_BEAM_SIZE = 8
TRANSLATION_MAX_BATCH_SIZE = 128
# We can't set this to 'auto' or 'gpu' because of libcublas.so.11 linking errors.
TRANSLATION_DEVICE = 'cpu'
TRANSLATION_INTER_THREADS = 1
TRANSLATION_INTRA_THREADS = 0  # This results in the default value

# We make use of the ISO 639-1 standard for two-letter language codes. See
# https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
LANGUAGE_TO_CODE = {
    'bosnian': 'bs',
    'bulgarian': 'bg',
    'croatian': 'hr',
    'czech': 'cs',
    'danish': 'da',
    'dutch': 'nl',
    'english': 'en',
    'estonian': 'et',
    'finnish': 'fi',
    'french': 'fr',
    'german': 'de',
    'greek': 'el',
    'hebrew': 'he',
    'hungarian': 'hu',
    'irish': 'ga',
    'italian': 'it',
    'latvian': 'lv',
    'lithuanian': 'lt',
    'luxembourgish': 'lb',
    'maltese': 'mt',
    'norwegian': 'no',
    'polish': 'pl',
    'portuguese': 'pt',
    'romanian': 'ro',
    'russian': 'ru',
    'serbian': 'sr',
    'slovak': 'sk',
    'slovenian': 'sl',
    'spanish': 'es',
    'swedish': 'sv',
    'turkish': 'tr'
}
CODE_TO_LANGUAGE = {value: key for (key, value) in LANGUAGE_TO_CODE.items()}

WORD_TOKENIZED_LANGUAGES = [
    'bulgarian', 'croatian', 'czech', 'danish', 'dutch', 'english', 'french',
    'german', 'hebrew', 'hungarian', 'italian', 'lithuanian', 'polish',
    'romanian', 'russian', 'spanish', 'swedish', 'turkish'
]

# If a language is in this list, we use the NLTK punkt tokenizer. But, for
# example, Hungarian and Romanian are not currently supported so we fall back
# to the regular-expression based wordpunct tokenizer.
PUNKT_LANGUAGES = [
    'croatian', 'czech', 'danish', 'dutch', 'english', 'french',
    'german', 'italian', 'polish', 'russian', 'spanish', 'swedish'
]

# Determine the list of languages we can translate from by scanning the
# `nmt/models/` folder for the language pairs of each ctranslate2 model.
TRANSLATABLE_LANGUAGES = ['english']
TRANSLATABLE_LANGUAGE_CODES = ['en']
for folder in glob.glob('data/nmt/models/*'):
    translation_code = os.path.basename(os.path.normpath(folder))
    langs = translation_code.split('-')
    langs.remove('en')
    language_code = langs[0]
    language = CODE_TO_LANGUAGE[language_code]
    TRANSLATABLE_LANGUAGES.append(language)
    TRANSLATABLE_LANGUAGE_CODES.append(language_code)
print('TRANSLATABLE_LANGUAGES: {}'.format(TRANSLATABLE_LANGUAGES))


def get_corpus_code(language):
    src_code = LANGUAGE_TO_CODE[language]
    tgt_code = 'en'
    return '-'.join(sorted([src_code, tgt_code]))


def get_translation_code(language):
    src_code = LANGUAGE_TO_CODE[language]
    tgt_code = 'en'
    return '-'.join([src_code, tgt_code])


def bernoulli(likelihood):
    '''Flip a weighted coin with the head having the given likelihood.'''
    return np.random.binomial(1, likelihood)


def tokenize(phrase, language='english'):
    if language in WORD_TOKENIZED_LANGUAGES:
        if language in PUNKT_LANGUAGES:
            return nltk.tokenize.word_tokenize(phrase, language=language)
        else:
            return nltk.tokenize.wordpunct_tokenize(phrase)
    else:
        print('ERROR: Only word-tokenized languages are currently supported.')
        return []


def preprocess(corpus_src,
               corpus_tgt,
               inputs_dir,
               language,
               train_portion=TRAIN_PORTION,
               test_portion=TEST_PORTION,
               validation_portion=VALIDATION_PORTION,
               max_characters=INPUT_MAX_CHARACTERS):
    '''Splits sentence alignments into training/testing/validation sets.'''
    if not os.path.exists(inputs_dir):
        os.makedirs(inputs_dir)
    train_src = os.path.join(inputs_dir, 'src-train.txt')
    train_tgt = os.path.join(inputs_dir, 'tgt-train.txt')
    test_src = os.path.join(inputs_dir, 'src-test.txt')
    test_tgt = os.path.join(inputs_dir, 'tgt-test.txt')
    validation_src = os.path.join(inputs_dir, 'src-val.txt')
    validation_tgt = os.path.join(inputs_dir, 'tgt-val.txt')
    with open(corpus_src) as corpusfile_src, \
         open(corpus_tgt) as corpusfile_tgt, \
         open(train_src, 'w') as trainfile_src, \
         open(train_tgt, 'w') as trainfile_tgt, \
         open(test_src, 'w') as testfile_src, \
         open(test_tgt, 'w') as testfile_tgt, \
         open(validation_src, 'w') as valfile_src, \
         open(validation_tgt, 'w') as valfile_tgt:
        while True:
            line_src = corpusfile_src.readline()
            line_tgt = corpusfile_tgt.readline()
            if not line_src or not line_tgt:
                break
            line_src = line_src.strip()
            line_tgt = line_tgt.strip()
            if not line_src or len(line_src) > max_characters:
                continue

            if bernoulli(train_portion):
                outfile_src = trainfile_src
                outfile_tgt = trainfile_tgt
            elif bernoulli(test_portion / (1. - train_portion)):
                outfile_src = testfile_src
                outfile_tgt = testfile_tgt
            elif bernoulli(validation_portion /
                           (1. - (train_portion + test_portion))):
                outfile_src = valfile_src
                outfile_tgt = valfile_tgt
            else:
                continue

            line_src = ' '.join(tokenize(line_src, language=language))
            line_tgt = ' '.join(tokenize(line_tgt, language='english'))

            outfile_src.write('{}\n'.format(line_src))
            outfile_tgt.write('{}\n'.format(line_tgt))


def preprocess_cc_aligned(language,
                          train_portion=TRAIN_PORTION,
                          test_portion=TEST_PORTION,
                          validation_portion=VALIDATION_PORTION,
                          max_characters=INPUT_MAX_CHARACTERS):
    '''Preprocesses CC aligned corpus into training/testing/validation sets.'''
    src_code = LANGUAGE_TO_CODE[language]
    tgt_code = 'en'
    corpus_code = get_corpus_code(language)
    translation_code = get_translation_code(language)
    corpus_dir = os.path.join(DATA_DIR, 'nmt/corpus/{}'.format(corpus_code))
    inputs_dir = os.path.join(DATA_DIR,
                              'nmt/inputs/{}'.format(translation_code))
    corpus_src = os.path.join(corpus_dir,
                              'CCAligned.{}.{}'.format(corpus_code, src_code))
    corpus_tgt = os.path.join(corpus_dir,
                              'CCAligned.{}.{}'.format(corpus_code, tgt_code))
    preprocess(corpus_src, corpus_tgt, inputs_dir, language, train_portion,
               test_portion, validation_portion, max_characters)


def cc_aligned_url(language):
    code = get_corpus_code(language)
    return '{}/{}.txt.zip'.format(
        'https://object.pouta.csc.fi/OPUS-CCAligned/v1/moses', code)


def get_cc_aligned(language, preprocess=True):
    code = get_corpus_code(language)
    url = cc_aligned_url(language)
    dest_dir = os.path.join(DATA_DIR, 'nmt/corpus/{}'.format(code))
    util.zip.download_and_unpack(url, dest_dir)
    if preprocess:
        preprocess_cc_aligned(language)


def get_translator(language,
                   device,
                   inter_threads=TRANSLATION_INTER_THREADS,
                   intra_threads=TRANSLATION_INTRA_THREADS):
    code = get_translation_code(language)
    model_path = os.path.join(DATA_DIR, 'nmt/models/{}'.format(code))
    return ctranslate2.Translator(model_path,
                                  device=device,
                                  inter_threads=inter_threads,
                                  intra_threads=intra_threads)


def translate_(inputs,
               translator,
               language,
               detokenize=True,
               beam_size=TRANSLATION_BEAM_SIZE,
               replace_unknowns=TRANSLATION_REPLACE_UNKNOWNS,
               max_batch_size=TRANSLATION_MAX_BATCH_SIZE,
               inter_threads=TRANSLATION_INTER_THREADS):
    '''Returns the translation of inputs with a specified translator'''
    # We could translate in a single call to translator.translate_batch,
    # but we can lower memory usage (by eliminating temporary products) and
    # add in support for a progress bar (via tqdm) by manually chunking the
    # inputs via the maximum batch size. As a bonus, we can catch mkl_alloc
    # exceptions which might occur deep into a large translation job without
    # forfeiting all of the results.
    word_tokenize = language in WORD_TOKENIZED_LANGUAGES
    if word_tokenize:
        detokenizer = (nltk.tokenize.treebank.TreebankWordDetokenizer()
                       if detokenize else None)
        output = []
        with tqdm(total=len(inputs)) as pbar:
            chunk_size = inter_threads * max_batch_size
            for offset in range(0, len(inputs), chunk_size):
                input_slice = inputs[offset:offset + chunk_size]
                try:
                    input_tokens = [
                        tokenize(item, language=language)
                        for item in input_slice
                    ]
                    results = translator.translate_batch(
                        input_tokens,
                        beam_size=beam_size,
                        replace_unknowns=replace_unknowns,
                        max_batch_size=max_batch_size)
                    output_tokens = [item[0]['tokens'] for item in results]
                    if detokenize:
                        output += [
                            detokenizer.detokenize(item)
                            for item in output_tokens
                        ]
                    else:
                        output += output_tokens
                except Exception as e:
                    print('Could not translate slice at offset {}'.format(
                        offset))
                    print(e)
                    if detokenize:
                        output += [''] * len(input_slice)
                    else:
                        output += [[]] * len(input_slice)
                pbar.update(len(input_slice))
        return output
    else:
        # TODO(Jack Poulson): Add support for character-level tokenization,
        # e.g., to handle Chinese.
        return None


def translate_to_english(inputs,
                         language,
                         detokenize=True,
                         beam_size=TRANSLATION_BEAM_SIZE,
                         replace_unknowns=TRANSLATION_REPLACE_UNKNOWNS,
                         max_batch_size=TRANSLATION_MAX_BATCH_SIZE,
                         inter_threads=TRANSLATION_INTER_THREADS,
                         intra_threads=TRANSLATION_INTRA_THREADS,
                         device=TRANSLATION_DEVICE,
                         verbose=False):
    '''Returns the translation of a set of input sentences (and the runtime)'''
    if language == 'english' or not len(inputs):
        return inputs, 0.
    start = time.time()
    translator = get_translator(language,
                                device=device,
                                inter_threads=inter_threads,
                                intra_threads=intra_threads)
    if verbose and device == 'auto':
        print('Selected {} device for translation.'.format(translator.device))
    results = translate_(inputs,
                         translator,
                         language,
                         detokenize=detokenize,
                         beam_size=beam_size,
                         replace_unknowns=replace_unknowns,
                         max_batch_size=max_batch_size,
                         inter_threads=inter_threads)
    runtime = time.time() - start
    return results, runtime


# TODO(Jack Poulson): Automate the install of punkt elsewhere.
nltk.download('punkt')
