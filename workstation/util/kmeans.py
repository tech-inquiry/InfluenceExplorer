#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# NOTE: sklearn is left out of pip_requirements.
#
import pickle
import time

from sklearn.cluster import KMeans

def get_kmeans(embeddings, num_clusters=5000, num_seeds=10):
    '''Returns a scikit-learn KMeans fitting of the given embeddings.

    Args:
      embeddings: A matrix whose rows will be clustered with k-means.
      num_clusters: The number of clusters to build.
      num_seeds: The number of different initializations to use.

    Returns:
      An sklearn.cluster.KMeans estimator for the rows of the embedding matrix.
    '''
    estimator = KMeans(init='k-means++',
                       n_clusters=num_clusters,
                       n_init=num_seeds)
    start_time = time.time()
    estimator.fit(embeddings)
    end_time = time.time()
    run_time = end_time - start_time
    print('{}-way KMeans took {} seconds.'.format(num_clusters, run_time))
    return estimator


def save_estimator(estimator, filename):
    '''Saves an sklearn estimator via pickling.

    Args:
      estimator: The estimator to pickle.
      filename: The filename to pickle into.
    '''
    with open(filename, 'wb') as outfile:
        pickle.dump(estimator, outfile)


def load_estimator(filename):
    '''Loads an sklearn estimator that has been pickled.

    Args:
      filename: The filename containing the pickled estimator.

    Returns:
      The unpickled estimator.
    '''
    estimator = None
    with open(filename, 'rb') as infile:
        estimator = pickle.load(infile)
    return estimator
