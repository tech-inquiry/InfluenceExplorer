#
# Copyright (c) 2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

LABEL_TYPE_TO_PREFIX = {
    'TERM': ':',
    'ENTITY': 'entity:',
    # North American Industry Classification System codes.
    'PRINCIPAL_NAICS_CODE': 'naics:',
    'PRODUCT_OR_SERVICE_CODE': 'product_or_service:',
    'OFFICE': 'office:',
    'OFFICE_AGENCY': 'agency:',
    'OFFICE_DEPARTMENT': 'dept:'
}

LABEL_PREFIX_TO_TYPE = {v: k for k, v in LABEL_TYPE_TO_PREFIX.items()}

FUNDER_TYPES = ['OFFICE', 'OFFICE_AGENCY', 'OFFICE_DEPARTMENT']


def key_from_label(label, label_type):
    if label_type in LABEL_TYPE_TO_PREFIX:
        prefix = LABEL_TYPE_TO_PREFIX[label_type]
        return '{}{}'.format(prefix, label)
    else:
        return None


def label_type_from_key(key):
    for prefix in LABEL_PREFIX_TO_TYPE:
        if key.startswith(prefix):
            return LABEL_PREFIX_TO_TYPE[prefix]
    return None


def label_from_key(key):
    components = key.split(':', 1)
    if len(components) == 2:
        return components[1]
    else:
        return None


def key_is_funder(key):
    '''Returns whether the key corresponds to a funder.'''
    return label_type_from_key(key) in FUNDER_TYPES
