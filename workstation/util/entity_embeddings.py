#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Utilities for generating embeddings for squashed entity/term cooccurrence
# matrices.
#
# These python scripts can take some time to run over a year of FPDS awards.
# After sufficient exploration, (multithreaded) C++ equivalents should be
# substituted for search and embedding generation.
#
import copy
import glob
import pandas as pd
import json
import jsonlines
import numpy as np
import os
import re

from tqdm import tqdm

import gov.au.procurement as au_contracts
import gov.eu.procurement as eu_proc
import gov.ca.lobbying as ca_lobby
import gov.ca.procurement as ca_contracts
import gov.uk.procurement as uk_contracts
import gov.us.central.procurement as fpds
import gov.us.central.lobbying.opr as us_lobby
import gov.us.ca.procurement as us_ca_procurement
import gov.us.fl.procurement as us_fl_procurement
import util.entities
import util.keys
import util.scores
import util.webtext.main as webtext

from . import awls
from gov.eu.procurement.util import COUNTRIES

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '..')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')
EMBEDDING_DIR = os.path.join(EXPORT_DIR, 'embeddings')
SHARED_DIR = os.path.join(WORKSTATION_DIR, '..')
SHARED_DATA_DIR = os.path.join(SHARED_DIR, 'data')

VENDORS_CSV = '{}/vendors.csv'.format(EMBEDDING_DIR)
VENDOR_NEIGHBORS_CSV = '{}/vendor_neighbors.csv'.format(EMBEDDING_DIR)

NEEDED_ENVS = [
    'INF_EXP_POSTGRES_USER', 'INF_EXP_POSTGRES_PASSWORD',
    'INF_EXP_POSTGRES_DATABASE', 'INF_EXP_POSTGRES_PORT',
    'INF_EXP_POSTGRES_HOST'
]

# A map from vendor names to their JSON profiles.
ENTITIES = util.entities.get_entities()

# A map of journalism and miscellaneous entity-tagged citations.
CITATIONS = json.load(open('{}/data/citations.json'.format(SHARED_DIR)))
for url, item_orig in CITATIONS['citation'].items():
    item = copy.deepcopy(item_orig)
    item['url'] = url
    for entity_type in ['investigatedEntities', 'miscEntities']:
        if not entity_type in item:
            continue
        for entity in item[entity_type]:
            name = entity if type(entity) == str else entity['name']
            profile = ENTITIES[name]
            citation_type = 'misc' if entity_type == 'miscEntities' else 'journalism'
            if citation_type not in profile:
                profile[citation_type] = []
            profile[citation_type].append(item)

# A map from vendor names to their JSON financial profiles.
# This is now DEPRECATED.
ENTITY_FINANCIAL_PROFILES_FILENAME = '{}/financials.json'.format(
    SHARED_DATA_DIR)
ENTITY_FINANCIALS = json.load(open(ENTITY_FINANCIAL_PROFILES_FILENAME))

# A list of terms to skip.
SKIP_LIST_FILENAME = '{}/skip_list.json'.format(SHARED_DATA_DIR)
SKIP_LIST = json.load(open(SKIP_LIST_FILENAME))

WEBTEXT_FILENAME = '{}/webtext.json'.format(SHARED_DATA_DIR)

DEFAULT_INTERACTION_LENGTH = 10

DEFAULT_ROW_CAPS = {
    'TERM': 0,
    'ENTITY': 100000,
    'PRINCIPAL_NAICS_CODE': 0,
    'PRODUCT_OR_SERVICE_CODE': 0,
    'OFFICE': 0,
    'OFFICE_AGENCY': 0,
    'OFFICE_DEPARTMENT': 0
}

DEFAULT_COLUMN_CAPS = {
    'TERM': 75000,
    'ENTITY': 100000,
    'PRINCIPAL_NAICS_CODE': 50000,
    'PRODUCT_OR_SERVICE_CODE': 50000,
    'OFFICE': 100000,
    'OFFICE_AGENCY': 100000,
    'OFFICE_DEPARTMENT': 20000
}


def get_alt(vendor, entities):
    if vendor not in entities:
        return ''
    profile = entities[vendor]
    if 'name' not in profile:
        return ''
    names = profile['name']
    for key in ['dba', 'aka', 'short']:
        if key in names:
            return names[key]
    return ''


def lift_score(score, exponent):
    '''Lifts a score from a descendant using an exponential decay.

    Scores greater than one decay towards 1, but scores less than one are
    preserved.
    '''
    return score**(1. / exponent) if score > 1. else score


def write_vendors(vendor_list, dba_list):
    df = pd.DataFrame({
        'vendor': vendor_list,
        'dba': dba_list
    },
                      columns=['vendor', 'dba'])
    df = df.drop_duplicates(subset=['vendor'])
    df.vendor = df.vendor.str.replace('\\', '\\\\')
    df.dba = df.dba.str.replace('\\', '\\\\')
    util.postgres.df_to_csv(df, VENDORS_CSV)


def write_vendors_with_profiles(entities):
    vendor_set = set()
    vendor_list = []
    alt_list = []
    for vendore in entities:
        if vendor in vendor_set:
            continue
        vendor_set.add(vendor)
        vendor_list.append(vendor)
        alt = get_alt(vendor, entities)
        alt_list.append(alt)

    write_vendors(vendor_list, alt_list)


def get_entity_urls(name):
    urls = []
    if name not in ENTITIES:
        return urls
    profile = ENTITIES[name]
    if 'journalism' in profile:
        for article in profile['journalism']:
            item = {
                'type': 'journalism',
                'url': article['url'],
                'weight': 1.25
            }
            if 'org' in article:
                item['org'] = article['org']
            if 'author' in article:
                item['author'] = article['author']
            if 'title' in article:
                item['title'] = article['title']
            if 'date' in article:
                item['date'] = article['date']
            urls.append(item)
    if 'misc' in profile:
        for article in profile['misc']:
            item = {'type': 'misc', 'url': article['url'], 'weight': 0.3125}
            if 'org' in article:
                item['org'] = article['org']
            if 'author' in article:
                item['author'] = article['author']
            if 'title' in article:
                item['title'] = article['title']
            if 'date' in article:
                item['date'] = article['date']
            urls.append(item)

    if 'urls' in profile:
        url_dict = profile['urls']
        for url_key in url_dict:
            url_value = url_dict[url_key]
            if url_key == 'logo':
                # This is an image.
                continue
            if url_key == 'logoType':
                # There is no URL.
                continue
            if url_key == 'annual reports':
                # The page is essentially just metadata.
                continue
            if url_key == 'craft':
                # Said site has unreliable information.
                continue
            if url_key == 'facebook':
                continue
            if url_key == 'sec':
                # The page is essentially just metadata.
                continue
            if url_key == 'linkedin':
                # We can into processing errors.
                continue
            #if url_key == 'littlesis':
            #    # LittleSis appears to block python requests.. :-(
            #    continue
            if url_key == 'opencorporates':
                # OpenCorporates appears to block python requests... :-(
                continue
            if url_key == 'opensecrets':
                # We can into processing errors.
                continue
            if url_key == 'vimeo':
                continue
            if url_key == 'youtube':
                continue
            elif url_key == 'transparency':
                for article in url_value:
                    item = {
                        'type': url_key,
                        'url': article['citation'],
                        'weight': 1.25
                    }
                    urls.append(item)
            else:
                urls.append({
                    'type': url_key,
                    'url': url_value,
                    'weight': 0.125
                })

    return urls


def get_vendor_associations(entities=None, financials=None):
    '''Builds a symmetric association map.'''
    SCALE = 10
    ASSOCIATION_SCORE = SCALE
    COMPETITOR_SCORE = 6 * SCALE
    PRIMARY_PEER_SCORE = 6 * SCALE
    SECONDARY_PEER_SCORE = 3 * SCALE
    SELF_ASSOCIATION_SCORE = 6 * SCALE

    if entities is None:
        entities = ENTITIES
    if financials is None:
        financials = ENTITY_FINANCIALS

    children = util.entities.get_children(entities)

    print('Building first set of associations.')
    associations = {}
    for vendor, profile in entities.items():
        if 'associations' in profile:
            if vendor not in associations:
                associations[vendor] = list()
            for association in profile['associations']:
                from_weight = (association['from']['weight']
                               if 'weight' in association['from'] else 1.)
                to_weight = (association['to']['weight'] if
                             ('to' in association
                              and 'weight' in association['to']) else 1.)

                targets = []
                if 'textTemplate' in association:
                    for nameInfo in association['from']['names']:
                        if isinstance(nameInfo, str):
                            targets.append(nameInfo.lower())
                        else:
                            targets.append(nameInfo['name'].lower())
                else:
                    targets.append(association['from']['name'].lower())
                for target in targets:
                    if target in entities:
                        assn = {
                            'name': target,
                            'fromScore': from_weight * ASSOCIATION_SCORE,
                            'toScore': to_weight * ASSOCIATION_SCORE
                        }
                        associations[vendor].append(assn)
                    else:
                        print('Association btw {} and {}'.format(
                            vendor, target) + ' did not recognize the target')

    # Add peerings to each ancestor and symmetrize.
    print('Adding peerings to each ancestor and symmetrizing.')
    for vendor in list(associations.keys()):
        ancestors_and_exponents = util.entities.get_ancestors_and_weight_exponents(
            vendor, entities, children)
        ancestors_and_exponents.append({'name': vendor, 'exponent': 1.})

        old_target_associations = list(associations[vendor])
        ancestor_set = set()
        for ancestor_and_exponent in ancestors_and_exponents:
            ancestor = ancestor_and_exponent['name']
            exponent = ancestor_and_exponent['exponent']
            if ancestor in ancestor_set:
                continue
            ancestor_set.add(ancestor)

            if ancestor not in associations:
                associations[ancestor] = list()
            for target_association in old_target_associations:
                target = target_association['name']
                target_ancestors_and_exponents = util.entities.get_ancestors_and_weight_exponents(
                    target, entities, children)

                association = {
                    'name':
                    target_association['name'],
                    'fromScore':
                    lift_score(target_association['fromScore'], exponent),
                    'toScore':
                    target_association['toScore']
                }

                from_score = association['fromScore']
                to_score = association['toScore']
                if ancestor != vendor:
                    have_match = False
                    for target_ancestor_and_exponent in target_ancestors_and_exponents:
                        if ancestor == target_ancestor_and_exponent['name']:
                            have_match = True
                    if not have_match:
                        associations[ancestor].append(association)
                if target not in associations:
                    associations[target] = list()
                reversed_association = {
                    'name': ancestor,
                    'fromScore': to_score,
                    'toScore': from_score
                }
                associations[target].append(reversed_association)

                target_ancestors = set()
                for target_ancestor_and_exponent in target_ancestors_and_exponents:
                    target_ancestor = target_ancestor_and_exponent['name']
                    target_exponent = target_ancestor_and_exponent['exponent']
                    if target_ancestor in target_ancestors:
                        continue
                    target_ancestors.add(target_ancestor)
                    associations[ancestor].append({
                        'name':
                        target_ancestor,
                        'fromScore':
                        from_score,
                        'toScore':
                        lift_score(to_score, target_exponent)
                    })
                    if target_ancestor not in associations:
                        associations[target_ancestor] = list()
                    associations[target_ancestor].append({
                        'name':
                        ancestor,
                        'fromScore':
                        lift_score(to_score, target_exponent),
                        'toScore':
                        from_score
                    })

    # Add self-associations with ancestors.
    print('Adding self-associations with ancestors.')
    for vendor in entities:
        ancestors_and_exponents = util.entities.get_ancestors_and_weight_exponents(
            vendor, entities, children)
        ancestors_and_exponents.append({'name': vendor, 'exponent': 1.})
        if vendor not in associations:
            associations[vendor] = list()

        ancestor_set = set()
        for ancestor_and_exponent in ancestors_and_exponents:
            ancestor = ancestor_and_exponent['name']
            exponent = ancestor_and_exponent['exponent']

            if ancestor in ancestor_set:
                continue
            ancestor_set.add(ancestor)

            associations[vendor].append({
                'name':
                ancestor,
                'fromScore':
                SELF_ASSOCIATION_SCORE,
                'toScore':
                lift_score(SELF_ASSOCIATION_SCORE, exponent)
            })

            if ancestor not in associations:
                associations[ancestor] = list()
            associations[ancestor].append({
                'name':
                vendor,
                'fromScore':
                lift_score(SELF_ASSOCIATION_SCORE, exponent),
                'toScore':
                SELF_ASSOCIATION_SCORE
            })

    print('Done constructing associations')

    return associations


def join_text(text, addition):
    if text:
        return '{} {}'.format(text, addition)
    else:
        return addition


def normalize_text(text):
    '''Normalizes a text field for further processing.

    We assume that the input is lower-case.
    '''
    if not text:
        return None

    stripped_text = text.translate(
        str.maketrans('\n\t&', '   ', '!"#$%\'()*+,-./:;<=>?@[\\]^_`{|}~'))
    return stripped_text.strip()


def tokenize_text(text, skip_set=None, skip_digits=True, min_chars=3):
    '''Returns a tokenized list of terms.

    Args:
      text: The text string to tokenize via whitespace.
      skip_set: The set of terms that should be filtered out.

    Returns:
      A list of tokens from the text.
    '''
    if not text:
        return []

    token_list = text.split(' ')
    token_list = [token for token in token_list if len(token) >= min_chars]
    if skip_digits:
        token_list = [token for token in token_list if not token.isdigit()]
    if skip_set:
        token_list = [token for token in token_list if token not in skip_set]
    return token_list


def nearest_neighbors(embeddings,
                      key,
                      key_index_bijection,
                      num_neighbors=20,
                      penalize_smaller_norm=True,
                      only_vendors=True,
                      embedding_norms=None):
    '''Returns lists of keys, indices, and cosine-similarities of neighbors.

    Args:
      embeddings: The tall-skinny matrix of embeddings.
      key: The key of the embedding to query neighbors of.
      key_index_bijection: The KeyIndexBijection between keys and indices.
      num_neighbors: The maximum number of neighbors to return.
      penalize_smaller_norm: If true, the traditional cosine similarity measure
          <x, y> / (|| x || || y ||)
        is replaced by
          <x, y> / (|| x || max(|| x ||, || y ||)),
        which is equal to cosine similarity when y is as large as x, but smaller
        otherwise.
      only_vendors: If true, only vendors are preserved in the results.
      embedding_norms: If specified, the list of two-norms of rows of the
        embedding matrix.

    Returns:
      The tuple of the list of keys and indices of the nearest neighbors and
      their cosine similarity to the query embedding. If only_vendors is True,
      the vendor names are returned, rather than their corresponding (prefixed)
      keys.
    '''
    awls_result = awls.nearest_neighbors(
        embeddings,
        key,
        key_index_bijection,
        num_neighbors=num_neighbors,
        penalize_smaller_norm=penalize_smaller_norm,
        embedding_norms=embedding_norms)

    if only_vendors:
        vendor_keys = []
        vendor_indices = []
        vendor_cosines = []

        num_returned_neighbors = len(awls_result['indices'])
        for i in range(num_returned_neighbors):
            key = awls_result['keys'][i]
            if util.keys.label_type_from_key(key) != 'ENTITY':
                continue
            vendor = util.keys.label_from_key(key)
            vendor_keys.append(vendor)
            vendor_indices.append(awls_result['indices'][i])
            vendor_cosines.append(awls_result['cosines'][i])

        keys = vendor_keys
        indices = vendor_indices
        cosines = vendor_cosines

    return {'keys': keys, 'indices': indices, 'cosines': cosines}


def get_all_nearest_neighbors(embeddings,
                              key_index_bijection,
                              num_neighbors=20,
                              penalize_smaller_norm=True,
                              only_vendors=True,
                              verbose=True):
    '''Returns a dictionary from every vendor to its nearest neighbors.

    Args:
      embeddings: The tall-skinny matrix of embeddings.
      key_index_bijection: The KeyIndexBijection between keys and indices.
      num_neighbors: The maximum number of neighbors to return.
      penalize_smaller_norm: If true, the traditional cosine similarity measure
          <x, y> / (|| x || || y ||)
        is replaced by
          <x, y> / (|| x || max(|| x ||, || y ||)),
        which is equal to cosine similarity when y is as large as x, but smaller
        otherwise.
      only_vendors: If true, only vendors are preserved in the results.
      verbose: If true, progress is periodically printed.

    Returns:
      The tuple of the list of keys and indices of the nearest neighbors and
      their cosine similarity to the query embedding. If only_vendors is True,
      the vendor names are returned, rather than their corresponding (prefixed)
      keys.
    '''
    neighbors = {}
    num_keys = len(key_index_bijection.key_to_index)
    num_keys_percent = num_keys // 100
    if verbose:
        print('Will process {} source keys.'.format(num_keys))

    embedding_norms = [
        np.linalg.norm(embeddings[i, :]) for i in range(num_keys)
    ]
    if verbose:
        print('Cached embedding norms.')

    # Get the total number of entity keys.
    num_entity_keys = 0
    for key in key_index_bijection.key_to_index:
        if util.keys.label_type_from_key(key) == 'ENTITY':
            num_entity_keys += 1

    with tqdm(total=num_entity_keys) as pbar:
        for key in key_index_bijection.key_to_index:
            if util.keys.label_type_from_key(key) != 'ENTITY':
                continue
            vendor = util.keys.label_from_key(key)
            result = nearest_neighbors(
                embeddings,
                key,
                key_index_bijection,
                num_neighbors=num_neighbors,
                penalize_smaller_norm=penalize_smaller_norm,
                only_vendors=only_vendors,
                embedding_norms=embedding_norms)
            if only_vendors:
                neighbors[vendor] = result['keys']
            else:
                neighbors[key] = result['keys']
            pbar.update(1)

    return neighbors


def scale_caps(caps, scale):
    scaled_caps = {}
    for item_type in caps:
        scaled_caps[item_type] = caps[item_type] * scale
    return scaled_caps


def incorporate_webtext(scores,
                        entities,
                        hparams,
                        row_caps,
                        column_caps,
                        verbose=False):
    # Prevent truncation of any entities with defined profiles.
    keep_list = list(entities.keys())

    # Ensure that we have updated webtext.
    webtext.cache_entity_websites(entities, WEBTEXT_FILENAME)
    webtext_data = json.load(open(WEBTEXT_FILENAME))

    skip_set = set(SKIP_LIST)

    include_row_terms = row_caps['TERM'] > 0
    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
                               row_caps['OFFICE_DEPARTMENT']) > 0)
    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)

    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])

    # Get the total number of URLs in the webtext_data.
    num_urls = 0
    for source in entities:
        urls = get_entity_urls(source)
        for item in urls:
            if item['url'] in webtext_data:
                num_urls += 1

    num_incorporated = 0
    with tqdm(total=num_urls) as pbar:
        for source, profile in entities.items():
            urls = get_entity_urls(source)
            for item in urls:
                url = item['url']
                weight = item['weight']

                ancestors = util.entities.get_ancestors(source, entities)
                header = {'ENTITY': ancestors + [source]}

                text = ''
                if url in webtext_data:
                    if verbose:
                        print('  Using cache for {}: {}'.format(source, url))
                    text = webtext_data[url]['text']
                else:
                    if verbose:
                        print('  Skipping {}'.format(url))
                    continue
                text = text.lower()
                normalized_text = normalize_text(text)
                tokens = tokenize_text(normalized_text, skip_set)
                if verbose:
                    print('  Retrieved {} tokens.'.format(len(tokens)))

                util.scores.update_scores(
                    tokens,
                    header,
                    scores,
                    interaction_length=hparams['interaction_length'],
                    include_row_terms=include_row_terms,
                    include_row_funders=include_row_funders,
                    include_row_product_types=include_row_product_types,
                    multiplier=weight)

                num_incorporated += 1
                if num_incorporated % hparams['truncation_rate'] == 0:
                    if verbose:
                        print('Truncating scores.')
                    kept = util.scores.truncate_scores(scores,
                                                       row_trunc_caps,
                                                       column_trunc_caps,
                                                       keep_rows=keep_list,
                                                       keep_columns=keep_list,
                                                       verbose=verbose)
                pbar.update(1)


def incorporate_us_central_procurement(scores,
                                       entities,
                                       hparams,
                                       row_caps,
                                       column_caps,
                                       verbose=False):
    # Since we do not have access to the unique key from FPDS, we build a map
    # from PIIDs to the list of unique keys with such a PIID.
    #
    # As of this writing, the combination of PIID and parent PIID is entirely
    # unique.
    def piids_to_key(piid, parent_piid):
        if parent_piid:
            return '{} {}'.format(piid, parent_piid)
        else:
            return piid

    def subaward_to_key(subaward):
        piid = subaward['prime_piid']
        parent_piid = subaward['prime_parent_piid']
        return piids_to_key(piid, parent_piid)

    # Prevent truncation of any entities with defined profiles.
    keep_list = list(entities.keys())

    # Get the list of terms to skip when tokenizing.
    skip_set = set(SKIP_LIST)

    # Get the normalization map for entity names.
    normalization = fpds.get_normalization_map(entities)
    contractor_normalization = fpds.get_contractor_normalization_map(entities)

    # Get the map normalizing (dept, agency, office) tuples to US gov entities.
    agency_normalization = fpds.get_agency_normalization_map(entities)

    include_row_terms = row_caps['TERM'] > 0
    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
                               row_caps['OFFICE_DEPARTMENT']) > 0)
    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)

    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])

    prime_filenames = sorted(glob.glob(hparams['file_regexp']))
    num_prime_filenames = len(prime_filenames)

    sub_filename = hparams['subaward_file']
    if not os.path.exists(sub_filename):
        print('{} did not exist.'.format(sub_filename))
        return
    sub_json = (json.load(open(sub_filename))
                if sub_filename is not None else None)

    # We can free up memory by only keeping the portion of the subawards which
    # we will use for our associations. An alternative would be importing a
    # custom file which is pre-filtered.
    KEPT_SUBAWARD_KEYS = [
        'prime_key', 'prime_piid', 'prime_parent_piid', 'sub_uei', 'sub_name',
        'sub_description'
    ]
    for unique_key in sub_json:
        for subaward in sub_json[unique_key]:
            for subaward_key in list(subaward.keys()):
                if subaward_key not in KEPT_SUBAWARD_KEYS:
                    del subaward[subaward_key]

    piid_to_uniques = {}
    for unique_key in sub_json:
        subaward = sub_json[unique_key][0]
        key = subaward_to_key(subaward)
        if key not in piid_to_uniques:
            piid_to_uniques[key] = []
        piid_to_uniques[key].append(unique_key)

    # We will use this to prevent processing subaward data more than once.
    piid_keys_processed = set()

    num_incorporated = 0
    with tqdm(total=num_prime_filenames) as pbar:
        for i in range(num_prime_filenames):
            prime_filename = prime_filenames[i]
            if verbose:
                print('  Processing US award file {} of {}: {}'.format(
                    i, num_prime_filenames, prime_filename))
            with jsonlines.open(prime_filename) as reader:
                for filing in reader:
                    prime = fpds.award.get_vendor(filing)
                    prime_uei = fpds.award.get_uei(filing)
                    if prime_uei:
                        prime_uei = prime_uei.lower()
                    if prime:
                        prime = fpds.normalize(prime, prime_uei, normalization)
                    else:
                        prime_parent = fpds.award.get_ultimate_parent(filing)
                        prime_parent_uei = fpds.award.get_ultimate_parent_uei(
                            filing)
                        prime_uei = prime_uei if prime_uei else prime_parent_uei
                        prime = fpds.normalize(prime_parent, prime_uei,
                                               normalization)
                    prime_contractor = fpds.award.get_contractor_name(filing)
                    if prime_contractor:
                        prime_contractor = fpds.normalize_contractor(
                            prime_contractor, normalization,
                            contractor_normalization)

                    piid = fpds.award.get_piid(filing)
                    parent_piid = fpds.award.get_parent_piid(filing)
                    piid_key = piids_to_key(piid, parent_piid)

                    contracting_department = fpds.award.get_department(
                        filing, contracting=True)
                    contracting_department_code = fpds.award.get_department(
                        filing, contracting=True, name=False)
                    contracting_agency = fpds.award.get_agency(
                        filing, contracting=True)
                    contracting_agency_code = fpds.award.get_agency(
                        filing, contracting=True, name=False)
                    contracting_office = fpds.award.get_office(
                        filing, contracting=True)
                    contracting_office_code = fpds.award.get_office(
                        filing, contracting=True, name=False)
                    contracting_entity = fpds.normalize_agency(
                        contracting_department_code, contracting_agency_code,
                        contracting_office_code, agency_normalization)
                    contracting_entity_ancestors = util.entities.get_ancestors(
                        contracting_entity, entities)

                    funding_department = fpds.award.get_department(
                        filing, contracting=False)
                    funding_department_code = fpds.award.get_department(
                        filing, contracting=False, name=False)
                    funding_agency = fpds.award.get_agency(filing,
                                                           contracting=False)
                    funding_agency_code = fpds.award.get_agency(
                        filing, contracting=False, name=False)
                    funding_office = fpds.award.get_office(filing,
                                                           contracting=False)
                    funding_office_code = fpds.award.get_office(
                        filing, contracting=False, name=False)
                    funding_entity = fpds.normalize_agency(
                        funding_department_code, funding_agency_code,
                        funding_office_code, agency_normalization)
                    funding_entity_ancestors = util.entities.get_ancestors(
                        funding_entity, entities)

                    naics_code = fpds.award.get_principal_naics_code(filing)
                    product_or_service_code = fpds.award.get_product_or_service_code(
                        filing)

                    header = {
                        'ENTITY': [funding_entity] + funding_entity_ancestors +
                        [contracting_entity] + contracting_entity_ancestors,
                        'PRINCIPAL_NAICS_CODE': [],
                        'PRODUCT_OR_SERVICE_CODE': [],
                        'OFFICE': [],
                        'OFFICE_AGENCY': [],
                        'OFFICE_DEPARTMENT': []
                    }
                    if prime:
                        ancestors = util.entities.get_ancestors(
                            prime, entities)
                        header['ENTITY'] += ancestors + [prime]
                    if prime_contractor:
                        ancestors = util.entities.get_ancestors(
                            prime_contractor, entities)
                        header['ENTITY'] += ancestors + [prime_contractor]
                    if naics_code:
                        header['PRINCIPAL_NAICS_CODE'].append(naics_code)
                    if product_or_service_code:
                        header['PRODUCT_OR_SERVICE_CODE'].append(
                            product_or_service_code)
                    if contracting_office:
                        header['OFFICE'].append(contracting_office)
                    if funding_office:
                        header['OFFICE'].append(funding_office)
                    if contracting_department:
                        header['OFFICE_DEPARTMENT'].append(
                            contracting_department)
                        if contracting_agency:
                            header['OFFICE_AGENCY'].append('{}; {}'.format(
                                contracting_department, contracting_agency))

                    if funding_department:
                        header['OFFICE_DEPARTMENT'].append(funding_department)
                        if funding_agency:
                            # Grab the department.
                            header['OFFICE_AGENCY'].append('{}; {}'.format(
                                funding_department, funding_agency))

                    subawards = []
                    sub_entities_lists = []
                    init_piid = False
                    if piid_key in piid_to_uniques:
                        uniques = piid_to_uniques[piid_key]
                        init_piid = piid_key not in piid_keys_processed
                        piid_keys_processed.add(piid_key)
                        if len(uniques) == 1:
                            subawards = sub_json[uniques[0]]
                            for subaward in subawards:
                                sub_entities = []
                                sub = subaward['sub_name']
                                sub = sub.lower() if sub else ''
                                sub_uei = subaward['sub_uei']
                                if sub_uei:
                                    sub_uei = sub_uei.lower()
                                sub = fpds.normalize(sub, sub_uei,
                                                     normalization)
                                if sub:
                                    sub_entities.append(sub)
                                sub_ancestors = util.entities.get_ancestors(
                                    sub, entities)
                                if sub_ancestors is not None:
                                    sub_entities += sub_ancestors
                                sub_entities_lists.append(sub_entities)
                        else:
                            if verbose:
                                print('PIID pair {}/{} was non-unique'.format(
                                    piid, parent_piid))

                    prime_entities = header['ENTITY'].copy()

                    # Incorporate the prime award text using all of the entities,
                    # including the subs and their ancestors.
                    all_entities = prime_entities.copy()
                    for sub_entities in sub_entities_lists:
                        all_entities += sub_entities
                    header['ENTITY'] = all_entities
                    text = fpds.award.get_description_of_contract_requirement(
                        filing)
                    normalized_text = normalize_text(text.lower())
                    tokens = tokenize_text(normalized_text, skip_set)
                    util.scores.update_scores(
                        tokens,
                        header,
                        scores,
                        interaction_length=hparams['interaction_length'],
                        include_row_terms=include_row_terms,
                        include_row_funders=include_row_funders,
                        include_row_product_types=include_row_product_types,
                        header_update=hparams['header_update'],
                        multiplier=hparams['weight'])

                    # If this is the first time the piid key has been
                    # If we have subawards, and this is our first time
                    # encountering them, then process the subaward text with a
                    # single subawardee combined with the parent at a time.
                    if init_piid:
                        for subaward_index in range(len(subawards)):
                            subaward = subawards[subaward_index]
                            sub_entities = sub_entities_lists[subaward_index]
                            header['ENTITY'] = prime_entities + sub_entities
                            text = subaward['sub_description']
                            if text is None:
                                text = ''
                            normalized_text = normalize_text(text.lower())
                            tokens = tokenize_text(normalized_text, skip_set)
                            util.scores.update_scores(
                                tokens,
                                header,
                                scores,
                                interaction_length=hparams[
                                    'interaction_length'],
                                include_row_terms=include_row_terms,
                                include_row_funders=include_row_funders,
                                include_row_product_types=
                                include_row_product_types,
                                header_update=hparams['header_update'],
                                multiplier=hparams['subaward_weight'])

                    num_incorporated += 1
                    if num_incorporated % hparams['truncation_rate'] == 0:
                        if verbose:
                            print('Truncating scores.')
                        kept = util.scores.truncate_scores(
                            scores,
                            row_trunc_caps,
                            column_trunc_caps,
                            keep_rows=keep_list,
                            keep_columns=keep_list,
                            verbose=verbose)
                pbar.update(1)


def incorporate_us_central_lobbying_filings(scores,
                                            entities,
                                            hparams,
                                            row_caps,
                                            column_caps,
                                            verbose=False):
    # Prevent truncation of any entities with defined profiles.
    keep_list = list(entities.keys())

    # Get the list of terms to skip when tokenizing.
    skip_set = set(SKIP_LIST)

    # Get the normalization map for entity names.
    normalization = us_lobby.get_normalization_map(entities)

    include_row_terms = row_caps['TERM'] > 0
    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
                               row_caps['OFFICE_DEPARTMENT']) > 0)
    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)

    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])

    json_filenames = sorted(glob.glob(hparams['file_regexp']))
    num_filenames = len(json_filenames)

    # Get the total number of filings.
    num_filings = 0
    for json_filename in json_filenames:
        filings = json.load(open(json_filename))
        num_filings += len(filings)

    num_incorporated = 0
    with tqdm(total=num_filings) as pbar:
        for i in range(num_filenames):
            json_filename = json_filenames[i]
            if verbose:
                print('  Processing lobbying filings file {} of {}: {}'.format(
                    i, num_filenames, json_filename))
            filings = json.load(open(json_filename))
            for filing in filings:
                header = {
                    'ENTITY': ['government of the united states'],
                    'PRINCIPAL_NAICS_CODE': [],
                    'PRODUCT_OR_SERVICE_CODE': [],
                    'OFFICE': [],
                    'OFFICE_AGENCY': [],
                    'OFFICE_DEPARTMENT': []
                }

                registrant = filing['registrant']['name']
                if registrant:
                    registrant = us_lobby.normalize(registrant, normalization)
                    ancestors = util.entities.get_ancestors(
                        registrant, entities)
                    header['ENTITY'] += ancestors + [registrant]

                client = filing['client']['name']
                if client:
                    client = us_lobby.normalize(client, normalization)
                    ancestors = util.entities.get_ancestors(client, entities)
                    header['ENTITY'] += ancestors + [client]

                for affiliate in filing['affiliated_organizations']:
                    name = us_lobby.normalize(affiliate['name'], normalization)
                    ancestors = util.entities.get_ancestors(name, entities)
                    header['ENTITY'] += ancestors + [name]

                for affiliate in filing['foreign_entities']:
                    name = us_lobby.normalize(affiliate['name'], normalization)
                    ancestors = util.entities.get_ancestors(name, entities)
                    header['ENTITY'] += ancestors + [name]

                text = ''
                if 'general_description' in filing['client']:
                    description = filing['client']['general_description']
                    if description:
                        join_text(text, description.lower())
                for activity in filing['lobbying_activities']:
                    if 'description' not in activity or not activity[
                            'description']:
                        continue
                    description = activity['description'].lower().replace(
                        ';', ' ')
                    join_text(text, description)
                normalized_text = normalize_text(text.lower())
                tokens = tokenize_text(normalized_text, skip_set)

                util.scores.update_scores(
                    tokens,
                    header,
                    scores,
                    interaction_length=hparams['interaction_length'],
                    include_row_terms=include_row_terms,
                    include_row_funders=include_row_funders,
                    include_row_product_types=include_row_product_types,
                    header_update=hparams['header_update'],
                    multiplier=hparams['weight'])

                num_incorporated += 1
                if num_incorporated % hparams['truncation_rate'] == 0:
                    if verbose:
                        print('Truncating scores.')
                    kept = util.scores.truncate_scores(scores,
                                                       row_trunc_caps,
                                                       column_trunc_caps,
                                                       keep_rows=keep_list,
                                                       keep_columns=keep_list,
                                                       verbose=verbose)
                pbar.update(1)


# TODO(Jack Poulson): Begin storing California procurement rather than deleting
# after each retrieval. Afterwards, mirror the Florida procurement approach.


def incorporate_us_fl_procurement(scores,
                                  entities,
                                  hparams,
                                  row_caps,
                                  column_caps,
                                  verbose=False):
    # Prevent truncation of any entities with defined profiles.
    keep_list = list(entities.keys())

    # Get the list of terms to skip when tokenizing.
    skip_set = set(SKIP_LIST)

    # Get the normalization map for entity names.
    normalization = us_fl_procurement.get_normalization_map(entities)

    include_row_terms = row_caps['TERM'] > 0
    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
                               row_caps['OFFICE_DEPARTMENT']) > 0)
    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)

    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])

    json_filenames = sorted(glob.glob(hparams['file_regexp']))
    num_filenames = len(json_filenames)

    # Get the total number of filings.
    num_filings = 0
    for json_filename in json_filenames:
        filings = json.load(open(json_filename))
        num_filings += len(filings)

    num_incorporated = 0
    with tqdm(total=num_filings) as pbar:
        for i in range(num_filenames):
            json_filename = json_filenames[i]
            if verbose:
                print(
                    '  Processing US FL procurement file {} of {}: {}'.format(
                        i, num_filenames, json_filename))
            filings = json.load(open(json_filename))
            for filing in filings:
                header = {
                    'ENTITY': ['state of florida'],
                    'PRINCIPAL_NAICS_CODE': [],
                    'PRODUCT_OR_SERVICE_CODE': [],
                    'OFFICE': [],
                    'OFFICE_AGENCY': [],
                    'OFFICE_DEPARTMENT': []
                }

                vendor_pieces = []
                if filing['Vendor/Grantor Name']:
                    vendor_pieces.append(filing['Vendor/Grantor Name'])
                if filing['Vendor/Grantor Name Line 2']:
                    vendor_pieces.append(filing['Vendor/Grantor Name Line 2'])
                if len(vendor_pieces):
                    name = ' '.join(vendor_pieces).lower()
                    name = us_fl_procurement.normalize(name, normalization)
                    header['ENTITY'].append(name)

                if filing['Agency Name']:
                    header['OFFICE_AGENCY'].append(
                        filing['Agency Name'].lower())

                text = ''
                if filing['Long Title/PO Title']:
                    join_text(text, filing['Long Title/PO Title'])
                if filing['Short Title']:
                    join_text(text, filing['Short Title'])
                if filing['Comment']:
                    join_text(text, filing['Comment'])
                normalized_text = normalize_text(text.lower())
                tokens = tokenize_text(normalized_text, skip_set)

                util.scores.update_scores(
                    tokens,
                    header,
                    scores,
                    interaction_length=hparams['interaction_length'],
                    include_row_terms=include_row_terms,
                    include_row_funders=include_row_funders,
                    include_row_product_types=include_row_product_types,
                    header_update=hparams['header_update'],
                    multiplier=hparams['weight'])

                num_incorporated += 1
                if num_incorporated % hparams['truncation_rate'] == 0:
                    if verbose:
                        print('Truncating scores.')
                    kept = util.scores.truncate_scores(scores,
                                                       row_trunc_caps,
                                                       column_trunc_caps,
                                                       keep_rows=keep_list,
                                                       keep_columns=keep_list,
                                                       verbose=verbose)
                pbar.update(1)


def incorporate_uk_contracts(scores,
                             entities,
                             hparams,
                             row_caps,
                             column_caps,
                             verbose=False):
    # Prevent truncation of any entities with defined profiles.
    keep_list = list(entities.keys())

    # Get the list of terms to skip when tokenizing.
    skip_set = set(SKIP_LIST)

    # Get the normalization map for entity names.
    normalization = uk_contracts.get_normalization_map(entities)
    agency_normalization = uk_contracts.get_agency_normalization_map(entities)

    include_row_terms = row_caps['TERM'] > 0
    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
                               row_caps['OFFICE_DEPARTMENT']) > 0)
    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)

    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])

    json_filenames = sorted(glob.glob(hparams['file_regexp']))
    num_filenames = len(json_filenames)

    # Get the total number of filings.
    num_filings = 0
    for json_filename in json_filenames:
        filings = json.load(open(json_filename))
        num_filings += len(filings)

    num_incorporated = 0
    with tqdm(total=num_filings) as pbar:
        for i in range(num_filenames):
            json_filename = json_filenames[i]
            if verbose:
                print('  Processing UK contract file {} of {}: {}'.format(
                    i, num_filenames, json_filename))
            filings = json.load(open(json_filename))
            for filing in filings:
                header = {
                    'ENTITY': ['government of the united kingdom'],
                    'PRINCIPAL_NAICS_CODE': [],
                    'PRODUCT_OR_SERVICE_CODE': [],
                    'OFFICE': [],
                    'OFFICE_AGENCY': [],
                    'OFFICE_DEPARTMENT': []
                }

                text = ''
                if 'tender' in filing and filing['tender'] is not None:
                    tender = filing['tender']
                    if 'title' in tender:
                        title = tender['title'].lower().replace(';',
                                                                ' ').replace(
                                                                    ':', ' ')
                        join_text(text, title)
                    if 'description' in tender:
                        description = tender['description'].lower()
                        join_text(text, description)
                    if 'items' in tender:
                        for item in tender['items']:
                            if 'classification' in item:
                                classification = item['classification']
                                if 'description' in classification:
                                    description = classification['description']
                                    join_text(text, description.lower())
                            elif 'description' in item:
                                description = item['description'].lower()
                                join_text(text, description)
                if 'buyer' in filing and filing['buyer'] is not None:
                    buyer = filing['buyer']
                    if 'name' in buyer:
                        agency = uk_contracts.normalize(
                            buyer['name'], agency_normalization)
                        header['ENTITY'].append(agency)
                        header['OFFICE_AGENCY'].append(agency)
                if 'award' in filing and filing['award'] is not None:
                    award = filing['award']
                    if 'suppliers' in award and award['suppliers'] is not None:
                        for supplier in award['suppliers']:
                            if 'name' in supplier:
                                name = supplier['name']
                                name = uk_contracts.normalize(
                                    name, normalization)
                                header['ENTITY'].append(name)

                normalized_text = normalize_text(text.lower())
                tokens = tokenize_text(normalized_text, skip_set)

                util.scores.update_scores(
                    tokens,
                    header,
                    scores,
                    interaction_length=hparams['interaction_length'],
                    include_row_terms=include_row_terms,
                    include_row_funders=include_row_funders,
                    include_row_product_types=include_row_product_types,
                    header_update=hparams['header_update'],
                    multiplier=hparams['weight'])

                num_incorporated += 1
                if num_incorporated % hparams['truncation_rate'] == 0:
                    if verbose:
                        print('Truncating scores.')
                    kept = util.scores.truncate_scores(scores,
                                                       row_trunc_caps,
                                                       column_trunc_caps,
                                                       keep_rows=keep_list,
                                                       keep_columns=keep_list,
                                                       verbose=verbose)
                pbar.update(1)


def incorporate_eu_contracts(country_code,
                             file_regexp,
                             scores,
                             entities,
                             hparams,
                             row_caps,
                             column_caps,
                             verbose=False):
    # Prevent truncation of any entities with defined profiles.
    keep_list = list(entities.keys())

    # Get the list of terms to skip when tokenizing.
    skip_set = set(SKIP_LIST)

    # Get the name of the entity corresponding to this country code.
    country_entity = COUNTRIES[country_code]['entity']

    # Get the normalization map for entity names.
    normalization = eu_proc.get_normalization_map(country_code, entities)

    include_row_terms = row_caps['TERM'] > 0
    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
                               row_caps['OFFICE_DEPARTMENT']) > 0)
    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)

    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])

    json_filenames = sorted(glob.glob(file_regexp))
    num_filenames = len(json_filenames)

    # Get the total number of filings.
    num_filings = 0
    for json_filename in json_filenames:
        filings = json.load(open(json_filename))
        num_filings += len(filings)

    num_incorporated = 0
    with tqdm(total=num_filings) as pbar:
        for i in range(num_filenames):
            json_filename = json_filenames[i]
            if verbose:
                print('  Processing EU {} contract file {} of {}: {}'.format(
                    country_code, i, num_filenames, json_filename))
            filings = json.load(open(json_filename))
            for filing in filings:
                if 'is_legacy' in filing and not filing['is_legacy']:
                    # TODO(Jack Poulson): Add support.
                    continue
                eu_proc.legacy.remove_nulls(filing)

                header = {
                    'ENTITY': [country_entity],
                    'PRINCIPAL_NAICS_CODE': [],
                    'PRODUCT_OR_SERVICE_CODE': [],
                    'OFFICE': [],
                    'OFFICE_AGENCY': [],
                    'OFFICE_DEPARTMENT': []
                }

                # TODO(Jack Poulson): Switch to newer functions
                contractors = list(
                    set(
                        eu_proc.legacy.get_contractors(filing) +
                        eu_proc.legacy.get_defence_operators(filing)))
                if not len(contractors):
                    pbar.update(1)
                    continue
                for name in contractors:
                    name = eu_proc.normalize(name, normalization)
                    header['ENTITY'].append(name)

                text = '  '.join(eu_proc.legacy.get_descriptions(filing))

                # TODO(Jack Poulson): Add OFFICE, OFFICE_AGENCY, NAICS, etc.

                normalized_text = normalize_text(text.lower())
                tokens = tokenize_text(normalized_text, skip_set)

                util.scores.update_scores(
                    tokens,
                    header,
                    scores,
                    interaction_length=hparams['interaction_length'],
                    include_row_terms=include_row_terms,
                    include_row_funders=include_row_funders,
                    include_row_product_types=include_row_product_types,
                    header_update=hparams['header_update'],
                    multiplier=hparams['weight'])

                num_incorporated += 1
                if num_incorporated % hparams['truncation_rate'] == 0:
                    if verbose:
                        print('Truncating scores.')
                    kept = util.scores.truncate_scores(scores,
                                                       row_trunc_caps,
                                                       column_trunc_caps,
                                                       keep_rows=keep_list,
                                                       keep_columns=keep_list,
                                                       verbose=verbose)
                pbar.update(1)


def incorporate_au_contracts(scores,
                             entities,
                             hparams,
                             row_caps,
                             column_caps,
                             verbose=False):
    # Prevent truncation of any entities with defined profiles.
    keep_list = list(entities.keys())

    # Get the list of terms to skip when tokenizing.
    skip_set = set(SKIP_LIST)

    # Get the normalization map for vendors and agencies names.
    normalization = au_contracts.get_normalization_map(entities)
    agency_normalization = au_contracts.get_agency_normalization_map(entities)

    include_row_terms = row_caps['TERM'] > 0
    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
                               row_caps['OFFICE_DEPARTMENT']) > 0)
    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)

    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])

    json_filenames = sorted(glob.glob(hparams['file_regexp']))
    num_filenames = len(json_filenames)

    # Get the total number of filings.
    num_filings = 0
    for json_filename in json_filenames:
        filings = json.load(open(json_filename))
        num_filings += len(filings)

    num_incorporated = 0
    with tqdm(total=num_filings) as pbar:
        for i in range(num_filenames):
            json_filename = json_filenames[i]
            if verbose:
                print('  Processing AU contract file {} of {}: {}'.format(
                    i, num_filenames, json_filename))
            filings = json.load(open(json_filename))
            for filing in filings:

                header = {
                    'ENTITY': ['government of australia'],
                    'PRINCIPAL_NAICS_CODE': [],
                    'PRODUCT_OR_SERVICE_CODE': [],
                    'OFFICE': [],
                    'OFFICE_AGENCY': [],
                    'OFFICE_DEPARTMENT': []
                }

                if 'parties' in filing and filing['parties'] is not None:

                    def is_procuring_entity(party):
                        if 'role' not in party or party['role'] is None:
                            return False
                        if party['role'] == 'procuringEntity':
                            return True

                    for party in filing['parties']:
                        if 'name' not in party or party['name'] is None:
                            continue
                        name = party['name']
                        if is_procuring_entity(party):
                            agency = au_contracts.normalize_agency(
                                name, agency_normalization)
                            header['OFFICE_AGENCY'].append(agency)
                            header['ENTITY'].append(agency)
                        else:
                            abn = au_contracts.get_party_abn(party)
                            header['ENTITY'].append(
                                au_contracts.normalize(name, abn,
                                                       normalization))

                text = ''
                if 'contract' in filing:
                    contract = filing['contract']
                    if 'description' in contract:
                        description = contract['description'].lower()
                        join_text(text, description)

                normalized_text = normalize_text(text.lower())
                tokens = tokenize_text(normalized_text, skip_set)

                util.scores.update_scores(
                    tokens,
                    header,
                    scores,
                    interaction_length=hparams['interaction_length'],
                    include_row_terms=include_row_terms,
                    include_row_funders=include_row_funders,
                    include_row_product_types=include_row_product_types,
                    header_update=hparams['header_update'],
                    multiplier=hparams['weight'])

                num_incorporated += 1
                if num_incorporated % hparams['truncation_rate'] == 0:
                    if verbose:
                        print('Truncating scores.')
                    kept = util.scores.truncate_scores(scores,
                                                       row_trunc_caps,
                                                       column_trunc_caps,
                                                       keep_rows=keep_list,
                                                       keep_columns=keep_list,
                                                       verbose=verbose)
                pbar.update(1)


#def incorporate_il_contracts(scores,
#                             entities,
#                             hparams,
#                             row_caps,
#                             column_caps,
#                             verbose=False):
#    # Prevent truncation of any entities with defined profiles.
#    keep_list = list(entities.keys())
#
#    # Get the list of terms to skip when tokenizing.
#    skip_set = set(SKIP_LIST)
#
#    # Get the normalization map for entity names.
#    normalization = il_contracts.get_normalization_map(entities)
#
#    include_row_terms = row_caps['TERM'] > 0
#    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
#                               row_caps['OFFICE_DEPARTMENT']) > 0)
#    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
#                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)
#
#    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
#    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])
#
#    json_filenames = sorted(glob.glob(hparams['file_regexp']))
#    num_filenames = len(json_filenames)
#
#    # Get the total number of filings.
#    num_filings = 0
#    for json_filename in json_filenames:
#        filings = json.load(open(json_filename))
#        num_filings += len(filings)
#
#    num_incorporated = 0
#    with tqdm(total=num_filings) as pbar:
#        for i in range(num_filenames):
#            json_filename = json_filenames[i]
#            if verbose:
#                print('  Processing IL contract file {} of {}: {}'.format(
#                    i, num_filenames, json_filename))
#            filings = json.load(open(json_filename))
#            for filing in filings:
#                header = {
#                    'ENTITY': ['government of israel'],
#                    'PRINCIPAL_NAICS_CODE': [],
#                    'PRODUCT_OR_SERVICE_CODE': [],
#                    'OFFICE': [],
#                    'OFFICE_AGENCY': [],
#                    'OFFICE_DEPARTMENT': []
#                }
#
#                text = ''
#                if 'vendor_name' in filing and filing['vendor_name']:
#                    il_contracts.normalize(filing['vendor_name'],
#                                           normalization)
#                if 'buyer_detailed' in filing and filing['buyer_detailed']:
#                    header['OFFICE_AGENCY'].append(
#                        filing['buyer_detailed'].lower())
#
#                if 'procedure_name' in filing and filing['procedure_name_en']:
#                    join_text(text, filing['procedure_name_en'].lower())
#                if 'topics' in filing and filing['topics_en']:
#                    join_text(text, filing['topics_en'].lower())
#
#                normalized_text = normalize_text(text.lower())
#                tokens = tokenize_text(normalized_text, skip_set)
#
#                util.scores.update_scores(
#                    tokens,
#                    header,
#                    scores,
#                    interaction_length=hparams['interaction_length'],
#                    include_row_terms=include_row_terms,
#                    include_row_funders=include_row_funders,
#                    include_row_product_types=include_row_product_types,
#                    header_update=hparams['header_update'],
#                    multiplier=hparams['weight'])
#
#                num_incorporated += 1
#                if num_incorporated % hparams['truncation_rate'] == 0:
#                    if verbose:
#                        print('Truncating scores.')
#                    kept = util.scores.truncate_scores(scores,
#                                                       row_trunc_caps,
#                                                       column_trunc_caps,
#                                                       keep_rows=keep_list,
#                                                       keep_columns=keep_list,
#                                                       verbose=verbose)
#                pbar.update(1)
#


def incorporate_ca_lobbying_communications(scores,
                                           entities,
                                           hparams,
                                           row_caps,
                                           column_caps,
                                           verbose=False):
    # Prevent truncation of any entities with defined profiles.
    keep_list = list(entities.keys())

    # Get the list of terms to skip when tokenizing.
    skip_set = set(SKIP_LIST)

    # Get the normalization map for entity names.
    normalization = ca_lobby.get_normalization_map(entities)

    include_row_terms = row_caps['TERM'] > 0
    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
                               row_caps['OFFICE_DEPARTMENT']) > 0)
    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)

    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])

    json_filenames = sorted(glob.glob(hparams['file_regexp']))
    num_filenames = len(json_filenames)

    # Get the total number of filings.
    num_filings = 0
    for json_filename in json_filenames:
        filings = json.load(open(json_filename))
        num_filings += len(filings)

    num_incorporated = 0
    with tqdm(total=num_filings) as pbar:
        for i in range(num_filenames):
            json_filename = json_filenames[i]
            if verbose:
                print(
                    '  Processing CA Lobbying Comm. file {} of {}: {}'.format(
                        i, num_filenames, json_filename))
            filings = json.load(open(json_filename))
            for filing in filings:
                header = {
                    'ENTITY': ['government of canada'],
                    'PRINCIPAL_NAICS_CODE': [],
                    'PRODUCT_OR_SERVICE_CODE': [],
                    'OFFICE': [],
                    'OFFICE_AGENCY': [],
                    'OFFICE_DEPARTMENT': []
                }

                for entity in filing['entities']:
                    header['ENTITY'].append(
                        ca_lobby.normalize(entity, normalization))

                if 'dpoh' in filing and 'institution' in filing['dpoh']:
                    header['OFFICE_AGENCY'].append(
                        filing['dpoh']['institution'].lower())

                text = ''
                if 'activity' in filing:
                    if 'category' in filing['activity']:
                        join_text(text, filing['activity']['category'].lower())
                    if 'text' in filing['activity']:
                        join_text(text, filing['activity']['text'].lower())

                normalized_text = normalize_text(text.lower())
                tokens = tokenize_text(normalized_text, skip_set)

                util.scores.update_scores(
                    tokens,
                    header,
                    scores,
                    interaction_length=hparams['interaction_length'],
                    include_row_terms=include_row_terms,
                    include_row_funders=include_row_funders,
                    include_row_product_types=include_row_product_types,
                    header_update=hparams['header_update'],
                    multiplier=hparams['weight'])

                num_incorporated += 1
                if num_incorporated % hparams['truncation_rate'] == 0:
                    if verbose:
                        print('Truncating scores.')
                    kept = util.scores.truncate_scores(scores,
                                                       row_trunc_caps,
                                                       column_trunc_caps,
                                                       keep_rows=keep_list,
                                                       keep_columns=keep_list,
                                                       verbose=verbose)
                pbar.update(1)


def incorporate_ca_lobbying_registrations(scores,
                                          entities,
                                          hparams,
                                          row_caps,
                                          column_caps,
                                          verbose=False):
    # Prevent truncation of any entities with defined profiles.
    keep_list = list(entities.keys())

    # Get the list of terms to skip when tokenizing.
    skip_set = set(SKIP_LIST)

    # Get the normalization map for entity names.
    normalization = ca_lobby.get_normalization_map(entities)

    include_row_terms = row_caps['TERM'] > 0
    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
                               row_caps['OFFICE_DEPARTMENT']) > 0)
    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)

    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])

    json_filenames = sorted(glob.glob(hparams['file_regexp']))
    num_filenames = len(json_filenames)

    # Get the total number of filings.
    num_filings = 0
    for json_filename in json_filenames:
        filings = json.load(open(json_filename))
        num_filings += len(filings)

    num_incorporated = 0
    with tqdm(total=num_filings) as pbar:
        for i in range(num_filenames):
            json_filename = json_filenames[i]
            if verbose:
                print('  Processing CA Lobbying Reg. file {} of {}: {}'.format(
                    i, num_filenames, json_filename))
            filings = json.load(open(json_filename))
            for filing in filings:
                header = {
                    'ENTITY': ['government of canada'],
                    'PRINCIPAL_NAICS_CODE': [],
                    'PRODUCT_OR_SERVICE_CODE': [],
                    'OFFICE': [],
                    'OFFICE_AGENCY': [],
                    'OFFICE_DEPARTMENT': []
                }

                for entity in filing['entities']:
                    header['ENTITY'].append(
                        ca_lobby.normalize(entity, normalization))

                for institution in filing['comm_institutions']:
                    header['OFFICE_AGENCY'].append(institution.lower())

                text = ''
                if 'activity' in filing:
                    if 'description' in filing['activity']:
                        join_text(text,
                                  filing['activity']['description'].lower())
                    if 'topics' in filing['activity']:
                        join_text(text, filing['activity']['topics'].lower())

                normalized_text = normalize_text(text.lower())
                tokens = tokenize_text(normalized_text, skip_set)

                util.scores.update_scores(
                    tokens,
                    header,
                    scores,
                    interaction_length=hparams['interaction_length'],
                    include_row_terms=include_row_terms,
                    include_row_funders=include_row_funders,
                    include_row_product_types=include_row_product_types,
                    header_update=hparams['header_update'],
                    multiplier=hparams['weight'])

                num_incorporated += 1
                if num_incorporated % hparams['truncation_rate'] == 0:
                    if verbose:
                        print('Truncating scores.')
                    kept = util.scores.truncate_scores(scores,
                                                       row_trunc_caps,
                                                       column_trunc_caps,
                                                       keep_rows=keep_list,
                                                       keep_columns=keep_list,
                                                       verbose=verbose)
                pbar.update(1)


def incorporate_ca_proactive_disclosures(scores,
                                         entities,
                                         hparams,
                                         row_caps,
                                         column_caps,
                                         verbose=False):
    # Prevent truncation of any entities with defined profiles.
    keep_list = list(entities.keys())

    # Get the list of terms to skip when tokenizing.
    skip_set = set(SKIP_LIST)

    # Get the normalization map for entity names.
    normalization = ca_contracts.get_proactive_disclosure_normalization_map(
        entities)
    agency_normalization = ca_contracts.get_proactive_disclosure_agency_normalization_map(
        entities)

    include_row_terms = row_caps['TERM'] > 0
    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
                               row_caps['OFFICE_DEPARTMENT']) > 0)
    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)

    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])

    json_filenames = sorted(glob.glob(hparams['file_regexp']))
    num_filenames = len(json_filenames)

    # Get the total number of filings.
    num_filings = 0
    for json_filename in json_filenames:
        filings = json.load(open(json_filename))
        num_filings += len(filings)

    num_incorporated = 0
    with tqdm(total=num_filings) as pbar:
        for i in range(num_filenames):
            json_filename = json_filenames[i]
            if verbose:
                print('  Processing CA Proactive Disclosure file {} of {}: {}'.
                      format(i, num_filenames, json_filename))
            filings = json.load(open(json_filename))
            for filing in filings:
                header = {
                    'ENTITY': ['government of canada'],
                    'PRINCIPAL_NAICS_CODE': [],
                    'PRODUCT_OR_SERVICE_CODE': [],
                    'OFFICE': [],
                    'OFFICE_AGENCY': [],
                    'OFFICE_DEPARTMENT': []
                }

                if filing['vendor_name'] is not None:
                    header['ENTITY'].append(
                        ca_contracts.normalize(filing['vendor_name'],
                                               normalization))
                if filing['owner_org'] is not None:
                    agency = ca_contracts.normalize(filing['owner_org'],
                                                    agency_normalization)
                    header['OFFICE_AGENCY'].append(agency)
                    header['ENTITY'].append(agency)

                text = ''
                if filing['description'] is not None:
                    join_text(text, filing['description'].lower())
                if filing['comments'] is not None:
                    join_text(text, filing['comments'].lower())

                normalized_text = normalize_text(text.lower())
                tokens = tokenize_text(normalized_text, skip_set)

                util.scores.update_scores(
                    tokens,
                    header,
                    scores,
                    interaction_length=hparams['interaction_length'],
                    include_row_terms=include_row_terms,
                    include_row_funders=include_row_funders,
                    include_row_product_types=include_row_product_types,
                    header_update=hparams['header_update'],
                    multiplier=hparams['weight'])

                num_incorporated += 1
                if num_incorporated % hparams['truncation_rate'] == 0:
                    if verbose:
                        print('Truncating scores.')
                    kept = util.scores.truncate_scores(scores,
                                                       row_trunc_caps,
                                                       column_trunc_caps,
                                                       keep_rows=keep_list,
                                                       keep_columns=keep_list,
                                                       verbose=verbose)
                pbar.update(1)


def incorporate_ca_pwsgc(scores,
                         entities,
                         hparams,
                         row_caps,
                         column_caps,
                         verbose=False):
    # Prevent truncation of any entities with defined profiles.
    keep_list = list(entities.keys())

    # Get the list of terms to skip when tokenizing.
    skip_set = set(SKIP_LIST)

    # Get the normalization map for entity names.
    normalization = ca_contracts.get_pwsgc_normalization_map(entities)
    agency_normalization = ca_contracts.get_pwsgc_agency_normalization_map(
        entities)

    include_row_terms = row_caps['TERM'] > 0
    include_row_funders = (max(row_caps['OFFICE'], row_caps['OFFICE_AGENCY'],
                               row_caps['OFFICE_DEPARTMENT']) > 0)
    include_row_product_types = (max(row_caps['PRINCIPAL_NAICS_CODE'],
                                     row_caps['PRODUCT_OR_SERVICE_CODE']) > 0)

    row_trunc_caps = scale_caps(row_caps, hparams['truncation_scale'])
    column_trunc_caps = scale_caps(column_caps, hparams['truncation_scale'])

    json_filenames = sorted(glob.glob(hparams['file_regexp']))
    num_filenames = len(json_filenames)

    # Get the total number of filings.
    num_filings = 0
    for json_filename in json_filenames:
        filings = json.load(open(json_filename))
        num_filings += len(filings)

    num_incorporated = 0
    with tqdm(total=num_filings) as pbar:
        for i in range(num_filenames):
            json_filename = json_filenames[i]
            if verbose:
                print('  Processing CA PWSGC file {} of {}: {}'.format(
                    i, num_filenames, json_filename))
            filings = json.load(open(json_filename))
            for filing in filings:
                header = {
                    'ENTITY': ['government of canada'],
                    'PRINCIPAL_NAICS_CODE': [],
                    'PRODUCT_OR_SERVICE_CODE': [],
                    'OFFICE': [],
                    'OFFICE_AGENCY': [],
                    'OFFICE_DEPARTMENT': []
                }

                if filing['supplier_legal_name'] is not None:
                    header['ENTITY'].append(
                        ca_contracts.normalize(filing['supplier_legal_name'],
                                               normalization))

                if filing['end_user_entity'] is not None:
                    agency = ca_contracts.normalize(filing['end_user_entity'],
                                                    agency_normalization)
                    header['OFFICE_AGENCY'].append(agency)
                    header['ENTITY'].append(agency)
                if filing['contracting_entity_office_name'] is not None:
                    header['OFFICE'].append(
                        filing['contracting_entity_office_name'].lower())

                text = ''
                if filing['gsin_description'] is not None:
                    join_text(text, filing['gsin_description'].lower())

                normalized_text = normalize_text(text.lower())
                tokens = tokenize_text(normalized_text, skip_set)

                util.scores.update_scores(
                    tokens,
                    header,
                    scores,
                    interaction_length=hparams['interaction_length'],
                    include_row_terms=include_row_terms,
                    include_row_funders=include_row_funders,
                    include_row_product_types=include_row_product_types,
                    header_update=hparams['header_update'],
                    multiplier=hparams['weight'])

                num_incorporated += 1
                if num_incorporated % hparams['truncation_rate'] == 0:
                    if verbose:
                        print('Truncating scores.')
                    kept = util.scores.truncate_scores(scores,
                                                       row_trunc_caps,
                                                       column_trunc_caps,
                                                       keep_rows=keep_list,
                                                       keep_columns=keep_list,
                                                       verbose=verbose)
                pbar.update(1)


def get_interactions_over_cache(
        hparams,
        row_caps=DEFAULT_ROW_CAPS,
        column_caps=DEFAULT_COLUMN_CAPS,
        pretruncation_score_file='pretruncation_scores.json',
        score_file='scores.json',
        logp1_file='logp1.json',
        pmi_file=None):
    '''Returns a dict of dicts of bigram interactions over the cache.

    Args:
      hparams: A nested dictionary of hyperparams for each data feed.
      row_caps: Max number of rows to keep for each item type.
      column_caps: Max number of columns to keep for each item type.
      pretruncation_score_file: Path to store pre-truncation scores in.
      score_file: Path to store scores in.
      logp1_file: Path to store log(x+1) transformation of scores in.
      pmi_file: Path to store PMI scores in. If empty, PMI is not computed.

    Returns:
      The dictionary of kept items, scores, PMI scores, and log(x+1) scores.
    '''
    # Load a map from vendors to their profile.
    entities = ENTITIES

    associations = get_vendor_associations(entities)

    scores = {}
    for source in associations:
        source_key = util.keys.key_from_label(source, 'ENTITY')
        scores[source_key] = {}
        for association in associations[source]:
            target = association['name']
            score = association['fromScore']
            target_key = util.keys.key_from_label(target, 'ENTITY')
            if target_key not in scores[source_key]:
                scores[source_key][target_key] = 0
            scores[source_key][target_key] += score

    print('Incorporating webtext')
    incorporate_webtext(scores,
                        entities,
                        hparams=hparams['webtext'],
                        row_caps=row_caps,
                        column_caps=column_caps)

    print('Incorporating Australian procurement')
    incorporate_au_contracts(scores,
                             entities,
                             hparams=hparams['au_contracts'],
                             row_caps=row_caps,
                             column_caps=column_caps)

    #print('Incorporating Israeli procurement')
    #incorporate_il_contracts(scores,
    #                         entities,
    #                         hparams=hparams['il_contracts'],
    #                         row_caps=row_caps,
    #                         column_caps=column_caps)

    print('Incorporating Canadian proactive disclosure procurement')
    incorporate_ca_proactive_disclosures(
        scores,
        entities,
        hparams=hparams['ca_proactive_disclosure'],
        row_caps=row_caps,
        column_caps=column_caps)

    print('Incorporating Canadian lobbying communications')
    incorporate_ca_lobbying_communications(
        scores,
        entities,
        hparams=hparams['ca_lobby_communication'],
        row_caps=row_caps,
        column_caps=column_caps)

    print('Incorporating Canadian lobbying registrations')
    incorporate_ca_lobbying_registrations(
        scores,
        entities,
        hparams=hparams['ca_lobby_registration'],
        row_caps=row_caps,
        column_caps=column_caps)

    print('Incorporating Canadian PWSGC procurement')
    incorporate_ca_pwsgc(scores,
                         entities,
                         hparams=hparams['ca_pwsgc'],
                         row_caps=row_caps,
                         column_caps=column_caps)

    print('Incorporating US Florida procurement')
    incorporate_us_fl_procurement(scores,
                                  entities,
                                  hparams['us_fl_procurement'],
                                  row_caps=row_caps,
                                  column_caps=column_caps)

    for country_code in COUNTRIES:
        print('Incorporating {} procurement'.format(country_code))
        file_regexp = '{}/{}-*.json'.format(
            hparams['eu_contracts']['file_dir'], country_code)
        incorporate_eu_contracts(country_code,
                                 file_regexp,
                                 scores,
                                 entities,
                                 hparams=hparams['eu_contracts'],
                                 row_caps=row_caps,
                                 column_caps=column_caps)

    print('Incorporating UK procurement')
    incorporate_uk_contracts(scores,
                             entities,
                             hparams=hparams['uk_contracts'],
                             row_caps=row_caps,
                             column_caps=column_caps)

    print('Incorporating US OPR filings')
    incorporate_us_central_lobbying_filings(scores,
                                            entities,
                                            hparams=hparams['us_opr_filings'],
                                            row_caps=row_caps,
                                            column_caps=column_caps)

    print('Incorporating US central procurement')
    incorporate_us_central_procurement(scores,
                                       entities,
                                       hparams=hparams['us_fpds'],
                                       row_caps=row_caps,
                                       column_caps=column_caps)

    kept = util.scores.truncate_scores(scores,
                                       row_caps,
                                       column_caps,
                                       keep_rows=list(entities.keys()),
                                       keep_columns=list(entities.keys()))
    with open(score_file, 'w') as outfile:
        json.dump(scores, outfile, indent=1)
    print('Wrote truncated scores to {}'.format(score_file))

    # Squash via x mapsto log(x + 1).
    logp1_scores = copy.deepcopy(scores)
    util.scores.transform_scores_via_logp1(logp1_scores)
    with open(logp1_file, 'w') as outfile:
        json.dump(logp1_scores, outfile, indent=1)
    print('Wrote logp1 scores to {}'.format(logp1_file))

    info = {'kept': kept, 'scores': scores, 'logp1_scores': logp1_scores}

    if pmi_file:
        # Convert to pointwise mutual information.
        pmi_scores = copy.deepcopy(scores)
        util.scores.transform_scores_to_pmi(pmi_scores)
        with open(pmi_file, 'w') as outfile:
            json.dump(pmi_scores, outfile, indent=1)
        print('Wrote PMI to {}'.format(pmi_file))
        info['pmi_scores'] = pmi_scores

    return info


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute('''
CREATE TABLE IF NOT EXISTS vendor(
  id SERIAL PRIMARY KEY,
  vendor VARCHAR(255),
  dba VARCHAR(255));

CREATE UNIQUE INDEX IF NOT EXISTS vendor_vendor_index ON vendor (vendor);

CREATE INDEX IF NOT EXISTS vendor_search_index ON vendor USING GIN (
  to_tsvector('simple', vendor || ' ' || dba)
);

CREATE TABLE IF NOT EXISTS vendor_neighbors(
  id SERIAL PRIMARY KEY,
  vendor VARCHAR(255),
  neighbors TEXT);

CREATE UNIQUE INDEX IF NOT EXISTS vendor_neighbors_vendor_index ON
  vendor_neighbors (vendor);
    ''')


def postgres_replace_neighbors(neighbors_csv=VENDOR_NEIGHBORS_CSV,
                               user=None,
                               password=None,
                               dbname=None,
                               port=None,
                               host=None,
                               vacuum=True,
                               neighbors_table='vendor_neighbors'):
    postgres_create(user, password, dbname, port, host)
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute("SET statement_timeout = '12h';")
    cursor.execute('TRUNCATE {}'.format(neighbors_table))
    with open(neighbors_csv, 'r') as infile:
        cursor.copy_expert(
            '''
COPY {} (vendor, neighbors) FROM STDIN WITH (
  FORMAT CSV, delimiter E'\t', quote '"', escape '\\', HEADER
)
'''.format(neighbors_table), infile)
    if vacuum:
        cursor.execute('VACUUM ANALYZE {}'.format(neighbors_table))


def postgres_replace_vendors(vendor_csv=VENDORS_CSV,
                             user=None,
                             password=None,
                             dbname=None,
                             port=None,
                             host=None,
                             vacuum=True,
                             vendor_table='vendor'):
    postgres_create(user, password, dbname, port, host)
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute("SET statement_timeout = '12h';")
    cursor.execute('TRUNCATE {}'.format(vendor_table))
    with open(vendor_csv, 'r') as infile:
        cursor.copy_expert(
            '''
COPY {} (vendor, dba) FROM STDIN WITH (
  FORMAT CSV, delimiter E'\t', quote '"', escape '\\', HEADER
)
'''.format(vendor_table), infile)
    if vacuum:
        cursor.execute('VACUUM ANALYZE {}'.format(vendor_table))


def postgres_replace(vendor_csv=VENDORS_CSV,
                     neighbors_csv=VENDOR_NEIGHBORS_CSV,
                     user=None,
                     password=None,
                     dbname=None,
                     port=None,
                     host=None,
                     vacuum=True,
                     vendor_table='vendor',
                     neighbors_table='vendor_neighbors'):
    postgres_replace_vendors(vendor_csv=vendor_csv,
                             user=user,
                             password=password,
                             dbname=dbname,
                             port=port,
                             host=host,
                             vacuum=vacuum,
                             vendor_table=vendor_table)
    postgres_replace_neighbors(neighbors_csv=neighbors_csv,
                               user=user,
                               password=password,
                               dbname=dbname,
                               port=port,
                               host=host,
                               vacuum=vacuum,
                               neighbors_table=neighbors_table)


def write_and_update_vendors(entities,
                             row_entity_interactions,
                             max_vendor_length=255,
                             needed_envs=NEEDED_ENVS):
    vendor_set = set()
    vendor_list = []
    dba_list = []

    for vendor_key in row_entity_interactions:
        vendor = util.keys.label_from_key(vendor_key)
        if len(vendor) > max_vendor_length:
            # This is an anomalous name that we can skip.
            continue
        if vendor in vendor_set:
            continue
        vendor_list.append(vendor)
        vendor_set.add(vendor)
        dba = get_alt(vendor, entities)
        dba_list.append(dba.lower())

    for vendor in entities:
        if vendor in vendor_set:
            continue
        vendor_list.append(vendor)
        vendor_set.add(vendor)
        dba = get_alt(vendor, entities)
        dba_list.append(dba.lower())

    write_vendors(vendor_list, dba_list)
    print('Finished neighbor generation.')

    have_envs = True
    for needed_env in needed_envs:
        if needed_env not in os.environ:
            have_envs = False
            print(
                'Cannot automatically update Postgres due to missing environment variable {}'
                .format(needed_env))
    if have_envs:
        postgres_replace()
