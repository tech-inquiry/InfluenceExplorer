#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Utilities for interacting with the Postgres database.
#
import csv
import json
import os
import psycopg2
import re
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from tqdm import tqdm

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '..')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

# A map from ISO 639-1 two-letter language codes to Postgres language config's
# (when they exist). See https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
LANGUAGE_CODE_TO_TS_CONFIG = {
    'da': 'danish',
    'de': 'german',
    'en': 'english',
    'es': 'spanish',
    'fr': 'french',
    'hu': 'hungarian',
    'it': 'italian',
    'nl': 'dutch',
    'no': 'norwegian',
    'pt': 'portuguese',
    'ro': 'romanian',
    'ru': 'russian',
    'sv': 'swedish',
    'tr': 'turkish'
}

# Allow 12 hours for Postgres statement execution.
STATEMENT_TIMEOUT = '12h'


def get_connection(dbname=None,
                   user=None,
                   password=None,
                   port=None,
                   host=None):
    ENVIRON_VARS = {
        'dbname': 'INF_EXP_POSTGRES_DATABASE',
        'user': 'INF_EXP_POSTGRES_USER',
        'password': 'INF_EXP_POSTGRES_PASSWORD',
        'port': 'INF_EXP_POSTGRES_PORT',
        'host': 'INF_EXP_POSTGRES_HOST'
    }

    read_vars = {
        'dbname': dbname,
        'user': user,
        'password': password,
        'port': port,
        'host': host
    }
    for name, env_name in ENVIRON_VARS.items():
        if read_vars[name] is None:
            if env_name in os.environ:
                read_vars[name] = os.environ[env_name]
            else:
                print(
                    'ERROR: Either specify the Postgres {} or set the environment variable {}'
                    .format(name, env_name))
                return None

    con = psycopg2.connect(dbname=read_vars['dbname'],
                           user=read_vars['user'],
                           password=read_vars['password'],
                           port=read_vars['port'],
                           host=read_vars['host'])
    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    return con


def sanitize_entry(entry):
    if type(entry) is not str:
        return entry

    SPACE_REPLACES = ['\\', '\u0000', '\u009d', '\u00a0']
    NEWLINE_REPLACES = ['\r\n', '\r', '\n']

    for pattern in SPACE_REPLACES:
        entry = entry.replace(pattern, ' ')
    for pattern in NEWLINE_REPLACES:
        entry = entry.replace(pattern, '; ')

    # Merge spaces.
    entry = re.sub(r'[ ]{2,}', ' ', entry)

    # Replace all remaining backslashes with two backslashes.
    entry = entry.replace('\\', '\\\\')

    # Avoid any pluses after backslashes.
    entry = re.sub(r'\\+;', '; ', entry)

    return entry


def export_json(data):
    '''Exports a dictionary as escaped JSON for Postgres.'''
    UNICODE_REPLACES = ['\\u0000', '\\u009d', '\\u00a0']
    NEWLINE_REPLACES = ['\\r\\n', '\\r', '\\n', '\r\n', '\r', '\n']

    string = json.dumps(data)
    for pattern in UNICODE_REPLACES:
        string = string.replace(pattern, ' ')
    for pattern in NEWLINE_REPLACES:
        string = string.replace(pattern, '; ')

    # Merge spaces.
    string = re.sub(r'[ ]{2,}', ' ', string)

    # Escape backslashes.
    string = string.replace('\\', '\\\\')

    # Replace all sequences of the form '\...;' with ';' since PostgreSQL
    # refuses to parse JSON otherwise.
    string = re.sub(r'\\+;', ';', string)

    return string


def preprocess_dataframe(df,
                         json_columns=None,
                         unique_columns=None,
                         run_regexes=True,
                         verbose=False):
    '''Converts a dataframe into a format suitable for export into Postgres.

    Args:
      df: The DataFrame to modify.
      json_columns: The list of dictionaries/lists to dump into a JSON string.
      unique_columns: An optional list of columns defining a UNIQUE constraint.
          Redundant columns (after the first occurrence) will be dropped.
      run_regexes: Whether to run a sequence of regular expression replacements.
      verbose: Whether to print if columns were dropped.

    Returns:
      The modified DataFrame.
    '''
    if json_columns is not None:
        if verbose:
            print('Preprocessing DF via serializing JSON columns.')
        for column in json_columns:
            df[column] = df[column].apply(lambda value: json.dumps(value))

    REGEX_REPLACES = {
        r'\\u0000': ' ',
        r'\\u009d': ' ',
        r'\\u00a0': ' ',
        r'\\\\u0000': ' ',
        r'\\\\u009d': ' ',
        r'\\\\u00a0': ' ',
        # Replace multiple spaces with a single space.
        r'[ ]{2,}': ' ',
        r'\\': r'\\\\',
        # Replace all sequences of the form '\...;' with ';' since PostgreSQL
        # refuses to parse JSON otherwise.
        # TODO(Jack Poulson): Apply apply this to JSON columns.
        r'\\+;': ';'
    }

    if run_regexes:
        if verbose:
            print('Preprocessing DF via regex sequence.')
            with tqdm(total=len(REGEX_REPLACES)) as pbar:
                for regex, replace in REGEX_REPLACES.items():
                    df.replace(re.compile(regex),
                               replace,
                               regex=True,
                               inplace=True)
                    pbar.update(1)
        else:
            for regex, replace in REGEX_REPLACES.items():
                df.replace(re.compile(regex),
                           replace,
                           regex=True,
                           inplace=True)

    if unique_columns:
        if verbose:
            print('Preprocessing DF via deduplication.')
        duplicated = df[unique_columns].duplicated(keep='first')
        num_dropped = sum(duplicated)
        if num_dropped:
            if verbose:
                print('Dropping {} redundant rows.'.format(num_dropped))
            df.drop(df[duplicated].index, inplace=True)
        else:
            if verbose:
                print('No rows were duplicated.')

    return df


def df_to_csv(df, filename, columns=None):
    df.to_csv(filename,
              index=False,
              escapechar='\\',
              doublequote=False,
              sep='\t',
              quoting=csv.QUOTE_ALL,
              columns=columns)


def get_num_data_lines(csv_file):
    with open(csv_file) as infile:
        csv_reader = csv.reader(infile,
                                delimiter='\t',
                                quotechar='"',
                                doublequote=False,
                                escapechar='\\',
                                quoting=csv.QUOTE_ALL)
        for i, components in enumerate(csv_reader):
            pass
    return i


def create_table_sql(name, types, serial_id=False, constraints={}):
    full_types = {}
    if serial_id:
        if 'id' in types:
            print("WARNING: Overwriting 'id' field of table with SERIAL.")
        full_types['id'] = 'SERIAL'
    for k, v in types.items():
        full_types[k] = v

    lines = ['{} {}'.format(k, v) for k, v in full_types.items()]
    for constraint_name, constraint in constraints.items():
        lines.append('CONSTRAINT {}_{} {}'.format(name, constraint_name,
                                                  constraint))

    body = ',\n'.join(lines)
    sql = 'CREATE TABLE IF NOT EXISTS {} (\n{}\n)'.format(name, body)
    return sql


def copy_from_sql(table, columns, force_null_columns=None):
    columns_str = ', '.join(columns)
    force_null_str = ('FORCE_NULL({}),'.format(', '.join(force_null_columns))
                      if force_null_columns else '')
    sql = '''
COPY {} (
  {}
) FROM STDIN WITH (
  FORMAT CSV, {} delimiter E'\t', quote '"', escape '\\', null '', HEADER
)
'''.format(table, columns_str, force_null_str)
    return sql


def upsert_sql(dest_table, src_table, columns, unique_columns):
    columns_str = ', '.join(columns)
    unique_columns_str = ', '.join(unique_columns)
    unique_str = ', '.join(unique_columns)
    if len(unique_columns) < len(columns):
        update_str = ('UPDATE SET ' +
                      ',\n'.join('{} = excluded.{}'.format(v, v)
                                 for v in columns if v not in unique_columns))
    else:
        update_str = 'NOTHING'
    sql = '''
INSERT INTO {} (
  {}
) SELECT * FROM {} ON CONFLICT ({}) DO {}
'''.format(dest_table, columns_str, src_table, unique_str, update_str)
    return sql


def get_cursor(user=None, password=None, dbname=None, port=None, host=None):
    '''A simplified interface to executing a sequence of Postgres commands.'''
    conn = get_connection(user=user,
                          password=password,
                          dbname=dbname,
                          port=port,
                          host=host)
    cursor = conn.cursor()
    cursor.execute("SET statement_timeout = '{}';".format(STATEMENT_TIMEOUT))
    return cursor


def execute(command,
            user=None,
            password=None,
            dbname=None,
            port=None,
            host=None):
    '''A simplified interface to executing a Postgres command.'''
    conn = get_connection(user=user,
                          password=password,
                          dbname=dbname,
                          port=port,
                          host=host)
    cursor = conn.cursor()
    cursor.execute("SET statement_timeout = '{}';".format(STATEMENT_TIMEOUT))
    cursor.execute(command)


def update(table,
           table_types,
           unique_columns,
           force_null_columns,
           table_tmp,
           export_csv,
           user=None,
           password=None,
           dbname=None,
           port=None,
           host=None,
           vacuum=True,
           num_chunks=1,
           start_chunk=None,
           end_chunk=None,
           tmp_dir=EXPORT_DIR,
           verbose=True):
    '''Updates a Postgres table with the data in a CSV.'''
    def upload_csv(cursor, csv_file):
        cursor.execute(
            "SET statement_timeout = '{}';".format(STATEMENT_TIMEOUT))
        if verbose:
            print('  Dropping table {} if it exists.'.format(table_tmp))
        cursor.execute('DROP TABLE IF EXISTS {}'.format(table_tmp))
        cursor.execute(create_table_sql(table_tmp, table_types))
        if verbose:
            print('  Uploading into table {}.'.format(table_tmp))
        with open(csv_file, 'r') as infile:
            cursor.copy_expert(
                copy_from_sql(table_tmp, table_types.keys(),
                              force_null_columns), infile)
        cursor.execute(
            upsert_sql(table, table_tmp, table_types.keys(), unique_columns))
        if verbose:
            print('  Dropping table {}.'.format(table_tmp))
        cursor.execute('DROP TABLE {}'.format(table_tmp))
        cursor.execute('INSERT INTO table_updates (table_name, last_update) VALUES ({}, CURRENT_DATE) ON CONFLICT (table_name) DO UPDATE SET table_name = excluded.table_name, last_update = excluded.last_update;'.format(table))

    conn = get_connection(user=user,
                          password=password,
                          dbname=dbname,
                          port=port,
                          host=host)
    cursor = conn.cursor()
    if num_chunks == 1:
        upload_csv(cursor, export_csv)
    elif num_chunks > 1:
        if not export_csv.endswith('.csv'):
            print('ERROR: Export CSV {} did not end with .csv'.format(
                export_csv))
            return
        export_base = export_csv[:-4]

        num_data_lines = get_num_data_lines(export_csv)

        if verbose:
            print('Chunking {} line file into {} pieces'.format(
                num_data_lines, num_chunks))
        num_data_lines_per_chunk = -(-num_data_lines // num_chunks)
        if verbose:
            print('Splitting into at most {} lines per chunk'.format(
                num_data_lines_per_chunk))

        # The location we will store the temporary chunk CSV at
        tmp_file = '{}/postgres_chunk.csv'.format(tmp_dir)

        start_data_line = 0
        start_chunk = start_chunk if start_chunk != None else 0
        end_chunk = end_chunk if end_chunk != None else num_chunks
        start_data_line += start_chunk * num_data_lines_per_chunk
        for chunk in range(start_chunk, end_chunk):
            end_data_line = min(num_data_lines,
                                start_data_line + num_data_lines_per_chunk)
            if verbose:
                print('  Creating chunk {} over data lines [{}, {}]'.format(
                    chunk, start_data_line, end_data_line))
            with open(export_csv, 'r') as infile, open(tmp_file,
                                                       'w') as outfile:
                csv_reader = csv.reader(infile,
                                        delimiter='\t',
                                        quotechar='"',
                                        doublequote=False,
                                        escapechar='\\',
                                        quoting=csv.QUOTE_ALL)
                csv_writer = csv.writer(outfile,
                                        delimiter='\t',
                                        quotechar='"',
                                        doublequote=False,
                                        escapechar='\\',
                                        quoting=csv.QUOTE_ALL)
                for line_number, components in enumerate(csv_reader):
                    data_line_number = line_number - 1
                    if data_line_number == -1:
                        csv_writer.writerow(components)
                    elif data_line_number < start_data_line:
                        continue
                    elif data_line_number >= start_data_line and data_line_number < end_data_line:
                        # We have to escape the backslashes.
                        for j in range(len(components)):
                            if type(components[j]) == str:
                                components[j] = components[j].replace(
                                    '\\', '\\\\')
                        csv_writer.writerow(components)
                    else:
                        break
            if verbose:
                print('  Handling chunk {} over data lines [{}, {}]'.format(
                    chunk, start_data_line, end_data_line))
            upload_csv(cursor, tmp_file)
            start_data_line = end_data_line

    if vacuum:
        print('Vacuuming {}'.format(table))
        cursor.execute('VACUUM ANALYZE {}'.format(table))
