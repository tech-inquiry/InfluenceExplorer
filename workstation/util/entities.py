#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import json
import os
import re
import sys

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '..')
SHARED_DIR = os.path.join(WORKSTATION_DIR, '..')
SHARED_DATA_DIR = os.path.join(SHARED_DIR, 'data')
sys.path.append(WORKSTATION_DIR)

import util.dict_path


def get_entities():
    '''Returns a map from entity names to their JSON profile.'''
    filename = '{}/entities.json'.format(SHARED_DATA_DIR)
    return json.load(open(filename))


def get_children(entities=None):
    '''Inverts entity parents map to get the entity's children.'''
    if entities is None:
        entities = get_entities()

    children = {}
    for entity in entities:
        if 'parents' in entities[entity]:
            parents = entities[entity]['parents']
            for parent in parents:
                if not parent in children:
                    children[parent] = []
                children[parent].append(entity)

    return children


def append_ancestors(entity, entities, ancestor_list):
    '''Recursive helper for forming ancestor list of an entity.

    Args:
      entity: The normalized name of the entity to query ancestors of.
      entities: The map from entity names to their profiles.
    '''
    if entity in entities:
        if 'parents' in entities[entity]:
            parents = entities[entity]['parents']
            for parent in parents:
                ancestor_list.append(parent)
                append_ancestors(parent, entities, ancestor_list)


def get_ancestors(entity, entities):
    '''Returns the list of ancestor entities of a given entity name.

    Args:
      entity: The normalized name of the entity to query ancestors of.
      entities: The map from entity names to their profiles.

    Returns:
      The list of ancestor entity names.
    '''
    ancestors = []
    append_ancestors(entity, entities, ancestors)
    return ancestors


def append_ancestors_and_weight_exponents(entity, entities, children,
                                          ancestor_list, exponent, damp):
    '''Recursive helper for forming ancestor and weight list of an entity.

    Args:
      entity: The normalized name of the entity to query ancestors of.
      entities: The map from entity names to their profiles.
    '''
    if entity in entities:
        if 'parents' in entities[entity]:
            profile = entities[entity]
            manual_multiplier = profile[
                'liftExponent'] if 'liftExponent' in profile else None

            parents = profile['parents']
            for parent in parents:
                num_children = len(children[parent])
                multiplier = manual_multiplier if manual_multiplier != None else (
                    num_children * len(parents) + 1)**damp
                new_exponent = exponent * multiplier
                ancestor_list.append({
                    'name': parent,
                    'exponent': new_exponent
                })
                append_ancestors_and_weight_exponents(parent, entities,
                                                      children, ancestor_list,
                                                      new_exponent, damp)


def get_ancestors_and_weight_exponents(entity, entities, children, damp=0.25):
    '''Returns the list of ancestor entities and reweighting exponents.

    For each movement up from n children to a single parent, we reweight an
    association weight w as w^{1 / n^damp}. In the case of an entity with k
    parents, we reweight as w^{1 / (n * k)^damp}.

    Args:
      entity: The normalized name of the entity to query ancestors of.
      entities: The map from entity names to their profiles.

    Returns:
      The list of ancestor entity names and their associated weights.
    '''
    ancestors = []
    exponent = 1.
    append_ancestors_and_weight_exponents(entity, entities, children,
                                          ancestors, exponent, damp)
    return ancestors


def ultimate_parent(entity, entities):
    '''Returns the root ancestor, not including multiple parentages.'''
    if entity not in entities:
        return entity
    profile = entities[entity]
    if 'parents' not in profile or len(profile['parents']) != 1:
        return entity
    return ultimate_parent(profile['parents'][0], entities)


def canonicalize(entity):
    '''Canonicalizes an entity string.'''
    if not entity:
        return entity
    # Compress sequences of whitespace into a single space.
    entity = str(entity).lower().strip()
    entity = re.sub(r'\s\s+', ' ', entity)

    # Drop all trailing backslashes.
    while len(entity) and entity[-1] == '\\':
        entity = entity[:-1]

    return entity


def normalize(entity, normalization):
    '''Normalizes an entity string.'''
    entity = canonicalize(entity)
    if entity in normalization:
        return normalization[entity]
    else:
        return entity


def normalization_map_helper(normalization_path,
                             entities=None,
                             object_normalization=False,
                             primary_category='name',
                             primary_key='names',
                             block_key='blockName',
                             additional_category=None,
                             additional_key=None):
    '''Inverts the entity alternate spellings into a normalization map.

    Args:
      normalization_path: A tuple of keys specifying the path through the JSON
        entity profile's 'feeds' object. For example,
        ['uk', 'procurement'].
      entities: An optional JSON map from entity names to entity profiles.
      object_normalization: If dictionaries of the form
        `{primary_category: 'some name', 'other_key': 'other value'}` are to be used
        instead of simple strings.
      primary_category: The key to use for the entity name in dictionary normalizers.
      primary_key: The field to read the list of alternates from.
      block_key: The field to read the boolean of whether to block the original name from the list of possibilities.
      additional_category: An optional additional field to fill.
      additional_key: The optional top-level key to find the fields.

    Returns:
      The normalization map from entity alternate spellings to the normalized
      name.
    '''
    if entities is None:
        entities = util.entities.get_entities()

    normalization = {}
    for entity in entities:
        profile = entities[entity]
        info = util.dict_path.get_from_path(profile,
                                            ['feeds'] + normalization_path)
        if not info:
            continue
        if block_key and block_key in info:
            blocked_name = '{} [blocked]'.format(entity)
            if object_normalization:
                normalization[json.dumps({primary_category: entity})] = blocked_name
            else:
                normalization[entity] = blocked_name
        if primary_key in info:
            for alternate in info[primary_key]:
                if isinstance(alternate, str):
                    if object_normalization:
                        normalization[json.dumps({primary_category:
                                                  alternate})] = entity
                    else:
                        normalization[alternate] = entity
                else:
                    if object_normalization:
                        normalization[json.dumps(alternate)] = entity
        if additional_key in info:
            for alternate in info[additional_key]:
                normalization[json.dumps({additional_category:
                                          alternate})] = entity

    return normalization
