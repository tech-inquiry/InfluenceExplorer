#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This script retrieves and exports the recent data windows for the subset of
# data sources which can be handled automatically. All exported CSVS -- meant
# for import into Postgres -- are placed in the exports/ folder.
#
# If the appropriate environment variables for the Postgres connection are set,
# then the Postgres tables are updated through a psycopg2 interface.
#
import argparse
import glob
import os
import sys
import tarfile

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '..')
sys.path.append(WORKSTATION_DIR)

import gov.au.procurement as au_tenders
import gov.ca.lobbying as ca_lobbying
import gov.ca.procurement as ca_tenders
import gov.nz.procurement as nz_awards
import gov.uk.procurement as uk_contracts
import gov.us.ca.lobbying as calaccess
import gov.us.central.fara as fara
import gov.us.central.labor_relations as nlrb
import gov.us.central.lobbying.opr as us_opr
import gov.us.central.procurement as fpds
import gov.us.central.securities as sec
import gov.us.az.procurement as usazproc
import gov.us.ca.procurement as uscaproc
import gov.us.fl.procurement as usflproc
import gov.us.ny.procurement as nyogs
import gov.us.tx.dps_procurement as txdps
import util.args

parser = argparse.ArgumentParser(description='Daily data scraper')
parser.add_argument('--disable_au_procurement',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable Australian tender retrieval')
parser.add_argument('--disable_ca_lobbying',
                    type=util.args.str2bool,
                    default=True,
                    help='Disable Canadian lobbying retrieval')
parser.add_argument('--disable_ca_procurement',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable Canadian tender retrieval')
parser.add_argument('--disable_nz_procurement',
                    type=util.args.str2bool,
                    default=True,
                    help='Disable New Zealand notices retrieval')
parser.add_argument('--disable_uk_procurement',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable UK contracts retrieval')
parser.add_argument('--disable_us_procurement',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable US FPDS retrieval')
parser.add_argument('--disable_us_dod_announcements',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable US DoD announcement retrieval')
parser.add_argument('--disable_us_foreign_agents',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable US FARA retrieval')
parser.add_argument('--disable_us_labor_relations',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable US NLRB retrieval')
parser.add_argument('--disable_us_opr',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable US lobbying OPR retrieval')
parser.add_argument('--disable_us_ca_lobbying',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable US California lobbying retrieval')
parser.add_argument('--disable_us_securities',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable US SEC filing retrieval')
parser.add_argument('--disable_us_az_procurement',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable US Arizona contract retrieval')
parser.add_argument('--disable_us_ca_procurement',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable US California contract retrieval')
parser.add_argument('--disable_us_fl_procurement',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable US Florida contract retrieval')
parser.add_argument('--disable_us_ny_procurement',
                    type=util.args.str2bool,
                    default=False,
                    help='Disable US NY OGS notice retrieval')
parser.add_argument('--disable_us_tx_dps_procurement',
                    type=util.args.str2bool,
                    default=True,
                    help='Disable US TX DPS notice retrieval')

args = parser.parse_args()

NEEDED_ENVS = [
    'INF_EXP_POSTGRES_USER', 'INF_EXP_POSTGRES_PASSWORD',
    'INF_EXP_POSTGRES_DATABASE', 'INF_EXP_POSTGRES_PORT',
    'INF_EXP_POSTGRES_HOST'
]
have_envs = True
for needed_env in NEEDED_ENVS:
    if needed_env not in os.environ:
        have_envs = False
        print(
            'Cannot automatically update Postgres due to missing environment variable {}'
            .format(needed_env))


def scrape(label, retrieve_func, update_func, state):
    try:
        retrieve_func()
        state['retrieve']['successes'].append(label)
        if have_envs:
            try:
                update_func()
                state['update']['successes'].append(label)
            except Exception as update_except:
                state['update']['failures'].append(label)
                state['update']['exceptions'].append(update_except)
                print('ERROR: Could not update {}'.format(label))
                print(update_except)
    except Exception as e:
        state['retrieve']['failures'].append(label)
        state['retrieve']['exceptions'].append(e)
        print('ERROR: Could not retrieve {}'.format(label))
        print(e)


datasets = [{
    'label': 'US procurement',
    'retrieve': fpds.retrieve,
    'update': fpds.postgres.update,
    'disable_option': args.disable_us_procurement
}, {
    'label': 'US DoD announcements',
    'retrieve': fpds.dod_announce.retrieve,
    'update': fpds.dod_announce.postgres_update,
    'disable_option': args.disable_us_dod_announcements
}, {
    'label': 'US lobbying OPR',
    'retrieve': us_opr.retrieve,
    'update': us_opr.postgres_update,
    'disable_option': args.disable_us_opr
}, {
    'label': 'UK procurement',
    'retrieve': uk_contracts.retrieve,
    'update': uk_contracts.postgres_update,
    'disable_option': args.disable_uk_procurement
}, {
    'label': 'Australian procurement',
    'retrieve': au_tenders.retrieve,
    'update': au_tenders.postgres_update,
    'disable_option': args.disable_au_procurement
}, {
    'label': 'Canadian lobbying',
    'retrieve': ca_lobbying.retrieve,
    'update': ca_lobbying.postgres_update,
    'disable_option': args.disable_ca_lobbying
}, {
    'label': 'Canadian procurement',
    'retrieve': ca_tenders.retrieve,
    'update': ca_tenders.postgres_update,
    'disable_option': args.disable_ca_procurement
}, {
    'label': 'New Zealand procurement',
    'retrieve': nz_awards.retrieve,
    'update': nz_awards.postgres_update,
    'disable_option': args.disable_nz_procurement
}, {
    'label': 'US securities',
    'retrieve': sec.retrieve,
    'update': sec.postgres_update,
    'disable_option': args.disable_us_securities
}, {
    'label': 'Arizona procurement',
    'retrieve': usazproc.retrieve,
    'update': usazproc.postgres_update,
    'disable_option': args.disable_us_az_procurement
}, {
    'label': 'California procurement',
    'retrieve': uscaproc.retrieve,
    'update': uscaproc.postgres_update,
    'disable_option': args.disable_us_ca_procurement
}, {
    'label': 'Florida procurement',
    'retrieve': usflproc.retrieve,
    'update': usflproc.postgres_update,
    'disable_option': args.disable_us_fl_procurement
}, {
    'label': 'New York procurement',
    'retrieve': nyogs.retrieve,
    'update': nyogs.postgres_update,
    'disable_option': args.disable_us_ny_procurement
}, {
    'label': 'Texas DPS procurement',
    'retrieve': txdps.retrieve,
    'update': txdps.postgres_update,
    'disable_option': args.disable_us_tx_dps_procurement
}, {
    'label': 'US labor relations',
    'retrieve': nlrb.retrieve,
    'update': nlrb.postgres_update,
    'disable_option': args.disable_us_labor_relations
}, {
    'label': 'US foreign agents',
    'retrieve': fara.retrieve,
    'update': fara.postgres_update,
    'disable_option': args.disable_us_foreign_agents
}, {
    'label': 'California lobbying',
    'retrieve': calaccess.retrieve,
    'update': calaccess.postgres_update,
    'disable_option': args.disable_us_ca_lobbying
}]

state = {
    'retrieve': {
        'successes': [],
        'failures': [],
        'exceptions': []
    },
    'update': {
        'successes': [],
        'failures': [],
        'exceptions': []
    }
}

for dataset in datasets:
    if dataset['disable_option']:
        print(
            'Skipping {} retrieval because it was explicitly disabled.'.format(
                dataset['label']))
        continue
    scrape(dataset['label'], dataset['retrieve'], dataset['update'], state)

if len(state['retrieve']['successes']):
    print('Retrieve successes:')
    for label in state['retrieve']['successes']:
        print('  {}'.format(label))

if len(state['update']['successes']):
    print('Update successes:')
    for label in state['update']['successes']:
        print('  {}'.format(label))

if len(state['retrieve']['failures']):
    print('Retrieve failures:')
    for i in range(len(state['retrieve']['failures'])):
        label = state['retrieve']['failures'][i]
        message = state['retrieve']['exceptions'][i]
        print('  {}: {}'.format(label, message))

if len(state['update']['failures']):
    print('Update failures:')
    for i in range(len(state['update']['failures'])):
        label = state['update']['failures'][i]
        message = state['update']['exceptions'][i]
        print('  {}: {}'.format(label, message))
