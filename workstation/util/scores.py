#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import util.keys

from math import log, sqrt
from numpy import float32, zeros
from scipy.sparse import csr_matrix
from util.awls import KeyIndexBijection
from util.keys import key_from_label

DEFAULT_INTERACTION_LENGTH = 20


def description_interaction_kernel(num_terms_apart):
    '''Returns an interaction contribution of terms a given distance apart.

    Args:
      num_terms_apart:

    Returns:
      The floating-point weight of the interaction.
    '''
    return 1. / num_terms_apart if num_terms_apart > 1 else 1.


def initialize_score_update(token_list,
                            header,
                            scores,
                            interaction_length=DEFAULT_INTERACTION_LENGTH,
                            include_row_terms=False,
                            include_row_funders=False,
                            include_row_product_types=False,
                            use_bigrams=True):
    '''Initializes for an update of scores given an award.

    Args:
      token_list: The list of description tokens to update using.
      header: The header information for the contract, such as the vendor,
          contracting_office, contracting_office_agency,
          contracting_office_dept, requesting_office, requesting_office_agency,
          and requesting_office_dept.
      scores: The dictionary of dictionary of weights to update.
      interaction_length: The maximum distance between interacting tokens.
      include_row_terms: Whether terms should be included as row items.
      include_row_funders: Whether funders should be included as row items.
      include_row_product_types: Whether product types are included in rows.
      use_bigrams: Whether bigrams, rather than just unigrams, should be used.

    Returns:
      A tuple of the:
        * Unigrams,
        * Bigrams (possibly empty),
        * Row header keys, and
        * Column header keys.
    '''
    def initialize_score_source(scores, source):
        '''Sets source row to an empty dictionary if uninitialized.

        Args:
          scores: Dictionary of dictionaries of interaction scores.
          source: The key for the row of the interaction matrix to initialize.
        '''
        if not source in scores:
            scores[source] = {}

    def initialize_score_pair(scores, source, target):
        '''Sets (source, target) interaction score to zero if uninitialized.

        Args:
          scores: Dictionary of dictionaries of interaction scores.
          source: The key for the row of the interaction matrix.
          target: The key for the column of the interaction matrix.
        '''
        if not target in scores[source]:
            scores[source][target] = 0

    num_tokens = len(token_list)

    # Form the list of vendor header keys.
    entity_header_keys = []
    if 'ENTITY' in header:
        header['ENTITY'] = list(set(header['ENTITY']))
        entity_header_keys = [
            key_from_label(entity, 'ENTITY') for entity in header['ENTITY']
        ]

    # Form the list of funder header keys.
    funder_header_keys = []
    if 'OFFICE' in header:
        funder_header_keys = [
            key_from_label(office, 'OFFICE') for office in header['OFFICE']
        ]
    if 'OFFICE_AGENCY' in header:
        funder_header_keys += [
            key_from_label(agency, 'OFFICE_AGENCY')
            for agency in header['OFFICE_AGENCY']
        ]
    if 'OFFICE_DEPARTMENT' in header:
        funder_header_keys += [
            key_from_label(department, 'OFFICE_DEPARTMENT')
            for department in header['OFFICE_DEPARTMENT']
        ]

    # Form the list of product type header keys.
    product_type_header_keys = []
    if 'PRINCIPAL_NAICS_CODE' in header:
        product_type_header_keys = [
            key_from_label(naics, 'PRINCIPAL_NAICS_CODE')
            for naics in header['PRINCIPAL_NAICS_CODE']
        ]
    if 'PRODUCT_OR_SERVICE_CODE' in header:
        product_type_header_keys += [
            key_from_label(naics, 'PRODUCT_OR_SERVICE_CODE')
            for naics in header['PRODUCT_OR_SERVICE_CODE']
        ]

    row_header_keys = entity_header_keys
    if include_row_funders:
        row_header_keys += funder_header_keys
    if include_row_product_types:
        row_header_keys += product_type_header_keys
    column_header_keys = (entity_header_keys + funder_header_keys +
                          product_type_header_keys)

    # Build the unigrams
    unigrams = [key_from_label(token, 'TERM') for token in token_list]

    # Build the bigrams (if they are needed).
    bigrams = []
    if use_bigrams:
        bigrams = [
            key_from_label('{} {}'.format(token_list[i], token_list[i + 1]),
                           'TERM') for i in range(num_tokens - 1)
        ]

    # Ensure that the dictionaries exist.
    for header_key in row_header_keys:
        initialize_score_source(scores, header_key)
    if include_row_terms:
        for i in range(num_tokens):
            initialize_score_source(scores, unigrams[i])
        if use_bigrams:
            for i in range(num_tokens - 1):
                initialize_score_source(scores, bigrams[i])

    # Ensure that the header self-interactions are initialized.
    for outer_header in row_header_keys:
        for inner_header in column_header_keys:
            initialize_score_pair(scores, outer_header, inner_header)

    # Ensure that the unigram interactions are initialized.
    for i in range(num_tokens):
        unigram_i = unigrams[i]

        # Initialize all row header interactions with this unigram.
        for header_key in row_header_keys:
            initialize_score_pair(scores, header_key, unigram_i)

        if include_row_terms:
            # Initialize this unigram's interaction with all column headers.
            for header_key in column_header_keys:
                initialize_score_pair(scores, unigram_i, header_key)

            # Initialize the unigram self-interactions.
            initialize_score_pair(scores, unigram_i, unigram_i)
            for j in range(i + 1, min(num_tokens, i + interaction_length + 1)):
                unigram_j = unigrams[j]
                initialize_score_pair(scores, unigram_i, unigram_j)
                initialize_score_pair(scores, unigram_j, unigram_i)

    if use_bigrams:
        # Ensure that all bigram interactions are initialized.
        for i in range(num_tokens - 1):
            bigram_i = bigrams[i]

            # Initialize all row header interactions with this bigram.
            for header_key in row_header_keys:
                initialize_score_pair(scores, header_key, bigram_i)

            if include_row_terms:
                # Initialize this bigram's interaction with all column headers.
                for header_key in column_header_keys:
                    initialize_score_pair(scores, bigram_i, header_key)

                # Initialize all unigram interactions with this bigram.
                for j in range(
                        max(0, i - interaction_length),
                        min(num_tokens, (i + 1) + interaction_length + 1)):
                    unigram_j = unigrams[j]
                    initialize_score_pair(scores, bigram_i, unigram_j)
                    initialize_score_pair(scores, unigram_j, bigram_i)

                # Initialize all bigram interactions with this bigram.
                initialize_score_pair(scores, bigram_i, bigram_i)
                for j in range(
                        i + 1,
                        min(num_tokens - 1, (i + 1) + interaction_length + 1)):
                    bigram_j = bigrams[j]
                    initialize_score_pair(scores, bigram_i, bigram_j)
                    initialize_score_pair(scores, bigram_j, bigram_i)

    return (unigrams, bigrams, row_header_keys, column_header_keys)


def unigram_unigram_update(scores, unigram_i, i, unigram_j, j, multiplier=1.):
    '''Updates the score matrix using a unigram/unigram interaction.

    Args:
      scores: The dictionary of dictionary of scores to update.
      unigram_i: The unigram in position 'i'.
      i: The index the first unigram begins at.
      unigram_j: The unigram in position 'j'.
      j: The index the second unigram begins at.
      multiplier: The factor to multiply all updates by.
    '''
    update = description_interaction_kernel(j - i) * multiplier
    scores[unigram_i][unigram_j] += update
    scores[unigram_j][unigram_i] += update


def bigram_unigram_update(scores, bigram_i, i, unigram_j, j, multiplier=1.):
    '''Updates the score matrix using a bigram/unigram interaction.

    Args:
      scores: The dictionary of dictionary of scores to update.
      bigram_i: The bigram in position 'i'.
      i: The index the first term begins at.
      unigram_j: The unigram in position 'j'.
      j: The index the second term begins at.
      multiplier: The factor to multiply all updates by.
    '''
    if j <= i:
        distance = i - j
    else:
        distance = j - (i + 1)
    update = description_interaction_kernel(distance) * multiplier
    scores[bigram_i][unigram_j] += update
    scores[unigram_j][bigram_i] += update


def bigram_bigram_update(scores, bigram_i, i, bigram_j, j, multiplier=1.):
    '''Updates the score matrix using a bigram/bigram interaction.

    Args:
      scores: The dictionary of dictionary of scores to update.
      bigram_i: The bigram in position 'i'.
      i: The index the first term begins at.
      bigram_j: The bigram in position 'j'.
      j: The index the second term begins at.
      multiplier: The factor to multiply all updates by.
    '''
    if j <= i - 1:
        distance = i - (j + 1)
    else:
        distance = max(0, j - (i + 1))
    update = description_interaction_kernel(distance) * multiplier
    scores[bigram_i][bigram_j] += update
    scores[bigram_j][bigram_i] += update


def update_scores(token_list,
                  header,
                  scores,
                  interaction_length=DEFAULT_INTERACTION_LENGTH,
                  include_row_terms=False,
                  include_row_funders=False,
                  include_row_product_types=False,
                  use_bigrams=True,
                  header_update=10.,
                  multiplier=1.,
                  squash_by_token_length=True):
    '''Updates dict of dicts of interaction weights w/ token list interactions.

    Args:
      token_list: The list of description tokens to update using.
      header: The header information for the contract, such as the vendor,
          contracting_office, contracting_office_agency, requesting_office, and
          requesting_office_agency.
      scores: The dictionary of dictionary of weights to update.
      interaction_length: The maximum distance between interacting tokens.
      include_row_terms: Whether terms should be included as row items.
      include_row_funders: Whether funders should be included as row items.
      include_row_product_types: Whether product types are included in rows.
      use_bigrams: Whether bigrams, rather than just unigrams, should be used.
      header_update: The interaction score with header information.
      multiplier: The factor to multiply all updates by.
      squash_by_token_length: If true, divide token updates by sqrt(num tokens).
    '''
    if squash_by_token_length:
        squash = 1. / sqrt(max(len(token_list), 1))
    else:
        squash = 1.

    header_update *= multiplier
    self_interaction = description_interaction_kernel(0) * multiplier * squash

    (unigrams, bigrams,
     row_header_keys, column_header_keys) = initialize_score_update(
         token_list, header, scores, interaction_length, include_row_terms,
         include_row_funders, include_row_product_types, use_bigrams)

    # Incorporate the header/header interactions.
    for outer_header in row_header_keys:
        for inner_header in column_header_keys:
            scores[outer_header][inner_header] += header_update

    num_tokens = len(token_list)
    for i in range(num_tokens):
        unigram_i = unigrams[i]

        # Incorporate the header interactions with this unigram.
        for header_key in row_header_keys:
            scores[header_key][unigram_i] += header_update * squash

        if include_row_terms:
            # Incorporate this unigram's interactions with the column headers.
            for header_key in column_header_keys:
                scores[unigram_i][header_key] += header_update * squash

            # Incorporate this unigram's interactions with nearby subsequent
            # unigrams.
            scores[unigram_i][unigram_i] += self_interaction
            for j in range(i + 1, min(i + interaction_length + 1, num_tokens)):
                unigram_j = unigrams[j]
                unigram_unigram_update(scores,
                                       unigram_i,
                                       i,
                                       unigram_j,
                                       j,
                                       multiplier=multiplier * squash)

    if use_bigrams:
        # Incorporate all bigram interactions.
        for i in range(num_tokens - 1):
            bigram_i = bigrams[i]

            # Incorporate all header interactions with this bigram.
            for header_key in row_header_keys:
                scores[header_key][bigram_i] += header_update * squash

            if include_row_terms:
                # Incorporate this bigram's interactions with column headers.
                for header_key in column_header_keys:
                    scores[bigram_i][header_key] += header_update * squash

                # Incorporate this bigram's interactions with nearby subsequent
                # unigrams.
                interacting_uni_beg = max(0, i - interaction_length)
                interacting_uni_end = min(num_tokens,
                                          (i + 1) + interaction_length + 1)
                for j in range(interacting_uni_beg, interacting_uni_end):
                    bigram_unigram_update(scores,
                                          bigram_i,
                                          i,
                                          unigrams[j],
                                          j,
                                          multiplier=multiplier * squash)

                # Incorporate this bigram's interactions with nearby subsequent
                # bigrams.
                scores[bigram_i][bigram_i] += self_interaction
                interacting_bi_end = min(num_tokens - 1,
                                         (i + 1) + interaction_length + 1)
                for j in range(i + 1, interacting_bi_end):
                    bigram_bigram_update(scores,
                                         bigram_i,
                                         i,
                                         bigrams[j],
                                         j,
                                         multiplier=multiplier * squash)


def transpose_scores(scores):
    '''Returns the transpose of the given interaction scores.

    Args:
      scores: The scores to transpose.

    Returns:
      The transposition of the input scores.
    '''
    trans_scores = {}
    for source in scores:
        targets = scores[source]
        for target in targets:
            if not target in trans_scores:
                trans_scores[target] = {}
            trans_scores[target][source] = scores[source][target]

    return trans_scores


def get_row_sums(scores):
    '''Returns the row sums of the given score matrix.'''
    row_sums = {}
    for source in scores:
        row_sum = 0
        for target in scores[source]:
            row_sum += scores[source][target]
        row_sums[source] = row_sum

    return row_sums


def get_column_sums(scores):
    '''Returns the column sums of the given score matrix.'''
    column_sums = {}
    for source in scores:
        for target in scores[source]:
            if not target in column_sums:
                column_sums[target] = 0
            column_sums[target] += scores[source][target]

    return column_sums


def get_row_and_column_sums(scores):
    '''Returns the row and column sums of the given score matrix.'''
    row_sums = {}
    column_sums = {}
    for source in scores:
        row_sum = 0
        for target in scores[source]:
            value = scores[source][target]
            row_sum += value
            if not target in column_sums:
                column_sums[target] = 0
            column_sums[target] += value

        row_sums[source] = row_sum

    return row_sums, column_sums


def truncate_rows_by_sum(scores, row_sums, caps, keep_rows=[]):
    '''Thresholds to the dominant rows based upon their row sum.

    Args:
      scores: The score matrix to be truncated.
      row_sums: The precomputed row sums of the score matrix.
      caps: The dictionary mapping item types to the max number of rows which
          can be kept for that type.
      keep_rows: An optional list of entity names to prevent deletion of.

    Returns:
      A dictionary of kept types.
    '''
    # Sort the row sum dictionary by value, in descending order.
    sorted_row_sums = {
        k: v
        for k, v in sorted(
            row_sums.items(), key=lambda item: item[1], reverse=True)
    }

    keep_set = set(keep_rows)

    kept = {}
    num_kept = {}
    for item_type in caps:
        kept[item_type] = set()
        num_kept[item_type] = 0

    for source in sorted_row_sums:
        item_type = util.keys.label_type_from_key(source)
        item_label = util.keys.label_from_key(source)
        if item_label in keep_set or (row_sums[source] > 0 and
                                      num_kept[item_type] < caps[item_type]):
            kept[item_type].add(source)
            num_kept[item_type] += 1
        else:
            del scores[source]

    return kept


def truncate_columns_by_sum(scores, column_sums, caps, keep_columns=[]):
    '''Thresholds to the dominant columns based upon their column sums.

    Args:
      scores: The score matrix to be truncated.
      column_sums: The precomputed column sums of the score matrix.
      caps: The dictionary mapping item types to the max number of columns which
          can be kept for that type.
      keep_columns: An optional list of entity names to prevent deletion of.

    Returns:
      A dictionary of kept item types.
    '''
    # Sort the column sum dictionary by value, in descending order.
    sorted_column_sums = {
        k: v
        for k, v in sorted(
            column_sums.items(), key=lambda item: item[1], reverse=True)
    }

    keep_set = set(keep_columns)

    kept = {}
    num_kept = {}
    for item_type in caps:
        kept[item_type] = set()
        num_kept[item_type] = 0

    for source in sorted_column_sums:
        item_type = util.keys.label_type_from_key(source)
        item_label = util.keys.label_from_key(source)
        if item_label in keep_set or (column_sums[source] > 0 and
                                      num_kept[item_type] < caps[item_type]):
            kept[item_type].add(source)
            num_kept[item_type] += 1

    for source in scores:
        targets = list(scores[source].keys())
        for target in targets:
            kept_target = False
            for item_type in kept:
                if target in kept[item_type]:
                    kept_target = True
            if not kept_target:
                del scores[source][target]

    return kept


def sort_scores(scores):
    '''Sorts the interactions of each source in descending order.

    Args:
      scores: The score matrix (dict of dicts) to sort.
    '''
    for source in scores:
        scores[source] = {
            k: v
            for k, v in sorted(
                scores[source].items(), key=lambda item: item[1], reverse=True)
        }


def transform_scores_to_pmi(scores):
    '''Replaces co-occurrence scores with their pointwise mutual information.

    We use log(x + 1) instead of log(x) to avoid domain errors for x = 0.

    Args:
      scores: The scores to normalize.
    '''
    row_sums, column_sums = get_row_and_column_sums(scores)

    for source in scores:
        row_sum = row_sums[source]
        targets = scores[source]
        for target in targets:
            column_sum = column_sums[target]
            score = scores[source][target]
            pmi = log((score + 1) / (row_sum * column_sum))
            scores[source][target] = pmi


def transform_scores_via_logp1(scores):
    '''Replaces co-occurrence scores x with log(x + 1).

    A similar approach -- albeit incorporating entrywise weighting, is
    advocated in: Pennington et al., "GloVe: Global Vectors for Word
    Representations.".

    Args:
      scores: The scores to squash.
    '''
    for source in scores:
        targets = scores[source]
        for target in targets:
            score = scores[source][target]
            scores[source][target] = log(score + 1.)


def truncate_scores(scores,
                    row_caps,
                    column_caps,
                    keep_rows=[],
                    keep_columns=[],
                    verbose=False):
    '''Truncates the score matrix based upon row and column sums.

    Args:
      scores: The input interaction scores (which will be overwritten).
      row_caps: The dictionary mapping item types to the max number of rows
          which can be kept for that type.
      column_caps: The dictionary mapping item types to the max number of
          columns which can be kept for that type.
      keep_rows: An optional list of entity names to prevent deletion of.
      keep_columns: An optional list of entity names to prevent deletion of.
      verbose: Whether to print progress information.

    Returns:
      A dictionary of the kept row and column keys.
    '''
    # Truncate by row sums.
    row_sums = get_row_sums(scores)
    row_kept = truncate_rows_by_sum(scores, row_sums, row_caps, keep_rows)
    if verbose:
        print('Row truncation kept:')
        for item_type in row_kept:
            print('  {}: {} items'.format(item_type, len(row_kept[item_type])))

    # Truncate by column sums.
    column_sums = get_column_sums(scores)
    column_kept = truncate_columns_by_sum(scores, column_sums, column_caps,
                                          keep_columns)
    if verbose:
        print('Column truncation kept:')
        for item_type in column_kept:
            print('  {}: {} items'.format(item_type,
                                          len(column_kept[item_type])))

    # Sort each source's dict based upon its values (in descending order).
    sort_scores(scores)

    return {'row': row_kept, 'column': column_kept}


def get_source_and_target_bijections(scores):
    '''Returns maps between keys and numerical indices.

    Args:
      scores: The dictionary of dictionaries of cooccurrence scores.

    Returns:
      The pairing of source and target key/index bijections.
    '''
    sources = scores.keys()
    targets = set()
    for source in scores:
        for target in scores[source]:
            targets.add(target)

    source_bijection = KeyIndexBijection({}, [])
    source_ind = 0
    for source in sources:
        source_bijection.key_to_index[source] = source_ind
        source_bijection.index_to_key.append(source)
        source_ind += 1

    target_bijection = KeyIndexBijection({}, [])
    target_ind = 0
    for target in targets:
        target_bijection.key_to_index[target] = target_ind
        target_bijection.index_to_key.append(target)
        target_ind += 1

    print('Processed {} sources and {} targets.'.format(
        source_ind, target_ind))

    return (source_bijection, target_bijection)


def get_matrix_dimensions(scores):
    '''Returns the pairing of the number of rows and columns of a score matrix.

    Args:
      scores: The dictionary of dictionaries of cooccurrence scores.

    Returns:
      The pairing of the number of rows and columns.
    '''
    source_bijection, target_bijection = get_source_and_target_bijections(
        scores)
    num_rows = len(source_bijection.key_to_index.keys())
    num_columns = len(target_bijection.key_to_index.keys())
    return (num_rows, num_columns)


def get_csr_matrix_from_scores(scores,
                               source_bijection,
                               target_bijection,
                               dtype=float32):
    '''Returns a scipy.sparse.csr_matrix of the given scores.

    Args:
      scores: The dictionary of dictionaries of scores.
      source_bijection: The map between source keys and source indices.
      target_bijection: The map between target keys and target indices.

    Returns:
      The scipy.sparse.csr_matrix equivalent of the scores.
    '''
    # Count the total number of entries -- including in each row.
    num_rows = len(source_bijection.key_to_index.keys())
    num_columns = len(target_bijection.key_to_index.keys())
    indptr = zeros((num_rows + 1, ), dtype=int)
    num_nonzero = 0
    for i in range(num_rows):
        indptr[i] = num_nonzero
        source = source_bijection.index_to_key[i]
        num_nonzero += len(scores[source])
    indptr[num_rows] = num_nonzero

    # Fill the column indices and data.
    data = zeros((num_nonzero, ), dtype=dtype)
    indices = zeros((num_nonzero, ), dtype=int)
    offset = 0
    for i in range(num_rows):
        source = source_bijection.index_to_key[i]
        scores_row = scores[source]
        for target in scores_row:
            j = target_bijection.key_to_index[target]
            indices[offset] = j
            data[offset] = scores_row[target]
            offset += 1

    scores_csr = csr_matrix((data, indices, indptr),
                            shape=(num_rows, num_columns),
                            dtype=dtype)

    return scores_csr


def print_source_interactions_in_csr(scores, source, source_bijection,
                                     target_bijection):
    '''Gets pairs of target keys and values for nonzeros in source row.

    Args:
      scores: A scipy.sparse.csr_matrix of the interaction scores.
      source: The key for the source to query interactions with.
      source_bijection: The map between source keys and source indices.
      target_bijection: The map between target keys and target indices.

    Returns:
      The list of pairings of target keys and interaction values.
    '''
    source_ind = source_bijection.key_to_index[source]
    interactions = []
    for ind in range(scores.indptr[source_ind], scores.indptr[source_ind + 1]):
        column = scores.indices[ind]
        value = scores.data[ind]
        column_key = target_bijection.index_to_key[column]
        interactions.append((column_key, value))

    return interactions
