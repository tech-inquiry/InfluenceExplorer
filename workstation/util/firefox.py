#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Utilities for automating Firefox browser interactions through Selenium.
#
import glob
import os
import time
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service

# The maximum number of seconds to monitor a download before failing.
MAX_DOWNLOAD_SECONDS = 600

# The number of seconds to wait between each check if a download completed.
DOWNLOAD_RECHECK_SECONDS = 0.25


def get_browser(headless=True,
                download_dir=None,
                gecko_path=None,
                gecko_env='SELENIUM_GECKO_DRIVER'):
    '''Returns a Selenium Firefox instance with configured download options.

    Args:
      headless: Whether the browser should be blocked from displaying.
      download_dir: The directory downloaded files will be saved to.
      gecko_path: The path to the Selenium Firefox Gecko driver. If None, we
          attempt to read it from the 'gecko_env' environment variable.
    '''
    profile = webdriver.FirefoxProfile()

    options = Options()
    options.headless = headless

    if gecko_path is None:
        if gecko_env in os.environ:
            gecko_path = os.environ[gecko_env]
            print('Set Gecko driver path to {} using {} environment variable'.
                  format(gecko_path, gecko_env))
        else:
            print(
                'ERROR: Neither gecko_path nor the fallback environment variable {} was set.'
                .format(gecko_env))
            return None

    if download_dir is not None:
        # Setting 'folderList' to '2' switches to a manually specified download
        # directory rather than the default location.
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.dir', download_dir)
        profile.set_preference('browser.download.useDownloadDir', True)
        print('Set Selenium Firefox download dir to {}'.format(download_dir))

    # This should help prevent browser.quit() from hanging. See
    #   https://stackoverflow.com/a/48767729
    profile.set_preference('browser.tabs.warnOnClose', False)
    profile.set_preference('dom.disable_beforeunload', True)

    # We have to set both the types to save automatically and clear the list
    # of directly-viewable types to ensure a direct download on a click.
    DOWNLOAD_MIME_TYPES = [
        'application/x-www-form-urlencoded', 'application/octet-stream',
        'text/csv'
    ]
    profile.set_preference('browser.download.viewableInternally.enabledTypes',
                           '')
    profile.set_preference('browser.helperApps.neverAsk.saveToDisk',
                           ';'.join(DOWNLOAD_MIME_TYPES))
    profile.set_preference('browser.download.manager.showWhenStarting', False)

    service = Service(executable_path=gecko_path)

    return webdriver.Firefox(service=service,
                             options=options,
                             firefox_profile=profile)


def watch_download(filepath,
                   download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
                   max_download_seconds=MAX_DOWNLOAD_SECONDS,
                   verbose=False):
    '''Waits until a download completes by monitoring its .part file.

    Args:
      filepath: The full path to the file being downloaded.
      download_recheck_seconds: How frequently to check download progress.
      max_download_seconds: The maximum time to wait for the download to finish.
      verbose: Whether to print progress information.
    '''
    filepart = '{}.part'.format(filepath)

    download_seconds = 0
    while download_seconds < max_download_seconds and (
            os.path.exists(filepart) or not os.path.exists(filepath)):
        if verbose:
            if os.path.exists(filepart):
                print('  Retrying because {} still exists.'.format(filepart))
            elif not os.path.exists(filepath):
                print('  Retrying because {} does not yet exist.'.format(
                    filepath))
        time.sleep(download_recheck_seconds)
        download_seconds += download_recheck_seconds

    successful = os.path.exists(filepath) and not os.path.exists(filepart)
    return successful


def watch_download_glob(pattern,
                        download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
                        max_download_seconds=MAX_DOWNLOAD_SECONDS,
                        verbose=False):
    '''Waits until a download completes by monitoring its .part file.

    Args:
      filepath: The full path to the file being downloaded.
      download_recheck_seconds: How frequently to check download progress.
      max_download_seconds: The maximum time to wait for the download to finish.
      verbose: Whether to print progress information.
    '''
    part_pattern = '{}.part'.format(pattern)

    download_seconds = 0
    while download_seconds < max_download_seconds and (len(
            glob.glob(part_pattern)) or not len(glob.glob(pattern))):
        if verbose:
            if glob.glob(part_pattern):
                print(
                    '  Retrying because {} still exists.'.format(part_pattern))
            elif not glob.glob(pattern):
                print('  Retrying because {} does not yet exist.'.format(
                    pattern))
        time.sleep(download_recheck_seconds)
        download_seconds += download_recheck_seconds

    successful = len(glob.glob(pattern)) and not len(glob.glob(part_pattern))
    return successful


def find_element_by_url(browser, url):
    '''A helper function to find an element via its URL'''
    return browser.find_element_by_xpath('//a[@href="{}"]'.format(url))
