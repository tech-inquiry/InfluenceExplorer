#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Utilities for extracting JSON subobjects.
#


def get_from_path(data, path):
    '''Extracts a component of a JSON object via its array of keys/indices.

    Args: 
      - data: The JSON object to extract a component from.
      - path: The array of keys -- read from left to right -- to iterate through
            in order to extract the component. For example, if `path` is
            `['a', 5, 'blue']`, then the return value will be
            `data['a'][5]['blue']`.

    Returns:
      The component at the specified path in the input JSON object.
    '''
    obj = data
    for path_piece in path:
        if isinstance(obj, dict):
            if path_piece not in obj:
                return None
        elif isinstance(obj, list):
            if path_piece >= len(obj):
                return None
        else:
            return None
        obj = obj[path_piece]
    return obj


def has_path(data, path):
    '''Returns whether a JSON object has a component at the given path.

    Args: 
      - data: The JSON object to the component existence from.
      - path: The array of keys -- read from left to right -- to iterate through
            in order to test for the component. For example, if `path` is
            `['a', 5, 'blue']`, then the target path would be
            `data['a'][5]['blue']`.

    Returns:
      A boolean denoting whether or not `data` has a component at the given
      path.
    '''
    obj = data
    for path_piece in path:
        if isinstance(obj, dict):
            if path_piece not in obj:
                return False
        elif isinstance(obj, list):
            if path_piece >= len(obj):
                return False
        else:
            return False
        obj = obj[path_piece]
    return True


def set_from_path(data, path, value):
    '''Sets a component of a JSON object via an array of keys/indices.

    Args: 
      - data: The JSON object to modify.
      - path: The array of keys -- read from left to right -- to iterate through
            in order to set the component. For example, if `path` is
            `['a', 5, 'blue']`, then we will assign
            `data['a'][5]['blue'] = value`.
    '''
    if not len(path):
        return
    obj = data

    # Iterate over the non-leaf components.
    for path_index, path_piece in enumerate(path[:-1]):
        next_path_piece = path[path_index + 1]
        if isinstance(obj, dict):
            if path_piece not in obj:
                if isinstance(next_path_piece, int):
                    obj[path_piece] = []
                elif isinstance(next_path_piece, str):
                    obj[path_piece] = {}
        elif isinstance(obj, list):
            old_len = len(obj)
            if path_piece >= old_len:
                for index in range(old_len, path_piece):
                    obj.append(None)
                if isinstance(next_path_piece, int):
                    obj.append([])
                elif isinstance(next_path_piece, str):
                    obj.append({})
        else:
            print('ERROR: Path component {} of {} did not exist'.format(
                path_piece, path))
            return
        obj = obj[path_piece]

    # Handle the leaf node.
    path_piece = path[-1]
    if isinstance(obj, dict):
        obj[path_piece] = value
    elif isinstance(obj, list):
        old_len = len(obj)
        if path_piece >= old_len:
            for index in range(old_len, path_piece):
                obj.append(None)
            obj.append(value)
        else:
            obj[path_piece] = value
