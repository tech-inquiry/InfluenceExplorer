#
# Copyright (c) 2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#


def num_days_in_month(year, month):
    num_days = None
    if month == 1:
        num_days = 31
    elif month == 2:
        leap_year = (year % 4 == 0)
        num_days = 29 if leap_year else 28
    elif month == 3:
        num_days = 31
    elif month == 4:
        num_days = 30
    elif month == 5:
        num_days = 31
    elif month == 6:
        num_days = 30
    elif month == 7:
        num_days = 31
    elif month == 8:
        num_days = 31
    elif month == 9:
        num_days = 30
    elif month == 10:
        num_days = 31
    elif month == 11:
        num_days = 30
    elif month == 12:
        num_days = 31
    return num_days


def next_day(year, month, day):
    num_days = num_days_in_month(year, month)
    if month < 1 or month > 12 or day < 1 or day > num_days:
        raise Exception
        print('{}-{}-{} is an invalid date'.format(year, month, day))
    if day == num_days:
        if month == 12:
            return (year + 1, 1, 1)
        else:
            return (year, month + 1, 1)
    else:
        return (year, month, day + 1)


def later_day(year, month, day, days_later):
    for i in range(days_later):
        (year, month, day) = next_day(year, month, day)
    return (year, month, day)


def previous_day(year, month, day):
    num_days = num_days_in_month(year, month)
    if month < 1 or month > 12 or day < 1 or day > num_days:
        raise Exception
        print('{}-{}-{} is an invalid date'.format(year, month, day))
    if day == 1:
        if month == 1:
            return (year - 1, 12, 31)
        else:
            return (year, month - 1, num_days_in_month(year, month - 1))
    else:
        return (year, month, day - 1)


def earlier_day(year, month, day, days_earlier):
    for i in range(days_earlier):
        (year, month, day) = previous_day(year, month, day)
    return (year, month, day)


def date_to_tuple(date, sep='-'):
    tokens = date.split(sep)
    return (int(tokens[0]), int(tokens[1]), int(tokens[2]))


def tuple_to_date(year, month, day, sep='-'):
    return sep.join([str(year), str(month).zfill(2), str(day).zfill(2)])


def date_range_tuples(start_date, end_date):
    start_year, start_month, start_day = date_to_tuple(start_date)
    end_year, end_month, end_day = date_to_tuple(end_date)

    day_tuples = []
    year, month, day = (start_year, start_month, start_day)
    while (year, month, day) <= (end_year, end_month, end_day):  
        day_tuples.append((year, month, day))
        (year, month, day) = next_day(year, month, day)

    return day_tuples
