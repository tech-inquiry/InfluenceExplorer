import json

ENTITIES = json.load(open('../data/entities.json'))


def symmetrize_graph(graph):
    names = list(graph.keys())
    for name in names:
        for connection in graph[name]:
            if connection not in graph:
                graph[connection] = set()
            graph[connection].add(name)


def convert_name(name, entities=ENTITIES, use_short=True):
    if not use_short:
        return name
    if name not in entities:
        return name
    profile = entities[name]
    if 'name' not in profile:
        return name
    if 'short' in profile['name']:
        return profile['name']['short']
    elif 'dba' in profile['name']:
        return profile['name']['dba']
    elif 'aka' in profile['name']:
        return profile['name']['aka']
    elif 'full' in profile['name']:
        return profile['name']['full']
    else:
        print(profile['name'])
        return name


def get_ancestors(entities=ENTITIES):

    def _ancestors_helper(entities, name, ancestor_set):
        if name not in entities:
            return
        profile = entities[name]
        if 'parents' not in profile:
            return
        for parent in profile['parents']:
            if parent in ancestor_set:
                return
            ancestor_set.add(parent)
            _ancestors_helper(entities, parent, ancestor_set)

    ancestors = {}
    for name in entities.keys():
        ancestor_set = set()
        _ancestors_helper(entities, name, ancestor_set)
        ancestors[name] = list(ancestor_set)

    return ancestors


def get_graph(entities=ENTITIES):
    names = entities.keys()
    ancestors = get_ancestors(entities)
    graph = {}
    # Extract the original graph, without the implied symmetry.
    for name, profile in entities.items():
        graph[name] = set()
        if 'associations' in profile:
            for association in profile['associations']:
                from_obj = association['from']
                if 'names' in from_obj:
                    for item in from_obj['names']:
                        if isinstance(item, str):
                            graph[name].add(item)
                        else:
                            graph[name].add(item['name'])
                else:
                    graph[name].add(from_obj['name'])
    # Enforce the implied symmetry.
    symmetrize_graph(graph)
    # Push the connections up to the ancestors.
    for name in names:
        for ancestor in ancestors[name]:
            if ancestor not in graph:
                graph[ancestor] = set()
            for connection in graph[name]:
                graph[ancestor].add(connection)
    # Flatten the sets into lists.
    for name in names:
        graph[name] = list(graph[name])
    return graph


def get_neighborhood(graph,
                     name,
                     prevent_self_edge=True,
                     prevent_double_edge=True,
                     absorb_single_children=True,
                     entities=ENTITIES):

    def allow_entity(n):
        if not absorb_single_children:
            return True
        if n not in entities or 'parents' not in entities[n] or len(
                entities[n]['parents']) != 1:
            return True
        parent = entities[n]['parents'][0]
        if parent == name or parent in graph[name]:
            return False
        return True

    filter = set()
    if allow_entity(name):
        filter.add(name)
    for connection in graph[name]:
        if allow_entity(connection):
            filter.add(connection)
    subgraph = {}
    if name in filter:
        subgraph[name] = graph[name]
    for connection in graph[name]:
        if connection not in filter:
            continue
        subgraph[connection] = set()
        for c2 in graph[connection]:
            if c2 in filter:
                if prevent_self_edge and c2 == connection:
                    continue
                if prevent_double_edge and c2 in subgraph and connection in subgraph[
                        c2]:
                    continue
                subgraph[connection].add(c2)
        subgraph[connection] = list(subgraph[connection])
    return subgraph


def shorten_graph(graph, entities=ENTITIES):
    short_graph = {}
    for name in graph:
        short_name = convert_name(name, entities=entities)
        short_graph[short_name] = [
            convert_name(connection, entities=entities)
            for connection in graph[name]
        ]
    return short_graph


def graph_to_viz(graph, filename='viz.html', use_short=True):

    def sanitize_name(name):
        return name.replace("'", "\\'")

    viz_graph = shorten_graph(graph) if use_short else graph
    name_to_id = {}
    for i, name in enumerate(viz_graph.keys()):
        name_to_id[name] = i + 1

    def name_to_node_string(name):
        return "{{'id': {}, 'label': '{}'}}".format(name_to_id[name],
                                                    sanitize_name(name))

    def names_to_edge_string(name_from, name_to):
        return "{{'from': {}, 'to': {}}}".format(name_to_id[name_from],
                                                 name_to_id[name_to])

    def name_to_edges_string(name):
        return ',\n'.join([
            names_to_edge_string(name, connection)
            for connection in viz_graph[name]
        ])

    nodes_string = ',\n'.join(
        [name_to_node_string(name) for name in viz_graph.keys()])
    edges_string = ',\n'.join([
        name_to_edges_string(name) for name in viz_graph.keys()
        if len(viz_graph[name])
    ])

    with open(filename, 'w') as outfile:
        outfile.write('''
<html>
<head>
    <script type="text/javascript" src="https://unpkg.com/vis-network/standalone/umd/vis-network.min.js"></script>

    <style type="text/css">
        #mynetwork {{
            width: 100%;
            height: 100%;
            border: 1px solid lightgray;
        }}
    </style>
</head>
<body>
<div id="mynetwork"></div>
<script type="text/javascript">
    var nodes = new vis.DataSet([{}]);
    var edges = new vis.DataSet([{}]);
    var container = document.getElementById('mynetwork');
    var data = {{
        nodes: nodes,
        edges: edges
    }};
    var options = {{}};
    var network = new vis.Network(container, data, options);
</script>
</body>
</html>
        '''.format(nodes_string, edges_string))


def generate_neighborhood_viz(name, filename='viz.html'):
    graph = get_graph()
    subgraph = get_neighborhood(graph, name)
    graph_to_viz(subgraph, filename=filename)
