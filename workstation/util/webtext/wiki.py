#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
from urllib.parse import unquote

import wikipediaapi

WIKI = wikipediaapi.Wikipedia('en-user-agent-for-embeddings', timeout=60)


def url_to_label(url):
    tokens = url.split('wikipedia.org/wiki/')
    if len(tokens) != 2:
        print('Error splitting {}'.format(url))
        return ''
    label = tokens[1]
    label = unquote(label)
    if '%' in label:
        print('Warning: post-processed label was {}'.format(label))
    return label


def url_to_summary(url):
    try:
        label = url_to_label(url)
        if not label:
            return ''
        page = WIKI.page(label)
        return page.summary
    except Exception as e:
        print(e)
        return ''
