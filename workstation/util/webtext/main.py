#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import csv
import json
import os
import pandas as pd

import util.postgres
import util.webtext.core
import util.webtext.wiki

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../..')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')
SHARED_DIR = os.path.join(WORKSTATION_DIR, '..')
SHARED_DATA_DIR = os.path.join(SHARED_DIR, 'data')
SHARED_DATA_PATH = os.path.join(os.path.dirname(__file__), '../../data')
WEBTEXT_FILENAME = '{}/webtext.json'.format(SHARED_DATA_DIR)
EXPORT_CSV = '{}/webtext.csv'.format(EXPORT_DIR)


def get_profile_urls(profile):
    urls = []
    if 'urls' in profile:
        url_dict = profile['urls']
        for url_key in url_dict:
            url_value = url_dict[url_key]
            if 'https://www.eswcapital.com/' in url_value:
                # This website keeps hanging.
                continue
            if 'https://www.sothebys.com/' in url_value:
                # This website keeps hanging.
                continue
            if url_key == 'logo' or url_key == 'logoSmall':
                # This is an image.
                continue
            if url_key == 'logoType':
                # There is no URL.
                continue
            if url_key == 'annual reports' or url_key == 'sec':
                # The page is essentially just metadata.
                continue
            if url_key == 'craft':
                # Said site has unreliable information.
                continue
            if url_key == 'facebook':
                continue
            if url_key == 'linkedin':
                continue
            if url_key == 'opencorporates':
                # OpenCorporates appears to block python requests... :-(
                continue
            if url_key == 'opensecrets':
                # We can into processing errors.
                continue
            if url_key == 'vimeo':
                continue
            if url_key == 'youtube':
                continue

            if url_key == 'wikipedia':
                urls.append({'type': 'wikipedia', 'url': url_value})
            elif url_key == 'journalism':
                for article in url_value:
                    item = {'type': url_key, 'url': article['url']}
                    if 'org' in article:
                        item['org'] = article['org']
                    if 'author' in article:
                        item['author'] = article['author']
                    if 'title' in article:
                        item['title'] = article['title']
                    if 'date' in article:
                        item['date'] = article['date']
                    urls.append(item)
            elif url_key == 'misc':
                for article in url_value:
                    item = {'type': url_key, 'url': article['url']}
                    if 'org' in article:
                        item['org'] = article['org']
                    if 'author' in article:
                        item['author'] = article['author']
                    if 'title' in article:
                        item['title'] = article['title']
                    if 'date' in article:
                        item['date'] = article['date']
                    urls.append(item)
            elif url_key == 'transparency':
                for article in url_value:
                    item = {'type': url_key, 'url': article['citation']}
                    urls.append(item)
            else:
                urls.append({'type': url_key, 'url': url_value})

    return urls


def cache_entity_websites(entities, webtext_filename=WEBTEXT_FILENAME):
    if not os.path.exists(webtext_filename):
        with open(webtext_filename, 'w') as outfile:
            json.dump({}, outfile, indent=1)
    webtext = json.load(open(webtext_filename))

    SAVE_FREQUENCY = 50

    retrieval = 0
    for entity in entities:
        profile = entities[entity]
        profile_urls = get_profile_urls(profile)
        for item in profile_urls:
            url = item['url']

            if url not in webtext:
                print('  Retrieving URL for {}: {}'.format(entity, url))

                if item['type'] == 'wikipedia':
                    print('  Retrieving wikipedia URL {}'.format(url))
                    webtext[url] = {
                        'text': util.webtext.wiki.url_to_summary(url)
                    }
                    continue

                MAX_TRIES = 5
                for num_tries in range(MAX_TRIES):
                    try:
                        text = util.webtext.core.main_text_from_url(url)
                        webtext[url] = {'text': text}
                        retrieval += 1
                        if retrieval % SAVE_FREQUENCY == 0:
                            print(
                                'Saving after {} retrievals'.format(retrieval))
                            with open(webtext_filename, 'w') as outfile:
                                json.dump(webtext, outfile, indent=1)
                        break
                    except Exception as e:
                        print('  Try {}: {}'.format(num_tries, e))

    with open(webtext_filename, 'w') as outfile:
        json.dump(webtext, outfile, indent=1)


def export(entities, webtext_filename=WEBTEXT_FILENAME, export_csv=EXPORT_CSV):
    webtext = json.load(open(webtext_filename))

    text_and_entities = {}
    for entity in entities:
        profile = entities[entity]
        profile_urls = get_profile_urls(profile)
        for item in profile_urls:
            url = item['url']
            if url not in webtext:
                continue
            text = webtext[url]['text']
            if not text:
                continue

            if url not in text_and_entities:
                text_and_entities[url] = {'text': text, 'entities': []}
            text_and_entities[url]['entities'].append(entity)

    data = []
    for url in text_and_entities:
        data.append({
            'url': url,
            'text': text_and_entities[url]['text'],
            'entities': json.dumps(text_and_entities[url]['entities'])
        })
    df = pd.DataFrame(data)
    df = util.postgres.preprocess_dataframe(df)
    util.postgres.df_to_csv(df, export_csv)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    util.postgres.execute('''
CREATE TABLE IF NOT EXISTS webtext(
  id SERIAL PRIMARY KEY,
  url VARCHAR(1023),
  text TEXT,
  entities JSONB,
  CONSTRAINT unique_webtext UNIQUE (url)
);

CREATE INDEX IF NOT EXISTS webtext_search_index ON webtext USING GIN(
  to_tsvector('simple',
    lower(text) || ' ' || lower(entities::TEXT)
  )
);

CREATE INDEX IF NOT EXISTS webtext_entities_index ON webtext USING GIN(
  (entities) jsonb_path_ops);
    ''')


def postgres_update(export_csv=EXPORT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    TABLE_TYPES = {'url': 'VARCHAR(1023)', 'text': 'TEXT', 'entities': 'JSONB'}
    UNIQUE_COLUMNS = ['url']
    FORCE_NULL_COLUMNS = []
    TABLE_NAME = 'webtext'
    TABLE_TMP_NAME = 'webtext_tmp'
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
