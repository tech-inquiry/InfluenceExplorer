#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import os
import re
import sys
import trafilatura
import trafilatura.settings

THIS_DIR = os.path.dirname(__file__)
WEBTEXT_DIR = os.path.join(THIS_DIR, '..')
WORKSTATION_DIR = os.path.join(WEBTEXT_DIR, '..')
sys.path.append(WORKSTATION_DIR)

import util.pdf

RE_SPACES = re.compile(r'\s{3,}')

DEFAULT_TIMEOUT = 10


def main_text(body):
    text = trafilatura.extract(body)
    if text:
        text = text.replace('\n', ' ')
        return RE_SPACES.sub('  ', text)
    else:
        return ''


def main_text_from_url_(url):
    downloaded = trafilatura.fetch_url(url)
    if downloaded is None:
        return ''
    return main_text(downloaded)


def main_text_from_url(url, timeout=DEFAULT_TIMEOUT):
    SKIPPED_SUBSTRINGS = {
        'delltelephone': 'Avoided Dell Telephone website because it was unresponsive',
        'flickr.com': 'Avoided parsing flickr at {}'.format(url),
        'fondationvarenne.com': 'Avoiding Varenne website because it was unresponsive',
        'projects.propublica.org/nonprofits': 'Avoiding boilerplate of Propublica Nonprofit Explorer',
        'raptor-vision.com': 'Avoided Raptor-Vision because it was unresponsive',
        'vimeo.com': 'Avoided parsing video at {}'.format(url),
        'youtube.com': 'Avoided parsing video at {}'.format(url)
    }

    if not isinstance(url, str):
        print('URL was {}, of type {}'.format(url, type(url)))
        return ''
    if url.endswith('.pdf'):
        pages_text = util.pdf.get_text_from_url(url, join='\n')
    if 'wikipedia.org' in url:
        print('Specially handling wikipedia link.')

    for substring, message in SKIPPED_SUBSTRINGS.items():
        if substring in url:
            print(message)
            return ''

    trafilatura.settings.TIMEOUT = timeout
    return main_text_from_url_(url)
