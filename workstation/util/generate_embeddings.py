#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This is a helper script for generating an embedding of the vendors, terms,
# and funders from the metadata of Federal Procurement Data System awards. This
# can take a few hours if run on a full year of FPDS data.
#
# Several more hours are then spent forming a k-means clustering of the vendor
# portion of the normalized left factor of the embeddings.
#
import gc
import gzip
import json
import numpy as np
import os.path
import pandas as pd
import shutil
import sys
import time

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '..')
sys.path.append(WORKSTATION_DIR)

import gov.us.central.procurement as fpds
import util.awls as awls
import util.entities
import util.entity_embeddings as entity_embeddings
import util.keys
import util.postgres
import util.scores

DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
US_OPR_DATA_DIR = os.path.join(DATA_DIR, 'us_opr')
US_FL_PROCUREMENT_DATA_DIR = os.path.join(DATA_DIR, 'us_fl_facts')
AU_CONTRACTS_DATA_DIR = os.path.join(DATA_DIR, 'au_contracts')
IL_CONTRACTS_DATA_DIR = os.path.join(DATA_DIR, 'il_procurement')
CA_LOBBYING_DATA_DIR = os.path.join(DATA_DIR, 'ca_lobbying')
CA_CONTRACTS_DATA_DIR = os.path.join(DATA_DIR, 'ca_awards')
UK_CONTRACTS_DATA_DIR = os.path.join(DATA_DIR, 'uk_contracts')
EU_TENDERS_DATA_DIR = os.path.join(DATA_DIR, 'eu_tenders')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')
MODEL_DIR = os.path.join(EXPORT_DIR, 'embeddings')

# The directory to store model outputs into.
if not os.path.exists(MODEL_DIR):
    os.makedirs(MODEL_DIR)


def gzip_file(orig_file, zipped_file=None):
    '''A utility for zipping a binary file.'''
    if zipped_file == None:
        zipped_file = orig_file + '.gz'
    with open(orig_file, 'rb') as f_in:
        with gzip.open(zipped_file, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)


def write_bijection(bijection, bijection_filename, labels_filename):
    with open(bijection_filename, 'w') as outfile:
        json.dump(bijection, outfile, indent=1)
    labels = [
        '"{}"'.format(key[7:].replace('"', '\\"')) for key in bijection[1]
    ]
    with open(labels_filename, 'w') as outfile:
        outfile.write('\n'.join(labels))


def write_neighbors(neighbors,
                    neighbors_json,
                    neighbors_csv,
                    max_vendor_length=255):
    with open(neighbors_json, 'w') as outfile:
        json.dump(neighbors, outfile, indent=1)
    neighbor_data = [[vendor, json.dumps(neighbors[vendor])]
                     for vendor in neighbors
                     if len(vendor) <= max_vendor_length]
    df = pd.DataFrame(neighbor_data, columns=['vendor', 'neighbors'])
    for key in ['vendor', 'neighbors']:
        df[key] = df[key].str.replace('\\', '\\\\')
    util.postgres.df_to_csv(df, neighbors_csv)


def train_embeddings(rank, zero_weight, frobenius_regularization,
                     num_iterations, use_row_weights, use_column_weights,
                     squash_exponent, hparams, row_caps, column_caps,
                     filenames):
    # Build the cooccurrence score matrices. We will save copies of the original
    # counts, their truncation, and the subsequent transformation via Pointwise
    # Mutual Information and a log(x + 1) squashing. The log(x + 1) squashing
    # will serve as the input for our low-rank model.
    score_start = time.time()
    interactions = entity_embeddings.get_interactions_over_cache(
        hparams=hparams,
        row_caps=row_caps,
        column_caps=column_caps,
        pretruncation_score_file=filenames['pretruncation_score'],
        score_file=filenames['score'],
        logp1_file=filenames['logp1'])
    score_end = time.time()
    print('Interactions took: {} seconds'.format(score_end - score_start))
    del interactions['kept']['column']
    del interactions['scores']
    gc.collect()
    source_bijection, target_bijection = (
        util.scores.get_source_and_target_bijections(
            interactions['logp1_scores']))
    write_bijection(source_bijection, filenames['source_bijection'],
                    filenames['entity_labels'])

    logp1_matrix = util.scores.get_csr_matrix_from_scores(
        interactions['logp1_scores'], source_bijection, target_bijection)
    del interactions['logp1_scores']
    gc.collect()
    with open(filenames['kept_row_entities'], 'w') as outfile:
        json.dump(list(interactions['kept']['row']['ENTITY']),
                  outfile,
                  indent=1)

    # Generate a low-rank approximation of the log(x + 1) squashing of the
    # cooccurrence counts. We save the original factors to file, as well as the
    # normalization of the rows of the left factor, which will serve as our
    # embedding onto a (RANK - 1)-sphere.
    left_factor, right_factor = awls.generate_embeddings(
        logp1_matrix,
        rank=rank,
        zero_weight=zero_weight,
        frobenius_regularization=frobenius_regularization,
        num_iterations=num_iterations,
        use_row_weights=use_row_weights,
        use_column_weights=use_column_weights)
    np.save(filenames['left_factor'], left_factor)
    awls.normalize_embeddings(left_factor, squash_exponent=squash_exponent)
    np.save(filenames['left_factor_squash'], left_factor)
    np.savetxt(filenames['entity_vectors'], left_factor, delimiter='\t')
    np.save(filenames['right_factor'], right_factor)
    del logp1_matrix
    del right_factor
    gc.collect()
    gzip_file(filenames['logp1'])
    gzip_file(filenames['left_factor_squash'])

    return {
        'left_factor': left_factor,
        'source_bijection': source_bijection,
        'row_entity_interactions': interactions['kept']['row']['ENTITY']
    }


def train_model(rank, zero_weight, frobenius_regularization, num_iterations,
                use_row_weights, use_column_weights, squash_exponent,
                num_neighbors, penalize_smaller_norm, hparams, row_caps,
                column_caps, filenames, max_vendor_length):
    model = train_embeddings(rank, zero_weight, frobenius_regularization,
                             num_iterations, use_row_weights,
                             use_column_weights, squash_exponent, hparams,
                             row_caps, column_caps, filenames)

    # Retrieve the nearest neighbors
    neighbors = entity_embeddings.get_all_nearest_neighbors(
        model['left_factor'],
        model['source_bijection'],
        num_neighbors=num_neighbors,
        penalize_smaller_norm=penalize_smaller_norm)

    write_neighbors(neighbors, filenames['neighbors_json'],
                    filenames['neighbors_csv'], max_vendor_length)

    entities = util.entities.get_entities()
    entity_embeddings.write_and_update_vendors(
        entities, model['row_entity_interactions'])


# The configuration parameters for the cooccurrence formation.
ROW_CAPS = {
    'TERM': 0,
    'ENTITY': 200000,
    'PRINCIPAL_NAICS_CODE': 0,
    'PRODUCT_OR_SERVICE_CODE': 0,
    'OFFICE': 0,
    'OFFICE_AGENCY': 0,
    'OFFICE_DEPARTMENT': 0
}
COLUMN_CAPS = {
    'TERM': 75000,
    'ENTITY': 200000,
    'PRINCIPAL_NAICS_CODE': 50000,
    'PRODUCT_OR_SERVICE_CODE': 50000,
    'OFFICE': 100000,
    'OFFICE_AGENCY': 100000,
    'OFFICE_DEPARTMENT': 20000
}

# The configuration parameters for the low-rank approximation.
# NOTE: Drop the rank to, say, 150, if you do not have the memory for 250.
RANK = 250
ZERO_WEIGHT = 0.02
FROBENIUS_REGULARIZATION = 0.01
NUM_ITERATIONS = 15
USE_ROW_WEIGHTS = False
USE_COLUMN_WEIGHTS = True

# Configuration for neighbor retrieval
SQUASH_EXPONENT = 0.35
PENALIZE_SMALLER_NORM = True
NUM_NEIGHBORS = 50

PRETRUNCATION_SCORE_FILE = '{}/pretruncation_scores.json'.format(MODEL_DIR)
SCORE_FILE = '{}/scores.json'.format(MODEL_DIR)
LOGP1_FILE = '{}/logp1.json'.format(MODEL_DIR)
SOURCE_BIJECTION_FILE = '{}/source_bijection.json'.format(MODEL_DIR)
KEPT_ROW_ENTITIES_FILE = '{}/kept_row_entities.json'.format(MODEL_DIR)

MODEL_TAG = 'rank_{}-zw_{}-fr_{}'.format(RANK, ZERO_WEIGHT,
                                         FROBENIUS_REGULARIZATION)
LEFT_FACTOR_FILE = '{}/left_factor-{}.npy'.format(MODEL_DIR, MODEL_TAG)
LEFT_FACTOR_SQUASH_FILE = '{}/left_factor_squash-{}.npy'.format(
    MODEL_DIR, MODEL_TAG)
RIGHT_FACTOR_FILE = '{}/right_factor-{}.npy'.format(MODEL_DIR, MODEL_TAG)

NEIGHBORS_JSON = '{}/neighbors.json'.format(MODEL_DIR)
NEIGHBORS_CSV = '{}/vendor_neighbors.csv'.format(MODEL_DIR)
VENDORS_CSV = '{}/vendors.csv'.format(MODEL_DIR)

ENTITY_LABELS_FILE = 'entity_labels.tsv'
ENTITY_VECTORS_FILE = 'entity_vectors.tsv'

# Hyperparameters configuring the score update process for each data feed.
hparams = {
    'webtext': {
        'interaction_length': 10,
        'truncation_rate': 500,
        'truncation_scale': 2.
    },
    'au_contracts': {
        'interaction_length': 10,
        'header_update': 10.,
        'weight': 1.,
        'truncation_rate': 100000,
        'truncation_scale': 2.,
        'file_regexp': '{}/*simplified.json'.format(AU_CONTRACTS_DATA_DIR)
    },
    'il_contracts': {
        'interaction_length': 10,
        'header_update': 10.,
        'weight': 1.,
        'truncation_rate': 100000,
        'truncation_scale': 2.,
        'file_regexp': '{}/exemptions.json'.format(IL_CONTRACTS_DATA_DIR)
    },
    'ca_lobby_communication': {
        'interaction_length': 10,
        'header_update': 10.,
        'weight': 0.1,
        'truncation_rate': 100000,
        'truncation_scale': 2.,
        'file_regexp': '{}/communication.json'.format(CA_LOBBYING_DATA_DIR)
    },
    'ca_lobby_registration': {
        'interaction_length': 10,
        'header_update': 10.,
        'weight': 0.1,
        'truncation_rate': 100000,
        'truncation_scale': 2.,
        'file_regexp': '{}/registration.json'.format(CA_LOBBYING_DATA_DIR)
    },
    'ca_proactive_disclosure': {
        'interaction_length':
        10,
        'header_update':
        10.,
        'weight':
        1.,
        'truncation_rate':
        100000,
        'truncation_scale':
        2.,
        'file_regexp':
        '{}/ca_proactive_disclosure_filings_orig.json'.format(
            CA_CONTRACTS_DATA_DIR)
    },
    'ca_pwsgc': {
        'interaction_length':
        10,
        'header_update':
        10.,
        'weight':
        1.,
        'truncation_rate':
        100000,
        'truncation_scale':
        2.,
        'file_regexp':
        '{}/ca_pwsgc_contract_filings_orig.json'.format(CA_CONTRACTS_DATA_DIR)
    },
    'eu_contracts': {
        'interaction_length': 10,
        'header_update': 10.,
        'weight': 1.,
        'truncation_rate': 100000,
        'truncation_scale': 2.,
        'file_dir': EU_TENDERS_DATA_DIR
    },
    'uk_contracts': {
        'interaction_length': 10,
        'header_update': 10.,
        'weight': 1.,
        'truncation_rate': 100000,
        'truncation_scale': 2.,
        'file_regexp': '{}/*simplified.json'.format(UK_CONTRACTS_DATA_DIR)
    },
    'us_opr_filings': {
        'interaction_length': 10,
        'header_update': 5.,
        'weight': 0.65,
        'truncation_rate': 100000,
        'truncation_scale': 2.,
        'file_regexp': '{}/filings*.json'.format(US_OPR_DATA_DIR)
    },
    'us_fpds': {
        'interaction_length':
        10,
        'header_update':
        1.,
        'weight':
        1.,
        'subaward_weight':
        0.25,
        'truncation_rate':
        100000,
        'truncation_scale':
        2.,
        'file_regexp':
        '{}/*.json'.format(fpds.award.SIGNED_DIR),
        'subaward_file':
        '{}/data/us_subawards/contracts/subawards.json'.format(WORKSTATION_DIR)
    },
    'us_fl_procurement': {
        'interaction_length': 10,
        'header_update': 10.,
        'weight': 1.,
        'truncation_rate': 100000,
        'truncation_scale': 2.,
        'file_regexp': '{}/corrected.json'.format(US_FL_PROCUREMENT_DATA_DIR)
    }
}

filenames = {
    'pretruncation_score': PRETRUNCATION_SCORE_FILE,
    'score': SCORE_FILE,
    'logp1': LOGP1_FILE,
    'source_bijection': SOURCE_BIJECTION_FILE,
    'entity_labels': ENTITY_LABELS_FILE,
    'entity_vectors': ENTITY_VECTORS_FILE,
    'kept_row_entities': KEPT_ROW_ENTITIES_FILE,
    'left_factor': LEFT_FACTOR_FILE,
    'left_factor_squash': LEFT_FACTOR_SQUASH_FILE,
    'right_factor': RIGHT_FACTOR_FILE,
    'neighbors_json': NEIGHBORS_JSON,
    'neighbors_csv': NEIGHBORS_CSV
}

train_model(rank=RANK,
            zero_weight=ZERO_WEIGHT,
            frobenius_regularization=FROBENIUS_REGULARIZATION,
            num_iterations=NUM_ITERATIONS,
            use_row_weights=USE_ROW_WEIGHTS,
            use_column_weights=USE_COLUMN_WEIGHTS,
            squash_exponent=SQUASH_EXPONENT,
            num_neighbors=NUM_NEIGHBORS,
            penalize_smaller_norm=PENALIZE_SMALLER_NORM,
            hparams=hparams,
            row_caps=ROW_CAPS,
            column_caps=COLUMN_CAPS,
            filenames=filenames,
            max_vendor_length=255)
