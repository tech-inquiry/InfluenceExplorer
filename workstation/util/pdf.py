#
# Copyright (c) 2020-2023 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import io
import PyPDF2
import requests


def get_text_from_filehandle(filehandle, join=None):
    page_texts = []
    read_pdf = PyPDF2.PdfReader(filehandle)
    for page in read_pdf.pages:
        page_texts.append(page.extract_text())
    return join.join(page_texts) if join else page_texts


def get_text(filename, join=None):
    with open(filename, 'rb') as pdf_file:
        return get_text_from_filehandle(pdf_file, join=join)


def get_text_from_url(url, join=None):
    r = requests.get(url)
    f = io.BytesIO(r.content)
    return get_text_from_filehandle(f, join=join)


def get_text_from_url(url, join=None):
    r = requests.get(url)
    f = io.BytesIO(r.content)
    return get_text_from_filehandle(f, join=join)
