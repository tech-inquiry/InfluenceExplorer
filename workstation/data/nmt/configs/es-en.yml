#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Configuration file for training a Spanish (ES) to English (EN) Neural Machine
# Translation model using the OPUS 'CC Aligned' corpus. We follow the best
# practice of using 'transformers' but use a slightly smaller word and RNN
# embedding dimension than the example configuration provided by OpenNMT
# (256 instead of 512).
#
# After downloading and converting the OPUS CC Aligned de-en corpus into
# training/testing/validation data via:
#
#    >>> import util.opus
#    >>> util.opus.get_cc_aligned('spanish')
#
# the model can be trained using the following two commands
# (which make use of the checkpoint after 100,000 steps):
#
#   onmt_build_vocab -config data/nmt/configs/es-en.yml -n_sample 10000000
#   onmt_train -config data/nmt/configs/es-en.yml
#
# Towards the end of training, the accuracy should be roughly 80% and the
# perplexity should be around or below 3.
#
# After training, the test set can optionally be translated via:
#
#   onmt_translate \
#       -model data/nmt/run/es-en/model_step_100000.pt \
#       -src data/nmt/inputs/es-en/src-test.txt \
#       -replace_unk \
#       -output pred.txt \
#       -gpu 0
#
# For production usage we accelerate the inference via the ctranslate2 package:
#
#   ct2-opennmt-py-converter \
#       --model_path data/nmt/run/es-en/model_step_100000.pt \
#       --output_dir data/nmt/models/es-en
#
# The model can then be used to translate German into English via:
#
#   >>> import util.opus
#   >>> results, seconds = util.opus.translate_to_english(
#   ...     ['hola', 'cómo estás?'], 'spanish')
#   >>> results
#   ['hello', 'how are you?']
#   >>> seconds
#   0.1589221954345703)
#

data:
    corpus_1:
        path_src: data/nmt/inputs/es-en/src-train.txt
        path_tgt: data/nmt/inputs/es-en/tgt-train.txt
    valid:
        path_src: data/nmt/inputs/es-en/src-val.txt
        path_tgt: data/nmt/inputs/es-en/tgt-val.txt

save_data: data/nmt/run/es-en/example
src_vocab: data/nmt/run/es-en/example.vocab.src
tgt_vocab: data/nmt/run/es-en/example.vocab.tgt
overwrite: True

save_model: data/nmt/run/es-en/model
save_checkpoint_steps: 10000
keep_checkpoint: 10
seed: 3435
#train_steps: 500000
train_steps: 100000
valid_steps: 10000
warmup_steps: 8000
report_every: 100

decoder_type: transformer
encoder_type: transformer
word_vec_size: 256
rnn_size: 256
layers: 6
transformer_ff: 2048
heads: 8

accum_count: 8
optim: adam
adam_beta1: 0.9
adam_beta2: 0.998
decay_method: noam
learning_rate: 2.0
max_grad_norm: 0.0

batch_size: 4096
batch_type: tokens
normalization: tokens
dropout: 0.1
label_smoothing: 0.1

max_generator_batches: 2

param_init: 0.0
param_init_glorot: 'true'
position_encoding: 'true'

# We assume a single GPU.
world_size: 1
gpu_ranks: [0]
