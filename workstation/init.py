#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# A utility for loading sample Postgres data from the Docker compose setup.
#
import gov.au.procurement as au_proc
import gov.ca.lobbying as ca_lobby
import gov.ca.procurement as ca_proc
import gov.eu.procurement as eu_proc
import gov.uk.procurement as uk_proc
import gov.us.ca.lobbying as us_ca_lobby
import gov.us.central.labor_relations as us_labor
import gov.us.central.lobbying.opr as opr
import gov.us.central.procurement as us_proc
import gov.us.central.procurement.subawards as us_subawards
import gov.us.central.securities as us_sec
import gov.us.fl.procurement as us_fl_proc
import gov.us.ny.procurement as us_ny_proc
import util.entity_embeddings as entity_embeddings
import util.webtext.main as webtext

print('Updating AU procurement')
au_proc.postgres_update('example-data/au_contract_filing.csv')
print('Updating CA lobbying')
ca_lobby.postgres_update(
  communication_csv='example-data/ca_lobbying_communication.csv',
  lobbyist_csv='example-data/ca_lobbying_lobbyist.csv',
  registration_csv='example-data/ca_lobbying_registration.csv')
print('Updating CA procurement')
ca_proc.postgres_update(
    proactive_csv='example-data/ca_proactive_disclosure_filing.csv',
    pwsgc_csv='example-data/ca_pwsgc_contract_filing.csv')
print('Updating NZ procurement')
print('Updating UK procurement')
uk_proc.postgres_update('example-data/uk_contract_filing.csv')
print('Updating California lobbying data')
us_ca_lobby.postgres_update(
    lobbying_disclosure_cover_csv='example-data/us_calaccess_lobby_disc_cover.csv',
    lobbying_disclosure_payment_csv='example-data/us_calaccess_lobby_disc_payment.csv',
    campaign_disclosure_cover_csv='example-data/us_calaccess_campaign_disc_cover.csv',
    campaign_contribution_csv='example-data/us_calaccess_campaign_contrib.csv',
    pre_election_expenditure_csv='example-data/us_calaccess_pre_elect_expend.csv')
print('Updating US labor relations filings')
us_labor.postgres_update(export_csv='example-data/us_nlrb_filing.csv')
print('Updating US lobbying filings and contributions')
opr.postgres_update(
    filings_csv='example-data/us_lobbying_issues.csv',
    contributions_csv='example-data/us_lobbying_contributions.csv')
print('Updating US procurement data')
us_proc.postgres.update('example-data/us_central_procurement.csv')
us_subawards.postgres_update(
  'example-data/us_central_procurement_subawards.csv')
print('Updating DOD announcements')
us_proc.dod_announce.postgres_update('example-data/us_dod_announcements.csv')
print('Updating SEC filings')
us_sec.postgres_update(
    'example-data/us_securities_meta.csv',
    'example-data/us_securities_filing.csv')
print('Updating FL FACTS filings')
us_fl_proc.postgres_update('example-data/us_fl_facts_filing.csv')
#print('Updating NY OGS')
#us_ny_proc.postgres_update('example-data/us_ny_ogs_filing.csv')

print('Updating EU procurement')
EU_COUNTRY_CODES = [
    'be', 'ch', 'cz', 'de', 'es', 'fr', 'it', 'mk', 'mt', 'nl', 'no', 'pl', 'uk'
]
eu_proc.postgres_create()
for country_code in EU_COUNTRY_CODES:
    csv_filename = 'example-data/{}_eu_tender.csv'.format(country_code)
    eu_proc.postgres_country_update(country_code, csv_filename)

print('Updating webtext')
webtext.postgres_update('example-data/webtext.csv')

print('Replacing vendors and vendor neighbors')
entity_embeddings.postgres_replace(
    vendor_csv='example-data/vendors.csv',
    neighbors_csv='example-data/vendor_neighbors.csv')
