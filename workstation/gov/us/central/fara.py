#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
#  https://efile.fara.gov/bulk/zip/FARA_All_RegistrantDocs.csv.zip
#
import json
import os
import pandas as pd

import util.postgres
import util.zip

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
FARA_DIR = os.path.join(DATA_DIR, 'us_fara')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

FARA_ZIP = 'https://efile.fara.gov/bulk/zip/FARA_All_RegistrantDocs.csv.zip'

EXPORT_JSON = '{}/us_fara.json'.format(EXPORT_DIR)
EXPORT_CSV = '{}/us_fara.csv'.format(EXPORT_DIR)

COLUMN_MAP = {
    'Date Stamped': 'date_stamped',
    'Registrant Name': 'registrant_name',
    'Registration Number': 'registrant_number',
    'Document Type': 'document_type',
    'Short Form Name': 'short_form_name',
    'Foreign Principal Name': 'foreign_principal_name',
    'Foreign Principal Country': 'foreign_principal_country',
    'URL': 'url'
}

DATE_COLUMNS = ['date_stamped']

TABLE_TYPES = {
    'date_stamped': 'TIMESTAMP',
    'registrant_name': 'VARCHAR(255)',
    'registrant_number': 'VARCHAR(15)',
    'document_type': 'VARCHAR(255)',
    'short_form_name': 'VARCHAR(255)',
    'foreign_principal_name': 'VARCHAR(255)',
    'foreign_principal_country': 'VARCHAR(255)',
    'url': 'VARCHAR(255)'
}

UNIQUE_COLUMNS = ['url']

FORCE_NULL_COLUMNS = ['date_stamped']

TABLE_NAME = 'us_fara'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

if not os.path.exists(FARA_DIR):
    os.makedirs(FARA_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)


def export_df_to_csv(df, csv_filename):
    df = util.postgres.preprocess_dataframe(df, unique_columns=UNIQUE_COLUMNS)
    util.postgres.df_to_csv(df, csv_filename)


def export_csv(csv_downloaded,
               csv_filename=EXPORT_CSV,
               json_filename=EXPORT_JSON):
    if os.path.exists(csv_downloaded):
        df = pd.read_csv(csv_downloaded, encoding='ISO-8859-1')
    else:
        print('Did not find downloaded CSV at {}'.format(csv_downloaded))
        return

    df = df.rename(columns=COLUMN_MAP)
    for column in DATE_COLUMNS:
        df[column] = pd.to_datetime(df[column],
                                    format='%m/%d/%Y',
                                    errors='coerce')
    df.to_json(json_filename, orient='records', indent=1, date_format='iso')
    export_df_to_csv(df, csv_filename)


def download_csv(download_dir=FARA_DIR, verbose=False):
    util.zip.download_and_unpack(FARA_ZIP, download_dir)


def retrieve(csv_filename=EXPORT_CSV,
             json_filename=EXPORT_JSON,
             verbose=False):
    '''Downloads and exports the most recent notices using Selenium.'''
    download_csv(download_dir=FARA_DIR, verbose=verbose)

    downloaded_csv = '{}/FARA_All_RegistrantDocs.csv'.format(FARA_DIR)
    try:
        export_csv(downloaded_csv,
                   csv_filename=csv_filename,
                   json_filename=json_filename)
    except Exception as e:
        print('Could not export US FARA filings: {}'.format(e))


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(TABLE_NAME,
                                       TABLE_TYPES,
                                       serial_id=True,
                                       constraints={'unique': 'UNIQUE (url)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_registrant_number_index ON {} (
  lower(registrant_number));
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=EXPORT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
