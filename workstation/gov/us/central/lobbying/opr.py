#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Utilities for analyzing the lobbying disclosure data available at:
#
#   https://www.senate.gov/legislative/Public_Disclosure/database_download.htm
#
# While no API key is required, it is recommended due to the dramatically
# increased request rate cap.
#
# While the API token can be manually specified in each relevant function call,
# it is recommended to instead set the environment variable
# US_SENATE_OPR_REST_TOKEN.
#
import datetime
import glob
import json
import os
import pandas as pd
import re
import requests
import xmltodict

import util.dates
import util.dict_path
import util.entities
import util.postgres
import util.tor

from tqdm import tqdm
from util.entities import canonicalize, normalize, normalization_map_helper

FILINGS_TABLE_TYPES = {
    'filing_uuid': 'VARCHAR(123)',
    'filing_type': 'VARCHAR(3)',
    'filing_year': 'INT',
    'filing_period': 'VARCHAR(15)',
    'income': 'FLOAT',
    'expenses': 'FLOAT',
    'expenses_method': 'VARCHAR(3)',
    'posted_by_name': 'VARCHAR(255)',
    'dt_posted': 'TIMESTAMP',
    'termination_date': 'TIMESTAMP',
    'registrant': 'JSONB',
    'client': 'JSONB',
    'lobbying_activities': 'JSONB',
    'conviction_disclosures': 'JSONB',
    'foreign_entities': 'JSONB',
    'affiliated_organizations': 'JSONB',
    'lobbyist_ids': 'JSONB',
    'search_text': 'TEXT'
}
FILINGS_UNIQUE_COLUMNS = ['filing_uuid']
FILINGS_FORCE_NULL_COLUMNS = [
    'dt_posted', 'termination_date', 'income', 'expenses'
]
FILINGS_TABLE_NAME = 'us_opr_issues_filing'
FILINGS_TABLE_TMP_NAME = '{}_tmp'.format(FILINGS_TABLE_NAME)

CONTRIBUTIONS_TABLE_TYPES = {
    'filing_uuid': 'VARCHAR(123)',
    'filing_type': 'VARCHAR(3)',
    'filing_year': 'INT',
    'filing_period': 'VARCHAR(15)',
    'filer_type': 'VARCHAR(15)',
    'dt_posted': 'TIMESTAMP',
    'contact_name': 'VARCHAR(255)',
    'comments': 'TEXT',
    'address_1': 'VARCHAR(255)',
    'address_2': 'VARCHAR(255)',
    'city': 'VARCHAR(255)',
    'state_display': 'VARCHAR(63)',
    'zip': 'VARCHAR(63)',
    'country_display': 'VARCHAR(63)',
    'registrant': 'JSONB',
    'lobbyist': 'JSONB',
    'pacs': 'JSONB',
    'contribution_items': 'JSONB'
}
CONTRIBUTIONS_UNIQUE_COLUMNS = ['filing_uuid']
CONTRIBUTIONS_FORCE_NULL_COLUMNS = ['dt_posted']
CONTRIBUTIONS_TABLE_NAME = 'us_opr_contributions_filing'
CONTRIBUTIONS_TABLE_TMP_NAME = '{}_tmp'.format(CONTRIBUTIONS_TABLE_NAME)

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../../..')
SHARED_DIR = os.path.join(WORKSTATION_DIR, '..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
LOBBYING_DATA_DIR = os.path.join(DATA_DIR, 'us_opr')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')
# DEPRECATED
GOV_IDS_FILE = '{}/webserver/node/public/js/us/central/lobbying/government_ids.json'.format(
    SHARED_DIR)

OUTPUT_FILINGS_CSV = '{}/us_opr_issues.csv'.format(EXPORT_DIR)
OUTPUT_CONTRIBUTIONS_CSV = '{}/us_opr_contributions.csv'.format(EXPORT_DIR)

GOV_IDS = json.load(open(GOV_IDS_FILE))

if not os.path.exists(LOBBYING_DATA_DIR):
    os.makedirs(LOBBYING_DATA_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)


# We only call this if the token was not manually specified.
def token_from_environment(label='US_SENATE_OPR_REST_TOKEN'):
    '''Attempts to retrieve the API token from an environment variable.'''
    return os.environ[label]


def headers_from_token(token):
    return {'Authorization': 'Token {}'.format(token)}


def api_call(url, token, session=None):
    if session == None:
        return requests.get(url, headers=headers_from_token(token))
    else:
        return session.get(url, headers=headers_from_token(token))


def get_filings(args, token=None, use_tor=False):
    BASE_URL = 'https://lda.senate.gov/api/v1/filings/'
    url = BASE_URL
    if len(args):
        url += '?'
        url += '&'.join('{}={}'.format(key, args[key]) for key in args)

    with (util.tor.get_session()
          if use_tor else requests.session()) as session:
        if token is None:
            token = token_from_environment()
        try:
            response = api_call(url, token, session).json()
        except Exception as e:
            print('Could not process filings from {}'.format(url))
            print(e)
            return []

        count = response['count']
        with tqdm(total=count) as pbar:
            piece = response['results']
            filings = piece
            pbar.update(len(piece))
            while 'next' in response and response['next']:
                raw_response = api_call(response['next'], token, session)
                try:
                    response = raw_response.json()
                    piece = response['results']
                    filings += piece
                    pbar.update(len(piece))
                except Exception as e:
                    print(e)
                    break
            return filings


def get_contributions(args, token=None, use_tor=False):
    BASE_URL = 'https://lda.senate.gov/api/v1/contributions/'
    url = BASE_URL
    if len(args):
        url += '?'
        url += '&'.join('{}={}'.format(key, args[key]) for key in args)

    with (util.tor.get_session()
          if use_tor else requests.session()) as session:
        if token is None:
            token = token_from_environment()
        try:
            response = api_call(url, token, session).json()
        except Exception as e:
            print('Could not process contributions from {}'.format(url))
            print(e)
            return []

        count = response['count']
        with tqdm(total=count) as pbar:
            piece = response['results']
            contributions = piece
            pbar.update(len(piece))
            while 'next' in response and response['next']:
                raw_response = api_call(response['next'], token, session)
                try:
                    response = raw_response.json()
                    piece = response['results']
                    contributions += piece
                    pbar.update(len(piece))
                except Exception as e:
                    print(e)
                    break
        return contributions


def rest_filings_json_to_csv(rest_json_file, export_csv):
    df = pd.read_json(rest_json_file)

    df = df.drop(columns=[
        'url', 'filing_type_display', 'filing_period_display',
        'filing_document_url', 'filing_document_content_type',
        'expenses_method_display'
    ])

    def lobbyist_ids(activity):
        ids = set()
        for item in activity:
            for lobbyist in item['lobbyists']:
                ids.add(lobbyist['lobbyist']['id'])
        return json.dumps(sorted(list(ids)))

    df['lobbyist_ids'] = df['lobbying_activities'].apply(lobbyist_ids)

    json_columns = [
        'registrant', 'client', 'lobbying_activities',
        'conviction_disclosures', 'foreign_entities',
        'affiliated_organizations'
    ]

    date_columns = ['dt_posted', 'termination_date']

    for column in date_columns:
        df[column] = pd.to_datetime(df[column], errors='coerce')

    df = util.postgres.preprocess_dataframe(
        df, json_columns=json_columns, unique_columns=FILINGS_UNIQUE_COLUMNS)

    columns = list(FILINGS_TABLE_TYPES.keys())
    util.postgres.df_to_csv(df, export_csv, columns=columns)


def rest_contributions_json_to_csv(rest_json_file, export_csv):
    df = pd.read_json(rest_json_file)

    df = df.drop(columns=[
        'url', 'filing_type_display', 'filing_period_display',
        'filing_document_url', 'filing_document_content_type',
        'filer_type_display', 'state', 'country', 'no_contributions'
    ])

    json_columns = ['registrant', 'lobbyist', 'pacs', 'contribution_items']

    date_columns = ['dt_posted']

    for column in date_columns:
        df[column] = pd.to_datetime(df[column], errors='coerce')

    df = util.postgres.preprocess_dataframe(
        df,
        json_columns=json_columns,
        unique_columns=CONTRIBUTIONS_UNIQUE_COLUMNS)

    util.postgres.df_to_csv(df, export_csv)


def retrieve_filings(start_date=None,
                     end_date=None,
                     token=None,
                     use_tor=False,
                     output_json=None,
                     output_csv=OUTPUT_FILINGS_CSV,
                     window_days=31):
    if start_date is None and end_date is None:
        today = datetime.date.today()
        start_year, start_month, start_day = util.dates.earlier_day(
            today.year, today.month, today.day, window_days)
        start_date = util.dates.tuple_to_date(start_year, start_month,
                                              start_day)

    args = {}
    if start_date is not None:
        args['filing_dt_posted_after'] = start_date
    if end_date is not None:
        args['filing_dt_posted_before'] = end_date
    data = get_filings(args, token, use_tor=use_tor)

    def incorporate_search_item(search_items, item):
        item = str(item)
        if item.startswith('https://lda.senate.gov/api/'):
            return
        search_items.append(item.lower())

    # Drop the null and empty JSON fields and add the text search field.
    simple_json_fields = ['registrant', 'client']
    for item in data:
        search_items = []
        incorporate_search_item(search_items, item['filing_uuid'])
        incorporate_search_item(search_items, item['posted_by_name'])
        for json_field in simple_json_fields:
            obj = item[json_field]
            for key in list(obj.keys()):
                if obj[key]:
                    incorporate_search_item(search_items, obj[key])
                else:
                    del obj[key]
            item[json_field] = obj
        for org in item['affiliated_organizations']:
            for key in list(org.keys()):
                if org[key]:
                    incorporate_search_item(search_items, org[key])
                else:
                    del org[key]
        for org in item['foreign_entities']:
            for key in list(org.keys()):
                if org[key]:
                    incorporate_search_item(search_items, org[key])
                else:
                    del org[key]
        for activity in item['lobbying_activities']:
            # Only keep the government entity IDs.
            gov_ids = []
            for entity in activity['government_entities']:
                gov_id = entity['id']
                gov_name = entity['name']
                if str(gov_id) not in GOV_IDS:
                    print('  Government ID {} ({}) not in dictionary'.format(
                        gov_id, gov_name))
                elif GOV_IDS[str(gov_id)]['text'] != gov_name.strip():
                    print('  Government ID {} ({}) disagreed with {}'.format(
                        gov_id, GOV_IDS[str(gov_id)]['text'], gov_name))
                gov_ids.append(gov_id)
                incorporate_search_item(search_items, gov_name)
            activity['government_activities'] = gov_ids
            # Drop the empty subfields.
            for lobbyist_obj in activity['lobbyists']:
                if lobbyist_obj['covered_position']:
                    incorporate_search_item(search_items,
                                            lobbyist_obj['covered_position'])
                else:
                    del lobbyist_obj['covered_position']
                for key in list(lobbyist_obj['lobbyist'].keys()):
                    if lobbyist_obj['lobbyist'][key]:
                        incorporate_search_item(search_items,
                                                lobbyist_obj['lobbyist'][key])
                    else:
                        del lobbyist_obj['lobbyist'][key]
        item['search_text'] = ' '.join(search_items)

    if not output_json:
        prefix = '{}/filings'.format(LOBBYING_DATA_DIR)
        if start_date and end_date:
            output_json = '{}_{}-{}.json'.format(prefix, start_date, end_date)
        elif start_date:
            output_json = '{}_{}-.json'.format(prefix, start_date)
        elif end_date:
            output_json = '{}_-{}.json'.format(prefix, end_date)
        else:
            output_json = '{}.json'.format(prefix)

    with open(output_json, 'w') as outfile:
        json.dump(data, outfile, indent=1)

    rest_filings_json_to_csv(output_json, output_csv)

    return data


def retrieve_contributions(start_date=None,
                           end_date=None,
                           token=None,
                           use_tor=False,
                           output_json=None,
                           output_csv=OUTPUT_CONTRIBUTIONS_CSV,
                           window_days=31):
    if start_date is None and end_date is None:
        today = datetime.date.today()
        start_year, start_month, start_day = util.dates.earlier_day(
            today.year, today.month, today.day, window_days)
        start_date = util.dates.tuple_to_date(start_year, start_month,
                                              start_day)

    args = {}
    if start_date is not None:
        args['filing_dt_posted_after'] = start_date
    if end_date is not None:
        args['filing_dt_posted_before'] = end_date
    data = get_contributions(args, token, use_tor=use_tor)

    if not output_json:
        prefix = '{}/contributions'.format(LOBBYING_DATA_DIR)
        if start_date and end_date:
            output_json = '{}_{}-{}.json'.format(prefix, start_date, end_date)
        elif start_date:
            output_json = '{}_{}-.json'.format(prefix, start_date)
        elif end_date:
            output_json = '{}_-{}.json'.format(prefix, end_date)
        else:
            output_json = '{}.json'.format(prefix)

    with open(output_json, 'w') as outfile:
        json.dump(data, outfile, indent=1)

    rest_contributions_json_to_csv(output_json, output_csv)

    return data


def retrieve(start_date=None,
             end_date=None,
             token=None,
             use_tor=False,
             filings_csv=OUTPUT_FILINGS_CSV,
             contributions_csv=OUTPUT_CONTRIBUTIONS_CSV,
             window_days=31):
    result = {}
    result['filings'] = retrieve_filings(start_date,
                                         end_date,
                                         token,
                                         use_tor=use_tor,
                                         output_json=None,
                                         output_csv=filings_csv,
                                         window_days=window_days)
    result['contributions'] = retrieve_contributions(
        start_date,
        end_date,
        token,
        use_tor=use_tor,
        output_json=None,
        output_csv=contributions_csv,
        window_days=window_days)
    return result


def postgres_create_filings(user=None,
                            password=None,
                            dbname=None,
                            port=None,
                            host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            FILINGS_TABLE_NAME,
            FILINGS_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (filing_uuid)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple', search_text)
);
    '''.format(FILINGS_TABLE_NAME, FILINGS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_client_name_index ON {} (lower(client->>'name'));
    '''.format(FILINGS_TABLE_NAME, FILINGS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_registrant_name_index ON {} (
  lower(registrant->>'name')
);
    '''.format(FILINGS_TABLE_NAME, FILINGS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_registrant_contact_name_index ON {} (
  lower(registrant->>'contact_name')
);
    '''.format(FILINGS_TABLE_NAME, FILINGS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_lobbyist_ids_index ON {} USING GIN (
  (lobbyist_ids) jsonb_path_ops
);
    '''.format(FILINGS_TABLE_NAME, FILINGS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_affiliated_index ON {} USING GIN (
    (affiliated_organizations) jsonb_path_ops
);
    '''.format(FILINGS_TABLE_NAME, FILINGS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_foreign_index ON {} USING GIN (
    (foreign_entities) jsonb_path_ops
);
    '''.format(FILINGS_TABLE_NAME, FILINGS_TABLE_NAME))


def postgres_update_filings(export_csv=OUTPUT_FILINGS_CSV,
                            user=None,
                            password=None,
                            dbname=None,
                            port=None,
                            host=None,
                            vacuum=True):
    postgres_create_filings(user, password, dbname, port, host)
    util.postgres.update(FILINGS_TABLE_NAME,
                         FILINGS_TABLE_TYPES,
                         FILINGS_UNIQUE_COLUMNS,
                         FILINGS_FORCE_NULL_COLUMNS,
                         FILINGS_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_create_contributions(user=None,
                                  password=None,
                                  dbname=None,
                                  port=None,
                                  host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            CONTRIBUTIONS_TABLE_NAME,
            CONTRIBUTIONS_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (filing_uuid)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(filing_uuid),              '') || ' ' ||
    coalesce(lower(contact_name),             '') || ' ' ||
    coalesce(lower(comments),                 '') || ' ' ||
    coalesce(lower(address_1),                '') || ' ' ||
    coalesce(lower(address_2),                '') || ' ' ||
    coalesce(lower(city),                     '') || ' ' ||
    coalesce(lower(state_display),            '') || ' ' ||
    coalesce(lower(zip),                      '') || ' ' ||
    coalesce(lower(country_display),          '') || ' ' ||
    coalesce(lower(registrant::TEXT),         '') || ' ' ||
    coalesce(lower(lobbyist::TEXT),           '') || ' ' ||
    coalesce(lower(pacs::TEXT),               '') || ' ' ||
    coalesce(lower(contribution_items::TEXT), '')
  )
);
    '''.format(CONTRIBUTIONS_TABLE_NAME, CONTRIBUTIONS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_contact_name_index ON {} (lower(contact_name));
    '''.format(CONTRIBUTIONS_TABLE_NAME, CONTRIBUTIONS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_registrant_name_index ON {} (
  lower(registrant->>'name')
);
    '''.format(CONTRIBUTIONS_TABLE_NAME, CONTRIBUTIONS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_registrant_contact_index ON {} (
  lower(registrant->>'contact_name')
);
    '''.format(CONTRIBUTIONS_TABLE_NAME, CONTRIBUTIONS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_registrant_id_index ON {} ((registrant->>'id'));
    '''.format(CONTRIBUTIONS_TABLE_NAME, CONTRIBUTIONS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_lobbyist_id_index ON {} ((lobbyist->>'id'));
    '''.format(CONTRIBUTIONS_TABLE_NAME, CONTRIBUTIONS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_contributions_index ON {} USING GIN (
  (lower(contribution_items::TEXT)::JSONB) jsonb_path_ops
);
    '''.format(CONTRIBUTIONS_TABLE_NAME, CONTRIBUTIONS_TABLE_NAME))


def postgres_update_contributions(export_csv=OUTPUT_CONTRIBUTIONS_CSV,
                                  user=None,
                                  password=None,
                                  dbname=None,
                                  port=None,
                                  host=None,
                                  vacuum=True):
    postgres_create_contributions(user, password, dbname, port, host)
    util.postgres.update(CONTRIBUTIONS_TABLE_NAME,
                         CONTRIBUTIONS_TABLE_TYPES,
                         CONTRIBUTIONS_UNIQUE_COLUMNS,
                         CONTRIBUTIONS_FORCE_NULL_COLUMNS,
                         CONTRIBUTIONS_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_update(filings_csv=OUTPUT_FILINGS_CSV,
                    contributions_csv=OUTPUT_CONTRIBUTIONS_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_update_filings(filings_csv,
                            user=user,
                            password=password,
                            dbname=dbname,
                            port=port,
                            host=host,
                            vacuum=vacuum)
    postgres_update_contributions(contributions_csv,
                                  user=user,
                                  password=password,
                                  dbname=dbname,
                                  port=port,
                                  host=host,
                                  vacuum=vacuum)


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['us', 'central', 'lobbying', 'opr'],
                                    entities)
