#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
#  https://www.fec.gov/data/browse-data/?tab=bulk-data
#
import csv
import json
import os
import re

import util.entities
import util.postgres
import util.zip

from util.entities import canonicalize
from util.postgres import sanitize_entry as sanitize

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
DOWNLOAD_DIR = os.path.join(DATA_DIR, 'us_fec')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

OUTPUT_CAND_CSV = '{}/us_fec_cand.csv'.format(EXPORT_DIR)
OUTPUT_CAND_JSON = '{}/us_fec_cand.json'.format(EXPORT_DIR)

OUTPUT_COMM_CSV = '{}/us_fec_comm.csv'.format(EXPORT_DIR)
OUTPUT_COMM_JSON = '{}/us_fec_comm.json'.format(EXPORT_DIR)

OUTPUT_INDV_CSV = '{}/us_fec_indv.csv'.format(EXPORT_DIR)
OUTPUT_INDV_JSON = '{}/us_fec_indv.json'.format(EXPORT_DIR)

CAND_IN_COLUMNS = [
    'candidate_id', 'candidate_name', 'candidate_party_affiliation',
    'election_year', 'candidate_office_state', 'candidate_office',
    'candidate_office_district', 'candidate_incumbant_status',
    'candidate_status', 'candidate_principal_campaign_committee',
    'candidate_street1', 'candidate_street2', 'candidate_city',
    'candidate_state', 'candidate_zip'
]
CAND_OUT_COLUMNS = CAND_IN_COLUMNS + ['cycle']
CAND_STRING_COLUMNS = [
    'candidate_name', 'candidate_office_state', 'candidate_office',
    'candidate_office_district', 'candidate_street1', 'candidate_street2',
    'candidate_city', 'candidate_state'
]
CAND_TABLE_TYPES = {
    'candidate_id': 'VARCHAR(9)',
    'candidate_name': 'VARCHAR(200)',
    'candidate_party_affiliation': 'VARCHAR(3)',
    'election_year': 'NUMERIC(4)',
    'candidate_office_state': 'VARCHAR(2)',
    'candidate_office': 'VARCHAR(1)',
    'candidate_office_district': 'VARCHAR(2)',
    'candidate_incumbant_status': 'VARCHAR(1)',
    'candidate_status': 'VARCHAR(1)',
    'candidate_principal_campaign_committee': 'VARCHAR(9)',
    'candidate_street1': 'VARCHAR(34)',
    'candidate_street2': 'VARCHAR(34)',
    'candidate_city': 'VARCHAR(30)',
    'candidate_state': 'VARCHAR(2)',
    'candidate_zip': 'VARCHAR(9)',
    'cycle': 'INT'
}
CAND_FORCE_NULL_COLUMNS = []
CAND_UNIQUE_COLUMNS = ['candidate_id', 'cycle']
CAND_TABLE_NAME = 'us_fec_cand'
CAND_TABLE_TMP_NAME = '{}_tmp'.format(CAND_TABLE_NAME)

COMM_IN_COLUMNS = [
    'committee_id', 'committee_name', 'treasurer_name', 'committee_street1',
    'committee_street2', 'committee_city', 'committee_state', 'committee_zip',
    'committee_designation', 'committee_type', 'committee_party_affiliation',
    'committee_filing_frequency', 'interest_group_category',
    'connected_organization_name', 'candidate_id'
]
COMM_OUT_COLUMNS = COMM_IN_COLUMNS + ['cycle']
COMM_STRING_COLUMNS = [
    'committee_name', 'treasurer_name', 'committee_street1',
    'committee_street2', 'committee_city', 'connected_organization_name'
]
COMM_TABLE_TYPES = {
    'committee_id': 'VARCHAR(9)',
    'committee_name': 'VARCHAR(200)',
    'treasurer_name': 'VARCHAR(90)',
    'committee_street1': 'VARCHAR(34)',
    'committee_street2': 'VARCHAR(34)',
    'committee_city': 'VARCHAR(30)',
    'committee_state': 'VARCHAR(2)',
    'committee_zip': 'VARCHAR(9)',
    'committee_designation': 'VARCHAR(1)',
    'committee_type': 'VARCHAR(1)',
    'committee_party_affiliation': 'VARCHAR(3)',
    'committee_filing_frequency': 'VARCHAR(1)',
    'interest_group_category': 'VARCHAR(1)',
    'connected_organization_name': 'VARCHAR(200)',
    'candidate_id': 'VARCHAR(9)',
    'cycle': 'INT'
}
COMM_FORCE_NULL_COLUMNS = []
COMM_UNIQUE_COLUMNS = ['committee_id', 'cycle']
COMM_TABLE_NAME = 'us_fec_comm'
COMM_TABLE_TMP_NAME = '{}_tmp'.format(COMM_TABLE_NAME)

INDV_IN_COLUMNS = [
    'filer_id', 'amendment_indicator', 'report_type',
    'primary_general_indicator', 'image_number', 'transaction_type',
    'entity_type', 'contributor_name', 'city', 'state', 'zip', 'employer',
    'occupation', 'transaction_date', 'transaction_amount', 'other_id',
    'transaction_id', 'file_number', 'memo_code', 'memo_text',
    'fec_record_number'
]
INDV_OUT_COLUMNS = INDV_IN_COLUMNS + ['cycle', 'entities']

INDV_STRING_COLUMNS = [
    'report_type', 'contributor_name', 'city', 'state', 'zip', 'employer',
    'occupation', 'memo_text'
]

INDV_DATE_COLUMNS = ['transaction_date']

INDV_TABLE_TYPES = {
    'filer_id': 'VARCHAR(9)',
    'amendment_indicator': 'VARCHAR(1)',
    'report_type': 'VARCHAR(3)',
    'primary_general_indicator': 'VARCHAR(5)',
    'image_number': 'VARCHAR(18)',
    'transaction_type': 'VARCHAR(3)',
    'entity_type': 'VARCHAR(3)',
    'contributor_name': 'VARCHAR(200)',
    'city': 'VARCHAR(30)',
    'state': 'VARCHAR(2)',
    'zip': 'VARCHAR(9)',
    'employer': 'VARCHAR(38)',
    'occupation': 'VARCHAR(38)',
    'transaction_date': 'DATE',
    'transaction_amount': 'NUMERIC(14,2)',
    'other_id': 'VARCHAR(9)',
    'transaction_id': 'VARCHAR(32)',
    'file_number': 'NUMERIC(22)',
    'memo_code': 'VARCHAR(1)',
    'memo_text': 'VARCHAR(100)',
    'fec_record_number': 'NUMERIC(19)',
    'cycle': 'INT',
    'entities': 'JSONB'
}

INDV_FORCE_NULL_COLUMNS = ['transaction_date']
INDV_UNIQUE_COLUMNS = ['fec_record_number', 'cycle']
INDV_TABLE_NAME = 'us_fec_indv'
INDV_TABLE_TMP_NAME = '{}_tmp'.format(INDV_TABLE_NAME)

if not os.path.exists(DOWNLOAD_DIR):
    os.mkdir(DOWNLOAD_DIR)
if not os.path.exists(EXPORT_DIR):
    os.mkdir(EXPORT_DIR)


def format_datetime(date):
    if len(date) == 8:
        month = date[0:2]
        day = date[2:4]
        year = date[4:8]
        return '{}-{}-{}'.format(year, month, day)
    else:
        return None


def process_candidates(cand_txt,
                       cycle,
                       output_json=OUTPUT_CAND_JSON,
                       output_csv=OUTPUT_CAND_CSV):
    committee_ids = {}
    with open(cand_txt, newline='') as cand_csv, open(output_csv,
                                                      'w',
                                                      newline='') as outcsv:
        reader = csv.reader(cand_csv, delimiter='|', quotechar=None)
        writer = csv.DictWriter(outcsv,
                                delimiter='\t',
                                quotechar='"',
                                escapechar='\\',
                                doublequote=False,
                                quoting=csv.QUOTE_ALL,
                                fieldnames=CAND_OUT_COLUMNS)
        writer.writeheader()
        with open(output_json, 'w') as outjson:
            for row in reader:
                json_row = {
                    CAND_IN_COLUMNS[index]: row[index]
                    for index in range(len(row))
                }
                json_row['cycle'] = cycle
                for string_column in CAND_STRING_COLUMNS:
                    json_row[string_column] = sanitize(
                        util.entities.canonicalize(json_row[string_column]))
                committee_id = json_row[
                    'candidate_principal_campaign_committee']
                if committee_id:
                    if committee_id in committee_ids:
                        committee_ids[committee_id].append(json_row)
                    else:
                        committee_ids[committee_id] = [json_row]
                outjson.write(json.dumps(json_row) + '\n')
                writer.writerow(json_row)
    return committee_ids


def process_committees(comm_txt,
                       cycle,
                       output_json=OUTPUT_COMM_JSON,
                       output_csv=OUTPUT_COMM_CSV):
    committee_ids = {}
    with open(comm_txt, newline='') as comm_csv, open(output_csv,
                                                      'w',
                                                      newline='') as outcsv:
        reader = csv.reader(comm_csv, delimiter='|', quotechar=None)
        writer = csv.DictWriter(outcsv,
                                delimiter='\t',
                                quotechar='"',
                                escapechar='\\',
                                doublequote=False,
                                quoting=csv.QUOTE_ALL,
                                fieldnames=COMM_OUT_COLUMNS)
        writer.writeheader()
        with open(output_json, 'w') as outjson:
            for row in reader:
                json_row = {
                    COMM_IN_COLUMNS[index]: row[index]
                    for index in range(len(row))
                }
                json_row['cycle'] = cycle
                for string_column in COMM_STRING_COLUMNS:
                    json_row[string_column] = sanitize(
                        util.entities.canonicalize(json_row[string_column]))
                committee_id = json_row['committee_id']
                if committee_id:
                    if committee_id in committee_ids:
                        committee_ids[committee_id].append(json_row)
                    else:
                        committee_ids[committee_id] = [json_row]
                outjson.write(json.dumps(json_row) + '\n')
                writer.writerow(json_row)
    return committee_ids


def incorporate_entity(entities,
                       name,
                       city=None,
                       state=None,
                       employer=None,
                       occupation=None,
                       candidate=False):
    name = canonicalize(sanitize(name))

    if candidate:
        contributorObj = {'candidate': name}
        entities.add(json.dumps(contributorObj))
        return

    employer = canonicalize(sanitize(employer)) if employer else ''
    if name:
        contributorObj = {'name': name}
        if city and state:
            contributorObj['city'] = '{}, {}'.format(sanitize(city.lower()),
                                                     sanitize(state.lower()))
        elif city:
            contributorObj['city'] = sanitize(city.lower())
        elif state:
            contributorObj['city'] = sanitize(state.lower())
        if employer:
            contributorObj['employer'] = employer
        entities.add(json.dumps(contributorObj))

    if employer:
        employerObj = {'name': employer}
        if name:
            employerObj['contributor'] = name
        entities.add(json.dumps(employerObj))


def process_individual(indv_txt,
                       cycle,
                       output_json=OUTPUT_INDV_JSON,
                       output_csv=OUTPUT_INDV_CSV,
                       candidate_map={},
                       committee_map={}):
    employers = set()
    contributors = set()
    with open(indv_txt, newline='') as indv_csv, open(output_csv,
                                                      'w',
                                                      newline='') as outcsv:
        reader = csv.reader(indv_csv, delimiter='|', quotechar=None)
        writer = csv.DictWriter(outcsv,
                                delimiter='\t',
                                quotechar='"',
                                escapechar='\\',
                                doublequote=False,
                                quoting=csv.QUOTE_ALL,
                                fieldnames=INDV_OUT_COLUMNS)
        writer.writeheader()
        with open(output_json, 'w') as outjson:
            for row in reader:
                json_row = {
                    INDV_IN_COLUMNS[index]: row[index]
                    for index in range(len(row))
                }
                json_row['cycle'] = cycle
                for string_column in INDV_STRING_COLUMNS:
                    json_row[string_column] = sanitize(
                        util.entities.canonicalize(json_row[string_column]))
                json_row['transaction_date'] = format_datetime(
                    json_row['transaction_date'])

                entities = set()
                incorporate_entity(entities,
                                   json_row['contributor_name'],
                                   city=json_row['city'],
                                   state=json_row['state'],
                                   employer=json_row['employer'],
                                   occupation=json_row['occupation'])
                filer_id = json_row['filer_id']
                if filer_id in candidate_map:
                    for candidate in candidate_map[filer_id]:
                        incorporate_entity(entities,
                                           candidate['candidate_name'],
                                           candidate=True)
                if filer_id in committee_map:
                    for committee in committee_map[filer_id]:
                        incorporate_entity(entities,
                                           committee['committee_name'])
                        incorporate_entity(entities,
                                           committee['treasurer_name'])
                        incorporate_entity(
                            entities, committee['connected_organization_name'])
                json_row['entities'] = json.dumps(
                    [json.loads(x) for x in sorted(entities)])

                contributors.add(json_row['contributor_name'])
                employers.add(json_row['employer'])
                outjson.write(json.dumps(json_row) + '\n')

                # Escape the backslashes in the CSV export of entities.
                # TODO(Jack Poulson): Consider more escapes.
                json_row['entities'] = re.sub(r'\\', r'\\\\',
                                              json_row['entities'])
                writer.writerow(json_row)

    return employers, contributors


def candidate_zip_url(cycle):
    return 'https://www.fec.gov/files/bulk-downloads/{}/cn{}.zip'.format(
        cycle,
        str(cycle)[2:])


def committee_zip_url(cycle):
    return 'https://www.fec.gov/files/bulk-downloads/{}/cm{}.zip'.format(
        cycle,
        str(cycle)[2:])


def individual_zip_url(cycle):
    return 'https://www.fec.gov/files/bulk-downloads/{}/indiv{}.zip'.format(
        cycle,
        str(cycle)[2:])


def process_cycle(cycle,
                  output_cand_json=OUTPUT_CAND_JSON,
                  output_cand_csv=OUTPUT_CAND_CSV,
                  output_comm_json=OUTPUT_COMM_JSON,
                  output_comm_csv=OUTPUT_COMM_CSV,
                  output_indv_json=OUTPUT_INDV_JSON,
                  output_indv_csv=OUTPUT_INDV_CSV,
                  preexisting_candidate_file=False,
                  preexisting_committee_file=False,
                  preexisting_individual_file=False,
                  verbose=False):
    if not preexisting_candidate_file:
        cand_zip_url = candidate_zip_url(cycle)
        files_to_extract = ['cn.txt']
        if verbose:
            print('Unzipping candidates')
        util.zip.download_and_unpack(cand_zip_url,
                                     DOWNLOAD_DIR,
                                     files_to_extract,
                                     verbose=verbose)
        if verbose:
            print('Unzipped candidates.')
    cand_csv = '{}/cn.txt'.format(DOWNLOAD_DIR)
    print('Generating {}'.format(output_cand_json))
    candidate_map = process_candidates(cand_csv, cycle, output_cand_json,
                                       output_cand_csv)

    if not preexisting_committee_file:
        comm_zip_url = committee_zip_url(cycle)
        files_to_extract = ['cm.txt']
        if verbose:
            print('Unzipping committees')
        util.zip.download_and_unpack(comm_zip_url,
                                     DOWNLOAD_DIR,
                                     files_to_extract,
                                     verbose=verbose)
        if verbose:
            print('Unzipped committees.')
    comm_csv = '{}/cm.txt'.format(DOWNLOAD_DIR)
    print('Generating {}'.format(output_comm_json))
    committee_map = process_committees(comm_csv, cycle, output_comm_json,
                                       output_comm_csv)

    if not preexisting_individual_file:
        indv_zip_url = individual_zip_url(cycle)
        files_to_extract = ['itcont.txt']
        if verbose:
            print('Unzipping individual contributions')
        util.zip.download_and_unpack(indv_zip_url,
                                     DOWNLOAD_DIR,
                                     files_to_extract,
                                     verbose=verbose)
        if verbose:
            print('Unzipped contributions.')
    indv_csv = '{}/itcont.txt'.format(DOWNLOAD_DIR)
    print('Generating {}'.format(output_indv_json))
    process_individual(indv_csv,
                       cycle,
                       output_indv_json,
                       output_indv_csv,
                       candidate_map=candidate_map,
                       committee_map=committee_map)


def process_existing_files(year):
    '''A helper routine for processing already-downloaded files.'''
    committee_map = process_committees('data/us_fec/cm.txt', year)
    candidate_map = process_candidates('data/us_fec/cn.txt', year)
    process_individual('data/us_fec/itcont.txt',
                       year,
                       candidate_map=candidate_map,
                       committee_map=committee_map)
    postgres_update_candidates()
    postgres_update_committees()
    postgres_update_individual()


def postgres_create_candidates(user=None,
                               password=None,
                               dbname=None,
                               port=None,
                               host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            CAND_TABLE_NAME,
            CAND_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (candidate_id, cycle)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_left_join_index ON {} (
  candidate_principal_campaign_committee, cycle
);
    '''.format(CAND_TABLE_NAME, CAND_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(candidate_id),                           '') || ' ' ||
    coalesce(candidate_name,                                '') || ' ' ||
    coalesce(election_year::TEXT,                           '') || ' ' ||
    coalesce(candidate_office_state,                        '') || ' ' ||
    coalesce(lower(candidate_principal_campaign_committee), '') || ' ' ||
    coalesce(candidate_street1,                             '') || ' ' ||
    coalesce(candidate_street2,                             '') || ' ' ||
    coalesce(candidate_city,                                '') || ' ' ||
    coalesce(candidate_state,                               '') || ' ' ||
    coalesce(candidate_zip,                                 '')
  )
);
    '''.format(CAND_TABLE_NAME, CAND_TABLE_NAME))


def postgres_create_committees(user=None,
                               password=None,
                               dbname=None,
                               port=None,
                               host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            COMM_TABLE_NAME,
            COMM_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (committee_id, cycle)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_left_join_index ON {} (
  committee_id, cycle
);
    '''.format(COMM_TABLE_NAME, COMM_TABLE_NAME))


def postgres_create_individual(user=None,
                               password=None,
                               dbname=None,
                               port=None,
                               host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            INDV_TABLE_NAME,
            INDV_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (fec_record_number, cycle)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_unique_data_index ON {} (
  fec_record_number, cycle
);
    '''.format(INDV_TABLE_NAME, INDV_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_left_join_index ON {} (
  filer_id, cycle
);
    '''.format(INDV_TABLE_NAME, INDV_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(contributor_name,        '') || ' ' ||
    coalesce(lower(city),             '') || ' ' ||
    coalesce(employer,                '') || ' ' ||
    coalesce(lower(occupation),       '') || ' ' ||
    coalesce(lower(transaction_id) ,  '') || ' ' ||
    coalesce(file_number::TEXT,       '') || ' ' ||
    coalesce(memo_text,               '') || ' ' ||
    coalesce(fec_record_number::TEXT, '') || ' ' ||
    coalesce(entities::TEXT,          '')
  )
);
    '''.format(INDV_TABLE_NAME, INDV_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_entities_index ON {} USING GIN (
  (entities) jsonb_path_ops
);
    '''.format(INDV_TABLE_NAME, INDV_TABLE_NAME))


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    postgres_create_candidates(user=user,
                               password=password,
                               dbname=dbname,
                               port=port,
                               host=host)
    postgres_create_committees(user=user,
                               password=password,
                               dbname=dbname,
                               port=port,
                               host=host)
    postgres_create_individual(user=user,
                               password=password,
                               dbname=dbname,
                               port=port,
                               host=host)


def postgres_update_candidates(export_csv=OUTPUT_CAND_CSV,
                               user=None,
                               password=None,
                               dbname=None,
                               port=None,
                               host=None,
                               vacuum=True):
    postgres_create_candidates(user, password, dbname, port, host)
    util.postgres.update(CAND_TABLE_NAME,
                         CAND_TABLE_TYPES,
                         CAND_UNIQUE_COLUMNS,
                         CAND_FORCE_NULL_COLUMNS,
                         CAND_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_update_committees(export_csv=OUTPUT_COMM_CSV,
                               user=None,
                               password=None,
                               dbname=None,
                               port=None,
                               host=None,
                               vacuum=True,
                               num_chunks=10):
    postgres_create_committees(user, password, dbname, port, host)
    util.postgres.update(COMM_TABLE_NAME,
                         COMM_TABLE_TYPES,
                         COMM_UNIQUE_COLUMNS,
                         COMM_FORCE_NULL_COLUMNS,
                         COMM_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum,
                         num_chunks=num_chunks)


def postgres_update_individual(export_csv=OUTPUT_INDV_CSV,
                               user=None,
                               password=None,
                               dbname=None,
                               port=None,
                               host=None,
                               vacuum=True,
                               num_chunks=10,
                               start_chunk=None,
                               end_chunk=None):
    postgres_create_individual(user, password, dbname, port, host)
    util.postgres.update(INDV_TABLE_NAME,
                         INDV_TABLE_TYPES,
                         INDV_UNIQUE_COLUMNS,
                         INDV_FORCE_NULL_COLUMNS,
                         INDV_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum,
                         num_chunks=num_chunks,
                         start_chunk=start_chunk,
                         end_chunk=end_chunk)


def postgres_update(cand_export_csv=OUTPUT_CAND_CSV,
                    comm_export_csv=OUTPUT_COMM_CSV,
                    indv_export_csv=OUTPUT_INDV_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True,
                    num_chunks=10):
    postgres_update_candidates(cand_export_csv, user, password, dbname, port,
                               host, vacuum)
    postgres_update_committees(comm_export_csv, user, password, dbname, port,
                               host, vacuum)
    postgres_update_individual(indv_export_csv,
                               user,
                               password,
                               dbname,
                               port,
                               host,
                               vacuum,
                               num_chunks=num_chunks)


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return util.entities.normalization_map_helper(
        ['us', 'central', 'lobbying', 'fec'],
        entities=entities,
        object_normalization=True)


def normalizeContributor(entity, city, state, employer, normalization):
    '''Normalizes a contributor into an entity.'''
    entity = canonicalize(entity)

    cityState = ''
    if city and state:
        cityState = '{}, {}'.format(canonicalize(city), canonicalize(state))
    elif city:
        cityState = canonicalize(city)
    elif state:
        cityState = canonicalize(state)

    employer = canonicalize(employer)

    key_list = [
        json.dumps({
            'name': entity,
            'city': cityState,
            'employer': employer
        }),
        json.dumps({
            'name': entity,
            'city': cityState
        }),
        json.dumps({
            'name': entity,
            'employer': employer
        }),
        json.dumps({'name': entity})
    ]
    for key in key_list:
        if key in normalization:
            return normalization[key]
    return entity


def normalizeEmployer(entity, contributor, normalization):
    '''Normalizes an employer into an entity.'''
    entity = canonicalize(entity)

    contributor = canonicalize(contributor)

    key_list = [
        json.dumps({
            'name': entity,
            'contributor': contributor
        }),
        json.dumps({'name': entity})
    ]
    for key in key_list:
        if key in normalization:
            return normalization[key]
    return entity


def normalizeCandidate(entity, normalization):
    '''Normalizes a candidate into an entity.'''
    entity = canonicalize(entity)

    key_list = [json.dumps({'name': entity})]
    for key in key_list:
        if key in normalization:
            return normalization[key]
    return entity


def normalizeCommittee(entity, normalization):
    '''Normalizes a committee into an entity.'''
    entity = canonicalize(entity)

    key_list = [json.dumps({'name': entity})]
    for key in key_list:
        if key in normalization:
            return normalization[key]
    return entity


def normalizeTreasurer(entity, normalization):
    '''Normalizes a treasurer into an entity.'''
    entity = canonicalize(entity)

    key_list = [json.dumps({'name': entity})]
    for key in key_list:
        if key in normalization:
            return normalization[key]
    return entity
