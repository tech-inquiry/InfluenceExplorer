#
# Copyright (c) 2020-2024 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import jsonlines
import os
import pandas as pd

import util.postgres

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')
DATA_DIR = os.path.join(WORKSTATION_DIR, '../data')

OUTPUT_CSV = '{}/us_teixeira.csv'.format(EXPORT_DIR)

TABLE_TYPES = {'reference_id': 'TEXT', 'text': 'TEXT'}
UNIQUE_COLUMNS = ['reference_id']
FORCE_NULL_COLUMNS = None
TABLE_NAME = 'us_teixeira'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)

def load_jsonlines(filename):
    data = []
    if os.path.exists(filename):
        with jsonlines.open(filename) as reader:
            for obj in reader:
                data.append(obj)
    else:
        print('Could not load {}'.format(filename))
    return data


def export(data=None, output_csv=OUTPUT_CSV):
    '''Writes out a CSV from an array of the Teixeira leaks.'''
    if data == None:
        data = load_jsonlines(os.path.join(DATA_DIR, 'teixeira.jsonl'))
    df = pd.DataFrame(data)
    util.postgres.df_to_csv(df, output_csv)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute('''
CREATE TABLE IF NOT EXISTS us_teixeira(
  id SERIAL PRIMARY KEY,
  reference_id TEXT,
  text TEXT,
  CONSTRAINT unique_us_teixeira UNIQUE (reference_id)
);

CREATE INDEX IF NOT EXISTS us_teixeira_reference_id_index ON
  us_teixeira (reference_id);

CREATE INDEX IF NOT EXISTS us_teixeira_search_index ON
  us_teixeira USING GIN (to_tsvector('simple', lower(text)));
    ''')


def postgres_update(export_csv=OUTPUT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
