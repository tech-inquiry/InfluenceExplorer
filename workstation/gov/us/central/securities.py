'''
Copyright (c) 2020 Jack Poulson <jack@techinquiry.org>

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

PySEC is a utility for mapping company CIKs into the URL containing their SEC
data.

The original version of this utility made use of a BeautifulSoup scraper, but
the SEC has since provided a standardized API such that each CIK's data is
available at:

    https://data.sec.gov/submissions/CIK${cik.zfill(10)}.json

where cik.zfill(10) is the left padding of the cik by zeroes up to a length
of 10. For example, Accenture, whose CIK is 1467373, has a URL of

    https://data.sec.gov/submissions/CIK0001467373.json

See https://www.sec.gov/edgar/sec-api-documentation for more information.
A map between CIKs and entity names is also provided at

    https://www.sec.gov/Archives/edgar/cik-lookup-data.txt

The primary complication is that a User Agent must now be specified in order to
prevent the SEC from blocking your request.

You can retrieve data via queries of the form:

  >>> import gov.us.central.securities as sec
  >>> info = sec.cik_to_json(cik='1467373',
  ...     user_agent='Your Org contact@yourwebsite.com')

'''
import gc
import io
import json
import os
import pandas as pd
import requests
import time

from tqdm import tqdm

import util.postgres

COLUMN_RENAMES = {
    'entityType': 'entity_type',
    'sicDescription': 'sic_description',
    'insiderTransactionForOwnerExists': 'insider_owner_transaction',
    'insiderTransactionForIssuerExists': 'insider_issuer_transaction',
    'investorWebsite': 'investor_website',
    'fiscalYearEnd': 'fiscal_year_end',
    'stateOfIncorporation': 'state_of_incorporation',
    'stateOfIncorporationDescription': 'state_of_incorporation_description',
    'formerNames': 'former_names'
}
COLUMN_INVERSE_RENAMES = {v: k for k, v in COLUMN_RENAMES.items()}

META_TABLE_TYPES = {
    'cik': 'VARCHAR(15)',
    'entity_type': 'VARCHAR(15)',
    'sic': 'VARCHAR(15)',
    'sic_description': 'VARCHAR(255)',
    'insider_owner_transaction': 'BOOLEAN',
    'insider_issuer_transaction': 'BOOLEAN',
    'name': 'VARCHAR(255)',
    'tickers': 'JSONB',
    'exchanges': 'JSONB',
    'ein': 'VARCHAR(15)',
    'description': 'VARCHAR(255)',
    'website': 'VARCHAR(255)',
    'category': 'VARCHAR(255)',
    'fiscal_year_end': 'VARCHAR(15)',
    'state_of_incorporation': 'VARCHAR(7)',
    'state_of_incorporation_description': 'VARCHAR(63)',
    'addresses': 'JSONB',
    'phone': 'VARCHAR(63)',
    'flags': 'VARCHAR(15)',
    'former_names': 'JSONB'
}
META_UNIQUE_COLUMNS = ['cik']
META_JSON_COLUMNS = ['tickers', 'exchanges', 'addresses', 'former_names']
META_FORCE_NULL_COLUMNS = ['tickers', 'exchanges', 'addresses']
META_TABLE_NAME = 'us_securities_meta'
META_TABLE_TMP_NAME = '{}_tmp'.format(META_TABLE_NAME)

FILING_TABLE_TYPES = {
    'cik': 'VARCHAR(15)',
    'accession': 'VARCHAR(31)',
    'filing_date': 'DATE',
    'report_date': 'DATE',
    'acceptance_date_time': 'TIMESTAMP',
    'act': 'VARCHAR(7)',
    'form': 'VARCHAR(31)',
    'file_number': 'VARCHAR(63)',
    'film_number': 'VARCHAR(63)',
    'items': 'VARCHAR(63)',
    'size': 'BIGINT',
    'is_xbrl': 'BOOLEAN',
    'is_inline_xbrl': 'BOOLEAN',
    'primary_document': 'VARCHAR(63)',
    'primary_doc_description': 'VARCHAR(127)'
}
FILING_UNIQUE_COLUMNS = ['cik', 'accession', 'primary_document']
FILING_JSON_COLUMNS = []
FILING_FORCE_NULL_COLUMNS = [
    'filing_date', 'report_date', 'acceptance_date_time'
]
FILING_TABLE_NAME = 'us_securities_filing'
FILING_TABLE_TMP_NAME = '{}_tmp'.format(FILING_TABLE_NAME)

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')
SHARED_DIR = os.path.join(WORKSTATION_DIR, '..')
SHARED_DATA_DIR = os.path.join(SHARED_DIR, 'data')

ENTITIES_FILE = '{}/entities.json'.format(SHARED_DATA_DIR)

OVERALL_JSON = '{}/us_securities.json'.format(DATA_DIR)

META_CSV = '{}/{}.csv'.format(EXPORT_DIR, META_TABLE_NAME)
FILING_CSV = '{}/{}.csv'.format(EXPORT_DIR, FILING_TABLE_NAME)


def cik_to_json_url(cik):
    return 'https://data.sec.gov/submissions/CIK{}.json'.format(cik.zfill(10))


def cik_to_json(cik, user_agent):
    headers = {'User-Agent': user_agent}
    url = cik_to_json_url(cik)
    response = requests.get(url, headers=headers)
    try:
        data = response.json()
        return data
    except Exception as e:
        print('Could not retrieve JSON SEC data for CIK {}'.format(cik))
        return None


def company_info_url(cik, count=100, start=0):
    ''' Returns a URL for querying SEC documents.'''
    return '{}&CIK={}&type=&dateb=&owner=include&start={}&count={}'.format(
        BASE_SEC_URL, cik, start, count)


def filing_url(cik, accession_number, primary_document):
    return 'https://www.sec.gov/Archives/edgar/data/{}/{}/{}'.format(
        cik.zfill(10), accession_number.replace('-', ''), primary_document)


def retrieve(user_agent=None, max_requests_per_second=9):
    if not user_agent:
        if 'INF_EXP_USER_AGENT' in os.environ:
            user_agent = os.environ['INF_EXP_USER_AGENT']
        else:
            print(
                'Error: User-Agent was not provided either directly or through environment variable (e.g., INF_EXP_USER_AGENT="Your Company info@yourcompany.com).'
            )
            return
    if not os.path.exists(DATA_DIR):
        os.makedirs(DATA_DIR)
    if not os.path.exists(EXPORT_DIR):
        os.makedirs(EXPORT_DIR)

    data = json.load(open(ENTITIES_FILE))

    # Download all entities' data.
    entity_data = {}
    num_ciks = 0
    for vendor, profile in data.items():
        if 'cik' in profile:
            num_ciks += 1
    with tqdm(total=num_ciks) as pbar:
        for vendor, profile in data.items():
            if 'cik' in profile:
                cik = profile['cik']
                entity_data[vendor] = cik_to_json(cik, user_agent)
                pbar.update(1)
                time.sleep(1. / max_requests_per_second)

    with open(OVERALL_JSON, 'w') as outfile:
        json.dump(entity_data, outfile, indent=1)

    # Convert into a column-oriented form for loading into a DataFrame.
    # We would directly load a list of dicts but were hitting errors relating
    # to iterating of None types.
    #
    # We thus drop None items and rename columns in the conversion.
    meta_columns = list(META_TABLE_TYPES.keys())
    filing_columns = list(FILING_TABLE_TYPES.keys())
    meta = {}
    filings = {}
    for column in meta_columns:
        meta[column] = []
    for column in filing_columns:
        filings[column] = []
    for item in list(entity_data.values()):
        if item is None:
            continue

        # Append to the data for the meta table.
        for column in meta_columns:
            if column in COLUMN_INVERSE_RENAMES:
                old_column = COLUMN_INVERSE_RENAMES[column]
                meta[column].append(item[old_column])
            else:
                meta[column].append(item[column])

        # Append to the data for the filing table.
        cik = item['cik']
        recent = item['filings']['recent']
        num_recent = len(recent['accessionNumber'])
        for i in range(num_recent):
            filings['cik'].append(cik)
            filings['accession'].append(recent['accessionNumber'][i])
            filings['filing_date'].append(recent['filingDate'][i])
            filings['report_date'].append(recent['reportDate'][i])
            filings['acceptance_date_time'].append(
                recent['acceptanceDateTime'][i])
            filings['act'].append(recent['act'][i])
            filings['form'].append(recent['form'][i])
            filings['file_number'].append(recent['fileNumber'][i])
            filings['film_number'].append(recent['filmNumber'][i])
            filings['items'].append(recent['items'][i])
            filings['size'].append(recent['size'][i])
            filings['is_xbrl'].append(recent['isXBRL'][i])
            filings['is_inline_xbrl'].append(recent['isInlineXBRL'][i])
            filings['primary_document'].append(recent['primaryDocument'][i])
            filings['primary_doc_description'].append(
                recent['primaryDocDescription'][i])

    meta_df = pd.DataFrame(meta)
    meta_df = util.postgres.preprocess_dataframe(
        meta_df,
        json_columns=META_JSON_COLUMNS,
        unique_columns=META_UNIQUE_COLUMNS)
    util.postgres.df_to_csv(meta_df, META_CSV)

    filing_df = pd.DataFrame(filings)
    meta_df = util.postgres.preprocess_dataframe(
        filing_df,
        json_columns=FILING_JSON_COLUMNS,
        unique_columns=FILING_UNIQUE_COLUMNS)
    util.postgres.df_to_csv(filing_df, FILING_CSV)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(META_TABLE_NAME,
                                       META_TABLE_TYPES,
                                       serial_id=True,
                                       constraints={'unique': 'UNIQUE (cik)'}))
    cursor.execute(
        util.postgres.create_table_sql(
            FILING_TABLE_NAME,
            FILING_TABLE_TYPES,
            serial_id=True,
            constraints={
                'unique': 'UNIQUE (cik, accession, primary_document)'
            }))

    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_cik_index ON {} (cik);
    '''.format(META_TABLE_NAME, META_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_cik_index ON {} (cik);
    '''.format(FILING_TABLE_NAME, FILING_TABLE_NAME))

    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(cik,                    '') || ' ' ||
    coalesce(ein,                    '') || ' ' ||
    coalesce(lower(sic_description), '') || ' ' ||
    coalesce(lower(name),            '') || ' ' ||
    coalesce(lower(description),     '') || ' ' ||
    coalesce(lower(addresses::TEXT), '')
  )
);
    '''.format(META_TABLE_NAME, META_TABLE_NAME))


def postgres_update(meta_csv=META_CSV,
                    filing_csv=FILING_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(META_TABLE_NAME,
                         META_TABLE_TYPES,
                         META_UNIQUE_COLUMNS,
                         META_FORCE_NULL_COLUMNS,
                         META_TABLE_TMP_NAME,
                         meta_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
    util.postgres.update(FILING_TABLE_NAME,
                         FILING_TABLE_TYPES,
                         FILING_UNIQUE_COLUMNS,
                         FILING_FORCE_NULL_COLUMNS,
                         FILING_TABLE_TMP_NAME,
                         filing_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
