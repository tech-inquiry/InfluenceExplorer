#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import glob
import io
import json
import jsonlines
import mimetypes
import os
import pandas as pd
import re
import requests
import time

from collections import OrderedDict

import util.docx
import util.firefox
import util.postgres
import util.pdf

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data/us_opportunities/')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

ORIG_CSV = '{}/ContractOpportunitiesFullCSV.csv'.format(DATA_DIR)
OPPORTUNITIES_JSON = '{}/us_opportunities.json'.format(DATA_DIR)
OPPORTUNITIES_CSV = '{}/us_opportunities.csv'.format(EXPORT_DIR)

OPPORTUNITIES_INFO_JSONL = '{}/us_opportunities_info.jsonl'.format(DATA_DIR)
OPPORTUNITIES_INFO_FAILS_JSON = '{}/us_opportunities_info_fails.json'.format(
    DATA_DIR)
ATTACHMENT_FAILS_JSON = '{}/attachment_fails.json'.format(DATA_DIR)

# If no bytes have been received by a request for this many seconds, then we
# abort.
REQUEST_TIMEOUT = 2

if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)

COLUMN_RENAMES = {
    'NoticeId': 'notice_id',
    'Title': 'title',
    'Sol#': 'solicitation_number',
    'Department/Ind.Agency': 'department',
    'CGAC': 'cgac',
    'Sub-Tier': 'subtier',
    'FPDS Code': 'fpds_code',
    'Office': 'office',
    'AAC Code': 'aac_code',
    'PostedDate': 'posted_date',
    'Type': 'type',
    'BaseType': 'base_type',
    'ArchiveType': 'archive_type',
    'ArchiveDate': 'archive_date',
    'SetASideCode': 'set_aside_code',
    'SetASide': 'set_aside',
    'ResponseDeadLine': 'deadline',
    'NaicsCode': 'naics_code',
    'ClassificationCode': 'classification_code',
    'PopStreetAddress': 'pop_street_address',
    'PopCity': 'pop_city',
    'PopState': 'pop_state',
    'PopZip': 'pop_zip',
    'PopCountry': 'pop_country',
    'Active': 'active',
    'AwardNumber': 'award_number',
    'AwardDate': 'award_date',
    'Award$': 'award_amount',
    'Awardee': 'awardee',
    'PrimaryContactTitle': 'primary_contact_title',
    'PrimaryContactFullname': 'primary_contact_name',
    'PrimaryContactEmail': 'primary_contact_email',
    'PrimaryContactPhone': 'primary_contact_phone',
    'PrimaryContactFax': 'primary_contact_fax',
    'SecondaryContactTitle': 'secondary_contact_title',
    'SecondaryContactFullname': 'secondary_contact_name',
    'SecondaryContactEmail': 'secondary_contact_email',
    'SecondaryContactPhone': 'secondary_contact_phone',
    'SecondaryContactFax': 'secondary_contact_fax',
    'OrganizationType': 'organization_type',
    'State': 'state',
    'City': 'city',
    'ZipCode': 'zip',
    'CountryCode': 'country_code',
    'AdditionalInfoLink': 'additional_info_link',
    'Link': 'link',
    'Description': 'description'
}

TABLE_TYPES = {
    'notice_id': 'TEXT',
    'title': 'TEXT',
    'solicitation_number': 'TEXT',
    'department': 'TEXT',
    'cgac': 'TEXT',
    'subtier': 'TEXT',
    'fpds_code': 'TEXT',
    'office': 'TEXT',
    'aac_code': 'TEXT',
    'posted_date': 'TIMESTAMP',
    'type': 'TEXT',
    'base_type': 'TEXT',
    'archive_type': 'TEXT',
    'archive_date': 'TIMESTAMP',
    'set_aside_code': 'TEXT',
    'set_aside': 'TEXT',
    'deadline': 'TIMESTAMP',
    'naics_code': 'TEXT',
    'classification_code': 'TEXT',
    'pop_street_address': 'TEXT',
    'pop_city': 'TEXT',
    'pop_state': 'TEXT',
    'pop_zip': 'TEXT',
    'pop_country': 'TEXT',
    'active': 'TEXT',
    'award_number': 'TEXT',
    'award_date': 'TIMESTAMP',
    'award_amount': 'TEXT',
    'awardee': 'TEXT',
    'primary_contact_title': 'TEXT',
    'primary_contact_name': 'TEXT',
    'primary_contact_email': 'TEXT',
    'primary_contact_phone': 'TEXT',
    'primary_contact_fax': 'TEXT',
    'secondary_contact_title': 'TEXT',
    'secondary_contact_name': 'TEXT',
    'secondary_contact_email': 'TEXT',
    'secondary_contact_phone': 'TEXT',
    'secondary_contact_fax': 'TEXT',
    'organization_type': 'TEXT',
    'state': 'TEXT',
    'city': 'TEXT',
    'zip': 'TEXT',
    'country_code': 'TEXT',
    'additional_info_link': 'TEXT',
    'link': 'TEXT',
    'description': 'TEXT',
    'text': 'TEXT'
}

UNIQUE_COLUMNS = ['notice_id', 'title']
DATE_COLUMNS = ['posted_date', 'archive_date', 'deadline', 'award_date']
FORCE_NULL_COLUMNS = ['posted_date', 'archive_date', 'deadline', 'award_date']
TABLE_NAME = 'us_central_opportunities'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)


def load_jsonlines_dict(filename):
    data = {}
    if os.path.exists(filename):
        with jsonlines.open(filename) as reader:
            for obj in reader:
                k = list(obj.keys())[0]
                v = obj[k]
                data[k] = v
    else:
        print('Could not load {}'.format(filename))
    return data


def write_jsonlines_dict(data, filename):
    with jsonlines.open(filename, mode='w') as writer:
        for k, v in data.items():
            writer.write({k: v})


def accept_sam_conditions(browser, conditions_wait):
    ok_button = WebDriverWait(browser, conditions_wait).until(
        EC.presence_of_element_located((By.XPATH, "//button[text()='OK']")))
    ok_button.click()


def scroll_to_attachments(browser, attachments_wait):
    attachments = WebDriverWait(browser, attachments_wait).until(
        EC.presence_of_element_located(
            (By.XPATH, "//*[text()='Attachments/Links']")))
    browser.execute_script("arguments[0].scrollIntoView();", attachments)


def fill_main_text(browser, info):
    main_div = browser.find_element(By.ID, 'main-container')
    info['main_text'] = main_div.text


def fill_attachment(browser, info, link, timeout=REQUEST_TIMEOUT):
    url = link.get_attribute('href')
    if not url:
        return
    print(url)
    link_text = link.text
    print(link_text)
    info['attachments'][url] = {'title': link_text}
    link_text_components = link_text.split('.')

    text = ''
    extension = link_text_components[-1].lower() if len(
        link_text_components) else ''
    if extension == 'pdf':
        text = util.pdf.get_text_from_url(url)
    elif extension == 'docx':
        text = util.docx.get_text_from_url(url)
    elif extension == 'xlsx':
        df = pd.read_excel(url, engine='openpyxl')
        text = ' '.join(df.to_string(na_rep='').strip().split())
    else:
        print('Could not parse link text "{}"'.format(link_text))
        response = requests.get(url, timeout=timeout)
        content_type = response.headers['content-type']
        extension = mimetypes.guess_extension(content_type)
        print('Extension guess: {}'.format(extension))
        # TODO(Jack Poulson): Decide if there are any extensions we don't like
        f = io.BytesIO(response.content)
        info['attachments'][url]['text'] = util.pdf.get_text_from_filehandle(f)
        return
    info['attachments'][url]['text'] = text


def fill_attachment_info(browser, info, attachment_fails):
    # It's not clear a priori which ID to search for, so we try two.
    attachments_elems = browser.find_elements(By.ID,
                                              'opp-view-attachments-tableId')
    if len(attachments_elems):
        attachments_elem = attachments_elems[0]
    else:
        attachments_elems = browser.find_elements(By.ID, 'attachments-links')
        if len(attachments_elems):
            attachments_elem = attachments_elems[0]
        else:
            attachments_elem = None
    if attachments_elem == None:
        print('Warning: Could not find attachments.')
        return

    links = attachments_elem.find_elements(By.TAG_NAME, 'a')
    info['attachments'] = {}
    for link in links:
        try:
            fill_attachment(browser, info, link)
        except Exception as e:
            print(e)
            url = link.get_attribute('href')
            title = link.text
            print('Failed to extract from "{}" at {}'.format(title, url))
            if attachment_fails != None:
                attachment_fails[url] = title


def fill_history(browser, info, headless, attachments_wait, post_scroll_wait,
                 attachment_fails):
    history = browser.find_element(By.TAG_NAME, 'sam-history')
    links = history.find_elements(By.TAG_NAME, 'a')
    if len(links):
        info['history'] = {}
    for link in links:
        history_title = link.text
        history_url = link.get_attribute('href')
        print('Recursing on history {} at {}'.format(history_title,
                                                     history_url))
        info['history'][history_url] = {
            'title':
            history_title,
            'info':
            get_opportunity_info(history_url,
                                 browser=browser,
                                 headless=headless,
                                 attachments_wait=attachments_wait,
                                 post_scroll_wait=post_scroll_wait,
                                 is_original=False,
                                 attachment_fails=attachment_fails)
        }


def get_opportunity_info(url,
                         browser=None,
                         headless=True,
                         attachments_wait=10,
                         post_scroll_wait=1,
                         is_original=True,
                         attachment_fails=None,
                         verbose=False):
    if browser == None:
        destroy_browser = True
        browser = util.firefox.get_browser(headless=headless)
    else:
        destroy_browser = False
    browser.get(url)

    info = {}

    try:
        if is_original:
            accept_sam_conditions(browser, attachments_wait)
        scroll_to_attachments(browser, attachments_wait)
        fill_main_text(browser, info)
        time.sleep(post_scroll_wait)
        fill_attachment_info(browser, info, attachment_fails=attachment_fails)

        if is_original:
            fill_history(browser,
                         info,
                         headless=headless,
                         attachments_wait=attachments_wait,
                         post_scroll_wait=post_scroll_wait,
                         attachment_fails=attachment_fails)

    except Exception as e:
        print(e)
        return None
    finally:
        if destroy_browser:
            browser.quit()

    return info


# TODO(Jack Poulson): Add different update modes. Initially, we will focus on
# untouched urls, but later we can focus on updating too. This would require
# more sophisticated routines.
def update_opportunity_info(info_cache=OPPORTUNITIES_INFO_JSONL,
                            info_fails_cache=OPPORTUNITIES_INFO_FAILS_JSON,
                            attachment_fails_cache=ATTACHMENT_FAILS_JSON,
                            num_updates_per_save=150,
                            headless=True,
                            reuse_browser=False,
                            verbose=False):
    info = load_jsonlines_dict(info_cache)
    fail_urls = []
    attachment_fails = []

    def write_cache():
        write_jsonlines_dict(info, info_cache)
        with open(info_fails_cache, 'w') as outfile:
            json.dump(fail_urls, outfile, indent=2)
        with open(attachment_fails_cache, 'w') as outfile:
            json.dump(attachment_urls, outfile, indent=2)

    if reuse_browser:
        browser = util.firefox.get_browser(headless=headless)
    else:
        browser = None

    updates_since_save = 0
    urls = list(info.keys())
    for url in urls:
        item = info[url]
        text = string_from_info(info)
        if text:
            continue
        try:
            if verbose:
                print('Updating URL {}'.format(url))
            new_item = get_opportunity_info(url,
                                            browser=browser,
                                            headless=headless,
                                            attachment_fails=attachment_fails,
                                            verbose=verbose)
            if new_item == None:
                print('Failed to update {}'.format(url))
                fail_urls.append(url)
                continue
            info[url] = new_item

            if updates_since_save == num_updates_per_save:
                if verbose:
                    print('Writing cache to {}'.format(info_cache))
                write_cache()
                updates_since_save = 0
            else:
                updates_since_save += 1
        except Exception as e:
            print(e)
            print('Failed to update {}'.format(url))
            fail_urls.append(url)
    write_cache()


NUMBER_MATCH = re.compile(r'(\d+[.,]?\d*\s+)')
MAX_NUMBERS = 5000
MAX_STRING_LENGTH = 400000


def prepare_text(text):
    if not text:
        return text

    if len(NUMBER_MATCH.findall(text)) > MAX_NUMBERS:
        print('Replacing numbers with spaces.')
        NUMBER_MATCH.sub(r' ', text)
    if len(text) > MAX_STRING_LENGTH:
        print('Shortening string.')
        text = text[:MAX_STRING_LENGTH]
    return text.replace('\x00', ' ').replace('\u0000', ' ').strip()


def string_from_info(info):
    texts = []

    def append_text(item):
        if not isinstance(item, list):
            item = [item]
        for subitem in item:
            if isinstance(subitem, str):
                texts.append(prepare_text(subitem))
            elif isinstance(subitem, list):
                for subsubitem in subitem:
                    append_text(subsubitem)
            elif isinstance(subitem, dict):
                for subsubitem in subitem.values():
                    append_text(subsubitem)

    if 'main_text' in info:
        append_text(info['main_text'])
    if 'attachments' in info:
        for attachment in info['attachments'].values():
            if 'title' in attachment:
                append_text(attachment['title'])
            if 'text' in attachment:
                append_text(attachment['text'])
    if 'history' in info:
        for history in info['history'].values():
            if 'main_text' in history:
                append_text(history['main_text'])
            if 'attachments' in history:
                for attachment in history['attachments'].values():
                    if 'title' in attachment:
                        append_text(attachment['title'])
                    if 'text' in attachment:
                        append_text(attachment['text'])

    for text in texts:
        text = ' '.join(text.strip().split(' '))
    combined_text = ' '.join(texts)
    if len(combined_text) > MAX_STRING_LENGTH:
        print('Shortening string outer.')
        combined_text = combined_text[:MAX_STRING_LENGTH]
    return combined_text


def export(orig_csv=ORIG_CSV,
           output_json=OPPORTUNITIES_JSON,
           export_csv=OPPORTUNITIES_CSV,
           info_jsonl=OPPORTUNITIES_INFO_JSONL,
           verbose=False):
    datatypes = {
        'PostedDate': str,
        'ArchiveDate': str,
        'ResponseDeadLine': str,
        'AwardDate': str,
        'CGAC': str,
        'NaicsCode': str
    }
    df = pd.read_csv(orig_csv, encoding='ISO-8859-1', dtype=datatypes)
    df.rename(columns=COLUMN_RENAMES, inplace=True)
    print(df)

    if os.path.exists(info_jsonl):
        info = load_jsonlines_dict(info_jsonl)
    else:
        info = {}
    data = json.loads(df.to_json(orient='records'))
    for item in data:
        link = item['link']
        if link in info:
            text = string_from_info(info[link])
            item['text'] = text
        else:
            info[link] = {}
            item['text'] = ''
        item['description'] = prepare_text(item['description'])
    write_jsonlines_dict(info, info_jsonl)
    with open(output_json, 'w') as outfile:
        json.dump(data, outfile, indent=2)

    df = pd.read_json(output_json, orient='records')
    for column in DATE_COLUMNS:
        df[column] = pd.to_datetime(df[column],
                                    errors='coerce')

    df = util.postgres.preprocess_dataframe(df,
                                            unique_columns=UNIQUE_COLUMNS,
                                            verbose=verbose)

    util.postgres.df_to_csv(df, export_csv, columns=list(TABLE_TYPES.keys()))


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TABLE_NAME,
            TABLE_TYPES,
            serial_id=True,
            constraints={'unique': '''
UNIQUE (notice_id, title)
        '''}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(notice_id),               '') || ' ' ||
    coalesce(lower(title),                   '') || ' ' ||
    coalesce(lower(solicitation_number),     '') || ' ' ||
    coalesce(lower(department),              '') || ' ' ||
    coalesce(lower(fpds_code),               '') || ' ' ||
    coalesce(lower(office),                  '') || ' ' ||
    coalesce(lower(naics_code),              '') || ' ' ||
    coalesce(lower(pop_street_address),      '') || ' ' ||
    coalesce(lower(pop_city),                '') || ' ' ||
    coalesce(lower(pop_state),               '') || ' ' ||
    coalesce(lower(pop_zip),                 '') || ' ' ||
    coalesce(lower(pop_country),             '') || ' ' ||
    coalesce(lower(award_number),            '') || ' ' ||
    coalesce(lower(awardee),                 '') || ' ' ||
    coalesce(lower(primary_contact_title),   '') || ' ' ||
    coalesce(lower(primary_contact_name),    '') || ' ' ||
    coalesce(lower(primary_contact_email),   '') || ' ' ||
    coalesce(lower(primary_contact_phone),   '') || ' ' ||
    coalesce(lower(primary_contact_fax),     '') || ' ' ||
    coalesce(lower(secondary_contact_title), '') || ' ' ||
    coalesce(lower(secondary_contact_name),  '') || ' ' ||
    coalesce(lower(secondary_contact_email), '') || ' ' ||
    coalesce(lower(secondary_contact_phone), '') || ' ' ||
    coalesce(lower(secondary_contact_fax),   '') || ' ' ||
    coalesce(lower(state),                   '') || ' ' ||
    coalesce(lower(city),                    '') || ' ' ||
    coalesce(lower(zip),                     '') || ' ' ||
    coalesce(lower(country_code),            '') || ' ' ||
    coalesce(lower(description),             '') || ' ' ||
    coalesce(text,                           '')
  )
);
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=OPPORTUNITIES_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
