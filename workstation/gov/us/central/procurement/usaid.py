#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This utility helps process USAID grant data made available through:
#
#  https://explorer.usaid.gov/prepared/us_foreign_aid_complete.csv
#
import os
import pandas as pd
import requests

import util.postgres

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../../..')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')


CSV_URL = 'https://explorer.usaid.gov/prepared/us_foreign_aid_complete.csv'
EXPORT_CSV = '{}/us_foreign_aid.csv'.format(EXPORT_DIR)

def retrieve():
    df = pd.read_csv(CSV_URL)
    df.to_csv(EXPORT_CSV) 
