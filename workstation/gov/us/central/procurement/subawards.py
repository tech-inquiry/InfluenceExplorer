#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# A utility for incorporating the US federal subaward data provided at:
#
#  https://www.usaspending.gov/download_center/custom_award_data
#
# We download by only selecting the 'Sub-Awards' option, along with all agencies,
# for a single year at a time.
#
# 2023-12-18: The subaward primary place of performance 'address_line_1' field
#             appears to have been removed.
#
import glob
import json
import os
import pandas as pd

from collections import OrderedDict

import util.postgres

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data/us_subawards/')
CONTRACTS_DIR = os.path.join(DATA_DIR, 'contracts')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

SUBAWARDS_JSON = '{}/subawards.json'.format(CONTRACTS_DIR)
SUBAWARDS_CSV = '{}/us_central_procurement_subawards.csv'.format(EXPORT_DIR)

if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)

# The original subaward columns are:
#
# 001. prime_award_unique_key,
# 002. prime_award_piid,
# 003. prime_award_parent_piid,
# 004. prime_award_amount,
# 005. prime_award_disaster_emergency_fund_codes,
# 006. prime_award_outlayed_amount_funded_by_COVID-19_supplementals,
# 007. prime_award_obligated_amount_funded_by_COVID-19_supplementals,
# 008. prime_award_base_action_date,
# 009. prime_award_base_action_date_fiscal_year,
# 010. prime_award_latest_action_date,
# 011. prime_award_latest_action_date_fiscal_year,
# 012. prime_award_period_of_performance_start_date,
# 013. prime_award_period_of_performance_current_end_date,
# 014. prime_award_period_of_performance_potential_end_date,
# 015. prime_award_awarding_agency_code,
# 016. prime_award_awarding_agency_name,
# 017. prime_award_awarding_sub_agency_code,
# 018. prime_award_awarding_sub_agency_name,
# 019. prime_award_awarding_office_code,
# 020. prime_award_awarding_office_name,
# 021. prime_award_funding_agency_code,
# 022. prime_award_funding_agency_name,
# 023. prime_award_funding_sub_agency_code,
# 024. prime_award_funding_sub_agency_name,
# 025. prime_award_funding_office_code,
# 026. prime_award_funding_office_name,
# 027. prime_award_treasury_accounts_funding_this_award,
# 028. prime_award_federal_accounts_funding_this_award,
# 029. prime_award_object_classes_funding_this_award,
# 030. prime_award_program_activities_funding_this_award,
# 031. prime_awardee_uei,
# 032. prime_awardee_duns,
# 033. prime_awardee_name,
# 034. prime_awardee_dba_name,
# 035. prime_awardee_parent_uei,
# 036. prime_awardee_parent_duns,
# 037. prime_awardee_parent_name,
# 038. prime_awardee_country_code,
# 039. prime_awardee_country_name,
# 040. prime_awardee_address_line_1,
# 041. prime_awardee_city_name,
# 042. prime_awardee_county_name,
# 043. prime_awardee_state_code,
# 044. prime_awardee_state_name,
# 045. prime_awardee_zip_code,
# 046. prime_award_summary_recipient_cd_original,
# 047. prime_award_summary_recipient_cd_current,
# 048. prime_awardee_foreign_postal_code,
# 049. prime_awardee_business_types,
# 050. prime_award_primary_place_of_performance_city_name,
# 051. prime_award_primary_place_of_performance_state_code,
# 052. prime_award_primary_place_of_performance_state_name,
# 053. prime_award_primary_place_of_performance_address_zip_code,
# 054. prime_award_summary_place_of_performance_cd_original,
# 055. prime_award_summary_place_of_performance_cd_current,
# 056. prime_award_primary_place_of_performance_country_code,
# 057. prime_award_primary_place_of_performance_country_name,
# 058. prime_award_description,
# 059. prime_award_project_title,
# 060. prime_award_naics_code,
# 061. prime_award_naics_description,
# 062. prime_award_national_interest_action_code,
# 063. prime_award_national_interest_action,
# 064. subaward_type,
# 065. subaward_fsrs_report_id,
# 066. subaward_fsrs_report_year,
# 067. subaward_fsrs_report_month,
# 068. subaward_number,
# 069. subaward_amount,
# 070. subaward_action_date,
# 071. subaward_action_date_fiscal_year,
# 072. subawardee_uei,
# 073. subawardee_duns,
# 074. subawardee_name,
# 075. subawardee_dba_name,
# 076. subawardee_parent_uei,
# 077. subawardee_parent_duns,
# 078. subawardee_parent_name,
# 079. subawardee_country_code,
# 080. subawardee_country_name,
# 081. subawardee_address_line_1,
# 082. subawardee_city_name,
# 083. subawardee_state_code,
# 084. subawardee_state_name,
# 085. subawardee_zip_code,
# 086. subawardee_recipient_cd_original,
# 087. subawardee_recipient_cd_current,
# 088. subawardee_foreign_postal_code,
# 089. subawardee_business_types,
# 090. subaward_primary_place_of_performance_address_line_1,
# 091. subaward_primary_place_of_performance_city_name,
# 092. subaward_primary_place_of_performance_state_code,
# 093. subaward_primary_place_of_performance_state_name,
# 094. subaward_primary_place_of_performance_address_zip_code,
# 095. subaward_place_of_performance_cd_original,
# 096. subaward_place_of_performance_cd_current,
# 097. subaward_primary_place_of_performance_country_code,
# 098. subaward_primary_place_of_performance_country_name,
# 099. subaward_description,
# 100. subawardee_highly_compensated_officer_1_name,
# 101. subawardee_highly_compensated_officer_1_amount,
# 102. subawardee_highly_compensated_officer_2_name,
# 103. subawardee_highly_compensated_officer_2_amount,
# 104. subawardee_highly_compensated_officer_3_name,
# 105. subawardee_highly_compensated_officer_3_amount,
# 106. subawardee_highly_compensated_officer_4_name,
# 107. subawardee_highly_compensated_officer_4_amount,
# 108. subawardee_highly_compensated_officer_5_name,
# 109. subawardee_highly_compensated_officer_5_amount,
# 110. usaspending_permalink,
# 111. subaward_fsrs_report_last_modified_date
#
# Out of this list of 111 columns, we keep:
#
# 001. prime_award_unique_key,
# 002. prime_award_piid,
# 003. prime_award_parent_piid,
# 008. prime_award_base_action_date,
# 015. prime_award_awarding_agency_code,
# 016. prime_award_awarding_agency_name,
# 017. prime_award_awarding_sub_agency_code,
# 018. prime_award_awarding_sub_agency_name,
# 019. prime_award_awarding_office_code,
# 020. prime_award_awarding_office_name,
# 021. prime_award_funding_agency_code,
# 022. prime_award_funding_agency_name,
# 023. prime_award_funding_sub_agency_code,
# 024. prime_award_funding_sub_agency_name,
# 025. prime_award_funding_office_code,
# 026. prime_award_funding_office_name,
# 028. prime_award_federal_accounts_funding_this_award,
# 029. prime_award_object_classes_funding_this_award,
# 030. prime_award_program_activities_funding_this_award,
# 031. prime_awardee_uei,
# 033. prime_awardee_name,
# 034. prime_awardee_dba_name,
# 035. prime_awardee_parent_uei,
# 037. prime_awardee_parent_name,
# 064. subaward_type,
# 065. subaward_fsrs_report_id,
# 066. subaward_fsrs_report_year,
# 067. subaward_fsrs_report_month,
# 068. subaward_number,
# 069. subaward_amount,
# 070. subaward_action_date,
# 071. subaward_action_date_fiscal_year,
# 072. subawardee_uei,
# 074. subawardee_name,
# 075. subawardee_dba_name,
# 076. subawardee_parent_uei,
# 078. subawardee_parent_name,
# 079. subawardee_country_code,
# 080. subawardee_country_name,
# 081. subawardee_address_line_1,
# 082. subawardee_city_name,
# 083. subawardee_state_code,
# 084. subawardee_state_name,
# 085. subawardee_zip_code,
# 087. subawardee_recipient_cd_current,
# 088. subawardee_foreign_postal_code,
# 089. subawardee_business_types,
# 090. subaward_primary_place_of_performance_address_line_1,
# 091. subaward_primary_place_of_performance_city_name,
# 092. subaward_primary_place_of_performance_state_code,
# 093. subaward_primary_place_of_performance_state_name,
# 094. subaward_primary_place_of_performance_address_zip_code,
# 096. subaward_place_of_performance_cd_current,
# 097. subaward_primary_place_of_performance_country_code,
# 098. subaward_primary_place_of_performance_country_name,
# 099. subaward_description,
# 100. subawardee_highly_compensated_officer_1_name,
# 101. subawardee_highly_compensated_officer_1_amount,
# 102. subawardee_highly_compensated_officer_2_name,
# 103. subawardee_highly_compensated_officer_2_amount,
# 104. subawardee_highly_compensated_officer_3_name,
# 105. subawardee_highly_compensated_officer_3_amount,
# 106. subawardee_highly_compensated_officer_4_name,
# 107. subawardee_highly_compensated_officer_4_amount,
# 108. subawardee_highly_compensated_officer_5_name,
# 109. subawardee_highly_compensated_officer_5_amount,
# 110. usaspending_permalink,
# 111. subaward_fsrs_report_last_modified_date

SUBAWARDEE_PROPERTIES_COLUMNS = [
    'subawardee_uei', 'subawardee_name', 'subawardee_country_code',
    'subawardee_country_name', 'subawardee_address_line_1',
    'subawardee_city_name', 'subawardee_state_code', 'subawardee_state_name',
    'subawardee_zip_code', 'subawardee_recipient_cd_current',
    'subawardee_foreign_postal_code', 'subawardee_business_types',
    'subaward_type', 'subaward_action_date',
    'subawardee_highly_compensated_officer_1_name',
    'subawardee_highly_compensated_officer_1_amount',
    'subawardee_highly_compensated_officer_2_name',
    'subawardee_highly_compensated_officer_2_amount',
    'subawardee_highly_compensated_officer_3_name',
    'subawardee_highly_compensated_officer_3_amount',
    'subawardee_highly_compensated_officer_4_name',
    'subawardee_highly_compensated_officer_4_amount',
    'subawardee_highly_compensated_officer_5_name',
    'subawardee_highly_compensated_officer_5_amount',
    'subaward_primary_place_of_performance_address_line_1',
    'subaward_primary_place_of_performance_city_name',
    'subaward_primary_place_of_performance_state_name',
    'subaward_primary_place_of_performance_state_code',
    'subaward_primary_place_of_performance_address_zip_code',
    'subaward_place_of_performance_cd_current',
    'subaward_primary_place_of_performance_country_name',
    'subaward_primary_place_of_performance_country_code',
    'subaward_description', 'subaward_fsrs_report_last_modified_date'
]

SUBAWARD_EXPORT_COLUMNS = [
    'prime_award_unique_key', 'prime_award_piid', 'prime_award_parent_piid',
    'prime_award_base_action_date', 'prime_award_awarding_agency_name',
    'prime_award_awarding_agency_code', 'prime_award_awarding_sub_agency_name',
    'prime_award_awarding_sub_agency_code', 'prime_award_awarding_office_name',
    'prime_award_awarding_office_code', 'prime_award_funding_agency_name',
    'prime_award_funding_agency_code', 'prime_award_funding_sub_agency_name',
    'prime_award_funding_sub_agency_code', 'prime_award_funding_office_name',
    'prime_award_funding_office_code', 'prime_awardee_uei',
    'prime_awardee_name', 'prime_awardee_dba_name', 'prime_awardee_parent_uei',
    'prime_awardee_parent_name', 'subaward_fsrs_report_year',
    'subaward_fsrs_report_month', 'subaward_number', 'subaward_amount',
    'subaward_action_date', 'subawardee_uei', 'subawardee_name',
    'subawardee_dba_name', 'subawardee_parent_uei', 'subawardee_parent_name',
    'subaward_primary_place_of_performance_address_line_1',
    'subaward_primary_place_of_performance_city_name',
    'subaward_primary_place_of_performance_state_name',
    'subaward_primary_place_of_performance_address_zip_code',
    'subaward_place_of_performance_cd_current',
    'subaward_primary_place_of_performance_country_name',
    'subaward_description', 'subaward_fsrs_report_last_modified_date'
]

SUBAWARD_RENAMES = {
    'prime_award_unique_key': 'prime_key',
    'prime_award_piid': 'prime_piid',
    'prime_award_parent_piid': 'prime_parent_piid',
    'prime_award_base_action_date': 'prime_base_action_date',
    'prime_award_awarding_agency_name': 'prime_awarding_agency',
    'prime_award_awarding_agency_code': 'prime_awarding_agency_code',
    'prime_award_awarding_sub_agency_name': 'prime_awarding_sub_agency',
    'prime_award_awarding_sub_agency_code': 'prime_awarding_sub_agency_code',
    'prime_award_awarding_office_name': 'prime_awarding_office',
    'prime_award_awarding_office_code': 'prime_awarding_office_code',
    'prime_award_funding_agency_name': 'prime_funding_agency',
    'prime_award_funding_agency_code': 'prime_funding_agency_code',
    'prime_award_funding_sub_agency_name': 'prime_funding_sub_agency',
    'prime_award_funding_sub_agency_code': 'prime_funding_sub_agency_code',
    'prime_award_funding_office_name': 'prime_funding_office',
    'prime_award_funding_office_code': 'prime_funding_office_code',
    'prime_awardee_uei': 'prime_uei',
    'prime_awardee_name': 'prime_name',
    'prime_awardee_dba_name': 'prime_dba',
    'prime_awardee_parent_uei': 'prime_parent_uei',
    'prime_awardee_parent_name': 'prime_parent_name',
    'subaward_number': 'sub_number',
    'subaward_amount': 'sub_amount',
    'subaward_action_date': 'sub_action_date',
    'subawardee_uei': 'sub_uei',
    'subawardee_name': 'sub_name',
    'subawardee_dba_name': 'sub_dba',
    'subawardee_parent_uei': 'sub_parent_uei',
    'subawardee_parent_name': 'sub_parent_name',
    'subaward_primary_place_of_performance_address_line_1':
    'sub_pop_address_line_1',
    'subaward_primary_place_of_performance_city_name': 'sub_pop_city',
    'subaward_primary_place_of_performance_state_name': 'sub_pop_state',
    'subaward_primary_place_of_performance_address_zip_code':
    'sub_pop_zipcode',
    'subaward_place_of_performance_cd_current': 'sub_pop_district',
    'subaward_primary_place_of_performance_country_name': 'sub_pop_country',
    'subaward_description': 'sub_description',
    'subaward_fsrs_report_year': 'sub_fsrs_year',
    'subaward_fsrs_report_month': 'sub_fsrs_month',
    'subaward_fsrs_report_last_modified_date': 'sub_fsrs_last_modified'
}

TABLE_TYPES = {
    'prime_key': 'VARCHAR(123)',
    'prime_piid': 'VARCHAR(63)',
    'prime_parent_piid': 'VARCHAR(63)',
    'prime_base_action_date': 'TIMESTAMP',
    'prime_awarding_agency': 'VARCHAR(123)',
    'prime_awarding_agency_code': 'VARCHAR(4)',
    'prime_awarding_sub_agency': 'VARCHAR(123)',
    'prime_awarding_sub_agency_code': 'VARCHAR(4)',
    'prime_awarding_office': 'VARCHAR(123)',
    'prime_awarding_office_code': 'VARCHAR(6)',
    'prime_funding_agency': 'VARCHAR(123)',
    'prime_funding_agency_code': 'VARCHAR(4)',
    'prime_funding_sub_agency': 'VARCHAR(123)',
    'prime_funding_sub_agency_code': 'VARCHAR(4)',
    'prime_funding_office': 'VARCHAR(123)',
    'prime_funding_office_code': 'VARCHAR(6)',
    'prime_uei': 'VARCHAR(15)',
    'prime_name': 'VARCHAR(255)',
    'prime_dba': 'VARCHAR(255)',
    'prime_parent_uei': 'VARCHAR(15)',
    'prime_parent_name': 'VARCHAR(255)',
    'sub_fsrs_year': 'INT',
    'sub_fsrs_month': 'INT',
    'sub_number': 'VARCHAR(63)',
    'sub_amount': 'FLOAT',
    'sub_action_date': 'TIMESTAMP',
    'sub_uei': 'VARCHAR(15)',
    'sub_name': 'VARCHAR(255)',
    'sub_dba': 'VARCHAR(255)',
    'sub_parent_uei': 'VARCHAR(15)',
    'sub_parent_name': 'VARCHAR(255)',
    'sub_pop_address_line_1': 'VARCHAR(255)',
    'sub_pop_city': 'VARCHAR(255)',
    'sub_pop_state': 'VARCHAR(255)',
    'sub_pop_zipcode': 'VARCHAR(63)',
    'sub_pop_district': 'VARCHAR(15)',
    'sub_pop_country': 'VARCHAR(255)',
    'sub_description': 'TEXT',
    'sub_fsrs_last_modified': 'TIMESTAMP'
}

UNIQUE_COLUMNS = ['prime_key', 'sub_number', 'sub_name', 'sub_action_date']
FORCE_NULL_COLUMNS = [
    'prime_base_action_date', 'sub_fsrs_year', 'sub_fsrs_month',
    'sub_action_date', 'sub_fsrs_last_modified'
]
TABLE_NAME = 'us_central_procurement_subaward'
TABLE_NAME_SHORT = 'us_central_subaward'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)


def df_to_json_by_unique_key(df, unique_key='prime_key'):
    data = json.loads(df.to_json(orient='records'))
    subs = {}
    for item in data:
        key = item[unique_key]
        if key not in subs:
            subs[key] = []
        subs[key].append(item)
    return subs


def extract_subaward_data_frames(regex,
                                 get_export=True,
                                 get_properties=False,
                                 verbose=False):
    DATATYPES = {
        'prime_awardee_uei': str,
        'prime_awardee_parent_uei': str,
        'prime_awarding_agency_code': str,
        'prime_awarding_sub_agency_code': str,
        'prime_awarding_office_code': str,
        'prime_funding_agency_code': str,
        'prime_funding_sub_agency_code': str,
        'prime_funding_office_code': str,
        'subawardee_uei': str,
        'subawardee_parent_uei': str
    }

    def replace_or_concat(df, df_addition):
        if df is None:
            return df_addition
        else:
            return pd.concat([df, df_addition], ignore_index=True)

    filenames = glob.glob(regex)

    subawardee_props = None
    export = None

    for filename in filenames:
        if verbose:
            print('Extracting subaward dataframe from {}'.format(filename))
        file_df = pd.read_csv(filename, dtype=DATATYPES)

        # Add blank column for subaward address line if it does not exist
        sub_pop_address_key = 'subaward_primary_place_of_performance_address_line_1'
        if sub_pop_address_key not in file_df:
            file_df[sub_pop_address_key] = ''

        if get_export:
            file_df_export = file_df[SUBAWARD_EXPORT_COLUMNS]
            file_df_export.rename(columns=SUBAWARD_RENAMES, inplace=True)
            export = replace_or_concat(export, file_df_export)

        if get_properties:
            file_df_properties = file_df[SUBAWARDEE_PROPERTIES_COLUMNS]
            subawardee_props = replace_or_concat(subawardee_props,
                                                 file_df_properties)

    return {'sub_properties': subawardee_props, 'export': export}


def export_subaward_data(regex,
                         output_json=SUBAWARDS_JSON,
                         export_csv=SUBAWARDS_CSV,
                         verbose=False):
    if verbose:
        print('Extracting subaward data frames.')
    dfs = extract_subaward_data_frames(regex, verbose=verbose)

    # Export the combined data to JSON by unique key.
    if verbose:
        print('Exporting subaward JSON by unique key.')
    data = df_to_json_by_unique_key(dfs['export'])
    with open(output_json, 'w') as outfile:
        json.dump(data, outfile, indent=1)
    del data

    if verbose:
        print('Lowercasing subaward columns.')
    COLUMNS_TO_LOWERCASE = [
        'prime_uei', 'prime_name', 'prime_dba', 'prime_parent_uei',
        'prime_parent_name', 'sub_uei', 'sub_name', 'sub_dba',
        'sub_parent_uei', 'sub_parent_name'
    ]
    for column in COLUMNS_TO_LOWERCASE:
        # Despite being ostensibly safer, replacing the `.str` conversion to
        # string with `.to_string(na_rep='')` results in massive slowdowns and
        # increased memory usage.
        dfs['export'][column] = dfs['export'][column].str.lower()

    if verbose:
        print('Preprocessing subaward dataframe for export.')
    dfs['export'] = util.postgres.preprocess_dataframe(
        dfs['export'], unique_columns=UNIQUE_COLUMNS, verbose=verbose)

    if verbose:
        print('Exporting subaward dataframe.')
    util.postgres.df_to_csv(dfs['export'], export_csv)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(TABLE_NAME,
                                       TABLE_TYPES,
                                       serial_id=True,
                                       constraints={
                                           'unique':
                                           '''
UNIQUE (
  prime_key, sub_number, sub_name, sub_action_date
)
        '''
                                       }))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(prime_key),                    '') || ' ' ||
    coalesce(lower(prime_piid),                   '') || ' ' ||
    coalesce(lower(prime_parent_piid),            '') || ' ' ||
    coalesce(lower(prime_awarding_agency),        '') || ' ' ||
    coalesce(lower(prime_awarding_sub_agency),    '') || ' ' ||
    coalesce(lower(prime_awarding_office),        '') || ' ' ||
    coalesce(lower(prime_funding_agency),         '') || ' ' ||
    coalesce(lower(prime_funding_sub_agency),     '') || ' ' ||
    coalesce(lower(prime_funding_office),         '') || ' ' ||
    coalesce(prime_uei,                           '') || ' ' ||
    coalesce(prime_name,                          '') || ' ' ||
    coalesce(prime_dba,                           '') || ' ' ||
    coalesce(prime_parent_uei,                    '') || ' ' ||
    coalesce(prime_parent_name,                   '') || ' ' ||
    coalesce(lower(sub_number),                   '') || ' ' ||
    coalesce(sub_uei,                             '') || ' ' ||
    coalesce(sub_name,                            '') || ' ' ||
    coalesce(sub_dba,                             '') || ' ' ||
    coalesce(sub_parent_uei,                      '') || ' ' ||
    coalesce(sub_parent_name,                     '') || ' ' ||
    coalesce(lower(sub_pop_address_line_1),       '') || ' ' ||
    coalesce(lower(sub_pop_city),                 '') || ' ' ||
    coalesce(lower(sub_pop_state),                '') || ' ' ||
    coalesce(lower(sub_pop_zipcode),              '') || ' ' ||
    coalesce(lower(sub_pop_country),              '') || ' ' ||
    coalesce(lower(sub_description),              '')
  )
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_primary_index ON {} (id);
    '''.format(TABLE_NAME_SHORT, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_prime_name_index ON {} (prime_name);
    '''.format(TABLE_NAME_SHORT, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_sub_name_index ON {} (sub_name);
    '''.format(TABLE_NAME_SHORT, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_prime_uei_index ON {} (prime_uei);
    '''.format(TABLE_NAME_SHORT, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_sub_uei_index ON {} (sub_uei);
    '''.format(TABLE_NAME_SHORT, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_funding_department_code_index ON {} (
    lower(prime_funding_agency_code)
);
    '''.format(TABLE_NAME_SHORT, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_awarding_department_code_index ON {} (
    lower(prime_awarding_agency_code)
);
    '''.format(TABLE_NAME_SHORT, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_funding_agency_code_index ON {} (
    lower(prime_funding_sub_agency_code)
);
    '''.format(TABLE_NAME_SHORT, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_awarding_agency_code_index ON {} (
    lower(prime_awarding_sub_agency_code)
);
    '''.format(TABLE_NAME_SHORT, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_funding_office_code_index ON {} (
    lower(prime_funding_office_code)
);
    '''.format(TABLE_NAME_SHORT, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_awarding_office_code_index ON {} (
    lower(prime_awarding_office_code)
);
    '''.format(TABLE_NAME_SHORT, TABLE_NAME))


def postgres_update(export_csv=SUBAWARDS_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def incorporate_year(year, vacuum=True, export=True, verbose=True):
    '''Incorporates a year of subcontracts downloaded from USASpending.gov'''
    export_subaward_data('{}/subawards-{}-*.csv'.format(CONTRACTS_DIR, year),
                         verbose=verbose)
    postgres_update(vacuum=vacuum)
    if export:
        export_subaward_data('{}/*.csv'.format(CONTRACTS_DIR), verbose=verbose)


def get_offices_over_cache(cache_regexp=SUBAWARDS_JSON):
    '''Returns dict mapping agencies to their offices.

    Args:
      cache_regexp: The regular expression for the JSON file glob.

    Returns:
      A dictionary mapping each agency to its list of associated offices.
      This is useful for creating normalization maps that correct overly course
      agency assignments, such as an agency of "Defense" and office of
      "Defense Information Systems Agency", which should cause the agency to be
      normalized to DISA.
    '''
    offices = {}
    dept_codes = {}
    agency_codes = {}
    office_codes = {}

    json_filenames = sorted(glob.glob(cache_regexp))
    num_filenames = len(json_filenames)
    for i in range(num_filenames):
        json_filename = json_filenames[i]
        print('  Processing file {} of {}: {}'.format(i, num_filenames,
                                                      json_filename))
        data = json.load(open(json_filename))
        for unique_id, subawards in data.items():
            for subaward in subawards:
                for dept, dept_code, agency, agency_code, office, office_code in [
                    (subaward['prime_funding_agency'],
                     subaward['prime_funding_agency_code'],
                     subaward['prime_funding_sub_agency'],
                     subaward['prime_funding_sub_agency_code'],
                     subaward['prime_funding_office'],
                     subaward['prime_funding_office_code']),
                    (subaward['prime_awarding_agency'],
                     subaward['prime_awarding_agency_code'],
                     subaward['prime_awarding_sub_agency'],
                     subaward['prime_awarding_sub_agency_code'],
                     subaward['prime_awarding_office'],
                     subaward['prime_awarding_office_code'])
                ]:
                    dept = util.entities.canonicalize(dept)
                    dept = dept if dept else ''
                    dept_code = util.entities.canonicalize(dept_code)
                    agency = util.entities.canonicalize(agency)
                    agency_code = util.entities.canonicalize(agency_code)
                    office = util.entities.canonicalize(office)
                    office_code = util.entities.canonicalize(office_code)

                    if not dept:
                        print('Missing dept value:')
                        print('  names: {}, {}, {}'.format(
                            dept, agency, office))
                        print('  codes: {}, {}, {}'.format(
                            dept_code, agency_code, office_code))

                    if not office:
                        continue
                    # Store the office
                    if dept not in offices:
                        offices[dept] = {}
                    if agency not in offices[dept]:
                        offices[dept][agency] = set()
                    offices[dept][agency].add(office)
                    # Store the codes
                    if dept not in dept_codes:
                        dept_codes[dept] = set()
                    dept_codes[dept].add(dept_code)
                    if dept not in agency_codes:
                        agency_codes[dept] = {}
                    if agency not in agency_codes[dept]:
                        agency_codes[dept][agency] = set()
                    agency_codes[dept][agency].add(agency_code)
                    if dept not in office_codes:
                        office_codes[dept] = {}
                    if agency not in office_codes[dept]:
                        office_codes[dept][agency] = {}
                    if office not in office_codes[dept][agency]:
                        office_codes[dept][agency][office] = set()
                    office_codes[dept][agency][office].add(office_code)

    for dept in offices:
        for agency in offices[dept]:
            offices[dept][agency] = sorted(list(offices[dept][agency]))
        offices[dept] = OrderedDict(sorted(offices[dept].items()))
    offices = OrderedDict(sorted(offices.items()))

    for dept in dept_codes:
        dept_codes[dept] = sorted(list(dept_codes[dept]))
    dept_codes = OrderedDict(sorted(dept_codes.items()))

    for dept in agency_codes:
        for agency in agency_codes[dept]:
            agency_codes[dept][agency] = sorted(
                list(agency_codes[dept][agency]))
        agency_codes[dept] = OrderedDict(sorted(agency_codes[dept].items()))
    agency_codes = OrderedDict(sorted(agency_codes.items()))

    for dept in office_codes:
        for agency in office_codes[dept]:
            for office in office_codes[dept][agency]:
                office_codes[dept][agency][office] = sorted(
                    list(office_codes[dept][agency][office]))
            office_codes[dept][agency] = OrderedDict(
                sorted(office_codes[dept][agency].items()))
        office_codes[dept] = OrderedDict(sorted(office_codes[dept].items()))
    office_codes = OrderedDict(sorted(office_codes.items()))

    return {
        'offices': offices,
        'dept_codes': dept_codes,
        'agency_codes': agency_codes,
        'office_codes': office_codes
    }


# TODO(Jack Poulson): After extending subaward table to support codes in
# addition to names, compute the maps.
