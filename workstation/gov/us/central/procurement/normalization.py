#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import json
import os
import re

import util.dict_path
import util.entities

from util.entities import canonicalize

THIS_DIR = os.path.dirname(__file__)
SHARED_DIR = os.path.join(THIS_DIR, '../../../../../')
SHARED_DATA_DIR = os.path.join(SHARED_DIR, 'data')


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return util.entities.normalization_map_helper(
        ['us', 'central', 'procurement'],
        entities=entities,
        additional_category='uei',
        additional_key='ueis',
        object_normalization=True)


def get_contractor_normalization_map(entities=None):
    '''Inverts the contractor alternate spellings into a normalization map.'''
    return util.entities.normalization_map_helper(
        ['us', 'central', 'procurement'],
        entities=entities,
        primary_key='contractorNames',
        block_key=None,
        object_normalization=False)


def normalize(entity, uei, normalization):
    '''Normalizes a (entity, UEI) pairing into an entity.'''
    entity = canonicalize(entity)

    key_list = [
        json.dumps({
            'name': entity,
            'uei': uei
        }),
        json.dumps({'name': entity}),
        json.dumps({'uei': uei})
    ]

    for key in key_list:
        if key in normalization:
            return normalization[key]

    return entity


def normalize_contractor(entity, normalization, contractor_normalization):
    if entity in contractor_normalization:
        return contractor_normalization[entity] 
    else:
        return normalize(entity, None, normalization)


def get_department_codes(office_data, dept):
    if dept not in office_data['dept_codes']:
        print('Could not find "{}" in dept_codes.'.format(dept))
        return []
    return office_data['dept_codes'][dept]


def get_agency_codes(office_data, dept, agency):
    if dept not in office_data['agency_codes']:
        print('Could not find "{}" in agency_codes.'.format(dept))
        return []
    if agency not in office_data['agency_codes'][dept]:
        print('Could not find "{}" in agency_codes[{}].'.format(agency, dept))
        return []
    return office_data['agency_codes'][dept][agency]


def get_office_codes(office_data, dept, agency, office):
    if dept not in office_data['office_codes']:
        print('Could not find "{}" in office_codes.'.format(dept))
        return []
    if agency not in office_data['office_codes'][dept]:
        print('Could not find "{}" in office_codes[{}].'.format(agency, dept))
        return []
    if office not in office_data['office_codes'][dept][agency]:
        print('Could not find "{}" in office_codes[{}][{}].'.format(office, dept, agency))
        return []
    return office_data['office_codes'][dept][agency][office]


def get_agency_normalization_map(entities=None):
    '''Inverts the entity agency alternates into a normalization map.'''
    if entities is None:
        entities = util.entities.get_entities()
    office_data = json.load(open(
        '{}/us_federal_offices.json'.format(SHARED_DATA_DIR)))

    normalization = {'dept': {}, 'agency': {}, 'office': {}}
    for entity in entities:
        profile = entities[entity]
        alternates = util.dict_path.get_from_path(
            profile,
            ['feeds', 'us', 'central', 'procurement', 'agencyNames'])
        if alternates is None:
            continue
        for alternate in alternates:
            dept = alternate['dept']
            if 'office' in alternate or 'offices' in alternate:
                agency = alternate['agency']
                offices = alternate['offices'] if 'offices' in alternate else [
                    alternate['office']
                ]
                for office in offices:
                    for code in get_office_codes(office_data, dept, agency,
                                                 office):
                        normalization['office'][code] = entity
            elif 'agency' in alternate:
                agency = alternate['agency']
                for code in get_agency_codes(office_data, dept, agency):
                    normalization['agency'][code] = entity
            else:
                for code in get_department_codes(office_data, dept):
                    normalization['dept'][code] = entity

    return normalization


def normalize_agency(dept_code, agency_code, office_code, normalization):
    '''Normalizes a (department, agency, office) triplet into a US entity.'''
    if dept_code:
        dept_code = dept_code.lower()
    if agency_code:
        agency_code = agency_code.lower()
    if office_code:
        office_code = office_code.lower()
    if office_code in normalization['office']:
        return normalization['office'][office_code]
    elif agency_code in normalization['agency']:
        return normalization['agency'][agency_code]
    elif dept_code in normalization['dept']:
        return normalization['dept'][dept_code]
    else:
        return 'government of the united states'
