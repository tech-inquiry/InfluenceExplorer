#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# A set of python utilities for converting the XML FPDS Atom feeds into JSON.
# Said FPDS API only allows 10 awards to be downloaded at a time, and so
# we accumulate the XML records over the given time span 10 at a time.
#
# The XML is then converted to JSON via xmltodict.
#
import gc
import itertools
import json
import jsonlines
import os
import re
import requests
import time
import urllib.parse
import xmltodict
from multiprocessing import Pool
from tqdm import tqdm
from urllib.parse import urlparse, parse_qs

import util.dates

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../../../')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')

# The directory we store downloaded awards into.
AWARDS_DIR = os.path.join(DATA_DIR, 'us_awards')
MODIFIED_DIR = '{}/mod_date'.format(AWARDS_DIR)
SIGNED_DIR = '{}/sign_date'.format(AWARDS_DIR)
DELETED_MODIFIED_DIR = '{}/deleted_mod_date'.format(AWARDS_DIR)
DELETED_SIGNED_DIR = '{}/deleted_sign_date'.format(AWARDS_DIR)

if not os.path.exists(MODIFIED_DIR):
    os.makedirs(MODIFIED_DIR)
if not os.path.exists(SIGNED_DIR):
    os.makedirs(SIGNED_DIR)
if not os.path.exists(DELETED_MODIFIED_DIR):
    os.makedirs(DELETED_MODIFIED_DIR)
if not os.path.exists(DELETED_SIGNED_DIR):
    os.makedirs(DELETED_SIGNED_DIR)

# The base of the URL to query the FPDS award XML from.
# is available. The previous DUNS-based versions are now disabled.
FPDS_BASE_URL = ('https://fpds.gov/dbsight/FEEDS/ATOM?FEEDNAME=PUBLIC&' +
                 'templateName=1.5.3')

FPDS_DELETED_URL = ('https://fpds.gov/dbsight/FEEDS/ATOM?FEEDNAME=DELETED&' +
                    'templateName=1.5.3')

# A sink for the FPDS namespaces so that attributes are not prefixed with 'ns1'.
FPDS_NAMESPACE_SINK = {
    'http://www.w3.org/2005/Atom': None,
    'https://www.fpds.gov/FPDS': None,
}

# Note: Increasing this value up to 10 may increase the speed at which you
# can download FPDS award data, but increasing too far past your number of
# cores may cause errors.
NUM_PROCESSES = 5


# Remove leading and trailing whitespace then merge whitespaces.
def simplify_string(value):
    return ' '.join(value.strip().split())


def lower_if_exists(value):
    return value.lower() if value else ''


def get_contract_detail(contract):
    '''Returns the award/OTA/OTIDV/IDV info for the award object.

    Args:
      contract: The JSON object for the award.

    Returns:
      The equivalent of the 'contractDetail' member of 'OtherTransactionAward'.
    '''
    content = contract['content']
    if 'award' in content:
        return content['award']
    elif 'OtherTransactionAward' in content:
        return content['OtherTransactionAward']['contractDetail']
    elif 'OtherTransactionIDV' in content:
        return content['OtherTransactionIDV']['contractDetail']
    elif 'IDV' in content:
        return content['IDV']
    else:
        print('No contract details found in {}'.format(contract))
        return None


def get_id(contract):
    '''Returns the ID object for the award.

    Args:
      contract: The JSON object for the award.

    Returns:
      The ID object for the award.
    '''
    content = contract['content']
    if 'award' in content:
        return content['award']['awardID']['awardContractID']
    elif 'OtherTransactionAward' in content:
        return content['OtherTransactionAward']['OtherTransactionAwardID'][
            'OtherTransactionAwardContractID']
    elif 'OtherTransactionIDV' in content:
        return content['OtherTransactionIDV']['OtherTransactionIDVID'][
            'OtherTransactionIDVContractID']
    elif 'IDV' in content:
        return content['IDV']['contractID']['IDVID']
    else:
        print('No contract details found in {}'.format(contract))
        return None


def get_piid(contract):
    '''Returns the PIID for the award object.

    Args:
      contract: The JSON object for the award.

    Returns:
      The (ideally) unique Procurement Instrument IDentifier (PIID) associated
      for the award series.
    '''
    identifier = get_id(contract)
    return identifier['PIID'] if identifier else None


def get_modification_number(contract):
    '''Returns the modification number in the PIID series for the award.

    Args:
      contract: The JSON object for the award.

    Returns:
      The modification number associated with the Procurement Instrument
      IDentifier (PIID) award series.
    '''
    identifier = get_id(contract)
    return identifier['modNumber'] if identifier else None


def get_transaction_number(contract):
    '''Returns the transaction number in the PIID series for the award.

    Args:
      contract: The JSON object for the award.

    Returns:
      The transaction number associated with the Procurement Instrument
      IDentifier (PIID) award series.
    '''
    identifier = get_id(contract)
    if not identifier:
        return None
    if 'transactionNumber' not in identifier:
        # We could output debugging information, but this appears to hold for
        # entire classes of contracts, including IDVs.
        return None
    return identifier['transactionNumber']


def get_parent_id(contract):
    '''Returns the parent identifier for the award object (if it exists).

    Args:
      contract: The JSON object for the award.

    Returns:
      If this is an award, as opposed to a contract or IDV, the referenced
      (parent) IDV's ID is returned. Otherwise, None is returned.
    '''
    content = contract['content']
    if not 'award' in content:
        return None
    awardID = content['award']['awardID']
    key = 'referencedIDVID'
    return awardID[key] if key in awardID else None


def get_parent_piid(contract):
    '''Returns the parent PIID for the award object (if it exists).

    Args:
      contract: The JSON object for the award.

    Returns:
      If this is an award, as opposed to a contract or IDV, the referenced
      (parent) IDV's PIID is returned. Otherwise, None is returned.
    '''
    parent_id = get_parent_id(contract)
    if not parent_id:
        return None
    key = 'PIID'
    return parent_id[key] if key in parent_id else None


def get_parent_modification_number(contract):
    '''Returns the parent mod. number for the award object (if it exists).

    Args:
      contract: The JSON object for the award.

    Returns:
      If this is an award, as opposed to a contract or IDV, the referenced
      (parent) IDV's modification number is returned. Otherwise, None is
      returned.
    '''
    parent_id = get_parent_id(contract)
    if not parent_id:
        return None
    key = 'modNumber'
    return parent_id[key] if key in parent_id else None


def get_place_of_performance(contract):
    '''Returns the place of performance JSON for the award object.'''
    detail = get_contract_detail(contract)
    if detail is None:
        return None
    if 'placeOfPerformance' in detail:
        return detail['placeOfPerformance']
    return None


def get_solicitation_id(contract, lower=True):
    '''Returns the solicitation ID of the contract requirement.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The text in the 'solicitationID' field.
    '''
    detail = get_contract_detail(contract)
    if detail is None or 'contractData' not in detail:
        return None
    contract_data = detail['contractData']
    key = 'solicitationID'
    description = contract_data[key] if key in contract_data else None
    return lower_if_exists(description) if lower else description


def get_description_of_contract_requirement(contract, lower=True):
    '''Returns the text for the description of the contract requirement.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The text in the 'descriptionOfContractRequirement' field.
    '''
    detail = get_contract_detail(contract)
    if detail is None or 'contractData' not in detail:
        return None
    contract_data = detail['contractData']
    key = 'descriptionOfContractRequirement'
    description = contract_data[key] if key in contract_data else None
    return lower_if_exists(description) if lower else description


def get_major_program_code(contract):
    '''Returns any majorProgramCode field for the award.

    Args:
      contract: The JSON object for the award.

    Returns:
      The text in the 'majorProgramCode' field.
    '''
    detail = get_contract_detail(contract)
    if detail is None or 'contractData' not in detail:
        return None
    contract_data = detail['contractData']
    key = 'majorProgramCode'
    return contract_data[key].lower() if key in contract_data else None


def get_national_interest_action_code(contract):
    '''Returns any nationalInterestActionCode field for the award.

    Args:
      contract: The JSON object for the award.

    Returns:
      The text in the 'majorProgramCode' field.
    '''
    detail = get_contract_detail(contract)
    if detail is None or 'contractData' not in detail:
        return None
    contract_data = detail['contractData']
    key = 'nationalInterestActionCode'
    return contract_data[key] if key in contract_data else None


def get_national_interest_action_code_description(contract):
    '''Returns any nationalInterestActionCode description field for the award.

    Args:
      contract: The JSON object for the award.

    Returns:
      The description in the 'majorProgramCode' field.
    '''
    niac = get_national_interest_action_code(contract)
    if not niac:
        return None
    key = '@description'
    return niac[key].lower() if key in niac else None


def get_national_interest_action_code_text(contract):
    '''Returns any nationalInterestActionCode text field for the award.

    Args:
      contract: The JSON object for the award.

    Returns:
      The text in the 'majorProgramCode' field.
    '''
    niac = get_national_interest_action_code(contract)
    if not niac:
        return None
    key = '#text'
    return niac[key].lower() if key in niac else None


def get_contract_amount(contract, key, total=False):
    '''Returns the amount for a particular contract value type.

    Args:
      contract: The JSON object for the contract.
      key: The key of the value type.
      total: Whether the single-award or series total is requested.

    Returns:
      The amount for the specified values type of the award. For more
      information, see the official FPDS-NG documentation at:
      https://techinquiry.org/docs/FPDSNG_DataDictionary_V15_OT.pdf.
    '''
    detail = get_contract_detail(contract)
    if detail is None:
        return float(0)
    values_key = 'totalDollarValues' if total else 'dollarValues'
    values = detail[values_key] if values_key in detail else None
    if not values:
        return float(0)
    return float(values[key]) if key in values else float(0)


def get_obligated_amount(contract):
    '''Returns the Obligated Amount of a contract.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The "Obligated Amount" (in USD) of an award, as defined in
      https://techinquiry.org/docs/FPDSNG_DataDictionary_V15_OT.pdf.
    '''
    return get_contract_amount(contract, 'obligatedAmount', False)


def get_base_and_exercised_options_value(contract):
    '''Returns the Base and Exercised Options Value of a contract.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The "Base and Exercised Options Value" (in USD) of an award, as defined in
      https://techinquiry.org/docs/FPDSNG_DataDictionary_V15_OT.pdf.
    '''
    return get_contract_amount(contract, 'baseAndExercisedOptionsValue', False)


def get_base_and_all_options_value(contract):
    '''Returns the Base and All Options Value of a contract.

    You can read about the definitions of these award values at:
        https://techinquiry.org/docs/FPDSNG_DataDictionary_V15_OT.pdf

    Args:
      contract: The JSON object for the contract.

    Returns:
      The "Base and All Options Options Value" (in USD) of an award, as defined
      in https://techinquiry.org/docs/FPDSNG_DataDictionary_V15_OT.pdf.
    '''
    return get_contract_amount(contract, 'baseAndAllOptionsValue', False)


def get_total_obligated_amount(contract):
    '''Returns the Total Obligated Amount of a contract.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The "Total Obligated Amount" (in USD) of the series of awards, as defined
      in https://techinquiry.org/docs/FPDSNG_DataDictionary_V15_OT.pdf.
    '''
    return get_contract_amount(contract, 'totalObligatedAmount', True)


def get_total_base_and_exercised_options_value(contract):
    '''Returns the Total Base and Exercised Options Value of a contract.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The "Total Base and Exercised Options Value" (in USD) of the series of
      awards, as defined in
      https://techinquiry.org/docs/FPDSNG_DataDictionary_V15_OT.pdf.
    '''
    return get_contract_amount(contract, 'totalBaseAndExercisedOptionsValue',
                               True)


def get_total_base_and_all_options_value(contract):
    '''Returns the Total Base and All Options Value of a contract.

    You can read about the definitions of these award values at:
        https://techinquiry.org/docs/FPDSNG_DataDictionary_V15_OT.pdf

    Args:
      contract: The JSON object for the contract.

    Returns:
      The "Total Base and All Options Options Value" (in USD) of the series of
      awards, as defined in
      https://techinquiry.org/docs/FPDSNG_DataDictionary_V15_OT.pdf.
    '''
    return get_contract_amount(contract, 'totalBaseAndAllOptionsValue', True)


def get_contract_dates(contract):
    '''Returns the relativeContractDates object.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The dictionary of 'relevantContractDates' in the contract details.
    '''
    detail = get_contract_detail(contract)
    if detail is None:
        return None
    key = 'relevantContractDates'
    return detail[key] if key in detail else None


def get_date(contract, key):
    '''Returns a significant date for the contract.

    Args:
      contract: The JSON object for the contract.
      key: The key of the type of date to return.

    Returns:
      The string representing the significant date, e.g.,
      '2019-12-01 00:00:00' for December 1, 2019 at midnight.
    '''
    dates = get_contract_dates(contract)
    if not dates:
        return None
    return dates[key] if key in dates else None


def get_signed_date(contract):
    '''Returns the date the contract was signed.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The string representing the date the contract was signed, e.g.,
      '2019-12-01 00:00:00' for December 1, 2019 at midnight.
    '''
    return get_date(contract, 'signedDate')


def get_effective_date(contract):
    '''Returns the date the contract becomes effective.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The string representing the date the contract becomes effective, e.g.,
      '2019-12-01 00:00:00' for December 1, 2019 at midnight.
    '''
    return get_date(contract, 'effectiveDate')


def get_modified_date(contract):
    '''Returns the date the contract was last modified.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The string representing the date the contract becomes effective, e.g.,
      '2019-12-01 00:00:00' for December 1, 2019 at midnight.
    '''
    # NOTE: You would think this would be similar to effectiveData and signedDate...
    return contract['modified']


def get_current_completion_date(contract):
    '''Returns the date the completion date for the current order.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The string representing the date this contract should end, e.g.,
      '2019-12-01 00:00:00' for December 1, 2019 at midnight.
    '''
    return get_date(contract, 'currentCompletionDate')


def get_ultimate_completion_date(contract):
    '''Returns the date the ultimate date for this contract series.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The string representing the date this contract series should end, e.g.,
      '2019-12-01 00:00:00' for December 1, 2019 at midnight.
    '''
    return get_date(contract, 'ultimateCompletionDate')


def get_last_date_to_order(contract):
    '''Returns the last date to order from this procurement vehicle.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The string representing the last date to order from this vehicle, e.g.,
      '2019-12-01 00:00:00' for December 1, 2019 at midnight.
    '''
    return get_date(contract, 'lastDateToOrder')


def get_signed_date_and_negated_total_base_and_all_options_value(contract):
    '''Returns a tuple of the signing date of the contract and negated award.

    Args:
      contract: The JSON object for the contract.

    Returns:
      The pairing of the signed date and award amount.
    '''
    return (get_signed_date(contract),
            -get_total_base_and_all_options_value(contract))


def sort_by_total_base_and_all_options_value(contracts):
    '''Returns the sorting of a list of FPDS JSON contracts using total $s.

    Args:
      contracts: A list of FPDS contract JSON objects.

    Returns:
      A sorted version of the input list with descending dollar amounts.
    '''
    sorted_contracts = sorted(contracts,
                              key=get_total_base_and_all_options_value,
                              reverse=True)
    return sorted_contracts


def sort_by_signed_date_and_total_base_and_all_options_value(contracts):
    '''Sorts contracts based upon their signed date then award amount.

    Args:
      contracts: A list of FPDS contract JSON objects.

    Returns:
      A sorted version of the input list with ascending signed date and
      descending dollar amounts.
    '''
    sorted_contracts = sorted(
        contracts,
        key=get_signed_date_and_negated_total_base_and_all_options_value)
    return sorted_contracts


def get_title(contract):
    ''' Returns the title of the contract.'''
    return contract['title']


def get_link(contract):
    '''Returns the query parameter of the suggested FPDS link.'''

    def get_query_string(link):
        pieces = link.split('&q=')
        if len(pieces) < 2:
            print('Could not extract query parameter from {}'.format(link))
            return ''
        else:
            return pieces[1]

    link = contract['link']
    if '@href' in link:
        return get_query_string(link['@href'])
    elif 'href' in link:
        return get_query_string(link['href'])
    else:
        print(link)
        print('Could not find href in link dictionary.')
        return None


def get_purchaser_information(contract):
    '''Returns the purchaser information of the contract, or None.'''
    detail = get_contract_detail(contract)
    if detail is None:
        return None
    key = 'purchaserInformation'
    return detail[key] if key in detail else None


def get_department(contract,
                   contracting=True,
                   name=True,
                   lower=True,
                   verbose=False):
    '''Returns the department of the contract.'''
    purchaser_info = get_purchaser_information(contract)
    if not purchaser_info:
        return None

    key_prefix = 'contractingOffice' if contracting else 'fundingRequesting'
    key = '{}AgencyID'.format(key_prefix)
    if not key in purchaser_info:
        return None

    info = purchaser_info[key]
    department = ''
    if name:
        if '@departmentName' in info:
            department = info['@departmentName']
    else:
        if '@departmentID' in info:
            department = info['@departmentID']

    return lower_if_exists(department) if lower else department


# This routine is now deprecated.
def get_contracting_department(contract, lower=True, verbose=False):
    '''Returns the contracting department of the contract.'''

    return get_department(contract,
                          contracting=True,
                          lower=lower,
                          verbose=verbose)


# This routine is now deprecated
def get_funding_department(contract, lower=True, verbose=False):
    '''Returns the funding department of the contract.'''

    return get_department(contract,
                          contracting=False,
                          lower=lower,
                          verbose=verbose)


def get_agency(contract,
               contracting=True,
               name=True,
               lower=True,
               verbose=False):
    '''Returns the agency of the contract.'''
    purchaser_info = get_purchaser_information(contract)
    if not purchaser_info:
        return None

    key_prefix = 'contractingOffice' if contracting else 'fundingRequesting'
    key = '{}AgencyID'.format(key_prefix)
    if not key in purchaser_info:
        return None

    info = purchaser_info[key]
    agency = ''
    if name:
        if '@name' in info:
            agency = info['@name']
        elif 'name' in info:
            agency = info['name']
        else:
            if verbose:
                print('No "name" or "@name" in agency: {}'.format(info))
    else:
        if '#text' in info:
            agency = info['#text']
        else:
            if verbose:
                print('No "#text" in agency: {}'.format(info))
            agency = None

    return lower_if_exists(agency) if lower else agency


# This routine is now deprecated.
def get_contracting_agency(contract, lower=True, verbose=False):
    '''Returns the contracting agency of the contract.'''
    return get_agency(contract, contracting=True, lower=lower, verbose=verbose)


# This routine is now deprecated.
def get_funding_agency(contract, lower=True, verbose=False):
    '''Returns the funding agency of the contract.'''
    return get_agency(contract,
                      contracting=False,
                      lower=lower,
                      verbose=verbose)


def get_office(contract,
               contracting=True,
               name=True,
               lower=True,
               verbose=False):
    '''Returns the office of the contract.'''
    purchaser_info = get_purchaser_information(contract)
    if not purchaser_info:
        return None

    key_prefix = 'contracting' if contracting else 'fundingRequesting'
    key = '{}OfficeID'.format(key_prefix)
    if not key in purchaser_info:
        return None

    info = purchaser_info[key]
    if name:
        if '@name' in info:
            office = info['@name']
        elif 'name' in info:
            office = info['name']
        else:
            if verbose:
                print('No "name" or "@name" in office: {}'.format(info))
            office = None
    else:
        if '#text' in info:
            office = info['#text']
        else:
            if verbose:
                print('No "#text" in office: {}'.format(info))
            office = None

    return lower_if_exists(office) if lower else office


# This routine is now deprecated.
def get_contracting_office(contract, lower=True, verbose=False):
    '''Returns the contracting office of the contract.'''
    return get_office(contract,
                      contracting=True,
                      name=True,
                      lower=lower,
                      verbose=verbose)


# This routine is now deprecated.
def get_funding_office(contract, lower=True, verbose=False):
    '''Returns the funding office of the contract.'''
    return get_office(contract,
                      contracting=False,
                      name=True,
                      lower=lower,
                      verbose=verbose)


def get_contract_action_type(contract):
    '''Returns the type of the contracts (e.g., Delivery Order or OTA).'''
    detail = get_contract_detail(contract)
    if detail is None or 'contractData' not in detail:
        return None
    contract_data = detail['contractData']
    if not 'contractActionType' in contract_data:
        return None
    action_type = contract_data['contractActionType']
    key = '@description'
    return action_type[key].lower() if key in action_type else None


def get_product_or_service_information(contract):
    detail = get_contract_detail(contract)
    if detail is None:
        return None
    key = 'productOrServiceInformation'
    return detail[key] if key in detail else None


def get_principal_naics_code(contract):
    info = get_product_or_service_information(contract)
    if info is None or 'principalNAICSCode' not in info:
        return None
    code = info['principalNAICSCode']
    key = '#text'
    return code[key] if key in code else None


def get_principal_naics_description(contract):
    info = get_product_or_service_information(contract)
    if info is None or 'principalNAICSCode' not in info:
        return None
    code = info['principalNAICSCode']
    key = '@description'
    return code[key] if key in code else None


def get_product_or_service_code(contract):
    info = get_product_or_service_information(contract)
    if info is None or 'productOrServiceCode' not in info:
        return None
    code = info['productOrServiceCode']
    key = '#text'
    return code[key] if key in code else None


def get_product_or_service_description(contract):
    info = get_product_or_service_information(contract)
    if info is None or 'productOrServiceCode' not in info:
        return None
    code = info['productOrServiceCode']
    key = '@description'
    return code[key] if key in code else None


def get_product_or_service_type(contract):
    info = get_product_or_service_information(contract)
    if info is None or 'productOrServiceCode' not in info:
        return None
    code = info['productOrServiceCode']
    key = '@productOrServiceType'
    return code[key] if key in code else None


def get_vendor_site_details(contract):
    '''Returns vendorSiteDetails object of contract.'''
    detail = get_contract_detail(contract)
    if detail is None or 'vendor' not in detail:
        return None
    vendor_object = detail['vendor']
    return (vendor_object['vendorSiteDetails']
            if 'vendorSiteDetails' in vendor_object else None)


def get_contractor_name(contract, lower=True):
    '''Returns contractorName string of contract.'''
    detail = get_contract_detail(contract)
    if detail is None or 'vendor' not in detail:
        return None
    vendor_object = detail['vendor']
    contractor = (vendor_object['contractorName']
                  if 'contractorName' in vendor_object else None)
    if contractor:
        contractor = simplify_string(contractor)
        if lower:
            contractor = contractor.lower()
    return contractor


def get_vendor_socioeconomic_indicators(contract):
    '''Returns socioeconomic indicators object.'''
    details = get_vendor_site_details(contract)
    if details is None:
        return None

    key = 'vendorSocioEconomicIndicators'
    return details[key] if key in details else None


def get_entity_identifiers(contract):
    '''Returns entityIdentifiers object of the contract.'''
    details = get_vendor_site_details(contract)
    if details is None:
        return None

    key = 'entityIdentifiers'
    return details[key] if key in details else None


def get_cage_code(contract, lower=True):
    '''Returns the CAGE code of the vendor handling the contract.'''
    ids = get_entity_identifiers(contract)
    if ids is None:
        return None

    key = 'cageCode'
    code = ids[key] if key in ids else None
    return lower_if_exists(code) if lower else code


def get_unique_entity_identifiers(contract):
    '''Returns all of the UEI data of the vendor handling the contract.'''
    ids = get_entity_identifiers(contract)
    if ids is None:
        return None

    key = 'vendorUEIInformation'
    return ids[key] if key in ids else None


def get_uei(contract, lower=True):
    '''Returns the UEI for the vendor handling the contract.'''
    ids = get_unique_entity_identifiers(contract)
    if ids is None:
        return None

    key = 'UEI'
    uei = ids[key] if key in ids else None
    return lower_if_exists(uei) if lower else uei


def get_vendor(contract, use_uei=True, lower=True):
    '''Returns the legal vendor name for the contract.'''
    if use_uei:
        ids = get_unique_entity_identifiers(contract)
        key = 'UEILegalBusinessName'
        if ids is not None and key in ids:
            vendor = simplify_string(ids[key])
            return lower_if_exists(vendor) if lower else vendor

    detail = get_contract_detail(contract)
    if detail is None or 'vendor' not in detail:
        return None
    vendor_object = detail['vendor']
    if not 'vendorHeader' in vendor_object:
        return None
    header = vendor_object['vendorHeader']
    if not header:
        return None

    key = 'vendorName'
    backup_key = 'vendorLegalOrganizationName'
    backup_backup_key = 'vendorDoingAsBusinessName'
    if key in header:
        vendor = header[key]
    elif backup_key in header:
        vendor = header[backup_key]
    elif backup_backup_key in header:
        vendor = header[backup_backup_key]
    else:
        print('No vendor names in {}'.format(header))
        vendor = None

    if vendor:
        vendor = simplify_string(vendor)
    else:
        vendor = get_ultimate_parent(contract, lower=lower)
        vendor = simplify_string(vendor)
    vendor = lower_if_exists(vendor) if lower else vendor

    return vendor


def get_ultimate_parent(contract, lower=True):
    '''Returns the name of the ultimate parent of the vendor.'''
    ids = get_unique_entity_identifiers(contract)
    if ids is None:
        return None

    key = 'ultimateParentUEIName'
    value = ids[key] if key in ids else None
    return lower_if_exists(value) if lower else value


def get_ultimate_parent_uei(contract, lower=True):
    '''Returns the UEI for the ultimate parent of the vendor.'''
    ids = get_unique_entity_identifiers(contract)
    if ids is None:
        return None

    key = 'ultimateParentUEI'
    uei = ids[key] if key in ids else None
    return lower_if_exists(uei) if lower else uei


def get_vendor_location(contract):
    '''Returns vendorLocation object of the contract.'''
    details = get_vendor_site_details(contract)
    if details is None:
        return None

    key = 'vendorLocation'
    return details[key] if key in details else None


def retrieve_offset(offset,
                    start_date,
                    end_date,
                    date_prefix,
                    use_deleted=False,
                    verify=False):
    offset_awards = []
    try:
        feed_url = FPDS_DELETED_URL if use_deleted else FPDS_BASE_URL
        url = '{}&q={}:[{}, {}]&start={}'.format(feed_url, date_prefix,
                                                 start_date, end_date, offset)

        node_start = time.time()
        response = requests.get(url, verify=verify)
        data = xmltodict.parse(response.text,
                               process_namespaces=True,
                               namespaces=FPDS_NAMESPACE_SINK)
        node_time = time.time() - node_start

        if not 'feed' in data:
            print('WARNING: Missing \'feed\' key in data.')
            print(data)
            del data
            return offset_awards
        if not 'entry' in data['feed']:
            print('WARNING: Missing \'entry\' key in data[\'feed\'].')
            del data
            return offset_awards
        entries = data['feed']['entry']
        if not isinstance(entries, list):
            entries = [entries]
        offset_awards = entries.copy()
        del data
    except Exception as e:
        print('Exception in retrieval for offset {}'.format(offset))
        print(e)
    return offset_awards


# The 'map' function cannot make use of a local equivalent of this function due
# to the requirements of 'pickle'.
def _retrieve_offset_packed(params):
    return retrieve_offset(*params)


num_requests_since_gc_ = 0


def get_awards_over_date_range(start_date,
                               end_date,
                               use_modification_date=True,
                               use_deleted=False,
                               num_previous_awards=0,
                               num_processes=NUM_PROCESSES,
                               num_requests_per_gc=50,
                               pause_in_seconds_between_requests=0,
                               verbose=False,
                               verify=False):
    '''Returns list of JSON representations of FPDS awards in date range.

    Args:
      start_date: The date string the search window should begin with.
      end_date: The date string the search window should end with.
      use_modification_date: Whether awards modified during said day -- as
          opposed to signed -- should be downloaded.
      use_deleted: Whether to retrieve deleted awards.
      num_previous_awards: The number of previous awards available for
          this date range. If this number is greater than or equal to the
          number that would be downloaded, we immediately return.
      num_processes: The number of concurrent processes to download via.
          FPDS policy allows up to 10 concurrent threads/processes.
      num_requests_per_gc: The number of request calls before a garbage collect.

    Returns:
      A list of JSON representations of FPDS awards in the given date range.
    '''
    global num_requests_since_gc_

    if use_modification_date:
        date_prefix = 'LAST_MOD_DATE'
    else:
        date_prefix = 'SIGNED_DATE'

    feed_url = FPDS_DELETED_URL if use_deleted else FPDS_BASE_URL
    url = '{}&q={}:[{}, {}]&start={}'.format(feed_url, date_prefix, start_date,
                                             end_date, 0)
    if verbose:
        print('  Retrieving {}'.format(url))
    data = xmltodict.parse(requests.get(url, verify=verify).text)

    # Extract the offset range from the URL.
    feed_links = []
    if isinstance(data['feed']['link'], list):
        feed_links = data['feed']['link'].copy()
    else:
        print('The only feed link was: {}'.format(data['feed']['link']))
    del data
    offset_range_start = 0
    offset_range_end = 10
    for feed_link in feed_links:
        if feed_link['@rel'] == 'last':
            offset_range_end = int(
                parse_qs(urlparse(feed_link['@href']).query)['start'][0])
    offset_stride = 10
    offset_range = range(offset_range_start, offset_range_end + 1,
                         offset_stride)

    awards = []
    num_new_awards_lower_bound = (offset_range_end - offset_range_start) + 1
    if num_previous_awards > 0:
        if num_new_awards_lower_bound <= num_previous_awards:
            print(
                'Early-exiting: there were {} previous awards and we could only guarantee {} replacements.'
                .format(num_previous_awards, num_new_awards_lower_bound))
            return awards
        else:
            print('Extending from {} to at least {} awards.'.format(
                num_previous_awards, num_new_awards_lower_bound))

    if num_processes > 1:
        p = Pool(processes=num_processes)
        param_range = []
        for offset in offset_range:
            param_range.append(
                [offset, start_date, end_date, date_prefix, use_deleted])
        offset_awards = list(
            tqdm(p.imap(_retrieve_offset_packed, param_range),
                 total=len(param_range)))
        awards = list(itertools.chain.from_iterable(offset_awards))
    else:
        # NOTE: This is a stop-gap solution for now to avoid out-of-memory
        # errors when retrieving days with 70K+ contract modifications.
        with tqdm(total=len(offset_range)) as pbar:
            for offset in offset_range:
                result = retrieve_offset(offset, start_date, end_date,
                                         date_prefix, use_deleted)
                awards += result.copy()
                del result
                pbar.update(1)
                if pause_in_seconds_between_requests > 0:
                    time.sleep(pause_in_seconds_between_requests)
                num_requests_since_gc_ += 1
                if num_requests_since_gc_ >= num_requests_per_gc:
                    num_requests_since_gc_ = 0
                    gc.collect()

    return awards


def write_awards_over_date_range(start_date,
                                 end_date,
                                 filename,
                                 use_modification_date=True,
                                 use_deleted=False,
                                 num_previous_awards=0,
                                 num_processes=NUM_PROCESSES,
                                 pause_in_seconds_between_requests=0,
                                 num_requests_per_gc=50,
                                 max_offsets_per_write=500,
                                 verbose=False,
                                 verify=False):
    '''Writes list of JSON representations of FPDS awards in date range.

    Args:
      start_date: The date string the search window should begin with.
      end_date: The date string the search window should end with.
      filename: The file to write the JSON lines to.
      use_modification_date: Whether awards modified during said day -- as
          opposed to signed -- should be downloaded.
      use_deleted: Whether to retrieve deleted awards.
      num_previous_awards: The number of previous awards available for
          this date range. If this number is greater than or equal to the
          number that would be downloaded, we immediately return.
      num_processes: The number of concurrent processes to download via.
          FPDS policy allows up to 10 concurrent threads/processes.
      num_requests_per_gc: The number of request calls before a garbage collect.
      max_offsets_per_write: The maximum number of requests to run in parallel
          per write.
      verbose: Whether to print progress information
      verify: Whether to ensure the request security can be verified.

    Returns:
      A list of JSON representations of FPDS awards in the given date range.
    '''
    global num_requests_since_gc_

    if use_modification_date:
        date_prefix = 'LAST_MOD_DATE'
    else:
        date_prefix = 'SIGNED_DATE'

    feed_url = FPDS_DELETED_URL if use_deleted else FPDS_BASE_URL
    url = '{}&q={}:[{}, {}]&start={}'.format(feed_url, date_prefix, start_date,
                                             end_date, 0)
    if verbose:
        print('  Retrieving {}'.format(url))
    request_text = requests.get(url, verify=verify).text
    try:
        data = xmltodict.parse(request_text)
    except Exception as e:
        print('Could not parse {} from {}'.format(request_text, url))
        print(e)
        return

    if not 'link' in data['feed']:
        with jsonlines.open(filename, mode='w') as writer:
            with tqdm(total=1) as pbar:
                result = retrieve_offset(0, start_date, end_date, date_prefix,
                                         use_deleted)
                writer.write_all(result)
                del result
                pbar.update(1)
    else:
        # Extract the offset range from the URL.
        feed_links = []
        if isinstance(data['feed']['link'], list):
            feed_links = data['feed']['link'].copy()
        else:
            print('The only feed link was: {}'.format(data['feed']['link']))
        del data
        offset_range_start = 0
        offset_range_end = 10
        for feed_link in feed_links:
            if feed_link['@rel'] == 'last':
                offset_range_end = int(
                    parse_qs(urlparse(feed_link['@href']).query)['start'][0])
        offset_stride = 10
        offset_range = list(
            range(offset_range_start, offset_range_end + 1, offset_stride))

        num_new_awards_lower_bound = (offset_range_end -
                                      offset_range_start) + 1
        if num_previous_awards > 0:
            if num_new_awards_lower_bound <= num_previous_awards:
                print(
                    'Early-exiting: there were {} previous awards and we could only guarantee {} replacements.'
                    .format(num_previous_awards, num_new_awards_lower_bound))
                return
            else:
                print('Extending from {} to at least {} awards.'.format(
                    num_previous_awards, num_new_awards_lower_bound))

        if num_processes > 1:
            num_offsets = len(offset_range)
            p = Pool(processes=num_processes)
            if num_offsets > max_offsets_per_write:
                print(
                    'Will break up {} offsets into pieces'.format(num_offsets))
            with jsonlines.open(filename, mode='w') as writer:
                for offset_beg in range(0, num_offsets, max_offsets_per_write):
                    if num_offsets > max_offsets_per_write:
                        print(' offset_beg: {} of {}'.format(
                            offset_beg, num_offsets))
                    offset_end = offset_beg + max_offsets_per_write
                    param_range = []
                    for offset in offset_range[offset_beg:offset_end]:
                        param_range.append([
                            offset, start_date, end_date, date_prefix,
                            use_deleted
                        ])
                    offset_awards = list(
                        tqdm(p.imap(_retrieve_offset_packed, param_range),
                             total=len(param_range)))
                    awards = list(itertools.chain.from_iterable(offset_awards))
                    writer.write_all(awards)
                    del awards
        else:
            # NOTE: This is a stop-gap solution for now to avoid out-of-memory
            # errors when retrieving days with 70K+ contract modifications.
            with jsonlines.open(filename, mode='w') as writer:
                with tqdm(total=len(offset_range)) as pbar:
                    for offset in offset_range:
                        result = retrieve_offset(offset, start_date, end_date,
                                                 date_prefix, use_deleted)
                        writer.write_all(result)
                        del result
                        pbar.update(1)
                        if pause_in_seconds_between_requests > 0:
                            time.sleep(pause_in_seconds_between_requests)
                        num_requests_since_gc_ += 1
                        if num_requests_since_gc_ >= num_requests_per_gc:
                            num_requests_since_gc_ = 0
                            gc.collect()


def get_day(year,
            month,
            day,
            use_modification_date=True,
            use_deleted=False,
            num_previous_awards=0,
            num_processes=NUM_PROCESSES,
            pause_in_seconds_between_requests=0,
            verbose=False):
    '''Returns list of JSON representations of FPDS awards for a day.

    Args:
      year: The four-digit string of the year.
      month: The two-digit string (from '01' to '12') of the month.
      day: The two-digit string (from '01' to '31') of the day.
      use_modification_date: Whether awards modified during said day -- as
          opposed to signed -- should be downloaded.
      use_deleted: Whether to retrieve deleted awards.
      num_previous_awards: The number of previous awards available for
          this date range. If this number is greater than or equal to the
          number that would be retrieved, we immediately return.
      num_processes: The number of concurrent processes to retrieve with.
          FPDS policy allows up to 10 concurrent threads/processes.

    Returns:
      A list of JSON representations of FPDS awards on the given day.
    '''
    date = util.dates.tuple_to_date(year, month, day, '/')
    return get_awards_over_date_range(
        date,
        date,
        use_modification_date,
        use_deleted,
        num_previous_awards,
        num_processes,
        pause_in_seconds_between_requests=pause_in_seconds_between_requests,
        verbose=verbose)


def write_day(year,
              month,
              day,
              filename,
              use_modification_date=True,
              use_deleted=False,
              num_previous_awards=0,
              num_processes=NUM_PROCESSES,
              pause_in_seconds_between_requests=0,
              verbose=False):
    '''Returns list of JSON representations of FPDS awards for a day.

    Args:
      year: The four-digit string of the year.
      month: The two-digit string (from '01' to '12') of the month.
      day: The two-digit string (from '01' to '31') of the day.
      filename: The filename to write the JSON lines to.
      use_modification_date: Whether awards modified during said day -- as
          opposed to signed -- should be downloaded.
      use_deleted: Whether to retrieve deleted awards.
      num_previous_awards: The number of previous awards available for
          this date range. If this number is greater than or equal to the
          number that would be retrieved, we immediately return.
      num_processes: The number of concurrent processes to retrieve with.
          FPDS policy allows up to 10 concurrent threads/processes.
    '''
    date = util.dates.tuple_to_date(year, month, day, '/')
    return write_awards_over_date_range(
        date,
        date,
        filename,
        use_modification_date,
        use_deleted,
        num_previous_awards,
        num_processes,
        pause_in_seconds_between_requests=pause_in_seconds_between_requests,
        verbose=verbose)


# The deleted feed endpoint appears to randomly block requests.
def attempt_to_write_deleted_day(year, month, day, use_modification_date=True):
    if use_modification_date:
        filename = 'deleted_fpds/{}-{}-{}.jsonl'.format(
            year,
            str(month).zfill(2),
            str(day).zfill(2))
    else:
        filename = 'deleted_fpds/{}-{}-{}_signed.jsonl'.format(
            year,
            str(month).zfill(2),
            str(day).zfill(2))
    write_day(year,
              month,
              day,
              filename,
              use_modification_date=use_modification_date,
              use_deleted=True,
              num_processes=1,
              pause_in_seconds_between_requests=0)


# The deleted feed endpoint appears to randomly block requests.
def attempt_to_write_deleted_date_range(start_date, end_date):
    write_awards_over_date_range(start_date,
                                 end_date,
                                 'deleted_fpds/{}_to_{}.jsonl'.format(
                                     start_date, end_date),
                                 True,
                                 True,
                                 num_processes=1,
                                 pause_in_seconds_between_requests=0)


def simplify(award):
    '''Returns a simplified version of the given award.

    Args:
      award: The full conversion of the original XML FPDS award into JSON.

    Returns:
      The (subjectively) most relevant subset of the award.
    '''
    piid = get_piid(award)
    mod_number = get_modification_number(award)
    transaction_number = get_transaction_number(award)
    parent_piid = get_parent_piid(award)
    parent_mod_number = get_parent_modification_number(award)
    title = get_title(award)
    link = get_link(award)
    place_of_performance = get_place_of_performance(award)
    contracting_department = get_department(award,
                                            contracting=True,
                                            name=True,
                                            lower=True)
    contracting_department_code = get_department(award,
                                                 contracting=True,
                                                 name=False,
                                                 lower=True)
    contracting_agency = get_agency(award,
                                    contracting=True,
                                    name=True,
                                    lower=True)
    contracting_agency_code = get_agency(award,
                                         contracting=True,
                                         name=False,
                                         lower=True)
    contracting_office = get_office(award,
                                    contracting=True,
                                    name=True,
                                    lower=True)
    contracting_office_code = get_office(award,
                                         contracting=True,
                                         name=False,
                                         lower=True)
    funding_department = get_department(award,
                                        contracting=False,
                                        name=True,
                                        lower=True)
    funding_department_code = get_department(award,
                                             contracting=False,
                                             name=False,
                                             lower=True)
    funding_agency = get_agency(award,
                                contracting=False,
                                name=True,
                                lower=True)
    funding_agency_code = get_agency(award,
                                     contracting=False,
                                     name=False,
                                     lower=True)
    funding_office = get_office(award,
                                contracting=False,
                                name=True,
                                lower=True)
    funding_office_code = get_office(award,
                                     contracting=False,
                                     name=False,
                                     lower=True)
    contract_action_type = get_contract_action_type(award)
    principal_naics_code = get_principal_naics_code(award)
    product_or_service_code = get_product_or_service_code(award)
    vendor = get_vendor(award, lower=True)
    uei = get_uei(award, lower=True)
    cage_code = get_cage_code(award, lower=True)
    ultimate_parent = get_ultimate_parent(award, lower=True)
    ultimate_parent_uei = get_ultimate_parent_uei(award, lower=True)
    contractor = get_contractor_name(award, lower=True)
    signed_date = get_signed_date(award)
    effective_date = get_effective_date(award)
    modified_date = get_modified_date(award)
    current_completion_date = get_current_completion_date(award)
    ultimate_completion_date = get_ultimate_completion_date(award)
    last_date_to_order = get_last_date_to_order(award)
    obligated_amount = get_obligated_amount(award)
    base_and_exercised_options_value = get_base_and_exercised_options_value(
        award)
    base_and_all_options_value = get_base_and_all_options_value(award)
    total_obligated_amount = get_total_obligated_amount(award)
    total_base_and_exercised_options_value = (
        get_total_base_and_exercised_options_value(award))
    total_base_and_all_options_value = get_total_base_and_all_options_value(
        award)
    solicitation_id = get_solicitation_id(award, lower=True)
    description = get_description_of_contract_requirement(award, lower=True)
    major_program_code = get_major_program_code(award)

    # TODO(Jack Poulson): Drop the niac_description field.
    niac_description = get_national_interest_action_code_description(award)
    niac_text = get_national_interest_action_code_text(award)

    simplified_award = {
        'piid': piid,
        'mod_number': mod_number,
        'transaction_number': transaction_number,
        'parent_piid': parent_piid,
        'parent_mod_number': parent_mod_number,
        'title': title,
        'fpds_link': link,
        'place_of_performance': json.dumps(place_of_performance),
        'contracting_department': contracting_department,
        'contracting_department_code': contracting_department_code,
        'contracting_agency': contracting_agency,
        'contracting_agency_code': contracting_agency_code,
        'contracting_office': contracting_office,
        'contracting_office_code': contracting_office_code,
        'funding_department': funding_department,
        'funding_department_code': funding_department_code,
        'funding_agency': funding_agency,
        'funding_agency_code': funding_agency_code,
        'funding_office': funding_office,
        'funding_office_code': funding_office_code,
        'contract_action_type': contract_action_type,
        'principal_naics_code': principal_naics_code,
        'product_or_service_code': product_or_service_code,
        'vendor': vendor,
        'uei': uei,
        'cage_code': cage_code,
        'ultimate_parent': ultimate_parent,
        'ultimate_parent_uei': ultimate_parent_uei,
        'contractor': contractor,
        'signed_date': signed_date,
        'effective_date': effective_date,
        'modified_date': modified_date,
        'current_completion_date': current_completion_date,
        'ultimate_completion_date': ultimate_completion_date,
        'last_date_to_order': last_date_to_order,
        'obligated_amount': obligated_amount,
        'base_and_exercised_options_value': base_and_exercised_options_value,
        'base_and_all_options_value': base_and_all_options_value,
        'total_obligated_amount': total_obligated_amount,
        'total_base_and_exercised_options_value':
        total_base_and_exercised_options_value,
        'total_base_and_all_options_value': total_base_and_all_options_value,
        'description': description,
        'major_program_code': major_program_code,
        'niac_description': niac_description,
        'niac_text': niac_text,
        'solicitation_id': solicitation_id
    }

    return simplified_award
