#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import glob
import jsonlines
import numpy as np
import os
import pandas as pd

import util.postgres

from . import award

TABLE_TYPES = {
    'piid': 'VARCHAR(63)',
    'mod_number': 'VARCHAR(63)',
    'transaction_number': 'INT',
    'parent_piid': 'VARCHAR(63)',
    'parent_mod_number': 'VARCHAR(63)',
    'title': 'VARCHAR(255)',
    'fpds_link': 'VARCHAR(255)',
    'contracting_department': 'VARCHAR(127)',
    'contracting_department_code': 'VARCHAR(4)',
    'contracting_agency': 'VARCHAR(127)',
    'contracting_agency_code': 'VARCHAR(4)',
    'contracting_office': 'VARCHAR(127)',
    'contracting_office_code': 'VARCHAR(6)',
    'funding_department': 'VARCHAR(127)',
    'funding_department_code': 'VARCHAR(4)',
    'funding_agency': 'VARCHAR(127)',
    'funding_agency_code': 'VARCHAR(4)',
    'funding_office': 'VARCHAR(127)',
    'funding_office_code': 'VARCHAR(6)',
    'contract_action_type': 'VARCHAR(63)',
    'vendor': 'VARCHAR(255)',
    'uei': 'VARCHAR(15)',
    'cage_code': 'VARCHAR(7)',
    'ultimate_parent': 'VARCHAR(255)',
    'ultimate_parent_uei': 'VARCHAR(15)',
    'signed_date': 'TIMESTAMP',
    'effective_date': 'TIMESTAMP',
    'modified_date': 'TIMESTAMP',
    'obligated_amount': 'DECIMAL(16,2)',
    'base_and_exercised_options_value': 'DECIMAL(16,2)',
    'base_and_all_options_value': 'DECIMAL(16,2)',
    'total_obligated_amount': 'DECIMAL(16,2)',
    'total_base_and_exercised_options_value': 'DECIMAL(16,2)',
    'total_base_and_all_options_value': 'DECIMAL(16,2)',
    'description': 'TEXT',
    'major_program_code': 'VARCHAR(255)',
    'niac_description': 'VARCHAR(255)',
    'niac_text': 'VARCHAR(255)',
    'place_of_performance': 'JSONB',
    'contractor': 'VARCHAR(255)',
    'principal_naics_code': 'VARCHAR(7)',
    'product_or_service_code': 'VARCHAR(7)',
    'current_completion_date': 'TIMESTAMP',
    'ultimate_completion_date': 'TIMESTAMP',
    'last_date_to_order': 'TIMESTAMP',
    'solicitation_id': 'VARCHAR(63)'
}
UNIQUE_COLUMNS = [
    'vendor', 'uei', 'contractor', 'piid', 'mod_number', 'transaction_number',
    'parent_piid', 'parent_mod_number', 'fpds_link'
]
FORCE_NULL_COLUMNS = [
    'signed_date', 'effective_date', 'modified_date', 'transaction_number',
    'current_completion_date', 'ultimate_completion_date', 'last_date_to_order'
]
TABLE_NAME = 'us_central_procurement'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../../..')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')
STAGING_DIR = os.path.join(WORKSTATION_DIR, 'staging')

EXPORT_CSV = '{}/{}.csv'.format(EXPORT_DIR, TABLE_NAME)
STAGING_JSON = '{}/{}.json'.format(STAGING_DIR, TABLE_NAME)


def export(input_regexp='**.json',
           use_modification_date=True,
           export_csv=EXPORT_CSV,
           staging_json=STAGING_JSON):
    '''Combines the JSON contract objects in file glob into a single file.

    Typically, this routine is used to export a single day's data into a CSV
    for import into Postgres.

    Args:
      input_regexp: The regular expression used to define the input file glob.
      use_modification_date: Whether the modified date cache should be used.
      export_csv: The output CSV of awards to generate.
      staging_json: The location of the intermediate JSON file.
    '''
    if not os.path.exists(EXPORT_DIR):
        os.makedirs(EXPORT_DIR)
    if not os.path.exists(STAGING_DIR):
        os.makedirs(STAGING_DIR)

    USED_DIR = (award.MODIFIED_DIR
                if use_modification_date else award.SIGNED_DIR)
    json_filenames = sorted(glob.glob('{}/{}'.format(USED_DIR, input_regexp)))
    total_extracted = 0
    with jsonlines.open(staging_json, mode='w') as writer:
        for json_filename in json_filenames:
            with jsonlines.open(json_filename) as reader:
                num_extracted = 0
                simplified = []
                for contract in reader:
                    simplified.append(award.simplify(contract))
                    num_extracted += 1
                writer.write_all(simplified)
                print('  Extracted {} awards from {}'.format(
                    num_extracted, json_filename))
                total_extracted += num_extracted

    if not total_extracted:
        return

    df = pd.read_json(staging_json,
                      lines=True,
                      dtype={
                          'transaction_number': int,
                          'principal_naics_code': str,
                          'product_or_service_code': str,
                          'contracting_department_code': str,
                          'contracting_agency_code': str,
                          'contracting_office_code': str,
                          'funding_department_code': str,
                          'funding_agency_code': str,
                          'funding_office_code': str
                      })

    for field, field_type in TABLE_TYPES.items():
        if field_type == 'TIMESTAMP':
            df[field] = pd.to_datetime(df[field], errors='coerce')

    df = util.postgres.preprocess_dataframe(df, unique_columns=UNIQUE_COLUMNS)
    util.postgres.df_to_csv(df, export_csv, columns=TABLE_TYPES.keys())


def create(user=None, password=None, dbname=None, port=None, host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute('''
DO $$ BEGIN
  CREATE TYPE agency_office AS (
   department VARCHAR(255), agency VARCHAR(255), office VARCHAR(255)
  );
EXCEPTION
  WHEN duplicate_object THEN null;
END $$;
    ''')
    cursor.execute(
        util.postgres.create_table_sql(TABLE_NAME,
                                       TABLE_TYPES,
                                       serial_id=True,
                                       constraints={
                                           'unique':
                                           '''
UNIQUE NULLS NOT DISTINCT (
  vendor, uei, contractor, piid, mod_number, transaction_number,
  parent_piid, parent_mod_number, fpds_link)
'''
                                       }))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_primary_index ON {} (id);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_vendor_index ON {} (vendor);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_uei_index ON {} (uei);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_contractor_index ON {} (contractor);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_contracting_department_signed_index ON {} (
    contracting_department_code, signed_date DESC
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_funding_department_signed_index ON {} (
    funding_department_code, signed_date DESC
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_contracting_agency_signed_index ON {} (
    contracting_agency_code, signed_date DESC
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_funding_agency_signed_index ON {} (
    funding_agency_code, signed_date DESC
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_contracting_office_signed_index ON {} (
    contracting_office_code, signed_date DESC
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_funding_office_signed_index ON {} (
    funding_office_code, signed_date DESC
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
ALTER TABLE {} ADD COLUMN IF NOT EXISTS tsvector_col tsvector GENERATED ALWAYS AS (
  to_tsvector('simple',
    coalesce(parent_piid,                                     '') || ' ' ||
    coalesce(lower(title),                                    '') || ' ' ||
    coalesce(translate(description,               '/.', ' '), '') || ' ' ||
    coalesce(contracting_department,                          '') || ' ' ||
    coalesce(contracting_agency,                              '') || ' ' ||
    coalesce(contracting_office,                              '') || ' ' ||
    coalesce(funding_department,                              '') || ' ' ||
    coalesce(funding_agency,                                  '') || ' ' ||
    coalesce(funding_office,                                  '') || ' ' ||
    coalesce(vendor,                                          '') || ' ' ||
    coalesce(uei,                                             '') || ' ' ||
    coalesce(cage_code,                                       '') || ' ' ||
    coalesce(ultimate_parent,                                 '') || ' ' ||
    coalesce(ultimate_parent_uei,                             '') || ' ' ||
    coalesce(contractor,                                      '') || ' ' ||
    coalesce(translate(lower(major_program_code), '/.', ' '), '') || ' ' ||
    coalesce(translate(lower(niac_description),   '/.', ' '), '') || ' ' ||
    coalesce(lower(niac_text),                                '') || ' ' ||
    coalesce(lower(place_of_performance::text),               '') || ' ' ||
    coalesce(lower(solicitation_id),                          '')
  )
) STORED;
    '''.format(TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_tsvector_index ON {} USING GIN (tsvector_col);
    '''.format(TABLE_NAME, TABLE_NAME))


def update(export_csv=EXPORT_CSV,
           user=None,
           password=None,
           dbname=None,
           port=None,
           host=None,
           vacuum=True,
           table=TABLE_NAME,
           tmp_table=TABLE_TMP_NAME):
    create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
