#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import requests

import util.tor

# NOTE: While there is a subaward API here, it was error-prone with incomplete
# results and slow extraction speeds. We ultimately went with bulk downloads
# from https://www.usaspending.gov/download_center/custom_award_data.
BASE_URL = 'https://api.usaspending.gov/api/v2/subawards/'


def api_call(url, args, session=None):
    if session is None:
        return requests.post(url, json=args)
    else:
        return session.post(url, json=args)


def subaward_call(args, session=None):
    return api_call(BASE_URL, args, session)


def get_subawards(page=1, max_pages=1000, limit=25, use_tor=False):
    args = {'limit': limit, 'page': page}
    with (util.tor.get_session()
          if use_tor else requests.session()) as session:
        subawards = []
        try:
            raw_response = subaward_call(args, session)
            response = raw_response.json()
            subawards = response['results']
            num_pages = 0
            while (num_pages < max_pages and 'page_metadata' in response
                   and response['page_metadata']['hasNext']):
                next_page = response['page_metadata']['next']
                print('  Requesting page {}'.format(next_page))
                args['page'] = next_page
                raw_response = subaward_call(args, session)
                try:
                    response = raw_response.json()
                    subawards += response['results']
                except Exception as e:
                    print(e)
                    break
                num_pages += 1
        except Exception as e:
            print(e)
        return subawards
