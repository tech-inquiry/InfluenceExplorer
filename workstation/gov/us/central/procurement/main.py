#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# FPDS provides partial, typically out-of-date dumps of XML data at:
#
#   https://www.fpds.gov/fpdsng_cms/index.php/en/ex.php/archives/18-data-archives
# According to said page,
#
#  "Organization linkage Information is not provided in this archive in order
#  to honor the Federal Government's licensing agreement with Dun & Bradstreet."
#
import datetime
import glob
import jsonlines
import numpy as np
import os
import pandas as pd
import re
from collections import OrderedDict

import util.dates
import util.entities
import util.postgres

from .normalization import *
from . import award
from . import postgres
from . import subawards

# The regular expression to use for globbing award JSON files.
AWARDS_MODIFIED_REGEXP = '{}/**.json'.format(award.MODIFIED_DIR)
AWARDS_SIGNED_REGEXP = '{}/**.json'.format(award.SIGNED_DIR)
AWARDS_ALL_REGEXP = '{}/**/**.json'.format(award.AWARDS_DIR)
AWARDS_EXPORT_REGEXP = AWARDS_SIGNED_REGEXP


def invert_uei_to_ultimate_parents(uei_to_ultimate_parents):
    '''Forms the list of children associated with each ultimate parent.

    Args:
      uei_to_ultimate_parents: The dictionary mapping UEIs to the list of
          ultimate parents they have been associated with.

    Returns:
      A dictionary mapping each UEI to the list of children UEIs which have
      used it as their ultimate parent.
    '''
    uei_to_children = {}
    for child in uei_to_ultimate_parents:
        parents = uei_to_ultimate_parents[child]
        for parent in parents:
            if not parent in uei_to_children:
                uei_to_children[parent] = []
            uei_to_children[parent].append(child)

    return uei_to_children


def convert_uei_keys_to_vendors(uei_dictionary, uei_to_vendors):
    '''Converts a dictionary whose keys are lists of UEIs into vendors.

    Args:
      uei_dictionary: A dictionary whose keys are lists of UEI numbers.
      uei_to_vendors: A dictionary mapping UEIs to the list of vendors which
          have corresponded to that UEI.

    Returns:
      An analogue of the passed-in dictionary whose keys are expanded into the
      list of vendors which have corresponded to a UEI in the key.
    '''
    vendor_dictionary = {}
    for key in uei_dictionary:
        uei_list = uei_dictionary[key]
        vendor_set = set()
        for uei in uei_list:
            vendors = uei_to_vendors[uei]
            for vendor in vendors:
                vendor_set.add(vendor)
        vendor_dictionary[key] = sorted(list(vendor_set))

    # Sort by the number of members.
    vendor_dictionary = sorted(vendor_dictionary.items(),
                               key=lambda x: len(x[1]),
                               reverse=True)
    vendor_dictionary = OrderedDict(vendor_dictionary)

    return vendor_dictionary


def get_product_or_service_codes_over_cache(cache_regexp=AWARDS_SIGNED_REGEXP):
    '''Returns dictionaries mapping product/serve codes to their descriptions.

    Args:
      cache_regexp: The regular expression for the JSON file glob.

    Returns:
      A dictionary of dictionaries mapping codes to associated descriptions:
        - A dictionary from principalNAICSCodes to their descriptions, and
        - A dictionary from productOrServiceCodes to their descriptions.
    '''
    naics_codes = {}
    product_or_service_codes = {}

    json_filenames = sorted(glob.glob(cache_regexp))
    num_filenames = len(json_filenames)
    for i in range(num_filenames):
        json_filename = json_filenames[i]
        print('  Processing file {} of {}: {}'.format(i, num_filenames,
                                                      json_filename))
        with jsonlines.open(json_filename) as reader:
            for contract in reader:
                naics_code = award.get_principal_naics_code(contract)
                naics_description = award.get_principal_naics_description(
                    contract)
                if naics_code not in naics_codes:
                    naics_codes[naics_code] = set()
                naics_codes[naics_code].add(naics_description)

                product_or_service_code = award.get_product_or_service_code(
                    contract)
                product_or_service_description = (
                    award.get_product_or_service_description(contract))
                if product_or_service_code not in product_or_service_codes:
                    product_or_service_codes[product_or_service_code] = set()
                product_or_service_codes[product_or_service_code].add(
                    product_or_service_description)

    for code in naics_codes:
        naics_codes[code] = sorted(list(naics_codes[code]))
    for code in product_or_service_codes:
        product_or_service_codes[code] = sorted(
            list(product_or_service_codes[code]))

    return {
        'naics_codes': naics_codes,
        'product_or_service_codes': product_or_service_codes
    }


def get_offices_over_cache(cache_regexp=AWARDS_ALL_REGEXP,
                           sub_cache_regexp=subawards.SUBAWARDS_JSON):
    '''Returns dict mapping the (department, agency, office) hierarchy.

    Args:
      cache_regexp: The regular expression for the prime award JSON file glob.
      sub_cache_regexp: The regular expression for the subaward JSON file glob.

    Returns:
      A nested dictionary mapping each department to its agencies, and each
      agency to its list of associated offices.

      This is useful for creating normalization maps that correct overly course
      agency assignments, such as an agency of "Defense" and office of
      "Defense Information Systems Agency", which should cause the agency to be
      normalized to DISA.
    '''
    offices = {}
    dept_codes = {}
    agency_codes = {}
    office_codes = {}

    sub_depts = json.load(
        open('{}/us_federal_subaward_depts.json'.format(SHARED_DATA_DIR)))

    # Process the prime awards.
    json_filenames = sorted(glob.glob(cache_regexp))
    num_filenames = len(json_filenames)
    for i in range(num_filenames):
        json_filename = json_filenames[i]
        print('  Processing file {} of {}: {}'.format(i, num_filenames,
                                                      json_filename))
        with jsonlines.open(json_filename) as reader:
            for contract in reader:
                for contracting in [True, False]:
                    dept = award.get_department(contract,
                                                contracting=contracting)
                    dept = util.entities.canonicalize(dept)
                    dept = dept if dept else ''
                    dept_code = award.get_department(contract,
                                                     contracting=contracting,
                                                     name=False)
                    dept_code = dept_code if dept_code else ''
                    agency = award.get_agency(contract,
                                              contracting=contracting)
                    agency = util.entities.canonicalize(agency)
                    agency = agency if agency else ''
                    agency_code = award.get_agency(contract,
                                                   contracting=contracting,
                                                   name=False)
                    agency_code = agency_code if agency_code else ''
                    office = award.get_office(contract,
                                              contracting=contracting)
                    office = util.entities.canonicalize(office)
                    office = office if office else ''
                    office_code = award.get_office(contract,
                                                   contracting=contracting,
                                                   name=False)
                    office_code = office_code if office_code else ''

                    # Store the office
                    if dept not in offices:
                        offices[dept] = {}
                    if agency not in offices[dept]:
                        offices[dept][agency] = set()
                    if office:
                        offices[dept][agency].add(office)
                    # Store the codes
                    if dept not in dept_codes:
                        dept_codes[dept] = set()
                    dept_codes[dept].add(dept_code)
                    if dept not in agency_codes:
                        agency_codes[dept] = {}
                    if agency not in agency_codes[dept]:
                        agency_codes[dept][agency] = set()
                    agency_codes[dept][agency].add(agency_code)
                    if dept not in office_codes:
                        office_codes[dept] = {}
                    if agency not in office_codes[dept]:
                        office_codes[dept][agency] = {}
                    if office not in office_codes[dept][agency]:
                        office_codes[dept][agency][office] = set()
                    office_codes[dept][agency][office].add(office_code)

    # Process the subawards
    json_filenames = sorted(glob.glob(sub_cache_regexp))
    num_filenames = len(json_filenames)
    skipped_sub_depts = set()
    skipped_sub_dept_codes = set()
    for i in range(num_filenames):
        json_filename = json_filenames[i]
        print('  Processing file {} of {}: {}'.format(i, num_filenames,
                                                      json_filename))
        data = json.load(open(json_filename))
        for unique_id, subawards in data.items():
            for subaward in subawards:
                for dept, dept_code, agency, agency_code, office, office_code in [
                    (subaward['prime_funding_agency'],
                     subaward['prime_funding_agency_code'],
                     subaward['prime_funding_sub_agency'],
                     subaward['prime_funding_sub_agency_code'],
                     subaward['prime_funding_office'],
                     subaward['prime_funding_office_code']),
                    (subaward['prime_awarding_agency'],
                     subaward['prime_awarding_agency_code'],
                     subaward['prime_awarding_sub_agency'],
                     subaward['prime_awarding_sub_agency_code'],
                     subaward['prime_awarding_office'],
                     subaward['prime_awarding_office_code'])
                ]:
                    dept = util.entities.canonicalize(dept)
                    dept = dept if dept else ''
                    if dept not in sub_depts['name']:
                        if dept not in skipped_sub_depts:
                            print('Skipping subaward dept {}'.format(dept))
                            skipped_sub_depts.add(dept)
                        continue
                    dept = sub_depts['name'][dept]
                    dept_code = util.entities.canonicalize(dept_code)
                    if dept_code not in sub_depts['code']:
                        if dept_code not in skipped_sub_dept_codes:
                            print(
                                'Skipping sub dept code {}'.format(dept_code))
                            skipped_sub_dept_codes.add(dept_code)
                        continue
                    dept_code = sub_depts['code'][dept_code]
                    agency = util.entities.canonicalize(agency)
                    agency = agency if agency else ''
                    agency_code = util.entities.canonicalize(agency_code)
                    agency_code = agency_code if agency_code else ''
                    office = util.entities.canonicalize(office)
                    office = office if office else ''
                    office_code = util.entities.canonicalize(office_code)
                    office_code = office_code if office_code else ''

                    if not dept:
                        print('Missing dept value:')
                        print('  names: {}, {}, {}'.format(
                            dept, agency, office))
                        print('  codes: {}, {}, {}'.format(
                            dept_code, agency_code, office_code))

                    # Store the office
                    if dept not in offices:
                        offices[dept] = {}
                    if agency not in offices[dept]:
                        offices[dept][agency] = set()
                    if office:
                        offices[dept][agency].add(office)
                    # Store the codes
                    if dept not in dept_codes:
                        dept_codes[dept] = set()
                    dept_codes[dept].add(dept_code)
                    if dept not in agency_codes:
                        agency_codes[dept] = {}
                    if agency not in agency_codes[dept]:
                        agency_codes[dept][agency] = set()
                    agency_codes[dept][agency].add(agency_code)
                    if dept not in office_codes:
                        office_codes[dept] = {}
                    if agency not in office_codes[dept]:
                        office_codes[dept][agency] = {}
                    if office not in office_codes[dept][agency]:
                        office_codes[dept][agency][office] = set()
                    office_codes[dept][agency][office].add(office_code)

    for dept in offices:
        for agency in offices[dept]:
            offices[dept][agency] = sorted(list(offices[dept][agency]))
        offices[dept] = OrderedDict(sorted(offices[dept].items()))
    offices = OrderedDict(sorted(offices.items()))

    for dept in dept_codes:
        dept_codes[dept] = sorted(list(dept_codes[dept]))
    dept_codes = OrderedDict(sorted(dept_codes.items()))

    for dept in agency_codes:
        for agency in agency_codes[dept]:
            agency_codes[dept][agency] = sorted(
                list(agency_codes[dept][agency]))
        agency_codes[dept] = OrderedDict(sorted(agency_codes[dept].items()))
    agency_codes = OrderedDict(sorted(agency_codes.items()))

    for dept in office_codes:
        for agency in office_codes[dept]:
            for office in office_codes[dept][agency]:
                office_codes[dept][agency][office] = sorted(
                    list(office_codes[dept][agency][office]))
            office_codes[dept][agency] = OrderedDict(
                sorted(office_codes[dept][agency].items()))
        office_codes[dept] = OrderedDict(sorted(office_codes[dept].items()))
    office_codes = OrderedDict(sorted(office_codes.items()))

    return {
        'offices': offices,
        'dept_codes': dept_codes,
        'agency_codes': agency_codes,
        'office_codes': office_codes
    }


def get_vendor_award_series_summaries_over_cache(
        cache_regexp=AWARDS_SIGNED_REGEXP):
    '''Returns map from vendor to dict from PIID's to award summaries.

    Args:
      cache_regexp: The regular expression for the JSON file glob.

    Returns:
      A dictionary whose keys are the normalized vendors and values are
      dictionaries mapping Procurement Instrument IDentifiers (PIIDs) to
      award series summaries.
    '''
    # Load a map from vendors to their profile.
    entities = util.entities.get_entities()

    normalization = get_normalization_map(entities)

    vendor_to_summaries = {}

    json_filenames = sorted(glob.glob(cache_regexp))
    num_filenames = len(json_filenames)
    for i in range(num_filenames):
        json_filename = json_filenames[i]
        print('  Processing file {} of {}: {}'.format(i, num_filenames,
                                                      json_filename))
        with jsonlines.open(json_filename) as reader:
            for contract in reader:
                vendor = award.get_vendor(contract)
                uei = award.get_uei(contract)
                ultimate_parent_uei = award.get_ultimate_parent_uei(contract)
                if vendor is None or uei is None:
                    continue

                normalized_vendor = normalize(vendor, uei, normalization)

                piid = award.get_piid(contract)
                mod_number = award.get_modification_number(contract)
                signed_date = award.get_signed_date(contract)
                modified_date = award.get_modified_date(contract)
                total_obligated = award.get_total_obligated_amount(contract)
                total_baeov = award.get_total_base_and_exercised_options_value(
                    contract)
                total_baaov = award.get_total_base_and_all_options_value(
                    contract)
                funding_office = award.get_funding_office(contract)
                funding_agency = award.get_funding_agency(contract)
                contracting_office = award.get_contracting_office(contract)
                contracting_agency = award.get_contracting_agency(contract)

                if normalized_vendor not in vendor_to_summaries:
                    vendor_to_summaries[normalized_vendor] = {}
                summaries = vendor_to_summaries[normalized_vendor]

                if piid in summaries:
                    series = summaries[piid]

                    candidate = [
                        signed_date, mod_number, modified_date,
                        max(float(total_obligated), float(total_baeov),
                            float(total_baaov))
                    ]
                    incumbent = [
                        series['signed_date'], series['mod_number'],
                        series['modified_date'],
                        max(float(series['total_obligated']),
                            float(series['total_baeov']),
                            float(series['total_baaov']))
                    ]

                    if candidate > incumbent:
                        series['signed_date'] = signed_date
                        series['modified_date'] = modified_date
                        series['mod_number'] = mod_number
                        series['total_obligated'] = total_obligated
                        series['total_baeov'] = total_baeov
                        series['total_baaov'] = total_baaov
                        series['contracting_office'] = contracting_office
                        series['contracting_agency'] = contracting_agency
                        series['funding_office'] = funding_office
                        series['funding_agency'] = funding_agency
                else:
                    summaries[piid] = {}
                    series = summaries[piid]
                    series['signed_date'] = signed_date
                    series['mod_number'] = mod_number
                    series['modified_date'] = modified_date
                    series['total_obligated'] = total_obligated
                    series['total_baeov'] = total_baeov
                    series['total_baaov'] = total_baaov
                    series['contracting_office'] = contracting_office
                    series['contracting_agency'] = contracting_agency
                    series['funding_office'] = funding_office
                    series['funding_agency'] = funding_agency

    return vendor_to_summaries


def get_uei_over_cache(cache_regexp=AWARDS_SIGNED_REGEXP):
    '''Returns maps from UEI to vendor strings and UEI to ultimate parent UEI.

    Args:
      cache_regexp: The regular expression for the JSON file glob.

    Returns:
      A nested dictionary containing:
        - A map from UEIs to a list of corresponding normalized vendor string,
        - A map from UEIs to a list of corresponding unnormalized vendor string,
        - A map from UEIs to their list of ultimate parent UEIs, and
        - A map from normalized vendor strings to the most recent known UEIs --
          if any.
        - A map from unnormalized vendor strings to the most recent known UEIs
          -- if any.
    '''
    # Load a map from vendors to their profile.
    entities = util.entities.get_entities()

    normalization = get_normalization_map(entities)

    uei_to_normalized_vendors = {}
    uei_to_vendors = {}
    uei_to_ultimate_parents = {}
    normalized_vendor_to_uei = {}
    vendor_to_uei = {}

    json_filenames = sorted(glob.glob(cache_regexp))
    num_filenames = len(json_filenames)
    for i in range(num_filenames):
        json_filename = json_filenames[i]
        print('  Processing file {} of {}: {}'.format(i, num_filenames,
                                                      json_filename))
        with jsonlines.open(json_filename) as reader:
            for contract in reader:
                vendor = award.get_vendor(contract)
                uei = award.get_uei(contract)
                ultimate_parent_uei = award.get_ultimate_parent_uei(contract)
                if vendor is None or uei is None:
                    continue

                normalized_vendor = normalize(vendor, uei, normalization)

                if not uei in uei_to_normalized_vendors:
                    uei_to_normalized_vendors[uei] = set()
                if not uei in uei_to_vendors:
                    uei_to_vendors[uei] = set()

                uei_to_normalized_vendors[uei].add(normalized_vendor)
                uei_to_vendors[uei].add(vendor)

                if ultimate_parent_uei is not None:
                    if not uei in uei_to_ultimate_parents:
                        uei_to_ultimate_parents[uei] = set()
                    uei_to_ultimate_parents[uei].add(ultimate_parent_uei)

                signed_date = award.get_signed_date(contract)

                if vendor in vendor_to_uei:
                    if signed_date is not None:
                        value = vendor_to_uei[vendor]
                        if value['signed_date'] is None or value[
                                'signed_date'] < signed_date:
                            value['signed_date'] = signed_date
                            value['uei'] = uei
                            vendor_to_uei[vendor] = value
                else:
                    vendor_to_uei[vendor] = {
                        'signed_date': signed_date,
                        'uei': uei
                    }

                if normalized_vendor in normalized_vendor_to_uei:
                    if signed_date is not None:
                        value = normalized_vendor_to_uei[normalized_vendor]
                        if value['signed_date'] is None or value[
                                'signed_date'] < signed_date:
                            value['signed_date'] = signed_date
                            value['uei'] = uei
                            normalized_vendor_to_uei[normalized_vendor] = value
                else:
                    normalized_vendor_to_uei[normalized_vendor] = {
                        'signed_date': signed_date,
                        'uei': uei
                    }

    for uei in uei_to_normalized_vendors:
        uei_to_normalized_vendors[uei] = sorted(
            list(uei_to_normalized_vendors[uei]))
    for uei in uei_to_vendors:
        uei_to_vendors[uei] = sorted(list(uei_to_vendors[uei]))

    uei_to_normalized_vendors = sorted(uei_to_normalized_vendors.items(),
                                       key=lambda x: len(x[1]),
                                       reverse=True)
    uei_to_vendors = sorted(uei_to_vendors.items(),
                            key=lambda x: len(x[1]),
                            reverse=True)

    uei_to_normalized_vendors = OrderedDict(uei_to_normalized_vendors)
    uei_to_vendors = OrderedDict(uei_to_vendors)

    for uei in uei_to_ultimate_parents:
        uei_to_ultimate_parents[uei] = sorted(
            list(uei_to_ultimate_parents[uei]))

    uei_to_children = invert_uei_to_ultimate_parents(uei_to_ultimate_parents)

    uei_to_child_vendors = convert_uei_keys_to_vendors(uei_to_children,
                                                       uei_to_vendors)
    uei_to_child_normalized_vendors = convert_uei_keys_to_vendors(
        uei_to_children, uei_to_normalized_vendors)

    result = {
        'uei_to_ultimate_parents': uei_to_ultimate_parents,
        'uei_to_normalized_vendors': uei_to_normalized_vendors,
        'uei_to_vendors': uei_to_vendors,
        'uei_to_child_vendors': uei_to_child_vendors,
        'uei_to_child_normalized_vendors': uei_to_child_normalized_vendors,
        'normalized_vendor_to_uei': normalized_vendor_to_uei,
        'vendor_to_uei': vendor_to_uei
    }

    return result


def retrieve(date=None,
             use_modification_date=True,
             num_previous_awards=0,
             num_processes=award.NUM_PROCESSES,
             export_csv=True,
             verbose=True):
    '''Stores and exports JSON rep. of FPDS awards for a single day.

    Args:
      date: The date of the day to retreive, e.g., '2021-05-01'. The default
          value is yesterday.
      use_modification_date: Whether awards modified during said day -- as
          opposed to signed -- should be downloaded.
      num_previous_awards: The number of previous awards available for
          this date range. If this number is greater than or equal to the
          number that would be retrieved, we immediately return.
      num_processes: The number of concurrent processes to retrieve with.
          FPDS policy allows up to 10 concurrent threads/processes.
    '''
    if date is None:
        # Get yesterday's date string.
        today = datetime.date.today()
        (year, month, day) = util.dates.previous_day(today.year, today.month,
                                                     today.day)
        date = util.dates.tuple_to_date(year, month, day)

    USED_DIR = award.MODIFIED_DIR if use_modification_date else award.SIGNED_DIR
    year, month, day = util.dates.date_to_tuple(date)
    basename = '{}.json'.format(util.dates.tuple_to_date(
        year, month, day, '.'))
    filename = '{}/{}'.format(USED_DIR, basename)

    num_previous_awards = 0
    if os.path.exists(filename):
        with jsonlines.open(filename) as reader:
            for contract in reader:
                num_previous_awards += 1

    if verbose:
        print('Retrieving {}'.format(filename))
    award.write_day(year,
                    month,
                    day,
                    filename,
                    use_modification_date,
                    num_previous_awards=num_previous_awards,
                    num_processes=num_processes,
                    verbose=verbose)
    if export_csv:
        postgres.export(basename, use_modification_date)


def retrieve_month(year=None,
                   month=None,
                   last_day=None,
                   num_processes=award.NUM_PROCESSES,
                   verbose=False):
    '''Retrieves a month of FPDS awards but does not export to CSV.

    Args:
      year: The integer of the year to retrieve.
      month: The integer (from 1 to 12) of the month to retrieve.
      last_day: The (optional) last day of the month to retrieve.
      num_processes: The number of concurrent processes to retrieve with.
          FPDS policy allows up to 10 concurrent threads/processes.
      verbose: Whether to print logging information.
    '''
    if year is None or month is None:
        today = datetime.date.today()
        year = today.year
        month = today.month
        last_day = today.day
    else:
        if last_day == None:
            last_day = util.dates.num_days_in_month(year, month)

    for day in range(1, last_day + 1):
        date = util.dates.tuple_to_date(year, month, day)
        for use_modification_date in [True, False]:
            retrieve(date,
                     use_modification_date,
                     export_csv=False,
                     num_processes=num_processes,
                     verbose=verbose)


def update_month(year, month, use_modification_date=True, vacuum=False):
    postgres.export('{}.{}*.json'.format(year,
                                         str(month).zfill(2)),
                    use_modification_date=use_modification_date)
    postgres.update(vacuum=vacuum)


def get_prime_contracts_fiscal(year):
    # TODO(Jack Poulson): Also incorporate subaward receipts.
    # SUBAWARD_FILE = '{}/data/us_subawards/contracts/subawards.json'.format(WORKSTATION_DIR)
    SIGNED_DIR = 'data/us_awards/sign_date'
    filenames = []
    for month in range(10, 13):
        file_regexp = '{}/{}.{}.*.json'.format(SIGNED_DIR, year - 1,
                                               str(month).zfill(2))
        filenames += sorted(glob.glob(file_regexp))
    for month in range(1, 10):
        file_regexp = '{}/{}.{}.*.json'.format(SIGNED_DIR, year,
                                               str(month).zfill(2))
        filenames += sorted(glob.glob(file_regexp))
    return filenames


def get_prime_contracts_calendar(year):
    # TODO(Jack Poulson): Also incorporate subaward receipts.
    # SUBAWARD_FILE = '{}/data/us_subawards/contracts/subawards.json'.format(WORKSTATION_DIR)
    SIGNED_DIR = 'data/us_awards/sign_date'
    file_regexp = '{}/{}.*.json'.format(SIGNED_DIR, year)
    print(file_regexp)
    return sorted(glob.glob(file_regexp))


def get_vendor_department_amounts(prime_filenames,
                                  sub_filenames=[],
                                  start_date=None,
                                  end_date=None,
                                  verbose=True):

    def coerce(value, default_value=''):
        return value if value else default_value

    def get_prime_unique_key(contract):
        # We are dropping the parent modification number from the hash, as there
        # was an instance of a Palantir contract modification with the U.S.
        # Coast Guard in which a $6 million obligation was double-counted as
        # a result of double-publication with different parent mod. numbers.
        getters = [
            award.get_vendor, award.get_uei, award.get_contractor_name,
            award.get_piid, award.get_modification_number,
            award.get_transaction_number, award.get_parent_piid,
            #award.get_parent_modification_number,
            award.get_link
        ]
        return ':'.join([coerce(str(f(contract))) for f in getters])

    def get_prime_last_modified(contract):
        return award.get_modified_date(contract)

    def get_sub_unique_key(contract):
        columns = [
            'prime_award_unique_key', 'subaward_number', 'subawardee_name',
            'subaward_action_date'
        ]
        return ':'.join([coerce(contract[column]) for column in columns])

    def get_sub_last_modified(contract):
        return contract['subaward_fsrs_report_last_modified_date']

    print('Number of files: {}'.format(len(prime_filenames)))

    amounts = {}
    entities = util.entities.get_entities()
    normalization = get_normalization_map(entities)
    agency_normalization = get_agency_normalization_map(entities)

    prime_unique_map = {}
    for filename in prime_filenames:
        print('Processing prime file {}'.format(filename))
        with jsonlines.open(filename) as reader:
            dates_set = set()
            for contract in reader:
                unique_key = get_prime_unique_key(contract)
                last_modified = get_prime_last_modified(contract)
                if unique_key in prime_unique_map:
                    old_last_modified = prime_unique_map[unique_key]
                    if last_modified > old_last_modified:
                        if verbose:
                            print(
                                'WARNING: Collision with {}, replacing contract because {} > {}'
                                .format(unique_key, last_modified,
                                        old_last_modified))
                        prime_unique_map[unique_key] = last_modified
                    else:
                        if verbose:
                            print(
                                'WARNING: Collision with {}, skipping contract because {} <= {}'
                                .format(unique_key, last_modified,
                                        old_last_modified))
                        continue
                else:
                    prime_unique_map[unique_key] = last_modified

                signed_date = award.get_signed_date(contract)
                if start_date and signed_date < start_date:
                    if verbose:
                        print('Skipping {}, as it is less than {}'.format(
                            signed_date, start_date))
                    continue
                if end_date and signed_date > end_date:
                    if verbose:
                        print('Skipping {}, as it is greater than {}'.format(
                            signed_date, end_date))
                    continue
                dates_set.add(signed_date)
                obligated = award.get_obligated_amount(contract)
                if obligated == 0.:
                    continue
                vendor = award.get_vendor(contract)
                uei = award.get_uei(contract)
                normalized_vendor = normalize(vendor, uei, normalization)
                vendor_root = util.entities.ultimate_parent(
                    normalized_vendor, entities)
                if vendor_root:
                    if vendor_root not in amounts:
                        amounts[vendor_root] = {
                            'combined': {},
                            'contracting': {},
                            'funding': {},
                            'contracting_prime': {},
                            'funding_prime': {},
                            'prime_ota': {},
                            'contracting_prime_ota': {},
                            'funding_prime_ota': {},
                            'sub': {},
                            'contracting_sub': {},
                            'funding_sub': {},
                            'vendors': set(),
                            'ueis': set()
                        }
                    info = amounts[vendor_root]
                    info['vendors'].add(normalized_vendor)
                    info['ueis'].add(uei)
                    contracting_dept = award.get_department(contract,
                                                            contracting=True)
                    action_type = award.get_contract_action_type(contract)
                    is_ota = action_type.startswith('other transaction')
                    if contracting_dept:
                        dept_code = award.get_department(contract,
                                                         name=False,
                                                         contracting=True)
                        agency_code = award.get_agency(contract,
                                                       name=False,
                                                       contracting=True)
                        office_code = award.get_office(contract,
                                                       name=False,
                                                       contracting=True)
                        agency = normalize_agency(dept_code, agency_code, None,
                                                  agency_normalization)
                        office = normalize_agency(dept_code, agency_code,
                                                  office_code,
                                                  agency_normalization)
                        type_keys = [
                            'combined', 'contracting', 'contracting_prime'
                        ]
                        if is_ota:
                            type_keys.append('prime_ota')
                            type_keys.append('contracting_prime_ota')
                        for type_key in type_keys:
                            if contracting_dept not in info[type_key]:
                                info[type_key][contracting_dept] = 0.
                            info[type_key][contracting_dept] += obligated
                            if 'by_agency' not in info[type_key]:
                                info[type_key]['by_agency'] = {}
                            if agency not in info[type_key]['by_agency']:
                                info[type_key]['by_agency'][agency] = 0.
                            info[type_key]['by_agency'][agency] += obligated
                            if 'by_office' not in info[type_key]:
                                info[type_key]['by_office'] = {}
                            if office not in info[type_key]['by_office']:
                                info[type_key]['by_office'][office] = 0.
                            info[type_key]['by_office'][office] += obligated

                    funding_dept = award.get_department(contract,
                                                        contracting=False)
                    if funding_dept:
                        dept_code = award.get_department(contract,
                                                         name=False,
                                                         contracting=False)
                        agency_code = award.get_agency(contract,
                                                       name=False,
                                                       contracting=False)
                        office_code = award.get_office(contract,
                                                       name=False,
                                                       contracting=False)
                        agency = normalize_agency(dept_code, agency_code, None,
                                                  agency_normalization)
                        office = normalize_agency(dept_code, agency_code,
                                                  office_code,
                                                  agency_normalization)
                        type_keys = ['combined', 'funding', 'funding_prime']
                        if is_ota:
                            type_keys.append('funding_prime_ota')
                            type_keys.append('prime_ota')
                        for type_key in type_keys:
                            if funding_dept not in info[type_key]:
                                info[type_key][funding_dept] = 0.
                            # Avoid double-counting with the contracting department.
                            if not (type_key in ['combined', 'prime_ota']
                                    and contracting_dept == funding_dept):
                                info[type_key][funding_dept] += obligated
                            if 'by_agency' not in info[type_key]:
                                info[type_key]['by_agency'] = {}
                            if agency not in info[type_key]['by_agency']:
                                info[type_key]['by_agency'][agency] = 0.
                            info[type_key]['by_agency'][agency] += obligated
                            if 'by_office' not in info[type_key]:
                                info[type_key]['by_office'] = {}
                            if office not in info[type_key]['by_office']:
                                info[type_key]['by_office'][office] = 0.
                            info[type_key]['by_office'][office] += obligated

            dates = sorted(list(dates_set))
            if len(dates) > 1:
                print(dates)

    missing_sub_depts = set()
    sub_unique_map = {}
    for filename in sub_filenames:
        print('Processing sub file {}'.format(filename))
        df = pd.read_csv(filename)
        data = json.loads(df.to_json(orient='records'))
        for item in data:
            unique_key = get_sub_unique_key(item)
            last_modified = get_sub_last_modified(item)
            if unique_key in sub_unique_map:
                old_last_modified = sub_unique_map[unique_key]
                if last_modified > old_last_modified:
                    if verbose:
                        print(
                            'WARNING: Collision with {}, replacing subcontract because {} > {}'
                            .format(unique_key, last_modified,
                                    old_last_modified))
                    sub_unique_map[unique_key] = last_modified
                else:
                    if verbose:
                        print(
                            'WARNING: Collision with {}, skipping subcontract because {} <= {}'
                            .format(unique_key, last_modified,
                                    old_last_modified))
                    continue
            else:
                sub_unique_map[unique_key] = last_modified

            subawardee = item['subawardee_name']
            subawardee_uei = item['subawardee_uei']
            subaward_amount = item['subaward_amount']
            if subaward_amount == 0.:
                continue
            contracting_agency = item['prime_award_awarding_agency_name']
            funding_agency = item['prime_award_funding_agency_name']
            normalized_subawardee = normalize(subawardee, subawardee_uei,
                                              normalization)
            vendor_root = util.entities.ultimate_parent(
                normalized_subawardee, entities)
            if vendor_root:
                if vendor_root not in amounts:
                    amounts[vendor_root] = {
                        'combined': {},
                        'contracting': {},
                        'funding': {},
                        'contracting_prime': {},
                        'funding_prime': {},
                        'contracting_prime': {},
                        'funding_prime': {},
                        'prime_ota': {},
                        'contracting_prime_ota': {},
                        'funding_prime_ota': {},
                        'sub': {},
                        'contracting_sub': {},
                        'funding_sub': {},
                        'vendors': set(),
                        'ueis': set()
                    }
                info = amounts[vendor_root]
                info['vendors'].add(normalized_subawardee)
                info['ueis'].add(subawardee_uei)
                if contracting_agency in SUB_DEPARTMENT_MAP:
                    dept = SUB_DEPARTMENT_MAP[contracting_agency]
                    dept_code = item['prime_award_awarding_agency_code']
                    agency_code = item['prime_award_awarding_sub_agency_code']
                    office_code = item['prime_award_awarding_office_code']
                    agency = normalize_agency(dept_code, agency_code, None,
                                              agency_normalization)
                    office = normalize_agency(dept_code, agency_code,
                                              office_code,
                                              agency_normalization)
                    type_keys = [
                        'combined', 'contracting', 'sub', 'contracting_sub'
                    ]
                    for type_key in type_keys:
                        if dept not in info[type_key]:
                            info[type_key][dept] = 0.
                        info[type_key][dept] += subaward_amount
                        if 'by_agency' not in info[type_key]:
                            info[type_key]['by_agency'] = {}
                        if agency not in info[type_key]['by_agency']:
                            info[type_key]['by_agency'][agency] = 0.
                        info[type_key]['by_agency'][agency] += subaward_amount
                        if 'by_office' not in info[type_key]:
                            info[type_key]['by_office'] = {}
                        if office not in info[type_key]['by_office']:
                            info[type_key]['by_office'][office] = 0.
                        info[type_key]['by_office'][office] += subaward_amount
                else:
                    if contracting_agency not in missing_sub_depts:
                        print('Missing subaward dept {}'.format(
                            contracting_agency))
                        missing_sub_depts.add(contracting_agency)
                if funding_agency in SUB_DEPARTMENT_MAP:
                    dept = SUB_DEPARTMENT_MAP[funding_agency]
                    dept_code = item['prime_award_funding_agency_code']
                    agency_code = item['prime_award_funding_sub_agency_code']
                    office_code = item['prime_award_funding_office_code']
                    agency = normalize_agency(dept_code, agency_code, None,
                                              agency_normalization)
                    office = normalize_agency(dept_code, agency_code,
                                              office_code,
                                              agency_normalization)
                    type_keys = ['combined', 'funding', 'sub', 'funding_sub']
                    for type_key in type_keys:
                        if dept not in info[type_key]:
                            info[type_key][dept] = 0.
                        # Avoid double-counting with the contracting department.
                        if not (type_key in ['combined', 'sub']
                                and contracting_agency == funding_agency):
                            info[type_key][dept] += subaward_amount
                        if 'by_agency' not in info[type_key]:
                            info[type_key]['by_agency'] = {}
                        if agency not in info[type_key]['by_agency']:
                            info[type_key]['by_agency'][agency] = 0.
                        info[type_key]['by_agency'][agency] += subaward_amount
                        if 'by_office' not in info[type_key]:
                            info[type_key]['by_office'] = {}
                        if office not in info[type_key]['by_office']:
                            info[type_key]['by_office'][office] = 0.
                        info[type_key]['by_office'][office] += subaward_amount
                else:
                    if funding_agency not in missing_sub_depts:
                        print(
                            'Missing subaward dept {}'.format(funding_agency))
                        missing_sub_depts.add(funding_agency)

    for vendor_root in amounts:
        info = amounts[vendor_root]
        info['vendors'] = sorted(list(info['vendors']))
        info['ueis'] = sorted(list(info['ueis']))
    return amounts


def sort_vendor_amounts(amounts, department):
    type_key = 'combined'
    return OrderedDict(
        sorted(amounts.items(),
               key=lambda x: x[1][type_key][department]
               if department in x[1][type_key] else 0.,
               reverse=True))


def combine_vendor_amounts(filenames):
    total_amounts = {}
    for amounts_json in filenames:
        amounts = json.load(open(amounts_json))
        for vendor in amounts:
            info = amounts[vendor]
            if vendor not in total_amounts:
                total_amounts[vendor] = {'combined': {}, 'vendors': set()}
            total_info = total_amounts[vendor]
            for fund_type in [
                    'combined', 'contracting', 'funding', 'contracting_prime',
                    'funding_prime', 'prime_ota', 'contracting_prime_ota',
                    'funding_prime_ota', 'sub', 'contracting_sub',
                    'funding_sub'
            ]:
                if fund_type not in total_info:
                    total_info[fund_type] = {}
                for agency in info[fund_type]:
                    if agency in ['by_agency', 'by_office']:
                        if agency not in total_info[fund_type]:
                            total_info[fund_type][agency] = {}
                        for subagency in info[fund_type][agency]:
                            if subagency not in total_info[fund_type][agency]:
                                total_info[fund_type][agency][subagency] = 0.
                            total_info[fund_type][agency][subagency] += info[
                                fund_type][agency][subagency]
                    else:
                        if agency not in total_info[fund_type]:
                            total_info[fund_type][agency] = 0.
                        total_info[fund_type][agency] += info[fund_type][
                            agency]
            for vendor in info['vendors']:
                total_info['vendors'].add(vendor)
    for vendor in total_amounts:
        info = total_amounts[vendor]
        info['vendors'] = sorted(list(info['vendors']))
    return total_amounts


def generate_top_vendors(amounts_json):
    top_vendors = {}
    amounts = json.load(open(amounts_json))
    for vendor in amounts:
        info = amounts[vendor]
        fund_type = 'combined'
        for agency in info[fund_type]:
            if agency in ['by_agency', 'by_office']:
                for subagency in info[fund_type][agency]:
                    if subagency not in top_vendors:
                        top_vendors[subagency] = {}
                    if vendor not in top_vendors[subagency]:
                        top_vendors[subagency][vendor] = info[fund_type][
                            agency][subagency]
            else:
                if agency not in top_vendors:
                    top_vendors[agency] = {}
                if vendor not in top_vendors[agency]:
                    top_vendors[agency][vendor] = info[fund_type][agency]
    for agency in list(top_vendors.keys()):
        top_vendors[agency] = {
            k: v
            for k, v in sorted(top_vendors[agency].items(),
                               key=lambda item: item[1],
                               reverse=True)
        }
    return top_vendors


# For now, we only ensure that DoD, DHS, DoI, DoJ, and GSA are consistently
# handled across both prime and subawards.
#
# TODO(Jack Poulson): Handle the following agencies:
#
# Agency for International Development (USAID)
# Consumer Financial Protection Bureau (CFPB)
# Consumer Product Safety Commission (CPSC)
# Corporation for National and Community Service (CNCS)
# Corps of Engineers - Civil Works (USACE)
# Department of Agriculture (USDA)
# Department of Commerce (DOC)
# Department of Education (ED)
# Department of Energy (DOE)
# Department of Health and Human Services (HHS)
# Department of Housing and Urban Development (HUD)
# Department of Labor (DOL)
# Department of State (DOS)
# Department of the Treasury (TREAS)
# Department of Transportation (DOT)
# Department of Veterans Affairs (VA)
# Equal Employment Opportunity Commission (EEOC)
# Environmental Protection Agency (EPA)
# Executive Office of the President (EOP)
# Federal Communications Commission (FCC)
# Government Accountability Office (GAO)
# Millennium Challenge Corporation (MCC)
# National Aeronautics and Space Administration (NASA)
# National Science Foundation (NSF)
# National Transportation Safety Board (NTSB)
# Nuclear Regulatory Commission (NRC)
# Office of Personnel Management (OPM)
# Pension Benefit Guaranty Corporation (PBGC)
# Railroad Retirement Board (RRB)
# Securities and Exchange Commission (SEC)
# Small Business Administration (SBA)
# Social Security Administration (SSA)
# The Judicial Branch (JUD BRANCH)
#
SUB_DEPARTMENT_MAP = {
    'Department of Defense (DOD)': 'dept of defense',
    'Department of Homeland Security (DHS)':
    'homeland security, department of',
    'Department of the Interior (DOI)': 'interior, department of the',
    'Department of Justice (DOJ)': 'justice, department of',
    'General Services Administration (GSA)': 'general services administration'
}


def produce_rankings_from_amounts(amounts,
                                  filename_prefix='obligations',
                                  label='total'):
    DEPARTMENT_TAGS = {
        'dept of defense': 'defense',
        'homeland security, department of': 'dhs'
    }

    amounts_json = '{}-{}.json'.format(filename_prefix, label)
    with open(amounts_json, 'w') as outfile:
        json.dump(amounts, outfile, indent=2)

    for department in DEPARTMENT_TAGS:
        tag = DEPARTMENT_TAGS[department]
        sorted_amounts = sort_vendor_amounts(amounts, department)
        sorted_csv = '{}-{}-{}.csv'.format(filename_prefix, tag, label)
        sorted_json = '{}-{}-{}.json'.format(filename_prefix, tag, label)
        with open(sorted_json, 'w') as outfile:
            json.dump(sorted_amounts, outfile, indent=2)
        amounts_list = []
        for vendor, values in sorted_amounts.items():
            type_key = 'combined'
            ota_type_key = 'prime_ota'
            sub_type_key = 'sub'
            obligated = 0.
            ota_obligated = 0.
            sub_obligated = 0.
            if department in values[type_key]:
                obligated = values[type_key][department]
            if ota_type_key in values and department in values[ota_type_key]:
                ota_obligated = values[ota_type_key][department]
            if sub_type_key in values and department in values[sub_type_key]:
                sub_obligated = values[sub_type_key][department]
            if obligated == 0. and sub_obligated == 0.:
                continue
            item = {
                'name': vendor,
                'obligated': obligated,
                'ota_obligated': ota_obligated,
                'sub_obligated': sub_obligated,
                'vendors': json.dumps(values['vendors'])
            }
            amounts_list.append(item)
        df = pd.DataFrame(amounts_list)
        df.to_csv(sorted_csv,
                  index=False,
                  escapechar='\\',
                  doublequote=False,
                  encoding='utf-8')


def produce_rankings(filename, filename_prefix='obligations', label='total'):
    amounts = get_vendor_department_amounts(filenames)
    produce_rankings_from_amounts(amounts,
                                  filename_prefix=filename_prefix,
                                  label=label)


def produce_year_rankings(year,
                          calendar=False,
                          filename_prefix='obligations',
                          skip_extraction=False):
    if skip_extraction:
        amounts = json.load(open(amounts_json))
    else:
        prime_filenames = get_prime_contracts_calendar(
            year) if calendar else get_prime_contracts_fiscal(year)
        amounts = get_vendor_department_amounts(prime_filenames)
    produce_rankings_from_amounts(amounts,
                                  filename_prefix=filename_prefix,
                                  label=year)
