#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import geopy
import glob
import json
from geocodio import GeocodioClient

import util.entities

from .normalization import *
from . import award


# The regular expression to use for globbing award JSON files.
AWARDS_MODIFIED_REGEXP = '{}/**.json'.format(award.MODIFIED_DIR)
AWARDS_SIGNED_REGEXP = '{}/**.json'.format(award.SIGNED_DIR)


# See https://www.geocod.io/coverage/ for current coverage information.
# As of April 5, 2020, only the US and major Canadian cities are supported.
GEOCODIO_SUPPORTED_COUNTRIES = [
    'canada',
    'united states',
]


def get_location_over_cache(cache_regexp=AWARDS_SIGNED_REGEXP):
    '''Returns maps from vendor strings to last-known location.

    Args:
      cache_regexp: The regular expression for the JSON file glob.

    Returns:
      A dictionary mapping the normalized vendor string to the pairing of the
      last known location of the vendor and the signing date of the contract it
      was received from.
    '''
    # Load a map from vendors to their profile.
    entities = util.entities.get_entities()

    normalization = get_normalization_map(entities)

    vendor_to_location = {}

    json_filenames = sorted(glob.glob(cache_regexp))
    num_filenames = len(json_filenames)
    for i in range(num_filenames):
        json_filename = json_filenames[i]
        print('  Processing file {} of {}: {}'.format(i, num_filenames,
                                                      json_filename))
        awards = json.load(open(json_filename))
        for contract in awards:
            vendor = award.get_vendor(contract)
            if vendor is None:
                continue
            duns = award.get_duns(contract)

            normalized_vendor = normalize(vendor, duns, normalization)

            signed_date = award.get_signed_date(contract)
            if signed_date is None:
                continue
            location = award.get_vendor_location(contract)
            if location is None:
                continue

            if normalized_vendor in vendor_to_location:
                value = vendor_to_location[normalized_vendor]
                if value['signed_date'] < signed_date:
                    value['signed_date'] = signed_date
                    value['vendor_location'] = location
                    vendor_to_location[normalized_vendor] = value
            else:
                vendor_to_location[normalized_vendor] = {
                    'signed_date': signed_date,
                    'vendor_location': location
                }

    return vendor_to_location


def location_to_vendors_over_cache(cache_regexp=AWARDS_SIGNED_REGEXP):
    '''Returns maps from location strings to associated vendor names.

    Args:
      cache_regexp: The regular expression for the JSON file glob.

    Returns:
      A dictionary mapping the location string to the associated unnormalized
      vendor, DUNS, and normalized vendor.
    '''
    # Load a map from vendors to their profile.
    entities = util.entities.get_entities()

    normalization = get_normalization_map(entities)

    location_to_vendors = {}

    json_filenames = sorted(glob.glob(cache_regexp))
    num_filenames = len(json_filenames)
    for i in range(num_filenames):
        json_filename = json_filenames[i]
        print('  Processing file {} of {}: {}'.format(i, num_filenames,
                                                      json_filename))
        awards = json.load(open(json_filename))
        for contract in awards:
            vendor = award.get_vendor(contract)
            if vendor is None:
                continue
            duns = award.get_duns(contract)

            normalized_vendor = normalize(vendor, duns, normalization)

            location = award.get_vendor_location(contract)
            if location is None:
                continue
            location_str = json.dumps(location)

            if location_str not in location_to_vendors:
                location_to_vendors[location_str] = set()

            location_to_vendors[location_str].add(
                json.dumps({
                    'vendor': vendor,
                    'duns': duns,
                    'normalized_vendor': normalized_vendor
                }))

    for location in list(location_to_vendors.keys()):
        location_to_vendors[location] = list(location_to_vendors[location])

    return location_to_vendors


def short_address_to_vendors_and_socioeconomic_over_cache(
    cache_regexp=AWARDS_SIGNED_REGEXP):
    '''Returns maps from short address strings to associated vendors.

    Args:
      cache_regexp: The regular expression for the JSON file glob.

    Returns:
      A dictionary mapping the location string to the associated unnormalized
      vendor, DUNS, and normalized vendor.
    '''
    # Load a map from vendors to their profile.
    entities = util.entities.get_entities()

    normalization = get_normalization_map(entities)

    short_address_to_vendors = {}

    json_filenames = sorted(glob.glob(cache_regexp))
    num_filenames = len(json_filenames)
    for i in range(num_filenames):
        json_filename = json_filenames[i]
        print('  Processing file {} of {}: {}'.format(i, num_filenames,
                                                      json_filename))
        awards = json.load(open(json_filename))
        for contract in awards:
            vendor = award.get_vendor(contract)
            if vendor is None:
                continue
            duns = award.get_duns(contract)

            normalized_vendor = normalize(vendor, duns, normalization)

            location = award.get_vendor_location(contract)
            if location is None:
                continue
            indicators = award.get_vendor_socioeconomic_indicators(contract)

            KEYS_TO_COPY = ['streetAddress', 'streetAddress2', 'city']
            short_address = {}
            for key in KEYS_TO_COPY:
                if key in location:
                    short_address[key] = location[key]
            short_address_str = json.dumps(short_address)

            if short_address_str not in short_address_to_vendors:
                short_address_to_vendors[short_address_str] = set()

            short_address_to_vendors[short_address_str].add(
                json.dumps({
                    'vendor': vendor,
                    'duns': duns,
                    'normalized_vendor': normalized_vendor,
                    'socioeconomic': indicators
                }))

    for short_address in list(short_address_to_vendors.keys()):
        short_address_to_vendors[short_address] = list(
            short_address_to_vendors[short_address])

    return short_address_to_vendors


def parse_vendor_location(location):
    '''Parses the vendorLocation object into a dictionary.'''
    parsed_location = {}

    if 'streetAddress' in location:
        parsed_location['street_address'] = location['streetAddress'].lower()

    if 'city' in location:
        parsed_location['city'] = location['city'].lower()

    if 'state' in location:
        # Given the non-US example of:
        #
        #  "sk energy corporation": {
        #   "signed_date": "2019-12-27 00:00:00",
        #   "vendor_location": {
        #    "streetAddress": "99 SEORIN-DONG, JONGNO-GU",
        #    "city": "SEOUL",
        #    "state": "SEOUL",
        #    "ZIPCode": {
        #     "@city": "SEOUL",
        #     "#text": "110728"
        #    },
        #    "countryCode": {
        #     "@name": "KOREA, SOUTH",
        #     "#text": "KOR"
        #    }
        #   }
        #  }
        #
        # we must test if the 'state' field is a string or dictionary.
        if type(location['state']) is dict:
            parsed_location['state'] = location['state']['@name'].lower()
        else:
            parsed_location['state'] = location['state'].lower()

    if 'ZIPCode' in location and '#text' in location['ZIPCode']:
        parsed_location['zipcode'] = location['ZIPCode']['#text']

    if 'countryCode' in location:
        if type(location['countryCode']) is dict:
            parsed_location['country'] = location['countryCode']['@name'].lower()
        else:
            parsed_location['country'] = location['countryCode'].lower()

    if 'phoneNo' in location:
        parsed_location['phone_number'] = location['phoneNo']

    if 'faxNo' in location:
        parsed_location['fax_number'] = location['faxNo']

    if 'congressionalDistrictCode' in location:
        parsed_location['congressional_district'] = location[
            'congressionalDistrictCode']

    return parsed_location


def initialize_nominatim_state(user_agent='',
                               num_queries=0,
                               max_queries=2500,
                               cache_filename=None):
    '''Returns a state object for Nominatim forward geocoding.'''
    if not user_agent:
        return None
    state = {}
    state['client'] = geopy.geocoders.Nominatim(user_agent=user_agent)
    state['num_queries'] = num_queries
    state['max_queries'] = max_queries
    if cache_filename:
        state['cache'] = json.load(open(cache_filename))
    else:
        state['cache'] = {}
    return state


def cached_nominatim_query(state, components_data, verbose=True):
    '''Performs a cached forward geocoding call to Nominatim.'''
    if verbose:
        print('Geocoding "{}"'.format(components_data))
    components_str = json.dumps(components_data)
    if components_str in state['cache']:
        if verbose:
            print('"{}" was in Nominatim cache'.format(components_str))
        return state['cache'][components_str]
    else:
        raw_location = state['client'].geocode(components_data)

        location = {}
        if raw_location:
            # We skip the 'point' and 'raw' members.
            location['address'] = raw_location.address
            location['altitude'] = raw_location.altitude
            location['latitude'] = raw_location.latitude
            location['longitude'] = raw_location.longitude

        state['cache'][components_str] = location
        state['num_queries'] += 1
        if verbose:
            print('{}: Geocoded "{}" into "{}" with Nominatim.'.format(
                state['num_queries'], components_str, location))
        return location


def initialize_geocodio_state(api_key='',
                              num_queries=0,
                              max_queries=2500,
                              cache_filename=None):
    '''Returns a state object for geocodio forward geocoding.'''
    if not api_key:
        return None
    state = {}
    state['client'] = GeocodioClient(api_key)
    state['num_queries'] = num_queries
    state['max_queries'] = max_queries
    if cache_filename:
        state['cache'] = json.load(open(cache_filename))
    else:
        state['cache'] = {}
    return state


def cached_geocodio_query(state, components_data, verbose=True):
    '''Performs a cached forward geocoding call to Geocodio.'''
    if ('country' in components_data
            and not components_data['country'].lower() in
            GEOCODIO_SUPPORTED_COUNTRIES):
        country = components_data['country']
        print('Geocodio doesn\'t support {}: {}'.format(
            country, components_data))
        return None

    if verbose:
        print('Geocoding "{}"'.format(components_data))
    components_str = json.dumps(components_data)
    if components_str in state['cache']:
        if verbose:
            print('"{}" was in geocodio cache'.format(components_str))
        return state['cache'][components_str]
    else:
        location = state['client'].geocode(address_date=None,
                                           components_data=components_data)
        state['cache'][components_str] = location
        state['num_queries'] += 1
        if verbose:
            print('{}: Geocoded "{}" into "{}" with geocodio.'.format(
                state['num_queries'], components_str, location))
        return location


def geocode_vendor_location(geocodio_state,
                            nominatim_state,
                            location,
                            enable_nominatim=True):
    '''Uses a mix of Geocodio and Nominatim to perform forward geocoding.

    Args:
      geocodio_state: The state dictionary for Geocodio.
      nominatim_state: The state dictionary for Nominatim.
      location: The original vendorLocation object from an FPDS award.
      enable_nominatim: If nominatim should be used as a fallback.

    Returns:
      A dictionary representing the geocoded the vendor location.
    '''
    geocoded_value = parse_vendor_location(location)
    print('initial parsed value: {}'.format(geocoded_value))
    geocoded_value['latitude'] = None
    geocoded_value['longitude'] = None

    print(geocoded_value)

    if not (('city' in geocoded_value and geocoded_value['city']) or
            ('zipcode' in geocoded_value and geocoded_value['zipcode'])):
        print('Missing city and postal code: cannot geocode.')
        return geocoded_value

    GEOCODIO_MAP = {
        'street_address': 'street',
        'city': 'city',
        'state': 'state',
        'zipcode': 'postal_code',
        'country': 'country'
    }
    geocodio_component_data = {}
    for key, value in GEOCODIO_MAP.items():
        if key in geocoded_value:
            geocodio_component_data[value] = geocoded_value[key]

    if ('country' in geocoded_value
            and geocoded_value['country'] in GEOCODIO_SUPPORTED_COUNTRIES):

        geocoding = None
        try:
            geocoding = cached_geocodio_query(geocodio_state,
                                              geocodio_component_data)
        except Exception as e:
            print('Caught exception: {}'.format(e))

        if geocoding and geocoding['results']:
            coords = geocoding['results'][0]['location']
            geocoded_value['latitude'] = coords['lat']
            geocoded_value['longitude'] = coords['lng']
            return geocoded_value
        else:
            print('Could not geocode with geocodio.')

    if enable_nominatim:
        # Notice that the Nominatim postal code key is 'postalcode', rather than
        # 'postal_code' as used by geocodio.
        nominatim_component_data = {}
        if 'street_address' in geocoded_value:
            nominatim_component_data['street'] = geocoded_value[
                'street_address']
        if 'city' in geocoded_value:
            nominatim_component_data['city'] = geocoded_value['city']
        if 'state' in geocoded_value:
            nominatim_component_data['state'] = geocoded_value['state']
        if 'zipcode' in geocoded_value:
            nominatim_component_data['postal_code'] = geocoded_value['zipcode']
        if 'country' in geocoded_value:
            nominatim_component_data['country'] = geocoded_value['country']

        geocoding = None
        try:
            geocoding = cached_nominatim_query(nominatim_state,
                                               nominatim_component_data)
        except Exception as e:
            print('Caught exception: {}'.format(e))

        if geocoding:
            geocoded_value['latitude'] = geocoding['latitude']
            geocoded_value['longitude'] = geocoding['longitude']
            return geocoded_value

    return geocoded_value


def geocode_vendor_to_location(geocodio_state,
                               nominatim_state,
                               vendor_to_location,
                               geocoded_vendor_cache=None,
                               prioritized_vendors=None,
                               enable_nominatim=True):
    '''Returns a geocoding of a map from vendors to location objects.

    Args:
      geocodio_state: The state dictionary for Geocodio.
      nominatim_state: The state dictionary for Nominatim.
      vendor_to_location: A dictionary mapping FPDS vendors to their most recent
          (as measured by signing date) vendorLocation award object.
      geocoded_vendor_cache: The cache of geocodings of vendor locations.
      prioritized_vendors: A list of vendor strings to process first (before
          switching to the keys of 'vendor_to_location').
      enable_nominatim: If nominatim should be used as a fallback.

    Returns:
      The extension of the 'geocoded_vendor_cache' map to (hopefully) more
      vendors.
    '''
    if geocoded_vendor_cache is None:
        geocoded_vendor_to_location = {}
    else:
        geocoded_vendor_to_location = geocoded_vendor_cache

    try:
        if prioritized_vendors is not None:
            for vendor in prioritized_vendors:
                if vendor in geocoded_vendor_to_location:
                    continue
                if not vendor in vendor_to_location:
                    print('Skipping prioritized vendor "{}"'.format(vendor))
                    continue
                else:
                    print('Processing prioritized vendor "{}"'.format(vendor))

                if geocodio_state['num_queries'] >= geocodio_state[
                        'max_queries']:
                    print('Exhausted user-defined geocodio limit.')
                    return geocoded_vendor_to_location

                value = vendor_to_location[vendor]
                location = value['vendor_location']
                geocoding = geocode_vendor_location(
                    geocodio_state,
                    nominatim_state,
                    location,
                    enable_nominatim=enable_nominatim)
                geocoding['signed_date'] = value['signed_date']
                geocoded_vendor_to_location[vendor] = geocoding

        for vendor in vendor_to_location:
            if vendor in geocoded_vendor_to_location:
                continue

            print('Processing vendor "{}"'.format(vendor))
            if geocodio_state['num_queries'] >= geocodio_state['max_queries']:
                print('Exhausted user-defined geocodio limit.')
                return geocoded_vendor_to_location

            value = vendor_to_location[vendor]
            location = value['vendor_location']
            geocoding = geocode_vendor_location(
                geocodio_state,
                nominatim_state,
                location,
                enable_nominatim=enable_nominatim)
            geocoding['signed_date'] = value['signed_date']
            geocoded_vendor_to_location[vendor] = geocoding

    except Exception as e:
        print('Caught an exception: {}'.format(e))

    return geocoded_vendor_to_location


def convert_vendor_geocodings_to_json_array(geocoded_vendor_to_location):
    '''Converts a dictionary mapping vendors to geocodings to an array.'''
    vendor_geocodings = []
    for vendor in geocoded_vendor_to_location:
        value = geocoded_vendor_to_location[vendor]
        geocoding = value
        geocoding['vendor'] = vendor
        vendor_geocodings.append(geocoding)

    return vendor_geocodings
