#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import os
import pandas as pd
import re
import requests
import trafilatura
import trafilatura.settings
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from tqdm import tqdm

import util.firefox
import util.postgres
import util.tor

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../../..')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

# As of May 15, 2021, this was the number of contract announcement overview
# pages. Obviously, it will increase over time.
MAX_PAGE = 172

MONTHS = {
    'Jan.': '01',
    'Feb.': '02',
    'March': '03',
    'April': '04',
    'May': '05',
    'June': '06',
    'July': '07',
    'Aug.': '08',
    'Sept.': '09',
    'Oct.': '10',
    'Nov.': '11',
    'Dec.': '12'
}

OUTPUT_CSV = '{}/us_dod_announcements.csv'.format(EXPORT_DIR)

TABLE_TYPES = {'article_id': 'INT', 'date': 'TIMESTAMP', 'text': 'TEXT'}
UNIQUE_COLUMNS = ['article_id']
FORCE_NULL_COLUMNS = None
TABLE_NAME = 'us_dod_announcements'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)


def get_announcement(article_id, session=None):
    '''Retrieves a particular DoD contracts announcement from its ID.'''
    trafilatura.settings.TIMEOUT = 10
    url = 'https://www.defense.gov/News/Contracts/Contract/Article/{}/'.format(
        article_id)
    if session == None:
        content = trafilatura.fetch_url(url)
    else:
        content = session.get(url).content
    return trafilatura.extract(content)


def get_metadata_from_url(url, browser):
    '''Retrieves the metadata for a particular URL of DoD announcements.'''
    browser.get(url)
    metadata = []
    for item in browser.find_elements(By.PARTIAL_LINK_TEXT, 'Contracts For'):
        article_date = item.text[14:]
        article_url = item.get_attribute('href')
        article_id = article_url.split('Article/')[1][:-1]
        metadata.append({'date': article_date, 'id': article_id})
    return metadata


def get_metadata_from_page(page, browser):
    '''Retrieves the metadata for a particular page # of DoD announcements.'''
    url = 'https://www.defense.gov/News/Contracts/?Page={}'.format(page)
    return get_metadata_from_url(url, browser)


def get_metadata(max_page=MAX_PAGE):
    '''Retrieves the metadata for DoD contract announcements.'''
    browser = util.firefox.get_browser(headless=True)
    metadata = []
    for page in range(1, max_page + 1):
        metadata += get_metadata_from_page(page, browser)
    return metadata


def get_announcements_from_metadata(metadata, session=None):
    '''Retrieves a dictionary of DoD contract announcements from metadata.'''
    announcements = {}
    with tqdm(total=len(metadata)) as pbar:
        for item in metadata:
            id_str = str(item['id'])
            date = item['date']
            text = get_announcement(id_str, session)
            announcements[id_str] = {'date': date, 'text': text}
            pbar.update(1)
    return announcements


def get_announcements(max_page=MAX_PAGE, use_tor=False):
    '''Retrieves a dictionary of DoD contract announcements.'''
    session = util.tor.get_session() if use_tor else requests.session()
    metadata = get_metadata(max_page)
    return get_announcements_from_metadata(metadata, session)


def standardize_date(natural_date):
    '''Converts a natural language DoD date string into standard form.'''
    date_tokens = natural_date.split(' ')
    month = MONTHS[date_tokens[0]]
    day = date_tokens[1][:-1]
    year = date_tokens[2]
    return '{}-{}-{}'.format(year, month, day)


def export_announcements(announcements, output_csv=OUTPUT_CSV):
    '''Writes out a CSV from a dictionary of DoD announcement extracts.'''
    announcements_list = []
    for article_id in announcements:
        item = announcements[article_id]
        announcements_list.append({
            'id': article_id,
            'date': standardize_date(item['date']),
            'text': item['text']
        })
    df = pd.DataFrame(announcements_list)
    df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d')
    util.postgres.df_to_csv(df, output_csv)


def retrieve(max_page=10, output_csv=OUTPUT_CSV, use_tor=False):
    announcements = get_announcements(max_page=max_page, use_tor=use_tor)
    export_announcements(announcements, output_csv)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute('''
CREATE TABLE IF NOT EXISTS us_dod_announcements(
  id SERIAL PRIMARY KEY,
  article_id INT NOT NULL,
  date TIMESTAMP,
  text TEXT,
  CONSTRAINT unique_us_dod_announcements UNIQUE (article_id)
);

CREATE INDEX IF NOT EXISTS us_dod_announcements_article_id_index ON
  us_dod_announcements (article_id);

CREATE INDEX IF NOT EXISTS us_dod_announcements_search_index ON
  us_dod_announcements USING GIN (to_tsvector('simple', lower(text)));
    ''')


def postgres_update(export_csv=OUTPUT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
