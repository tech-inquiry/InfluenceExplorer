#
# The new approach
# ================
# Several cables were found to have been incomplete when using an HTML parser
# of the published WikiLeaks cache.
#
# We now directly process the CSV file uploaded at:
#   https://archive.org/download/wikileaks-cables-csv
# using the routine original_csv_to_new_csv.
#
#
# The previous approach
# =====================
# Using the most recent release, all cablegates files can be extracted into
# a JSON dictionary keyed on the unique reference ID using:
#
# >>> import glob
# >>> import cablegate
# >>> filenames = glob.glob('cablegate-201108300212/cable/**/**/*.html')
# >>> len(filenames)
# 251287
# >>> cables = cablegate.extract_cables(filenames)
#
# The resulting cable dictionary can then be written into a JSON file via:
#
# >>> import json
# >>> with open('cables.json', 'w') as outfile:
# ...    json.dump(cables, outfile, indent=2)
#
# The full dataset of 251,288 cables can be downloaded directly from WikiLeaks
# at:
#
#   https://file.wikileaks.org/file/cablegate/cablegate-201108300212.7z
#
# The raw data from the cables can then be extracted by passing the recursive
# glob of files within the cable/ folder into extract_cables. The cable with
# a spurious inclusion of an '<' in its HTML is patched automatically.
#
# We note that at least one of these cables appears to have false data that
# even disagrees with what is publicly published on PlusD: the contents of
# 10ASTANA267 appear to be an article summarizing reporting on the cable
# rather than the cable itself. Strangely, WikiLeaks appears to display the
# cable at https://wikileaks.org/plusd/cables/10ASTANA267_a.html.
#
# We therefore patch this artificial cable with the version originally
# published within the 2010-12-15 release
#
#  https://file.wikileaks.org/file/cablegate/cablegate-201012151420.7z
#
import datetime
import os
import pandas as pd

from bs4 import BeautifulSoup
from tqdm import tqdm

import util.postgres

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

TABLE_NAME = 'cablegate'

# These same columns can be extracted from columns 2, 1, 4, 3, 6, 7.
TABLE_TYPES = {
    'reference_id': 'VARCHAR(35)',
    'date': 'TIMESTAMP',
    'classification': 'VARCHAR(127)',
    'embassy': 'VARCHAR(40)',
    'preamble': 'TEXT',
    'text': 'TEXT'
}
UNIQUE_COLUMNS = ['reference_id']

EXPORT_CSV = '{}/{}.csv'.format(EXPORT_DIR, TABLE_NAME)

if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)


def extract_cable(filename, parser='xml'):
    with open(filename, 'r') as file:
        html = file.read()

    # One of the files in Cablegate has invalid HTML, and so we search and
    # replace to prevent it.
    html = html.replace('UAE FTA: UAEG OFFICIALS RAISE CONCERNS< BUT',
                        'UAE FTA: UAEG OFFICIALS RAISE CONCERNS&lt; BUT')
    html = html.replace('< Administrative services >',
                        '&lt; Administrative services &gt;')
    html = html.replace('< Creation of new businesses >',
                        '&lt; Creation of new businesses &gt;')
    html = html.replace('< Education >', '&lt; Education &gt;')
    html = html.replace('< Environment >', '&lt; Environment &gt;')
    html = html.replace('< Government >', '&lt; Government &gt;')
    html = html.replace('< Medial care >', '&lt; Medial care &gt;')
    html = html.replace('<<Noforn>>', '&lt;&lt;Noforn&gt;&gt;')
    html = html.replace('<<sanctions>>', '&lt;&lt;sanctions&gt;&gt;')

    soup = BeautifulSoup(html, parser)

    header = soup.find('table', {'class': 'cable'})
    header_tds = header.find_all('td')
    ref_id = header_tds[0].contents[1].text
    date = header_tds[1].contents[1].text
    classification = header_tds[3].contents[1].text
    embassy = header_tds[4].contents[1].text

    codes = soup.find_all('code')
    if len(codes) != 2:
        print('ERROR: There were {} <code> blocks.'.format(len(codes)))

    cable = {
        'reference_id': ref_id,
        'date': date,
        'classification': classification,
        'embassy': embassy,
        'preamble': codes[0].text,
        'text': codes[1].text
    }

    return cable


def extract_cables(filenames, parser='xml'):
    cables = {}
    with tqdm(total=len(filenames)) as pbar:
        for filename in filenames:
            try:
                cable = extract_cable(filename)
                cables[cable['reference_id']] = cable
            except Exception as e:
                print(e)
                print('Could not parse {}'.format(filename))
            pbar.update(1)
    return cables


def json_to_csv(cables_filename, csv_filename=EXPORT_CSV):
    date_columns = ['date']
    export_columns = list(TABLE_TYPES.keys())

    df = pd.read_json(cables_filename)

    for column in date_columns:
        df[column] = pd.to_datetime(df[column],
                                    format='%Y-%m-%d %H-%M',
                                    errors='coerce')

    df = util.postgres.preprocess_dataframe(df, unique_columns=UNIQUE_COLUMNS)
    util.postgres.df_to_csv(df, csv_filename, columns=export_columns)


# The primary target for this routine is the unzipped 7z file from:
#   https://archive.org/download/wikileaks-cables-csv
def original_csv_to_new_csv(orig_csv, csv_filename=EXPORT_CSV):
    names = [
        'count', 'date', 'reference_id', 'source', 'classification',
        'references', 'preamble', 'text'
    ]
    usecols = [
        'reference_id', 'date', 'classification', 'source', 'preamble', 'text'
    ]
    df = pd.read_csv(orig_csv,
                     header=None,
                     names=names,
                     doublequote=False,
                     escapechar='\\',
                     sep=',',
                     usecols=usecols)[usecols]
    df['date'] = pd.to_datetime(df['date'],
                                format='%m/%d/%Y %H:%M',
                                errors='coerce')
    df = util.postgres.preprocess_dataframe(df, unique_columns=UNIQUE_COLUMNS)
    util.postgres.df_to_csv(df, csv_filename)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TABLE_NAME,
            TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (reference_id)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_embassy_index ON {} (LOWER(embassy));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_reference_id_index ON {} (LOWER(reference_id));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_preamble_search_index ON {} USING GIN (
  to_tsvector('simple', preamble)
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
ALTER TABLE {} ADD COLUMN IF NOT EXISTS tsvector_col tsvector GENERATED ALWAYS AS (
  to_tsvector('simple',
    coalesce(lower(reference_id),   '') || ' ' ||
    coalesce(lower(classification), '') || ' ' ||
    coalesce(lower(embassy),        '') || ' ' ||
    coalesce(lower(preamble),       '') || ' ' ||
    coalesce(lower(text),           '')
  )
) STORED;
    '''.format(TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (tsvector_col);
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=EXPORT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    FORCE_NULL_COLUMNS = ['date']
    TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
