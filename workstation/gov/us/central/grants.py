#
# Copyright (c) 2020-2024 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This utility is used to process grants data from USASpending.gov.
#
import datetime
import glob
import json
import numpy as np
import os
import pandas as pd
import re
import time

import util.dates
import util.postgres

from util.entities import canonicalize, normalize, normalization_map_helper

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
DOWNLOAD_DIR = os.path.join(DATA_DIR, 'us_central_grants')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

OUTPUT_CSV = '{}/us_central_grants.csv'.format(EXPORT_DIR)

if not os.path.exists(DOWNLOAD_DIR):
    os.makedirs(DOWNLOAD_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)

TABLE_NAME = 'us_central_grants'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

COLUMN_NAMES = {
    'assistance_transaction_unique_key': 'transaction_unique_key',
    'assistance_award_unique_key': 'unique_key',
    'award_id_fain': 'id_fain',
    'modification_number': 'mod_number',
    'award_id_uri': 'id_uri',
    'sai_number': 'sai_number',
    'federal_action_obligation': 'federal_obligation',
    'total_obligated_amount': 'total_obligated',
    'total_outlayed_amount_for_overall_award': 'total_outlayed',
    'indirect_cost_federal_share_amount': 'indirect_cost_federal_share',
    'non_federal_funding_amount': 'non_federal_funding',
    'total_non_federal_funding_amount': 'total_non_federal_funding',
    'face_value_of_loan': 'face_value_of_loan',
    'original_loan_subsidy_cost': 'original_loan_subsidy_cost',
    'total_face_value_of_loan': 'total_face_value_of_loan',
    'total_loan_subsidy_cost': 'total_loan_subsidy_cost',
    'generated_pragmatic_obligations': 'pragmatic_obligations',
    'disaster_emergency_fund_codes_for_overall_award': 'emergency_fund_codes',
    'outlayed_amount_from_COVID-19_supplementals_for_overall_award':
    'covid_outlayed',
    'obligated_amount_from_COVID-19_supplementals_for_overall_award':
    'covid_obligated',
    'outlayed_amount_from_IIJA_supplemental_for_overall_award':
    'iija_outlayed',
    'obligated_amount_from_IIJA_supplemental_for_overall_award':
    'iija_obligated',
    'action_date': 'action_date',
    'action_date_fiscal_year': 'action_fiscal_year',
    'period_of_performance_start_date': 'start_date',
    'period_of_performance_current_end_date': 'end_date',
    'awarding_agency_code': 'awarding_agency_code',
    'awarding_agency_name': 'awarding_agency_name',
    'awarding_sub_agency_code': 'awarding_sub_agency_code',
    'awarding_sub_agency_name': 'awarding_sub_agency_name',
    'awarding_office_code': 'awarding_office_code',
    'awarding_office_name': 'awarding_office_name',
    'funding_agency_code': 'funding_agency_code',
    'funding_agency_name': 'funding_agency_name',
    'funding_sub_agency_code': 'funding_sub_agency_code',
    'funding_sub_agency_name': 'funding_sub_agency_name',
    'funding_office_code': 'funding_office_code',
    'funding_office_name': 'funding_office_name',
    'treasury_accounts_funding_this_award': 'treasury_accounts',
    'federal_accounts_funding_this_award': 'federal_accounts',
    'object_classes_funding_this_award': 'object_classes',
    'program_activities_funding_this_award': 'program_activities',
    'recipient_uei': 'recipient_uei',
    'recipient_duns': 'recipient_duns',
    'recipient_name': 'recipient_name',
    'recipient_name_raw': 'recipient_name_raw',
    'recipient_parent_uei': 'recipient_parent_uei',
    'recipient_parent_duns': 'recipient_parent_duns',
    'recipient_parent_name': 'recipient_parent_name',
    'recipient_parent_name_raw': 'recipient_parent_name_raw',
    'recipient_country_code': 'recipient_country_code',
    'recipient_country_name': 'recipient_country_name',
    'recipient_address_line_1': 'recipient_address_line_1',
    'recipient_address_line_2': 'recipient_address_line_2',
    'recipient_city_code': 'recipient_city_code',
    'recipient_city_name': 'recipient_city_name',
    'prime_award_transaction_recipient_county_fips_code':
    'recipient_county_fips',
    'recipient_county_name': 'recipient_county_name',
    'prime_award_transaction_recipient_state_fips_code':
    'recipient_state_fips',
    'recipient_state_code': 'recipient_state_code',
    'recipient_state_name': 'recipient_state_name',
    'recipient_zip_code': 'recipient_zip_code',
    'recipient_zip_last_4_code': 'recipient_zip_last_4',
    'prime_award_transaction_recipient_cd_original': 'recipient_cd_original',
    'prime_award_transaction_recipient_cd_current': 'recipient_cd_current',
    'recipient_foreign_city_name': 'recipient_foreign_city_name',
    'recipient_foreign_province_name': 'recipient_foreign_province_name',
    'recipient_foreign_postal_code': 'recipient_foreign_postal_code',
    'primary_place_of_performance_scope': 'pop_scope',
    'primary_place_of_performance_country_code': 'pop_country_code',
    'primary_place_of_performance_country_name': 'pop_country_name',
    'primary_place_of_performance_code': 'pop_code',
    'primary_place_of_performance_city_name': 'pop_city_name',
    'prime_award_transaction_place_of_performance_county_fips_code':
    'pop_county_fips',
    'primary_place_of_performance_county_name': 'pop_county_name',
    'prime_award_transaction_place_of_performance_state_fips_code':
    'pop_state_fips',
    'primary_place_of_performance_state_name': 'pop_state_name',
    'primary_place_of_performance_zip_4': 'pop_zip_4',
    'prime_award_transaction_place_of_performance_cd_original':
    'pop_cd_original',
    'prime_award_transaction_place_of_performance_cd_current':
    'pop_cd_current',
    'primary_place_of_performance_foreign_location': 'pop_foreign_location',
    'cfda_number': 'cfda_number',
    'cfda_title': 'cfda_title',
    'funding_opportunity_number': 'funding_opportunity_number',
    'funding_opportunity_goals_text': 'funding_opportunity_goals',
    'assistance_type_code': 'assistance_type_code',
    'assistance_type_description': 'assistance_type_description',
    'transaction_description': 'transaction_description',
    'prime_award_base_transaction_description': 'base_transaction_description',
    'business_funds_indicator_code': 'funds_indicator_code',
    'business_funds_indicator_description': 'funds_indicator_description',
    'business_types_code': 'business_types_code',
    'business_types_description': 'business_types_description',
    'correction_delete_indicator_code': 'correction_delete_indicator_code',
    'correction_delete_indicator_description':
    'correction_delete_indicator_description',
    'action_type_code': 'action_type_code',
    'action_type_description': 'action_type_description',
    'record_type_code': 'record_type_code',
    'record_type_description': 'record_type_description',
    'highly_compensated_officer_1_name': 'officer_1_name',
    'highly_compensated_officer_1_amount': 'officer_1_amount',
    'highly_compensated_officer_2_name': 'officer_2_name',
    'highly_compensated_officer_2_amount': 'officer_2_amount',
    'highly_compensated_officer_3_name': 'officer_3_name',
    'highly_compensated_officer_3_amount': 'officer_3_amount',
    'highly_compensated_officer_4_name': 'officer_4_name',
    'highly_compensated_officer_4_amount': 'officer_4_amount',
    'highly_compensated_officer_5_name': 'officer_5_name',
    'highly_compensated_officer_5_amount': 'officer_5_amount',
    'usaspending_permalink': 'usaspending_link',
    'initial_report_date': 'initial_report_date',
    'last_modified_date': 'last_modified_date'
}

DROP_COLUMNS = ['usaspending_link']

DATE_COLUMNS = [
    'action_date', 'start_date', 'end_date', 'initial_report_date',
    'last_modified_date'
]

UNIQUE_COLUMNS = ['unique_key']

FORCE_NULL_COLUMNS = [
    'federal_obligation', 'total_obligated', 'total_outlayed',
    'indirect_cost_federal_share', 'non_federal_funding',
    'total_non_federal_funding', 'face_value_of_loan',
    'original_loan_subsidy_cost', 'action_date', 'covid_outlayed',
    'covid_obligated', 'iija_outlayed', 'iija_obligated', 'start_date',
    'end_date', 'officer_1_amount', 'officer_2_amount', 'officer_3_amount',
    'officer_4_amount', 'officer_5_amount', 'initial_report_date',
    'last_modified_date'
]

TABLE_TYPES = {
    'transaction_unique_key': 'TEXT',
    'unique_key': 'TEXT',
    'id_fain': 'TEXT',
    'mod_number': 'TEXT',
    'id_uri': 'TEXT',
    'sai_number': 'TEXT',
    'federal_obligation': 'DOUBLE PRECISION',
    'total_obligated': 'DOUBLE PRECISION',
    'total_outlayed': 'DOUBLE PRECISION',
    'indirect_cost_federal_share': 'DOUBLE PRECISION',
    'non_federal_funding': 'DOUBLE PRECISION',
    'total_non_federal_funding': 'DOUBLE PRECISION',
    'face_value_of_loan': 'DOUBLE PRECISION',
    'original_loan_subsidy_cost': 'DOUBLE PRECISION',
    'total_face_value_of_loan': 'DOUBLE PRECISION',
    'total_loan_subsidy_cost': 'DOUBLE PRECISION',
    'pragmatic_obligations': 'DOUBLE PRECISION',
    'emergency_fund_codes': 'TEXT',
    'covid_outlayed': 'DOUBLE PRECISION',
    'covid_obligated': 'DOUBLE PRECISION',
    'iija_outlayed': 'DOUBLE PRECISION',
    'iija_obligated': 'DOUBLE PRECISION',
    'action_date': 'TIMESTAMP',
    'action_fiscal_year': 'TEXT',
    'start_date': 'TIMESTAMP',
    'end_date': 'TIMESTAMP',
    'awarding_agency_code': 'TEXT',
    'awarding_agency_name': 'TEXT',
    'awarding_sub_agency_code': 'TEXT',
    'awarding_sub_agency_name': 'TEXT',
    'awarding_office_code': 'TEXT',
    'awarding_office_name': 'TEXT',
    'funding_agency_code': 'TEXT',
    'funding_agency_name': 'TEXT',
    'funding_sub_agency_code': 'TEXT',
    'funding_sub_agency_name': 'TEXT',
    'funding_office_code': 'TEXT',
    'funding_office_name': 'TEXT',
    'treasury_accounts': 'TEXT',
    'federal_accounts': 'TEXT',
    'object_classes': 'TEXT',
    'program_activities': 'TEXT',
    'recipient_uei': 'TEXT',
    'recipient_duns': 'TEXT',
    'recipient_name': 'TEXT',
    'recipient_name_raw': 'TEXT',
    'recipient_parent_uei': 'TEXT',
    'recipient_parent_duns': 'TEXT',
    'recipient_parent_name': 'TEXT',
    'recipient_parent_name_raw': 'TEXT',
    'recipient_country_code': 'TEXT',
    'recipient_country_name': 'TEXT',
    'recipient_address_line_1': 'TEXT',
    'recipient_address_line_2': 'TEXT',
    'recipient_city_code': 'TEXT',
    'recipient_city_name': 'TEXT',
    'recipient_county_fips': 'TEXT',
    'recipient_county_name': 'TEXT',
    'recipient_state_fips': 'TEXT',
    'recipient_state_code': 'TEXT',
    'recipient_state_name': 'TEXT',
    'recipient_zip_code': 'TEXT',
    'recipient_zip_last_4': 'TEXT',
    'recipient_cd_original': 'TEXT',
    'recipient_cd_current': 'TEXT',
    'recipient_foreign_city_name': 'TEXT',
    'recipient_foreign_province_name': 'TEXT',
    'recipient_foreign_postal_code': 'TEXT',
    'pop_scope': 'TEXT',
    'pop_country_code': 'TEXT',
    'pop_country_name': 'TEXT',
    'pop_code': 'TEXT',
    'pop_city_name': 'TEXT',
    'pop_county_fips': 'TEXT',
    'pop_county_name': 'TEXT',
    'pop_state_fips': 'TEXT',
    'pop_state_name': 'TEXT',
    'pop_zip_4': 'TEXT',
    'pop_cd_original': 'TEXT',
    'pop_cd_current': 'TEXT',
    'pop_foreign_location': 'TEXT',
    'cfda_number': 'TEXT',
    'cfda_title': 'TEXT',
    'funding_opportunity_number': 'TEXT',
    'funding_opportunity_goals': 'TEXT',
    'assistance_type_code': 'TEXT',
    'assistance_type_description': 'TEXT',
    'transaction_description': 'TEXT',
    'base_transaction_description': 'TEXT',
    'funds_indicator_code': 'TEXT',
    'funds_indicator_description': 'TEXT',
    'business_types_code': 'TEXT',
    'business_types_description': 'TEXT',
    'correction_delete_indicator_code': 'TEXT',
    'correction_delete_indicator_description': 'TEXT',
    'action_type_code': 'TEXT',
    'action_type_description': 'TEXT',
    'record_type_code': 'TEXT',
    'record_type_description': 'TEXT',
    'officer_1_name': 'TEXT',
    'officer_1_amount': 'DOUBLE PRECISION',
    'officer_2_name': 'TEXT',
    'officer_2_amount': 'DOUBLE PRECISION',
    'officer_3_name': 'TEXT',
    'officer_3_amount': 'DOUBLE PRECISION',
    'officer_4_name': 'TEXT',
    'officer_4_amount': 'DOUBLE PRECISION',
    'officer_5_name': 'TEXT',
    'officer_5_amount': 'DOUBLE PRECISION',
    'initial_report_date': 'TIMESTAMP',
    'last_modified_date': 'TIMESTAMP'
}


def export_df_to_csv(df, export_csv_name):
    df = util.postgres.preprocess_dataframe(df, unique_columns=UNIQUE_COLUMNS)
    util.postgres.df_to_csv(df, export_csv_name, columns=TABLE_TYPES.keys())


def process_csv(orig_csv, output_csv=OUTPUT_CSV):
    df = pd.read_csv(orig_csv,
                     dtype={
                         'funding_agency_code': 'str',
                         'funding_sub_agency_code': 'str',
                         'funding_office_code': 'str',
                         'awarding_agency_code': 'str',
                         'awarding_sub_agency_code': 'str',
                         'awarding_office_code': 'str',
                         'recipient_foreign_postal_code': 'str',
                         'recipient_county_fips': 'str',
                         'recipient_state_fips': 'str',
                         'pop_county_fips': 'str',
                         'pop_state_fips': 'str',
                         'cfda_number': 'str'
                     })
    df = df.rename(columns=COLUMN_NAMES)
    df.drop(columns=DROP_COLUMNS, inplace=True)
    for date_column in DATE_COLUMNS:
        df[date_column] = pd.to_datetime(df[date_column],
                                         format='%Y-%m-%d',
                                         errors='coerce')
    export_df_to_csv(df, output_csv)
    return df


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TABLE_NAME,
            TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE(unique_key)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_recipient_name_index ON {} (lower(coalesce(recipient_name_raw, recipient_name)));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_recipient_parent_name_index ON {} (lower(coalesce(recipient_parent_name_raw, recipient_parent_name)));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_awarding_agency_code_index ON {} (
    awarding_agency_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_funding_agency_code_index ON {} (
    funding_agency_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_awarding_sub_agency_code_index ON {} (
    awarding_sub_agency_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_funding_sub_agency_code_index ON {} (
    funding_sub_agency_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_awarding_office_code_index ON {} (
    awarding_office_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_funding_office_code_index ON {} (
    funding_office_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(transaction_unique_key),          '') || ' ' ||
    coalesce(lower(unique_key),                      '') || ' ' ||
    coalesce(lower(id_fain),                         '') || ' ' ||
    coalesce(lower(id_uri),                          '') || ' ' ||
    coalesce(lower(sai_number),                      '') || ' ' ||
    coalesce(lower(emergency_fund_codes),            '') || ' ' ||
    coalesce(lower(awarding_agency_name),            '') || ' ' ||
    coalesce(lower(awarding_sub_agency_name),        '') || ' ' ||
    coalesce(lower(awarding_office_name),            '') || ' ' ||
    coalesce(lower(funding_agency_name),             '') || ' ' ||
    coalesce(lower(funding_sub_agency_name),         '') || ' ' ||
    coalesce(lower(funding_office_name),             '') || ' ' ||
    coalesce(lower(recipient_uei),                   '') || ' ' ||
    coalesce(lower(recipient_duns),                  '') || ' ' ||
    coalesce(lower(recipient_name),                  '') || ' ' ||
    coalesce(lower(recipient_name_raw),              '') || ' ' ||
    coalesce(lower(recipient_parent_uei),            '') || ' ' ||
    coalesce(lower(recipient_parent_duns),           '') || ' ' ||
    coalesce(lower(recipient_parent_name),           '') || ' ' ||
    coalesce(lower(recipient_parent_name_raw),       '') || ' ' ||
    coalesce(lower(recipient_country_name),          '') || ' ' ||
    coalesce(lower(recipient_address_line_1),        '') || ' ' ||
    coalesce(lower(recipient_address_line_2),        '') || ' ' ||
    coalesce(lower(recipient_city_name),             '') || ' ' ||
    coalesce(lower(recipient_county_name),           '') || ' ' ||
    coalesce(lower(recipient_state_name),            '') || ' ' ||
    coalesce(lower(recipient_zip_code),              '') || ' ' ||
    coalesce(lower(recipient_foreign_city_name),     '') || ' ' ||
    coalesce(lower(recipient_foreign_province_name), '') || ' ' ||
    coalesce(lower(recipient_foreign_postal_code),   '') || ' ' ||
    coalesce(lower(pop_scope),                       '') || ' ' ||
    coalesce(lower(pop_country_name),                '') || ' ' ||
    coalesce(lower(pop_city_name),                   '') || ' ' ||
    coalesce(lower(pop_county_name),                 '') || ' ' ||
    coalesce(lower(pop_state_name),                  '') || ' ' ||
    coalesce(lower(pop_zip_4),                       '') || ' ' ||
    coalesce(lower(pop_foreign_location),            '') || ' ' ||
    coalesce(lower(cfda_title),                      '') || ' ' ||
    coalesce(lower(funding_opportunity_goals),       '') || ' ' ||
    coalesce(lower(assistance_type_description),     '') || ' ' ||
    coalesce(lower(transaction_description),         '') || ' ' ||
    coalesce(lower(base_transaction_description),    '') || ' ' ||
    coalesce(lower(funds_indicator_description),     '') || ' ' ||
    coalesce(lower(business_types_description),      '') || ' ' ||
    coalesce(lower(action_type_description),         '') || ' ' ||
    coalesce(lower(record_type_description),         '') || ' ' ||
    coalesce(lower(officer_1_name),                  '') || ' ' ||
    coalesce(lower(officer_2_name),                  '') || ' ' ||
    coalesce(lower(officer_3_name),                  '') || ' ' ||
    coalesce(lower(officer_4_name),                  '') || ' ' ||
    coalesce(lower(officer_5_name),                  '')));
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=OUTPUT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=None):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['us', 'central', 'grants'], entities)
