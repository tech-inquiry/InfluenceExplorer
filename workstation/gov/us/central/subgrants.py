#
# Copyright (c) 2020-2024 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This utility is used to process grants data from USASpending.gov.
#
import datetime
import glob
import json
import numpy as np
import os
import pandas as pd
import re
import time

import util.dates
import util.postgres

from util.entities import canonicalize, normalize, normalization_map_helper

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
DOWNLOAD_DIR = os.path.join(DATA_DIR, 'us_central_subgrants')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

OUTPUT_CSV = '{}/us_central_subgrants.csv'.format(EXPORT_DIR)

if not os.path.exists(DOWNLOAD_DIR):
    os.makedirs(DOWNLOAD_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)

TABLE_NAME = 'us_central_subgrants'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

COLUMN_NAMES = {
    'prime_award_unique_key': 'prime_unique_key',
    'prime_award_fain': 'prime_fain',
    'prime_award_amount': 'prime_amount',
    'prime_award_disaster_emergency_fund_codes': 'prime_emergency_fund_codes',
    'prime_award_outlayed_amount_from_COVID-19_supplementals':
    'prime_covid_outlayed',
    'prime_award_obligated_amount_from_COVID-19_supplementals':
    'prime_covid_obligated',
    'prime_award_outlayed_amount_from_IIJA_supplemental':
    'prime_iija_outlayed',
    'prime_award_obligated_amount_from_IIJA_supplemental':
    'prime_iija_obligated',
    'prime_award_total_outlayed_amount': 'prime_total_outlayed',
    'prime_award_base_action_date': 'prime_base_action_date',
    'prime_award_base_action_date_fiscal_year':
    'prime_base_action_fiscal_year',
    'prime_award_latest_action_date': 'prime_latest_action_date',
    'prime_award_latest_action_date_fiscal_year':
    'prime_latest_action_fiscal_year',
    'prime_award_period_of_performance_start_date': 'prime_start_date',
    'prime_award_period_of_performance_current_end_date': 'prime_end_date',
    'prime_award_awarding_agency_code': 'prime_awarding_agency_code',
    'prime_award_awarding_agency_name': 'prime_awarding_agency_name',
    'prime_award_awarding_sub_agency_code': 'prime_awarding_sub_agency_code',
    'prime_award_awarding_sub_agency_name': 'prime_awarding_sub_agency_name',
    'prime_award_awarding_office_code': 'prime_awarding_office_code',
    'prime_award_awarding_office_name': 'prime_awarding_office_name',
    'prime_award_funding_agency_code': 'prime_funding_agency_code',
    'prime_award_funding_agency_name': 'prime_funding_agency_name',
    'prime_award_funding_sub_agency_code': 'prime_funding_sub_agency_code',
    'prime_award_funding_sub_agency_name': 'prime_funding_sub_agency_name',
    'prime_award_funding_office_code': 'prime_funding_office_code',
    'prime_award_funding_office_name': 'prime_funding_office_name',
    'prime_award_treasury_accounts_funding_this_award':
    'prime_treasury_accounts',
    'prime_award_federal_accounts_funding_this_award':
    'prime_federal_accounts',
    'prime_award_object_classes_funding_this_award': 'prime_object_classes',
    'prime_award_program_activities_funding_this_award':
    'prime_program_activities',
    'prime_awardee_uei': 'prime_uei',
    'prime_awardee_duns': 'prime_duns',
    'prime_awardee_name': 'prime_name',
    'prime_awardee_dba_name': 'prime_dba',
    'prime_awardee_parent_uei': 'prime_parent_uei',
    'prime_awardee_parent_duns': 'prime_parent_duns',
    'prime_awardee_parent_name': 'prime_parent_name',
    'prime_awardee_country_code': 'prime_country_code',
    'prime_awardee_country_name': 'prime_country_name',
    'prime_awardee_address_line_1': 'prime_address_line_1',
    'prime_awardee_city_name': 'prime_city_name',
    'prime_awardee_county_fips_code': 'prime_county_fips',
    'prime_awardee_county_name': 'prime_county_name',
    'prime_awardee_state_fips_code': 'prime_state_fips',
    'prime_awardee_state_code': 'prime_state_code',
    'prime_awardee_state_name': 'prime_state_name',
    'prime_awardee_zip_code': 'prime_zip_code',
    'prime_award_summary_recipient_cd_original': 'prime_cd_original',
    'prime_award_summary_recipient_cd_current': 'prime_cd_current',
    'prime_awardee_foreign_postal_code': 'prime_foreign_postal_code',
    'prime_awardee_business_types': 'prime_business_types',
    'prime_award_primary_place_of_performance_scope': 'prime_pop_scope',
    'prime_award_primary_place_of_performance_city_name':
    'prime_pop_city_name',
    'prime_award_primary_place_of_performance_county_fips_code':
    'prime_pop_county_fips',
    'prime_award_primary_place_of_performance_county_name':
    'prime_pop_county_name',
    'prime_award_primary_place_of_performance_state_fips_code':
    'prime_pop_state_fips',
    'prime_award_primary_place_of_performance_state_code':
    'prime_pop_state_code',
    'prime_award_primary_place_of_performance_state_name':
    'prime_pop_state_name',
    'prime_award_primary_place_of_performance_address_zip_code':
    'prime_pop_zip_code',
    'prime_award_summary_place_of_performance_cd_original':
    'prime_pop_cd_original',
    'prime_award_summary_place_of_performance_cd_current':
    'prime_pop_cd_current',
    'prime_award_primary_place_of_performance_country_code':
    'prime_pop_country_code',
    'prime_award_primary_place_of_performance_country_name':
    'prime_pop_country_name',
    'prime_award_base_transaction_description':
    'prime_base_transaction_description',
    'prime_award_cfda_numbers_and_titles': 'prime_cfda_numbers_and_titles',
    'subaward_type': 'sub_type',
    'subaward_fsrs_report_id': 'sub_fsrs_report_id',
    'subaward_fsrs_report_year': 'sub_fsrs_report_year',
    'subaward_fsrs_report_month': 'sub_fsrs_report_month',
    'subaward_number': 'sub_number',
    'subaward_amount': 'sub_amount',
    'subaward_action_date': 'sub_action_date',
    'subaward_action_date_fiscal_year': 'sub_action_fiscal_year',
    'subawardee_uei': 'sub_uei',
    'subawardee_duns': 'sub_duns',
    'subawardee_name': 'sub_name',
    'subawardee_dba_name': 'sub_dba',
    'subawardee_parent_uei': 'sub_parent_uei',
    'subawardee_parent_duns': 'sub_parent_duns',
    'subawardee_parent_name': 'sub_parent_name',
    'subawardee_country_code': 'sub_country_code',
    'subawardee_country_name': 'sub_country_name',
    'subawardee_address_line_1': 'sub_address_line_1',
    'subawardee_city_name': 'sub_city_name',
    'subawardee_state_code': 'sub_state_code',
    'subawardee_state_name': 'sub_state_name',
    'subawardee_zip_code': 'sub_zip_code',
    'subaward_recipient_cd_original': 'sub_recipient_cd_original',
    'subaward_recipient_cd_current': 'sub_recipient_cd_current',
    'subawardee_foreign_postal_code': 'sub_foreign_postal_code',
    'subawardee_business_types': 'sub_business_types',
    'subaward_primary_place_of_performance_city_name': 'sub_pop_city_name',
    'subaward_primary_place_of_performance_state_code': 'sub_pop_state_code',
    'subaward_primary_place_of_performance_state_name': 'sub_pop_state_name',
    'subaward_primary_place_of_performance_address_zip_code':
    'sub_pop_zip_code',
    'subaward_place_of_performance_cd_original': 'sub_pop_cd_original',
    'subaward_place_of_performance_cd_current': 'sub_pop_cd_current',
    'subaward_primary_place_of_performance_country_code':
    'sub_pop_country_code',
    'subaward_primary_place_of_performance_country_name':
    'sub_pop_country_name',
    'subaward_description': 'sub_description',
    'subawardee_highly_compensated_officer_1_name': 'sub_officer_1_name',
    'subawardee_highly_compensated_officer_1_amount': 'sub_officer_1_amount',
    'subawardee_highly_compensated_officer_2_name': 'sub_officer_2_name',
    'subawardee_highly_compensated_officer_2_amount': 'sub_officer_2_amount',
    'subawardee_highly_compensated_officer_3_name': 'sub_officer_3_name',
    'subawardee_highly_compensated_officer_3_amount': 'sub_officer_3_amount',
    'subawardee_highly_compensated_officer_4_name': 'sub_officer_4_name',
    'subawardee_highly_compensated_officer_4_amount': 'sub_officer_4_amount',
    'subawardee_highly_compensated_officer_5_name': 'sub_officer_5_name',
    'subawardee_highly_compensated_officer_5_amount': 'sub_officer_5_amount',
    'usaspending_permalink': 'usaspending_link',
    'subaward_fsrs_report_last_modified_date':
    'sub_fsrs_report_last_modified_date'
}

DROP_COLUMNS = ['usaspending_link']

DATE_COLUMNS = [
    'prime_base_action_date', 'prime_start_date', 'prime_end_date',
    'sub_action_date', 'sub_fsrs_report_last_modified_date'
]

UNIQUE_COLUMNS = [
    'prime_unique_key', 'sub_number', 'sub_name', 'sub_action_date'
]

FORCE_NULL_COLUMNS = [
    'prime_amount', 'prime_covid_outlayed', 'prime_covid_obligated',
    'prime_iija_outlayed', 'prime_iija_obligated', 'prime_total_outlayed',
    'prime_base_action_date', 'prime_latest_action_date', 'prime_start_date',
    'prime_end_date', 'sub_amount', 'sub_action_date', 'sub_officer_1_amount',
    'sub_officer_2_amount', 'sub_officer_3_amount', 'sub_officer_4_amount',
    'sub_officer_5_amount', 'sub_fsrs_report_last_modified_date'
]

TABLE_TYPES = {
    'prime_unique_key': 'TEXT',
    'prime_fain': 'TEXT',
    'prime_amount': 'DOUBLE PRECISION',
    'prime_emergency_fund_codes': 'TEXT',
    'prime_covid_outlayed': 'DOUBLE PRECISION',
    'prime_covid_obligated': 'DOUBLE PRECISION',
    'prime_iija_outlayed': 'DOUBLE PRECISION',
    'prime_iija_obligated': 'DOUBLE PRECISION',
    'prime_total_outlayed': 'DOUBLE PRECISION',
    'prime_base_action_date': 'TIMESTAMP',
    'prime_base_action_fiscal_year': 'TEXT',
    'prime_latest_action_date': 'TIMESTAMP',
    'prime_latest_action_fiscal_year': 'TEXT',
    'prime_start_date': 'TIMESTAMP',
    'prime_end_date': 'TIMESTAMP',
    'prime_awarding_agency_code': 'TEXT',
    'prime_awarding_agency_name': 'TEXT',
    'prime_awarding_sub_agency_code': 'TEXT',
    'prime_awarding_sub_agency_name': 'TEXT',
    'prime_awarding_office_code': 'TEXT',
    'prime_awarding_office_name': 'TEXT',
    'prime_funding_agency_code': 'TEXT',
    'prime_funding_agency_name': 'TEXT',
    'prime_funding_sub_agency_code': 'TEXT',
    'prime_funding_sub_agency_name': 'TEXT',
    'prime_funding_office_code': 'TEXT',
    'prime_funding_office_name': 'TEXT',
    'prime_treasury_accounts': 'TEXT',
    'prime_federal_accounts': 'TEXT',
    'prime_object_classes': 'TEXT',
    'prime_program_activities': 'TEXT',
    'prime_uei': 'TEXT',
    'prime_duns': 'TEXT',
    'prime_name': 'TEXT',
    'prime_dba': 'TEXT',
    'prime_parent_uei': 'TEXT',
    'prime_parent_duns': 'TEXT',
    'prime_parent_name': 'TEXT',
    'prime_country_code': 'TEXT',
    'prime_country_name': 'TEXT',
    'prime_address_line_1': 'TEXT',
    'prime_city_name': 'TEXT',
    'prime_county_fips': 'TEXT',
    'prime_county_name': 'TEXT',
    'prime_state_fips': 'TEXT',
    'prime_state_code': 'TEXT',
    'prime_state_name': 'TEXT',
    'prime_zip_code': 'TEXT',
    'prime_cd_original': 'TEXT',
    'prime_cd_current': 'TEXT',
    'prime_foreign_postal_code': 'TEXT',
    'prime_business_types': 'TEXT',
    'prime_pop_scope': 'TEXT',
    'prime_pop_city_name': 'TEXT',
    'prime_pop_county_fips': 'TEXT',
    'prime_pop_county_name': 'TEXT',
    'prime_pop_state_fips': 'TEXT',
    'prime_pop_state_code': 'TEXT',
    'prime_pop_state_name': 'TEXT',
    'prime_pop_zip_code': 'TEXT',
    'prime_pop_cd_original': 'TEXT',
    'prime_pop_cd_current': 'TEXT',
    'prime_pop_country_code': 'TEXT',
    'prime_pop_country_name': 'TEXT',
    'prime_base_transaction_description': 'TEXT',
    'prime_cfda_numbers_and_titles': 'TEXT',
    'sub_type': 'TEXT',
    'sub_fsrs_report_id': 'TEXT',
    'sub_fsrs_report_year': 'TEXT',
    'sub_fsrs_report_month': 'TEXT',
    'sub_number': 'TEXT',
    'sub_amount': 'DOUBLE PRECISION',
    'sub_action_date': 'TIMESTAMP',
    'sub_action_fiscal_year': 'TEXT',
    'sub_uei': 'TEXT',
    'sub_duns': 'TEXT',
    'sub_name': 'TEXT',
    'sub_dba': 'TEXT',
    'sub_parent_uei': 'TEXT',
    'sub_parent_duns': 'TEXT',
    'sub_parent_name': 'TEXT',
    'sub_country_code': 'TEXT',
    'sub_country_name': 'TEXT',
    'sub_address_line_1': 'TEXT',
    'sub_city_name': 'TEXT',
    'sub_state_code': 'TEXT',
    'sub_state_name': 'TEXT',
    'sub_zip_code': 'TEXT',
    'sub_recipient_cd_original': 'TEXT',
    'sub_recipient_cd_current': 'TEXT',
    'sub_foreign_postal_code': 'TEXT',
    'sub_business_types': 'TEXT',
    'sub_pop_city_name': 'TEXT',
    'sub_pop_state_code': 'TEXT',
    'sub_pop_state_name': 'TEXT',
    'sub_pop_zip_code': 'TEXT',
    'sub_pop_cd_original': 'TEXT',
    'sub_pop_cd_current': 'TEXT',
    'sub_pop_country_code': 'TEXT',
    'sub_pop_country_name': 'TEXT',
    'sub_description': 'TEXT',
    'sub_officer_1_name': 'TEXT',
    'sub_officer_1_amount': 'DOUBLE PRECISION',
    'sub_officer_2_name': 'TEXT',
    'sub_officer_2_amount': 'DOUBLE PRECISION',
    'sub_officer_3_name': 'TEXT',
    'sub_officer_3_amount': 'DOUBLE PRECISION',
    'sub_officer_4_name': 'TEXT',
    'sub_officer_4_amount': 'DOUBLE PRECISION',
    'sub_officer_5_name': 'TEXT',
    'sub_officer_5_amount': 'DOUBLE PRECISION',
    'sub_fsrs_report_last_modified_date': 'TIMESTAMP'
}


def export_df_to_csv(df, export_csv_name):
    df = util.postgres.preprocess_dataframe(df, unique_columns=UNIQUE_COLUMNS)
    util.postgres.df_to_csv(df, export_csv_name, columns=TABLE_TYPES.keys())


def process_csv(orig_csv, output_csv=OUTPUT_CSV):
    df = pd.read_csv(orig_csv,
                     dtype={
                         'prime_funding_agency_code': 'str',
                         'prime_funding_sub_agency_code': 'str',
                         'prime_funding_office_code': 'str',
                         'prime_awarding_agency_code': 'str',
                         'prime_awarding_sub_agency_code': 'str',
                         'prime_awarding_office_code': 'str',
                         'prime_foreign_postal_code': 'str',
                         'prime_county_fips': 'str',
                         'prime_state_fips': 'str',
                         'prime_pop_county_fips': 'str',
                         'prime_pop_state_fips': 'str',
                         'prime_cfda_numbers_and_titles': 'str'
                     })
    df = df.rename(columns=COLUMN_NAMES)
    df.drop(columns=DROP_COLUMNS, inplace=True)
    for date_column in DATE_COLUMNS:
        df[date_column] = pd.to_datetime(df[date_column],
                                         format='%Y-%m-%d',
                                         errors='coerce')
    export_df_to_csv(df, output_csv)
    return df


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TABLE_NAME,
            TABLE_TYPES,
            serial_id=True,
            constraints={
                'unique':
                'UNIQUE(prime_unique_key, sub_number, sub_name, sub_action_date)'
            }))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_prime_name_index ON {} (lower(prime_name));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_sub_name_index ON {} (lower(sub_name));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_prime_parent_name_index ON {} (lower(prime_parent_name));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_prime_awarding_agency_code_index ON {} (
    prime_awarding_agency_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_prime_funding_agency_code_index ON {} (
    prime_funding_agency_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_prime_awarding_sub_agency_code_index ON {} (
    prime_awarding_sub_agency_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_prime_funding_sub_agency_code_index ON {} (
    prime_funding_sub_agency_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_prime_awarding_office_code_index ON {} (
    prime_awarding_office_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_prime_funding_office_code_index ON {} (
    prime_funding_office_code
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(prime_unique_key),                   '') || ' ' ||
    coalesce(lower(prime_fain),                         '') || ' ' ||
    coalesce(lower(prime_emergency_fund_codes),         '') || ' ' ||
    coalesce(lower(prime_awarding_agency_name),         '') || ' ' ||
    coalesce(lower(prime_awarding_sub_agency_name),     '') || ' ' ||
    coalesce(lower(prime_awarding_office_name),         '') || ' ' ||
    coalesce(lower(prime_funding_agency_name),          '') || ' ' ||
    coalesce(lower(prime_funding_sub_agency_name),      '') || ' ' ||
    coalesce(lower(prime_funding_office_name),          '') || ' ' ||
    coalesce(lower(prime_program_activities),           '') || ' ' ||
    coalesce(lower(prime_uei),                          '') || ' ' ||
    coalesce(lower(prime_duns),                         '') || ' ' ||
    coalesce(lower(prime_name),                         '') || ' ' ||
    coalesce(lower(prime_dba),                          '') || ' ' ||
    coalesce(lower(prime_parent_uei),                   '') || ' ' ||
    coalesce(lower(prime_parent_duns),                  '') || ' ' ||
    coalesce(lower(prime_parent_name),                  '') || ' ' ||
    coalesce(lower(prime_country_name),                 '') || ' ' ||
    coalesce(lower(prime_address_line_1),               '') || ' ' ||
    coalesce(lower(prime_city_name),                    '') || ' ' ||
    coalesce(lower(prime_county_name),                  '') || ' ' ||
    coalesce(lower(prime_state_name),                   '') || ' ' ||
    coalesce(lower(prime_zip_code),                     '') || ' ' ||
    coalesce(lower(prime_business_types),               '') || ' ' ||
    coalesce(lower(prime_pop_scope),                    '') || ' ' ||
    coalesce(lower(prime_pop_city_name),                '') || ' ' ||
    coalesce(lower(prime_pop_county_name),              '') || ' ' ||
    coalesce(lower(prime_pop_state_name),               '') || ' ' ||
    coalesce(lower(prime_pop_zip_code),                 '') || ' ' ||
    coalesce(lower(prime_pop_country_name),             '') || ' ' ||
    coalesce(lower(prime_base_transaction_description), '') || ' ' ||
    coalesce(lower(prime_cfda_numbers_and_titles),      '') || ' ' ||
    coalesce(lower(sub_fsrs_report_id),                 '') || ' ' ||
    coalesce(lower(sub_number),                         '') || ' ' ||
    coalesce(lower(sub_uei),                            '') || ' ' ||
    coalesce(lower(sub_duns),                           '') || ' ' ||
    coalesce(lower(sub_name),                           '') || ' ' ||
    coalesce(lower(sub_dba),                            '') || ' ' ||
    coalesce(lower(sub_parent_uei),                     '') || ' ' ||
    coalesce(lower(sub_parent_duns),                    '') || ' ' ||
    coalesce(lower(sub_parent_name),                    '') || ' ' ||
    coalesce(lower(sub_country_name),                   '') || ' ' ||
    coalesce(lower(sub_address_line_1),                 '') || ' ' ||
    coalesce(lower(sub_city_name),                      '') || ' ' ||
    coalesce(lower(sub_state_name),                     '') || ' ' ||
    coalesce(lower(sub_zip_code),                       '') || ' ' ||
    coalesce(lower(sub_business_types),                 '') || ' ' ||
    coalesce(lower(sub_pop_city_name),                  '') || ' ' ||
    coalesce(lower(sub_pop_state_name),                 '') || ' ' ||
    coalesce(lower(sub_pop_zip_code),                   '') || ' ' ||
    coalesce(lower(sub_pop_country_name),               '') || ' ' ||
    coalesce(lower(sub_description),                    '') || ' ' ||
    coalesce(lower(sub_officer_1_name),                 '') || ' ' ||
    coalesce(lower(sub_officer_2_name),                 '') || ' ' ||
    coalesce(lower(sub_officer_3_name),                 '') || ' ' ||
    coalesce(lower(sub_officer_4_name),                 '') || ' ' ||
    coalesce(lower(sub_officer_5_name),                 '')));
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=OUTPUT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=None):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['us', 'central', 'grants'], entities)
