'''
Copyright (c) 2020 Jack Poulson <jack@techinquiry.org>

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

PyNLRB is a simple scraper for retrieving cases from https://www.nlrb.gov
using BeautifulSoup. There are four primary use cases at the moment:

  1. Retrieving all cases matching a particular query string, in the form of
     a dictionary mapping the relevant case numbers to their case info. E.g.,
     cases mentioning 'google' can be retrieved via:

     >>> import gov.us.central.labor_relations as nlrb
     >>> cases = nlrb.query_cases('google')

  2. Retrieving a particular case from its case number:

     >>> case = nlrb.case('02-CA-266980')

  3. Transform a list of case numbers into a dictionary mapping their case
     numbers to their case info.

     >>> cases = nlrb.cases(['02-CA-266980', ...])

  4. Constructing a pandas.DataFrame of the summaries of recent filings:

     >>> df = nlrb.recent_cases_dataframe()

Please keep in mind that the NLRB server is extremely slow.

Further, there is a well-known memory leak in lxml that limits the number of
independent NLRB cases that can be scraped in a single process. See

    https://groups.google.com/g/beautifulsoup/c/ZCEsPeTQjLk

for more information.

TODO(Jack Poulson):

 - Add a scraper for the cases mentioned on the Board Decisions page:

     https://www.nlrb.gov/cases-decisions/decisions/board-decisions

'''
import bs4
import gc
import io
import json
import os
import pandas as pd
import random
import requests
from bs4 import BeautifulSoup

import util.postgres

BASE_SEARCH_URL = 'https://www.nlrb.gov/search/case'
BASE_CASE_URL = 'https://www.nlrb.gov/case'

# Note: This link seems to now be broken and returns a CSV header but no rows.
RECENT_CASES_CSV_URL = 'https://www.nlrb.gov/reports/graphs-data/recent-filings/csv-export'

RECENT_ELECTIONS_CSV_URL = 'https://www.nlrb.gov/reports/graphs-data/recent-election-results/csv-export'

# If no bytes are received by requests.get for the following number of seconds,
# then an exception will be thrown.
DEFAULT_TIMEOUT = 30.0

# Using 'lxml' instead of 'html.parser' is critical for accurately
# parsing tables. Otherwise we see spurious patterns such as
# '</tr></tr></tr>...'.
PARSER = 'lxml'

TABLE_TYPES = {
    'case_number': 'VARCHAR(15)',
    'date_filed': 'TIMESTAMP',
    'status': 'VARCHAR(63)',
    'location': 'VARCHAR(127)',
    'region_assigned': 'VARCHAR(127)',
    'reason_closed': 'VARCHAR(63)',
    'docket_activity': 'JSONB',
    'related_documents': 'JSONB',
    'allegations': 'JSONB',
    'participants': 'JSONB',
    'related_cases': 'JSONB',
    'title': 'VARCHAR(1023)'
}
UNIQUE_COLUMNS = ['case_number']
FORCE_NULL_COLUMNS = ['date_filed']
TABLE_NAME = 'us_nlrb_filing'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
CASES_DIR = os.path.join(DATA_DIR, 'us_nlrb')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

CASE_NUMBERS_SAVE_JSON = '{}/case_numbers.json'.format(CASES_DIR)
CASES_SAVE_JSON = '{}/cases.json'.format(CASES_DIR)
CASES_EXPORT_JSON = '{}/nlrb_filings.json'.format(EXPORT_DIR)
CASES_EXPORT_CSV = '{}/nlrb_filings.csv'.format(EXPORT_DIR)

if not os.path.exists(DATA_DIR):
    os.makedirs(DATA_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)


# Remove leading and trailing whitespace then merge whitespaces.
def simplify_string(value):
    return ' '.join(value.strip().split())


def query_case_summaries_url(query, page_number=0):
    '''Returns the URL for the given page of a query's case summaries.'''
    return '{}/{}?page={}'.format(BASE_SEARCH_URL, query, page_number)


def case_url(case_number):
    '''Returns the URL for retrieving the case with the given case number.'''
    return '{}/{}'.format(BASE_CASE_URL, case_number)


def query_case_summaries_soup(query, page_number=0, timeout=DEFAULT_TIMEOUT):
    '''Returns a soup of summaries for the given query and page number.'''
    url = query_case_summaries_url(query, page_number)
    try:
        page = requests.get(url, timeout=timeout)
        return BeautifulSoup(page.content, PARSER)
    except:
        return None


def case_soup(case_number, timeout=DEFAULT_TIMEOUT):
    '''Returns a case soup for the given case number.'''
    url = case_url(case_number)
    try:
        page = requests.get(url, timeout=timeout)
        return BeautifulSoup(page.content, PARSER)
    except:
        return None


def num_total_case_summaries_from_soup(soup):
    '''Returns number of total case summaries from a search BeautifulSoup.'''
    total_results_num = soup.find(id='total_results_num')
    if total_results_num == None:
        total_results = 0
    else:
        total_results = int(total_results_num.text)
    return total_results


def extract_text(obj):
    if type(obj) is bs4.element.NavigableString:
        return simplify_string(obj)
    else:
        return simplify_string(obj.text)


def query_case_summaries_from_soup(soup):
    '''Returns an array of case summaries from a search BeautifulSoup.'''
    if soup == None:
        return []
    CASE_NUMBER_LABEL = 'Case Number: '
    DATE_FILED_LABEL = 'Date Filed: '
    STATUS_LABEL = 'Status: '
    LOCATION_LABEL = 'Location: '
    REGION_ASSIGNED_LABEL = 'Region Assigned: '

    summaries = []
    for result in soup.find_all(class_='partition-div'):
        left_div = result.contents[1]
        if left_div.contents[1].text == CASE_NUMBER_LABEL:
            case_number = left_div.contents[2].contents[0]
        else:
            print('Did not find case number label in position 1.')
            print('left_div: {}'.format(left_div.contents))
            case_number = None
        if left_div.contents[5].text == DATE_FILED_LABEL:
            date_filed = left_div.contents[6]
        else:
            print('Did not find date filed label in position 5.')
            print('left_div: {}'.format(left_div.contents))
            date_filed = None
        if left_div.contents[9].text == STATUS_LABEL:
            status = left_div.contents[10]
        else:
            print('Did not find status label in position 9.')
            print('left_div: {}'.format(left_div.contents))
            status = None

        right_div = result.contents[3]
        if right_div.contents[1].text == LOCATION_LABEL:
            location = right_div.contents[2]
        else:
            print('Did not find location label in position 1.')
            print('right_div: {}'.format(right_div.contents))
            location = None
        if right_div.contents[5].text == REGION_ASSIGNED_LABEL:
            region_assigned = right_div.contents[6]
        else:
            print('Did not find region assigned label in position 5.')
            print('right_div: {}'.format(right_div.contents))
            region_assigned = None

        summaries.append({
            'case_number': case_number,
            'date_filed': date_filed,
            'status': status,
            'location': location,
            'region_assigned': region_assigned
        })

    return summaries


def case_from_soup(soup):
    '''Returns a dictionary summarizing a case from its BeautifulSoup.'''
    if soup == None:
        return None

    case = {}

    # Read in the header information.
    for title_div in soup.find_all(id='block-nlrb-page-title', limit=1):
        if len(title_div.contents) < 2:
            print('Could not find title in: {}'.format(title_div.contents))
            return None
        if not hasattr(title_div.contents[1], 'text'):
            print('ERROR: Could not recover title from page.')
            return None
        case['title'] = simplify_string(title_div.contents[1].text)

    display_flexes = soup.find_all(class_='display-flex')
    if len(display_flexes) == 0:
        print('ERR: There were {} display-flex classes.'.format(
            len(display_flexes)))
        return None

    # TODO(Jack Poulson): If there were more than one display flexes, then
    # the extras should be about union elections. We should start storing this
    # information.

    display_flex = display_flexes[0] if len(
        display_flexes[0]) >= 4 else display_flexes[1]
    case['case_number'] = extract_text(
        display_flex.contents[1].contents[1].contents[2])
    case['date_filed'] = extract_text(
        display_flex.contents[1].contents[3].contents[2])
    case['status'] = extract_text(
        display_flex.contents[1].contents[5].contents[2])
    case['location'] = extract_text(
        display_flex.contents[3].contents[1].contents[2])
    case['region_assigned'] = extract_text(
        display_flex.contents[3].contents[3].contents[2])
    # TODO(Jack Poulson): Add support
    case['reason_closed'] = None

    # Read in the docket activity.
    case['docket_activity'] = []
    for docket_table in soup.find_all('table',
                                      class_='docket-activity-table',
                                      limit=1):
        table_body = docket_table.contents[3]
        table_body_len = len(table_body.contents)
        for index in range(1, table_body_len, 2):
            item = table_body.contents[index]
            date = extract_text(item.contents[1])
            document_type = extract_text(item.contents[3])
            document_url = None
            if len(item.contents[3].contents) >= 2:
                document_content = item.contents[3].contents[1]
                if document_content.has_attr('href'):
                    document_url = document_content['href']
            filed_by = extract_text(item.contents[5])
            case['docket_activity'].append({
                'date': date,
                'document_type': document_type,
                'document_url': document_url,
                'filed_by': filed_by
            })

    # Read in the related documents. We check that the found unordered list is
    # not actually for the list of allegations.
    #
    # TODO(Jack Poulson): Find how to extract document positions of the found
    # elements so that we can compare them against those of landmarks to
    # check if we are in the appropriate section.
    #
    case['related_documents'] = []
    for related_documents_ul in soup.find(
            'h2', text='Related Documents').find_next_siblings('ul', limit=1):
        for allegations_ul in soup.find(
                'h2', text='Allegations').find_next_siblings('ul', limit=1):
            if related_documents_ul == allegations_ul:
                continue
            related_documents_ul_len = len(related_documents_ul.contents)
            if related_documents_ul_len >= 2:
                for index in range(1, related_documents_ul_len, 2):
                    related_document = related_documents_ul.contents[index]
                    try:
                        url = related_document.contents[0]['href']
                        title = extract_text(related_document.contents[0])
                        case['related_documents'].append({
                            'url': url,
                            'title': title
                        })
                    except:
                        print(related_document.contents)
                        print(related_document.contents[0])

    # Read in the allegations.
    case['allegations'] = []
    for allegations_ul in soup.find(
            'h2', text='Allegations').find_next_siblings('ul', limit=1):
        allegations_ul_len = len(allegations_ul.contents)
        if allegations_ul_len >= 2:
            for index in range(1, allegations_ul_len, 2):
                allegation = allegations_ul.contents[index]
                if type(allegation) is bs4.element.NavigableString:
                    item = allegation
                else:
                    item = allegation.text
                case['allegations'].append(item)

    # Read in the participants.
    case['participants'] = []
    for participants_table in soup.find_all('table',
                                            class_='Participants',
                                            limit=1):
        table_body = participants_table.contents[3]
        table_body_len = len(table_body.contents)
        for body_index in range(1, table_body_len):
            participant_portion = table_body.contents[body_index]

            participant = {}

            if not hasattr(participant_portion, 'contents'):
                continue
            if len(participant_portion.contents) < 6:
                continue

            # Parse the participant type information.
            participant_type = participant_portion.contents[1]
            num_type_components = len(participant_type.contents)
            if num_type_components > 1:
                participant['charging_type'] = extract_text(
                    participant_type.contents[0])
            if num_type_components > 3:
                participant['employment_type'] = extract_text(
                    participant_type.contents[2])
            if num_type_components > 5:
                participant['name'] = extract_text(
                    participant_type.contents[4])
            if num_type_components > 7:
                participant['employer'] = extract_text(
                    participant_type.contents[6])

            # Parse the participant address information.
            if len(participant_portion.contents[3].contents) >= 4:
                participant_address = participant_portion.contents[3].contents[
                    3]
                participant_address_len = len(participant_address.contents)
                participant['street_address'] = None
                participant['city_and_state'] = None
                participant['zipcode'] = None
                if (participant_address_len == 9
                        or participant_address_len == 7
                        or participant_address_len == 5
                        or participant_address_len == 3):
                    index = 0
                    contents = participant_address.contents
                    if (participant_address_len == 9
                            or participant_address_len == 7):
                        participant['street_address'] = simplify_string(
                            contents[index])
                        index += 2
                        if participant_address_len == 9:
                            participant['street_address'] = '{}, {}'.format(
                                participant['street_address'],
                                simplify_string(contents[index]))
                            index += 2
                    participant['city_and_state'] = simplify_string(
                        contents[index])
                    index += 2
                    if participant_address_len > 3:
                        participant['zipcode'] = simplify_string(
                            contents[index])
                        index += 2
                else:
                    if participant_address_len > 1:
                        for index in range(participant_address_len):
                            piece = participant_address.contents[index]
                            print('  {}: {}'.format(index, piece))

            # Parse the participant phone information.
            participant_phone = participant_portion.contents[5]
            participant_phone_len = len(participant_phone.contents)
            if participant_phone_len >= 6:
                participant['phone_number'] = extract_text(
                    participant_phone.contents[5].contents[0])

            case['participants'].append(participant)

    # Read in the related cases.
    case['related_cases'] = []
    for related_cases_table in soup.find_all('table',
                                             class_='related-case',
                                             limit=1):
        table_body = related_cases_table.contents[3]
        table_body_len = len(table_body.contents)
        for index in range(1, table_body_len, 2):
            related_case = table_body.contents[index]
            case_number = related_case.contents[1].contents[1].text
            case_name = simplify_string(related_case.contents[3].text)
            status = simplify_string(related_case.contents[5].text)
            case['related_cases'].append({
                'case_number': case_number,
                'case_name': case_name,
                'status': status
            })

    return case


def query_case_summaries(query,
                         page_number=None,
                         timeout=DEFAULT_TIMEOUT,
                         verbose=True):
    '''Returns an array of case summaries for the given query and page.'''
    SUMMARIES_PER_PAGE = 10

    def ceildiv(a, b):
        return -(-a // b)

    summaries = []
    if page_number == None:
        soup = query_case_summaries_soup(query, page_number=0, timeout=timeout)
        if soup == None:
            return []
        num_total_summaries = num_total_case_summaries_from_soup(soup)
        num_pages = ceildiv(num_total_summaries, SUMMARIES_PER_PAGE)
        if verbose:
            print('Will retrieve {} summaries across {} pages.'.format(
                num_total_summaries, num_pages))
        for page in range(num_pages):
            if verbose:
                print('  Retrieving summary page {} of {}'.format(
                    page, num_pages))
            summaries += query_case_summaries(query,
                                              page_number=page,
                                              timeout=timeout,
                                              verbose=verbose)
        soup.decompose()
        del soup
        gc.collect()
    else:
        soup = query_case_summaries_soup(query,
                                         page_number=page_number,
                                         timeout=timeout)
        summaries = query_case_summaries_from_soup(soup)
        soup.decompose()
        del soup
        gc.collect()
    return summaries


def recent_cases_dataframe(timeout=DEFAULT_TIMEOUT):
    '''Returns a DataFrame representation of recent cases CSV.'''
    print(
        'WARNING: The recent cases CSV has been broken for more than a week.')
    try:
        page = requests.get(RECENT_CASES_CSV_URL, timeout=timeout)
        df = pd.read_csv(io.StringIO(page.content.decode('utf-8')))
        df['Date Filed'] = pd.to_datetime(df['Date Filed'], format='%m/%d/%Y')
        return df
    except:
        print('Timed out on retrieving {}'.format(RECENT_CASES_CSV_URL))
        return None


def recent_elections_dataframe(timeout=DEFAULT_TIMEOUT):
    '''Returns a DataFrame representation of recent elections CSV.'''
    try:
        page = requests.get(RECENT_ELECTIONS_CSV_URL, timeout=timeout)
        df = pd.read_csv(io.StringIO(page.content.decode('utf-8')))

        DATE_FIELDS = ['Date Filed', 'Date Closed', 'Tally Date']
        for field in DATE_FIELDS:
            df[field] = pd.to_datetime(df[field], format='%m/%d/%Y')
        return df
    except:
        print('Timed out on retrieving {}'.format(RECENT_ELECTIONS_CSV_URL))
        return None


def case(case_number, timeout=DEFAULT_TIMEOUT):
    '''Returns a dictionary for the case with the given case number.'''
    case_info = case_from_soup(case_soup(case_number, timeout=timeout))
    gc.collect()
    return case_info


def cases(case_numbers, timeout=DEFAULT_TIMEOUT, verbose=True):
    '''Returns a dictionary for each case with corresponding case number.'''
    num_cases = len(case_numbers)
    case_dict = {}
    for case_index in range(num_cases):
        case_number = case_numbers[case_index]
        if verbose:
            print('Retrieving case {} ({} of {})'.format(
                case_number, case_index, num_cases))
        case_dict[case_number] = case(case_number, timeout=timeout)
    return case_dict


def query_cases(query, timeout=DEFAULT_TIMEOUT, verbose=True):
    '''Returns the cases associated with a particular query.'''
    summaries = query_case_summaries(query, timeout=timeout, verbose=verbose)
    num_cases = len(summaries)
    case_dict = {}
    for case_index in range(num_cases):
        summary = summaries[case_index]
        case_number = summary['case_number']
        if verbose:
            print('Retrieving case {} ({} of {})'.format(
                case_number, case_index, num_cases))
        case_dict[case_number] = case(case_number, timeout=timeout)
    return case_dict


def participants_set(cases):
    '''Converts a dictionary from case numbers to cases to participants set.'''
    participants = set()
    for case_number in cases:
        case_info = cases[case_number]
        for participant in case_info['participants']:
            if 'name' in participant and participant['name'] != None:
                participants.add(participant['name'].lower())
    return participants


def json_to_csv(json_filename, csv_filename):
    '''Converts a JSON representation of cases into a CSV.'''
    df = pd.read_json(json_filename)

    date_columns = ['date_filed']

    json_columns = [
        'allegations', 'participants', 'docket_activity', 'related_cases',
        'related_documents'
    ]

    for column in date_columns:
        df[column] = pd.to_datetime(df[column],
                                    format='%m/%d/%Y',
                                    errors='coerce')

    df = util.postgres.preprocess_dataframe(df,
                                            json_columns=json_columns,
                                            unique_columns=UNIQUE_COLUMNS)

    util.postgres.df_to_csv(df, csv_filename, columns=TABLE_TYPES.keys())


def retrieve(max_parses=500):
    '''Updates a cached list of NRLB cases by scraping nlrb.gov

    Args:
      max_parses: The maximum number of parses to perform in the call. Due to a
          known memory leak due to the global dictionary of the lxml parser
          backend to BeautifulSoup required for properly parsing tables, one
          should exit the python process before calling this routine again.
    '''
    # Load the pre-existing case numbers list.
    if not os.path.exists(CASE_NUMBERS_SAVE_JSON):
        with open(CASE_NUMBERS_SAVE_JSON, 'w') as outfile:
            json.dump([], outfile, indent=1)
    case_numbers_list = json.load(open(CASE_NUMBERS_SAVE_JSON))

    # Load the pre-existing cases data.
    if not os.path.exists(CASES_SAVE_JSON):
        with open(CASES_SAVE_JSON, 'w') as outfile:
            json.dump({}, outfile, indent=1)
    cases_save = json.load(open(CASES_SAVE_JSON))

    # Pull in the cases to process.
    try:
        df = recent_cases_dataframe()
        print('Number of retrieved cases: {}'.format(len(df.index)))
        if len(df.index):
            case_numbers_list += list(df['Case Number'])
    except Exception as e:
        print(e)
    try:
        df = recent_elections_dataframe()
        print('Number of retrieved recent elections: {}'.format(len(df.index)))
        if len(df.index):
            case_numbers_list += list(df['Case'])
    except Exception as e:
        print(e)
    case_numbers_list = sorted(list(set(case_numbers_list)))

    num_parses_left = max_parses
    updated_case_numbers = {}

    # TODO(Jack Poulson): Add sorting to prevent detection of reordered lists as
    # different. Maybe use a set instead.

    # Prioritize getting the new cases processed.
    num_skipped = 0
    for case_number in case_numbers_list:
        if num_parses_left == 0:
            break
        if case_number in cases_save and cases_save[case_number] != None:
            num_skipped += 1
            continue
        if num_skipped > 0:
            print('  Skipped {} already-retrieved cases.'.format(num_skipped))
            num_skipped = 0
        print('Processing case {} ({} parses left)'.format(
            case_number, num_parses_left))
        case_info = case(case_number)
        if case_info != None:
            updated_case_numbers[case_number] = True
            cases_save[case_number] = case_info
        num_parses_left -= 1

    related_cases = set()
    for case_number in cases_save:
        case_info = cases_save[case_number]
        for related_case in case_info['related_cases']:
            related_cases.add(related_case['case_number'])
    related_cases = sorted(list(related_cases))
    print('Constructed list of {} related cases.'.format(len(related_cases)))

    for case_number in related_cases:
        if num_parses_left == 0:
            break
        if case_number in updated_case_numbers:
            continue
        if (case_number in cases_save
                and ('title' in cases_save[case_number]
                     and cases_save[case_number]['status'] == 'Closed')):
            print('Skipping related case {} because its status is {}'.format(
                case_number, cases_save[case_number]['status']))
            continue
        if case_number in cases_save:
            print('Updating related case {} ({} parses left)'.format(
                case_number, num_parses_left))
        else:
            print('Retrieving related case {} ({} parses left)'.format(
                case_number, num_parses_left))
        case_info = case(case_number)
        if case_info != None:
            updated_case_numbers[case_number] = True
            if case_number in cases_save and cases_save[
                    case_number] == case_info:
                print('  Parse of case {} was identical to save.'.format(
                    case_number))
            else:
                cases_save[case_number] = case_info
        num_parses_left -= 1

    # Spend the remaining parses on updating pre-existing parses.
    # (Skip cases that are not still open.)
    for case_number in random.sample(case_numbers_list,
                                     len(case_numbers_list)):
        if num_parses_left == 0:
            break
        if case_number in updated_case_numbers:
            continue
        if (case_number in cases_save
                and ('title' in cases_save[case_number]
                     and cases_save[case_number]['status'] != 'Open')):
            print('Skipping case {} because its status is {}'.format(
                case_number, cases_save[case_number]['status']))
            continue
        print('Updating case {} ({} parses left)'.format(
            case_number, num_parses_left))
        case_info = case(case_number)
        if case_info != None:
            updated_case_numbers[case_number] = True
            if case_number in cases_save and cases_save[
                    case_number] == case_info:
                print('  Parse of case {} was identical to save.'.format(
                    case_number))
            else:
                cases_save[case_number] = case_info
        num_parses_left -= 1

    with open(CASE_NUMBERS_SAVE_JSON, 'w') as outfile:
        json.dump(case_numbers_list, outfile, indent=1)

    with open(CASES_SAVE_JSON, 'w') as outfile:
        json.dump(cases_save, outfile, indent=1)

    with open(CASES_EXPORT_JSON, 'w') as outfile:
        json.dump(list(cases_save.values()), outfile, indent=1)
    json_to_csv(CASES_EXPORT_JSON, CASES_EXPORT_CSV)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TABLE_NAME,
            TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (case_number)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_case_number_index ON {} (case_number);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_participants_index ON {} USING GIN (
  (lower(participants::TEXT)::JSONB));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_title_index ON {} (LOWER(title));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(case_number),             '') || ' ' ||
    coalesce(lower(status),                  '') || ' ' ||
    coalesce(lower(location),                '') || ' ' ||
    coalesce(lower(region_assigned),         '') || ' ' ||
    coalesce(lower(reason_closed),           '') || ' ' ||
    coalesce(lower(docket_activity::TEXT),   '') || ' ' ||
    coalesce(lower(related_documents::TEXT), '') || ' ' ||
    coalesce(lower(allegations::TEXT),       '') || ' ' ||
    coalesce(lower(participants::TEXT),      '') || ' ' ||
    coalesce(lower(related_cases::TEXT),     '') || ' ' ||
    coalesce(lower(title),                   '')
  )
);
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=CASES_EXPORT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
