#
# Copyright (c) 2020-2023 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# A simple scraper for the Texas Department of Public Safety's list of
# contracts above $100,000.
#
# https://www.dps.texas.gov/sites/default/files/documents/iod/doingbusiness/docs/contractsover100k.pdf
#
# The contract descriptions are scraped from the PDF using Tabula and then
# exported to a CSV in a format suitable for import into a Postgres database.
# A list of unique contractor names is also exported to a JSON file.
#
import json
import os
import pandas as pd
import tabula

import util.postgres

# The PDF of procurement data we will scrape.
TX_DPS_PDF = 'https://www.dps.texas.gov/sites/default/files/documents/iod/doingbusiness/docs/contractsover100k.pdf'

TABLE_TYPES = {
    'contract': 'VARCHAR(31)',
    'description': 'VARCHAR(127)',
    'supplier': 'VARCHAR(127)',
    'start_date': 'DATE',
    'end_date': 'DATE',
    'contract_maximum': 'VARCHAR(31)'
}
UNIQUE_COLUMNS = ['contract', 'supplier', 'start_date']
FORCE_NULL_COLUMNS = ['start_date', 'end_date']
TABLE_NAME = 'us_tx_dps_procurement'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

EXPORT_CSV = '{}/us_tx_dps_procurement.csv'.format(EXPORT_DIR)
UNIQUE_NAMES_JSON = '{}/us_tx_dps_unique_names.json'.format(DATA_DIR)

if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)
if not os.path.exists(DATA_DIR):
    os.makedirs(DATA_DIR)

# The n+1 point values for the separators between the n columns can be found,
# for example, in Mac Preview by turning on the inspection utility in points
# units and selecting a sequence of rectangles beginning in the top-left corner
# of the page but with the right side lying on each of the target boundaries.
COLUMNS_DEFAULT = [35, 147.6, 320.33, 454.54, 543.07, 627.06, 745]

# The area quartet is (a, b, a + delta_a, b + delta_b), where a is the number
# of points from the top before the table begins, delta_a is the height of
# the table in points, and b and delta_b are similar from the left side.
AREA_DEFAULT = [80.3, 35, 575, 745]


# Get the list of dataframes -- one for each page.
# Note: The column size appears to change regularly.
def get_dfs(area=AREA_DEFAULT,
            columns=COLUMNS_DEFAULT,
            guess=False,
            lattice=False):
    dfs = tabula.read_pdf(TX_DPS_PDF,
                          pages='all',
                          guess=guess,
                          lattice=lattice,
                          area=area,
                          columns=columns,
                          pandas_options={
                              'header': None,
                              'dtype': str
                          })
    return dfs


def retrieve(drop_first_column=True):
    dfs = get_dfs()

    # Build up a list of post-processed dataframes for each page.
    df_subsets = []
    for index in range(len(dfs)):
        df = dfs[index]

        # Drop the header row on the first page.
        if index == 0:
            df = df.drop(0)

        print('Number of original columns: {}'.format(len(df.columns)))

        if drop_first_column:
            num_first_unique = len(df[0].unique())
            if num_first_unique > 1:
                print(
                    'WARNING: Dropping first column with more than just NaN.')
                print(df[0].unique())
            df = df.drop(columns=[0])

        df.columns = [
            'contract', 'supplier', 'description', 'start_date', 'end_date',
            'contract_maximum'
        ]

        # Lowercase and clean up the whitespace in the supplier column.
        df['supplier'] = df['supplier'].str.lower()
        df['supplier'] = df['supplier'].str.strip()
        df.replace({'supplier': r'\s+'}, value=' ', inplace=True, regex=True)

        df['start_date'] = pd.to_datetime(df['start_date'],
                                          format='%m/%d/%Y',
                                          errors='coerce')
        df['end_date'] = pd.to_datetime(df['end_date'],
                                        format='%m/%d/%Y',
                                        errors='coerce')

        # Clean up the maximum amount column by removing spaces, $, and commas.
        df.replace({'contract_maximum': r'\s+'},
                   value='',
                   inplace=True,
                   regex=True)
        df['contract_maximum'] = df['contract_maximum'].str.replace(',', '')
        df['contract_maximum'] = df['contract_maximum'].str.replace('$', '')

        df_subsets.append(df)

    # Combine the dataframe from each page into a single dataframe.
    df = pd.concat(df_subsets, ignore_index=True)

    df = util.postgres.preprocess_dataframe(df, unique_columns=UNIQUE_COLUMNS)

    # Write out the dataframe in a CSV format suitable for Postgres import.
    util.postgres.df_to_csv(df, EXPORT_CSV, columns=TABLE_TYPES.keys())

    # Write out the unique supplier names.
    unique_names = sorted([x.lower() for x in df['supplier'].unique()])
    with open(UNIQUE_NAMES_JSON, 'w') as outfile:
        json.dump(unique_names, outfile, indent=1)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    # There is not enough data for an index to be worthwhile.
    cursor.execute(
        util.postgres.create_table_sql(TABLE_NAME,
                                       TABLE_TYPES,
                                       serial_id=True,
                                       constraints={
                                           'unique':
                                           '''
UNIQUE (
  contract, supplier, start_date
)
                '''
                                       }))


def postgres_update(export_csv=EXPORT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
