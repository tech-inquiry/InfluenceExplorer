#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This is a prototype utility for downloading Arizona state procurement data
# from https://app.az.gov/page.aspx/en/pdt/item_browse_public
#
# At the moment, it simply grabs a copy -- page by page -- then writes the
# results to a JSON file in the current directory.
#
import json
import numpy as np
import os
import pandas as pd
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import util.entities
import util.firefox
import util.postgres

LOAD_SLEEP = 5
ERROR_SLEEP = 5
IMPLICIT_WAIT = 2
MAX_WAIT_SECONDS = 15
MAX_PAGES = 100
APP_URL = 'https://app.az.gov/page.aspx/en/pdt/item_browse_public'

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
DOWNLOAD_DIR = os.path.join(DATA_DIR, 'us_az_procurement')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

OUTPUT_JSON = '{}/us_az_procurement.json'.format(DOWNLOAD_DIR)
OUTPUT_CSV = '{}/us_az_procurement.csv'.format(EXPORT_DIR)

if not os.path.exists(DOWNLOAD_DIR):
    os.makedirs(DOWNLOAD_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)

TABLE_NAME = 'us_az_procurement'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

DATE_COLUMNS = ['start_date', 'end_date']
UNIQUE_COLUMNS = ['product_code', 'supplier', 'contract']
FORCE_NULL_COLUMNS = ['negotiated_price', 'start_date', 'end_date']
TABLE_TYPES = {
    'image': 'VARCHAR(255)',
    'commodity': 'VARCHAR(255)',
    'product_code': 'VARCHAR(255)',
    'supplier': 'VARCHAR(255)',
    'item_label': 'VARCHAR(255)',
    'summary': 'TEXT',
    'negotiated_price': 'FLOAT',
    'currency': 'VARCHAR(7)',
    'contract': 'VARCHAR(255)',
    'unit': 'VARCHAR(15)',
    'start_date': 'TIMESTAMP',
    'end_date': 'TIMESTAMP'
}


def page_to_xpath(page):
    return '//button[@aria-label="Page {}"]'.format(page)


def scrape_rows_from_current_state(browser):
    ITEM_XPATH = '//tr[@data-object-type="item"]'
    COLUMN_XPATH = './child::td[@data-iv-role="cell"]'

    rows = []
    elements = WebDriverWait(browser, MAX_WAIT_SECONDS).until(
        EC.presence_of_all_elements_located((By.XPATH, ITEM_XPATH)))
    for element in elements:
        row = []
        columns = element.find_elements(By.XPATH, COLUMN_XPATH)
        for column in columns:
            row.append(column.text)
        if len(row) != 12:
            print('Expected 12 entries in {}'.format(row))
            continue

        rows.append({
            'image': row[0],
            'commodity': row[1],
            'product_code': row[2],
            'supplier': util.entities.canonicalize(row[3]),
            'item_label': row[4],
            'summary': row[5],
            'negotiated_price': row[6],
            'currency': row[7],
            'contract': row[8],
            'unit': row[9],
            'start_date': row[10],
            'end_date': row[11]
        })
    return rows


def load_next_page(browser, page):
    xpath = page_to_xpath(page)
    elements = browser.find_elements(By.XPATH, xpath)
    if len(elements) != 1:
        return False
    element = WebDriverWait(browser, MAX_WAIT_SECONDS).until(
        EC.element_to_be_clickable((By.XPATH, xpath)))
    element.send_keys(Keys.ENTER)
    return True


def get_records(max_pages=MAX_PAGES,
                load_sleep=LOAD_SLEEP,
                error_sleep=ERROR_SLEEP,
                implicit_wait=IMPLICIT_WAIT):
    browser = util.firefox.get_browser(headless=True)
    browser.get(APP_URL)
    browser.implicitly_wait(implicit_wait)
    rows = []
    page = 1
    while page <= max_pages:
        time.sleep(load_sleep)
        # Scrape the elements from the current page state.
        try:
            appended_rows = scrape_rows_from_current_state(browser)
            rows += appended_rows
        except Exception as e:
            print('Caught exception: {}'.format(e))
            time.sleep(error_sleep)
            appended_rows = scrape_rows_from_current_state(browser)
            rows += appended_rows
        # Advance to the next page.
        print('Trying to advance from page {}'.format(page))
        if not load_next_page(browser, page):
            break
        page += 1
    return rows


def export_df_to_csv(df, export_csv_name, verbose=False):
    df = util.postgres.preprocess_dataframe(df,
                                            unique_columns=UNIQUE_COLUMNS,
                                            verbose=verbose)
    util.postgres.df_to_csv(df, export_csv_name, columns=TABLE_TYPES.keys())


def process_json(data, output_csv, verbose=False):
    df = pd.read_json(json.dumps(data))
    for date_column in DATE_COLUMNS:
        df[date_column] = pd.to_datetime(df[date_column],
                                         format='%m/%d/%Y',
                                         errors='coerce')
    df['negotiated_price'] = pd.to_numeric(df['negotiated_price'].str.replace(
        ',', ''),
                                           errors='coerce').astype(np.float32)

    export_df_to_csv(df, output_csv, verbose=verbose)


def retrieve(max_pages=MAX_PAGES,
             load_sleep=LOAD_SLEEP,
             error_sleep=ERROR_SLEEP,
             implicit_wait=IMPLICIT_WAIT,
             json_file=OUTPUT_JSON,
             csv_file=OUTPUT_CSV):
    rows = get_records(max_pages, load_sleep, error_sleep, implicit_wait)
    with open(json_file, 'w') as outfile:
        json.dump(rows, outfile, indent=1)
    process_json(rows, csv_file)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TABLE_NAME,
            TABLE_TYPES,
            serial_id=True,
            constraints={'unique':
                         'UNIQUE(product_code, supplier, contract)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_supplier_index ON {} (supplier);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(commodity),  '') || ' ' ||
    coalesce(supplier,          '') || ' ' ||
    coalesce(lower(item_label), '') || ' ' ||
    coalesce(lower(contract),   '')));
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=OUTPUT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=None):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
