#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# A simple scraper for the monthly-updated list of New York State procurement
# contracts at
#
#  https://online.ogs.ny.gov/purchase/spg/pdfdocs/StatewideTC.pdf
#
# The contract descriptions are scraped from the ~50 page PDF using Tabula
# and then exported to a CSV in a format suitable for import into a Postgres
# database. A list of unique contractor names is also exported to a JSON file.
#
# The listed contract periods are split into the start and end dates and then
# converted into a datetime object.
import json
import os
import pandas as pd
import tabula

import util.postgres

# The PDF of procurement data we will scrape.
NY_OGS_PDF = 'https://online.ogs.ny.gov/purchase/spg/pdfdocs/StatewideTC.pdf'

TABLE_TYPES = {
    'group_number': 'INT',
    'description': 'VARCHAR(255)',
    'award_number': 'INT',
    'contract_number': 'VARCHAR(63)',
    'contractor': 'VARCHAR(123)',
    'start_date': 'DATE',
    'end_date': 'DATE'
}
UNIQUE_COLUMNS = [
    'group_number', 'description', 'award_number', 'contract_number',
    'contractor', 'start_date', 'end_date'
]
FORCE_NULL_COLUMNS = ['start_date', 'end_date']
TABLE_NAME = 'us_ny_ogs_filing'
TABLE_TMP_NAME = 'us_ny_ogs_filing_tmp'

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

EXPORT_CSV = '{}/us_ny_ogs_filing.csv'.format(EXPORT_DIR)
UNIQUE_NAMES_JSON = '{}/unique_names.json'.format(DATA_DIR)


def retrieve():
    if not os.path.exists(EXPORT_DIR):
        os.makedirs(EXPORT_DIR)
    if not os.path.exists(DATA_DIR):
        os.makedirs(DATA_DIR)

    # Get the list of dataframes -- one for each page.
    dfs = tabula.read_pdf(NY_OGS_PDF,
                          pages='all',
                          pandas_options={'header': None})

    # Build up a list of post-processed dataframes for each page.
    df_subsets = []
    for df in dfs:
        # Sometimes tabula generates all null columns. Drop them.
        df = df.dropna(axis='columns', how='all')

        # Remove the subset of MB, WB, and SB columns that were active on
        # this page.
        df_piece = df.iloc[:, [0, 1, 3] + [-2, -1]].copy()
        df_piece.columns = [
            'group_number', 'description', 'award_number', 'contract_number',
            'contractor'
        ]

        # Convert the group number column into integers.
        df_piece['group_number'].fillna(-1, inplace=True)
        df_piece.loc[:, ['group_number']] = (
            df_piece['group_number'].round().astype('Int32'))

        # Convert the award number column into integers.
        df_piece['award_number'].fillna(-1, inplace=True)
        df_piece.loc[:, ['award_number']] = (
            df_piece['award_number'].round().astype('Int32'))

        # Clean up the whitespace in the contractor column.
        df_piece['contractor'] = df_piece['contractor'].str.strip()
        df_piece.replace({'contractor': r'\s+'},
                         value=' ',
                         inplace=True,
                         regex=True)

        # Split up the period column into the start and end dates.
        df_period = df.iloc[:, [2]].copy()
        df_period.columns = ['period']
        df_boundaries = df_period['period'].str.split(' - ', expand=True)
        df_boundaries.columns = ['start_date', 'end_date']
        df_boundaries['start_date'] = pd.to_datetime(
            df_boundaries['start_date'])
        df_boundaries['end_date'] = pd.to_datetime(df_boundaries['end_date'])

        # Form the post-processed dataframe for this page.
        df_subset = pd.concat([df_piece, df_boundaries], axis=1)
        df_subsets.append(df_subset)

    # Combine the dataframe from each page into a single dataframe.
    df = pd.concat(df_subsets, ignore_index=True)

    df = util.postgres.preprocess_dataframe(df, unique_columns=UNIQUE_COLUMNS)

    # Write out the dataframe in a CSV format suitable for Postgres import.
    util.postgres.df_to_csv(df, EXPORT_CSV, columns=TABLE_TYPES.keys())

    # Write out the unique contractor names.
    unique_names = sorted([x.lower() for x in df['contractor'].unique()])
    with open(UNIQUE_NAMES_JSON, 'w') as outfile:
        json.dump(unique_names, outfile, indent=1)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    # There is not enough data for an index to be worthwhile.
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(TABLE_NAME,
                                       TABLE_TYPES,
                                       serial_id=True,
                                       constraints={
                                           'unique':
                                           '''
UNIQUE (
 group_number, description, award_number, contract_number, contractor,
 start_date, end_date
)
                '''
                                       }))


def postgres_update(export_csv=EXPORT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
