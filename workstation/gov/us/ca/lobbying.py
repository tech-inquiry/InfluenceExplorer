#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This is a utility for making use of the California campaign finance and
# lobbying activity zipfile of data that powers CAL-ACCESS described at:
#
#    https://www.sos.ca.gov/campaign-lobbying/cal-access-resources/raw-data-campaign-finance-and-lobbying-activity
#
# After building interfaces to several of the tables the author discovered the
# California Civic Data Project's CAL-ACCESS documentation at:
#
#    https://calaccess.californiacivicdata.org/documentation/calaccess-forms/f625/
#
#    https://calaccess.californiacivicdata.org/documentation/calaccess-forms/f401/
#
# The California Civic Data site is a useful resource both for documentation and
# for directly downloading analogues of the 'corrected' CSVs produced by our
# code below.
#
import csv
import ijson
import json
import os
import pandas as pd
import re

import util.patch_csv as patch_csv
import util.postgres
import util.zip

from util.entities import canonicalize

# The util of the raw data zip.
ZIP_URL = 'https://campaignfinance.cdn.sos.ca.gov/dbwebexport.zip'

# Match any positive integer string.
POSITIVE_INTEGER_RE = re.compile(r'^\d+$')

# Match any floating-point number.
FLOAT_RE = re.compile(r'^-?\d+\.?\d*$')

# Match the timestamp format from the TSV.
TIMESTAMP_RE = re.compile(r'^(\d+/\d+/\d+) (\d+:\d+:\d+) (\w+)$')

# Match a US phone number.
PHONE_RE = re.compile(r'^1?-?\(?\d{3}? *\)?[ /\-\.]? *\d{3}[ /\-\.]?\d{4}$')

DATE_FORMAT_DAY_MONTH_YEAR = 0
DATE_FORMAT_MONTH_DAY_YEAR = 1

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
DOWNLOAD_DIR = os.path.join(DATA_DIR, 'us_ca_calaccess')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

CALACCESS_ORIG_DATA_DIR = '{}/CalAccess/DATA'.format(DOWNLOAD_DIR)

CORRECTION_MAP = {
    'CVR_CAMPAIGN_DISCLOSURE': 'receipts_cover',
    'CVR_LOBBY_DISCLOSURE': 'lobbying_cover',
    'CVR2_LOBBY_DISCLOSURE': 'lobbying_cover2',
    'LPAY': 'lobbying_payments',
    'TEXT_MEMO': 'memos',
    'RCPT': 'receipts',
    'S496': 's496'
}

LOBBY_DISC_COVER_TABLE_TYPES = {
    'filing_id': 'VARCHAR(63)',
    'cover': 'JSONB',
    'unmatched_memos': 'JSONB',
    'entities': 'JSONB'
}
LOBBY_DISC_COVER_FORCE_NULL_COLUMNS = None
LOBBY_DISC_COVER_UNIQUE_COLUMNS = ['filing_id']
LOBBY_DISC_COVER_TABLE_NAME = 'us_calaccess_lobby_disc_cover'
LOBBY_DISC_COVER_TABLE_TMP_NAME = '{}_tmp'.format(LOBBY_DISC_COVER_TABLE_NAME)

LOBBY_DISC_PAYMENT_TABLE_TYPES = {
    'filing_id': 'VARCHAR(63)',
    'transaction_id': 'VARCHAR(63)',
    'line_item': 'VARCHAR(15)',
    'payment': 'JSONB',
    'entities': 'JSONB'
}
LOBBY_DISC_PAYMENT_FORCE_NULL_COLUMNS = None
LOBBY_DISC_PAYMENT_UNIQUE_COLUMNS = [
    'filing_id', 'transaction_id', 'line_item'
]
LOBBY_DISC_PAYMENT_TABLE_NAME = 'us_calaccess_lobby_disc_payment'
LOBBY_DISC_PAYMENT_TABLE_TMP_NAME = '{}_tmp'.format(
    LOBBY_DISC_PAYMENT_TABLE_NAME)

CAMPAIGN_DISC_COVER_TABLE_TYPES = {
    'filing_id': 'VARCHAR(63)',
    'cover': 'JSONB',
    'unmatched_memos': 'JSONB',
    'entities': 'JSONB'
}
CAMPAIGN_DISC_COVER_FORCE_NULL_COLUMNS = None
CAMPAIGN_DISC_COVER_UNIQUE_COLUMNS = ['filing_id']
CAMPAIGN_DISC_COVER_TABLE_NAME = 'us_calaccess_campaign_disc_cover'
CAMPAIGN_DISC_COVER_TABLE_TMP_NAME = '{}_tmp'.format(
    CAMPAIGN_DISC_COVER_TABLE_NAME)

CAMPAIGN_CONTRIB_TABLE_TYPES = {
    'filing_id': 'VARCHAR(63)',
    'transaction_id': 'VARCHAR(63)',
    'line_item': 'VARCHAR(15)',
    'campaign_contribution': 'JSONB',
    'entities': 'JSONB'
}
CAMPAIGN_CONTRIB_FORCE_NULL_COLUMNS = None
CAMPAIGN_CONTRIB_UNIQUE_COLUMNS = ['filing_id', 'transaction_id', 'line_item']
CAMPAIGN_CONTRIB_TABLE_NAME = 'us_calaccess_campaign_contrib'
CAMPAIGN_CONTRIB_TABLE_TMP_NAME = '{}_tmp'.format(CAMPAIGN_CONTRIB_TABLE_NAME)

PRE_ELECT_EXPEND_TABLE_TYPES = {
    'filing_id': 'VARCHAR(63)',
    'transaction_id': 'VARCHAR(63)',
    'line_item': 'VARCHAR(15)',
    'pre_election_expenditure': 'JSONB',
    'entities': 'JSONB'
}
PRE_ELECT_EXPEND_FORCE_NULL_COLUMNS = None
PRE_ELECT_EXPEND_UNIQUE_COLUMNS = ['filing_id', 'transaction_id', 'line_item']
PRE_ELECT_EXPEND_TABLE_NAME = 'us_calaccess_pre_elect_expend'
PRE_ELECT_EXPEND_TABLE_TMP_NAME = '{}_tmp'.format(PRE_ELECT_EXPEND_TABLE_NAME)

SIMPLIFIED_LOBBY_DISCLOSURE_BASE = 'us_calaccess_lobby_disc'
SIMPLIFIED_CAMPAIGN_DISCLOSURE_COVER_BASE = 'us_calaccess_campaign_disc_cover'
SIMPLIFIED_CAMPAIGN_CONTRIBUTION_BASE = 'us_calaccess_campaign_contrib'
SIMPLIFIED_PRE_ELECTION_EXPENDITURE_BASE = 'us_calaccess_pre_elect_expend'

FILES = {}
for orig_name, new_name in CORRECTION_MAP.items():
    FILES[orig_name] = {
        'orig': '{}/{}_CD.TSV'.format(CALACCESS_ORIG_DATA_DIR, orig_name),
        'json': '{}/{}.json'.format(DOWNLOAD_DIR, new_name),
        'csv': '{}/{}.csv'.format(DOWNLOAD_DIR, new_name)
    }

SIMPLIFIED_LOBBY_DISCLOSURE_COVER_JSON = '{}/{}_cover.json'.format(
    DOWNLOAD_DIR, SIMPLIFIED_LOBBY_DISCLOSURE_BASE)
SIMPLIFIED_LOBBY_DISCLOSURE_COVER_CSV = '{}/{}_cover.csv'.format(
    EXPORT_DIR, SIMPLIFIED_LOBBY_DISCLOSURE_BASE)

SIMPLIFIED_LOBBY_DISCLOSURE_PAYMENT_JSON = '{}/{}_payment.json'.format(
    DOWNLOAD_DIR, SIMPLIFIED_LOBBY_DISCLOSURE_BASE)
SIMPLIFIED_LOBBY_DISCLOSURE_PAYMENT_CSV = '{}/{}_payment.csv'.format(
    EXPORT_DIR, SIMPLIFIED_LOBBY_DISCLOSURE_BASE)

SIMPLIFIED_CAMPAIGN_DISCLOSURE_COVER_CSV = '{}/{}.csv'.format(
    EXPORT_DIR, SIMPLIFIED_CAMPAIGN_DISCLOSURE_COVER_BASE)
SIMPLIFIED_CAMPAIGN_CONTRIBUTION_CSV = '{}/{}.csv'.format(
    EXPORT_DIR, SIMPLIFIED_CAMPAIGN_CONTRIBUTION_BASE)
SIMPLIFIED_PRE_ELECTION_EXPENDITURE_CSV = '{}/{}.csv'.format(
    EXPORT_DIR, SIMPLIFIED_PRE_ELECTION_EXPENDITURE_BASE)

if not os.path.exists(DOWNLOAD_DIR):
    os.makedirs(DOWNLOAD_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)


def json_to_csv(json_filename, csv_filename):
    '''Memory-heavy simple conversion of JSON into a CSV.'''
    df = pd.read_json(json_filename)
    df = util.postgres.preprocess_dataframe(df)
    util.postgres.df_to_csv(df, csv_filename)


def entity_map(prefix):
    return {
        '{}_ID'.format(prefix): 'id',
        '{}_NAME'.format(prefix): 'name',
        '{}_NAMF'.format(prefix): 'first_name',
        '{}_NAML'.format(prefix): 'last_name',
        '{}_NAMT'.format(prefix): 'prefix',
        '{}_NAMS'.format(prefix): 'suffix',
        '{}_CITY'.format(prefix): 'city',
        '{}_ST'.format(prefix): 'state',
        '{}_ZIP4'.format(prefix): 'zipcode',
        '{}_LOC'.format(prefix): 'location',
        '{}_PHON'.format(prefix): 'phone',
        '{}_FAX'.format(prefix): 'fax',
        '{}_EMAIL'.format(prefix[:4]): 'email',
        '{}_EMP'.format(prefix): 'employer',
        '{}_OCC'.format(prefix): 'occupation',
        '{}_SELF'.format(prefix): 'self_employed',
        '{}_TITLE'.format(prefix[:4]): 'title',
        '{}_INTER'.format(prefix[:4]): 'interest',
        '{}ACTVITY'.format(prefix[:3]): 'activity'
    }


def entity_date_map(prefix):
    return {'{}_DATE'.format(prefix): 'date'}


def translate_entity(filing_piece, entity_prefix, entity_label, item):
    entity = {}
    for key, value in entity_map(entity_prefix).items():
        if key in item and item[key]:
            entity[value] = util.postgres.sanitize_entry(item[key])
    for key, value in entity_date_map(entity_prefix).items():
        if key in item and item[key]:
            entity[value] = standardize_timestamp(
                item[key], date_format=DATE_FORMAT_MONTH_DAY_YEAR)
    if len(entity.keys()):
        filing_piece[entity_label] = entity


def build_name(item):
    KEY_LIST = ['prefix', 'first_name', 'last_name', 'suffix']
    entity_pieces = []
    for key in KEY_LIST:
        if key in item and item[key]:
            entity_pieces.append(item[key])
    if len(entity_pieces):
        return ' '.join(entity_pieces)
    else:
        return None


def standardize_timestamp(timestamp_str,
                          date_format=DATE_FORMAT_MONTH_DAY_YEAR):
    if not timestamp_str:
        return None
    date_str, time_str, am_or_pm = timestamp_str.split(' ')
    try:
        if date_format == DATE_FORMAT_DAY_MONTH_YEAR:
            day, month, year = date_str.split('/')
        elif date_format == DATE_FORMAT_MONTH_DAY_YEAR:
            month, day, year = date_str.split('/')
        else:
            print('Invalid date format')
            return None
    except Exception as e:
        print('Failed to split {}'.format(date_str))
        print(e)
    try:
        hour, minute, second = time_str.split(':')
    except Exception as e:
        print('Failed to split {}'.format(time_str))
        print(e)
    if am_or_pm.lower() == 'pm' and time_str != '12:00:00':
        hour = str(int(hour) + 12).zfill(2)
    return '{}-{}-{}T{}:{}:{}Z'.format(year, month.zfill(2), day.zfill(2),
                                       hour, minute, second)


def patch_s496_components(components,
                          header,
                          verbose=True,
                          very_verbose=False,
                          line_number=None):
    '''Patches the components from a line of the S496_CD.TSV.

    We expect the following list of components:

      1. FILING_ID
      2. AMEND_ID
      3. LINE_ITEM
      4. REC_TYPE
      5. FORM_TYPE
      6. TRAN_ID
      7. AMOUNT
      8. EXP_DATE
      9. EXPN_DSCR
      10. MEMO_CODE
      11. MEMO_REFNO
      12. DATE_THRU

    Args:
      components: The list of components from a line of the TSV.
      header: The array of header names that should match our 52-entry list.
      verbose: Whether to print patching information.
      line_number: The line number of the TSV (for debugging purposes).

    Returns:
      Either the patched list of components or (in the case of failure) None.
    '''
    EXPECTED_LEN = 12

    x = components.copy()

    if len(x) < EXPECTED_LEN:
        if verbose:
            print('Dropping {} because it only had {} components'.format(
                x, len(x)))
        return None

    if len(x) > EXPECTED_LEN:
        num_extra = len(x) - EXPECTED_LEN
        empty_extras = True
        for component in x[EXPECTED_LEN:]:
            if component:
                empty_extras = False
        if empty_extras:
            if verbose:
                print('Dropping {} empty extras from {}'.format(num_extra, x))
            x = x[:EXPECTED_LEN]
        else:
            return None

    return x


def patch_lobbying_cover_components(components,
                                    header,
                                    verbose=True,
                                    very_verbose=False,
                                    line_number=None):
    '''Patches the components from a line of the CVR_LOBBY_DISCLOSURE TSV.

    We expect the following list of components:

      1. FILING_ID: Unique filing identification number for the filing.
      2. AMEND_ID: Amendment identification number.
      3. REC_TYPE: Record Type Value. This will always be CVR.
      4. FORM_TYPE: Type of filing or formset with legal values of F615, F625,
        F635, or F645.
      5. SENDER_ID: Identification number of lobbyist entity that is submitting
        this report. This field is used to authenticate the filer and allows
        the firm to submit filings for their lobbyists.
      6. FILER_ID: Identification number of filer who is the subject of the
        report. (E.g. Lobbyist's ID for the 615 or the Firm's ID for the 625)
      7. ENTITY_CD: Entity code of the filer who is the subject of the report.
      8. FILER_NAML: Filer's last name or full name of the business entity.
      9. FILER_NAMF: Filer's first name.
      10. FILER_NAMT: The filer's prefix or title that preceeds their name if
        they are an individual.
      11. FILER_NAMS: Filer's name suffix.
      12. REPORT_NUM: Report Number; 000 Original; 001-999 Amended (as reported
        by the filer)
      13. RPT_DATE: Date this report is filed (as reported by the filer)
      14. FROM_DATE: Reporting period from date.
      15. THRU_DATE: Reporting period through date.
      16. CUM_BEG_DT: Cumulative period beginning date.
      17. FIRM_ID: Identification number of the Firm/Employer/Coalition.
      18. FIRM_NAME: Name of Firm/Employer/Coalition
      19. FIRM_CITY: Firm/Employer/Coalition or business's city.
      20. FIRM_ST: Firm/Employer/Coalition or business's state.
      21. FIRM_ZIP4: Firm/Employer/Coalition or business's zip code.
      22. FIRM_PHON: Firm/Employer/Coalition or business's phone number.
      23. MAIL_CITY: City portion of the filer's mailing address (if different
        than the street address).
      24. MAIL_ST: State portion of the filer's mailing address (if different
        than the street address).
      25. MAIL_ZIP4: Zip code portion of the filer's mailing address (if
        different than the street address).
      26. MAIL_PHON: Unused field carried in the CAL interface. Cox 5/4/2000
      27. SIG_DATE: Date when signed
      28. SIG_LOC: City and State where signed
      29. SIG_NAML: Signer's "as signed" last name
      30. SIG_NAMF: Signer's "as signed" first name
      31. SIG_NAMT: Signer's "as signed" prefix or title
      32. SIG_NAMS: Signer's "as signed" suffix
      33. PRN_NAML: Signer "as signed" last name
      34. PRN_NAMF: Signer "as signed" first name
      35. PRN_NAMT: Signer "as signed" prefix or title
      36. PRN_NAMS: Signer "as signed" suffix
      37. SIG_TITLE: Title of signer
      38. NOPART1_CB: "No part I information" check-box. Legal values are "X"
        and null.
      39. NOPART2_CB: "No part II information" check-box. Legal values are "X"
        and null.
      40. PART1_1_CB: "Partners, Owners, ... Form 615 Attached" check-box.
        Legal values are "X" and null.
      41. PART1_2_CB: "Partners, Owners, ... Listed Below" check-box. Legal
        values are "X" and null.
      42. CTRIB_N_CB: "Campaign Contributions? None" check-box. Legal values are
        "X" and null.
      43. CTRIB_Y_CB: "Campaign Contributions? P4 attached" check-box. Legal
        values are "X" and null.
      44. LBY_ACTVTY: Description of lobbying activity. Applies to the Form
        635/645. Additional description may be provided by the filer using text
        records.
      45. LOBBY_N_CB: "Lobby Coalition - None" check-box. Used for form
        rendering. Legal values are "X" and null.
      46. LOBBY_Y_CB: "Lobby Coalition - F630 Attached" check-box. Used for
        form rendering. Legal values are "X" and null.
      47. MAJOR_NAML: Major donor last name or business name.
      48. MAJOR_NAMF: Major donor first name if an individual.
      49. MAJOR_NAMT: Major donor prefix or title if an individual.
      50. MAJOR_NAMS: Major donor suffix if an individual.
      51. RCPCMTE_NM: Recipient Committee name
      52. RCPCMTE_ID: Recipient Committee or Major donor identification number.

    Args:
      components: The list of components from a line of the TSV.
      header: The array of header names that should match our 52-entry list.
      verbose: Whether to print patching information.
      line_number: The line number of the TSV (for debugging purposes).

    Returns:
      Either the patched list of components or (in the case of failure) None.
    '''
    EXPECTED_LEN = 52
    DATE_INDICES = [12, 13, 14, 15, 26]
    PHONE_INDICES = [21, 25]
    CHECK_BOX_INDICES = [37, 38, 39, 40, 41, 42, 44, 45]

    def components_str(x, header):
        desc = '\n'
        for i in range(len(x)):
            if x[i]:
                desc += '  {}, {}: {}\n'.format(
                    i, header[i] if i < len(header) else 'OUT_OF_BOUNDS', x[i])
        return desc

    line_str = '{}: '.format(line_number) if line_number else ''

    x = components.copy()
    for i in range(len(x)):
        x[i] = x[i].strip()

    if len(x) < EXPECTED_LEN:
        if verbose:
            print('{}Only {} components: {}'.format(line_str, len(x),
                                                    components_str(x, header)))
        return None

    # If there is still overflow, absorb into the lobbying activity description.
    if len(x) > EXPECTED_LEN:
        num_extra = len(x) - EXPECTED_LEN
        if verbose:
            print('{}Backshifting indices by {} of {}'.format(
                line_str, num_extra, components_str(x, header)))
        x[43] = '  '.join(x[43:43 + num_extra + 1])
        for i in range(44, EXPECTED_LEN):
            x[i] = x[i + num_extra]
        x = x[:EXPECTED_LEN]
        if verbose:
            print('{}Backshift produced {}'.format(line_str,
                                                   components_str(x, header)))

    # See if the phone numbers are in the right places.
    for i, component in enumerate(x):
        if not component:
            continue
        is_phone = PHONE_RE.match(component)
        if is_phone and i not in PHONE_INDICES:
            if verbose:
                print('{}Matched phone on {}, {}, which is not in {} of {}'.
                      format(line_str, i, x[i], PHONE_INDICES,
                             components_str(x, header)))
            return None
        if i in PHONE_INDICES and not is_phone:
            # We don't throw an error because the rest of the data could be
            # valid and user error on the phone number is common.
            if very_verbose:
                print('{}Did not match phone on {} in {}, {}, of {}'.format(
                    line_str, i, PHONE_INDICES, x[i],
                    components_str(x, header)))

    # See if a timestamp showed up in an invalid place.
    for i, component in enumerate(x):
        if not component:
            continue
        is_timestamp = TIMESTAMP_RE.match(component)
        if is_timestamp and i not in DATE_INDICES:
            if verbose:
                print('{}Matched timestamp on {} which is not in {} of {}'.
                      format(line_str, i, DATE_INDICES,
                             components_str(x, header)))
            return None
        if i in DATE_INDICES and not is_timestamp:
            if verbose:
                print('{}Did not match timestamp on {} in {} of {}'.format(
                    line_str, i, DATE_INDICES, components_str(x, header)))
            return None

    # See if the check-box indices are either an X or null.
    for i in CHECK_BOX_INDICES:
        component = x[i]
        if component and component.lower() != 'x':
            if verbose:
                print(
                    '{}Did not match check-box on {} in {}, {}, of {}'.format(
                        line_str, i, CHECK_BOX_INDICES, x[i],
                        components_str(x, header)))
            return None

    return x


def patch_lobbying_payments_components(components,
                                       header,
                                       verbose=True,
                                       very_verbose=False,
                                       line_number=None):
    '''Patches the components from a line of the LPAY TSV.

    We expect the following list of components:

      1. FILING_ID: Unique filing identification number.
      2. AMEND_ID: Amendment identification number. A number of 0 is an original
        filing and 1 to 999 amendments.
      3. LINE_ITEM: Record line item number.
      4. REC_TYPE: Record Type Value, LPAY.
      5. FORM_TYPE: Schedule Name/ID, either F625P2 or F635P3B.
      6. TRAN_ID: Transaction identifier - permanent value unique to this item.
      7. ENTITY_CD: Entity code.
      8. EMPLR_NAML: Name of firm, employer, coalition
      9. EMPLR_NAMF: Employer first name
      10. EMPLR_NAMT: Employer prefix or title
      11. EMPLR_NAMS: Employer suffix
      12. EMPLR_CITY: Employer city
      13. EMPLR_ST: Employer state
      14. EMPLR_ZIP4: Employer zip code
      15. EMPLR_PHON: Employer phone number.
      16. LBY_ACTVTY: Description of lobbying activity.
      17. FEES_AMT: Fees and retainers amount.
      18. REIMB_AMT: Reimbursements of expense amount.
      19. ADVAN_AMT: Advance and other payments amount.
      20. ADVAN_DSCR: Description of advance and other payments.
      21. PER_TOTAL: Total this reporting period.
      22. CUM_TOTAL: Cumulative total to date.
      23. MEMO_CODE: Memo amount flag.
      24. MEMO_REFNO: Reference to text contained in a TEXT record
      25. BAKREF_TID: Back reference to a transaction identifier of a parent
        record.
      26. EMPLR_ID: [No description given]

    Args:
      components: The list of components from a line of the TSV.
      header: The array of header names that should match our 26-entry list.
      verbose: Whether to print patching information.
      line_number: The line number of the TSV (for debugging purposes).

    Returns:
      Either the patched list of components or (in the case of failure) None.
    '''
    EXPECTED_LEN = 26
    PHONE_INDICES = [14]
    FLOAT_INDICES = [16, 17, 18, 20, 21]

    def components_str(x, header):
        desc = '\n'
        for i in range(len(x)):
            if x[i]:
                desc += '  {}, {}: {}\n'.format(
                    i, header[i] if i < len(header) else 'OUT_OF_BOUNDS', x[i])
        return desc

    line_str = '{}: '.format(line_number) if line_number else ''

    x = components.copy()
    for i in range(len(x)):
        x[i] = x[i].strip()

    if len(x) < EXPECTED_LEN:
        if verbose:
            print('{}Only {} components: {}'.format(line_str, len(x),
                                                    components_str(x, header)))
        return None

    # If there is an extra blank and the integer field directly after the
    # lobbying activity is filled with an integer, just drop the extra blank.
    if (len(x) == EXPECTED_LEN + 1 and POSITIVE_INTEGER_RE.match(x[16])
            and not x[EXPECTED_LEN]):
        x = x[:-1]

    if len(x) > EXPECTED_LEN:
        num_extra = len(x) - EXPECTED_LEN

        def float_not_integer(value):
            return (FLOAT_RE.match(value)
                    and not POSITIVE_INTEGER_RE.match(value))

        # Determine the maximum number of the extra fields we can absorb without
        # pulling in a purely numeric entry into the text description.
        num_mergable = 0
        for i in range(num_extra):
            value = x[16 + i]

            if FLOAT_RE.match(value):
                dscr_value = x[19 + i]
                memo_code_value = x[22 + i]
                memo_refno_value = x[23 + i]
                dscr_is_float = FLOAT_RE.match(dscr_value)
                memo_code_is_float_not_integer = float_not_integer(
                    memo_code_value)
                memo_refno_is_float_not_integer = float_not_integer(
                    memo_refno_value)
                if (POSITIVE_INTEGER_RE.match(value)
                        and (dscr_is_float or memo_code_is_float_not_integer
                             or memo_refno_is_float_not_integer)):
                    # We will assume the integer candidate to be merged into
                    # the lobbying activity is a bill number because it would
                    # move a floating point value out of a non-float field.
                    pass
                else:
                    break
            num_mergable += 1

        num_chopped = 0
        for i in range(num_extra - num_mergable):
            if x[-1]:
                break
            else:
                x = x[:-1]
                num_chopped += 1

        # Merge what we can into the lobbying activity.
        if verbose:
            print('{}Backshifting indices by {} of {}'.format(
                line_str, num_mergable, components_str(x, header)))
        x[15] = '  '.join(x[15:15 + num_mergable + 1])
        for i in range(16, EXPECTED_LEN):
            x[i] = x[i + num_mergable]
        x = x[:EXPECTED_LEN]
        if verbose:
            print('{}Backshift produced {}'.format(line_str,
                                                   components_str(x, header)))

        if num_chopped + num_mergable < num_extra:
            print('Could not merge enough entries.')
            return None

    # See if the phone numbers are in the right places.
    for i, component in enumerate(x):
        if not component:
            continue
        is_phone = PHONE_RE.match(component)
        if is_phone and i not in PHONE_INDICES:
            if verbose:
                print('{}Matched phone on {}, {}, which is not in {} of {}'.
                      format(line_str, i, x[i], PHONE_INDICES,
                             components_str(x, header)))
            return None
        if i in PHONE_INDICES and not is_phone:
            # We don't throw an error because the rest of the data could be
            # valid and user error on the phone number is common.
            if very_verbose:
                print('{}Did not match phone on {} in {}, {}, of {}'.format(
                    line_str, i, PHONE_INDICES, x[i],
                    components_str(x, header)))

    # See if each of the float indices can be parsed.
    for i in FLOAT_INDICES:
        component = x[i]
        if not component:
            continue
        if component and not FLOAT_RE.match(component):
            if verbose:
                print('{}Did not match float on {} in {} of {}'.format(
                    line_str, i, FLOAT_INDICES, x))
            return None

    return x


def patch_text_memo_components(components,
                               header,
                               verbose=True,
                               very_verbose=False,
                               line_number=None):
    '''Patches the components from a line of the TEXT_MEMO TSV.

    We expect the following list of components:

      1. FILING_ID: Unique filing identification number
      2. AMEND_ID: Amendment Identification number. A number of 0 is an original
        filing and 1 to 999 are amendments.
      3. LINE_ITEM: Record line item number
      4. REC_TYPE: Record type value
      5. FORM_TYPE: The type of form the memo text is associated with.
      6. REF_NO: The integer used to denote this memo text reference.
      7. TEXT4000: The extra text.

    Args:
      components: The list of components from a line of the TSV.
      header: The array of header names that should match our 7-entry list.
      verbose: Whether to print patching information.
      line_number: The line number of the TSV (for debugging purposes).

    Returns:
      Either the patched list of components or (in the case of failure) None.
    '''
    EXPECTED_LEN = 7

    def components_str(x, header):
        desc = '\n'
        for i in range(len(x)):
            if x[i]:
                desc += '  {}, {}: {}\n'.format(
                    i, header[i] if i < len(header) else 'OUT_OF_BOUNDS', x[i])
        return desc

    line_str = '{}: '.format(line_number) if line_number else ''

    x = components.copy()
    for i in range(len(x)):
        x[i] = x[i].strip()

    if len(x) < EXPECTED_LEN:
        if verbose:
            print('{}Only {} components: {}'.format(line_str, len(x),
                                                    components_str(x, header)))
        return None

    if len(x) > EXPECTED_LEN:
        num_extra = len(x) - EXPECTED_LEN

        x[6] = '  '.join(x[6:])
        x = x[:EXPECTED_LEN]

    return x


def patch_receipt_address(x, offset=11, verbose=True, line_number=None):
    '''Attempts an in-place fix of the address components of an RCPT line.

    Args:
      x: The list of components for a line of the original RCPT CSV.
      offset: The index the address components begin.
      verbose: Whether patching information should be printed.
      line_number: The line number from the CSV (for debugging purposes).
    '''
    LONDON_SUB_RE = re.compile(r'(?i)^london\w+$')
    LONDON_RE = re.compile(r'(?i)^london\s*(\w*)$')

    LONDON_POSTAL_RE = re.compile(r'\w\w\w?\w? ?\w\w?\w?')

    ENGLAND_POSTCODES = [
        'ab', 'al', 'b', 'ba', 'bb', 'bd', 'bh', 'bl', 'bn', 'br', 'bs', 'bt',
        'ca', 'cb', 'cf', 'ch', 'cm', 'co', 'cr', 'ct', 'cv', 'cw', 'da', 'dd',
        'de', 'dg', 'dh', 'dl', 'dn', 'dt', 'dy', 'e', 'ec', 'eh', 'en', 'ex',
        'fk', 'fy', 'g', 'gl', 'gu', 'ha', 'hd', 'hg', 'hp', 'hr', 'hs', 'hu',
        'hx', 'ig', 'ip', 'iv', 'ka', 'kt', 'kw', 'ky', 'l', 'la', 'ld', 'le',
        'll', 'ln', 'ls', 'lu', 'm', 'me', 'mk', 'ml', 'n', 'ne', 'ng', 'nn',
        'np', 'nr', 'nw', 'ol', 'ox', 'pa', 'pe', 'ph', 'pl', 'po', 'pr', 'rg',
        'rh', 'rm', 's', 'sa', 'se', 'sg', 'sk', 'sl', 'sm', 'sn', 'so', 'sp',
        'sr', 'ss', 'st', 'sw', 'sy', 'ta', 'td', 'tf', 'tn', 'tq', 'tr', 'ts',
        'tw', 'ub', 'w', 'wa', 'wc', 'wd', 'wf', 'wn', 'wr', 'ws', 'wv', 'yo',
        'ze'
    ]
    LONDON_CA_PROVINCES = ['on']
    LONDON_US_STATES = ['ky', 'mi', 'nj', 'ny', 'oh']
    UK_CODES = ['0', 'fs', 'gb', 'uk', 'xx']

    line_str = '{}: '.format(line_number) if line_number else ''

    # Specially handle London addresses.
    london_sub_match = LONDON_SUB_RE.match(x[offset])
    london_match = LONDON_RE.match(x[offset])
    london_proper_match = london_match and not london_sub_match
    if london_proper_match and x[offset + 2].lower() in ['england', 'uk']:
        if verbose:
            print('{}Modifying {} due to London UK match.'.format(line_str, x))
        x[offset] = 'London'
        postal_unit = '{} {}'.format(london_match.group(1), x[offset + 1])
        x[offset + 1] = 'UK'
        x[offset + 2] = postal_unit
    elif london_proper_match and x[offset + 1].lower() in ENGLAND_POSTCODES:
        x[offset + 2] = '{} {}'.format(x[offset + 1], x[offset + 2])
        x[offset + 1] = ''
    elif london_proper_match and x[offset + 1].lower() in LONDON_US_STATES:
        # The match is a US city named London.
        pass
    elif london_proper_match and x[offset + 1].lower() in LONDON_CA_PROVINCES:
        # The match is a Canadian city named London.
        pass
    elif london_proper_match and x[offset + 1].lower() in UK_CODES:
        # The match is a US-formatted London, UK address.
        pass
    elif london_proper_match and LONDON_POSTAL_RE.match(x[offset + 2]):
        # The match is a US-formatted London, UK address.
        pass
    elif london_proper_match and x[offset + 1]:
        # NOTE: This may cause problems if the country is not England.
        # There is no state and the database skips it.
        if verbose:
            print(
                '{}Inserting empty state code in {} to handle London.'.format(
                    line_str, x))
        x.insert(offset + 1, '')
    elif POSITIVE_INTEGER_RE.match(x[offset + 1]) and (
            x[offset + 2] and not POSITIVE_INTEGER_RE.match(x[offset + 2])):
        if verbose:
            print(
                '{}Removing index {} from {}, {}, because it was integral and appears to be a superfluous addition to the city, {}'
                .format(line_str, offset + 1, x, x[offset + 1], x[offset]))
        x.pop(offset + 1)


def patch_receipt_components(components,
                             header,
                             verbose=True,
                             line_number=None):
    '''Patches the components from a line of the RCPT TSV.

    We expect the following list of components:

      1. FILING_ID: Unique filing identification number
      2. AMEND_ID: Amendment Identification number. A number of 0 is an original
        filing and 1 to 999 are amendments.
      3. LINE_ITEM: Record line item number
      4. REC_TYPE: Record type value: RCPT
      5. FORM_TYPE: Schedule Name/ID: Sched A, C, I, A-1, F401A
      6. TRAN_ID: Transaction identifier - permanent value unique to this item
      7. ENTITY_CD: Entity code: Values are CMO, RCP, IND, and OTH.
      8. CTRIB_NAML: Contributor's last name or business name.
      9. CTRIB_NAMF: Contributor's first name.
      10. CTRIB_NAMT: Contributor's name prefix or title.
      11. CTRIB_NAMS: Contributor's name suffix.
      12. CTRIB_CITY: Contributor's city.
      13. CTRIB_ST: Contributor's state.
      14. CTRIB_ZIP4: Contributor's zip code.
      15. CTRIB_EMP: Contributor's employer.
      16. CTRIB_OCC: Contributor's occupation.
      17. CTRIB_SELF: Contributor is self-employed check-box.
      18. TRAN_TYPE: Transaction Type: Values are T for third party, F for
        Forgiven loan, and R for Returned (negative amount)
      19. RCPT_DATE: Date item received.
      20. DATE_THRU: End of date range for items received
      21. AMOUNT: Amount Received (Monetary, In-kind, Promise)
      22. CUM_YTD: Cumulative year-to-date amount (Form 460 Schedule A and Form
        401 Schedule A, A-1)
      23. CUM_OTH: Cumulative Other (Sched A, A-1)
      24. CTRIB_DSCR: Description of goods/services received.
      25. CMTE_ID: Committee Identification number.
      26. TRES_NAML: Treasurer or responsible officer's last name.
      27. TRES_NAMF: Treasurer or responsible officer's first name.
      28. TRES_NAMT: Treasurer or responsible officer's name prefix or title.
      29. TRES_NAMS: Treasurer or responsible officer's name suffix.
      30. TRES_CITY: Treasurer or responsible officer's city.
      31. TRES_ST: Treasurer or responsible officer's state.
      32. TRES_ZIP4: Treasurer or responsible officer's zip code.
      33. INTR_NAML: Intermediary's last name.
      34. INTR_NAMF: Intermediary's first name.
      35. INTR_NAMT: Intermediary's name prefix or title.
      36. INTR_NAMS: Intermediary's name suffix.
      37. INTR_CITY: Intermediary's city.
      38. INTR_ST: Intermediary's state.
      39. INTR_ZIP4: Intermediary's zip code.
      40. INTR_EMP: Intermediary's employer.
      41. INTR_OCC: Intermediary's occupation.
      42. INTR_SELF: Intermediary is self-employed check-box.
      43. CAND_NAML: Candidate/officeholder's last name.
      44. CAND_NAMF: Candidate/officeholder's first name.
      45. CAND_NAMT: Candidate/officeholder's name prefix or title.
      46. CAND_NAMS: Candidate/officeholder's name suffix.
      47. OFFICE_CD: Code that identifies the office being sought. See the CAL
        document for a list of valid codes.
      48. OFFIC_DSCR: Office Sought Description (used on F401A)
      49. JURIS_CD: Office jurisdiction code. See the CAL document for the list
        of legal values. Used on Form 401 Schedule A.
      50. JURIS_DSCR: Office Jurisdiction Description (used on F401A)
      51. DIST_NO: Office District Number (used on F401A)
      52. OFF_S_H_CD: Office Sought or Held Code. Legal values are "S" for
        sought and "H" for held.
      53. BAL_NAME: Ballot measure name. Used on the Form 401 Schedule A.
      54. BAL_NUM: Ballot measure number or letter. Used on the Form 401
        Schedule A.
      55. BAL_JURIS: Jurisdiction of ballot measure. Used on the Form 401
        Schedule A.
      56. SUP_OPP_CD: Support or oppose code. Legal values are "S" for support
        or "O" for oppose.
      57. MEMO_CODE: Memo amount flag
      58. MEMO_REFNO: Reference to text contained in a TEXT record
      59. BAKREF_TID: Back Reference to a transaction identifier of a parent
        record.
      60. XREF_SCHNM: Related item on other schedule has same transaction
        identifier.
      61. XREF_MATCH: Related record is included on Sched 'B2' or 'F'
      62. INT_RATE: [No description given]
      63. INTR_CMTEID: [No description given]

    Args:
      components: The list of components from a line of the RCPT TSV.
      verbose: Whether to print patching information.
      line_number: The line number of the RCPT TSV (for debugging purposes).

    Returns:
      Either the patched list of components or (in the case of failure) None.
    '''
    EXPECTED_LEN = 63

    FILING_ID_INDEX = 0
    ENTITY_CD_INDEX = 6

    RCPT_DATE_INDEX = 18
    DATE_THRU_INDEX = 19
    DATE_INDICES = [RCPT_DATE_INDEX, DATE_THRU_INDEX]

    AMOUNT_INDEX = 20
    CUM_YTD_INDEX = 21
    CUM_OTH_INDEX = 22
    FLOAT_INDICES = [AMOUNT_INDEX, CUM_YTD_INDEX, CUM_OTH_INDEX]

    x = components.copy()
    for i in range(len(x)):
        x[i] = x[i].strip()

    line_str = '{}: '.format(line_number) if line_number else ''

    if len(x) < EXPECTED_LEN:
        if verbose:
            print('{}Only {} components: {}'.format(line_str, len(x), x))
        return None

    # Note: While it would seem to make sense to remove fields which simply
    # consist of a single '.', in most cases the correct response is
    # replacement with a blank entry.

    if x[FILING_ID_INDEX] == 'BER CONTENT':
        # Rows beginning with 'BER CONTENT' seem to be misformatted, and
        # frequently associated with Hollywood.
        return None

    if x[ENTITY_CD_INDEX] == 'OTH':
        # While corrupted rows are more common for 'Other' entity types,
        # there are far too many instances to skip the field entirely.
        pass

    if x[15] == 'Exec.' and x[16] == 'Director of Non Profit':
        # Specially handle a parser error that falsely splits the title
        # 'Exec. Director of Non Profit'.
        x[15] = '{} {}'.format(x[15], x[16])
        x.pop(16)

    if len(x) < EXPECTED_LEN:
        if verbose:
            print('{}Only {} components: {}'.format(line_str, len(x), x))
        return None

    patch_receipt_address(x,
                          offset=11,
                          verbose=verbose,
                          line_number=line_number)

    # See if a timestamp showed up in an invalid place.
    for i, component in enumerate(x):
        if not component:
            continue
        is_timestamp = TIMESTAMP_RE.match(component)
        if is_timestamp and i not in DATE_INDICES:
            if verbose:
                print('{}Matched timestamp on {} which is not in {} of {}'.
                      format(line_str, i, DATE_INDICES, x))
            return None
        if i in DATE_INDICES and not is_timestamp:
            if verbose:
                print('{}Did not match timestamp on {} in {} of {}'.format(
                    line_str, i, DATE_INDICES, x))
            return None

    # See if each of the float indices can be parsed.
    for i in FLOAT_INDICES:
        component = x[i]
        if not component:
            continue
        if component and not FLOAT_RE.match(component):
            if verbose:
                print('{}Did not match float on {} in {} of {}'.format(
                    line_str, i, FLOAT_INDICES, x))
            return None

    if len(x) > EXPECTED_LEN:
        x_kept = x[:EXPECTED_LEN]
        x_drop = x[EXPECTED_LEN:]
        if verbose:
            print('{}Shrinking to first {} entries of {}, dropping {}'.format(
                line_str, EXPECTED_LEN, x, x_drop))
        for value in x_drop:
            if value:
                print('  Attempted to truncate nonempty value')
                return None
        x = x_kept

    return x


def patch_receipts_cover_components(components,
                                    header,
                                    verbose=True,
                                    very_verbose=False,
                                    line_number=None):
    '''Patches the components from a line of the CVR_CAMPAIGN_DISCLOSURE TSV.

    We expect the following list of components:

      1. FILING_ID: Unique filing identification number for the filing.
      2. AMEND_ID: Amendment identification number.
      3. REC_TYPE: Record Type Value (CVR).
      4. FORM_TYPE: Type of filing.
      5. FILER_ID: Identification number of filer who is the subject of the
        report.
      6. ENTITY_CD: Entity code of the filer who is the subject of the report.
      7. FILER_NAML: The committee's or organization's name or, if an
        individual, their last name.
      8. FILER_NAMF: The filer's first name (if an individual).
      9. FILER_NAMT: The filer's title or prefix (if an individual).
      10. FILER_NAMS: The filer's name suffix (if an individual).
      11. REPORT_NUM: Amendment number as reported by the filer. 000 represents
        an original filing and 001-999 an amendment.
      12. RPT_DATE: Date the report was filed (according to the filer).
      13. STMT_TYPE: Type of statement.
      14. LATE_RPTNO: Identifying Report Number used to distinguish reports
        filed during the same period.
      15. FROM_DATE: The beginning of the reporting period.
      16. THRU_DATE: The end of the reporting period.
      17. ELECT_DATE: Date of the general election.
      18. FILER_CITY: The city of the filer.
      19. FILER_ST: The state of the filer.
      20. FILER_ZIP4: The zip code of the filer.
      21. FILER_PHON: The phone number of the filer.
      22. FILER_FAX: The fax number of the filer.
      23. FILE_EMAIL: The email address of the filer.
      24. MAIL_CITY: The city of the mailing address of the filer.
      25. MAIL_ST: The state of the mailing address of the filer.
      26. MAIL_ZIP4: The zip code of the mailing address of the filer.
      27. TRES_NAML: Last name of treasurer or responsible officer.
      28. TRES_NAMF: First name of treasurer or responsible officer.
      29. TRES_NAMT: Title or prefix of treasurer or responsible officer.
      30. TRES_NAMS: Name suffix of treasurer or responsible officer.
      31. TRES_CITY: The city of the treasurer or responsible officer.
      32. TRES_ST: The state of the treasurer or responsible officer.
      33. TRES_ZIP4: The zip code of the treasurer or responsible officer.
      34. TRES_PHON: The phone number of the treasurer or responsible officer.
      35. TRES_FAX: The fax number of the treasurer or responsible officer.
      36. TRES_EMAIL: The email address of the treasurer or responsible officer.
      37. CMTTE_TYPE: The type of the recipient committee.
      38. CONTROL_YN: Controlled Committee? (Legal values are 'Y' and 'N')
      39. SPONSOR_YN: Sponsored Committee? (Legal values are 'Y' and 'N')
      40. PRIMFRM_YN: Primarily Formed Committee? (Legal values are 'Y' and 'N')
      41. BRDBASE_YN: Broad Base Committee? (Legal values are 'Y' and 'N')
      42. AMENDEXP_1: Amendment explanation (line 1).
      43. AMENDEXP_2: Amendment explanation (line 2).
      44. AMENDEXP_3: Amendment explanation (line 3).
      45. RPT_ATT_CB: Committee Report Attached? Legal values are 'X' or null.
      46. CMTTE_ID: Committee ID of recipient committee who's campaign statement
        is attached.
      47. REPORTNAME: Attached campaign disclosure statement type.
      48. RPTFROMDT: Attached campaign disclosure statement's period begin.
      49. RPTTHRUDT: Attached campaign disclosure statement's period end.
      50. EMPLBUS_CB: Employer/Business info included? Legal values are 'X' or
        null.
      51. BUS_NAME: Name of employer or business.
      52. BUS_CITY: City of employer or business.
      53. BUS_ST: State of employer or business.
      54. BUS_ZIP4: Zip code of employer or business.
      55. BUS_INTER: Interest description of employer or business.
      56. BUSACT_CB: Business activity info included? Values are 'X' or null.
      57. BUSACTVITY: Business activity description.
      58. ASSOC_CB: Association interests info included? Legal values are 'X'
        or null.
      59. ASSOC_INT: Description of association interests.
      60. OTHER_CB: Other entity interests info included? Legal values are 'X'
        or null.
      61. OTHER_INT: Other entity interests description.
      62. CAND_NAML: Last name of candidate or officeholder.
      63. CAND_NAMF: First name of candidate or officeholder.
      64. CAND_NAMT: Title or prefix of candidate or officeholder.
      65. CAND_NAMS: Name suffix of candidate or officeholder.
      66. CAND_CITY: City of candidate.
      67. CAND_ST: State of candidate.
      68. CAND_ZIP4: Zip code of candidate.
      69. CAND_PHON: Phone number of candidate.
      70. CAND_FAX: Fax number of candidate.
      71. CAND_EMAIL: Email address of candidate.
      72. BAL_NAME: Name of ballot measure.
      73. BAL_NUM: Number or letter of ballot measure.
      74. BAL_JURIS: Jurisidiction of ballot measure.
      75. OFFICE_CD: Code of the office being sought.
      76. OFFIC_DSCR: Description of the office being sought.
      77. JURIS_CD: Code of the office jurisdiction.
      78. JURIS_DSCR: Description of the office jurisdiction.
      79. DIST_NO: District number for the office being sought.
      80. OFF_S_H_CD: Office is sought or held code.
      81. SUP_OPP_CD: Support or opposition code.
      82. EMPLOYER: Employer (typically unused).
      83. OCCUPATION: Occupation (typically unused).
      84. SELFEMP_CB: Self-employed checkbox.
      85. BAL_ID: Unused.
      86. CAND_ID: Unused.

    Args:
      components: The list of components from a line of the TSV.
      header: The array of header names that should match our 52-entry list.
      verbose: Whether to print patching information.
      line_number: The line number of the TSV (for debugging purposes).

    Returns:
      Either the patched list of components or (in the case of failure) None.
    '''
    EXPECTED_LEN = 86
    DATE_INDICES = [11, 14, 15, 16, 47, 48]
    PHONE_INDICES = [20, 21, 33, 34, 68, 69]
    EMAIL_INDICES = [22, 35, 70]
    REPORT_NUMBER_INDICES = [13]
    CHECK_BOX_INDICES = [44, 49, 55, 57, 59, 83]
    YES_NO_INDICES = [37, 38, 39, 40]
    SOUGHT_OR_HELD_INDICES = [79]
    SUPP_OR_OPP_INDICES = [80]

    def components_str(x, header):
        desc = '\n'
        for i in range(len(x)):
            if x[i]:
                desc += '  {}, {}: {}\n'.format(
                    i, header[i] if i < len(header) else 'OUT_OF_BOUNDS', x[i])
        return desc

    line_str = '{}: '.format(line_number) if line_number else ''

    x = components.copy()
    for i in range(len(x)):
        x[i] = x[i].strip()

    if len(x) < EXPECTED_LEN:
        if verbose:
            print('{}Only {} components: {}'.format(line_str, len(x),
                                                    components_str(x, header)))
        return None

    if len(x) > EXPECTED_LEN:
        num_extra = len(x) - EXPECTED_LEN
        extras = x[EXPECTED_LEN:]
        empty_extras = True
        for extra in extras:
            if extra:
                empty_extras = False
        if empty_extras:
            x = x[:EXPECTED_LEN]
            print('{}Chopping {} empty extras, components: {}'.format(
                line_str, num_extra, components_str(x, header)))
        else:
            print('{}num_extra: {}, components: {}'.format(
                line_str, num_extra, components_str(x, header)))
            return None

    # See if the phone numbers are in the right places.
    for i, component in enumerate(x):
        if not component:
            continue
        is_phone = PHONE_RE.match(component)
        if is_phone and i not in (PHONE_INDICES + EMAIL_INDICES +
                                  REPORT_NUMBER_INDICES):
            if verbose:
                print('{}Matched phone on {}, {}, which is not in {} of {}'.
                      format(line_str, i, x[i], PHONE_INDICES,
                             components_str(x, header)))
            return None
        if i in PHONE_INDICES and not is_phone:
            # We don't throw an error because the rest of the data could be
            # valid and user error on the phone number is common.
            if very_verbose:
                print('{}Did not match phone on {} in {}, {}, of {}'.format(
                    line_str, i, PHONE_INDICES, x[i],
                    components_str(x, header)))

    # See if a timestamp showed up in an invalid place.
    for i, component in enumerate(x):
        if not component:
            continue
        is_timestamp = TIMESTAMP_RE.match(component)
        if is_timestamp and i not in DATE_INDICES:
            if verbose:
                print('{}Matched timestamp on {} which is not in {} of {}'.
                      format(line_str, i, DATE_INDICES,
                             components_str(x, header)))
            return None
        if i in DATE_INDICES and not is_timestamp:
            if verbose:
                print('{}Did not match timestamp on {} in {} of {}'.format(
                    line_str, i, DATE_INDICES, components_str(x, header)))
            return None

    # See if the check-box indices are either an X or null.
    for i in CHECK_BOX_INDICES:
        component = x[i]
        if component and component.lower() != 'x':
            if verbose:
                print(
                    '{}Did not match check-box on {} in {}, {}, of {}'.format(
                        line_str, i, CHECK_BOX_INDICES, x[i],
                        components_str(x, header)))
            if component.lower() == 'y' or component.lower() == 'n':
                print('  Accepting Y or N answer for checkbox.')
            else:
                return None

    # See if the yes/no indices are either 'Y', 'N', or null.
    for i in YES_NO_INDICES:
        component = x[i]
        if component and component.lower() not in ['y', 'n']:
            if verbose:
                print('{}Did not match yes/no on {} in {}, {}, of {}'.format(
                    line_str, i, YES_NO_INDICES, x[i],
                    components_str(x, header)))
            if len(component.lower()) == 1:
                print('  Accepting single-character invalid yes/no')
            else:
                return None

    # See if the sought/held indices are 'S', 'H', 'F', 'O', or null.
    for i in SOUGHT_OR_HELD_INDICES:
        component = x[i]
        if component and component.lower() not in ['s', 'h', 'f', 'o']:
            if verbose:
                print('{}Did not match s/h on {} in {}, {}, of {}'.format(
                    line_str, i, SOUGHT_OR_HELD_INDICES, x[i],
                    components_str(x, header)))
            return None

    # See if the support/oppose indices are either 'S', 'O', or null.
    for i in SUPP_OR_OPP_INDICES:
        component = x[i]
        if component and component.lower() not in ['s', 'o']:
            if verbose:
                print('{}Did not match S/O on {} in {}, {}, of {}'.format(
                    line_str, i, SUPP_OR_OPP_INDICES, x[i],
                    components_str(x, header)))
            if len(component) == 1:
                print('  Accepting invalid single-character supp/opp')
            else:
                return None

    return x


def get_with_default(item, key, default):
    return item[key] if (key in item and item[key]) else default


def incorporate_memo(filing, memo):
    memo_text = util.postgres.sanitize_entry(memo['TEXT4000'])
    if not memo_text:
        # There is no point in keeping track of a blank memo.
        return

    def match_covers(memo):
        memo_text = util.postgres.sanitize_entry(memo['TEXT4000'])
        ref_no = memo['REF_NO']
        line_item = memo['LINE_ITEM']

        found_memo = False
        if not ref_no and not line_item:
            # Look in the covers
            for cover in filing['covers']:
                cover['memo'] = {'text': memo_text}
                found_memo = True
        return found_memo

    def match_memo_by_reference_or_transaction(memo, section):
        '''Try to match the reference number or transaction ID.'''
        if section not in filing:
            return False

        memo_text = util.postgres.sanitize_entry(memo['TEXT4000'])
        ref_no = memo['REF_NO']

        found_memo = False
        for piece in filing[section]:
            candidates = []
            if 'memo' in piece and 'reference' in piece['memo']:
                candidates.append(piece['memo']['reference'])
            if 'transaction' in piece and 'id' in piece['transaction']:
                candidates.append(piece['transaction']['id'])
            if ref_no in candidates:
                if 'memo' not in piece:
                    piece['memo'] = {}
                piece['memo']['text'] = memo_text
                found_memo = True
        return found_memo

    def match_memo_by_line_item(memo, section):
        '''Try to match the line item.'''
        if section not in filing:
            return False

        memo_text = util.postgres.sanitize_entry(memo['TEXT4000'])
        line_item = memo['LINE_ITEM']

        found_memo = False
        for piece in filing[section]:
            candidates = []
            if 'line_item' in piece:
                candidates.append(piece['line_item'])
            if line_item in candidates:
                if 'memo' not in piece:
                    piece['memo'] = {}
                piece['memo']['text'] = memo_text
                found_memo = True
        return found_memo

    def match_memo_if_single(memo, section):
        if section not in filing:
            return

        # If there is only a single payment with no memo yet, attach there.
        if len(filing[section]) == 1:
            piece = filing[section][0]
            if 'memo' in piece and 'text' in piece['memo']:
                return False
            else:
                if 'memo' not in piece:
                    filing[section][0]['memo'] = {}
                filing[section][0]['memo']['text'] = memo_text
                return True

    if match_covers(memo):
        return

    if match_memo_by_reference_or_transaction(memo, 'payments'):
        return
    if match_memo_by_reference_or_transaction(memo, 'campaign_contributions'):
        return
    if match_memo_by_reference_or_transaction(memo,
                                              'pre_election_expenditures'):
        return

    if match_memo_by_line_item(memo, 'payments'):
        return
    if match_memo_by_line_item(memo, 'campaign_contributions'):
        return
    if match_memo_by_line_item(memo, 'pre_election_expenditures'):
        return

    if match_memo_if_single(memo, 'payments'):
        return
    if match_memo_if_single(memo, 'campaign_contributions'):
        return
    if match_memo_if_single(memo, 'pre_election_expenditures'):
        return

    print('Could not find match for memo: {}'.format(memo))
    filing['unmatched_memos'].append({
        'amendment_id': memo['AMEND_ID'],
        'form_type': memo['FORM_TYPE'],
        'line_item': memo['LINE_ITEM'],
        'reference': memo['REF_NO'],
        'text': memo_text
    })


def sorted_filing(filing):
    ''''Returns a sorted filing (by amendment and/or line item).

    Args:
      filing: The input dictionary of covers, payments, etc.

    Returns:
      The sorted filing.
    '''

    def get_line_item_and_amendment(item):
        return (get_with_default(item, 'line_item',
                                 0), get_with_default(item, 'amendment_id', 0))

    def sort_section_by_amendment(f, section):
        if section not in f:
            return
        f[section] = sorted(
            f[section], key=lambda x: get_with_default(x, 'amendment_id', 0))

    def sort_section_by_line_item_and_amendment(f, section):
        if section not in f:
            return
        f[section] = sorted(f[section], key=get_line_item_and_amendment)

    sorted_filing = filing
    sort_section_by_amendment(sorted_filing, 'covers')
    sort_section_by_line_item_and_amendment(sorted_filing, 'payments')
    sort_section_by_line_item_and_amendment(sorted_filing,
                                            'campaign_contributions')
    sort_section_by_line_item_and_amendment(sorted_filing,
                                            'pre_election_expenditures')

    return sorted_filing


def truncate_filing_to_most_recent(filing):
    '''Returns a modified filing with only the most recent amendments.

    Args:
      filing: The input dictionary of covers, payments, etc. It is assumed that
          the covers and payments are presorted in increasing amendment order.

    Returns:
      The truncated filing.
    '''
    trunc_filing = filing

    # Drop all but the most recent cover.
    if len(trunc_filing['covers']):
        trunc_filing['covers'] = trunc_filing['covers'][-1:]

    # Only keep the most recent amendment of each line item of the following.
    sections = [
        'payments', 'campaign_contributions', 'pre_election_expenditures'
    ]
    for section in sections:
        if section not in trunc_filing:
            continue
        kept = {}
        for piece in trunc_filing[section]:
            if not 'line_item' in piece:
                print('WARNING: No line item in {} {}'.format(section, piece))
                continue
            line_item = piece['line_item']
            kept[line_item] = piece
        trunc_filing[section] = list(kept.values())

    return trunc_filing


def simplify_lobbying_disclosures(
        cvr_json=FILES['CVR_LOBBY_DISCLOSURE']['json'],
        cvr2_json=FILES['CVR2_LOBBY_DISCLOSURE']['json'],
        pay_json=FILES['LPAY']['json'],
        memo_json=FILES['TEXT_MEMO']['json'],
        output_cover_json=SIMPLIFIED_LOBBY_DISCLOSURE_COVER_JSON,
        output_cover_csv=SIMPLIFIED_LOBBY_DISCLOSURE_COVER_CSV,
        output_payment_json=SIMPLIFIED_LOBBY_DISCLOSURE_PAYMENT_JSON,
        output_payment_csv=SIMPLIFIED_LOBBY_DISCLOSURE_PAYMENT_CSV):
    CVR_ENTITY_PREFIXES = {
        'filer': 'FILER',
        'filer_mailing': 'MAIL',
        'firm': 'FIRM',
        'major_donor': 'MAJOR',
        'signer': 'SIG',
        'signer_prn': 'PRN'
    }
    CVR2_ENTITY_PREFIXES = {'entity': 'ENTY'}
    PAY_ENTITY_PREFIXES = {'employer': 'EMPLR'}

    CVR_DIRECT_MAP = {
        'amendment_id': 'AMEND_ID',
        'report_number': 'REPORT_NUM',
        'form_type': 'FORM_TYPE',
        'sender_id': 'SENDER_ID',
        'filer_id': 'FILER_ID',
        'entity_code': 'ENTITY_CD',
        'activity_description': 'LBY_ACTVTY'
    }
    CVR2_DIRECT_MAP = {'line_item': 'LINE_ITEM'}
    PAY_DIRECT_MAP = {
        'activity_description': 'LBY_ACTVTY',
        'amendment_id': 'AMEND_ID',
        'entity_code': 'ENTITY_CD',
        'form_type': 'FORM_TYPE',
        'line_item': 'LINE_ITEM',
        'parent_transaction_id': 'BAKREF_TID',
        'record_type': 'REC_TYPE'
    }

    CVR_DATE_MAP = {
        'date_filed': 'RPT_DATE',
        'period_begin': 'FROM_DATE',
        'period_end': 'THRU_DATE',
        'cumulative_begin': 'CUM_BEG_DT'
    }

    CVR_COMMITTEE_MAP = {'id': 'RCPCMTE_ID', 'name': 'RCPCMTE_NM'}

    CVR_CHECKBOX_MAP = {
        'no_part_1': 'NOPART1_CB',
        'no_part_2': 'NOPART2_CB',
        'partners_form_615_attached': 'PART1_1_CB',
        'partners_listed_below': 'PART1_2_CB',
        'contributions_none': 'CTRIB_N_CB',
        'contributions_p4_attached': 'CTRIB_Y_CB',
        'coalition_none': 'LOBBY_N_CB',
        'coalition_f630_attached': 'LOBBY_Y_CB'
    }

    CVR2_TRANSACTION_MAP = {'id': 'TRAN_ID'}
    PAY_TRANSACTION_MAP = {'id': 'TRAN_ID'}

    PAY_AMOUNT_MAP = {
        'advance_and_other': 'ADVAN_AMT',
        'advance_and_other_description': 'ADVAN_DSCR',
        'cumulative_total': 'CUM_TOTAL',
        'fees': 'FEES_AMT',
        'period_total': 'PER_TOTAL',
        'reimbursements': 'REIMB_AMT'
    }

    PAY_MEMO_MAP = {'code': 'MEMO_CODE', 'reference': 'MEMO_REFNO'}

    def generate_entities_list(filing, cover_entity_prefixes,
                               payment_entity_prefixes):
        '''Fills a filing with the list of mentioned entities.'''

        def incorporate_section(section, entity_prefixes, entities_set):
            if section not in filing:
                return
            piece = filing[section]
            for key in entity_prefixes:
                if key not in piece:
                    continue
                value = piece[key]
                name = build_name(value)
                if name:
                    entities_set.add(canonicalize(name))
                if 'employer' in value and value['employer']:
                    entities_set.add(canonicalize(value['employer']))

        entities_set = set()
        incorporate_section('cover', cover_entity_prefixes, entities_set)
        incorporate_section('payment', payment_entity_prefixes, entities_set)

        filing['entities'] = sorted(entities_set)

    filings = {}

    # Incorporate the CVR data.
    for item in json.load(open(cvr_json)):
        filing_id = item['FILING_ID']
        if filing_id not in filings:
            filings[filing_id] = {
                'filing_id': filing_id,
                'covers': [],
                'payments': [],
                'unmatched_memos': []
            }

        cover = {}

        for key, value in CVR_DIRECT_MAP.items():
            if value in item and item[value]:
                cover[key] = util.postgres.sanitize_entry(item[value])

        for key, value in CVR_DATE_MAP.items():
            if value in item and item[value]:
                cover[key] = standardize_timestamp(
                    item[value], date_format=DATE_FORMAT_MONTH_DAY_YEAR)

        committee = {}
        for key, value in CVR_COMMITTEE_MAP.items():
            if value in item and item[value]:
                committee[key] = util.postgres.sanitize_entry(item[value])
        if len(committee.keys()):
            cover['committee'] = committee

        for key, value in CVR_ENTITY_PREFIXES.items():
            translate_entity(cover, value, key, item)

        checkboxes = {}
        for key, value in CVR_CHECKBOX_MAP.items():
            if value in item and item[value]:
                checkboxes[key] = item[value]
        if len(checkboxes.keys()):
            cover['checkboxes'] = checkboxes

        filings[filing_id]['covers'].append(cover)

    # Incorporate the CVR2 data.
    for item in json.load(open(cvr2_json)):
        filing_id = item['FILING_ID']
        if filing_id not in filings:
            continue
        filing = filings[filing_id]

        amendment_id = item['AMEND_ID']

        found_cover = False
        for cover in filing['covers']:
            cover_amendment_id = get_with_default(cover, 'amendment_id', 0)
            if amendment_id != cover_amendment_id:
                continue

            found_cover = True

            for key, value in CVR2_DIRECT_MAP.items():
                if value in item and item[value]:
                    cover[key] = util.postgres.sanitize_entry(item[value])

            for key, value in CVR2_ENTITY_PREFIXES.items():
                translate_entity(cover, value, key, item)

            # Append the entity ID to the entity translation.
            if 'ENTITY_ID' in item and item['ENTITY_ID']:
                if 'entity' not in cover:
                    cover['entity'] = {}
                cover['entity']['id'] = item['ENTITY_ID']

            transaction = {}
            for key, value in CVR2_TRANSACTION_MAP.items():
                if value in item and item[value]:
                    transaction[key] = util.postgres.sanitize_entry(
                        item[value])
            if len(transaction.keys()):
                cover['transaction'] = transaction

        if not found_cover:
            print('ERROR: Did not find amendment ID {} in filing {}'.format(
                amendment_id, filing_id))
            print(filing['covers'])

    # Incorporate the lobbying payments.
    for item in json.load(open(pay_json)):
        filing_id = item['FILING_ID']
        if filing_id not in filings:
            filings[filing_id] = {
                'filing_id': filing_id,
                'covers': [],
                'payments': [],
                'unmatched_memos': []
            }

        payment = {}

        for key, value in PAY_DIRECT_MAP.items():
            if value in item and item[value]:
                payment[key] = util.postgres.sanitize_entry(item[value])

        for key, value in PAY_ENTITY_PREFIXES.items():
            translate_entity(payment, value, key, item)

        amount = {}
        for key, value in PAY_AMOUNT_MAP.items():
            if value in item and item[value]:
                amount[key] = item[value]
        if len(amount.keys()):
            payment['amount'] = amount

        transaction = {}
        for key, value in PAY_TRANSACTION_MAP.items():
            if value in item and item[value]:
                transaction[key] = util.postgres.sanitize_entry(item[value])
        if len(transaction.keys()):
            payment['transaction'] = transaction

        memo = {}
        for key, value in PAY_MEMO_MAP.items():
            if value in item and item[value]:
                memo[key] = util.postgres.sanitize_entry(item[value])
        if len(memo.keys()):
            payment['memo'] = memo

        filings[filing_id]['payments'].append(payment)

    # Incorporate the memo notes
    for memo in json.load(open(memo_json)):
        filing_id = memo['FILING_ID']
        if not filing_id in filings:
            continue
        filing = filings[filing_id]
        incorporate_memo(filing, memo)
        filings[filing_id] = filing

    #  Generate covers and payments files.
    cover_filings = []
    payment_filings = []
    for filing_id in list(filings.keys()):
        filing = filings[filing_id]

        filing = sorted_filing(filing)
        filing = truncate_filing_to_most_recent(filing)

        # Insert the cover.
        COVER_ENTITY_KEYS = ['entity', 'filer', 'signer', 'signer_prn']
        COVER_JSON_KEYS = ['cover', 'unmatched_memos', 'entities']
        cover = filing['covers'][-1] if len(filing['covers']) else {}
        chunk = {
            'filing_id': filing['filing_id'],
            'cover': cover,
            'unmatched_memos': filing['unmatched_memos'],
            'entities': []
        }
        generate_entities_list(chunk, COVER_ENTITY_KEYS, [])
        for key in COVER_JSON_KEYS:
            chunk[key] = json.dumps(chunk[key])
        cover_filings.append(chunk)

        # Insert the payments.
        PAYMENT_ENTITY_KEYS = ['employer']
        PAYMENT_JSON_KEYS = ['payment', 'entities']
        for payment in filing['payments']:
            chunk = {
                'filing_id': filing['filing_id'],
                'transaction_id': payment['transaction']['id'],
                'line_item': payment['line_item'],
                'payment': payment,
                'entities': []
            }
            generate_entities_list(chunk, [], PAYMENT_ENTITY_KEYS)
            for key in PAYMENT_JSON_KEYS:
                chunk[key] = json.dumps(chunk[key])
            payment_filings.append(chunk)

        del filings[filing_id]

    with open(output_cover_json, 'w') as outfile:
        json.dump(list(cover_filings), outfile, indent=1)
    json_to_csv(output_cover_json, output_cover_csv)

    with open(output_payment_json, 'w') as outfile:
        json.dump(list(payment_filings), outfile, indent=1)
    json_to_csv(output_payment_json, output_payment_csv)


def simplify_campaign_disclosures(
        s496_json=FILES['S496']['json'],
        receipts_json=FILES['RCPT']['json'],
        receipts_cover_json=FILES['CVR_CAMPAIGN_DISCLOSURE']['json'],
        memo_json=FILES['TEXT_MEMO']['json'],
        output_cover_csv=SIMPLIFIED_CAMPAIGN_DISCLOSURE_COVER_CSV,
        output_campaign_contrib_csv=SIMPLIFIED_CAMPAIGN_CONTRIBUTION_CSV,
        output_pre_elect_expend_csv=SIMPLIFIED_PRE_ELECTION_EXPENDITURE_CSV):
    '''Blends (corrected) original receipt and cover data into simplified form.

    Args:
      s496_json: JSON file containing corrected S496 pre-election gift info.
      receipts_json: JSON file containing corrected receipts info.
      receipts_cover_json: JSON file containing corrected receipts cover info.
      memo_json: JSON file containing the out-of-place text memos.
      output_cover_csv: Output CSV of campaign disclosure cover information.
      output_campaign_contrib_csv: Output CSV of campaign contribution info.
      output_pre_elect_expend_csv: Output CSV of pre-election expenditures.
    '''
    ENTITY_PREFIXES = {
        'contributor': 'CTRIB',
        'treasurer': 'TRES',
        'candidate': 'CAND',
        'intermediary': 'INTR'
    }

    CVR_DIRECT_MAP = {
        'filing_id': 'FILING_ID',
        'amendment_id': 'AMEND_ID',
        'record_type': 'REC_TYPE',
        'form_type': 'FORM_TYPE',
        'entity_code': 'ENTITY_CD',
        'report_number': 'REPORT_NUM',
        'late_report_number': 'LATE_RPTNO',
        'statement_type': 'STMT_TYPE',
        'attached_statement_type': 'REPORTNAME',
        'support_or_oppose_code': 'SUP_OPP_CD',
        'association_interests': 'ASSOC_INT',
        'other_entity_interests': 'OTHER_INT',
        'employer': 'EMPLOYER',
        'occupation': 'OCCUPATION'
    }

    DIRECT_MAP = {
        'amendment_id': 'AMEND_ID',
        'committee_id': 'CMTE_ID',
        'description': 'CTRIB_DSCR',
        'entity_code': 'ENTITY_CD',
        'form_type': 'FORM_TYPE',
        'int_rate': 'INT_RATE',
        'line_item': 'LINE_ITEM',
        'parent_transaction_id': 'BAKREF_TID',
        'record_type': 'REC_TYPE',
        'support_or_oppose_code': 'SUP_OPP_CD'
    }

    CVR_CHECKBOX_MAP = {
        'committee_report_attached': 'RPT_ATT_CB',
        'employer_info_included': 'EMPLBUS_CB',
        'business_activity_included': 'BUSACT_CB',
        'association_interests_included': 'ASSOC_CB',
        'other_interests_included': 'OTHER_CB',
        'self_employed': 'SELFEMP_CB'
    }

    CVR_YES_NO_MAP = {
        'controlled_committee': 'CONTROL_YN',
        'sponsored_committee': 'SPONSOR_YN',
        'primarily_formed_committee': 'PRIMFRM_YN',
        'broad_base_committee': 'BRDBASE_YN'
    }

    CVR_COMMITTEE_MAP = {'type': 'CMTTE_TYPE', 'id': 'CMTTE_ID'}

    CVR_DATE_MAP = {
        'filed_date': 'RPT_DATE',
        'period_begin': 'FROM_DATE',
        'period_end': 'THRU_DATE',
        'election_date': 'ELECT_DATE',
        'attached_statement_period_begin': 'RPTFROMDT',
        'attached_statement_period_end': 'RPTTHRUDT'
    }

    DATE_MAP = {'date_received': 'RCPT_DATE', 'end_date': 'DATE_THRU'}

    CVR_OFFICE_MAP = {
        'code': 'OFFICE_CD',
        'description': 'OFFIC_DSCR',
        'district_number': 'DIST_NO',
        'jurisdiction_code': 'JURIS_CD',
        'jurisdiction_description': 'JURIS_DSCR',
        'sought_or_held': 'OFF_S_H_CD'
    }

    OFFICE_MAP = {
        'code': 'OFFICE_CD',
        'description': 'OFFIC_DSCR',
        'district_number': 'DIST_NO',
        'jurisdiction_code': 'JURIS_CD',
        'jurisdiction_description': 'JURIS_DSCR',
        'sought_or_held': 'OFF_S_H_CD'
    }

    CVR_BALLOT_MAP = {
        'jurisdiction': 'BAL_JURIS',
        'name': 'BAL_NAME',
        'number_or_letter': 'BAL_NUM',
        'id': 'BAL_ID'
    }

    BALLOT_MAP = {
        'jurisdiction': 'BAL_JURIS',
        'name': 'BAL_NAME',
        'number_or_letter': 'BAL_NUM'
    }

    CVR_ENTITY_PREFIXES = {
        'filer': 'FILER',
        'filer_mailing': 'MAIL',
        'treasurer': 'TRES',
        'business': 'BUS',
        'candidate': 'CAND'
    }

    TRANSACTION_MAP = {'id': 'TRAN_ID', 'type': 'TRAN_TYPE'}

    AMOUNT_MAP = {
        'received': 'AMOUNT',
        'cumulative_year_to_date': 'CUM_YTD',
        'cumulative_other': 'CUM_OTH'
    }

    MEMO_MAP = {'code': 'MEMO_CODE', 'reference': 'MEMO_REFNO'}

    RELATED_ITEM_MAP = {
        'has_same_transaction_id': 'XREF_MATCH',
        'included_on_sched_b2_or_f': 'XREF_SCHNM'
    }

    def incorporate_item(item, filings):
        '''Inserts original item data into the simplified filings map.

        Args:
          item: The (corrected) original filing to incorporate.
          filings: A map from filings IDs to simplified receipt filings.
        '''
        filing_id = item['FILING_ID']
        if filing_id not in filings:
            filings[filing_id] = {
                'filing_id': filing_id,
                'covers': [],
                'campaign_contributions': [],
                'pre_election_expenditures': [],
                'unmatched_memos': []
            }

        piece = {}

        for key, value in DIRECT_MAP.items():
            if value in item and item[value]:
                piece[key] = util.postgres.sanitize_entry(item[value])

        transaction = {}
        for key, value in TRANSACTION_MAP.items():
            if value in item and item[value]:
                transaction[key] = util.postgres.sanitize_entry(item[value])
        if len(transaction.keys()):
            piece['transaction'] = transaction

        for key, value in DATE_MAP.items():
            if value in item and item[value]:
                piece[key] = standardize_timestamp(
                    item[value], date_format=DATE_FORMAT_MONTH_DAY_YEAR)

        office = {}
        for key, value in OFFICE_MAP.items():
            if value in item and item[value]:
                office[key] = util.postgres.sanitize_entry(item[value])
        if len(office.keys()):
            piece['office'] = office

        ballot = {}
        for key, value in BALLOT_MAP.items():
            if value in item and item[value]:
                ballot[key] = util.postgres.sanitize_entry(item[value])
        if len(ballot.keys()):
            piece['ballot_measure'] = ballot

        amount = {}
        for key, value in AMOUNT_MAP.items():
            if value in item and item[value]:
                amount[key] = item[value]
        if len(amount.keys()):
            piece['amount'] = amount

        memo = {}
        for key, value in MEMO_MAP.items():
            if value in item and item[value]:
                memo[key] = util.postgres.sanitize_entry(item[value])
        if len(memo.keys()):
            piece['memo'] = memo

        related_item = {}
        for key, value in RELATED_ITEM_MAP.items():
            if value in item and item[value]:
                related_item[key] = util.postgres.sanitize_entry(item[value])
        if len(related_item.keys()):
            piece['related_item'] = related_item

        # Translate the entities.
        for key, value in ENTITY_PREFIXES.items():
            translate_entity(piece, value, key, item)

        # This field has no documentation.
        if item['INTR_CMTEID']:
            if 'intermediary' not in piece:
                piece['intermediary'] = {}
            piece['intermediary'][
                'committee_id'] = util.postgres.sanitize_entry(
                    item['INTR_CMTEID'])

        filings[filing_id]['campaign_contributions'].append(piece)

    def incorporate_covers(filings, covers, filing_id):
        '''Adds cover information into simplified JSON rep of a receipts filing.

        Args:
          filings: A map from filing IDs to receipts filings.
          covers: A map from filing IDs to cover JSON.
          filing_id: The ID of the filing to update.
        '''
        if filing_id not in filings or filing_id not in covers:
            return
        filing = filings[filing_id]
        covers_simp = []
        for cover in covers[filing_id]:
            cover_simp = {}

            for key, value in CVR_DIRECT_MAP.items():
                if value in cover and cover[value]:
                    cover_simp[key] = util.postgres.sanitize_entry(
                        cover[value])

            amendment_exp = ""
            for value in ['AMENDEXP_1', 'AMENDEXP_2', 'AMENDEXP_3']:
                if value in cover and cover[value]:
                    if amendment_exp:
                        amendment_exp += '; '
                    amendment_exp += cover[value]
            if amendment_exp:
                cover_simp['amendment_explanation'] = amendment_exp

            checkboxes = {}
            for key, value in CVR_CHECKBOX_MAP.items():
                if value in cover and cover[value]:
                    checkboxes[key] = cover[value]
            if len(checkboxes.keys()):
                cover_simp['checkboxes'] = checkboxes

            yes_nos = {}
            for key, value in CVR_YES_NO_MAP.items():
                if value in cover and cover[value]:
                    yes_nos[key] = cover[value]
            if len(yes_nos.keys()):
                cover_simp['yes_nos'] = yes_nos

            committee = {}
            for key, value in CVR_COMMITTEE_MAP.items():
                if value in cover and cover[value]:
                    committee[key] = util.postgres.sanitize_entry(cover[value])
            if len(committee.keys()):
                cover_simp['committee'] = committee

            office = {}
            for key, value in CVR_OFFICE_MAP.items():
                if value in cover and cover[value]:
                    office[key] = util.postgres.sanitize_entry(cover[value])
            if len(office.keys()):
                cover_simp['office'] = office

            ballot = {}
            for key, value in CVR_BALLOT_MAP.items():
                if value in cover and cover[value]:
                    ballot[key] = util.postgres.sanitize_entry(cover[value])
            if len(ballot.keys()):
                cover_simp['ballot_measure'] = ballot

            for key, value in CVR_DATE_MAP.items():
                if value in cover and cover[value]:
                    cover_simp[key] = standardize_timestamp(
                        cover[value], date_format=DATE_FORMAT_MONTH_DAY_YEAR)

            for key, value in CVR_ENTITY_PREFIXES.items():
                translate_entity(cover_simp, value, key, cover)
            covers_simp.append(cover_simp)
        filing['covers'] = covers_simp
        filings[filing_id] = filing
        del covers[filing_id]

    def incorporate_s496s(filings, s496s, filing_id):
        '''Adds in S496 information for a particular filing ID.'''
        if filing_id not in filings or filing_id not in s496s:
            return
        filing = filings[filing_id]
        for s496 in s496s[filing_id]:
            amend_id = str(s496['AMEND_ID'])
            line_item = str(s496['LINE_ITEM'])
            transaction_id = s496['TRAN_ID']

            expenditure = {
                'amendment_id': amend_id,
                'line_item': line_item,
                'transaction': {
                    'id': transaction_id
                }
            }

            if s496['EXP_DATE']:
                expenditure['date_received'] = standardize_timestamp(
                    s496['EXP_DATE'], date_format=DATE_FORMAT_MONTH_DAY_YEAR)

            if s496['AMOUNT']:
                expenditure['amount'] = s496['AMOUNT']

            if s496['EXPN_DSCR']:
                expenditure['description'] = util.postgres.sanitize_entry(
                    s496['EXPN_DSCR'])

            memo = {}
            if s496['MEMO_CODE']:
                memo['code'] = util.postgres.sanitize_entry(s496['MEMO_CODE'])
            if s496['MEMO_REFNO']:
                memo['reference'] = util.postgres.sanitize_entry(
                    s496['MEMO_REFNO'])
            if len(memo.keys()):
                expenditure['memo'] = memo

            filing['pre_election_expenditures'].append(expenditure)

    def incorporate_memos(filings, memos, filing_id):
        '''Adds memos into a filing given a filing ID'''
        if not filing_id in filings:
            return
        filings[filing_id]['unmatched_memos'] = []
        if not filing_id in memos:
            return
        filing = filings[filing_id]
        for memo in memos[filing_id]:
            incorporate_memo(filing, memo)
        filings[filing_id] = filing

    def generate_entities_list(filing, cover_entity_prefixes,
                               payment_entity_prefixes):
        '''Fills a filing with the list of mentioned entities.'''

        def incorporate_section(section, entity_prefixes, entities_set):
            if section not in filing:
                return
            piece = filing[section]
            for key in entity_prefixes:
                if key not in piece:
                    continue
                value = piece[key]
                name = canonicalize(build_name(value))
                employer = ''
                if 'employer' in value and value['employer']:
                    employer = canonicalize(value['employer'])

                if name and employer:
                    # Add the name coupled with its employer.
                    entities_set.add(
                        json.dumps({
                            'name': name,
                            'employer': employer
                        }))
                    # Add the employer itself.
                    entities_set.add(json.dumps({'name': employer}))
                elif name:
                    entities_set.add(json.dumps({'name': name}))
                elif employer:
                    entities_set.add(json.dumps({'name': employer}))

        entities_set = set()
        incorporate_section('cover', cover_entity_prefixes, entities_set)
        incorporate_section('campaign_contribution', payment_entity_prefixes,
                            entities_set)
        incorporate_section('pre_election_expenditure',
                            payment_entity_prefixes, entities_set)

        filing['entities'] = [json.loads(x) for x in sorted(entities_set)]

    def write_filing(filings, covers, s496s, memos, filing_id, cover_writer,
                     contrib_writer, expend_writer):
        incorporate_covers(filings, covers, filing_id)
        incorporate_s496s(filings, s496s, filing_id)
        incorporate_memos(filings, memos, filing_id)
        filing = filings[filing_id]
        filing = sorted_filing(filing)
        filing = truncate_filing_to_most_recent(filing)

        # Write the cover.
        cover = filing['covers'][-1] if len(filing['covers']) else {}
        chunk = {
            'filing_id': filing['filing_id'],
            'cover': cover,
            'unmatched_memos': filing['unmatched_memos'],
            'entities': []
        }
        generate_entities_list(chunk, CVR_ENTITY_PREFIXES, [])
        cover_writer.writerow({
            'filing_id':
            chunk['filing_id'],
            'cover':
            json.dumps(chunk['cover']),
            'unmatched_memos':
            json.dumps(chunk['unmatched_memos']),
            'entities':
            json.dumps(chunk['entities'])
        })

        # Write the campaign contributions
        for payment in filing['campaign_contributions']:
            chunk = {
                'filing_id': filing['filing_id'],
                'transaction_id': payment['transaction']['id'],
                'line_item': payment['line_item'],
                'campaign_contribution': payment,
                'entities': []
            }
            generate_entities_list(chunk, [], ENTITY_PREFIXES)
            contrib_writer.writerow({
                'filing_id':
                chunk['filing_id'],
                'transaction_id':
                chunk['transaction_id'],
                'line_item':
                chunk['line_item'],
                'campaign_contribution':
                json.dumps(chunk['campaign_contribution']),
                'entities':
                json.dumps(chunk['entities'])
            })

        # Write the pre-election expenditures
        for payment in filing['pre_election_expenditures']:
            chunk = {
                'filing_id': filing['filing_id'],
                'transaction_id': payment['transaction']['id'],
                'line_item': payment['line_item'],
                'pre_election_expenditure': payment,
                'entities': []
            }
            generate_entities_list(chunk, [], ENTITY_PREFIXES)
            expend_writer.writerow({
                'filing_id':
                chunk['filing_id'],
                'transaction_id':
                chunk['transaction_id'],
                'line_item':
                chunk['line_item'],
                'pre_election_expenditure':
                json.dumps(chunk['pre_election_expenditure']),
                'entities':
                json.dumps(chunk['entities'])
            })

    # Load the S496 info into a dictionary keyed by filing ID.
    s496s = {}
    for item in json.load(open(s496_json)):
        # The original data might have been an integer instead of a string.
        filing_id = str(item['FILING_ID'])
        if filing_id not in s496s:
            s496s[filing_id] = []
        s496s[filing_id].append(item)

    # Load the memo notes into a dictionary keyed by filing ID.
    memos = {}
    for item in json.load(open(memo_json)):
        # The original data might have been an integer instead of a string.
        filing_id = str(item['FILING_ID'])
        if filing_id not in memos:
            memos[filing_id] = []
        memos[filing_id].append(item)

    # Load the cover information into a dict keyed by filing ID and inject on
    # each write.
    covers = {}
    for item in json.load(open(receipts_cover_json)):
        # The original data might have been an integer instead of a string.
        filing_id = str(item['FILING_ID'])
        if filing_id not in covers:
            covers[filing_id] = []
        covers[filing_id].append(item)

    # Count the number of pieces per filing
    num_pieces = {}
    first_item = True
    item = {}
    for prefix, the_type, value in ijson.parse(open(receipts_json)):
        prefix_components = prefix.split('.')
        if len(prefix_components) != 2:
            continue
        _, item_key = prefix_components
        # We assume the first key of each item is the FILING_ID.
        if item_key == 'FILING_ID':
            if not first_item:
                filing_id = item['FILING_ID']
                if filing_id not in num_pieces:
                    num_pieces[filing_id] = 0
                num_pieces[filing_id] += 1
            first_item = False
            item = {}
        item[item_key] = value
    filing_id = item['FILING_ID']
    if filing_id not in num_pieces:
        num_pieces[filing_id] = 0
    num_pieces[filing_id] += 1

    # Write out the filings when they reach their count.
    piece_offsets = {}
    filings = {}
    first_item = True
    item = {}
    with open(output_cover_csv, 'w', newline='') as out_cover_csv, open(
            output_campaign_contrib_csv, 'w',
            newline='') as out_campaign_contrib_csv, open(
                output_pre_elect_expend_csv, 'w',
                newline='') as out_pre_elect_expend_csv:
        cover_writer = csv.DictWriter(
            out_cover_csv,
            fieldnames=['filing_id', 'cover', 'unmatched_memos', 'entities'],
            delimiter='\t',
            quoting=csv.QUOTE_ALL,
            doublequote=False,
            escapechar='\\')
        cover_writer.writeheader()
        contrib_writer = csv.DictWriter(out_campaign_contrib_csv,
                                        fieldnames=[
                                            'filing_id', 'transaction_id',
                                            'line_item',
                                            'campaign_contribution', 'entities'
                                        ],
                                        delimiter='\t',
                                        quoting=csv.QUOTE_ALL,
                                        doublequote=False,
                                        escapechar='\\')
        contrib_writer.writeheader()
        expend_writer = csv.DictWriter(out_pre_elect_expend_csv,
                                       fieldnames=[
                                           'filing_id', 'transaction_id',
                                           'line_item',
                                           'pre_election_expenditure',
                                           'entities'
                                       ],
                                       delimiter='\t',
                                       quoting=csv.QUOTE_ALL,
                                       doublequote=False,
                                       escapechar='\\')
        expend_writer.writeheader()
        for prefix, the_type, value in ijson.parse(open(receipts_json)):
            prefix_components = prefix.split('.')
            if len(prefix_components) != 2:
                continue
            _, item_key = prefix_components
            if item_key == 'FILING_ID':
                if not first_item:
                    filing_id = item['FILING_ID']
                    incorporate_item(item, filings)
                    if filing_id not in piece_offsets:
                        piece_offsets[filing_id] = 0
                    piece_offsets[filing_id] += 1
                    if piece_offsets[filing_id] == num_pieces[filing_id]:
                        write_filing(filings, covers, s496s, memos, filing_id,
                                     cover_writer, contrib_writer,
                                     expend_writer)
                        del filings[filing_id]
                first_item = False
                item = {}
            item[item_key] = value

        filing_id = item['FILING_ID']
        incorporate_item(item, filings)
        if filing_id not in piece_offsets:
            piece_offsets[filing_id] = 0
        piece_offsets[filing_id] += 1
        if piece_offsets[filing_id] == num_pieces[filing_id]:
            write_filing(filings, covers, s496s, memos, filing_id,
                         cover_writer, contrib_writer, expend_writer)
            del filings[filing_id]

        if len(filings.keys()):
            print('There were leftover filing ids: {}'.format(filings.keys()))


def retrieve(
        correction_map=CORRECTION_MAP,
        simplified_lobbying_disclosure_cover_json=SIMPLIFIED_LOBBY_DISCLOSURE_COVER_JSON,
        simplified_lobbying_disclosure_cover_csv=SIMPLIFIED_LOBBY_DISCLOSURE_COVER_CSV,
        simplified_lobbying_disclosure_payment_json=SIMPLIFIED_LOBBY_DISCLOSURE_PAYMENT_JSON,
        simplified_lobbying_disclosure_payment_csv=SIMPLIFIED_LOBBY_DISCLOSURE_PAYMENT_CSV,
        simplified_campaign_disclosure_cover_csv=SIMPLIFIED_CAMPAIGN_DISCLOSURE_COVER_CSV,
        simplified_campaign_contribution_csv=SIMPLIFIED_CAMPAIGN_CONTRIBUTION_CSV,
        simplified_pre_election_expenditure_csv=SIMPLIFIED_PRE_ELECTION_EXPENDITURE_CSV,
        calaccess_orig_data_dir=CALACCESS_ORIG_DATA_DIR,
        download_dir=DOWNLOAD_DIR,
        skip_download=False,
        headless=True,
        verbose=False):
    '''Download the CAL-ACCESS data and prep it for loading into Pandas.'''
    FILES_TO_EXTRACT = [
        # Cover page of campaign disclosure forms.
        'CalAccess/DATA/CVR_CAMPAIGN_DISCLOSURE_CD.TSV',
        # Verification information from campaign-disclosure forms.
        'CalAccess/DATA/CVR3_VERIFICATION_INFO_CD.TSV',
        # Cover page information for lobbying disclosure forms.
        'CalAccess/DATA/CVR_LOBBY_DISCLOSURE_CD.TSV',
        # Cover page extra information for lobbying disclosure forms.
        'CalAccess/DATA/CVR2_LOBBY_DISCLOSURE_CD.TSV',
        # A link between filers and their filings.
        'CalAccess/DATA/FILER_FILINGS_CD.TSV',
        # A map between filer IDs and names.
        'CalAccess/DATA/FILERNAME_CD.TSV',
        # Payments involving lobbying firms reported on Form 625 P2 or 635 P3B.
        'CalAccess/DATA/LPAY_CD.TSV',
        # Individual political contribution receipts.
        'CalAccess/DATA/RCPT_CD.TSV',
        # Payments and other disclosures made by slate-mailer organizations.
        'CalAccess/DATA/S401_CD.TSV',
        # Itemized independent expenditures in the 90 days before the election.
        'CalAccess/DATA/S496_CD.TSV',
        # Summary totals from filings.
        'CalAccess/DATA/SMRY_CD.TSV',
        # Text memos attached to electronic filings.
        'CalAccess/DATA/TEXT_MEMO_CD.TSV'
    ]

    correction_keys = [
        'CVR_CAMPAIGN_DISCLOSURE', 'CVR_LOBBY_DISCLOSURE',
        'CVR2_LOBBY_DISCLOSURE', 'LPAY', 'TEXT_MEMO', 'RCPT', 'S496'
    ]

    files = {}
    for orig_name in correction_keys:
        new_name = correction_map[orig_name]
        files[orig_name] = {
            'orig': '{}/{}_CD.TSV'.format(calaccess_orig_data_dir, orig_name),
            'json': '{}/{}.json'.format(download_dir, new_name),
            'csv': '{}/{}.csv'.format(download_dir, new_name)
        }

    download_zip = '{}/dbwebexport.zip'.format(DOWNLOAD_DIR)
    if os.path.exists(download_zip) and not skip_download:
        print('WARNING: Deleting likely stale {}'.format(download_zip))
        os.remove(download_zip)
    if not skip_download or not os.path.exists(download_zip):
        util.zip.download_and_unpack(ZIP_URL,
                                     DOWNLOAD_DIR,
                                     FILES_TO_EXTRACT,
                                     use_selenium=True,
                                     headless=headless,
                                     download_dir=DOWNLOAD_DIR,
                                     verbose=verbose)

    # Process the lobbying disclosure information
    patch_csv.patch(orig_csv=files['CVR_LOBBY_DISCLOSURE']['orig'],
                    output_csv=files['CVR_LOBBY_DISCLOSURE']['csv'],
                    output_json=files['CVR_LOBBY_DISCLOSURE']['json'],
                    patch_components=patch_lobbying_cover_components,
                    sep='\t',
                    assume_never_quoted=True)
    patch_csv.patch(orig_csv=files['CVR2_LOBBY_DISCLOSURE']['orig'],
                    output_csv=files['CVR2_LOBBY_DISCLOSURE']['csv'],
                    output_json=files['CVR2_LOBBY_DISCLOSURE']['json'],
                    patch_components=None,
                    sep='\t',
                    assume_never_quoted=True)
    patch_csv.patch(orig_csv=files['LPAY']['orig'],
                    output_csv=files['LPAY']['csv'],
                    output_json=files['LPAY']['json'],
                    patch_components=patch_lobbying_payments_components,
                    sep='\t',
                    assume_never_quoted=True)
    patch_csv.patch(orig_csv=files['TEXT_MEMO']['orig'],
                    output_csv=files['TEXT_MEMO']['csv'],
                    output_json=files['TEXT_MEMO']['json'],
                    patch_components=patch_text_memo_components,
                    sep='\t',
                    assume_never_quoted=True)
    simplify_lobbying_disclosures(files['CVR_LOBBY_DISCLOSURE']['json'],
                                  files['CVR2_LOBBY_DISCLOSURE']['json'],
                                  files['LPAY']['json'],
                                  files['TEXT_MEMO']['json'],
                                  simplified_lobbying_disclosure_cover_json,
                                  simplified_lobbying_disclosure_cover_csv,
                                  simplified_lobbying_disclosure_payment_json,
                                  simplified_lobbying_disclosure_payment_csv)

    # Process the contribution receipts.
    patch_csv.patch(orig_csv=files['S496']['orig'],
                    output_csv=files['S496']['csv'],
                    output_json=files['S496']['json'],
                    patch_components=patch_s496_components,
                    sep='\t',
                    assume_never_quoted=True)
    patch_csv.patch(orig_csv=files['RCPT']['orig'],
                    output_csv=files['RCPT']['csv'],
                    output_json=files['RCPT']['json'],
                    patch_components=patch_receipt_components,
                    sep='\t',
                    assume_never_quoted=True)
    patch_csv.patch(orig_csv=files['CVR_CAMPAIGN_DISCLOSURE']['orig'],
                    output_csv=files['CVR_CAMPAIGN_DISCLOSURE']['csv'],
                    output_json=files['CVR_CAMPAIGN_DISCLOSURE']['json'],
                    patch_components=patch_receipts_cover_components,
                    sep='\t',
                    assume_never_quoted=True)
    simplify_campaign_disclosures(files['S496']['json'], files['RCPT']['json'],
                                  files['CVR_CAMPAIGN_DISCLOSURE']['json'],
                                  files['TEXT_MEMO']['json'],
                                  simplified_campaign_disclosure_cover_csv,
                                  simplified_campaign_contribution_csv,
                                  simplified_pre_election_expenditure_csv)


def postgres_create_lobbying_disclosure_covers(user=None,
                                               password=None,
                                               dbname=None,
                                               port=None,
                                               host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            LOBBY_DISC_COVER_TABLE_NAME,
            LOBBY_DISC_COVER_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (filing_id)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(filing_id),             '') || ' ' ||
    coalesce(lower(cover::text),           '') || ' ' ||
    coalesce(lower(unmatched_memos::text), '')
  )
);
    '''.format(LOBBY_DISC_COVER_TABLE_NAME, LOBBY_DISC_COVER_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_entities_index ON {} USING GIN (
  (entities) jsonb_path_ops
);
    '''.format(LOBBY_DISC_COVER_TABLE_NAME, LOBBY_DISC_COVER_TABLE_NAME))


def postgres_update_lobbying_disclosure_covers(
        export_csv=SIMPLIFIED_LOBBY_DISCLOSURE_COVER_CSV,
        user=None,
        password=None,
        dbname=None,
        port=None,
        host=None,
        vacuum=True):
    postgres_create_lobbying_disclosure_covers(user, password, dbname, port,
                                               host)
    util.postgres.update(LOBBY_DISC_COVER_TABLE_NAME,
                         LOBBY_DISC_COVER_TABLE_TYPES,
                         LOBBY_DISC_COVER_UNIQUE_COLUMNS,
                         LOBBY_DISC_COVER_FORCE_NULL_COLUMNS,
                         LOBBY_DISC_COVER_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_create_lobbying_disclosure_payments(user=None,
                                                 password=None,
                                                 dbname=None,
                                                 port=None,
                                                 host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            LOBBY_DISC_PAYMENT_TABLE_NAME,
            LOBBY_DISC_PAYMENT_TABLE_TYPES,
            serial_id=True,
            constraints={
                'unique': 'UNIQUE (filing_id, transaction_id, line_item)'
            }))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(filing_id),      '') || ' ' ||
    coalesce(lower(transaction_id), '') || ' ' ||
    coalesce(lower(payment::text),  '')
  )
);
    '''.format(LOBBY_DISC_PAYMENT_TABLE_NAME, LOBBY_DISC_PAYMENT_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_entities_index ON {} USING GIN (
  (entities) jsonb_path_ops
);
    '''.format(LOBBY_DISC_PAYMENT_TABLE_NAME, LOBBY_DISC_PAYMENT_TABLE_NAME))


def postgres_update_lobbying_disclosure_payments(
        export_csv=SIMPLIFIED_LOBBY_DISCLOSURE_PAYMENT_CSV,
        user=None,
        password=None,
        dbname=None,
        port=None,
        host=None,
        vacuum=True):
    postgres_create_lobbying_disclosure_payments(user, password, dbname, port,
                                                 host)
    util.postgres.update(LOBBY_DISC_PAYMENT_TABLE_NAME,
                         LOBBY_DISC_PAYMENT_TABLE_TYPES,
                         LOBBY_DISC_PAYMENT_UNIQUE_COLUMNS,
                         LOBBY_DISC_PAYMENT_FORCE_NULL_COLUMNS,
                         LOBBY_DISC_PAYMENT_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_create_campaign_disclosure_covers(user=None,
                                               password=None,
                                               dbname=None,
                                               port=None,
                                               host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            CAMPAIGN_DISC_COVER_TABLE_NAME,
            CAMPAIGN_DISC_COVER_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (filing_id)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(filing_id),             '') || ' ' ||
    coalesce(lower(cover::text),           '') || ' ' ||
    coalesce(lower(unmatched_memos::text), '')
  )
);
    '''.format(CAMPAIGN_DISC_COVER_TABLE_NAME, CAMPAIGN_DISC_COVER_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_entities_index ON {} USING GIN (
  (entities) jsonb_path_ops
);
    '''.format(CAMPAIGN_DISC_COVER_TABLE_NAME, CAMPAIGN_DISC_COVER_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_filing_id_index ON {} (filing_id);
    '''.format(CAMPAIGN_DISC_COVER_TABLE_NAME, CAMPAIGN_DISC_COVER_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_filed_date_index ON {} (
  ((cover ->> 'filed_date'))
);
    '''.format(CAMPAIGN_DISC_COVER_TABLE_NAME, CAMPAIGN_DISC_COVER_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_period_begin_index ON {} (
  ((cover ->> 'period_begin'))
);
    '''.format(CAMPAIGN_DISC_COVER_TABLE_NAME, CAMPAIGN_DISC_COVER_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_period_end_index ON {} (
  ((cover ->> 'period_end'))
);
    '''.format(CAMPAIGN_DISC_COVER_TABLE_NAME, CAMPAIGN_DISC_COVER_TABLE_NAME))
    cursor.execute('''
CREATE EXTENSION IF NOT EXISTS btree_gin;
CREATE INDEX IF NOT EXISTS {}_ext_index ON {} USING GIN (
  (entities) jsonb_path_ops,
  filing_id,
  ((cover ->> 'filed_date')),
  ((cover ->> 'period_begin')),
  ((cover ->> 'period_end'))
);
    '''.format(CAMPAIGN_DISC_COVER_TABLE_NAME, CAMPAIGN_DISC_COVER_TABLE_NAME))


def postgres_update_campaign_disclosure_covers(
        export_csv=SIMPLIFIED_CAMPAIGN_DISCLOSURE_COVER_CSV,
        user=None,
        password=None,
        dbname=None,
        port=None,
        host=None,
        vacuum=True,
        num_chunks=1):
    postgres_create_campaign_disclosure_covers(user, password, dbname, port,
                                               host)
    util.postgres.update(CAMPAIGN_DISC_COVER_TABLE_NAME,
                         CAMPAIGN_DISC_COVER_TABLE_TYPES,
                         CAMPAIGN_DISC_COVER_UNIQUE_COLUMNS,
                         CAMPAIGN_DISC_COVER_FORCE_NULL_COLUMNS,
                         CAMPAIGN_DISC_COVER_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum,
                         num_chunks=num_chunks)


def postgres_create_campaign_contributions(user=None,
                                           password=None,
                                           dbname=None,
                                           port=None,
                                           host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            CAMPAIGN_CONTRIB_TABLE_NAME,
            CAMPAIGN_CONTRIB_TABLE_TYPES,
            serial_id=True,
            constraints={
                'unique': 'UNIQUE (filing_id, transaction_id, line_item)'
            }))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(filing_id),                   '') || ' ' ||
    coalesce(lower(transaction_id),              '') || ' ' ||
    coalesce(lower(campaign_contribution::text), '')
  )
);
    '''.format(CAMPAIGN_CONTRIB_TABLE_NAME, CAMPAIGN_CONTRIB_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_index ON {} USING GIN ((entities) jsonb_path_ops);
    '''.format(CAMPAIGN_CONTRIB_TABLE_NAME, CAMPAIGN_CONTRIB_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_filing_id_index ON {} (filing_id);
    '''.format(CAMPAIGN_CONTRIB_TABLE_NAME, CAMPAIGN_CONTRIB_TABLE_NAME))
    cursor.execute('''
CREATE EXTENSION IF NOT EXISTS btree_gin;
CREATE INDEX IF NOT EXISTS {}_ext_index ON {} USING GIN (
  (entities) jsonb_path_ops, filing_id
);
    '''.format(CAMPAIGN_CONTRIB_TABLE_NAME, CAMPAIGN_CONTRIB_TABLE_NAME))


def postgres_update_campaign_contributions(
        export_csv=SIMPLIFIED_CAMPAIGN_CONTRIBUTION_CSV,
        user=None,
        password=None,
        dbname=None,
        port=None,
        host=None,
        vacuum=True,
        num_chunks=10,
        start_chunk=None,
        end_chunk=None):
    postgres_create_campaign_contributions(user, password, dbname, port, host)
    util.postgres.update(CAMPAIGN_CONTRIB_TABLE_NAME,
                         CAMPAIGN_CONTRIB_TABLE_TYPES,
                         CAMPAIGN_CONTRIB_UNIQUE_COLUMNS,
                         CAMPAIGN_CONTRIB_FORCE_NULL_COLUMNS,
                         CAMPAIGN_CONTRIB_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum,
                         num_chunks=num_chunks,
                         start_chunk=start_chunk,
                         end_chunk=end_chunk)


def postgres_create_pre_election_expenditures(user=None,
                                              password=None,
                                              dbname=None,
                                              port=None,
                                              host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            PRE_ELECT_EXPEND_TABLE_NAME,
            PRE_ELECT_EXPEND_TABLE_TYPES,
            serial_id=True,
            constraints={
                'unique': 'UNIQUE (filing_id, transaction_id, line_item)'
            }))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(filing_id),                      '') || ' ' ||
    coalesce(lower(transaction_id),                 '') || ' ' ||
    coalesce(lower(pre_election_expenditure::text), '')
  )
);
    '''.format(PRE_ELECT_EXPEND_TABLE_NAME, PRE_ELECT_EXPEND_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_entities_index ON {} USING GIN (
  (entities) jsonb_path_ops
);
    '''.format(PRE_ELECT_EXPEND_TABLE_NAME, PRE_ELECT_EXPEND_TABLE_NAME))


def postgres_update_pre_election_expenditures(
        export_csv=SIMPLIFIED_PRE_ELECTION_EXPENDITURE_CSV,
        user=None,
        password=None,
        dbname=None,
        port=None,
        host=None,
        vacuum=True,
        num_chunks=1):
    postgres_create_pre_election_expenditures(user, password, dbname, port,
                                              host)
    util.postgres.update(PRE_ELECT_EXPEND_TABLE_NAME,
                         PRE_ELECT_EXPEND_TABLE_TYPES,
                         PRE_ELECT_EXPEND_UNIQUE_COLUMNS,
                         PRE_ELECT_EXPEND_FORCE_NULL_COLUMNS,
                         PRE_ELECT_EXPEND_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum,
                         num_chunks=num_chunks)


def postgres_update(
        lobbying_disclosure_cover_csv=SIMPLIFIED_LOBBY_DISCLOSURE_COVER_CSV,
        lobbying_disclosure_payment_csv=SIMPLIFIED_LOBBY_DISCLOSURE_PAYMENT_CSV,
        campaign_disclosure_cover_csv=SIMPLIFIED_CAMPAIGN_DISCLOSURE_COVER_CSV,
        campaign_contribution_csv=SIMPLIFIED_CAMPAIGN_CONTRIBUTION_CSV,
        pre_election_expenditure_csv=SIMPLIFIED_PRE_ELECTION_EXPENDITURE_CSV,
        user=None,
        password=None,
        dbname=None,
        port=None,
        host=None,
        vacuum=True):
    '''Updates the corresponding Postgres databases with simplified CSV data.'''
    postgres_update_lobbying_disclosure_covers(lobbying_disclosure_cover_csv,
                                               user=user,
                                               password=password,
                                               dbname=dbname,
                                               port=port,
                                               host=host,
                                               vacuum=vacuum)
    postgres_update_lobbying_disclosure_payments(
        lobbying_disclosure_payment_csv,
        user=user,
        password=password,
        dbname=dbname,
        port=port,
        host=host,
        vacuum=vacuum)

    postgres_update_campaign_disclosure_covers(campaign_disclosure_cover_csv,
                                               user=user,
                                               password=password,
                                               dbname=dbname,
                                               port=port,
                                               host=host,
                                               vacuum=vacuum)
    postgres_update_campaign_contributions(campaign_contribution_csv,
                                           user=user,
                                           password=password,
                                           dbname=dbname,
                                           port=port,
                                           host=host,
                                           vacuum=vacuum)
    postgres_update_pre_election_expenditures(pre_election_expenditure_csv,
                                              user=user,
                                              password=password,
                                              dbname=dbname,
                                              port=port,
                                              host=host,
                                              vacuum=vacuum)
