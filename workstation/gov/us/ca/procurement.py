#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This utility scrapes California state procurement data from:
#
#  https://suppliers.fiscal.ca.gov/psc/psfpd1/SUPPLIER/ERP/c/ZZ_PO.ZZ_SCPRS1_CMP.GBL
#
# This is accomplished through automating form actions via Selenium.
#
import datetime
import glob
import json
import numpy as np
import os
import pandas as pd
import re
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import util.dates
import util.firefox
import util.postgres

from util.entities import canonicalize, normalize, normalization_map_helper

# The URL of the input form for the FL FACTS advanced search.
SEARCH_URL = 'https://suppliers.fiscal.ca.gov/psc/psfpd1/SUPPLIER/ERP/c/ZZ_PO.ZZ_SCPRS1_CMP.GBL'

# The maximum number of seconds to wait before timing out.
MAX_WAIT_SECONDS = 60

# The maximum number of seconds to monitor a download before failing.
MAX_DOWNLOAD_SECONDS = 1200

# The number of seconds to wait between each check if a download completed.
DOWNLOAD_RECHECK_SECONDS = 0.75

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
DOWNLOAD_DIR = os.path.join(DATA_DIR, 'us_ca_procurement')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

OUTPUT_CSV = '{}/us_ca_procurement.csv'.format(EXPORT_DIR)

if not os.path.exists(DOWNLOAD_DIR):
    os.makedirs(DOWNLOAD_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)

TABLE_NAME = 'us_ca_procurement'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

COLUMN_NAMES = {
    'Department': 'department',
    'Department Name': 'department_name',
    'Purchase Document #': 'document_id',
    'Associated POs': 'associated_purchase_orders',
    'First Item Title': 'first_item_title',
    'Start Date': 'start_date',
    'End Date': 'end_date',
    'Grand Total': 'grand_total',
    'Supplier ID': 'supplier_id',
    'Supplier Name': 'supplier_name',
    'Certification Type': 'certification_type',
    'Acquisition Type_ Sub-Type': 'acquisition_type',
    'Acquisition Method': 'acquisition_method',
    'LPA Contract ID': 'lpa_contract_id',
    'Buyer Name': 'buyer_name',
    'Buyer Email': 'buyer_email',
    'Status': 'status',
    'Version': 'version'
}

DATE_COLUMNS = ['start_date', 'end_date']

UNIQUE_COLUMNS = ['department', 'document_id']

FORCE_NULL_COLUMNS = ['department', 'start_date', 'end_date', 'grand_total']

TABLE_TYPES = {
    'department': 'INT',
    'department_name': 'VARCHAR(127)',
    'document_id': 'VARCHAR(63)',
    'associated_purchase_orders': 'TEXT',
    'first_item_title': 'VARCHAR(511)',
    'start_date': 'TIMESTAMP',
    'end_date': 'TIMESTAMP',
    'grand_total': 'FLOAT',
    'supplier_id': 'VARCHAR(63)',
    'supplier_name': 'VARCHAR(255)',
    'certification_type': 'VARCHAR(31)',
    'acquisition_type': 'VARCHAR(255)',
    'acquisition_method': 'VARCHAR(255)',
    'lpa_contract_id': 'VARCHAR(31)',
    'buyer_name': 'VARCHAR(63)',
    'buyer_email': 'VARCHAR(63)',
    'status': 'VARCHAR(15)',
    'version': 'VARCHAR(7)'
}


def download_csv(begin_date=None,
                 end_date=None,
                 headless=True,
                 max_wait_seconds=MAX_WAIT_SECONDS,
                 max_download_seconds=MAX_DOWNLOAD_SECONDS,
                 download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
                 download_dir=DOWNLOAD_DIR,
                 gecko_path=None,
                 quit_browser=False,
                 num_sleep_seconds=5,
                 verbose=False):
    if begin_date is None and end_date is None:
        print(
            'WARNING: Requesting more than a few months of data will prevent the download from being allowed.'
        )

    # Remove any old downloads.
    pattern = '{}/Summary_Information_*'.format(download_dir)
    stale_downloads = glob.glob(pattern)
    for filepath in stale_downloads:
        if verbose:
            print('Removing stale {}'.format(filepath))
        os.remove(filepath)

    browser = util.firefox.get_browser(headless=headless,
                                       download_dir=download_dir)

    # Fill in the date range in the Month/Day/Year format on the FL FACTS search
    # page then click the Search button.
    browser.get(SEARCH_URL)
    date_infos = [{
        'date': begin_date,
        'id': 'ZZ_SCPRS_SP_WRK_FROM_DATE'
    }, {
        'date': end_date,
        'id': 'ZZ_SCPRS_SP_WRK_TO_DATE'
    }]
    num_date_sleep_seconds = 1.
    for date_info in date_infos:
        if date_info['date'] is not None:
            year, month, day = util.dates.date_to_tuple(date_info['date'])
            date_keys = '{}/{}/{}'.format(month, day, year)
            try:
                element = browser.find_element(By.ID, date_info['id'])
                element.send_keys(date_keys + Keys.ENTER)
            except Exception as e:
                print('ERR: Could not find {}'.format(date_info['id']))
                with open('error.html', 'w') as outfile:
                    outfile.write(browser.page_source)
            if verbose:
                print('Found {} element.'.format(date_info['id']))
            time.sleep(num_date_sleep_seconds)

    search_button_id = 'ZZ_SCPRS_SP_WRK_BUTTON'
    num_sleep_seconds_before_search = 3.
    time.sleep(num_sleep_seconds_before_search)
    element = WebDriverWait(browser, max_wait_seconds).until(
        EC.presence_of_element_located((By.ID, search_button_id)))
    if element:
        if verbose:
            print('Found {}'.format(search_button_id))
        element.send_keys(Keys.ENTER)
    else:
        if verbose:
            print('WARNING: Did not find {}'.format(search_button_id))

    try:
        download_id = 'ZZ_SCPRS_SP_WRK_BUTTONS_GB'
        num_sleep_seconds_before_download = 3.
        time.sleep(num_sleep_seconds_before_download)
        element = WebDriverWait(browser, max_wait_seconds).until(
            EC.presence_of_element_located((By.ID, download_id)))
        if element:
            if verbose:
                print('Found {}'.format(download_id))
            element.send_keys(Keys.ENTER)
            ok_id = '#ICOK'
            element = WebDriverWait(browser, max_wait_seconds).until(
                EC.presence_of_element_located((By.ID, ok_id)))
            if element:
                if verbose:
                    print('Found {}'.format(ok_id))
                element.send_keys(Keys.ENTER)
        else:
            if verbose:
                print('WARNING: Did not find {}'.format(download_id))
    except Exception as e:
        print('Could not retrieve element.')
        print(e)

    completed = util.firefox.watch_download_glob(
        pattern,
        download_recheck_seconds=download_recheck_seconds,
        max_download_seconds=max_download_seconds,
        verbose=verbose)
    if verbose:
        if completed:
            print('Finished download of {}'.format(pattern))
        else:
            print('WARNING: Download of {} did not complete in time.'.format(
                pattern))

    # Due to a bug in which attempting to quit the browser results in the
    # program hanging, we have added this workaround.
    if quit_browser:
        browser.close()
        if verbose:
            print('Closed browser session')
        browser.quit()
        if verbose:
            print('Quit browser session')
    else:
        # We are attempting to ensure the download completes.
        print('Sleeping for {} seconds to help ensure download completes.'.
              format(num_sleep_seconds))
        time.sleep(num_sleep_seconds)


def export_df_to_csv(df, export_csv_name):
    df = util.postgres.preprocess_dataframe(df, unique_columns=UNIQUE_COLUMNS)
    util.postgres.df_to_csv(df, export_csv_name, columns=TABLE_TYPES.keys())


def process_csv(orig_xls, output_csv):
    df = pd.read_html(orig_xls)
    df = df[0].rename(columns=COLUMN_NAMES)

    for date_column in DATE_COLUMNS:
        df[date_column] = pd.to_datetime(df[date_column],
                                         format='%m/%d/%Y',
                                         errors='coerce')

    # Convert the 'grand_total' column from strings of the form '$123.45'
    # into floats.
    df.replace({'grand_total': r'\$*'}, value='', regex=True, inplace=True)
    df['grand_total'] = pd.to_numeric(df['grand_total'], errors='coerce')
    df['grand_total'] = df['grand_total'].astype(np.float32)

    df['supplier_name'] = df['supplier_name'].apply(util.entities.canonicalize)

    export_df_to_csv(df, output_csv)

    return df


def retrieve(begin_date=None,
             end_date=None,
             headless=True,
             window_days=31,
             output_csv=OUTPUT_CSV,
             max_wait_seconds=MAX_WAIT_SECONDS,
             max_download_seconds=MAX_DOWNLOAD_SECONDS,
             download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
             gecko_path=None,
             quit_browser=False,
             verbose=False):
    if begin_date is None and end_date is None:
        # Set the date range to the last 'window_days' days.
        today = datetime.date.today()
        begin_year, begin_month, begin_day = util.dates.earlier_day(
            today.year, today.month, today.day, window_days)
        begin_date = util.dates.tuple_to_date(begin_year, begin_month,
                                              begin_day)

    download_csv(begin_date=begin_date,
                 end_date=end_date,
                 headless=headless,
                 max_wait_seconds=max_wait_seconds,
                 max_download_seconds=max_download_seconds,
                 download_recheck_seconds=download_recheck_seconds,
                 download_dir=DOWNLOAD_DIR,
                 gecko_path=gecko_path,
                 quit_browser=quit_browser,
                 verbose=verbose)
    pattern = '{}/Summary_Information_*'.format(DOWNLOAD_DIR)
    orig_xls_name = glob.glob(pattern)[0]
    df = process_csv(orig_xls_name, output_csv)
    return df


def retrieve_first_half_of_month(
    year,
    month,
    headless=True,
    window_days=31,
    output_csv=OUTPUT_CSV,
    max_wait_seconds=MAX_WAIT_SECONDS,
    max_download_seconds=MAX_DOWNLOAD_SECONDS,
    download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
    gecko_path=None,
    quit_browser=False,
    verbose=False):
    begin_date = '{}-{}-01'.format(year, str(month).zfill(2))
    end_date = '{}-{}-16'.format(year, str(month).zfill(2))
    retrieve(begin_date,
             end_date,
             headless=headless,
             window_days=window_days,
             output_csv=output_csv,
             max_wait_seconds=max_wait_seconds,
             max_download_seconds=max_download_seconds,
             download_recheck_seconds=download_recheck_seconds,
             gecko_path=gecko_path,
             quit_browser=quit_browser,
             verbose=verbose)


def retrieve_second_half_of_month(
    year,
    month,
    headless=True,
    window_days=31,
    output_csv=OUTPUT_CSV,
    max_wait_seconds=MAX_WAIT_SECONDS,
    max_download_seconds=MAX_DOWNLOAD_SECONDS,
    download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
    gecko_path=None,
    quit_browser=False,
    verbose=False):
    begin_date = '{}-{}-15'.format(year, str(month).zfill(2))
    if month == 12:
        end_date = '{}-01-01'.format(year + 1)
    else:
        end_date = '{}-{}-01'.format(year, str(month + 1).zfill(2))
    retrieve(begin_date,
             end_date,
             headless=headless,
             window_days=window_days,
             output_csv=output_csv,
             max_wait_seconds=max_wait_seconds,
             max_download_seconds=max_download_seconds,
             download_recheck_seconds=download_recheck_seconds,
             gecko_path=gecko_path,
             quit_browser=quit_browser,
             verbose=verbose)


def retrieve_year(year,
                  headless=True,
                  window_days=31,
                  output_csv=OUTPUT_CSV,
                  max_wait_seconds=MAX_WAIT_SECONDS,
                  max_download_seconds=MAX_DOWNLOAD_SECONDS,
                  download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
                  gecko_path=None,
                  quit_browser=False,
                  verbose=False):
    for month in range(1, 13):
        if verbose:
            print('Processing {}-{}'.format(year, str(month).zfill(2)))
        retrieve_first_half_of_month(
            year,
            month,
            headless=headless,
            window_days=window_days,
            output_csv=output_csv,
            max_wait_seconds=max_wait_seconds,
            max_download_seconds=max_download_seconds,
            download_recheck_seconds=download_recheck_seconds,
            gecko_path=gecko_path,
            quit_browser=quit_browser,
            verbose=verbose)
        postgres_update(export_csv=output_csv)
        retrieve_second_half_of_month(
            year,
            month,
            headless=headless,
            window_days=window_days,
            output_csv=output_csv,
            max_wait_seconds=max_wait_seconds,
            max_download_seconds=max_download_seconds,
            download_recheck_seconds=download_recheck_seconds,
            gecko_path=gecko_path,
            quit_browser=quit_browser,
            verbose=verbose)
        postgres_update(export_csv=output_csv)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TABLE_NAME,
            TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE(department, document_id)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_vendor_index ON {} (supplier_name);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_agency_index ON {} (LOWER(department_name));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(department_name),            '') || ' ' ||
    coalesce(lower(document_id),                '') || ' ' ||
    coalesce(lower(associated_purchase_orders), '') || ' ' ||
    coalesce(lower(first_item_title),           '') || ' ' ||
    coalesce(supplier_name,                     '') || ' ' ||
    coalesce(lower(certification_type),         '') || ' ' ||
    coalesce(lower(acquisition_method),         '') || ' ' ||
    coalesce(lower(lpa_contract_id),            '')));
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=OUTPUT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=None):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['us', 'ca', 'procurement'], entities)
