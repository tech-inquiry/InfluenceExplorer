#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This tool is for exporting data from the Florida Accountability Tracking
# System (FACTS) for usage in a Postgres database.
#
# The recommended means of downloading data is via the CSV export made available
# after searching for a particular date range for contracts at:
#
#  https://facts.fldfs.com/Search/ContractAdvancedSearch.aspx
#
# Note that FACTS does not properly escape strings containing quotes in its
# CSV exports. For example, it has exported the CSV row:
#
#  "AGENCY FOR PERSONS WITH DISABILITIES","FIVE "S", INC.                 ","Purchase Order","","B55A6D","","SAPP'S SAW & MOWER CENTER      ","","2400.00","Multiple","Multiple","","","2400.00","BLKT-6732-FY19/20-AHINES-MMARTIN-MAINTENANCE/GROUNDS-SAPPS SAW & MOWER CENTER","Receiving","","","7/1/2019","6/30/2020","","","","7/1/2019","","","Purchase under $2,500 {Rule 60A-1.002(2), FAC]","","","","","","","","","","","","","","","","","","","","","","","","",""
#
# where the string
#
#  FIVE "S", INC.
#
# did not have its double-quotes escaped. The easiest solution is to manually
# escape said double-quotes through conveting them into double-quotes, i.e.,
#
#  FIVE ""S"", INC.
#
# Given a line number in Pandas where a problem occurred, e.g.,
#
#  pandas.errors.ParserError: Error tokenizing data. C error: Expected 52 fields in line 9834, saw 53
#
# one can add two to the reported line number to know roughly which row
# (zero-indexed) to manually correct in the original CSV (i.e., 9836).
#
# But we attempt to automatically correct such errors.
#
import datetime
import json
import numpy as np
import os
import pandas as pd
import re
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import util.dates
import util.firefox
import util.patch_csv
import util.postgres

from util.entities import canonicalize, normalize, normalization_map_helper

# The URL of the input form for the FL FACTS advanced search.
SEARCH_URL = 'https://facts.fldfs.com/Search/ContractAdvancedSearch.aspx'

# The maximum number of seconds to wait before timing out.
MAX_WAIT_SECONDS = 60

# The maximum number of seconds to monitor a download before failing.
MAX_DOWNLOAD_SECONDS = 600

# The number of seconds to wait between each check if a download completed.
DOWNLOAD_RECHECK_SECONDS = 0.25

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
DOWNLOAD_DIR = os.path.join(DATA_DIR, 'us_fl_facts')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

OUTPUT_CSV = '{}/us_fl_facts_filing.csv'.format(EXPORT_DIR)

# Note: This is for internal use only and is an attempt at automatically
# correcting some of the errors in the original CSVs.
CORRECTED_CSV = '{}/us_fl_facts_corrected.csv'.format(EXPORT_DIR)
CORRECTED_JSON = '{}/corrected.json'.format(DOWNLOAD_DIR)

FACTS_FLOATS = [
    'Original Contract Amount', 'Total Amount', 'PO Budget Amount',
    'Value of Capital Improvements'
]

FACTS_DATETIMES = [
    'Begin Date', 'Original End Date', 'New End Date',
    'Contract Execution Date', 'Grant Award Date', 'PO Order Date',
    'Business Case Date'
]

# We append any 'Vendor/Grantor Name Line 2' information to
# 'Vendor/Grantor Name'.
FACTS_SIMPLIFIED_NAMES = {
    'Agency Name': 'agency',
    'Vendor/Grantor Name': 'vendor',
    'Type': 'type',
    'Agency Contract ID': 'agency_contract_id',
    'PO Number': 'po_number',
    'Grant Award ID': 'grant_award_id',
    'Original Contract Amount': 'orig_contract_amount',
    'Total Amount': 'total_amount',
    'Recurring Budgetary Amount': 'recurring_budgetary_amount',
    'Non Recurring Budgetary Amount': 'non_recurring_budgetary_amount',
    'PO Budget Amount': 'po_budget_amount',
    'Commodity/Service Type Code': 'commodity_type_code',
    'Commodity/Service Type Description': 'commodity_type_description',
    'Short Title': 'short_title',
    'Long Title/PO Title': 'long_title',
    'Status': 'status',
    'FLAIR Contract ID': 'flair_contract_id',
    'Begin Date': 'begin_date',
    'Original End Date': 'original_end_date',
    'New End Date': 'new_end_date',
    'Contract Execution Date': 'contract_execution_date',
    'Grant Award Date': 'grant_award_date',
    'PO Order Date': 'po_order_date',
    'Agency Service Area': 'agency_service_area',
    'Authorized Advanced Payment': 'authorized_advanced_payment',
    'Method of Procurement': 'method_of_procurement',
    'State Term Contract ID': 'state_term_contract_id',
    'Agency Reference Number': 'agency_reference_number',
    'Contract Exemption Explanation': 'contract_exemption_explanation',
    'Statutory Authority': 'statutory_authority',
    'Recipient Type': 'recipient_type',
    'Contract Involves State or Federal Aid': 'involves_gov_aid',
    'Provide Administrative Cost': 'provide_admin_cost',
    'Administrative Cost Percentage': 'admin_cost_percent',
    'Provide for Periodic Increase': 'provide_periodic_increase',
    'Periodic Increase Percentage': 'periodic_increase_percent',
    'Business Case Study Done': 'business_case_study',
    'Business Case Date': 'business_case_date',
    'Legal Challenges to Procurement': 'legal_challenges',
    'Legal Challenge Description': 'legal_challenge_description',
    'Was the Contracted Functions Previously Done by the State':
    'previously_done_by_state',
    'Was the Contracted Functions Considered for Insourcing back to the State':
    'considered_for_insourcing',
    'Did the Vendor Make Capital Improvements on State Property':
    'improvement_on_state_property',
    'Capital Improvement Description': 'improvement_description',
    'Value of Capital Improvements': 'improvement_value',
    'Value of Unamortized Capital Improvements':
    'improvement_unamortized_value',
    'Comment': 'comment',
    'CFDA Code': 'cfda_code',
    'CFDA Description': 'cfda_description',
    'CSFA Code': 'csfa_code',
    'CSFA Description': 'csfa_description'
}

TABLE_TYPES = {
    'agency': 'VARCHAR(127)',
    'vendor': 'VARCHAR(127)',
    'type': 'VARCHAR(127)',
    'agency_contract_id': 'VARCHAR(63)',
    'po_number': 'VARCHAR(63)',
    'grant_award_id': 'VARCHAR(63)',
    'orig_contract_amount': 'FLOAT',
    'total_amount': 'FLOAT',
    'recurring_budgetary_amount': 'VARCHAR(63)',
    'non_recurring_budgetary_amount': 'VARCHAR(63)',
    'po_budget_amount': 'FLOAT',
    'commodity_type_code': 'VARCHAR(63)',
    'commodity_type_description': 'VARCHAR(63)',
    'short_title': 'VARCHAR(255)',
    'long_title': 'VARCHAR(255)',
    'status': 'VARCHAR(127)',
    'flair_contract_id': 'VARCHAR(63)',
    'begin_date': 'TIMESTAMP',
    'original_end_date': 'TIMESTAMP',
    'new_end_date': 'TIMESTAMP',
    'contract_execution_date': 'TIMESTAMP',
    'grant_award_date': 'TIMESTAMP',
    'po_order_date': 'TIMESTAMP',
    'agency_service_area': 'VARCHAR(127)',
    'authorized_advanced_payment': 'VARCHAR(127)',
    'method_of_procurement': 'VARCHAR(511)',
    'state_term_contract_id': 'VARCHAR(63)',
    'agency_reference_number': 'VARCHAR(127)',
    'contract_exemption_explanation': 'TEXT',
    'statutory_authority': 'VARCHAR(127)',
    'recipient_type': 'VARCHAR(63)',
    'involves_gov_aid': 'VARCHAR(63)',
    'provide_admin_cost': 'VARCHAR(63)',
    'admin_cost_percent': 'VARCHAR(63)',
    'provide_periodic_increase': 'VARCHAR(63)',
    'periodic_increase_percent': 'VARCHAR(63)',
    'business_case_study': 'VARCHAR(63)',
    'business_case_date': 'TIMESTAMP',
    'legal_challenges': 'VARCHAR(63)',
    'legal_challenge_description': 'TEXT',
    'previously_done_by_state': 'VARCHAR(63)',
    'considered_for_insourcing': 'VARCHAR(63)',
    'improvement_on_state_property': 'VARCHAR(63)',
    'improvement_description': 'TEXT',
    'improvement_value': 'FLOAT',
    'improvement_unamortized_value': 'VARCHAR(255)',
    'comment': 'TEXT',
    'cfda_code': 'VARCHAR(63)',
    'cfda_description': 'VARCHAR(255)',
    'csfa_code': 'VARCHAR(63)',
    'csfa_description': 'VARCHAR(127)'
}
UNIQUE_COLUMNS = [
    'agency', 'vendor', 'type', 'agency_contract_id', 'po_number',
    'grant_award_id', 'commodity_type_code', 'flair_contract_id',
    'state_term_contract_id', 'status'
]
FORCE_NULL_COLUMNS = [
    'orig_contract_amount', 'total_amount', 'po_budget_amount',
    'improvement_value', 'begin_date', 'original_end_date', 'new_end_date',
    'contract_execution_date', 'grant_award_date', 'po_order_date',
    'business_case_date'
]
TABLE_NAME = 'us_fl_facts_filing'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

if not os.path.exists(DOWNLOAD_DIR):
    os.makedirs(DOWNLOAD_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)


def csv_to_df(csv_name,
              corrected_csv=CORRECTED_CSV,
              corrected_json=CORRECTED_JSON):
    def preprocess_line(line):
        # If the line begins with `","`, assume the first quote is spurious.
        line = re.sub(r'^","', ',"', line)

        return line

    util.patch_csv.patch(csv_name,
                         corrected_csv,
                         corrected_json,
                         sep=',',
                         assume_always_quoted=True,
                         preprocess_line=preprocess_line)

    df = pd.read_csv(corrected_csv, dtype=object)

    # Join the vendor second line onto the first, then drop the second.
    VENDOR_COLUMNS = ['Vendor/Grantor Name', 'Vendor/Grantor Name Line 2']
    df[VENDOR_COLUMNS] = df[VENDOR_COLUMNS].astype(str)

    def join_names(names):
        return ' '.join([name for name in names if (name and name != 'nan')])

    df['Vendor/Grantor Name'] = df[VENDOR_COLUMNS].agg(join_names, axis=1)
    df = df.drop(columns=['Vendor/Grantor Name Line 2'])

    # Strip the strings.
    df = df.applymap(lambda x: x.strip() if isinstance(x, str) else x)

    # Coerce the numeric types out of strings.
    for float_column in FACTS_FLOATS:
        df[float_column] = pd.to_numeric(df[float_column], errors='coerce')
        df[float_column] = df[float_column].astype(np.float32)

    # Force date columns to datetime
    for datetime_column in FACTS_DATETIMES:
        df[datetime_column] = pd.to_datetime(df[datetime_column],
                                             format='%m/%d/%Y',
                                             errors='coerce')

    df = df.rename(columns=FACTS_SIMPLIFIED_NAMES)

    # Ensure the whitespace is merged in the vendor column.
    df.replace({'vendor': r'\s+'}, value=' ', inplace=True, regex=True)

    return df


def write_unique_vendors(df, unique_vendors_json):
    unique_names = sorted([x.lower() for x in df['vendor'].unique()])
    with open(unique_vendors_json, 'w') as outfile:
        json.dump(unique_names, outfile, indent=1)


def export_df_to_csv(df, export_csv_name):
    df = util.postgres.preprocess_dataframe(df, unique_columns=UNIQUE_COLUMNS)
    util.postgres.df_to_csv(df, export_csv_name, columns=TABLE_TYPES.keys())


def process_csv(orig_csv_name,
                output_csv=OUTPUT_CSV,
                corrected_csv=CORRECTED_CSV,
                corrected_json=CORRECTED_JSON):
    df = csv_to_df(orig_csv_name,
                   corrected_csv=corrected_csv,
                   corrected_json=corrected_json)
    export_df_to_csv(df, output_csv)
    os.remove(corrected_csv)


def download_csv(begin_date=None,
                 end_date=None,
                 headless=True,
                 max_wait_seconds=MAX_WAIT_SECONDS,
                 max_download_seconds=MAX_DOWNLOAD_SECONDS,
                 download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
                 download_dir=DOWNLOAD_DIR,
                 gecko_path=None,
                 quit_browser=False,
                 num_sleep_seconds=5,
                 verbose=False):
    if begin_date is None and end_date is None:
        print(
            'WARNING: Requesting more than a year of data will hang the FL FACTS server and is HIGHLY unrecommended.'
        )

    browser = util.firefox.get_browser(headless=headless,
                                       download_dir=download_dir)

    # Fill in the date range in the Month/Day/Year format on the FL FACTS search
    # page then click the Search button.
    browser.get(SEARCH_URL)
    date_infos = [{
        'date': begin_date,
        'id': 'PC_txtBeginDate'
    }, {
        'date': end_date,
        'id': 'PC_txtEndDate'
    }]
    num_date_sleep_seconds = 1
    for date_info in date_infos:
        if date_info['date'] is not None:
            year, month, day = util.dates.date_to_tuple(date_info['date'])
            date_keys = '{}/{}/{}'.format(month, day, year)
            try:
                element = browser.find_element(By.ID, date_info['id'])
                element.send_keys(date_keys + Keys.ENTER)
            except Exception as e:
                print('ERR: Could not find {}'.format(date_info['id']))
                with open('error.html', 'w') as outfile:
                    outfile.write(browser.page_source)
            if verbose:
                print('Found {} element.'.format(date_info['id']))
            time.sleep(num_date_sleep_seconds)

    search_button_id = 'PC_btnSearch'
    element = browser.find_element(By.ID, search_button_id)
    if element:
        if verbose:
            print('Found {}'.format(search_button_id))
        element.send_keys(Keys.ENTER)
    else:
        if verbose:
            print('WARNING: Did not find {}'.format(search_button_id))

    filename = 'ContractsExport.csv'
    filepath = '{}/{}'.format(download_dir, filename)
    if os.path.exists(filepath):
        if verbose:
            print('Removing stale {}'.format(filepath))
        os.remove(filepath)
    try:
        export_button_id = 'PC_hlkExport'
        element = WebDriverWait(browser, max_wait_seconds).until(
            EC.presence_of_element_located((By.ID, export_button_id)))
        if element:
            if verbose:
                print('Found {}'.format(export_button_id))
            element.send_keys(Keys.ENTER)
        else:
            if verbose:
                print('WARNING: Did not find {}'.format(export_button_id))
    except Exception as e:
        print('Could not retrieve element.')
        print(e)

    completed = util.firefox.watch_download(
        filepath,
        download_recheck_seconds=download_recheck_seconds,
        max_download_seconds=max_download_seconds,
        verbose=verbose)
    if verbose:
        if completed:
            print('Finished download of {}'.format(filepath))
        else:
            print('WARNING: Download of {} did not complete in time.'.format(
                filepath))

    # Due to a bug in which attempting to quit the browser results in the
    # program hanging, we have added this workaround.
    if quit_browser:
        browser.close()
        if verbose:
            print('Closed browser session')
        browser.quit()
        if verbose:
            print('Quit browser session')
    else:
        # We are attempting to ensure the download completes.
        print('Sleeping for {} seconds to help ensure download completes.'.
              format(num_sleep_seconds))
        time.sleep(num_sleep_seconds)


def retrieve(begin_date=None,
             end_date=None,
             headless=True,
             window_days=31,
             output_csv=OUTPUT_CSV,
             corrected_csv=CORRECTED_CSV,
             corrected_json=CORRECTED_JSON,
             max_wait_seconds=MAX_WAIT_SECONDS,
             max_download_seconds=MAX_DOWNLOAD_SECONDS,
             download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
             gecko_path=None,
             quit_browser=False,
             verbose=False):
    if begin_date is None and end_date is None:
        # Set the date range to the last 'window_days' days.
        today = datetime.date.today()
        begin_year, begin_month, begin_day = util.dates.earlier_day(
            today.year, today.month, today.day, window_days)
        begin_date = util.dates.tuple_to_date(begin_year, begin_month,
                                              begin_day)

    download_csv(begin_date=begin_date,
                 end_date=end_date,
                 headless=headless,
                 max_wait_seconds=max_wait_seconds,
                 max_download_seconds=max_download_seconds,
                 download_recheck_seconds=download_recheck_seconds,
                 download_dir=DOWNLOAD_DIR,
                 gecko_path=gecko_path,
                 quit_browser=quit_browser,
                 verbose=verbose)
    orig_csv_name = '{}/ContractsExport.csv'.format(DOWNLOAD_DIR)
    process_csv(orig_csv_name, output_csv, corrected_csv, corrected_json)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(TABLE_NAME,
                                       TABLE_TYPES,
                                       serial_id=True,
                                       constraints={
                                           'unique':
                                           '''
UNIQUE (
  agency, vendor, type, agency_contract_id, po_number, grant_award_id,
  commodity_type_code, flair_contract_id, state_term_contract_id, status
)
                '''
                                       }))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_vendor_index ON {} (LOWER(vendor));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_agency_index ON {} (LOWER(agency));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(agency),                         '') || ' ' ||
    coalesce(lower(vendor),                         '') || ' ' ||
    coalesce(lower(type),                           '') || ' ' ||
    coalesce(lower(agency_contract_id),             '') || ' ' ||
    coalesce(lower(po_number),                      '') || ' ' ||
    coalesce(lower(grant_award_id),                 '') || ' ' ||
    coalesce(lower(commodity_type_code),            '') || ' ' ||
    coalesce(lower(commodity_type_description),     '') || ' ' ||
    coalesce(lower(short_title),                    '') || ' ' ||
    coalesce(lower(long_title),                     '') || ' ' ||
    coalesce(lower(status),                         '') || ' ' ||
    coalesce(lower(flair_contract_id),              '') || ' ' ||
    coalesce(lower(agency_service_area),            '') || ' ' ||
    coalesce(lower(authorized_advanced_payment),    '') || ' ' ||
    coalesce(lower(method_of_procurement),          '') || ' ' ||
    coalesce(lower(state_term_contract_id),         '') || ' ' ||
    coalesce(lower(agency_reference_number),        '') || ' ' ||
    coalesce(lower(contract_exemption_explanation), '') || ' ' ||
    coalesce(lower(statutory_authority),            '') || ' ' ||
    coalesce(lower(recipient_type),                 '') || ' ' ||
    coalesce(lower(involves_gov_aid),               '') || ' ' ||
    coalesce(lower(business_case_study),            '') || ' ' ||
    coalesce(lower(legal_challenge_description),    '') || ' ' ||
    coalesce(lower(considered_for_insourcing),      '') || ' ' ||
    coalesce(lower(improvement_description),        '') || ' ' ||
    coalesce(lower(comment),                        '') || ' ' ||
    coalesce(lower(cfda_code),                      '') || ' ' ||
    coalesce(lower(cfda_description),               '') || ' ' ||
    coalesce(lower(csfa_code),                      '') || ' ' ||
    coalesce(lower(csfa_description),               '')));
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=OUTPUT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['us', 'fl', 'procurement'], entities)
