#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This utility is for processing the New Zealand federal procurement records
# located at:
#
#  https://www.mbie.govt.nz/cross-government-functions/new-zealand-government-procurement-and-property/open-data/
#
# We must convert away from the original non UTF-8 encoding (cp1252).
#
# The schema for the original columns was given as:
#
# Header Column	        Description
# ==============================================================================
# Posting Agency	      Agency who have posted the tender.
#
# RFx ID	              Unique Identifier created by the GETS system
#
# RFx Type              Type of tender. Limited to one of the following;
#                       Request for Proposal, Request for Quotations,
#                       Request for Tenders and Award Notice
#
# Competition Type      Open or Closed Competition.
#
# Title                 Descriptive title of the works, assigned by the tender
#                       poster.	Special characters may be present in this field.
#
# Reference Number	    A non-unique reference number, such as a project ID that
#                       can be used for multiple tenders. Can be a combination
#                       of alphabetical and numerical characters.
#
# Open Date             Date tender opens.
#
# Close Date            Date tender closes.
#
# Awarded Date          Date tender awarded (including tenders marked
#                       'Not Awarded').
#
# Department            Department within posting agencies that has submitted
#                       the tender.
#
# Tender Coverage       Who tender is for (All of Government, Cluster, On behalf
#                       of procurement agent, Sole Agency, Syndicated
#                       Opportunity)
#
# Prequalification      If 'Yes' is selected only tenders from prequalified
# Required?             suppliers will be accepted.
#
# Alternative Physical  Address (physical or electronic) where tenders may also
# Tender Box Delivery   be submitted.
# Address
#
# Overview
#
# Award Type            Tender will either be Awarded or Not Awarded.	If an
#                       agency wishes to award a tender to a manually received
#                       response they need to mark the Award Type field as not
#                       awarded so they can award it to a supplier off the
#                       system. However agencies may inadvertently mark this
#                       field as not award incorrectly.
#
# Comments              Free text field where the posting agency can give
#                       additional information bout the winning tender(s).
#
# Awarded Amount        Amount of the awarded.
#
# Report Date           Date report was last updated.
#
import json
import os
import pandas as pd
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import util.postgres
import util.firefox

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
AWARDS_DIR = os.path.join(DATA_DIR, 'nz_awards')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')
STAGING_DIR = os.path.join(WORKSTATION_DIR, 'staging')
AWARDS_STAGING_DIR = os.path.join(STAGING_DIR, 'nz_awards')

# The maximum number of seconds to wait before timing out.
MAX_WAIT_SECONDS = 60

# The maximum number of seconds to monitor a download before failing.
MAX_DOWNLOAD_SECONDS = 600

# The number of seconds to wait between each check if a download completed.
DOWNLOAD_RECHECK_SECONDS = 0.25

# The CSVs start out with the following encoding (and we must convert them).
ORIG_ENCODING = 'utf8'

OPEN_DATA_BASE = 'https://www.mbie.govt.nz'
OPEN_DATA_RELPATH = 'cross-government-functions/new-zealand-government-procurement-and-property/open-data/'
OPEN_DATA_URL = '{}/{}'.format(OPEN_DATA_BASE, OPEN_DATA_RELPATH)

# The URLs of the current and historic CSVs of award data.
# Note: New Zealand often blocks direct requests for these files.
NOTICES_CSV_CURRENT_RELPATH = 'assets/Data-Files/NZGPP-GETS-Open-Data/GETS-award-notices.csv'
NOTICES_CSV_HISTORIC_RELPATH = 'assets/Data-Files/NZGPP-GETS-Open-Data/GETS-award-notices-historic.csv'
NOTICES_CSV_CURRENT = '{}/{}'.format(OPEN_DATA_BASE,
                                     NOTICES_CSV_CURRENT_RELPATH)
NOTICES_CSV_HISTORIC = '{}/{}'.format(OPEN_DATA_BASE,
                                      NOTICES_CSV_HISTORIC_RELPATH)

# The names of the export files for the current and historic datasets.
CSV_CURRENT = '{}/nz_award_notice.csv'.format(EXPORT_DIR)
CSV_HISTORIC = '{}/nz_award_notice_historic.csv'.format(EXPORT_DIR)

# The name of the intermediate CSV for the current and historic notices.
CSV_CURRENT_ORIG = '{}/nz_award_notices_current.csv'.format(AWARDS_STAGING_DIR)
CSV_HISTORIC_ORIG = '{}/nz_award_notices_historic.csv'.format(
    AWARDS_STAGING_DIR)

# The name of the intermediate JSON files for the current and historic notices.
JSON_CURRENT = '{}/nz_award_notices_current.json'.format(AWARDS_STAGING_DIR)
JSON_HISTORIC = '{}/nz_award_notices_historic.json'.format(AWARDS_STAGING_DIR)

COLUMN_MAP = {
    'Posting Agency': 'posting_agency',
    'RFx ID': 'notice_id',
    'RFx Type': 'notice_type',
    'Competition Type': 'competition_type',
    'Title': 'title',
    'Reference Number': 'reference_number',
    'Open Date': 'open_date',
    'Close Date': 'close_date',
    'Awarded Date ': 'awarded_date',
    'Department': 'department',
    'Tender Coverage': 'tender_coverage',
    'Prequalification Required?': 'prequalification',
    'Alternative Physical Tender Box Delivery Address': 'alternate_address',
    'Overview': 'overview',
    'Award Type': 'award_type',
    'Comments': 'comments',
    'Awarded Amount': 'awarded_amount',
    'Report Date': 'report_date'
}

DTYPE_MAP = {'RFx ID': str}

DATE_COLUMNS = ['open_date', 'close_date', 'awarded_date', 'report_date']

HISTORIC_TABLE_TYPES = {
    'posting_agency': 'VARCHAR(255)',
    'notice_id': 'VARCHAR(31)',
    'notice_type': 'VARCHAR(31)',
    'competition_type': 'VARCHAR(31)',
    'title': 'VARCHAR(255)',
    'reference_number': 'VARCHAR(31)',
    'open_date': 'TIMESTAMP',
    'close_date': 'TIMESTAMP',
    'awarded_date': 'TIMESTAMP',
    'department': 'VARCHAR(127)',
    'tender_coverage': 'VARCHAR(31)',
    'prequalification': 'VARCHAR(31)',
    'alternate_address': 'VARCHAR(255)',
    'overview': 'TEXT',
    'award_type': 'VARCHAR(31)',
    'comments': 'TEXT',
    'report_date': 'TIMESTAMP'
}

CURRENT_TABLE_TYPES = {
    'posting_agency': 'VARCHAR(255)',
    'notice_id': 'VARCHAR(31)',
    'notice_type': 'VARCHAR(31)',
    'competition_type': 'VARCHAR(31)',
    'title': 'VARCHAR(255)',
    'reference_number': 'VARCHAR(31)',
    'open_date': 'TIMESTAMP',
    'close_date': 'TIMESTAMP',
    'awarded_date': 'TIMESTAMP',
    'department': 'VARCHAR(127)',
    'tender_coverage': 'VARCHAR(31)',
    'prequalification': 'VARCHAR(31)',
    'alternate_address': 'VARCHAR(255)',
    'overview': 'TEXT',
    'award_type': 'VARCHAR(31)',
    'comments': 'TEXT',
    'awarded_amount': 'FLOAT',
    'report_date': 'TIMESTAMP'
}

UNIQUE_COLUMNS = ['notice_id']

HISTORIC_FORCE_NULL_COLUMNS = [
    'open_date', 'close_date', 'awarded_date', 'report_date'
]
CURRENT_FORCE_NULL_COLUMNS = [
    'open_date', 'close_date', 'awarded_date', 'awarded_amount', 'report_date'
]

TABLE_NAME = 'nz_award_notice'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

if not os.path.exists(AWARDS_DIR):
    os.makedirs(AWARDS_DIR)
if not os.path.exists(AWARDS_STAGING_DIR):
    os.makedirs(AWARDS_STAGING_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)


def export_df_to_csv(df, csv_filename):
    df = util.postgres.preprocess_dataframe(df, unique_columns=UNIQUE_COLUMNS)
    util.postgres.df_to_csv(df, csv_filename)


def export_csv(csv_downloaded,
               csv_orig_filename=CSV_CURRENT_ORIG,
               csv_filename=CSV_CURRENT,
               json_filename=JSON_CURRENT):
    if os.path.exists(csv_downloaded):
        df = pd.read_csv(csv_downloaded,
                         dtype=DTYPE_MAP,
                         encoding=ORIG_ENCODING)
    else:
        print('Did not find downloaded CSV at {}'.format(csv_downloaded))
        return

    df = df.rename(columns=COLUMN_MAP)
    df['notice_id'] = df['notice_id'].astype(str)
    for column in DATE_COLUMNS:
        df[column] = pd.to_datetime(df[column],
                                    format='%Y%m%d',
                                    errors='coerce')
    df.to_csv(csv_orig_filename, encoding='utf-8')
    df.to_json(json_filename, orient='records', indent=1, date_format='iso')
    export_df_to_csv(df, csv_filename)


def export_historic_csv(csv_downloaded,
                        csv_orig_filename=CSV_HISTORIC_ORIG,
                        csv_filename=CSV_HISTORIC,
                        json_filename=JSON_HISTORIC):
    if os.path.exists(csv_downloaded):
        df = pd.read_csv(csv_downloaded,
                         dtype=DTYPE_MAP,
                         encoding=ORIG_ENCODING)
    else:
        print('Did not find downloaded CSV at {}'.format(csv_downloaded))
        return

    df = df.rename(columns=COLUMN_MAP)
    df['notice_id'] = df['notice_id'].astype(str)
    for column in DATE_COLUMNS:
        df[column] = pd.to_datetime(df[column],
                                    format='%Y%m%d',
                                    errors='coerce')
    df.to_csv(csv_orig_filename, encoding='utf-8')
    df.to_json(json_filename, orient='records', indent=1, date_format='iso')
    export_df_to_csv(df, csv_filename)


def download_csv(headless=True,
                 max_wait_seconds=MAX_WAIT_SECONDS,
                 max_download_seconds=MAX_DOWNLOAD_SECONDS,
                 download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
                 download_dir=AWARDS_STAGING_DIR,
                 gecko_path=None,
                 verbose=False):
    browser = util.firefox.get_browser(headless=headless,
                                       download_dir=download_dir)

    browser.get(OPEN_DATA_URL)

    filename = 'GETS-award-notices.csv'
    filepath = '{}/{}'.format(download_dir, filename)
    if os.path.exists(filepath):
        if verbose:
            print('Removing stale {}'.format(filepath))
        os.remove(filepath)
    util.firefox.find_element_by_url(
        browser, '/' + NOTICES_CSV_CURRENT_RELPATH).send_keys(Keys.ENTER)

    completed = util.firefox.watch_download(
        filepath,
        download_recheck_seconds=download_recheck_seconds,
        max_download_seconds=max_download_seconds,
        verbose=verbose)
    if not completed:
        print('Download of {} did not complete in time.'.format(filepath))
    browser.quit()


def download_historic_csv(headless=True,
                          max_wait_seconds=MAX_WAIT_SECONDS,
                          max_download_seconds=MAX_DOWNLOAD_SECONDS,
                          download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
                          download_dir=AWARDS_STAGING_DIR,
                          gecko_path=None,
                          verbose=False):
    browser = util.firefox.get_browser(headless=headless,
                                       download_dir=download_dir)

    browser.get(OPEN_DATA_URL)

    filename = 'GETS-award-notices-historic.csv'
    filepath = '{}/{}'.format(download_dir, filename)
    if os.path.exists(filepath):
        if verbose:
            print('Removing stale {}'.format(filepath))
        os.remove(filepath)
    util.firefox.find_element_by_url(
        browser, '/' + NOTICES_CSV_HISTORIC_RELPATH).send_keys(Keys.ENTER)

    completed = util.firefox.watch_download(
        filepath,
        download_recheck_seconds=download_recheck_seconds,
        max_download_seconds=max_download_seconds,
        verbose=verbose)
    if not completed:
        print('Download of {} did not complete in time.'.format(filepath))
    browser.quit()


def retrieve(csv_orig_filename=CSV_CURRENT_ORIG,
             csv_filename=CSV_CURRENT,
             json_filename=JSON_CURRENT,
             headless=True,
             max_wait_seconds=MAX_WAIT_SECONDS,
             max_download_seconds=MAX_DOWNLOAD_SECONDS,
             download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
             gecko_path=None,
             verbose=False):
    '''Downloads and exports the most recent notices using Selenium.'''
    download_csv(headless=headless,
                 max_wait_seconds=max_wait_seconds,
                 max_download_seconds=max_download_seconds,
                 download_recheck_seconds=download_recheck_seconds,
                 download_dir=AWARDS_STAGING_DIR,
                 gecko_path=gecko_path,
                 verbose=verbose)

    downloaded_csv = '{}/GETS-award-notices.csv'.format(AWARDS_STAGING_DIR)
    try:
        export_csv(downloaded_csv,
                   csv_orig_filename=csv_orig_filename,
                   csv_filename=csv_filename,
                   json_filename=json_filename)
    except Exception as e:
        print('Could not export New Zealand award notices: {}'.format(e))


def retrieve_historic(csv_orig_filename=CSV_HISTORIC_ORIG,
                      csv_filename=CSV_HISTORIC,
                      json_filename=JSON_HISTORIC,
                      headless=True,
                      max_wait_seconds=MAX_WAIT_SECONDS,
                      max_download_seconds=MAX_DOWNLOAD_SECONDS,
                      download_recheck_seconds=DOWNLOAD_RECHECK_SECONDS,
                      gecko_path=None,
                      verbose=False):
    '''Downloads and exports the historic notices using Selenium.'''
    download_historic_csv(headless=headless,
                          max_wait_seconds=max_wait_seconds,
                          max_download_seconds=max_download_seconds,
                          download_recheck_seconds=download_recheck_seconds,
                          download_dir=AWARDS_STAGING_DIR,
                          gecko_path=gecko_path,
                          verbose=verbose)

    downloaded_csv = '{}/GETS-award-notices-historic.csv'.format(
        AWARDS_STAGING_DIR)
    try:
        export_historic_csv(downloaded_csv,
                            csv_orig_filename=csv_orig_filename,
                            csv_filename=csv_filename,
                            json_filename=json_filename)
    except Exception as e:
        print('Could not export historic New Zealand award notices: {}'.format(
            e))


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TABLE_NAME,
            CURRENT_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (notice_id)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_id_index ON {} (lower(notice_id));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(posting_agency),    '') || ' ' ||
    coalesce(lower(notice_id),         '') || ' ' ||
    coalesce(lower(notice_type),       '') || ' ' ||
    coalesce(lower(competition_type),  '') || ' ' ||
    coalesce(lower(title),             '') || ' ' ||
    coalesce(lower(reference_number),  '') || ' ' ||
    coalesce(lower(department),        '') || ' ' ||
    coalesce(lower(alternate_address), '') || ' ' ||
    coalesce(lower(overview),          '') || ' ' ||
    coalesce(lower(comments),          '')
  )
);
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=CSV_CURRENT,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         CURRENT_TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         CURRENT_FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_update_historic(export_csv=CSV_HISTORIC,
                             user=None,
                             password=None,
                             dbname=None,
                             port=None,
                             host=None,
                             vacuum=True):
    util.postgres.update(TABLE_NAME,
                         HISTORIC_TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         HISTORIC_FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
