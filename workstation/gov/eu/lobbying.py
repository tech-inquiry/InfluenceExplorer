# The European Union Transparency Register is located at:
#
#   https://data.europa.eu/data/datasets/transparency-register?locale=en
#
# and the XML for the 'Organisations in Transparency Register' is located at:
#
#   https://ec.europa.eu/transparencyregister/public/consultation/statistics.do?action=getLobbyistsXml&fileType=NEW
#
# TODO(Jack Poulson): Investigate the
# 'Accredited persons for access to the European Parliament' XML located at
#
#   https://ec.europa.eu/transparencyregister/public/consultation/statistics.do?action=getLobbyistsXml&fileType=ACCREDITED_PERSONS
#
import json
import os
import urllib.request
import xmltodict

import pandas as pd

from io import StringIO

import util.postgres

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
LOBBY_DIR = os.path.join(DATA_DIR, 'eu_lobby')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

TABLE_NAME = 'eu_lobbying'
TABLE_TYPES = {
    'identification_code': 'TEXT',
    'registration_date': 'TIMESTAMP',
    'last_update_date': 'TIMESTAMP',
    'name': 'TEXT',
    'acronym': 'TEXT',
    'entity_form': 'TEXT',
    'website_url': 'TEXT',
    'registration_category': 'TEXT',
    'head_office': 'JSONB',
    'eu_office': 'JSONB',
    'goals': 'TEXT',
    'levels_of_interest': 'JSONB',
    'legislative_proposals': 'JSONB',
    'supported_forums': 'JSONB',
    'communications': 'TEXT',
    'members': 'JSONB',
    'interests': 'JSONB',
    'structure': 'JSONB',
    'interest_represented': 'TEXT',
    'financial_data': 'JSONB',
    'groupings': 'TEXT'
}
UNIQUE_COLUMNS = ['identification_code']
JSON_COLUMNS = [
    'head_office', 'eu_office', 'levels_of_interest', 'legislative_proposals',
    'supported_forums', 'members', 'interests', 'structure', 'financial_data'
]
DATE_COLUMNS = ['registration_date', 'last_update_date']
FORCE_NULL_COLUMNS = JSON_COLUMNS + DATE_COLUMNS

EXPORT_CSV = '{}/{}.csv'.format(EXPORT_DIR, TABLE_NAME)

if not os.path.exists(LOBBY_DIR):
    os.makedirs(LOBBY_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)

ORGANISATIONS_XML_URL = 'https://ec.europa.eu/transparencyregister/public/consultation/statistics.do?action=getLobbyistsXml&fileType=NEW'

ORGANISATIONS_XML_FILENAME = 'data/eu_lobby/full_export_new.xml'


# Unfortunately, the EU server frequently kills the connection part way, and
# so manually downloading the file is currently recommended.
def get_organisations_file(xml_url=ORGANISATIONS_XML_URL,
                           xml_filename=ORGANISATIONS_XML_FILENAME):
    urllib.request.urlretrieve(xml_url, xml_filename)


def delete_from_dict(d, key_list):
    if d == None:
        return
    obj = d
    for key in key_list[:-1]:
        if key not in obj:
            return
        obj = obj[key]
    if key_list[-1] not in obj:
        return
    del obj[key_list[-1]]


def process_supported_forums(d, key, new_key):
    if key not in d:
        d[new_key] = []
        return
    tokens = d[key].split('#')
    i = 0
    forums = []
    while i < len(tokens):
        forum = {'name': tokens[i]}
        i += 1
        if i == len(tokens):
            forums.append(forum)
            break
        # What is the difference between E03832, E03710, and E03898?
        if tokens[i].startswith('E0'):
            forum['url'] = tokens[i + 1]
            i += 2
            if i == len(tokens):
                forums.append(forum)
                break
        if tokens[i].strip() == 'MEMBER':
            forum['category'] = tokens[i].strip()
            i += 1
            if i == len(tokens):
                forums.append(forum)
                break
        if tokens[i].strip() == 'C':
            forum['classification'] = tokens[i + 1]
            i += 2
            if i == len(tokens):
                forums.append(forum)
                break
        if tokens[i].strip() == 'Industry':
            if (i + 1) == len(tokens):
                forums.append(forum)
                break
            forum['industry'] = tokens[i + 1]
            i += 2
            if i == len(tokens):
                forums.append(forum)
                break
        forums.append(forum)
    d[new_key] = forums
    del d[key]


def split_field(d, key, new_key=None):
    if new_key == None:
        new_key = key
    if key in d and d[key]:
        d[new_key] = d[key].split('\r\n')
        if new_key != key:
            del d[key]


def process_clients(year):
    if 'clients' not in year:
        return
    clients = year['clients']
    if not clients:
        return
    keys = clients.keys()
    if len(keys) > 1 or 'client' not in clients:
        print('ERROR: Client keys were {}'.format(keys))
        return
    if len(keys) == 1:
        client_list = year['clients']['client']
        client_list = client_list if isinstance(client_list,
                                                list) else [client_list]
        for client in client_list:
            delete_from_dict(client, ['revenue', '@xsi:type'])
        year['clients'] = client_list


def process_intermediaries(year):
    if 'intermediaries' not in year or not year['intermediaries']:
        return
    intermediaries = year['intermediaries']
    keys = intermediaries.keys()
    if len(keys) > 1 or 'intermediary' not in intermediaries:
        print('ERROR: Intermediary keys were {}'.format(keys))
        return
    if len(keys) == 1:
        intermediary_list = year['intermediaries']['intermediary']
        intermediary_list = intermediary_list if isinstance(
            intermediary_list, list) else [intermediary_list]
        for intermediary in intermediary_list:
            delete_from_dict(intermediary,
                             ['representationCosts', '@xsi:type'])
        year['intermediaries'] = intermediary_list


def process_funding_sources(year):
    if 'fundingSources' not in year:
        return
    sources = year['fundingSources']
    if sources == None:
        return
    keys = sources.keys()
    if len(keys) > 1 or 'fundingSource' not in sources:
        print('ERROR: Funding sources keys were {}'.format(keys))
        return
    if len(keys) == 1:
        source_list = year['fundingSources']['fundingSource']
        source_list = source_list if isinstance(source_list,
                                                list) else [source_list]
        new_source_list = []
        for source in source_list:
            if isinstance(source, str):
                new_source_list.append(source)
            else:
                if len(source.keys()) != 1 or 'source' not in source.keys():
                    print('ERROR: Source keys were {}'.format(source.keys()))
                    return
                new_source_list.append(source['source'])
        year['fundingSources'] = new_source_list


def process_grants(year):
    if 'grants' not in year:
        return
    grants = year['grants']
    if grants == None:
        return
    keys = grants.keys()
    if len(keys) > 1 or 'grant' not in grants:
        print('ERROR: Grant keys were {}'.format(keys))
        return
    if len(keys) == 1:
        grant_list = year['grants']['grant']
        grant_list = grant_list if isinstance(grant_list,
                                              list) else [grant_list]
        for grant in grant_list:
            if 'amount' in grant:
                amount_keys = grant['amount'].keys()
                if 'absoluteCost' not in grant['amount']:
                    print(
                        'ERROR: Grant amount keys were {}'.format(amount_keys))
                    return
                grant['cost'] = grant['amount']['absoluteCost']
                delete_from_dict(grant, ['amount'])
        year['grants'] = grant_list


def process_contributions(year):
    if 'contributions' not in year:
        return
    sources = year['contributions']
    if sources == None:
        return
    keys = sources.keys()
    if len(keys) > 1 or 'contributor' not in sources:
        print('ERROR: Contributions keys were {}'.format(keys))
        return
    if len(keys) == 1:
        contributions_list = year['contributions']['contributor']
        contributions_list = contributions_list if isinstance(
            contributions_list, list) else [contributions_list]
        for contribution in contributions_list:
            if 'amount' in contribution:
                amount_keys = contribution['amount'].keys()
                if 'absoluteCost' not in contribution['amount']:
                    print('ERROR: Contribution amount keys were {}'.format(
                        amount_keys))
                    return
                contribution['cost'] = contribution['amount']['absoluteCost']
                delete_from_dict(contribution, ['amount'])
        year['contributions'] = contributions_list


def simplify_financial_data(item):
    if 'financialData' not in item or not item['financialData']:
        item['financial_data'] = {}
        delete_from_dict(item, ['financialData'])
        return
    financial_data = item['financialData']
    for year_key in ['closedYear', 'currentYear']:
        if year_key in financial_data:
            year = financial_data[year_key]
            if year == None:
                continue
            delete_from_dict(year, ['@xmlns:xsi'])
            delete_from_dict(year, ['@xsi:type'])
            delete_from_dict(year, ['totalAnnualRevenue', '@xsi:type'])
            process_clients(year)
            process_intermediaries(year)
            process_grants(year)
            process_funding_sources(year)
            process_contributions(year)

            if 'intermediaries' in year and not year['intermediaries']:
                del year['intermediaries']
            delete_from_dict(year, ['costs', '@xsi:type'])
            if len(year.keys()) == 0:
                del financial_data[year_key]
    item['financial_data'] = financial_data
    del item['financialData']


def rename_key(d, orig_key, new_key):
    if orig_key in d:
        d[new_key] = d[orig_key]
        del d[orig_key]


def export_organisations(xml_filename=ORGANISATIONS_XML_FILENAME,
                         csv_filename=EXPORT_CSV):
    with open(xml_filename) as xml_file:
        parsed_xml = xmltodict.parse(xml_file.read())

    if 'ListOfIRPublicDetail' not in parsed_xml:
        print(parsed_xml.keys())
        print('ERROR')
        return None
    else:
        # TODO(Jack Poulson): Decide if we want to keep any of the metadata.
        data = parsed_xml['ListOfIRPublicDetail']['resultList'][
            'interestRepresentative']

    for item in data:
        # So far, this is always 0.
        delete_from_dict(item, ['EPAccreditedNumber'])

        rename_key(item, 'identificationCode', 'identification_code')
        rename_key(item, 'registrationDate', 'registration_date')
        rename_key(item, 'lastUpdateDate', 'last_update_date')
        # Coerce last_update_date to registration_date if the former is null.
        if 'last_update_date' not in item:
            item['last_update_date'] = item['registration_date']
        rename_key(item, 'entityForm', 'entity_form')
        rename_key(item, 'webSiteURL', 'website_url')
        rename_key(item, 'registrationCategory', 'registration_category')
        rename_key(item, 'headOffice', 'head_office')
        if 'EUOffice' in item:
            item['eu_office'] = item['EUOffice'] if item['EUOffice'] else {}
            del item['EUOffice']
        else:
            item['eu_office'] = {}
        if 'levelsOfInterest' in item:
            levels = item['levelsOfInterest']['levelOfInterest']
            level_list = []
            for level in levels:
                if isinstance(level, str):
                    if level != 'levelOfInterest':
                        level_list.append(level)
                else:
                    if len(level.keys()) != 1:
                        print('ERROR: Level keys were {}'.format(level.keys()))
                    level_list.append(level['levelOfInterest'])
            item['levels_of_interest'] = level_list
            del item['levelsOfInterest']
        split_field(item, 'EULegislativeProposals', 'legislative_proposals')
        process_supported_forums(item, 'EUSupportedForumsAndPlatforms',
                                 'supported_forums')
        split_field(item, 'EUSupportedForumsAndPlatforms', 'supported_forums')
        rename_key(item, 'communicationActivities', 'communications')
        rename_key(item, 'interestRepresented', 'interest_represented')

        if 'name' in item:
            if 'originalName' not in item['name']:
                print('ERROR: Name keys were {}'.format(item['name'].keys()))
            if 'nameInLatinAlphabet' in item['name']:
                item['name'] = item['name']['nameInLatinAlphabet']
            else:
                item['name'] = item['name']['originalName']
        for groupings_key in [
                'interOrUnofficalGroupings', 'interOrUnofficialGroupings'
        ]:
            if groupings_key in item:
                item['groupings'] = item[groupings_key]
                del item[groupings_key]
        if 'interests' in item:
            if len(item['interests'].keys()
                   ) != 1 or 'interest' not in item['interests']:
                print('ERROR: Interests keys were {}'.format(
                    item['interests'].keys()))
                return
            interests = item['interests']['interest']
            interests_list = []
            for interest in interests:
                if isinstance(interest, str):
                    interests_list.append(interest)
                else:
                    if len(interest.keys()) != 1 or 'name' not in interest:
                        print('ERROR: Interest keys were {}'.format(
                            interest.keys()))
                        return
                    interests_list.append(interest['name'])
                item['interests'] = interests_list
        if 'structure' in item:
            structure = item['structure']
            delete_from_dict(structure, ['@xmlns:xsi'])
            delete_from_dict(structure, ['@xsi:type'])
            split_field(structure, 'isMemberOf')
            split_field(structure, 'organisationMembers')
        simplify_financial_data(item)

    df = pd.read_json(StringIO(json.dumps(data)), orient='records')
    df = util.postgres.preprocess_dataframe(df,
                                            json_columns=JSON_COLUMNS,
                                            unique_columns=UNIQUE_COLUMNS)
    util.postgres.df_to_csv(df, csv_filename, columns=list(TABLE_TYPES.keys()))


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TABLE_NAME,
            TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (identification_code)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_name_index ON {} (lower(name));
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(identification_code),         '') || ' ' ||
    coalesce(lower(name),                        '') || ' ' ||
    coalesce(lower(acronym),                     '') || ' ' ||
    coalesce(lower(entity_form),                 '') || ' ' ||
    coalesce(lower(registration_category),       '') || ' ' ||
    coalesce(lower(head_office::TEXT),           '') || ' ' ||
    coalesce(lower(eu_office::TEXT),             '') || ' ' ||
    coalesce(lower(goals),                       '') || ' ' ||
    coalesce(lower(levels_of_interest::TEXT),    '') || ' ' ||
    coalesce(lower(legislative_proposals::TEXT), '') || ' ' ||
    coalesce(lower(supported_forums::TEXT),      '') || ' ' ||
    coalesce(lower(communications),              '') || ' ' ||
    coalesce(lower(members::TEXT),               '') || ' ' ||
    coalesce(lower(interests::TEXT),             '') || ' ' ||
    coalesce(lower(structure::TEXT),             '') || ' ' ||
    coalesce(lower(interest_represented),        '') || ' ' ||
    coalesce(lower(financial_data::TEXT),        '') || ' ' ||
    coalesce(lower(groupings),                   '')
  )
);
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=EXPORT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
