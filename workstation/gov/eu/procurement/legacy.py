#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# The primary exports are preprocess_xml and queue_translation_data.
#
from gov.eu.procurement.util import COUNTRIES, LANGUAGE_PREFERENCES, clean_nones, convert_to_list, make_list, rename_dict_key, rename_dict_keys

from util.dict_path import set_from_path
from util.entities import canonicalize
from util.opus import TRANSLATABLE_LANGUAGE_CODES

NOTICE_REPLACEMENTS = [{
    'orig': 'LG_ORIG',
    'new': 'language'
}, {
    'orig': 'n2021:PERFORMANCE_NUTS',
    'new': 'performance_nuts',
    'map': 'NUTS'
}, {
    'orig': 'n2021:CA_CE_NUTS',
    'new': 'ca_ce_nuts',
    'map': 'NUTS'
}, {
    'orig': 'n2021:TENDERER_NUTS',
    'new': 'tenderer_nuts',
    'map': 'NUTS'
}]

CODIF_REPLACEMENTS = [{
    'orig': 'AA_AUTHORITY_TYPE',
    'new': 'aa_authority',
    'map': 'AA_AUTHORITY_TYPE'
}, {
    'orig': 'AC_AWARD_CRIT',
    'new': 'ac_award_crit',
    'map': 'AC_AWARD_CRIT'
}, {
    'orig': 'MA_MAIN_ACTIVITIES',
    'new': 'ma_main_activities',
    'map': 'MA_MAIN_ACTIVITIES'
}, {
    'orig': 'NC_CONTRACT_NATURE',
    'new': 'nc_contract_nature',
    'map': 'NC_CONTRACT_NATURE'
}, {
    'orig': 'PR_PROC',
    'new': 'pr_proc',
    'map': 'PR_PROC'
}, {
    'orig': 'RP_REGULATION',
    'new': 'rp_regulation',
    'map': 'RP_REGULATION'
}, {
    'orig': 'TD_DOCUMENT_TYPE',
    'new': 'td_document',
    'map': 'TD_DOCUMENT_TYPE'
}, {
    'orig': 'TY_TYPE_BID',
    'new': 'ty_type_bid',
    'map': 'TY_TYPE_BID'
}]


def has_value(item, key):
    return item is not None and key in item and item[key] is not None


def replace_type_with_code(item, orig_key, new_key, code_dict):
    value = rename_dict_key(item, orig_key, new_key)
    if not value:
        return
    code = value['@CODE']
    if '#text' in value:
        text = value['#text']
        if code in code_dict:
            if code_dict[code] != text:
                print('Code {} text mismatch: {} != {}'.format(
                    code, text, code_dict[code]))
        else:
            code_dict[code] = text
    item[new_key] = code


def replace_type_list_with_codes(item, orig_key, new_key, code_dict):
    value = rename_dict_key(item, orig_key, new_key)
    if not value:
        return
    code_list = []
    value_list = convert_to_list(value)
    for value in value_list:
        code = value['@CODE']
        if '#text' in value:
            text = value['#text']
            if code in code_dict:
                if code_dict[code] != text:
                    print('Code {} text mismatch: {} != {}'.format(
                        code, text, code_dict[code]))
            else:
                code_dict[code] = text
        code_list.append(code)

    item[new_key] = '; '.join(code_list)


def get_paragraph(para):

    def extract_text(line):
        if isinstance(line, dict):
            key_list = list(line.keys())
            if not len(key_list):
                return None

            if '#text' in line:
                return line['#text']
            elif '#text' in line[key_list[0]]:
                # Handle cases such as:
                #
                #   {
                #     'FT': {
                #       '@TYPE': 'SUB',
                #       '#text': 'ODBIÓR I ZAGOSPODAROWANIE ODPADÓW KOMUNALNYCH Z NIERUCHOMOŚCI OBJĘTYCH SYSTEMEM GOSPODAROWANIA ODPADAMI KOMUNALNYMI PRZEZ CELOWY ZWIĄZEK GMIN „EKO-LOGICZNI” W 2022 ROKU'
                #     }
                #   }
                #
                return line[key_list[0]]['#text']
            else:
                return None
        elif isinstance(line, str):
            return line
        else:
            return ''

    return [extract_text(line) for line in convert_to_list(para) if line]


def merge_value_tag(item, key):
    if not has_value(item, key):
        return
    if '@VALUE' not in item[key]:
        del item[key]
        return
    item[key] = item[key]['@VALUE']


def merge_paragraph_tag(item, key):
    if not has_value(item, key):
        return
    if isinstance(item[key], dict):
        item[key] = get_paragraph(item[key]['P'])
    elif isinstance(item[key], list):
        print(
            'WARNING: Paragraph was actually a list of paragraphs, {}'.format(
                item[key]))
        item[key] = [get_paragraph(x['P'])[0] for x in item[key]]
    elif not isinstance(item[key], str):
        print('WARNING: Could not merge paragraph {}'.format(item[key]))
        del item[key]
        return

    if not item[key]:
        del item[key]


def convert_monetary(item, key):
    if key in item:
        rename_dict_keys(item[key], {
            '@CURRENCY': 'currency',
            '@TYPE': 'type',
            '#text': 'amount'
        })


def convert_duration(item, key):
    if key in item:
        rename_dict_keys(item[key], {'@TYPE': 'type', '#text': 'quantity'})


def convert_address(item, key, maps):
    if key in item:
        address = item[key]
        rename_dict_keys(
            address, {
                'OFFICIALNAME': 'official_name',
                'NATIONALID': 'national_id',
                'ADDRESS': 'address',
                'TOWN': 'city',
                'POSTAL_CODE': 'postal_code',
                'COUNTRY': 'country',
                'PHONE': 'phone',
                'FAX': 'fax',
                'E_MAIL': 'email',
                'CONTACT_POINT': 'contact_point',
                'URL': 'url',
                'URL_BUYER': 'buyer_url',
                'URL_GENERAL': 'general_url',
                'NO_SME': 'no_sme'
            })
        merge_value_tag(address, 'country')
        replace_type_with_code(address, 'n2021:NUTS', 'nuts', maps['NUTS'])


def simplify_coded_data(doc, maps):
    if not has_value(doc, 'CODED_DATA_SECTION'):
        return
    coded_data = doc.pop('CODED_DATA_SECTION')

    rename_dict_keys(
        coded_data, {
            'CODIF_DATA': 'codif_data',
            'NOTICE_DATA': 'notice_data',
            'REF_OJS': 'ref_ojs'
        })

    if has_value(coded_data, 'codif_data'):
        codif_data = coded_data['codif_data']
        rename_dict_keys(
            codif_data, {
                'DIRECTIVE': 'directive',
                'DS_DATE_DISPATCH': 'ds_date_dispatch',
                'DT_DATE_FOR_SUBMISSION': 'dt_date_for_submission',
                'HEADING': 'heading',
                'INITIATOR': 'initiator'
            })
        if has_value(codif_data, 'directive') and has_value(
                codif_data['directive'], '@VALUE'):
            codif_data['directive'] = codif_data['directive'].pop('@VALUE')
        for replacement in CODIF_REPLACEMENTS:
            replace_type_list_with_codes(codif_data, replacement['orig'],
                                         replacement['new'],
                                         maps[replacement['map']])

    if has_value(coded_data, 'notice_data'):
        notice_data = coded_data['notice_data']
        for replacement in NOTICE_REPLACEMENTS:
            orig_key = replacement['orig']
            new_key = replacement['new']
            if has_value(replacement, 'map'):
                replace_type_list_with_codes(notice_data, orig_key, new_key,
                                             maps[replacement['map']])
            else:
                rename_dict_key(notice_data, orig_key, new_key)

        rename_dict_keys(
            notice_data, {
                'IA_URL_ETENDERING': 'ia_url_etendering',
                'IA_URL_GENERAL': 'ia_url_general',
                'ISO_COUNTRY': 'country',
                'ORIGINAL_CPV': 'original_cpv',
                'NO_DOC_OJS': 'no_doc_ojs',
                'REF_NOTICE': 'ref_notice',
                'URI_LIST': 'uri',
                'VALUES': 'values'
            })

        if has_value(notice_data, 'ref_notice'):
            ref_notice = notice_data['ref_notice']
            rename_dict_key(ref_notice, 'NO_DOC_OJS', 'no_doc_ojs')

        if has_value(notice_data, 'uri'):
            uri_doc = notice_data['uri']['URI_DOC']
            if isinstance(uri_doc, dict):
                notice_data['uri'] = uri_doc['#text']
            elif isinstance(uri_doc, list):
                lang_state = {'score': -1, 'item': None}
                for item in uri_doc:
                    if has_value(LANGUAGE_PREFERENCES, item['@LG']):
                        score = LANGUAGE_PREFERENCES[item['@LG']]
                        if score > lang_state['score']:
                            lang_state['score'] = score
                            lang_state['item'] = item['#text']
                    elif lang_state['score'] < 0:
                        lang_state['score'] = 0
                        lang_state['item'] = item['#text']
                if lang_state['item']:
                    notice_data['uri'] = lang_state['item']

        merge_value_tag(notice_data, 'country')

        if has_value(notice_data, 'original_cpv'):
            original_cpv = notice_data['original_cpv']
            if isinstance(original_cpv, dict):
                notice_data['original_cpv'] = original_cpv['#text']
            elif isinstance(original_cpv, list):
                desc = '; '.join(item['#text'] for item in original_cpv)
                notice_data['original_cpv'] = desc

        if has_value(notice_data, 'values'):
            values = notice_data['values']
            rename_dict_key(values, 'VALUE', 'value')
            convert_monetary(values, 'value')

    if has_value(coded_data, 'ref_ojs'):
        ref_ojs = coded_data['ref_ojs']
        rename_dict_keys(ref_ojs, {
            'COLL_OJ': 'coll_oj',
            'NO_OJ': 'no_oj',
            'DATE_PUB': 'date_pub'
        })
        coded_data['ref_ojs'] = ref_ojs

    # Despite what the coded_data 'language' parameter claims, the text appears
    # to always be in English.
    set_from_path(doc['data'], ['en', 'coded_data'], coded_data)


def get_country(doc):
    country = doc['data']['en']['coded_data']['notice_data']['country']
    if isinstance(country, dict):
        country = country['@VALUE']
    return country.lower()


def simplify_defence_contract(form_value):
    DEFENCE_CONTRACT_RENAMING = {
        '@CTYPE': 'ctype',
        'CONTRACTING_AUTHORITY_INFORMATION_CONTRACT_AWARD_DEFENCE':
        'contracting_authority_info',
        'OBJECT_CONTRACT_INFORMATION_CONTRACT_AWARD_NOTICE_DEFENCE':
        'object_contract_info',
        'PROCEDURE_DEFINITION_CONTRACT_AWARD_NOTICE_DEFENCE':
        'procedure_definition',
        'ADMINISTRATIVE_INFORMATION_CONTRACT_AWARD_DEFENCE':
        'administrative_info',
        'AWARD_OF_CONTRACT_DEFENCE': 'contract_award',
        'COMPLEMENTARY_INFORMATION_CONTRACT_AWARD': 'complementary_info'
    }

    DEFENCE_CONTRACT_AWARD_RENAMING = {
        '@ITEM': 'item',
        'CONTRACT_AWARD_DATE': 'date',
        'CONTRACT_NUMBER': 'contract_number',
        'CONTRACT_TITLE': 'title',
        'CONTRACT_VALUE_INFORMATION': 'value',
        'ECONOMIC_OPERATOR_NAME_ADDRESS': 'operator',
        'MORE_INFORMATION_TO_SUB_CONTRACTED': 'subcontracting',
        'OFFERS_RECEIVED_NUMBER': 'offers_received_number',
        'OFFERS_RECEIVED_NUMBER_MEANING': 'offers_received_meaning'
    }

    DEFENCE_SUBCONTRACTING_RENAMING = {
        'NO_CONTRACT_LIKELY_SUB_CONTRACTED': 'no_subcontract_likely'
    }

    DEFENCE_VALUE_RENAMING = {
        'INITIAL_ESTIMATED_TOTAL_VALUE_CONTRACT': 'initial_estimated_total',
        'COSTS_RANGE_AND_CURRENCY_WITH_VAT_RATE': 'cost_range',
        'MORE_INFORMATION_IF_ANNUAL_MONTHLY': 'annual_monthly'
    }

    DEFENCE_AUTHORITY_RENAMING = {
        'NAME_ADDRESSES_CONTACT_CONTRACT_AWARD':
        'contact',
        'TYPE_AND_ACTIVITIES_OR_CONTRACTING_ENTITY_AND_PURCHASING_ON_BEHALF':
        'activitiesOrPurchasingInfo'
    }

    DEFENCE_ACTIVITIES_RENAMING = {
        'TYPE_AND_ACTIVITIES': 'typeAndActivities',
        'PURCHASING_ON_BEHALF': 'purchasingOnBehalf'
    }

    if 'fd_contract_award_defence' not in form_value:
        return

    defence = form_value['fd_contract_award_defence']
    rename_dict_keys(defence, DEFENCE_CONTRACT_RENAMING)
    make_list(defence, 'contract_award')
    if has_value(defence, 'contract_award'):
        for award in defence['contract_award']:
            rename_dict_keys(award, DEFENCE_CONTRACT_AWARD_RENAMING)
            if has_value(award, 'date') and has_value(award['date'], 'YEAR'):
                award['date'] = '{}-{}-{}'.format(
                    award['date']['YEAR'],
                    award['date']['MONTH'],
                    award['date']['DAY'],
                )
            if has_value(award, 'operator'):
                operator = award['operator']
                if has_value(operator,
                             'CONTACT_DATA_WITHOUT_RESPONSIBLE_NAME'):
                    contact_data = operator.pop(
                        'CONTACT_DATA_WITHOUT_RESPONSIBLE_NAME')
                    if has_value(contact_data, 'ORGANISATION') and has_value(
                            contact_data['ORGANISATION'], 'OFFICIALNAME'):
                        operator['name'] = contact_data['ORGANISATION'][
                            'OFFICIALNAME']
                    if has_value(contact_data, 'ADDRESS'):
                        operator['address'] = contact_data['ADDRESS']
                    if has_value(contact_data, 'TOWN'):
                        operator['town'] = contact_data['TOWN']
                    if has_value(contact_data, 'POSTAL_CODE'):
                        operator['postal_code'] = contact_data['POSTAL_CODE']
                    if has_value(contact_data, 'COUNTRY') and has_value(
                            contact_data['COUNTRY'], '@VALUE'):
                        operator['country'] = contact_data['COUNTRY']['@VALUE']

            if has_value(award, 'subcontracting'):
                subcontracting = award['subcontracting']
                rename_dict_keys(subcontracting,
                                 DEFENCE_SUBCONTRACTING_RENAMING)

            merge_paragraph_tag(award, 'title')

            if has_value(award, 'value'):
                value = award['value']
                rename_dict_keys(value, DEFENCE_VALUE_RENAMING)
                if has_value(value, 'initial_estimated_total'):
                    estimate = value['initial_estimated_total']
                    rename_dict_keys(estimate, {
                        '@CURRENCY': 'currency',
                        'EXCLUDING_VAT': 'excluding_vat'
                    })
                    if has_value(estimate, 'VALUE_COST'):
                        value_cost = estimate.pop('VALUE_COST')
                        if isinstance(value_cost, str):
                            estimate['amount'] = value_cost
                        else:
                            estimate['amount'] = value_cost['@FMTVAL']
                    if has_value(estimate, 'INCLUDING_VAT'):
                        including_vat = estimate.pop('INCLUDING_VAT')
                        if isinstance(including_vat['VAT_PRCT'], str):
                            estimate['including_vat'] = including_vat[
                                'VAT_PRCT']
                        else:
                            estimate['including_vat'] = including_vat[
                                'VAT_PRCT']['@FMTVAL']
                if has_value(value, 'cost_range'):
                    cost_range = value['cost_range']
                    rename_dict_keys(cost_range, {
                        '@CURRENCY': 'currency',
                        'EXCLUDING_VAT': 'excluding_vat'
                    })
                    if has_value(cost_range, 'VALUE_COST'):
                        value_cost = cost_range.pop('VALUE_COST')
                        if isinstance(value_cost, str):
                            cost_range['amount'] = value_cost
                        else:
                            cost_range['amount'] = value_cost['@FMTVAL']
                    if has_value(cost_range, 'INCLUDING_VAT'):
                        including_vat = cost_range.pop('INCLUDING_VAT')
                        if isinstance(including_vat['VAT_PRCT'], str):
                            cost_range['including_vat'] = including_vat[
                                'VAT_PRCT']
                        else:
                            cost_range['including_vat'] = including_vat[
                                'VAT_PRCT']['@FMTVAL']

    if has_value(defence, 'contracting_authority_info'):
        info = defence['contracting_authority_info']
        rename_dict_keys(info, DEFENCE_AUTHORITY_RENAMING)
        if 'activitiesOrPurchasingInfo' in info:
            activities = info['activitiesOrPurchasingInfo']
            rename_dict_keys(activities, DEFENCE_ACTIVITIES_RENAMING)
            if 'typeAndActivities' in activities:
                typeAndActivities = activities['typeAndActivities']
                if 'TYPE_OF_CONTRACTING_AUTHORITY' in typeAndActivities:
                    typeOf = typeAndActivities.pop(
                        'TYPE_OF_CONTRACTING_AUTHORITY')
                    if isinstance(typeOf, dict):
                        typeAndActivities['contractingAuthorityType'] = (
                            typeOf['@VALUE'])
                    else:
                        typeAndActivities['contractingAuthorityType'] = (
                            typeOf)
                if 'TYPE_OF_ACTIVITY' in typeAndActivities:
                    typeOf = typeAndActivities.pop('TYPE_OF_ACTIVITY')
                    if isinstance(typeOf, dict):
                        typeAndActivities['activityType'] = typeOf['@VALUE']
                    else:
                        typeAndActivities['activityType'] = typeOf
            if 'purchasingOnBehalf' in activities:
                del activities['purchasingOnBehalf']


def simplify_lefti(form_value):
    LEFTI_RENAMING = {
        'DEPOSIT_GUARANTEE_REQUIRED': 'deposit_guarantee_required',
        'MAIN_FINANCING_CONDITION': 'main_financing_condition',
        'ECONOMIC_FINANCIAL_INFO': 'economic_financial_info',
        'ECONOMIC_FINANCIAL_MIN_LEVEL': 'economic_financial_min_level',
        'PERFORMANCE_CONDITIONS': 'performance_conditions',
        'REFERENCE_TO_LAW': 'reference_to_law',
        'SUITABILITY': 'suitability',
        'TECHNICAL_PROFESSIONAL_INFO': 'technical_professional_info',
        'TECHNICAL_PROFESSIONAL_MIN_LEVEL': 'technical_professional_min_level'
    }

    if not has_value(form_value, 'lefti'):
        return

    lefti = form_value['lefti']
    rename_dict_keys(lefti, LEFTI_RENAMING)
    fields_to_merge = [
        'deposit_guarantee_required', 'main_financing_condition',
        'economic_financial_info', 'economic_financial_min_level',
        'performance_conditions', 'reference_to_law', 'suitability',
        'technical_professional_info', 'technical_professional_min_level'
    ]
    for key in fields_to_merge:
        merge_paragraph_tag(lefti, key)


def simplify_procedure(form_value):
    PROCEDURE_RENAMING = {
        'LANGUAGES': 'languages',
        'CONTRACT_COVERED_GPA': 'contract_covered_gpa',
        'EUACTION_USED': 'eu_action_used',
        'PT_OPEN': 'pt_open',
        'OPENING_CONDITION': 'opening_condition',
        'NO_CONTRACT_COVERED_GPA': 'no_contract_covered_gpa',
        'NOTICE_NUMBER_OJ': 'notice_number_oj',
        'DATE_RECEIPT_TENDERS': 'tenders_receipt_date',
        'TIME_RECEIPT_TENDERS': 'tenders_receipt_time',
        'DURATION_TENDER_VALID': 'tender_valid_duration'
    }

    PROCEDURE_OPENING_RENAMING = {
        'DATE_OPENING_TENDERS': 'date',
        'TIME_OPENING_TENDERS': 'time',
        'PLACE': 'place',
        'INFO_ADD': 'additional_info'
    }

    if not has_value(form_value, 'procedure'):
        return
    procedure = form_value['procedure']
    rename_dict_keys(procedure, PROCEDURE_RENAMING)
    if has_value(procedure, 'languages'):
        rename_dict_key(procedure['languages'], 'LANGUAGE', 'language')
        merge_value_tag(procedure['languages'], 'language')
    convert_duration(procedure, 'tender_valid_duration')
    if has_value(procedure, 'opening_condition'):
        opening = procedure['opening_condition']
        rename_dict_keys(opening, PROCEDURE_OPENING_RENAMING)
        merge_paragraph_tag(opening, 'place')
        merge_paragraph_tag(opening, 'additional_info')


def simplify_award_contracts(form_value, maps):
    AWARD_CONTRACTS_RENAMING = {
        '@ITEM': 'item_number',
        'LOT_NO': 'lot_number',
        'CONTRACT_NO': 'contract_number',
        'NO_AWARDED_CONTRACT': 'no_awarded_contract',
        'TITLE': 'title',
        'AWARDED_CONTRACT': 'awarded_contract'
    }

    AWARDED_CONTRACT_RENAMING = {
        'CONTRACTOR': 'contractor',
        'CONTRACTORS': 'contractors',
        'TENDERS': 'tenders',
        'VALUES': 'values',
        'DATE_CONCLUSION_CONTRACT': 'conclusion_date'
    }

    CONTRACTORS_RENAMING = {
        '@PUBLICATION': 'publication',
        'AWARDED_TO_GROUP': 'awarded_to_group',
        'NO_AWARDED_TO_GROUP': 'number_awarded_to_group',
        'CONTRACTOR': 'contractor'
    }

    CONTRACTOR_RENAMING = {
        'ADDRESS_CONTRACTOR': 'address',
        'NO_SME': 'sme_number',
        'SME': 'sme'
    }

    TENDERS_RENAMING = {
        '@PUBLICATION': 'publication',
        'NB_TENDERS_RECEIVED': 'num_tenders',
        'NB_TENDERS_RECEIVED_SME': 'num_tenders_sme',
        'NB_TENDERS_RECEIVED_OTHER_EU': 'num_tenders_other_eu',
        'NB_TENDERS_RECEIVED_NON_EU': 'num_tenders_non_eu',
        'NB_TENDERS_RECEIVED_EMEANS': 'num_tenders_emeans'
    }

    VALUES_RENAMING = {
        '@PUBLICATION': 'publication',
        'VAL_ESTIMATED_TOTAL': 'estimated_total',
        'VAL_TOTAL': 'total'
    }

    if not has_value(form_value, 'award_contracts'):
        return

    make_list(form_value, 'award_contracts')
    for contract in form_value['award_contracts']:
        rename_dict_keys(contract, AWARD_CONTRACTS_RENAMING)
        if has_value(contract, 'awarded_contract'):
            award = contract['awarded_contract']
            rename_dict_keys(award, AWARDED_CONTRACT_RENAMING)
            # It is possible that the 'contractors' wrapper around 'contractor'
            # has been left off, and so we handle both possibilities.
            contractor_list = None
            if has_value(award, 'contractors'):
                contractors = award['contractors']
                rename_dict_keys(contractors, CONTRACTORS_RENAMING)
                if has_value(contractors, 'contractor'):
                    make_list(contractors, 'contractor')
                    contractor_list = contractors['contractor']
            elif has_value(award, 'contractor'):
                make_list(award, 'contractor')
                contractor_list = award['contractor']
            if contractor_list != None:
                for contractor in contractor_list:
                    rename_dict_keys(contractor, CONTRACTOR_RENAMING)
                    convert_address(contractor, 'address', maps)
                    if has_value(contractor, 'address'):
                        address = contractor['address']
                        if has_value(address, 'official_name'):
                            official_name = address['official_name']

            if has_value(award, 'tenders'):
                tenders = award['tenders']
                rename_dict_keys(tenders, TENDERS_RENAMING)

            if has_value(award, 'values'):
                values = award['values']
                # TODO(Jack Poulson): Rename VAL_RANGE_TOTAL to
                # val_range_total
                rename_dict_keys(values, VALUES_RENAMING)
                for key in ['estimated_total', 'total']:
                    convert_monetary(values, key)

        merge_paragraph_tag(contract, 'title')

    # Remove the empty contracts from the list.
    # TODO(Jack Poulson): Determine why some null contracts still survive.
    form_value['award_contracts'] = clean_nones(form_value['award_contracts'])


def simplify_complementary_info(form_value, maps):
    INFO_RENAMING = {
        'ADDRESS_REVIEW_BODY': 'address_review_body',
        'ADDRESS_REVIEW_INFO': 'address_review_info',
        'ADDRESS_MEDIATION_BODY': 'address_mediation_body',
        'EINVOICING': 'einvoicing',
        'EORDERING': 'eordering',
        'INFO_ADD': 'additional_info',
        'REVIEW_PROCEDURE': 'review_procedure',
        'NO_RECURRENT_PROCUREMENT': 'no_recurrent_procurement',
        'DATE_DISPATCH_NOTICE': 'dispatch_notice_date'
    }

    if not has_value(form_value, 'complementary_info'):
        return

    info = form_value['complementary_info']
    rename_dict_keys(info, INFO_RENAMING)

    fields_to_merge = ['additional_info', 'review_procedure']
    for key in fields_to_merge:
        merge_paragraph_tag(info, key)

    convert_address(info, 'address_review_body', maps)
    convert_address(info, 'address_review_info', maps)
    convert_address(info, 'address_mediation_body', maps)


def simplify_contracting_body(form_value, maps):
    CONTRACTING_BODY_RENAMING = {
        'ADDRESS_CONTRACTING_BODY': 'address',
        'ADDRESS_FURTHER_INFO': 'address_further_info',
        'ADDRESS_PARTICIPATION': 'address_participation',
        'ADDRESS_FURTHER_INFO_IDEM': 'address_further_info_idem',
        'ADDRESS_PARTICIPATION_IDEM': 'address_participation_idem',
        'CA_ACTIVITY': 'ca_activity',
        'CA_ACTIVITY_OTHER': 'ca_activity_other',
        'CA_TYPE': 'ca_type',
        'CE_ACTIVITY': 'ce_activity',
        'CE_ACTIVITY_OTHER': 'ce_activity_other',
        'DOCUMENT_FULL': 'document_full',
        'DOCUMENT_RESTRICTED': 'document_restricted',
        'URL_DOCUMENT': 'document_url',
        'URL_PARTICIPATION': 'participation_url'
    }

    if not has_value(form_value, 'contracting_body'):
        return
    contracting_body = form_value['contracting_body']

    rename_dict_keys(contracting_body, CONTRACTING_BODY_RENAMING)

    convert_address(contracting_body, 'address', maps)

    if has_value(contracting_body, 'address_further_info'):
        info = contracting_body['address_further_info']
        rename_dict_key(info, 'OFFICIALNAME', 'official_name')

    if has_value(contracting_body, 'address_participation'):
        info = contracting_body['address_participation']
        rename_dict_key(info, 'OFFICIALNAME', 'official_name')

    fields_to_value_merge = ['ca_activity', 'ca_type', 'ce_activity']
    for key in fields_to_value_merge:
        merge_value_tag(contracting_body, key)


def simplify_modifications_contract(form_value, maps):
    CONTRACT_RENAMING = {
        'DESCRIPTION_PROCUREMENT': 'description_procurement',
        'INFO_MODIFICATIONS': 'info_modifications'
    }

    PROCUREMENT_RENAMING = {
        'MAIN_SITE': 'main_site',
        'SHORT_DESCR': 'short_descr'
    }

    if not has_value(form_value, 'modifications_contract'):
        return

    contract = form_value['modifications_contract']

    rename_dict_keys(contract, CONTRACT_RENAMING)

    if has_value(contract, 'description_procurement'):
        procurement = contract['description_procurement']
        rename_dict_keys(procurement, PROCUREMENT_RENAMING)
        replace_type_list_with_codes(procurement, 'n2021:NUTS', 'nuts',
                                     maps['NUTS'])
        fields_to_merge = ['main_site', 'short_descr']
        for key in fields_to_merge:
            merge_paragraph_tag(procurement, key)

    if has_value(contract, 'info_modifications'):
        modifications = contract['info_modifications']
        rename_dict_key(modifications, 'SHORT_DESCR', 'short_descr')
        merge_paragraph_tag(modifications, 'short_descr')


def simplify_object_contract(form_value, maps):
    CONTRACT_RENAMING = {
        'CPV_MAIN': 'cpv_main',
        'NO_LOT_DIVISION': 'num_lot_divisions',
        'LOT_DIVISION': 'lot_division',
        'OBJECT_DESCR': 'object_descr',
        'REFERENCE_NUMBER': 'reference_number',
        'SHORT_DESCR': 'short_descr',
        'TITLE': 'title',
        'TYPE_CONTRACT': 'type_contract',
        'VAL_ESTIMATED_TOTAL': 'estimated_total',
        'VAL_TOTAL': 'total'
    }

    LOT_DIVISION_RENAMING = {
        'LOT_MAX_NUMBER': 'lot_max_number',
        'LOT_MAX_ONE_TENDERER': 'lot_max_one_tenderer'
    }

    DESCR_RENAMING = {
        '@ITEM': 'item_number',
        'LOT_NO': 'lot_number',
        'AC': 'ac',
        'CPV_ADDITIONAL': 'cpv_additional',
        'CRITERIA_CANDIDATE': 'criteria_candidate',
        'EU_PROGR_RELATED': 'eu_progr_related',
        'INFO_ADD': 'additional_info',
        'MAIN_SITE': 'main_site',
        'NB_MIN_LIMIT_CANDIDATE': 'nb_min_limit_candidate',
        'NB_MAX_LIMIT_CANDIDATE': 'nb_max_limit_candidate',
        'NO_ACCEPTED_VARIANTS': 'no_accepted_variants',
        'NO_EU_PROGR_RELATED': 'no_eu_progr_related',
        'NO_OPTIONS': 'no_options',
        'NO_RENEWAL': 'no_renewal',
        'OPTIONS': 'options',
        'OPTIONS_DESCR': 'options_descr',
        'SHORT_DESCR': 'short_descr',
        'TITLE': 'title',
        'DURATION': 'duration',
        'DATE_END': 'date_end',
        'DATE_START': 'date_start',
        'VAL_OBJECT': 'val_object'
    }

    AC_RENAMING = {
        'AC_PRICE': 'price',
        'AC_PROCUREMENT_DOC': 'procurement_doc',
        'AC_QUALITY': 'quality'
    }

    AC_QUALITY_RENAMING = {
        'AC_CRITERION': 'criterion',
        'AC_WEIGHTING': 'weighting'
    }

    if not has_value(form_value, 'object_contract'):
        return

    contract = form_value['object_contract']

    rename_dict_keys(contract, CONTRACT_RENAMING)

    if has_value(contract, 'cpv_main'):
        cpv = contract['cpv_main']
        if 'CPV_CODE' in cpv and '@CODE' in cpv['CPV_CODE']:
            cpv['cpv_code'] = cpv.pop('CPV_CODE')['@CODE']

    if has_value(contract, 'lot_division'):
        rename_dict_keys(contract['lot_division'], LOT_DIVISION_RENAMING)

    if has_value(contract, 'object_descr'):
        make_list(contract, 'object_descr')
        for descr_value in contract['object_descr']:
            rename_dict_keys(descr_value, DESCR_RENAMING)
            if has_value(descr_value, 'ac'):
                ac = descr_value['ac']
                rename_dict_keys(ac, AC_RENAMING)
                if has_value(ac, 'price'):
                    rename_dict_keys(ac['price'],
                                     {'AC_WEIGHTING': 'weighting'})
                if has_value(ac, 'quality'):
                    rename_dict_keys(ac['quality'], AC_QUALITY_RENAMING)
            if has_value(descr_value, 'cpv_additional'):
                make_list(descr_value, 'cpv_additional')
                for cpv in descr_value['cpv_additional']:
                    if has_value(cpv, 'CPV_CODE') and has_value(
                            cpv['CPV_CODE'], '@CODE'):
                        cpv['cpv_code'] = cpv.pop('CPV_CODE')['@CODE']
            convert_duration(descr_value, 'duration')
            convert_monetary(descr_value, 'val_object')
            fields_to_merge = [
                'additional_info', 'eu_progr_related', 'main_site',
                'options_descr', 'short_descr', 'title'
            ]
            for key in fields_to_merge:
                merge_paragraph_tag(descr_value, key)
            replace_type_list_with_codes(descr_value, 'n2021:NUTS', 'nuts',
                                         maps['NUTS'])

    convert_monetary(contract, 'estimated_total')
    convert_monetary(contract, 'total')

    fields_to_merge = ['short_descr', 'title']
    for key in fields_to_merge:
        merge_paragraph_tag(contract, key)

    if has_value(contract, 'type_contract'):
        contract['type_contract'] = contract['type_contract']['@CTYPE']


# The preferred_language_code is only used to preference which of the available
# languages is selected when English is not available.
def simplify_form(doc, maps, preferred_language_code):
    FORM_RENAMING = {
        '@CATEGORY': 'category',
        '@FORM': 'form_type',
        '@VERSION': 'version',
        '@LG': 'language',
        'AWARD_CONTRACT': 'award_contracts',
        'COMPLEMENTARY_INFO': 'complementary_info',
        'CONTRACTING_BODY': 'contracting_body',
        'FD_CONTRACT_AWARD_DEFENCE': 'fd_contract_award_defence',
        'LEFTI': 'lefti',
        'LEGAL_BASIS': 'legal_basis',
        'LEGAL_BASIS_OTHER': 'legal_basis_other',
        'MODIFICATIONS_CONTRACT': 'modifications_contract',
        'OBJECT_CONTRACT': 'object_contract',
        'PROCEDURE': 'procedure'
    }

    if not has_value(doc, 'FORM_SECTION'):
        return
    form = doc.pop('FORM_SECTION')

    if 'NOTICE_UUID' in form:
        del form['NOTICE_UUID']
    if len(form.keys()) == 0:
        print('Zero form keys for doc ID {}'.format(doc['doc_id']))
        return
    if len(form.keys()) > 1:
        if '@xmlns' in form:
            del form['@xmlns']

    form_key = list(form.keys())[0]
    make_list(form, form_key)
    form_values = form[form_key]

    # Get the list of (lower-case) language codes for the form values.
    form_language_codes = []
    for form_value in form_values:
        form_language_codes.append(form_value['@LG'].lower())

    if 'en' in form_language_codes:
        index = form_language_codes.index('en')
    elif preferred_language_code in form_language_codes:
        index = form_language_codes.index(preferred_language_code)
    else:
        index = 0
    form_language_code = form_language_codes[index]
    form_value = form_values[index]

    form_value['form_key'] = form_key
    rename_dict_keys(form_value, FORM_RENAMING)

    simplify_defence_contract(form_value)
    simplify_lefti(form_value)
    simplify_procedure(form_value)

    merge_value_tag(form_value, 'legal_basis')
    merge_paragraph_tag(form_value, 'legal_basis_other')

    simplify_award_contracts(form_value, maps)
    simplify_complementary_info(form_value, maps)
    simplify_contracting_body(form_value, maps)
    simplify_modifications_contract(form_value, maps)
    simplify_object_contract(form_value, maps)

    set_from_path(doc['data'], [form_language_code, 'form'], form_value)


def simplify_translation(doc, maps):
    if not has_value(doc, 'TRANSLATION_SECTION'):
        return
    translation = doc.pop('TRANSLATION_SECTION')

    #
    # Move the data originally located at:
    #
    #   TRANSLATION_SECTION -> ML_AA_NAMES -> AA_NAME[] -> [@LG, #text]
    #
    # into
    #
    #   data -> ${language} -> translation -> names[]
    #
    # where the language is grabbed from the original @LG leaf value and each
    # name is extracted from the corresponding #text leaf.
    #
    if has_value(translation, 'ML_AA_NAMES'):
        names = translation.pop('ML_AA_NAMES')
        if has_value(names, 'AA_NAME'):
            make_list(names, 'AA_NAME')
            kept_name_idx = 0
            for name_idx, name in enumerate(names['AA_NAME']):
                language = name['@LG'].lower()
                if '#text' in name:
                    name_text = name['#text']
                else:
                    print('WARNING: Missing #text field of {}'.format(name))
                    name_text = None
                if not name_text:
                    continue
                set_from_path(
                    doc['data'],
                    [language, 'translation', 'names', kept_name_idx],
                    name_text)
                kept_name_idx += 1

    #
    # Move the data originally located at:
    #
    #   ML_TITLES -> ML_TI_DOC[] -> [@LG, TI_CY, TI_TOWN, TI_TEXT]
    #
    # into
    #
    #   data -> $(language) -> translation -> title -> [country, town, text]
    #
    # where the language is extracted from the @LG leaf value and the country,
    # town, and text are respectively extracted from the other three fields.
    #
    if has_value(translation, 'ML_TITLES'):
        titles = translation.pop('ML_TITLES')
        title = None

        if has_value(titles, 'ML_TI_DOC'):
            ti_doc = titles['ML_TI_DOC']
            if isinstance(ti_doc, list):
                lang_state = {'score': -1, 'item': None}

                for lang in ti_doc:
                    if not has_value(LANGUAGE_PREFERENCES, lang['@LG']):
                        continue
                    score = LANGUAGE_PREFERENCES[lang['@LG']]
                    if score > lang_state['score']:
                        lang_state['score'] = score
                        lang_state['item'] = lang
                    elif score < 0:
                        lang_state['score'] = 0
                        lang_state['item'] = lang

                if lang_state['item']:
                    item = lang_state['item']
                    title = {
                        'language': item['@LG'],
                        'country': item['TI_CY'],
                        'town': item['TI_TOWN']
                    }
                    if isinstance(item['TI_TEXT'], dict):
                        text_para = get_paragraph(item['TI_TEXT']['P'])
                    elif isinstance(item['TI_TEXT'], str):
                        text_para = get_paragraph(item['TI_TEXT'])
                    else:
                        text_para = None
                    if text_para:
                        title['text'] = text_para

        if title:
            language = title.pop('language').lower()
            set_from_path(doc['data'], [language, 'translation', 'title'],
                          title)


def remove_nulls(doc, verbose=False):
    # Remove null entries from:
    #
    #   data -> form -> award_contracts[]
    #
    # and from:
    #
    #   data -> translation -> names[]
    #
    for lang_code, data in doc['data'].items():
        if has_value(data, 'form'):
            form = data['form']
            if has_value(form, 'award_contracts'):
                old_len = len(form['award_contracts'])
                form['award_contracts'] = list(
                    filter(None, form['award_contracts']))
                new_len = len(form['award_contracts'])
                if verbose and new_len != old_len:
                    print('  Removed {} contracts from doc'.format(old_len -
                                                                   new_len))
        if has_value(data, 'translation'):
            translation = data['translation']
            if has_value(translation, 'names'):
                old_len = len(translation['names'])
                translation['names'] = list(filter(None, translation['names']))
                new_len = len(translation['names'])
                if verbose and new_len != old_len:
                    print('  Removed {} names from doc'.format(old_len -
                                                               new_len))


def get_contracting_authorities(doc):
    '''Returns the list of contracting authority from a processed record.'''
    # We combine the following into a canonicalized list of unique items:
    #
    #  data -> translation -> names[]
    #
    name_set = set()
    for language_code, data in doc['data'].items():
        if not has_value(data, 'translation'):
            continue
        translation = data['translation']
        if has_value(translation, 'names'):
            for name in translation['names']:
                if name:
                    name_set.add(canonicalize(name))

    return sorted(list(name_set))


def get_contractors(doc):
    '''Returns the list of contractors from a processed procurement record.'''
    # We combine the following into a canonicalized list of unique items:
    #
    #  data -> form -> award_contracts[] -> awarded_contract -> contractors ->
    #    contractor[] -> address -> official_name
    #
    # We also account for the possibility of the 'contractors' wrapper being
    # missing.
    #
    name_set = set()
    for language_code, data in doc['data'].items():
        if not has_value(data, 'form'):
            continue
        form = data['form']
        if not has_value(form, 'award_contracts'):
            continue
        for contract in form['award_contracts']:
            if not contract:
                print('WARNING: encountered null contract')
                continue
            if not has_value(contract, 'awarded_contract'):
                continue
            award = contract['awarded_contract']
            contractor_list = None
            if has_value(award, 'contractors'):
                contractors = award['contractors']
                if has_value(contractors, 'contractor'):
                    contractor_list = contractors['contractor']
            elif has_value(award, 'contractor'):
                contractor_list = award['contractor']
            if contractor_list != None:
                for contractor in contractor_list:
                    if not has_value(contractor, 'address'):
                        continue
                    address = contractor['address']
                    if has_value(address, 'official_name'):
                        name_set.add(canonicalize(address['official_name']))

    return sorted(list(name_set))


def get_defence_operators(doc):
    '''Returns the list of defence contract award operators.'''
    # We combine the following into a canonicalized list of unique items:
    #
    #   data -> form -> fd_contract_award_defence -> contract_award[] ->
    #     operator -> name
    #
    name_set = set()
    for language_code, data in doc['data'].items():
        if not has_value(data, 'form'):
            continue
        form = data['form']
        if not has_value(form, 'fd_contract_award_defence'):
            continue
        defence = form['fd_contract_award_defence']
        if not has_value(defence, 'contract_award'):
            continue
        for contract_award in defence['contract_award']:
            if not has_value(contract_award, 'operator'):
                continue
            operator = contract_award['operator']
            if has_value(operator, 'name'):
                name_set.add(canonicalize(operator['name']))

    return sorted(list(name_set))


def get_action_date_range(doc):

    def incorporate_date(date, dates):
        if (not dates['first']) or (date and date < dates['first']):
            dates['first'] = date
        if (not dates['last']) or (date and date > dates['last']):
            dates['last'] = date

    dates = {'first': None, 'last': None}

    # Incorporate the dispatch notice dates.
    #
    #   data -> form -> complementary_info -> dispatch_notice_date
    #
    for lang_code, data in doc['data'].items():
        if 'form' not in data:
            continue
        form = data['form']
        if 'complementary_info' not in form:
            continue
        info = form['complementary_info']
        if 'dispatch_notice_date' in info:
            incorporate_date(info['dispatch_notice_date'], dates)

    # Incorporate the defence contract award dates.
    #
    #   data -> form -> fd_contract_award_defence -> contract_award[] -> date
    #
    for lang_code, data in doc['data'].items():
        if 'form' not in data:
            continue
        form = data['form']
        if 'fd_contract_award_defence' not in form:
            continue
        defence = form['fd_contract_award_defence']
        if 'contract_award' not in defence:
            continue
        for award in defence['contract_award']:
            if award is not None and 'date' in award:
                incorporate_date(award['date'], dates)

    return dates


def preprocess_xml(doc,
                   processed_docs,
                   maps,
                   countries=sorted(list(COUNTRIES.keys())),
                   skipped_countries=None):
    # The top-level fields we immediately drop.
    FIELDS_TO_DROP = [
        '@xmlns:xlink', '@xmlns:xsi', '@xmlns', '@xmlns:n2021',
        '@xmlns:schemaLocation', '@xsi:schemaLocation', '@VERSION', '@EDITION',
        'LINKS_SECTION', 'TECHNICAL_SECTION'
    ]

    doc = doc['TED_EXPORT']

    for field in FIELDS_TO_DROP:
        if field in doc:
            del doc[field]

    rename_dict_key(doc, '@DOC_ID', 'doc_id')

    doc['is_legacy'] = True
    doc['data'] = {}

    simplify_coded_data(doc, maps)
    country = get_country(doc)
    if country not in countries:
        if skipped_countries is not None:
            if country not in skipped_countries:
                skipped_countries[country] = 0
            skipped_countries[country] += 1
        return
    if country not in COUNTRIES:
        print('ERROR: {} not in COUNTRIES'.format(country))
    language_code = COUNTRIES[country]['language']

    simplify_translation(doc, maps)
    simplify_form(doc, maps, language_code)

    # Add the detected entity names.
    entities_set = set()
    for entity in get_contracting_authorities(doc):
        entities_set.add(entity)
    for entity in get_contractors(doc):
        entities_set.add(entity)
    for entity in get_defence_operators(doc):
        entities_set.add(entity)
    doc['entities'] = sorted(list(entities_set))

    # Add the detected action date range.
    dates = get_action_date_range(doc)
    doc['first_action_date'] = dates['first']
    doc['last_action_date'] = dates['last']

    remove_nulls(doc, verbose=False)

    if country not in processed_docs:
        processed_docs[country] = []
    processed_docs[country].append(doc)


def get_descriptions(doc, language_code='en', join_str='  '):
    '''Returns the list of descriptive phrases in a particular language.'''
    descriptions = set()
    if not has_value(doc['data'], language_code):
        return descriptions
    data = doc['data'][language_code]
    if has_value(data, 'translation'):
        translation = data['translation']
        if 'title' in translation:
            title = translation['title']
            if 'text' in title:
                descriptions.add(join_str.join(title['text']))
    if has_value(data, 'form'):
        form = data['form']
        if has_value(form, 'award_contracts'):
            for contract in form['award_contracts']:
                if 'title' in contract:
                    descriptions.add(join_str.join(contract['title']))
        if has_value(form, 'object_contract'):
            obj = form['object_contract']
            if has_value(obj, 'title'):
                descriptions.add(join_str.join(obj['title']))
            if has_value(obj, 'short_descr'):
                descriptions.add(join_str.join(obj['short_descr']))
            if has_value(obj, 'object_descr'):
                for descr in obj['object_descr']:
                    if has_value(descr, 'title'):
                        descriptions.add(join_str.join(descr['title']))
                    if has_value(descr, 'short_descr'):
                        descriptions.add(join_str.join(descr['short_descr']))
                    if has_value(descr, 'additional_info'):
                        descriptions.add(
                            join_str.join(descr['additional_info']))
        if has_value(form, 'complementary_info'):
            complementary_info = form['complementary_info']
            if has_value(complementary_info, 'additional_info'):
                joined_info = join_str.join(
                    complementary_info['additional_info'])
                if joined_info:
                    descriptions.add(joined_info)
        if has_value(form, 'fd_contract_award_defence'):
            defence = form['fd_contract_award_defence']
            if has_value(defence, 'contract_award'):
                for award in defence['contract_award']:
                    if has_value(award, 'title'):
                        descriptions.add(join_str.join(award['title']))

    return sorted(list(descriptions))


def queue_translation_data(doc,
                           doc_index,
                           state,
                           translation_map,
                           country_data,
                           use_translation_cache=True):
    state['doc_id'] = doc['doc_id']

    for language_code in doc['data']:
        if language_code == 'en':
            continue
        if 'form' not in doc['data'][language_code]:
            continue
        form = doc['data'][language_code]['form']
        input_path = [doc_index, 'data', language_code, 'form']

        if language_code not in TRANSLATABLE_LANGUAGE_CODES:
            if language_code not in state['num_untranslated']:
                state['num_untranslated'][language_code] = 0
            state['num_untranslated'][language_code] += 1
            continue

        # Translate:
        #  - "form": {
        #      "complementary_info": {
        #        "additional_info",
        #        "review_procedure"
        #      },
        #      "award_contracts": [{"title"}],
        #      "object_contract": {
        #        "title",
        #        "short_descr",
        #        "object_descr": [
        #          {
        #            "title",
        #            "short_descr",
        #            "additional_info"
        #          }
        #        ]
        #      },
        #      "fd_contract_award_defence": {
        #        "contract_award": [
        #          {
        #            "title"
        #          }
        #        ]
        #      }
        #    }

        def append_input_helper(main_obj, key, path):
            append_input_if_exists(main_obj,
                                   key,
                                   path,
                                   language_code,
                                   state,
                                   translation_map,
                                   country_data,
                                   use_translation_cache=use_translation_cache)

        if 'complementary_info' in form:
            path = input_path + ['complementary_info']
            complementary_info = form['complementary_info']
            append_input_helper(complementary_info, 'additional_info', path)
            append_input_helper(complementary_info, 'review_procedure', path)
        if 'award_contracts' in form:
            for index, award_contract in enumerate(form['award_contracts']):
                path = input_path + ['award_contracts', index]
                append_input_helper(award_contract, 'title', path)
        if 'object_contract' in form:
            path = input_path + ['object_contract']
            object_contract = form['object_contract']
            append_input_helper(object_contract, 'title', path)
            append_input_helper(object_contract, 'short_descr', path)
            if 'object_descr' in object_contract:
                if not isinstance(object_contract['object_descr'], list):
                    print('object_descr was not a list: {}'.format(
                        object_contract['object_descr']))
                for index, object_descr in enumerate(
                        object_contract['object_descr']):
                    inner_path = path + ['object_descr', index]
                    append_input_helper(object_descr, 'title', inner_path)
                    append_input_helper(object_descr, 'short_descr',
                                        inner_path)
                    append_input_helper(object_descr, 'additional_info',
                                        inner_path)
        if 'fd_contract_award_defence' in form:
            path = input_path + ['fd_contract_award_defence']
            defence = form['fd_contract_award_defence']
            if 'contract_award' in defence:
                for index, award in enumerate(defence['contract_award']):
                    inner_path = path + ['contract_award', index]
                    append_input_helper(award, 'title', inner_path)
