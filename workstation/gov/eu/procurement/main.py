#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
#
# Note: TED added a new format on May 23, 2023 though continued publishing
# a subset of contract (award) notices in the legacy format.
#
# Question: Is it possible to translate from the new to the old format?
#
#
# One can directly query full-text search across EU contracts at:
#
#   https://ted.europa.eu/TED/search/expertSearch.do
#
# e.g., by entering the query
#
#   FT=[palantir]
#
# or
#
#   FT=["palantir technologies"]
#
# Alternatively, one can download tar.gz of XML files for each day's contracts
# (one file per contract) at:
#
#   ftp://ted.europa.eu/daily-packages/
#
# Alternatively, we can make use of the v2.0 API at
#
#   https://ted.europa.eu/api/v2.0/notices/search?q=FT%3D%5Bpalantir%5D
#
# by encoding the query string '=' signs as %3D, the '[' as %5B, and the ']' as
# %5D.
#
import datetime
import glob
import json
import os
import pandas as pd
import shutil
import traceback
import xmltodict

from tqdm import tqdm

import gov.eu.procurement.legacy as legacy
import gov.eu.procurement.modern as modern
import util.dates
import util.dict_path
import util.opus
import util.postgres
import util.zip

from gov.eu.procurement.util import COUNTRIES
from util.dict_path import get_from_path, has_path, set_from_path
from util.entities import normalization_map_helper, normalize

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
TENDERS_DIR = os.path.join(DATA_DIR, 'eu_tenders')
STAGING_DIR = os.path.join(WORKSTATION_DIR, 'staging')
TENDERS_STAGING_DIR = os.path.join(STAGING_DIR, 'eu_tenders')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

NON_ENGLISH_LANGUAGES = {
    '1a': ['sq', 'sr'],
    'al': ['sq'],
    'am': ['hy'],
    'ar': ['es'],
    'at': ['de', 'it'],
    'ba': ['bs'],
    'bd': ['bn'],
    'be': ['fr'],
    'bg': ['bg'],
    'bj': ['fr'],
    'bo': ['es'],
    'by': ['ru'],
    'cg': ['fr'],
    'ch': ['de'],
    'ci': ['fr'],
    'cm': ['fr'],
    'cn': ['cn'],
    'co': ['es'],
    'cy': ['el', 'tr'],
    'cz': ['cs'],
    'de': ['de', 'pl'],
    'dk': ['da'],
    'dz': ['ar'],
    'ec': ['es'],
    'ee': ['et'],
    'eg': ['ar'],
    'es': ['es'],
    'et': ['am'],
    'fi': ['fi'],
    'fr': ['fr'],
    'ge': ['ka'],
    'gp': ['fr'],
    'gr': ['el'],
    'hr': ['hr'],
    'ht': ['fr'],
    'hu': ['hu'],
    'ie': ['ga'],
    'il': ['he'],
    'in': ['hi'],
    'is': ['is'],
    'it': ['it'],
    'ke': [],
    'kg': ['ru'],
    'kn': [],
    'kz': ['kk'],
    'la': ['lo'],
    'lb': ['ar'],
    'li': ['de'],
    'lt': ['lt'],
    'lu': ['de', 'fr', 'lb'],
    'lv': ['lv'],
    'ma': ['ar'],
    'md': ['ro'],
    'me': ['cnr'],
    'mg': ['fr'],
    'mk': [],
    'ml': ['fr'],
    'mt': [],
    'mw': [],
    'ni': ['es'],
    'nl': ['nl'],
    'no': [],
    'np': ['ne'],
    'pl': ['pl'],
    'ps': ['ar'],
    'pt': ['pt'],
    'py': ['es'],
    're': ['fr'],
    'ro': ['ro'],
    'rs': ['ro'],
    'ru': ['ru'],
    'se': ['sv'],
    'si': ['sl'],
    'sk': ['sk'],
    'sl': [],
    'sn': ['fr'],
    'td': ['fr'],
    'th': ['th'],
    'tj': ['tg'],
    'tn': ['aeb'],
    'tr': ['tr'],
    'tz': [],
    'ua': ['uk'],
    'ug': [],
    'uk': [],
    'us': [],
    'yt': ['fr'],
    'zm': []
}

TABLE_TYPES = {
    'doc_id': 'VARCHAR(15)',
    'is_legacy': 'BOOLEAN',
    'data': 'JSONB',
    'entities': 'JSONB',
    'first_action_date': 'DATE',
    'last_action_date': 'DATE'
}
FORCE_NULL_COLUMNS = [
    'data', 'entities', 'first_action_date', 'last_action_date'
]

JSON_COLUMNS = ['data', 'entities']
UNIQUE_COLUMNS = ['doc_id']

MAP_FILES = {
    'AA_AUTHORITY_TYPE': 'aa_authority_type.json',
    'AC_AWARD_CRIT': 'ac_award_crit.json',
    'MA_MAIN_ACTIVITIES': 'ma_main_activities.json',
    'NC_CONTRACT_NATURE': 'nc_contract_nature.json',
    'NUTS': 'nuts.json',
    'PR_PROC': 'pr_proc.json',
    'RP_REGULATION': 'rp_regulation.json',
    'TD_DOCUMENT_TYPE': 'td_document_type.json',
    'TY_TYPE_BID': 'ty_type_bid.json'
}


def load_conversion_maps():
    '''Load the conversion maps for preprocessing EU tenders.'''
    maps = {}
    for key in MAP_FILES:
        filename = '{}/maps/{}'.format(TENDERS_DIR, MAP_FILES[key])
        maps[key] = json.load(open(filename))
    return maps


def export_conversion_maps(maps):
    '''Write out the conversion maps used for preprocessing EU tenders.'''
    for key in MAP_FILES:
        filename = '{}/maps/{}'.format(TENDERS_DIR, MAP_FILES[key])
        with open(filename, 'w') as outfile:
            json.dump(maps[key], outfile, indent=1)


def get_tenders(url, unpack_dir=STAGING_DIR, verbose=False):
    '''Downloads and unpacks a tar.gz of TED procurement records.'''
    if verbose:
        print('Downloading and unpacking {}'.format(url))
    try:
        util.zip.download_and_unpack(url, unpack_dir, compression_format='TGZ')
        return True
    except Exception as e:
        print('ERROR: Could not process {}'.format(url))
        print(e)
        return False
    return True


def get_month_of_tenders(year, month, unpack_dir=STAGING_DIR, verbose=False):
    '''Downloads and unpacks a single month of TED procurement records.'''
    if isinstance(year, int):
        year = str(year).zfill(4)
    if isinstance(month, int):
        month = str(month).zfill(1)
    url = 'https://ted.europa.eu/packages/monthly/{}-{}'.format(year, month)
    return get_tenders(url, unpack_dir=unpack_dir, verbose=verbose)


def get_year_of_tenders(year, unpack_dir=STAGING_DIR, verbose=False):
    '''Downloads and unpacks a year of TED procurement records.'''
    with tqdm(total=12) as pbar:
        for month in range(1, 13):
            get_month_of_tenders(year,
                                 month,
                                 unpack_dir=unpack_dir,
                                 verbose=verbose)
            pbar.update(1)


def country_json_filename(country_code, tag=None, directory=TENDERS_DIR):
    if tag:
        filename = '{}/{}-{}.json'.format(directory, country_code, tag)
    else:
        filename = '{}/{}.json'.format(directory, country_code)
    return filename


def country_csv_filename(country_code, tag=None):
    if tag:
        filename = '{}/{}_eu_tender-{}.csv'.format(EXPORT_DIR, country_code,
                                                   tag)
    else:
        filename = '{}/{}_eu_tender.csv'.format(EXPORT_DIR, country_code)
    return filename


def preprocess_xml(infile,
                   processed_docs,
                   maps,
                   countries=sorted(list(COUNTRIES.keys())),
                   skipped_countries=None):
    doc = xmltodict.parse(infile.read())

    # DEBUG
    #print('DEBUG output to test_eu_tenders.json')
    #with open('test_eu_tenders.json', 'w') as outfile:
    #    json.dump(doc, outfile, indent=2)

    if 'TED_EXPORT' in doc:
        legacy.preprocess_xml(doc, processed_docs, maps, countries,
                              skipped_countries)
    else:
        modern.preprocess_xml(doc, processed_docs, maps, countries,
                              skipped_countries)


def append_input_if_exists(item,
                           key,
                           base_path,
                           language_code,
                           state,
                           translation_map,
                           country_data,
                           use_translation_cache=True):
    '''A helper function to store a string to translate (and its path).'''

    def input_path_to_output_path(input_path):
        output_path = input_path.copy()
        output_path[2] = 'en'
        return output_path

    try:
        doc_id = state['doc_id']

        def has_path_helper(p):
            return doc_id in translation_map and has_path(
                translation_map[doc_id], p[1:])

        def get_from_path_helper(p):
            return get_from_path(translation_map[doc_id], p[1:])

        def set_from_path_helper(p):
            set_from_path(country_data, p, get_from_path_helper(p))

        if language_code not in state['inputs']:
            state['inputs'][language_code] = []
            state['input_paths'][language_code] = []
            state['output_paths'][language_code] = []

        if key in item:
            obj = item[key]
            if isinstance(obj, list):
                for member_idx, member in enumerate(obj):
                    in_path = base_path + [key, member_idx]
                    out_path = input_path_to_output_path(in_path)
                    if use_translation_cache and has_path(
                            country_data, out_path):
                        state['num_cached_translations'] += 1
                    elif use_translation_cache and has_path_helper(out_path):
                        set_from_path_helper(out_path)
                        state['num_manual_cached_translations'] += 1
                    else:
                        state['inputs'][language_code].append(member)
                        state['input_paths'][language_code].append(in_path)
                        state['output_paths'][language_code].append(out_path)
                    state['num_input_characters'] += len(member)
            elif isinstance(obj, str):
                in_path = base_path + [key]
                out_path = input_path_to_output_path(in_path)
                if use_translation_cache and has_path(country_data, out_path):
                    state['num_cached_translations'] += 1
                elif use_translation_cache and has_path_helper(out_path):
                    set_from_path_helper(out_path)
                    state['num_manual_cached_translations'] += 1
                else:
                    state['inputs'][language_code].append(obj)
                    state['input_paths'][language_code].append(in_path)
                    state['output_paths'][language_code].append(out_path)
                state['num_input_characters'] += len(obj)
    except Exception as e:
        print('Failed to append from {} in {}'.format(key, item))
        print(e)


def translate_country_data(
        country,
        country_data,
        inter_threads=util.opus.TRANSLATION_INTER_THREADS,
        intra_threads=util.opus.TRANSLATION_INTRA_THREADS,
        use_translation_cache=False,  # TODO: Set back to True
        translation_json_filename=None):
    translation_data = []
    if translation_json_filename and os.path.exists(translation_json_filename):
        print('  Loading translation cache for {} from {}'.format(
            country, translation_json_filename))
        translation_data = json.load(open(translation_json_filename))
    elif translation_json_filename:
        print('  No translation cache found at {}'.format(
            translation_json_filename))
    translation_map = {}
    for item in translation_data:
        translation_map[item['doc_id']] = item

    state = {
        'inputs': {},
        'input_paths': {},
        'output_paths': {},
        'outputs': {},
        'num_input_characters': 0,
        'num_cached_translations': 0,
        'num_manual_cached_translations': 0,
        'num_untranslated': {}
    }

    for doc_index, doc in enumerate(country_data):
        # We assume a document is in the legacy format if it has not been
        # explicitly marked otherwise.
        if 'is_legacy' in doc and not doc['is_legacy']:
            modern.queue_translation_data(doc, doc_index, state,
                                          translation_map, country_data,
                                          use_translation_cache)
        else:
            legacy.queue_translation_data(doc, doc_index, state,
                                          translation_map, country_data,
                                          use_translation_cache)

    print('Num untranslated: {}'.format(state['num_untranslated']))

    print('Num input characters: {} (Price @ $10/MChar: ${:.2f})'.format(
        state['num_input_characters'], state['num_input_characters'] / 1.e5))
    if state['num_cached_translations']:
        print('Reusing {} cached translations.'.format(
            state['num_cached_translations']))
    if state['num_manual_cached_translations']:
        print('Reusing {} manual cached translations.'.format(
            state['num_manual_cached_translations']))

    for language_code in state['inputs']:
        language = util.opus.CODE_TO_LANGUAGE[language_code]
        print('Will translate {} inputs from {} for {}'.format(
            len(state['inputs'][language_code]), language, country))
        lang_outputs, seconds = util.opus.translate_to_english(
            state['inputs'][language_code],
            language,
            inter_threads=inter_threads,
            intra_threads=intra_threads)
        print('Translated in {} seconds'.format(seconds))
        state['outputs'][language_code] = lang_outputs

    for language_code in state['outputs']:
        for index in range(len(state['output_paths'][language_code])):
            output_path = state['output_paths'][language_code][index]
            set_from_path(country_data, output_path,
                          state['outputs'][language_code][index])


def translate_country_json_file(
        country,
        filename,
        inter_threads=util.opus.TRANSLATION_INTER_THREADS,
        intra_threads=util.opus.TRANSLATION_INTRA_THREADS,
        translation_json_filename=None):
    country_data = json.load(open(filename))
    translate_country_data(country,
                           country_data,
                           inter_threads=inter_threads,
                           intra_threads=intra_threads,
                           translation_json_filename=translation_json_filename)
    with open(filename, 'w') as outfile:
        json.dump(country_data, outfile, indent=1)


def process_xml(
        folder_glob,
        countries=sorted(list(COUNTRIES.keys())),
        tag=None,
        disable_translation=False,
        inter_threads=util.opus.TRANSLATION_INTER_THREADS,
        intra_threads=util.opus.TRANSLATION_INTRA_THREADS,
        # DEBUG
        use_per_country_cache=False,  # Set this back to True
        verbose=False):
    '''Processes unpacked .tar.gz files of EU TED XMLs into JSON files.'''
    # Load the conversion maps (which we will extend as necessary).
    maps = load_conversion_maps()

    # Get the list of XML files to process.
    filenames = glob.glob(folder_glob)
    if verbose:
        print('Processing XML files: {}'.format(filenames))

    # Convert the XML inputs into a JSON dictionary organized by country.
    data = {}
    print('Preprocessing procurement XML files.')
    skipped_countries = {}
    with tqdm(total=len(filenames)) as pbar:
        for filename in filenames:
            with open(filename) as infile:
                try:
                    preprocess_xml(infile,
                                   data,
                                   maps,
                                   countries=countries,
                                   skipped_countries=skipped_countries)
                except Exception as e:
                    print('ERROR: Could not preprocess {}'.format(filename))
                    print(e)
                    traceback.print_exc()
            pbar.update(1)

    if len(skipped_countries):
        skipped_countries = {
            k: v
            for k, v in sorted(skipped_countries.items(),
                               key=lambda item: item[1],
                               reverse=True)
        }
        print('Skipped country codes: {}'.format(skipped_countries))

    kept_countries = {k: len(v) for k, v in data.items()}
    print('Countries to process: {}'.format(kept_countries))

    # Save the untranslated data to files.
    # NOTE: While we could avoid writing these files twice, this is a negligible
    # cost relative to the translation times.
    print('Saving untranslated procurement JSON, country-by-country.')
    json_filenames = {}
    for country in kept_countries:
        country_data = data[country]
        json_filename = country_json_filename(country, tag=tag)
        json_filenames[country] = json_filename
        if tag and use_per_country_cache and os.path.exists(json_filename):
            # We have a nontrivial tag -- usually a year and month -- and can
            # pull in the last version of this data, ideally including
            # pre-existing translations which we can avoid recomputing.
            print('  Loading cache for {} from {}'.format(
                country, json_filename))
            data[country] = json.load(open(json_filename))
        else:
            with open(json_filename, 'w') as outfile:
                json.dump(country_data, outfile, indent=1)

    # Save the updated maps.
    export_conversion_maps(maps)

    if not disable_translation:
        # Now translate the data.
        print('Translating procurement JSON, country-by-country.')
        for country, filename in json_filenames.items():
            print('Translating data for {}'.format(country))
            translate_country_json_file(country,
                                        filename,
                                        inter_threads=inter_threads,
                                        intra_threads=intra_threads)

    return json_filenames


def process_and_separate(tag,
                         unpack_dir,
                         json_filenames,
                         countries=sorted(list(COUNTRIES.keys())),
                         disable_translation=False,
                         inter_threads=util.opus.TRANSLATION_INTER_THREADS,
                         intra_threads=util.opus.TRANSLATION_INTRA_THREADS,
                         verbose=False):
    if verbose:
        print('Processing EU tenders for {}'.format(tag))
    filenames = process_xml('{}/*/**.xml'.format(unpack_dir),
                            countries=countries,
                            tag=tag,
                            disable_translation=disable_translation,
                            inter_threads=inter_threads,
                            intra_threads=intra_threads,
                            verbose=verbose)
    shutil.rmtree(unpack_dir)

    # Combine the filenames into the overall dictionary mapping
    # countries to the list of output JSON filenames.
    for country, filename in filenames.items():
        if country not in json_filenames:
            json_filenames[country] = []
        json_filenames[country].append(filename)


def export(json_filenames=None, date_pattern=None):
    if json_filenames is None:
        json_filenames = {}
        date_pattern = '*' if date_pattern == None else date_pattern
        for country in COUNTRIES:
            json_filenames[country] = glob.glob('{}/{}-{}.json'.format(
                TENDERS_DIR, country, date_pattern))
    for country in json_filenames:
        if not len(json_filenames[country]):
            continue

        # Combine this country's JSON files into a single DataFrame.
        dfs = []
        for json_filename in json_filenames[country]:
            dfs.append(pd.read_json(json_filename))
        df = pd.concat(dfs, ignore_index=True)
        df = df.drop(['@xmlns:n2016'], axis=1, errors='ignore')

        # Ensure that we have labeled whether it is legacy data.
        if 'is_legacy' not in df:
            df['is_legacy'] = True

        # Export the DataFrame into a CSV.
        csv_filename = country_csv_filename(country)
        df = util.postgres.preprocess_dataframe(df,
                                                json_columns=JSON_COLUMNS,
                                                unique_columns=UNIQUE_COLUMNS)
        util.postgres.df_to_csv(df, csv_filename, columns=TABLE_TYPES.keys())


def get_country_languages(json_filenames=None, verbose=False):
    '''Return the number of documents for each language in each country.'''
    languages = {}
    if json_filenames is None:
        json_filenames = {}
        for country in COUNTRIES:
            json_filenames[country] = glob.glob('{}/{}-*.json'.format(
                TENDERS_DIR, country))
    for country in json_filenames:
        if verbose:
            print('Incorporating {}'.format(country))
        if country not in languages:
            languages[country] = {}
        for filename in json_filenames[country]:
            data = json.load(open(filename))
            for filing in data:
                if 'data' not in filing:
                    continue
                for language in filing['data']:
                    if language not in languages[country]:
                        languages[country][language] = 0
                    languages[country][language] += 1

    return languages


# For the lists of files, see https://ted.europa.eu/en/simap/xml-bulk-download
def retrieve_days(day_descs,
                  countries=sorted(list(COUNTRIES.keys())),
                  disable_translation=False,
                  inter_threads=util.opus.TRANSLATION_INTER_THREADS,
                  intra_threads=util.opus.TRANSLATION_INTRA_THREADS,
                  verbose=False):
    staging_dir = TENDERS_STAGING_DIR

    json_filenames = {}
    for day_desc in day_descs:
        date = day_desc['date']
        year, month, day = util.dates.date_to_tuple(date)

        if 'ojs' in day_desc:
            url = 'https://ted.europa.eu/packages/daily/{}{}'.format(
                year,
                str(day_desc['ojs']).zfill(5))
        else:
            url = day_desc['url']

        unpack_dir = '{}/{}'.format(staging_dir, date)
        if verbose:
            print('Retrieving EU tenders for {}'.format(date))
        got_tenders = get_tenders(url, unpack_dir=unpack_dir, verbose=verbose)
        if got_tenders:
            process_and_separate(date,
                                 unpack_dir,
                                 json_filenames,
                                 countries,
                                 disable_translation=disable_translation,
                                 inter_threads=inter_threads,
                                 intra_threads=intra_threads,
                                 verbose=verbose)

    export(json_filenames)


def retrieve_month(year,
                   month,
                   start_ojs,
                   days,
                   countries=sorted(list(COUNTRIES.keys())),
                   disable_translation=False,
                   inter_threads=util.opus.TRANSLATION_INTER_THREADS,
                   intra_threads=util.opus.TRANSLATION_INTRA_THREADS,
                   verbose=False):
    day_descs = []
    ojs = start_ojs
    for day in days:
        day_descs.append({
            'ojs':
            ojs,
            'date':
            '{}-{}-{}'.format(year, str(month).zfill(2), str(day).zfill(2))
        })
        ojs += 1
    return retrieve_days(day_descs,
                         countries=countries,
                         disable_translation=disable_translation,
                         inter_threads=inter_threads,
                         intra_threads=intra_threads,
                         verbose=verbose)


# NOTE: This needs to be updated to handle the modification to month packages.
def _retrieve_months(year_month_pairs,
                     countries=sorted(list(COUNTRIES.keys())),
                     disable_translation=False,
                     inter_threads=util.opus.TRANSLATION_INTER_THREADS,
                     intra_threads=util.opus.TRANSLATION_INTRA_THREADS,
                     verbose=False):
    staging_dir = TENDERS_STAGING_DIR

    json_filenames = {}
    for year_month_pair in year_month_pairs:
        year, month = year_month_pair
        tag = '{}-{}'.format(year, str(month).zfill(2))
        unpack_dir = '{}/{}'.format(staging_dir, tag)
        print('Retrieving EU tenders for {}'.format(tag))
        got_tenders = get_month_of_tenders(year, month, unpack_dir=unpack_dir)
        if got_tenders:
            process_and_separate(tag,
                                 unpack_dir,
                                 json_filenames,
                                 countries,
                                 disable_translation=disable_translation,
                                 inter_threads=inter_threads,
                                 intra_threads=intra_threads,
                                 verbose=verbose)

    export(json_filenames)


def retrieve_years(years,
                   countries=sorted(list(COUNTRIES.keys())),
                   disable_translation=False,
                   inter_threads=util.opus.TRANSLATION_INTER_THREADS,
                   intra_threads=util.opus.TRANSLATION_INTRA_THREADS):
    year_month_pairs = []
    for year in years:
        for month in range(1, 13):
            year_month_pairs.append([year, month])
    retrieve_months(year_month_pairs,
                    countries=countries,
                    disable_translation=disable_translation,
                    inter_threads=inter_threads,
                    intra_threads=intra_threads)


def retrieve(start_year=None,
             start_month=None,
             end_year=None,
             end_month=None,
             window_days=30,
             countries=sorted(list(COUNTRIES.keys())),
             disable_translation=False,
             inter_threads=util.opus.TRANSLATION_INTER_THREADS,
             intra_threads=util.opus.TRANSLATION_INTRA_THREADS):
    if start_year is None or state_month is None:
        today = datetime.date.today()
        start_year, start_month, start_day = util.dates.earlier_day(
            today.year, today.month, today.day, window_days)
    if end_year is None or end_month is None:
        today = datetime.date.today()
        end_year, end_month, end_day = util.dates.next_day(
            today.year, today.month, today.day)

    year = start_year
    month = start_month

    num_months = (end_year - start_year) * 12 + (end_month - start_month) + 1

    year_month_pairs = []
    while year < end_year or (year == end_year and month <= end_month):
        year_month_pairs.append((year, month))
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1

    retrieve_months(year_month_pairs, countries, disable_translation,
                    inter_threads, intra_threads)


def postgres_create_country(country_code,
                            language_codes,
                            user=None,
                            password=None,
                            dbname=None,
                            port=None,
                            host=None):
    '''Creates the Postgres table for the specified country.'''
    table_name = 'eu_{}_contract_filing'.format(country_code)

    def create_language_index(cursor, language_code):
        # We have switched to always indexing using 'simple' configurations
        # rather than language-specific (when they exist) stemming.
        #
        # if language_code in util.postgres.LANGUAGE_CODE_TO_TS_CONFIG:
        #     config_tag = util.postgres.LANGUAGE_CODE_TO_TS_CONFIG[
        #         language_code]
        # else:
        #     config_tag = 'simple'
        #
        config_tag = 'simple'
        cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_{}_index ON {} USING GIN (
  to_tsvector('simple', data->'{}')
);
        '''.format(table_name, language_code, table_name, language_code))

    def create_data_index(cursor):
        # We have switched to always indexing using 'simple' configurations
        # rather than language-specific (when they exist) stemming.
        #
        # As a result, we can index the high-level 'data' object.
        #
        cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_data_index ON {} USING GIN (
  to_tsvector('simple', data)
);
        '''.format(table_name, table_name))

    def create_tsvector_index(cursor):
        cursor.execute('''
ALTER TABLE {} ADD COLUMN IF NOT EXISTS tsvector_col tsvector GENERATED ALWAYS AS (
  to_tsvector('simple', data)
) STORED;
        '''.format(table_name))
        cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (tsvector_col);
        '''.format(table_name, table_name))

    conn = util.postgres.get_connection(user=user,
                                        password=password,
                                        dbname=dbname,
                                        port=port,
                                        host=host)
    cursor = conn.cursor()
    cursor.execute("SET statement_timeout = '12h';")

    cursor.execute(
        util.postgres.create_table_sql(
            table_name,
            TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (doc_id)'}))
    #create_data_index(cursor)
    create_tsvector_index(cursor)
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_entities_index ON {} USING GIN (
  (entities) jsonb_path_ops
);
    '''.format(table_name, table_name))


def postgres_drop_data_index(country_code,
                             user=None,
                             password=None,
                             dbname=None,
                             port=None,
                             host=None):
    '''Drops the Postgres data index for the specified country.'''
    table_name = 'eu_{}_contract_filing'.format(country_code)

    def drop_data_index(cursor):
        config_tag = 'simple'
        cursor.execute('DROP INDEX {}_data_index;'.format(
            table_name, table_name))

    conn = util.postgres.get_connection(user=user,
                                        password=password,
                                        dbname=dbname,
                                        port=port,
                                        host=host)
    cursor = conn.cursor()
    drop_data_index(cursor)


def postgres_drop_country_indices(country_code,
                                  language_codes,
                                  user=None,
                                  password=None,
                                  dbname=None,
                                  port=None,
                                  host=None):
    '''Drops the Postgres indices for the specified country.'''
    table_name = 'eu_{}_contract_filing'.format(country_code)

    def drop_language_index(cursor, language_code):
        # We have switched to always indexing using 'simple' configurations
        # rather than language-specific (when they exist) stemming.
        #
        # if language_code in util.postgres.LANGUAGE_CODE_TO_TS_CONFIG:
        #     config_tag = util.postgres.LANGUAGE_CODE_TO_TS_CONFIG[
        #         language_code]
        # else:
        #     config_tag = 'simple'
        #
        config_tag = 'simple'
        cursor.execute('DROP INDEX {}_{}_index;'.format(
            table_name, language_code))

    conn = util.postgres.get_connection(user=user,
                                        password=password,
                                        dbname=dbname,
                                        port=port,
                                        host=host)
    cursor = conn.cursor()
    for language_code in language_codes:
        drop_language_index(cursor, language_code)
    cursor.execute('DROP INDEX {}_entities_index;'.format(table_name))


def postgres_drop_data_indices(user=None,
                               password=None,
                               dbname=None,
                               port=None,
                               host=None):
    '''Drops the Postgres data indices for the entire set of countries.'''
    for country_code, country_data in COUNTRIES.items():
        language_codes = ['en']
        postgres_drop_data_index(country_code, user, password, dbname, port,
                                 host)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    '''Creates the Postgres tables for the entire set of countries.'''
    for country_code, country_data in COUNTRIES.items():
        language_codes = ['en']
        if country_code in NON_ENGLISH_LANGUAGES:
            language_codes += NON_ENGLISH_LANGUAGES[country_code]
        postgres_create_country(country_code, language_codes, user, password,
                                dbname, port, host)


def postgres_country_update(country_code,
                            export_csv,
                            user=None,
                            password=None,
                            dbname=None,
                            port=None,
                            host=None,
                            vacuum=True):
    '''Updates the procurement data for a particular country.'''
    TABLE_NAME = 'eu_{}_contract_filing'.format(country_code)
    TABLE_TMP_NAME = 'eu_{}_contract_filing_tmp'.format(country_code)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_update(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True,
                    countries=sorted(list(COUNTRIES.keys()))):
    '''Updates the procurement data for the entire set of countries.'''
    postgres_create(user, password, dbname, port, host)
    for country_code in countries:
        export_csv = country_csv_filename(country_code)
        if not os.path.exists(export_csv):
            continue
        try:
            postgres_country_update(country_code, export_csv, user, password,
                                    dbname, port, host, vacuum)
        except Exception as e:
            print('Could not update country {}'.format(country_code))
            print(e)


def postgres_update_from_json(user=None,
                              password=None,
                              dbname=None,
                              port=None,
                              host=None,
                              vacuum=True,
                              countries=sorted(list(COUNTRIES.keys()))):
    '''Updates the procurement data from JSON one country-year at a time.'''
    YEARS = range(2015, datetime.date.today().year + 1)
    postgres_create(user, password, dbname, port, host)
    for country in countries:
        for year in YEARS:
            json_filenames = glob.glob('{}/{}-{}*.json'.format(
                TENDERS_DIR, country, year))
            if not len(json_filenames):
                continue
            print('Combining JSON for {}-{}'.format(country, year))

            # Combine this country's JSON files into a single DataFrame.
            dfs = []
            for json_filename in json_filenames:
                dfs.append(pd.read_json(json_filename))
            df = pd.concat(dfs, ignore_index=True)
            df = df.drop(['@xmlns:n2016'], axis=1, errors='ignore')

            # Export the DataFrame into a CSV.
            csv_filename = country_csv_filename(country)
            df = util.postgres.preprocess_dataframe(
                df, json_columns=JSON_COLUMNS, unique_columns=UNIQUE_COLUMNS)
            util.postgres.df_to_csv(df, csv_filename)
            try:
                postgres_country_update(country, csv_filename, user, password,
                                        dbname, port, host, vacuum)
            except Exception as e:
                print('Could not update country {}'.format(country))
                print(e)


def get_normalization_map(country_code, entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    # Map the departments of France up to the country itself.
    if country_code in ['gp', 'mq', 're', 'yt']:
        country_code = 'fr'
    return normalization_map_helper(['eu', country_code, 'procurement'],
                                    entities)
