#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# The primary exports are preprocess_xml and queue_translation_data.
#
from gov.eu.procurement.util import COUNTRIES, convert_to_list
from util.entities import canonicalize

AWARD_NOTICE_KEYS = [
    'ContractAwardNotice', 'can:ContractAwardNotice', 'urn:ContractAwardNotice'
]

INFORMATION_NOTICE_KEYS = [
    'PriorInformationNotice', 'pin:PriorInformationNotice',
    'urn:PriorInformationNotice', 'BusinessRegistrationInformationNotice',
    'brin:BusinessRegistrationInformationNotice',
    'ns:BusinessRegistrationInformationNotice'
]

NOTICE_KEYS = ['ContractNotice', 'cn:ContractNotice', 'urn:ContractNotice']

# A map from three-letter to two-letter ISO 639 language codes, extended as
# needed.
LANGUAGE_CODE_MAP = {
    'BUL': 'BG',
    'CES': 'CS',
    'DAN': 'DA',
    'DEU': 'DE',
    'ELL': 'EL',
    'ENG': 'EN',
    'EST': 'ET',
    'FIN': 'FI',
    'FRA': 'FR',
    'GLE': 'GA',
    'HRV': 'HR',
    'HUN': 'HU',
    'ITA': 'IT',
    'LAV': 'LV',
    'LIT': 'LT',
    'NLD': 'NL',
    'POL': 'PL',
    'POR': 'PT',
    'RON': 'RO',
    'SLK': 'SK',
    'SLV': 'SL',
    'SPA': 'ES',
    'SWE': 'SV'
}

# A map from three-letter to two-letter ISO 3166 country codes, extended as
# needed.
# See https://www.iban.com/country-codes.
COUNTRY_CODE_MAP = {
    '1a0': '1a',
    'ago': 'ao',
    'alb': 'al',
    'are': 'ae',
    'arg': 'ar',
    'atf': 'tf',
    'aut': 'at',
    'bel': 'be',
    'ben': 'bj',
    'bes': 'bq',
    'bfa': 'bf',
    'bgd': 'bd',
    'bgr': 'bg',
    'bih': 'ba',
    'blr': 'by',
    'bol': 'bo',
    'bra': 'br',
    'brb': 'bb',
    'caf': 'cf',
    'can': 'ca',
    'che': 'ch',
    'chn': 'cn',
    'cmr': 'cm',
    'cod': 'cd',
    'col': 'co',
    'cpv': 'cv',
    'cri': 'cr',
    'cyp': 'cy',
    'cze': 'cz',
    'deu': 'de',
    'dnk': 'dk',
    'dom': 'do',
    'dza': 'dz',
    'ecu': 'ec',
    'egy': 'eg',
    'esp': 'es',
    'est': 'ee',
    'eth': 'et',
    'fin': 'fi',
    'fji': 'fj',
    'fra': 'fr',
    'gbr': 'uk',  # note that 'gb' is also used
    'geo': 'ge',
    'gha': 'gh',
    'gin': 'gn',
    'grc': 'gr',
    'hrv': 'hr',
    'hun': 'hu',
    'idn': 'id',
    'ind': 'in',
    'irl': 'ie',
    'isl': 'is',
    'isr': 'il',
    'ita': 'it',
    'jor': 'jo',
    'jpn': 'jp',
    'kaz': 'kz',
    'ken': 'ke',
    'khm': 'kh',
    'kor': 'kr',
    'lbr': 'lr',
    'lca': 'lc',
    'lie': 'li',
    'lso': 'ls',
    'ltu': 'lt',
    'lux': 'lu',
    'lva': 'lv',
    'mar': 'ma',
    'mda': 'md',
    'mdg': 'mg',
    'mex': 'mx',
    'mkd': 'mk',
    'mli': 'ml',
    'mlt': 'mt',
    'mmr': 'mm',
    'mne': 'me',
    'mng': 'mn',
    'moz': 'mz',
    'mrt': 'mr',
    'mus': 'mu',
    'mwi': 'mw',
    'nam': 'na',
    'nld': 'nl',
    'nor': 'no',
    'npl': 'np',
    'nzl': 'nz',
    'pan': 'pa',
    'phl': 'ph',
    'pol': 'pl',
    'prt': 'pt',
    'pry': 'py',
    'pse': 'ps',
    'pyf': 'pf',
    'rou': 'ro',
    'rwa': 'rw',
    'sau': 'sa',
    'sen': 'sn',
    'srb': 'rs',
    'svk': 'sk',  # note that 'lo' is also used
    'svn': 'si',
    'swe': 'se',  # note that 'sw' is also used
    'tcd': 'td',
    'tgo': 'tg',
    'tha': 'th',
    'tjk': 'tj',
    'tls': 'tl',
    'tun': 'tn',
    'tur': 'tu',
    'twn': 'tw',
    'tza': 'tz',
    'ukr': 'ua',
    'usa': 'us',
    'uzb': 'uz',
    'vnm': 'vn',
    'xkx': '1a',
    'zaf': 'za',
    'zmb': 'zm'
}


def has_a_key(d, keys):
    for key in keys:
        if key in d:
            return True
    return False


# Returns the upper-case three-letter country code, if it exists.
def get_country3(info, data):
    if 'extension' in info:
        extension = info['extension']
        if 'organizations' in extension:
            organization = extension['organizations'][0]
            if 'address' in organization:
                address = organization['address']
                if 'country' in address:
                    return address['country']
    if 'cac:ProcurementProject' in data:
        project = data['cac:ProcurementProject']
        if 'cac:RealizedLocation' in project:
            return project['cac:RealizedLocation']['cac:Address'][
                'cac:Country']['cbc:IdentificationCode']['#text']
    elif 'cac:BusinessParty' in data:
        party = data['cac:BusinessParty']
        if 'cac:PostalAddress' in party:
            address = party['cac:PostalAddress']
            if 'cac:Country' in address:
                return get_text(
                    address['cac:Country']['cbc:IdentificationCode'])
        if 'cac:PartyLegalEntity' in party:
            legal_entity = party['cac:PartyLegalEntity']
            if 'cac:CorporateRegistrationScheme' in legal_entity:
                registration = legal_entity['cac:CorporateRegistrationScheme']
                if 'cac:JurisdictionRegionAddress' in registration:
                    address = registration['cac:JurisdictionRegionAddress']
                    if 'cac:Country' in address:
                        return get_text(
                            address['cac:Country']['cbc:IdentificationCode'])
    print('ERROR: Could not find a country code in {}.'.format(data))
    return None


def get_country(info, data):
    country_code3 = get_country3(info, data)
    if not country_code3:
        return None
    country_code3 = country_code3.lower()
    if country_code3 in COUNTRY_CODE_MAP:
        country_code2 = COUNTRY_CODE_MAP[country_code3]
    else:
        print('ERROR: Unrecognized country code of {}'.format(country_code3))
        return None
    return country_code2


def get_text(data, prefer_list_name=False):
    if data is None:
        raise Exception('ERROR: data was None in get_text.')
        return ''
    if isinstance(data, str):
        return data
    key_list = ['@listName', '#text'
                ] if prefer_list_name else ['#text', '@listName']
    for key in key_list:
        if key in data:
            return data[key]
    if isinstance(data, list):
        raise Exception('ERROR: Could not apply get_text to list {}'.format(data))
    print('WARNING: Unable to extract from {}'.format(data))
    return ''


def get_text_assignment(info, info_key, data, data_key):
    if data_key in data and data[data_key] != None:
        info[info_key] = get_text(data[data_key])


def get_notice_id(doc):
    return get_text(doc['cbc:ID'])


def get_notice_type_code(doc):
    return get_text(doc['cbc:NoticeTypeCode'])


def get_language_code(doc):
    code = get_text(doc['cbc:NoticeLanguageCode'])
    if code not in LANGUAGE_CODE_MAP:
        print('ERROR: Language code {} was not found.'.format(code))
    return LANGUAGE_CODE_MAP[code].lower()


def incorporate_contracting_parties(info, doc):
    CONTRACTING_PARTY_KEY = 'cac:ContractingParty'
    PARTY_TYPE_KEY = 'cac:ContractingPartyType'
    BUYER_PROFILE_URI_KEY = 'cbc:BuyerProfileURI'
    ACTIVITY_KEY = 'cac:ContractingActivity'
    if CONTRACTING_PARTY_KEY not in doc:
        return
    parties_info = []
    for party in convert_to_list(doc[CONTRACTING_PARTY_KEY]):
        party_info = {}
        if BUYER_PROFILE_URI_KEY in party:
            party_info['uri'] = party[BUYER_PROFILE_URI_KEY]
        if PARTY_TYPE_KEY in party:
            types_info = []
            for party_type in convert_to_list(party[PARTY_TYPE_KEY]):
                types_info.append(get_text(party_type['cbc:PartyTypeCode']))
            party_info['party_types'] = types_info
        if ACTIVITY_KEY in party:
            activities_info = []
            for activity in convert_to_list(party[ACTIVITY_KEY]):
                activities_info.append(
                    get_text(activity['cbc:ActivityTypeCode']))
            party_info['activity_type'] = activities_info
        if 'cac:Party' in party:
            party_data = party['cac:Party']
            if 'cac:PartyIdentification' in party_data:
                party_info['id'] = get_text(
                    party_data['cac:PartyIdentification']['cbc:ID'])
            if 'cac:ServiceProviderParty' in party_data:
                service_parties_info = []
                for service_data in convert_to_list(
                        party_data['cac:ServiceProviderParty']):
                    service_party_info = {}
                    if 'cbc:ServiceTypeCode' in service_data:
                        service_party_info['type'] = get_text(
                            service_data['cbc:ServiceTypeCode'])
                    service_party_info['id'] = get_text(
                        service_data['cac:Party']['cac:PartyIdentification']
                        ['cbc:ID'])
                party_info['service_parties'] = service_parties_info
        parties_info.append(party_info)
    info['contracting_parties'] = parties_info


def incorporate_tender_qualification_request(info, data):
    if 'cac:TendererQualificationRequest' not in data:
        return
    qual_request = data['cac:TendererQualificationRequest']
    request_info = {}
    if 'cac:SpecificTendererRequirement' in qual_request:
        spec_request = qual_request['cac:SpecificTendererRequirement']
        spec_info = {}
        get_text_assignment(spec_info, 'requirement_type', spec_request,
                            'cbc:TenderRequirementTypeCode')
        if 'cbc:Description' in spec_request:
            spec_info['description'] = prefer_english(
                spec_request['cbc:Description'])
        request_info['spec_request'] = spec_info
    if 'cbc:CompanyLegalFormCode' in qual_request:
        request_info['legal_form'] = qual_request['cbc:CompanyLegalFormCode']
    info['qual_request'] = request_info


def incorporate_tendering_process(info, doc):
    PROCESS_KEY = 'cac:TenderingProcess'
    if PROCESS_KEY not in doc:
        return
    data = doc[PROCESS_KEY]
    process_info = {}
    if 'cbc:Description' in data:
        process_info['description'] = prefer_english(data['cbc:Description'])
    get_text_assignment(process_info, 'procedure', data, 'cbc:ProcedureCode')
    get_text_assignment(process_info, 'part_presentation', data,
                        'cbc:PartPresentationCode')
    if 'cac:ProcessJustification' in data:
        justification = data['cac:ProcessJustification']
        process_info['justification'] = {}
        get_text_assignment(process_info['justification'], 'reason',
                            justification, 'cbc:ProcessReasonCode')
    get_text_assignment(process_info, 'submission_method', data,
                        'cbc:SubmissionMethodCode')
    get_text_assignment(process_info, 'gov_agreement_constraint', data,
                        'cbc:GovernmentAgreementConstraintIndicator')
    incorporate_deadline(process_info, data,
                         'cac:TenderSubmissionDeadlinePeriod',
                         'submission_deadline')
    # TODO(Jack Poulson): cac:EconomicOperatorShortList
    incorporate_event(process_info, data, 'cac:OpenTenderEvent', 'tender_open')
    # TODO(Jack Poulson): cac:AuctionTerms
    # TODO(Jack Poulson): cac:FrameworkAgreement
    incorporate_contracting_systems(process_info, data)

    info['process'] = process_info


def incorporate_requested_tender_total(info, data):
    if 'cac:RequestedTenderTotal' not in data:
        return
    rtt = data['cac:RequestedTenderTotal']
    rtt_info = {}
    if 'cbc:EstimatedOverallContractAmount' in rtt:
        eoca = rtt['cbc:EstimatedOverallContractAmount']
        rtt_info['estimated'] = {
            'currency': eoca['@currencyID'],
            'amount': eoca['#text']
        }
    info['requested_tender_total'] = rtt_info


def incorporate_commodity_classifications(info, data):
    MCC_KEY = 'cac:MainCommodityClassification'
    ACC_KEY = 'cac:AdditionalCommodityClassification'
    CLASSIFICATION_KEY = 'cbc:ItemClassificationCode'
    if MCC_KEY in data:
        mcc_info = []
        for mcc in convert_to_list(data[MCC_KEY]):
            mcc_info.append(get_text(mcc[CLASSIFICATION_KEY]))
        info['main_commodity_classification'] = mcc_info
    if ACC_KEY in data:
        acc_info = []
        for acc in convert_to_list(data[ACC_KEY]):
            for item in convert_to_list(acc):
                acc_info.append(get_text(item[CLASSIFICATION_KEY]))
        info['additional_commodity_classifications'] = acc_info


def incorporate_realized_location(info, data):
    if 'cac:RealizedLocation' not in data:
        return
    rl = data['cac:RealizedLocation']
    rl_info = {}
    if 'cbc:Description' in rl:
        rl_info['description'] = prefer_english(rl['cbc:Description'])
    if 'cac:Address' in rl:
        rl_address = rl['cac:Address']
        address_info = {}
        if 'cac:Country' in rl_address:
            address_info['country'] = get_text(
                rl_address['cac:Country']['cbc:IdentificationCode'])
        if 'cbc:CountrySubentityCode' in rl_address:
            address_info['country_subentity'] = get_text(
                rl['cac:Address']['cbc:CountrySubentityCode'])
        if len(address_info.keys()):
            rl_info['address'] = address_info
    if len(rl_info.keys()):
        info['realized_location'] = rl_info


def incorporate_planned_period(info, data):
    if 'cac:PlannedPeriod' not in data:
        return
    pp = data['cac:PlannedPeriod']
    if 'cbc:DurationMeasure' in pp:
        dm = pp['cbc:DurationMeasure']
        info['planned_period'] = {
            'unit': dm['@unitCode'],
            'amount': dm['#text']
        }
    elif 'cbc:StartDate' in pp:
        info['planned_period'] = {'startDate': pp['cbc:StartDate']}
    elif 'cbc:EndDate' in pp:
        info['planned_period'] = {'endDate': pp['cbc:EndDate']}
    if 'cbc:DescriptionCode' in pp:
        text = get_text(pp['cbc:DescriptionCode'])
        if text and text.lower() != 'unknown':
            info['description'] = text


def incorporate_contract_extension(info, data):
    if 'cac:ContractExtension' not in data:
        return
    extension = data['cac:ContractExtension']
    extension_info = {}
    if 'cbc:OptionsDescription' in extension:
        extension_info['options_description'] = prefer_english(extension['cbc:OptionsDescription'])
    get_text_assignment(extension_info, 'max_number', extension,
                        'cbc:MaximumNumberNumeric')
    info['extension'] = extension_info


def incorporate_project(info, doc):
    PROJECT_KEY = 'cac:ProcurementProject'
    if PROJECT_KEY not in doc:
        return
    data = doc[PROJECT_KEY]
    project_info = {}
    get_text_assignment(project_info, 'id', data, 'cbc:ID')
    project_info['name'] = prefer_english(data['cbc:Name'])
    if 'cbc:Description' in data:
        project_info['description'] = prefer_english(data['cbc:Description'])
    get_text_assignment(project_info, 'procurement_type', data,
                        'cbc:ProcurementTypeCode')
    if 'cbc:Note' in data:
        project_info['note'] = prefer_english(data['cbc:Note'])
    incorporate_requested_tender_total(project_info, data)
    incorporate_commodity_classifications(project_info, data)
    incorporate_realized_location(project_info, data)
    incorporate_planned_period(project_info, data)
    incorporate_contract_extension(project_info, data)
    info['project'] = project_info


def incorporate_deadline(info, data, orig_key, new_key):
    if orig_key not in data:
        return
    period = data[orig_key]
    info[new_key] = {
        'date': period['cbc:EndDate'],
        'time': period['cbc:EndTime']
    }


def incorporate_event(info, data, orig_key, new_key):
    if orig_key not in data:
        return
    event = data[orig_key]
    event_info = {}
    if 'cbc:OccurrenceDate' in event:
        event_info['date'] = event['cbc:OccurrenceDate']
    if 'cbc:OccurrenceTime' in event:
        event_info['time'] = event['cbc:OccurrenceTime']
    # Note that the misspelling of 'Occurence' in the following is intentional,
    # as that it what was observed in the published TED data. We check for
    # its existence and attempt to fall back to the correction.
    if 'cac:OccurenceLocation' in event:
        event_info['location'] = prefer_english(
            event['cac:OccurenceLocation']['cbc:Description'])
    elif 'cac:OccurrenceLocation' in event:
        event_info['location'] = prefer_english(
            event['cac:OccurrenceLocation']['cbc:Description'])
    info[new_key] = event_info


def incorporate_contracting_systems(info, data):
    if 'cac:ContractingSystem' not in data:
        return
    system_info = []
    for system in convert_to_list(data['cac:ContractingSystem']):
        system_info.append(get_text(system['cbc:ContractingSystemTypeCode']))
    info['contracting_systems'] = system_info


def incorporate_project_lot_project(info, lot):
    PROCUREMENT_PROJECT_KEY = 'cac:ProcurementProject'
    PROCUREMENT_TYPE_KEY = 'cbc:ProcurementTypeCode'
    if PROCUREMENT_PROJECT_KEY not in lot:
        return
    project = lot[PROCUREMENT_PROJECT_KEY]
    if isinstance(project, list):
        print('WARNING: project was a list')
    project_info = {}
    if 'cbc:ID' in project:
        project_info['id'] = project['cbc:ID']
    if 'cbc:Name' in project:
        project_info['name'] = prefer_english(project['cbc:Name'])
    if 'cbc:Description' in project:
        project_info['description'] = prefer_english(
            project['cbc:Description'])
    if 'cbc:Note' in project:
        project_info['note'] = prefer_english(project['cbc:Note'])
    get_text_assignment(project_info, 'procurement_type', project,
                        PROCUREMENT_TYPE_KEY)
    if 'cac:ProcurementAdditionalType' in project:
        additional_info = []
        for pat in convert_to_list(project['cac:ProcurementAdditionalType']):
            if PROCUREMENT_TYPE_KEY in pat:
                additional_info.append(get_text(pat[PROCUREMENT_TYPE_KEY]))
            elif 'cbc:ProcurementType' in pat:
                additional_info.append(prefer_english(pat['cbc:ProcurementType']))
            else:
                print('WARNING: Did not extract type from {}'.format(pat))
        project_info['procurement_additional_type'] = additional_info
    incorporate_requested_tender_total(project_info, project)
    incorporate_commodity_classifications(project_info, project)
    incorporate_realized_location(project_info, project)
    incorporate_planned_period(project_info, project)
    incorporate_contract_extension(project_info, project)
    info['project'] = project_info


def incorporate_contract_execution_requirement(info, data):
    if 'cac:ContractExecutionRequirement' not in data:
        return
    requirement_info = []
    for requirement in convert_to_list(
            data['cac:ContractExecutionRequirement']):
        if 'cbc:Description' in requirement:
            requirement_info.append(
                prefer_english(requirement['cbc:Description']))
        elif 'cbc:ExecutionRequirementCode' in requirement:
            requirement_info.append(
                get_text(requirement['cbc:ExecutionRequirementCode']))
        else:
            print('WARNING: did not extract from {}'.format(requirement))

    if len(requirement_info):
        info['contract_execution_requirement'] = requirement_info


def incorporate_awarding_terms(info, data):
    if 'cac:AwardingTerms' not in data:
        return
    awarding_terms = data['cac:AwardingTerms']
    terms_info = {}
    if 'cac:AwardingCriterion' not in awarding_terms:
        return
    awarding_criterion = awarding_terms['cac:AwardingCriterion']
    awarding_info = {}
    if 'cac:SubordinateAwardingCriterion' in awarding_criterion:
        subordinate_info = []
        for subordinate in convert_to_list(
                awarding_criterion['cac:SubordinateAwardingCriterion']):
            # TODO(Jack Poulson): Add ext:UBLExtensions
            if 'cbc:AwardingCriterionTypeCode' in subordinate:
                subordinate_info.append(
                    get_text(subordinate['cbc:AwardingCriterionTypeCode']))
        awarding_info['subordinate_criterion'] = subordinate_info
    terms_info['awarding_criterion'] = awarding_info
    info['awarding_terms'] = terms_info


def incorporate_additional_information_party(info, data):
    # TODO(Jack Poulson)
    pass


def incorporate_tender_recipient_party(info, data):
    # TODO(Jack Poulson)
    pass


def incorporate_tender_validity_period(info, data):
    # TODO(Jack Poulson)
    pass


def incorporate_appeal_terms(info, data):
    # TODO(Jack Poulson)
    pass


def incorporate_tendering_terms(info, lot):
    if 'cac:TenderingTerms' not in lot:
        return
    terms = lot['cac:TenderingTerms']
    terms_info = {}
    # TODO(Jack Poulson): ext:UBLExtensions
    get_text_assignment(terms_info, 'variant_constraint', terms,
                        'cbc:VariantConstraintCode')
    get_text_assignment(terms_info, 'funding_program', terms,
                        'cbc:FundingProgramCode')
    get_text_assignment(terms_info, 'required_curricula', terms,
                        'cbc:RequiredCurriculaCode')
    get_text_assignment(terms_info, 'recurring_procurement', terms,
                        'cbc:RecurringProcurementIndicator')
    if 'cac:RequiredFinancialGuarantee' in terms:
        rfg = terms['cac:RequiredFinancialGuarantee']
        terms_info['required_financial_guarantee'] = get_text(
            rfg['cbc:GuaranteeTypeCode'])
    if 'cac:CallForTendersDocumentReference' in terms:
        call_infos = []
        for cftdr in convert_to_list(
                terms['cac:CallForTendersDocumentReference']):
            call_info = {}
            call_info['id'] = cftdr['cbc:ID']
            if 'cbc:DocumentType' in cftdr:
                call_info['document_type'] = get_text(cftdr['cbc:DocumentType'])
            if 'cbc:LanguageID' in cftdr:
                call_info['language'] = cftdr['cbc:LanguageID']
            if 'cac:Attachment' in cftdr:
                attachment = cftdr['cac:Attachment']
                attach_info = {}
                if 'cac:ExternalReference' in attachment:
                    ext_ref = attachment['cac:ExternalReference']
                    attach_info['uri'] = ext_ref['cbc:URI']
                call_info['attachment'] = attach_info
            call_infos.append(call_info)
        terms_info['call_for_tenders'] = call_infos

    incorporate_tender_qualification_request(terms_info, terms)
    incorporate_contract_execution_requirement(terms_info, terms)
    incorporate_awarding_terms(terms_info, terms)
    incorporate_additional_information_party(terms_info, terms)
    incorporate_tender_recipient_party(terms_info, terms)
    incorporate_tender_validity_period(terms_info, terms)
    incorporate_appeal_terms(terms_info, terms)
    if 'cac:Language' in terms:
        languages = []
        for language in convert_to_list(terms['cac:Language']):
            languages.append(language['cbc:ID'])
        info['languages'] = languages
    if 'cac:PostAwardProcess' in terms:
        post_process = terms['cac:PostAwardProcess']
        post_process_info = {}
        if 'cbc:ElectronicOrderUsageIndicator' in post_process:
            post_process_info['electronic_order_usage'] = post_process[
                'cbc:ElectronicOrderUsageIndicator']
        if 'cbc:ElectronicPaymentUsageIndicator' in post_process:
            post_process_info['electronic_payment_usage'] = post_process[
                'cbc:ElectronicPaymentUsageIndicator']
        info['post_award_process'] = post_process_info

    # TODO(Jack Poulson): Handle these fields:
    #   cac:ProcurementLegislationDocumentReference
    #   cac:LotDistribution

    info['terms'] = terms_info


def incorporate_project_lots(info, doc):
    if 'cac:ProcurementProjectLot' not in doc:
        return
    lots_info = []
    for lot in convert_to_list(doc['cac:ProcurementProjectLot']):
        lot_info = {}
        lot_info['id'] = get_text(doc['cbc:ID'])
        incorporate_tendering_terms(lot_info, lot)
        incorporate_tendering_process(lot_info, lot)
        incorporate_project_lot_project(lot_info, lot)
        lots_info.append(lot_info)
    info['project_lots'] = lots_info


def prefer_english(name_obj, intermediate_key=None):

    def wrap(item):
        return item[intermediate_key] if intermediate_key else item

    if isinstance(name_obj, list):
        for item in name_obj:
            if wrap(item)['@languageID'] == 'ENG':
                return wrap(item)['#text']
        return wrap(name_obj[0])['#text']
    else:
        return get_text(wrap(name_obj))


def parse_company(company):
    WEBSITE_KEY = 'cbc:WebsiteURI'
    ADDRESS_KEY = 'cac:PostalAddress'
    STREET_KEY = 'cbc:StreetName'
    DEPARTMENT_KEY = 'cbc:Department'
    CITY_KEY = 'cbc:CityName'
    POSTAL_KEY = 'cbc:PostalZone'
    COUNTRY_KEY = 'cac:Country'
    PARTY_LEGAL_ENTITY_KEY = 'cac:PartyLegalEntity'
    CONTACT_KEY = 'cac:Contact'
    NAME_KEY = 'cbc:Name'
    PHONE_KEY = 'cbc:Telephone'
    EMAIL_KEY = 'cbc:ElectronicMail'
    info = {}
    info['party_id'] = get_text(company['cac:PartyIdentification']['cbc:ID'])
    if 'cac:PartyName' in company:
        info['name'] = prefer_english(company['cac:PartyName'], NAME_KEY)
    get_text_assignment(info, 'website', company, WEBSITE_KEY)
    if ADDRESS_KEY in company:
        address = company[ADDRESS_KEY]
        address_info = {}
        get_text_assignment(address_info, 'street', address, STREET_KEY)
        get_text_assignment(address_info, 'department', address,
                            DEPARTMENT_KEY)
        get_text_assignment(address_info, 'city', address, CITY_KEY)
        get_text_assignment(address_info, 'postal', address, POSTAL_KEY)
        if COUNTRY_KEY in address:
            address_info['country'] = get_text(
                address[COUNTRY_KEY]['cbc:IdentificationCode'])
        info['address'] = address_info
    if PARTY_LEGAL_ENTITY_KEY in company:
        entities_info = []
        for entity in convert_to_list(company[PARTY_LEGAL_ENTITY_KEY]):
            entities_info.append(entity['cbc:CompanyID'])
        info['entities'] = entities_info
    if CONTACT_KEY in company:
        contact = company[CONTACT_KEY]
        contact_info = {}
        get_text_assignment(contact_info, 'name', contact, NAME_KEY)
        get_text_assignment(contact_info, 'phone', contact, PHONE_KEY)
        get_text_assignment(contact_info, 'email', contact, EMAIL_KEY)
        info['contact'] = contact_info
    return info


def incorporate_lot_result(info, data):
    LOT_RESULT_KEY = 'efac:LotResult'
    STATISTICS_KEY = 'efac:ReceievedSubmissionsStatistics'
    if LOT_RESULT_KEY not in data:
        return
    result_infos = []
    for result in convert_to_list(data[LOT_RESULT_KEY]):
        result_info = {}
        get_text_assignment(result_info, 'id', result, 'cbc:ID')
        get_text_assignment(result_info, 'tender_result', result,
                            'cbc:TenderResultCode')
        if 'efac:LotTender' in result:
            lot_tenders_info = []
            for lot_tender in convert_to_list(result['efac:LotTender']):
                lot_tenders_info.append(get_text(lot_tender['cbc:ID']))
            result_info['lot_tenders'] = lot_tenders_info
        if STATISTICS_KEY in result:
            stats_info = []
            for submission in convert_to_list(result[STATISTICS_KEY]):
                stat_info = {
                    'type': get_text(submission['efbc:StatisticsCode']),
                    'numric': submission['efbc:StatisticsNumeric']
                }
                stats_info.append(stat_info)
            result_info['submission_stats'] = stats_info
        if 'efac:SettledContract' in result:
            contracts_info = []
            for contract in convert_to_list(result['efac:SettledContract']):
                contracts_info.append(get_text(contract['cbc:ID']))
            result_info['settled_contracts'] = contracts_info
        if 'efac:TenderLot' in result:
            result_info['tender_lot'] = get_text(
                result['efac:TenderLot']['cbc:ID'])
        result_infos.append(result_info)
    info['lot_result'] = result_infos


def incorporate_lot_tender(info, data):
    LOT_TENDER_KEY = 'efac:LotTender'
    if LOT_TENDER_KEY not in data:
        return
    tender_infos = []
    for tender in convert_to_list(data[LOT_TENDER_KEY]):
        tender_info = {}
        get_text_assignment(tender_info, 'id', tender, 'cbc:ID')
        if 'cac:LegalMonetaryTotal' in tender:
            monetary_total = tender['cac:LegalMonetaryTotal']
            total_info = {
                'currency': monetary_total['cbc:PayableAmount']['@currencyID'],
                'amount': monetary_total['cbc:PayableAmount']['#text']
            }
            tender_info['monetary_total'] = total_info
        # TODO(Jack Poulson): Rerun this on June 17, 2024, June 28, 2024.
        if 'efac:SubcontractingTerm' in tender:
            subterms_info = []
            for subterm in convert_to_list(tender['efac:SubcontractingTerm']):
                subterms_info.append(get_text(subterm['efbc:TermCode'])) 
            tender_info['subcontracting_term'] = subterms_info
        if 'efac:TenderingParty' in tender:
            tender_info['tendering_party'] = get_text(
                tender['efac:TenderingParty']['cbc:ID'])
        if 'efac:TenderLot' in tender:
            tender_info['tender_lot'] = get_text(
                tender['efac:TenderLot']['cbc:ID'])
        if 'efac:TenderReferencet' in tender:
            tender_info['tender_reference'] = get_text(
                tender['efac:TenderReference']['cbc:ID'])
        tender_infos.append(tender_info)
    info['lot_tender'] = tender_infos


def incorporate_settled_contract(info, data):
    CONTRACT_KEY = 'efac:SettledContract'
    if CONTRACT_KEY not in data:
        return
    contract_infos = []
    for contract in convert_to_list(data[CONTRACT_KEY]):
        contract_info = {}
        get_text_assignment(contract_info, 'id', contract, 'cbc:ID')
        get_text_assignment(contract_info, 'issue_date', contract,
                            'cbc:IssueDate')
        if 'cbc:Title' in contract:
            contract_info['title'] = prefer_english(contract['cbc:Title'])
        get_text_assignment(contract_info, 'framework', contract,
                            'efbc:ContractFrameworkIndicator')
        if 'efac:ContractReference' in contract:
            contract_info['reference'] = contract['efac:ContractReference'][
                'cbc:ID']
        if 'efac:LotTender' in contract:
            tenders_info = []
            for tender in convert_to_list(contract['efac:LotTender']):
                tenders_info.append(get_text(tender['cbc:ID']))
            contract_info['lot_tenders'] = tenders_info
        contract_infos.append(contract_info)
    info['settled_contracts'] = contract_infos


def incorporate_tendering_party(info, data):
    PARTY_KEY = 'efac:TenderingParty'
    if PARTY_KEY not in data:
        return
    parties_info = []
    for party in convert_to_list(data[PARTY_KEY]):
        party_info = {}
        party_info['id'] = get_text(party['cbc:ID'])
        if 'efac:Tenderer' in party:
            tenderers_info = []
            for tenderer in convert_to_list(party['efac:Tenderer']):
                tenderer_info = {}
                tenderer_info['id'] = get_text(tenderer['cbc:ID'])
                if 'efbc:GroupLeadIndicator' in tenderer:
                    tenderer_info['group_lead'] = tenderer[
                        'efbc:GroupLeadIndicator']
                tenderers_info.append(tenderer_info)
            party_info['tenderers'] = tenderers_info
        parties_info.append(party_info)
    info['tendering_parties'] = parties_info


def incorporate_notice_result(info, data):
    NOTICE_RESULT_KEY = 'efac:NoticeResult'
    if NOTICE_RESULT_KEY not in data:
        return
    result = data[NOTICE_RESULT_KEY]
    result_info = {}
    if 'cbc:TotalAmount' in result:
        total_amount = result['cbc:TotalAmount']
        result_info['total_amount'] = {
            'currency': total_amount['@currencyID'],
            'amount': total_amount['#text']
        }
    incorporate_lot_result(result_info, result)
    incorporate_lot_tender(result_info, result)
    incorporate_settled_contract(result_info, result)
    incorporate_tendering_party(result_info, result)
    info['notice_result'] = result_info


def incorporate_modification(info, eforms):
    if 'efac:ContractModification' not in eforms:
        return
    mod = eforms['efac:ContractModification']
    mod_info = {}
    get_text_assignment(mod_info, 'id', mod, 'efbc:ChangeNoticeIdentifier')
    if 'efac:Change' in mod:
        change = mod['efac:Change']
        change_info = {}
        if 'efbc:ChangeDescription' in change:
            change_info['description'] = prefer_english(
                change['efbc:ChangeDescription'])
        if 'efac:ChangedSection' in change:
            section_ids = []
            for section in convert_to_list(change['efac:ChangedSection']):
                section_ids.append(get_text(section['efbc:ChangedSectionIdentifier']))
            change_info['section_ids'] = section_ids
        mod_info['change'] = change_info
    if 'efac:ChangeReason' in mod:
        reason = mod['efac:ChangeReason']
        reason_info = {}
        if 'cbc:ReasonCode' in reason:
            reason_info['code'] = get_text(reason['cbc:ReasonCode'])
        if 'efbc:ReasonDescription' in reason:
            reason_info['description'] = prefer_english(
                reason['efbc:ReasonDescription'])
        mod_info['reason'] = reason_info
    info['modification'] = mod_info


def incorporate_extensions(info, doc):
    EXTENSIONS_KEY = 'ext:UBLExtensions'
    EXTENSION_KEY = 'ext:UBLExtension'
    CONTENT_KEY = 'ext:ExtensionContent'
    EFORMS_KEY = 'efext:EformsExtension'
    TRANSMISSION_DATE_KEY = 'efbc:TransmissionDate'
    TRANSMISSION_TIME_KEY = 'efbc:TransmissionTime'
    NOTICE_SUBTYPE_KEY = 'efac:NoticeSubType'
    ORGANIZATIONS_KEY = 'efac:Organizations'
    ORGANIZATION_KEY = 'efac:Organization'
    COMPANY_KEY = 'efac:Company'
    PUBLICATION_KEY = 'efac:Publication'
    if EXTENSIONS_KEY not in doc:
        print('WARNING: No extensions in document, so cannot extract doc ID.')
        return
    extensions = doc[EXTENSIONS_KEY]
    if isinstance(extensions, list):
        print('ERROR: extensions was a list')
        return
    if EXTENSION_KEY not in extensions:
        print('WARNING: No extension in document extensions.')
        return
    extension = extensions[EXTENSION_KEY]
    if isinstance(extension, list):
        print('ERROR: extension was a list')
        return
    if CONTENT_KEY not in extension:
        print('WARNING: Content inot in document extension.')
        return
    content = extension[CONTENT_KEY]
    if isinstance(content, list):
        print('ERROR: content was a list')
        return
    if EFORMS_KEY not in content:
        print('WARNING: Eform not in document extension content.')
        return
    eforms = content[EFORMS_KEY]
    if isinstance(eforms, list):
        print('ERROR: eforms was a list')
        return
    extension_info = {}
    incorporate_modification(extension_info, eforms)
    get_text_assignment(extension_info, 'transmission_date', eforms,
                        TRANSMISSION_DATE_KEY)
    get_text_assignment(extension_info, 'transmission_time', eforms,
                        TRANSMISSION_TIME_KEY)
    incorporate_notice_result(extension_info, eforms)
    if NOTICE_SUBTYPE_KEY in eforms:
        extension_info['notice_subtype'] = get_text(
            eforms[NOTICE_SUBTYPE_KEY]['cbc:SubTypeCode'])
    if ORGANIZATIONS_KEY in eforms:
        organizations = eforms[ORGANIZATIONS_KEY]
        if isinstance(organizations, list):
            print('ERROR: organizations was a list')
            return
        orgs_info = []
        for organization in convert_to_list(organizations[ORGANIZATION_KEY]):
            org_info = {}
            get_text_assignment(org_info, 'group_lead', organization,
                                'efbc:GroupLeadIndicator')
            if COMPANY_KEY not in organization:
                print('ERROR: Could not find company key in {}'.format(
                    list(organization.keys())))
                return
            orgs_info.append(parse_company(organization[COMPANY_KEY]))
        extension_info['organizations'] = orgs_info
    if PUBLICATION_KEY in eforms:
        publication = eforms[PUBLICATION_KEY]
        pub_info = {}
        get_text_assignment(pub_info, 'id', publication,
                            'efbc:NoticePublicationID')
        get_text_assignment(pub_info, 'gazette_id', publication,
                            'efbc:GazetteID')
        get_text_assignment(pub_info, 'date', publication,
                            'efbc:PublicationDate')
        extension_info['publication'] = pub_info
    else:
        print('WARNING: No publication info in document extension.')

    info['extension'] = extension_info


def incorporate_doc_id(data, output):
    if 'extension' in data:
        extension = data['extension']
        if 'publication' in extension:
            publication = extension['publication']
            if 'id' in publication:
                output['doc_id'] = publication['id'].strip('"')


def postprocess_output(data, output):
    # Extract the entities
    entities_set = set()
    if 'extension' in data:
        extension = data['extension']
        if 'organizations' in extension:
            for org in extension['organizations']:
                if 'name' in org:
                    entities_set.add(canonicalize(org['name']))
    output['entities'] = sorted(list(entities_set))

    # Set the first and last action dates.
    # TODO(Jack Poulson): Incorporate a more expressive date range.
    if 'extension' in data:
        extension = data['extension']
        if 'publication' in extension:
            publication = extension['publication']
            output['first_action_date'] = publication['date']
            output['last_action_date'] = publication['date']


def preprocess_contract_award_notice(doc,
                                     processed_docs,
                                     maps,
                                     countries=sorted(list(COUNTRIES.keys())),
                                     skipped_countries=None):
    for notice_key in AWARD_NOTICE_KEYS:
        if notice_key in doc:
            doc = doc[notice_key]
            break

    output = {}
    output['is_legacy'] = False
    output['data'] = {}

    data = {}
    incorporate_extensions(data, doc)
    incorporate_doc_id(data, output)

    country = get_country(data, doc)
    if country not in countries:
        if skipped_countries is not None:
            if country not in skipped_countries:
                skipped_countries[country] = 0
            skipped_countries[country] += 1
        return
    if country not in COUNTRIES:
        print('ERROR: {} not in COUNTRIES'.format(country))

    # TODO(Jack Poulson): Consider getting a default value from the country.
    language_code = get_language_code(doc)

    data['notice_id'] = get_notice_id(doc)
    data['notice_type'] = get_notice_type_code(doc)
    data['issue_date'] = get_text(doc['cbc:IssueDate'])
    data['issue_time'] = get_text(doc['cbc:IssueTime'])
    incorporate_contracting_parties(data, doc)
    incorporate_tendering_terms(data, doc)
    incorporate_tendering_process(data, doc)
    incorporate_project(data, doc)
    incorporate_project_lots(data, doc)

    output['data'][language_code] = data

    postprocess_output(data, output)

    if country not in processed_docs:
        processed_docs[country] = []
    processed_docs[country].append(output)


def preprocess_information_notice(doc,
                                  processed_docs,
                                  maps,
                                  countries=sorted(list(COUNTRIES.keys())),
                                  skipped_countries=None):
    for notice_key in INFORMATION_NOTICE_KEYS:
        if notice_key in doc:
            doc = doc[notice_key]
            break

    output = {}
    output['is_legacy'] = False
    output['data'] = {}

    data = {}
    incorporate_extensions(data, doc)
    incorporate_doc_id(data, output)

    country = get_country(data, doc)
    if country not in countries:
        if skipped_countries is not None:
            if country not in skipped_countries:
                skipped_countries[country] = 0
            skipped_countries[country] += 1
        return
    if country not in COUNTRIES:
        print('ERROR: {} not in COUNTRIES'.format(country))

    # TODO(Jack Poulson): Consider getting a default value from the country.
    language_code = get_language_code(doc)

    data['notice_id'] = get_notice_id(doc)
    data['notice_type'] = get_notice_type_code(doc)
    data['issue_date'] = get_text(doc['cbc:IssueDate'])
    data['issue_time'] = get_text(doc['cbc:IssueTime'])
    incorporate_contracting_parties(data, doc)
    incorporate_tendering_terms(data, doc)
    incorporate_tendering_process(data, doc)
    incorporate_project(data, doc)
    incorporate_project_lots(data, doc)

    output['data'][language_code] = data

    postprocess_output(data, output)

    if country not in processed_docs:
        processed_docs[country] = []
    processed_docs[country].append(output)


def preprocess_contract_notice(doc,
                               processed_docs,
                               maps,
                               countries=sorted(list(COUNTRIES.keys())),
                               skipped_countries=None):
    for notice_key in NOTICE_KEYS:
        if notice_key in doc:
            doc = doc[notice_key]
            break

    output = {}
    output['is_legacy'] = False
    output['data'] = {}

    data = {}
    incorporate_extensions(data, doc)
    incorporate_doc_id(data, output)

    country = get_country(data, doc)
    if country not in countries:
        if skipped_countries is not None:
            if country not in skipped_countries:
                skipped_countries[country] = 0
            skipped_countries[country] += 1
        return
    if country not in COUNTRIES:
        print('ERROR: {} not in COUNTRIES'.format(country))

    # TODO(Jack Poulson): Consider getting a default value from the country.
    language_code = get_language_code(doc)

    data['notice_id'] = get_notice_id(doc)
    data['notice_type'] = get_notice_type_code(doc)
    data['issue_date'] = get_text(doc['cbc:IssueDate'])
    data['issue_time'] = get_text(doc['cbc:IssueTime'])
    incorporate_contracting_parties(data, doc)
    incorporate_tendering_terms(data, doc)
    incorporate_tendering_process(data, doc)
    incorporate_project(data, doc)
    incorporate_project_lots(data, doc)

    output['data'][language_code] = data

    postprocess_output(data, output)

    if country not in processed_docs:
        processed_docs[country] = []
    processed_docs[country].append(output)


def preprocess_xml(doc,
                   processed_docs,
                   maps,
                   countries=sorted(list(COUNTRIES.keys())),
                   skipped_countries=None):
    if has_a_key(doc, AWARD_NOTICE_KEYS):
        preprocess_contract_award_notice(doc, processed_docs, maps, countries,
                                         skipped_countries)
    elif has_a_key(doc, NOTICE_KEYS):
        preprocess_contract_notice(doc, processed_docs, maps, countries,
                                   skipped_countries)
    elif has_a_key(doc, INFORMATION_NOTICE_KEYS):
        preprocess_information_notice(doc, processed_docs, maps, countries,
                                      skipped_countries)
    else:
        print('ERROR: Unrecognized modern doc type with top-level keys of {}.'.
              format(sorted(list(doc.keys()))))


def queue_translation_data(doc,
                           doc_index,
                           state,
                           translation_map,
                           country_data,
                           use_translation_cache=True):
    # TODO(Jack Poulson): Add this in after getting UI working for newer
    # format.
    pass
