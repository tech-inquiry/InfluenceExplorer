#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

# We make use of ISO 3166-2 for two-letter country codes.
COUNTRIES = {
    '1a': {
        'name': 'Republic of Kosovo',
        'language': 'sq',
        'entity': 'government of the republic of kosovo'
    },
    'al': {
        'name': 'Republic of Albania',
        'language': 'sq',
        'entity': 'government of the republic of albania'
    },
    'am': {
        'name': 'Republic of Armenia',
        'language': 'hy',
        'entity': 'government of the republic of armenia'
    },
    'ar': {
        'name': 'Argentine Republic',
        'language': 'es',
        'entity': 'government of the argentine republic'
    },
    'at': {
        'name': 'Austria',
        'language': 'de',
        'entity': 'government of austria'
    },
    'ba': {
        'name': 'Bosnia and Herzegovina',
        'language': 'bs',
        'entity': 'government of bosnia and herzegovina'
    },
    'bd': {
        'name': 'Bangladesh',
        'language': 'bn',
        'entity': 'government of the people\'s republic of bangladesh'
    },
    'be': {
        'name': 'Kingdom of Belgium',
        'language': 'fr',
        'entity': 'government of the kingdom of belgium'
    },
    'bg': {
        'name': 'Republic of Bulgaria',
        'language': 'bg',
        'entity': 'government of the republic of bulgaria'
    },
    'bj': {
        'name': 'Republic of Benin',
        'language': 'fr',
        'entity': 'government of the republic of benin'
    },
    'bo': {
        'name': 'Plurinational State of Bolivia',
        'language': 'es',
        'entity': 'government of the plurinational state of bolivia'
    },
    'by': {
        'name': 'Republic of Belarus',
        'language': 'ru',
        'entity': 'government of the republic of belarus'
    },
    'cg': {
        'name': 'Republic of Congo',
        'language': 'fr',
        'entity': 'government of the republic of congo'
    },
    'ch': {
        'name': 'Switzerland',
        'language': 'de',
        'entity': 'government of switzerland'
    },
    'ci': {
        'name': 'Republic of Cote d\'Ivoire',
        'language': 'fr',
        'entity': 'government of the republic of cote d\'ivoire'
    },
    'cm': {
        'name': 'Republic of Cameroon',
        'language': 'fr',
        'entity': 'government of the republic of cameroon'
    },
    'cn': {
        'name': 'People\'s Republic of China',
        'language': 'cn',
        'entity': 'government of the people\'s republic of china'
    },
    'co': {
        'name': 'Republic of Colombia',
        'language': 'es',
        'entity': 'government of the republic of colombia'
    },
    'cy': {
        'name': 'Republic of Cyprus',
        'language': 'el',
        'entity': 'government of the republic of cyprus'
    },
    'cz': {
        'name': 'Czech Republic',
        'language': 'cs',
        'entity': 'government of the czech republic'
    },
    'de': {
        'name': 'Federal Republic of Germany',
        'language': 'de',
        'entity': 'government of the federal republic of germany'
    },
    'dk': {
        'name': 'Denmark',
        'language': 'da',
        'entity': 'government of denmark'
    },
    'dz': {
        'name': 'People\'s Democratic Republic of Algeria',
        'language': 'ar',
        'entity': 'government of the people\'s democratic republic of algeria'
    },
    'ec': {
        'name': 'Republic of Ecuador',
        'language': 'es',
        'entity': 'government of the republic of ecuador'
    },
    'ee': {
        'name': 'Republic of Estonia',
        'language': 'et',
        'entity': 'government of the republic of estonia'
    },
    'eg': {
        'name': 'Arab Republic of Egypt',
        'language': 'ar',
        'entity': 'government of the arab republic of egypt'
    },
    'es': {
        'name': 'Kingdom of Spain',
        'language': 'es',
        'entity': 'government of the kingdom of spain'
    },
    'et': {
        'name': 'Federal Democratic Republic of Ethiopia',
        'language': 'am',
        'entity': 'government of the federal democratic republic of ethiopia'
    },
    'fi': {
        'name': 'Republic of Finland',
        'language': 'fi',
        'entity': 'government of the republic of finland'
    },
    'fr': {
        'name': 'France',
        'language': 'fr',
        'entity': 'government of france'
    },
    'ge': {
        'name': 'Georgia',
        'language': 'ka',
        'entity': 'government of georgia'
    },
    'gn': {
        'name': 'Republic of Guinea',
        'language': 'fr',
        'entity': 'government of the republic of guinea'
    },
    'gp': {
        'name': 'Guadeloupe',
        'language': 'fr',
        'entity': 'department of guadeloupe'
    },
    'gr': {
        'name': 'Hellenic Republic',
        'language': 'el',
        'entity': 'government of the hellenic republic'
    },
    'hr': {
        'name': 'Republic of Croatia',
        'language': 'hr',
        'entity': 'government of the republic of croatia'
    },
    'ht': {
        'name': 'Republic of Haiti',
        'language': 'fr',
        'entity': 'government of the republic of haiti'
    },
    'hu': {
        'name': 'Hungary',
        'language': 'hu',
        'entity': 'government of hungary'
    },
    'ie': {
        'name': 'Republic of Ireland',
        'language': 'ga',
        'entity': 'government of the republic of ireland'
    },
    'il': {
        'name': 'Israel',
        'language': 'he',
        'entity': 'government of israel'
    },
    'in': {
        'name': 'Republic of India',
        'language': 'hi',
        'entity': 'government of the republic of india'
    },
    'is': {
        'name': 'Iceland',
        'language': 'is',
        'entity': 'government of iceland'
    },
    'it': {
        'name': 'Italy',
        'language': 'it',
        'entity': 'government of italy'
    },
    'ke': {
        'name': 'Republic of Kenya',
        'language': 'en',
        'entity': 'government of the republic of kenya'
    },
    'kg': {
        'name': 'Kyrgyz Republic',
        'language': 'ru',
        'entity': 'government of the kyrgyz republic'
    },
    'kn': {
        'name': 'Federation of Saint Christopher and Nevis',
        'language': 'en',
        'entity': 'government of the federation of saint christopher and nevis'
    },
    'kz': {
        'name': 'Republic of Kazakhstan',
        'language': 'kk',
        'entity': 'government of the republic of kazakhstan'
    },
    'la': {
        'name': 'Lao People\'s Democratic Republic',
        'language': 'lo',
        'entity': 'government of the lao people\'s democratic republic'
    },
    'lb': {
        'name': 'Republic of Lebanon',
        'language': 'ar',
        'entity': 'government of the republic of lebanon'
    },
    'li': {
        'name': 'Principality of Liechtenstein',
        'language': 'de',
        'entity': 'government of the principality of liechtenstein'
    },
    'lt': {
        'name': 'Republic of Lithuania',
        'language': 'lt',
        'entity': 'government of the republic of lithuania'
    },
    'lu': {
        'name': 'Grand Duchy of Luxembourg',
        'language': 'lb',
        'entity': 'government of the grand duchy of luxembourg'
    },
    'lv': {
        'name': 'Latvia',
        'language': 'lv',
        'entity': 'government of the republic of latvia'
    },
    'ma': {
        'name': 'Kingdom of Morocco',
        'language': 'ar',
        'entity': 'government of the kingdom of morocco'
    },
    'md': {
        'name': 'Republic of Moldova',
        'language': 'ro',
        'entity': 'government of the republic of moldova'
    },
    'me': {
        'name': 'Montenegro',
        'language': 'cnr',
        'entity': 'government of montenegro'
    },
    'mg': {
        'name': 'Republic of Madagascar',
        'language': 'fr',
        'entity': 'government of the republic of madagascar'
    },
    'mk': {
        'name': 'Republic of North Macedonia',
        'language': 'en',
        'entity': 'government of the republic of north macedonia'
    },
    'ml': {
        'name': 'Republic of Mali',
        'language': 'fr',
        'entity': 'government of the republic of mali'
    },
    'mq': {
        'name': 'Martinique',
        'language': 'fr',
        'entity': 'department of martinique'
    },
    'mt': {
        'name': 'Republic of Malta',
        'language': 'en',
        'entity': 'government of the republic of malta'
    },
    'mw': {
        'name': 'Republic of Malawi',
        'language': 'en',
        'entity': 'government of the republic of malawi'
    },
    'ni': {
        'name': 'Republic of Nicaragua',
        'language': 'es',
        'entity': 'government of the republic of nicaragua'
    },
    'nl': {
        'name': 'Kingdom of The Netherlands',
        'language': 'nl',
        'entity': 'government of the kingdom of the netherlands'
    },
    'no': {
        'name': 'Norway',
        'language': 'en',
        'entity': 'government of norway'
    },
    'np': {
        'name': 'Federal Democratic Republic of Nepal',
        'language': 'ne',
        'entity': 'government of the federal democratic republic of nepal'
    },
    'pl': {
        'name': 'Poland',
        'language': 'pl',
        'entity': 'government of poland'
    },
    'ps': {
        'name': 'State of Palestine',
        'language': 'ar',
        'entity': 'government of the state of palestine'
    },
    'pt': {
        'name': 'Portuguese Republic',
        'language': 'pt',
        'entity': 'government of the portuguese republic'
    },
    'py': {
        'name': 'Republic of Paraguay',
        'language': 'es',
        'entity': 'government of the republic of paraguay'
    },
    're': {
        'name': 'Réunion',
        'language': 'fr',
        'entity': 'department of réunion'
    },
    'ro': {
        'name': 'Romania',
        'language': 'ro',
        'entity': 'government of romania'
    },
    'rs': {
        'name': 'Republic of Serbia',
        'language': 'ro',
        'entity': 'government of the republic of serbia'
    },
    'ru': {
        'name': 'Russian Federation',
        'language': 'ru',
        'entity': 'government of the russian federation'
    },
    'se': {
        'name': 'Sweden',
        'language': 'sv',
        'entity': 'government of sweden'
    },
    'si': {
        'name': 'Republic of Slovenia',
        'language': 'sl',
        'entity': 'government of the republic of slovenia'
    },
    'sk': {
        'name': 'Slovak Republic',
        'language': 'sk',
        'entity': 'government of the slovak republic'
    },
    'sl': {
        'name': 'Sierra Leone',
        'language': 'en',
        'entity': 'government of the republic of sierra leone'
    },
    'sn': {
        'name': 'Republic of Senegal',
        'language': 'fr',
        'entity': 'government of the republic of senegal'
    },
    'td': {
        'name': 'Republic of Chad',
        'language': 'fr',
        'entity': 'government of the republic of chad'
    },
    'th': {
        'name': 'Kingdom of Thailand',
        'language': 'th',
        'entity': 'government of the kingdom of thailand'
    },
    'tj': {
        'name': 'Republic of Tajikistan',
        'language': 'tg',
        'entity': 'government of the republic of tajikistan'
    },
    'tn': {
        'name': 'Republic of Tunisia',
        'language': 'aeb',
        'entity': 'government of the republic of tunisia'
    },
    'tr': {
        'name': 'Republic of Turkey',
        'language': 'tr',
        'entity': 'government of the republic of turkey'
    },
    'tz': {
        'name': 'United Republic of Tanzania',
        'language': 'en',
        'entity': 'government of the united republic of tanzania'
    },
    'ua': {
        'name': 'Ukraine',
        'language': 'uk',
        'entity': 'government of ukraine'
    },
    'ug': {
        'name': 'Republic of Uganda',
        'language': 'en',
        'entity': 'government of the republic of uganda'
    },
    'uk': {
        'name': 'United Kingdom',
        'language': 'en',
        'entity': 'government of the united kingdom'
    },
    'us': {
        'name': 'United States',
        'language': 'en',
        'entity': 'government of the united states'
    },
    'yt': {
        'name': 'Mayotte',
        'language': 'fr',
        'entity': 'department of mayotte'
    },
    'zm': {
        'name': 'Republic of Zambia',
        'language': 'en',
        'entity': 'government of the republic of zambia'
    }
}

# This list of language preference scores is somewhat arbitrary.
LANGUAGE_PREFERENCES = {
    'EN': 10,  # English
    'ES': 8,  # Spanish
    'FR': 8,  # French
    'DA': 7,  # Danish
    'DE': 7,  # German
    'FI': 7,  # Finnish
    'IT': 7,  # Italian
    'NL': 7,  # Dutch
    'PT': 7,  # Portugese
    'SV': 7,  # Swedish
    'BG': 6,  # Bulgarian
    'CS': 6,  # Czech
    'ET': 6,  # Estonian
    'EL': 6,  # Greek
    'HU': 6,  # Hungarian
    'LV': 6,  # Latvian
    'PL': 6,  # Polish
    'SK': 6,  # Slovak
    'RO': 4,  # Romanian
    'HR': 1,  # Croatian
    'LT': 1,  # Lithuanian
    'SL': 1,  # Slovenian
}


def clean_nones(value):
    """
    Recursively remove all None values from dictionaries and lists, and returns
    the result as a new dictionary or list.
    See: https://stackoverflow.com/a/60124334
    """
    if isinstance(value, list):
        return [clean_nones(x) for x in value if x is not None]
    elif isinstance(value, dict):
        return {
            key: clean_nones(val)
            for key, val in value.items() if val is not None
        }
    else:
        return value


def convert_to_list(item):
    if isinstance(item, list):
        return item
    else:
        return [item].copy()


def make_list(item, key):
    if item is None or key not in item:
        return
    if not isinstance(item[key], list):
        item[key] = [item[key]]
    # Remove any None items from the list.
    item[key] = list(filter(None, item[key]))


def rename_dict_key(obj, old_key, new_key):
    if obj is None or old_key not in obj:
        return None
    if obj[old_key] is None:
        del obj[old_key]
        return None
    obj[new_key] = obj.pop(old_key)
    return obj[new_key]


def rename_dict_keys(obj, key_map):
    for old_key, new_key in key_map.items():
        rename_dict_key(obj, old_key, new_key)
