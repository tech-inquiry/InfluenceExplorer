#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# A utility for retrieving UK Contract Finder data in flattened CSV form,
# unflattening via the Open Contracting Data Standard 'flatten' tool, and then
# simplifying for export into Postgres.
#
# Note that, while the 'releases' and 'awards' fields are designed as arrays,
# they are, in every instance so far, singletons. We therefore flatten the
# 'release' members into the main data structure and then turn the first (and
# only) entry of 'awards' into an item called 'award' if it exists.
#
# It is also important to note that the UK CSV export on 2017.12.04 is
# corrupted (e.g., the first unnamed field in the header is missing).
#
# Copyright (c) 2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import datetime
import flattentool
import glob
import json
import os
import pandas as pd
import re
import traceback

import util.dates
import util.dict_path
import util.entities
import util.postgres

from tqdm import tqdm
from util.entities import canonicalize, normalize, normalization_map_helper

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
CONTRACTS_DIR = os.path.join(DATA_DIR, 'uk_contracts')
STAGING_DIR = os.path.join(WORKSTATION_DIR, 'staging')
CONTRACTS_STAGING_DIR = os.path.join(STAGING_DIR, 'uk_contracts')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

UK_CONTRACTS_BASE_URL = 'https://www.contractsfinder.service.gov.uk/harvester/Notices/Data/CSV'

# It is unfortunately common for 2017-2018 CSVs to have completely
# misformatted rows. We detect them through checking if the uri matches
# a particular pattern.
URI_PATTERN = 'https://www.contractsfinder.service.gov.uk'

# 'flatten-tool' will ignore all but the last supplier out of a list (sometimes
# there are more than 30!) since the UK government often supplies them with
# identical 'id' fields. We therefore drop these useless identifiers in the
# staging process before calling 'flatten-tool'.
SUPPLIER_ID_REGEX = 'releases/0/awards/\d/suppliers/\d+/id$'

SUPPLIER_IDENTIFIER_ID_REGEX = (
    'releases/0/awards/\d/suppliers/\d+/identifier/id$')

TABLE_NAME = 'uk_contract_filing'
TABLE_TYPES = {
    'uri': 'TEXT',
    'publisher': 'JSONB',
    'published_date': 'TIMESTAMP',
    'tender': 'JSONB',
    'buyer': 'JSONB',
    'award': 'JSONB',
    'deadline_date': 'TIMESTAMP',
    'parties': 'JSONB'
}
UNIQUE_COLUMNS = ['uri']

EXPORT_CSV = '{}/{}.csv'.format(EXPORT_DIR, TABLE_NAME)

if not os.path.exists(CONTRACTS_DIR):
    os.makedirs(CONTRACTS_DIR)
if not os.path.exists(CONTRACTS_STAGING_DIR):
    os.makedirs(CONTRACTS_STAGING_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)

UNICODE_REPLACEMENTS = {
    '\u0002': '',
    '\u0003': '',
    '\u0005': '',
    '\u0006': '',
    '\u0007': '',
    '\u000e': '',
    '\u0010': '',
    '\u0013': '',
    '\u0014': '',
    '\u0015': '',
    '\u0016': '',
    '\u0017': '',
    '\u0018': '',
    '\u001b': '',
    '\u001c': '',
    '\u001d': '',
    '\u001e': '',
    '\u001f': ''
}


def sanitize(value):
    for pattern, result in UNICODE_REPLACEMENTS.items():
        value = value.replace(pattern, result)
    return value.strip()


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['uk', 'procurement'], entities)


def get_agency_normalization_map(entities=None):
    '''Inverts the agency spellings into a normalization map.'''
    return normalization_map_helper(['uk', 'procurement'],
                                    entities,
                                    primary_key='agencyNames')


# NOTE: We currently assume the amount is in GBP.
#
# TODO(Jack Poulson): Set up rough reference conversion rates.
def simplified_value(filing):
    '''Returns the award value of a filing.'''
    if ('award' not in filing or filing['award'] is None
            or 'value' not in filing['award']):
        return 0.
    return float(filing['award']['value']['amount'])


def supplier_names(filing, entities, normalization, inclusive=False):
    '''Returns the list of normalized supplier names of a filing.

    Args:
      filing: The simplified dictionary of the tender.
      entities: The map from normalized entity names to their profiles.
      normalization: The map from alternate entity names to normalized form.
      inclusive: Whether to reduce to ultimate parent entity names.
    '''
    if not 'award' in filing or filing['award'] is None:
        return []
    suppliers = []
    for supplier in filing['award']['suppliers']:
        name = normalize(supplier['name'], normalization)
        if inclusive:
            name = util.entities.ultimate_parent(name, entities)
        suppliers.append(name)
    return sorted(list(set(suppliers)))


def simplified_filings_to_entity_amounts(filings, inclusive=True):
    '''Returns the total contract amounts associated with each entity.

    Args:
      filings: The list of simplified filings.
      inclusive: Whether amounts should be rolled up to their ultimate parent.
    '''
    entities = util.entities.get_entities()
    normalization = get_normalization_map(entities)
    amounts = {}
    for filing in filings:
        value = simplified_value(filing)
        if value == 0.:
            continue
        suppliers = supplier_names(filing, entities, normalization, inclusive)
        for supplier in suppliers:
            if not supplier in amounts:
                amounts[supplier] = 0.
            amounts[supplier] += value / len(suppliers)

    return {
        k: v
        for k, v in sorted(
            amounts.items(), key=lambda item: item[1], reverse=True)
    }


def stage_csv(filename, filename_staged, verbose=False):
    if verbose:
        print('  Reading CSV from {}'.format(filename))
    df = pd.read_csv(filename)

    df = df.drop(df.columns[df.columns.str.contains(SUPPLIER_ID_REGEX)],
                 axis=1)

    # Prevent Pandas from treating the identifier ID integers as floats.
    identifier_ids = list(
        df.columns[df.columns.str.contains(SUPPLIER_IDENTIFIER_ID_REGEX)])
    for identifier_id in identifier_ids:
        df[identifier_id] = (df[identifier_id].astype('str').replace(
            '\.0$', '', regex=True))

    properURI = df['uri'].str.contains(URI_PATTERN, na=False, regex=False)
    df = df[properURI]

    if verbose:
        print('  Writing CSV to {}'.format(filename_staged))
    df.to_csv(filename_staged)


def unflatten(input_name, output_name):
    flattentool.unflatten(input_name=input_name,
                          output_name=output_name,
                          input_format='csv',
                          root_id='ocid',
                          root_is_list=True)


def convert_csv_to_json(filenames):
    for filename in filenames:
        basename = filename[:-4]
        json_filename = basename + '.json'
        df = pd.read_csv(filename)
        df.to_json(json_name)


def combine_json(json_files, output_json):
    combined_data = []
    for filename in json_files:
        data = json.load(open(filename))
        combined_data += data
    with open(output_json, 'w') as outfile:
        json.dump(combined_data, outfile, indent=1)


def convert_all_csv_to_json():
    convert_csv_to_json(glob.glob('{}/**.csv'.format(CONTRACTS_DIR)))


def simplify_unflattened_json(data):
    KEYS_TO_DELETE = [
        '', 'Unnamed: 0', 'version', 'extensions', 'license',
        'publicationPolicy'
    ]
    for item in data:
        for key in KEYS_TO_DELETE:
            if key in item:
                del item[key]

        if 'publishedDate' in item:
            item['published_date'] = item['publishedDate']
            del item['publishedDate']
        else:
            item['published_date'] = None

        if 'deadlineDate' in item:
            item['deadline_date'] = item['deadlineDate']
            del item['deadlineDate']
        else:
            item['deadline_date'] = None

        if 'releases' in item:
            releases = item['releases']
            if len(releases):
                if len(releases) > 1:
                    print(releases)
                    print("WARNING: 'releases' was of length {}".format(
                        len(releases)))
                release = releases[0]
                #if 'ocid' in release:
                #    item['ocid'] = release['ocid']
                #if 'id' in release:
                #    item['id'] = release['id']
                if 'tender' in release:
                    item['tender'] = release['tender']
                if 'buyer' in release:
                    item['buyer'] = release['buyer']
                if 'awards' in release:
                    awards = release['awards']
                    if len(awards):
                        if len(awards) > 1:
                            print(awards)
                            print("WARNING: 'awards' was of length {}".format(
                                len(awards)))
                        if awards[0]:
                            item['award'] = awards[0]
                        else:
                            print('WARNING: Initial award in release was {}'.
                                  format(awards[0]))
                        if 'datePublished' in item[
                                'award'] and 'published_date' not in item:
                            item['published_date'] = item['award'][
                                'datePublished']
                if 'parties' in release:
                    item['parties'] = release['parties']
            del item['releases']

        if 'publisher' not in item:
            item['publisher'] = None
        if 'tender' not in item:
            item['tender'] = 'null'
        if 'buyer' not in item:
            item['buyer'] = 'null'
        if 'award' in item:
            award = item['award']
            if 'datePublished' in award:
                item['published_date'] = award['datePublished']
            if 'suppliers' in award:
                suppliers = []
                for supplier in award['suppliers']:
                    if 'name' in supplier:
                        supplier['name'] = sanitize(supplier['name'])
                    if 'address' in supplier:
                        if 'streetAddress' in supplier['address']:
                            supplier['address']['streetAddress'] = sanitize(
                                supplier['address']['streetAddress'])
                    if 'name' in supplier or ('identifier' in supplier
                                              and supplier['identifier']['id']
                                              != 'nan'):
                        suppliers.append(supplier)
                award['suppliers'] = suppliers
            if award:
                item['award'] = award
            else:
                print('WARNING: award was {}'.format(award))
        else:
            item['award'] = 'null'
        if 'parties' not in item:
            item['parties'] = []


def simplified_json_to_csv(json_filename, csv_filename):
    '''Converts a JSON representation of cases into a CSV.'''
    date_columns = ['published_date', 'deadline_date']
    json_columns = ['publisher', 'tender', 'buyer', 'award', 'parties']
    export_columns = [
        'uri', 'publisher', 'published_date', 'tender', 'buyer', 'award',
        'deadline_date', 'parties'
    ]

    df = pd.read_json(json_filename)

    for date_column in date_columns:
        print('Converting date column {}'.format(date_column))
        df[date_column] = pd.to_datetime(df[date_column], errors='coerce')

    df = util.postgres.preprocess_dataframe(df,
                                            json_columns=json_columns,
                                            unique_columns=['uri'])
    util.postgres.df_to_csv(df, csv_filename, columns=export_columns)


def simplify_unflattened_json_file(unflattened_json, simplified_json):
    '''Outputs a Postgres-importable CSV from unflattened contract JSON.'''
    data = json.load(open(unflattened_json))
    simplify_unflattened_json(data)
    with open(simplified_json, 'w') as outfile:
        json.dump(data, outfile, indent=1)


def retrieve_days(day_tuples,
                  contracts_dir=CONTRACTS_DIR,
                  staging_dir=CONTRACTS_STAGING_DIR,
                  export_csv=EXPORT_CSV,
                  verbose=True,
                  download_only=False):
    '''Retrieves a CSV for a single day of UK procurement requests.'''

    json_simplified_files = []
    with tqdm(total=len(day_tuples)) as pbar:
        for year, month, day in day_tuples:
            # We have yet to determine how to fix the corruption for this day.
            # Simply prepending a row index for the first unnamed field does not
            # work, as the 'uri' column is loaded as the publication date.
            if year == 2017 and month == 12 and day == 4:
                print('Avoiding corruption on 2017.12.04')
                pbar.update(1)
                continue

            month_str = str(month).zfill(2)
            day_str = str(day).zfill(2)

            url = '{}/{}/{}/{}'.format(UK_CONTRACTS_BASE_URL, year, month_str,
                                       day_str)
            csv_orig = '{}/{}.{}.{}_orig.csv'.format(contracts_dir, year,
                                                     month_str, day_str)
            csv_staged = '{}/{}.{}.{}_staged.csv'.format(
                staging_dir, year, month_str, day_str)
            json_simplified = '{}/{}.{}.{}_simplified.json'.format(
                contracts_dir, year, month_str, day_str)

            date_str = '{}-{}-{}'.format(year, month_str, day_str)
            try:
                df = pd.read_csv(url, on_bad_lines='warn')
                df.to_csv(csv_orig)
                print('  Removing CSVs from {}.'.format(staging_dir))
                for filename in glob.glob('{}/**.csv'.format(staging_dir)):
                    os.remove(filename)
                print('  Staging {}.'.format(date_str))
                stage_csv(csv_orig, csv_staged)
                print('  Unflattening {}.'.format(date_str))
                json_unflattened = '{}/unflattened.json'.format(staging_dir)
                unflatten(staging_dir, json_unflattened)
                print('  Removing CSVs from {}.'.format(staging_dir))
                for filename in glob.glob('{}/**.csv'.format(staging_dir)):
                    os.remove(filename)
                print('  Simplifying {}.'.format(date_str))
                simplify_unflattened_json_file(json_unflattened,
                                               json_simplified)
                json_simplified_files.append(json_simplified)
                pbar.update(1)
            except Exception as e:
                traceback.print_exc()
                print(e)
                print('Could not process {}.'.format(date_str))
                pbar.update(1)
                continue

    if len(json_simplified_files):
        combined_json = '{}/simplified.json'.format(staging_dir)
        combine_json(json_simplified_files, combined_json)
        simplified_json_to_csv(combined_json, export_csv)
        for filename in glob.glob('{}/**.csv'.format(staging_dir)):
            os.remove(filename)


def retrieve_day(date,
                 contracts_dir=CONTRACTS_DIR,
                 staging_dir=CONTRACTS_STAGING_DIR,
                 export_csv=EXPORT_CSV,
                 verbose=True,
                 download_only=False):
    '''Retrieves a CSV for a single day of UK procurement requests.'''
    year, month, day = util.dates.date_to_tuple(date)
    retrieve_days([(year, month, day)], contracts_dir, staging_dir, export_csv,
                  verbose, download_only)


def retrieve(start_date=None,
             end_date=None,
             contracts_dir=CONTRACTS_DIR,
             staging_dir=CONTRACTS_STAGING_DIR,
             export_csv=EXPORT_CSV,
             verbose=True,
             download_only=False,
             window_days=31):
    '''Retrieves a CSV for a date range of UK procurement requests.'''
    if start_date is None:
        today = datetime.date.today()
        start_year, start_month, start_day = util.dates.earlier_day(
            today.year, today.month, today.day, window_days)
        start_date = util.dates.tuple_to_date(start_year, start_month,
                                              start_day)
    if end_date is None:
        today = datetime.date.today()
        end_date = util.dates.tuple_to_date(today.year, today.month, today.day)

    print('Retrieving from {} to {}.'.format(start_date, end_date))
    day_tuples = util.dates.date_range_tuples(start_date, end_date)
    retrieve_days(day_tuples, contracts_dir, staging_dir, export_csv, verbose,
                  download_only)


def retrieve_month(year,
                   month,
                   contracts_dir=CONTRACTS_DIR,
                   staging_dir=CONTRACTS_STAGING_DIR,
                   export_csv=EXPORT_CSV,
                   verbose=True,
                   download_only=False):
    '''Retrieves a CSV for a month of UK procurement requests.'''
    start_date = '{}-{}-01'.format(year, str(month).zfill(2))
    end_month = 1 if month == 12 else month + 1
    end_year = year + 1 if month == 12 else year
    end_date = '{}-{}-01'.format(end_year, str(end_month).zfill(2))
    return retrieve(start_date,
                    end_date,
                    contracts_dir=contracts_dir,
                    staging_dir=staging_dir,
                    export_csv=export_csv,
                    verbose=verbose,
                    download_only=download_only)


def build_cache(end_date,
                contracts_dir=CONTRACTS_DIR,
                staging_dir=CONTRACTS_STAGING_DIR,
                export_csv=EXPORT_CSV,
                verbose=True):
    FIRST_CONTRACT_DATE = '2015-01-01'
    day_tuples = util.dates.date_range_tuples(FIRST_CONTRACT_DATE, end_date)
    day_tuples.reverse()
    for day_tuple in day_tuples:
        try:
            date = util.dates.tuple_to_date(*day_tuple)
            retrieve_day(date,
                         contracts_dir,
                         staging_dir,
                         export_csv,
                         verbose,
                         download_only=True)
        except Exception as e:
            print(e)
            print('Could not retrieve for {}.{}.{}'.format(*day_tuple))


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(TABLE_NAME,
                                       TABLE_TYPES,
                                       serial_id=True,
                                       constraints={'unique': 'UNIQUE (uri)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_award_index ON {} USING GIN (
  (lower(award->>'suppliers')::JSONB) jsonb_path_ops
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(uri),             '') || ' ' ||
    coalesce(lower(publisher::text), '') || ' ' ||
    coalesce(lower(tender::text),    '') || ' ' ||
    coalesce(lower(buyer::text),     '') || ' ' ||
    coalesce(lower(award::text),     '')
  )
);
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=EXPORT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    FORCE_NULL_COLUMNS = [
        'published_date', 'deadline_date', 'award', 'parties'
    ]
    TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
