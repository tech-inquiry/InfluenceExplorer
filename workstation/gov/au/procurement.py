#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import copy
import datetime
import json
import os
import pandas as pd
import re
import requests

from tqdm import tqdm

import util.dates
import util.dict_path
import util.entities
import util.postgres
import util.tor

from util.entities import canonicalize, normalization_map_helper

TABLE_TYPES = {
    'publisher': 'JSONB',
    'published_date': 'TIMESTAMP',
    'filing_id': 'VARCHAR(255)',
    'date': 'TIMESTAMP',
    'initiation_type': 'VARCHAR(63)',
    'parties': 'JSONB',
    'award': 'JSONB',
    'contract': 'JSONB',
    'tag': 'VARCHAR(63)',
    'tender': 'JSONB'
}
UNIQUE_COLUMNS = ['filing_id']
JSON_COLUMNS = ['publisher', 'parties', 'award', 'contract', 'tender']
FORCE_NULL_COLUMNS = ['published_date', 'date']

TABLE_NAME = 'au_contract_filing'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

BASE_URL = 'https://api.tenders.gov.au/ocds'

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
AU_CONTRACTS_DIR = os.path.join(DATA_DIR, 'au_contracts')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')
EXPORT_CSV = '{}/au_contract_filing.csv'.format(EXPORT_DIR)

if not os.path.exists(AU_CONTRACTS_DIR):
    os.makedirs(AU_CONTRACTS_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['au', 'procurement'],
                                    entities,
                                    object_normalization=True)


def get_agency_normalization_map(entities=None):
    '''Inverts the agency spellings into a normalization map.'''
    return normalization_map_helper(['au', 'procurement'],
                                    entities,
                                    primary_key='agencyNames')


def normalize(name, abn, normalization):
    name = canonicalize(name)
    keys = []
    if name and abn:
        keys.append(json.dumps({'name': name, 'abn': abn}))
    if abn:
        keys.append(json.dumps({'abn': abn}))
    if name:
        keys.append(json.dumps({'name': name}))
    for key in keys:
        if key in normalization:
            return normalization[key]
    return name


def normalize_agency(name, normalization):
    return util.entities.normalize(name, normalization)


def simplify_awards_json(data):
    flat_data = []
    for item in data:
        if 'releases' in item:
            releases = item['releases']
            for release in releases:
                flat_item = {}
                flat_item['publisher'] = item['publisher']
                flat_item['published_date'] = item['publishedDate']
                flat_item['filing_id'] = release['id']
                flat_item['date'] = release['date']
                flat_item['initiation_type'] = release['initiationType']
                if 'parties' in release:
                    flat_item['parties'] = release['parties']
                    for party in flat_item['parties']:
                        if 'roles' in party:
                            party['role'] = party['roles'][0]
                        del party['roles']
                if 'awards' in release:
                    awards = release['awards']
                    if len(awards) > 1:
                        print(awards)
                        print('Error: len(awards) = {}'.format(len(awards)))
                    flat_item['award'] = awards[0]
                if 'contracts' in release:
                    contracts = release['contracts']
                    if len(contracts) > 1:
                        print(contracts)
                        print('Error: len(contracts) = {}'.format(
                            len(contracts)))
                    flat_item['contract'] = contracts[0]
                if 'tag' in release:
                    tag = release['tag']
                    if len(tag) > 1:
                        print(tag)
                        print('Error: len(tag) = {}'.format(len(tag)))
                    flat_item['tag'] = tag[0]
                if 'tender' in release:
                    flat_item['tender'] = release['tender']
                flat_data.append(flat_item)
    return flat_data


def simplified_json_to_csv(json_filename, csv_filename):
    df = pd.read_json(json_filename)

    for column, column_type in TABLE_TYPES.items():
        if column_type == 'TIMESTAMP':
            df[column] = pd.to_datetime(df[column],
                                        errors='coerce')

    df = util.postgres.preprocess_dataframe(df,
                                            json_columns=JSON_COLUMNS,
                                            unique_columns=UNIQUE_COLUMNS)

    util.postgres.df_to_csv(df, csv_filename)


def simplified_value(filing):
    '''Returns the maximum amount (assumed to be AUD) of the contract.'''
    value = 0
    if 'contract' in filing:
        contract = filing['contract']
        if 'minValue' in contract:
            value = float(contract['minValue']['amount'])
        if 'value' in contract:
            value = max(value, float(contract['value']['amount']))
    return value


def get_party_abn(party):
    abn = None
    if 'additionalIdentifiers' in party:
        for identifier in party['additionalIdentifiers']:
            if identifier['scheme'] == 'AU-ABN':
                abn = identifier['id'].lower()
    return abn


def party_names(filing, entities, normalization, inclusive=False):
    '''Returns the list of normalized parties of a filing.

    Args:
      filing: The simplified dictionary of the tender.
      entities: The map from normalized entity names to their profiles.
      normalization: The map from alternate entity names to normalized form.
      inclusive: Whether to reduce to ultimate parent entity names.
    '''
    parties = []
    for party in filing['parties']:
        abn = get_party_abn(party)
        name = normalize(party['name'], abn, normalization)
        if inclusive:
            name = util.entities.ultimate_parent(name, entities)
        parties.append(name)
    return sorted(list(set(parties)))


def supplier_names(filing, entities, normalization, inclusive=False):
    '''Returns the list of normalized supplier names of a filing.

    Args:
      filing: The simplified dictionary of the tender.
      entities: The map from normalized entity names to their profiles.
      normalization: The map from alternate entity names to normalized form.
      inclusive: Whether to reduce to ultimate parent entity names.
    '''
    if not 'award' in filing:
        return []
    suppliers = []
    for supplier in filing['award']['suppliers']:
        name = normalize(supplier['name'], None, normalization)
        if inclusive:
            name = util.entities.ultimate_parent(name, entities)
        suppliers.append(name)
    return sorted(list(set(suppliers)))


def simplified_filings_to_entity_amounts(filings, inclusive=True):
    '''Returns the total contract amounts associated with each entity.

    Args:
      filings: The list of simplified filings.
      inclusive: Whether amounts should be rolled up to their ultimate parent.
    '''
    entities = util.entities.get_entities()
    normalization = get_normalization_map(entities)
    amounts = {}
    for filing in filings:
        value = simplified_value(filing)
        if value == 0.:
            continue
        suppliers = supplier_names(filing, entities, normalization, inclusive)
        for supplier in suppliers:
            if not supplier in amounts:
                amounts[supplier] = 0.
            amounts[supplier] += value / len(suppliers)

    return {
        k: v
        for k, v in sorted(
            amounts.items(), key=lambda item: item[1], reverse=True)
    }


def retrieve(start_date=None,
             end_date=None,
             output_csv=EXPORT_CSV,
             window_days=31,
             use_tor=False):
    if start_date is None:
        today = datetime.date.today()
        start_year, start_month, start_day = util.dates.earlier_day(
            today.year, today.month, today.day, window_days)
        start_date = util.dates.tuple_to_date(start_year, start_month,
                                              start_day)
    if end_date is None:
        today = datetime.date.today()
        end_year, end_month, end_day = util.dates.next_day(
            today.year, today.month, today.day)
        end_date = util.dates.tuple_to_date(end_year, end_month, end_day)

    date_range_str = '{}-{}'.format(start_date, end_date)

    start_timestamp = '{}T00:00:00Z'.format(start_date)
    end_timestamp = '{}T00:00:00Z'.format(end_date)

    session = util.tor.get_session() if use_tor else requests.session()

    initial_url = '{}/findByDates/contractPublished/{}/{}'.format(
        BASE_URL, start_timestamp, end_timestamp)
    data = session.get(initial_url).json()

    results = []
    with tqdm() as pbar:
        results.append(copy.deepcopy(data))
        pbar.update(1)
        while 'links' in data and 'next' in data['links']:
            del results[-1]['links']
            next_url = data['links']['next']
            data = session.get(next_url).json()
            results.append(copy.deepcopy(data))
            pbar.update(1)

    orig_json = '{}/{}_orig.json'.format(AU_CONTRACTS_DIR, date_range_str)
    with open(orig_json, 'w') as outfile:
        json.dump(results, outfile, indent=1)

    simplified_results = simplify_awards_json(results)

    simplified_json = '{}/{}_simplified.json'.format(AU_CONTRACTS_DIR,
                                                     date_range_str)
    with open(simplified_json, 'w') as outfile:
        json.dump(simplified_results, outfile, indent=1)

    simplified_json_to_csv(simplified_json, output_csv)


def retrieve_month(year, month, output_csv=EXPORT_CSV, use_tor=False):
    end_year = year + 1 if month == 12 else year
    end_month = 1 if month == 12 else month + 1
    start_date = '{}-{}-01'.format(year, str(month).zfill(2))
    end_date = '{}-{}-01'.format(end_year, str(end_month).zfill(2))
    retrieve(start_date, end_date, output_csv, use_tor=use_tor)


def retrieve_year(year, output_csv=EXPORT_CSV, use_tor=False):
    start_date = '{}-01-01'.format(year)
    end_date = '{}-01-01'.format(year + 1)
    retrieve(start_date, end_date, output_csv, use_tor=use_tor)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TABLE_NAME,
            TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (filing_id)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_parties_index ON {} USING GIN (
  cast(lower(parties::text) AS jsonb)
);
    '''.format(TABLE_NAME, TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(filing_id),       '') || ' ' ||
    coalesce(lower(publisher::text), '') || ' ' ||
    coalesce(lower(parties::text),   '') || ' ' ||
    coalesce(lower(award::text),     '') || ' ' ||
    coalesce(lower(contract::text),  '') || ' ' ||
    coalesce(lower(tender::text),    '')
  )
);
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=EXPORT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
