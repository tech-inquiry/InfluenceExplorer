#
# The tenders can be retrieved from
#
#  https://data.gov.il/dataset/tenders
#
# though, despite its name, tender amounts only appear available through
#
#  https://data.gov.il/dataset/exemptions
#
# Further, the latter has 162K rows versus the 13.8K of the former.
#
# At the moment, we manually download from these URLs.
#
import json
import os
import pandas as pd

import util.opus
import util.postgres

from util.entities import canonicalize, normalize, normalization_map_helper

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
DOWNLOAD_DIR = os.path.join(DATA_DIR, 'il_procurement')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

TENDER_INPUT_CSV = '{}/tenders.csv'.format(DOWNLOAD_DIR)
TENDER_OUTPUT_JSON = '{}/tenders.json'.format(DOWNLOAD_DIR)

TENDER_DTYPES = {'מספר פרסום': 'Int64', 'מספר חפ ספק': 'Int64'}

TENDER_COLUMN_RENAMING = {
    'ספר פרסום': 'publication_id',
    'שם המשרד': 'buyer',
    'שם יחידה מפרסמת': 'buyer_detailed',
    'סוג הליך': 'procedure_type',
    'מספר הליך': 'procedure_number',
    'שם הליך': 'procedure_name',
    'סטטוס': 'status',
    'תאריך פרסום': 'date_published',
    'תאריך עדכון': 'date_updated',
    'תאריך אחרון להגשת השגות': 'objection_deadline_day',
    'שעה אחרונה להגשת השגות': 'objection_deadline_time',
    'שם הסל': 'basket',
    'שם ספק זוכה': 'vendor_name',
    'מספר חפ ספק': 'vendor_number',
    'תאריך תחילת תקופת התקשרות': 'contract_period_begin',
    'תאריך סיום תקופת התקשרות': 'contract_period_end',
    'נושאים': 'topics'
}

# For each column in each row, we translate the string into English and
# create a new column with an appended '_en'.
TENDER_COLUMNS_TO_TRANSLATE = [
    'buyer', 'buyer_detailed', 'procedure_type', 'procedure_name', 'status',
    'basket', 'vendor_name', 'topics'
]

EXEMPTION_INPUT_CSV = '{}/exemptions.csv'.format(DOWNLOAD_DIR)
EXEMPTION_OUTPUT_JSON = '{}/exemptions.json'.format(DOWNLOAD_DIR)
EXEMPTION_OUTPUT_CSV = '{}/il_procurement.csv'.format(EXPORT_DIR)

EXEMPTION_DTYPES = {
    'מספר פרסום': 'Int64',
    'מספר הליך': 'str',
    'מספר חפ ספק': 'Int64'
}

EXEMPTION_RENAMED_DTYPES = {
    'publication_id': 'Int64',
    'procedure_number': 'str',
    'vendor_number': 'Int64'
}

EXEMPTION_COLUMN_RENAMING = {
    'מספר פרסום': 'publication_id',
    'שם המשרד': 'buyer',
    'שם יחידה מפרסמת': 'buyer_detailed',
    'סוג הליך': 'procedure_type',
    'מספר הליך': 'procedure_number',
    'שם הליך': 'procedure_name',
    'לינק לטקסטים': 'link_to_texts',
    'תקנה': 'regulation',
    'סטטוס': 'status',
    'מהות החלטה': 'decision',
    'גורם מאשר': 'confirmation',
    'תאריך פרסום': 'date_published',
    'תאריך עדכון': 'date_updated',
    'תאריך אחרון להגשת השגות': 'objection_deadline_day',
    'שעה אחרונה להגשת השגות': 'objection_deadline_time',
    'שם ספק': 'vendor_name',
    'מספר חפ ספק': 'vendor_number',
    'היקף כספי': 'amount',
    'מטבע': 'currency',
    'תאריך תחילת תקופת התקשרות': 'contract_period_begin',
    'תאריך סיום תקופת התקשרות': 'contract_period_end',
    'נושאים': 'topics'
}

# For each column in each row, we translate the string into English and
# create a new column with an appended '_en'.
EXEMPTION_COLUMNS_TO_TRANSLATE = [
    'buyer', 'buyer_detailed', 'procedure_type', 'procedure_name',
    'link_to_texts', 'regulation', 'status', 'decision', 'confirmation',
    'vendor_name', 'topics'
]

EXEMPTION_TABLE_NAME = 'il_procurement'
EXEMPTION_TABLE_TMP_NAME = '{}_tmp'.format(EXEMPTION_TABLE_NAME)

EXEMPTION_TABLE_TYPES = {
    'publication_id': 'BIGINT',
    'vendor_name': 'VARCHAR(255)',
    'vendor_name_en': 'VARCHAR(255)',
    'vendor_number': 'BIGINT',
    'buyer': 'VARCHAR(255)',
    'buyer_en': 'VARCHAR(255)',
    'buyer_detailed': 'TEXT',
    'buyer_detailed_en': 'TEXT',
    'amount': 'FLOAT',
    'currency': 'VARCHAR(15)',
    'date_published': 'TIMESTAMP',
    'date_updated': 'TIMESTAMP',
    'objection_deadline_day': 'TIMESTAMP',
    'objection_deadline_time': 'VARCHAR(63)',
    'contract_period_begin': 'TIMESTAMP',
    'contract_period_end': 'TIMESTAMP',
    'procedure_type': 'VARCHAR(255)',
    'procedure_type_en': 'VARCHAR(255)',
    'procedure_name': 'VARCHAR(255)',
    'procedure_name_en': 'TEXT',
    'status': 'VARCHAR(255)',
    'status_en': 'VARCHAR(255)',
    'decision': 'VARCHAR(255)',
    'decision_en': 'VARCHAR(255)',
    'confirmation': 'VARCHAR(255)',
    'confirmation_en': 'VARCHAR(255)',
    'regulation': 'VARCHAR(255)',
    'regulation_en': 'VARCHAR(255)',
    'link_to_texts': 'VARCHAR(63)',
    'link_to_texts_en': 'VARCHAR(63)',
    'topics': 'TEXT',
    'topics_en': 'TEXT'
}

EXEMPTION_UNIQUE_COLUMNS = ['publication_id']

EXEMPTION_FORCE_NULL_COLUMNS = [
    'amount', 'vendor_number', 'date_published', 'date_updated',
    'objection_deadline_day', 'contract_period_begin', 'contract_period_end'
]


def process_tenders(input_csv=TENDER_INPUT_CSV,
                    output_json=TENDER_OUTPUT_JSON):
    df = pd.read_csv(input_csv, dtype=TENDER_DTYPES)
    df = df.rename(columns=TENDER_COLUMN_RENAMING)

    dates_to_convert = [
        'date_published', 'date_updated', 'objection_deadline_day',
        'contract_period_begin', 'contract_period_end'
    ]
    for column in dates_to_convert:
        df[column] = pd.to_datetime(df[column],
                                    format='%d.%m.%Y',
                                    errors='coerce')

    data = json.loads(df.to_json(orient='records', date_format='iso'))

    # Batch together the entries to translate.
    inputs = []
    for item in data:
        for column in TENDER_COLUMNS_TO_TRANSLATE:
            if item[column] and isinstance(item[column], str):
                inputs.append(item[column])
    outputs, runtime = util.opus.translate_to_english(inputs, 'hebrew')
    index = 0
    for item in data:
        for column in TENDER_COLUMNS_TO_TRANSLATE:
            column_en = '{}_en'.format(column)
            if item[column] and isinstance(item[column], str):
                item[column_en] = outputs[index]
                index += 1
            else:
                item[column_en] = None

    with open(output_json, 'w') as outfile:
        json.dump(data, outfile, indent=1)


def postgres_create_exemptions(user=None,
                               password=None,
                               dbname=None,
                               port=None,
                               host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            EXEMPTION_TABLE_NAME,
            EXEMPTION_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (publication_id)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_vendor_index ON {} (lower(vendor_name));
    '''.format(EXEMPTION_TABLE_NAME, EXEMPTION_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
   coalesce(publication_id::text,     '') || ' ' ||
   coalesce(lower(vendor_name),       '') || ' ' ||
   coalesce(lower(vendor_name_en),    '') || ' ' ||
   coalesce(vendor_number::text,      '') || ' ' ||
   coalesce(lower(buyer),             '') || ' ' ||
   coalesce(lower(buyer_en),          '') || ' ' ||
   coalesce(lower(buyer_detailed),    '') || ' ' ||
   coalesce(lower(buyer_detailed_en), '') || ' ' ||
   coalesce(lower(procedure_type),    '') || ' ' ||
   coalesce(lower(procedure_type_en), '') || ' ' ||
   coalesce(lower(procedure_name),    '') || ' ' ||
   coalesce(lower(procedure_name_en), '') || ' ' ||
   coalesce(lower(status),            '') || ' ' ||
   coalesce(lower(status_en),         '') || ' ' ||
   coalesce(lower(decision),          '') || ' ' ||
   coalesce(lower(decision_en),       '') || ' ' ||
   coalesce(lower(confirmation),      '') || ' ' ||
   coalesce(lower(confirmation_en),   '') || ' ' ||
   coalesce(lower(regulation),        '') || ' ' ||
   coalesce(lower(regulation_en),     '') || ' ' ||
   coalesce(lower(link_to_texts),     '') || ' ' ||
   coalesce(lower(link_to_texts_en),  '') || ' ' ||
   coalesce(lower(topics),            '') || ' ' ||
   coalesce(lower(topics_en),         '')
  )
);
    '''.format(EXEMPTION_TABLE_NAME, EXEMPTION_TABLE_NAME))


def postgres_update_exemptions(export_csv=EXEMPTION_OUTPUT_CSV,
                               user=None,
                               password=None,
                               dbname=None,
                               port=None,
                               host=None,
                               vacuum=True):
    postgres_create_exemptions(user, password, dbname, port, host)
    util.postgres.update(EXEMPTION_TABLE_NAME,
                         EXEMPTION_TABLE_TYPES,
                         EXEMPTION_UNIQUE_COLUMNS,
                         EXEMPTION_FORCE_NULL_COLUMNS,
                         EXEMPTION_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def process_exemptions(input_csv=EXEMPTION_INPUT_CSV,
                       output_json=EXEMPTION_OUTPUT_JSON,
                       output_csv=EXEMPTION_OUTPUT_CSV,
                       columns_to_translate=EXEMPTION_COLUMNS_TO_TRANSLATE):
    df = pd.read_csv(input_csv, dtype=EXEMPTION_DTYPES)
    df = df.rename(columns=EXEMPTION_COLUMN_RENAMING)

    dates_to_convert = [
        'date_published', 'date_updated', 'objection_deadline_day',
        'contract_period_begin', 'contract_period_end'
    ]
    for column in dates_to_convert:
        df[column] = pd.to_datetime(df[column],
                                    format='%d.%m.%Y',
                                    errors='coerce')

    data = json.loads(df.to_json(orient='records', date_format='iso'))

    # Batch together the entries to translate.
    inputs = []
    for item in data:
        for column in columns_to_translate:
            if item[column] and isinstance(item[column], str):
                inputs.append(item[column])
    outputs, runtime = util.opus.translate_to_english(inputs, 'hebrew')
    index = 0
    for item in data:
        for column in columns_to_translate:
            column_en = '{}_en'.format(column)
            if item[column] and isinstance(item[column], str):
                item[column_en] = outputs[index]
                index += 1
            else:
                item[column_en] = None

    with open(output_json, 'w') as outfile:
        json.dump(data, outfile, indent=1)

    df = pd.DataFrame(data=data)
    df['publication_id'] = df['publication_id'].astype('Int64',
                                                       errors='ignore')
    df['vendor_number'] = df['vendor_number'].astype('Int64', errors='ignore')
    df = util.postgres.preprocess_dataframe(
        df, unique_columns=EXEMPTION_UNIQUE_COLUMNS)
    util.postgres.df_to_csv(df,
                            output_csv,
                            columns=EXEMPTION_TABLE_TYPES.keys())
    postgres_update_exemptions(output_csv)


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['il', 'procurement'], entities)
