#
# This script is meant to be run from the root folder where the justice
# leaks are served on the webserver.
#
import extract_msg
import glob
import os
import shutil


def extract():
    for part_dir in ['part-01', 'part-02']:
        for attachment in glob.glob('{}/Attachment/*'.format(part_dir)):
            if os.path.isdir(attachment):
                continue
            filename, extension = os.path.splitext(attachment)
            if extension:
                continue
            try:
                msg = extract_msg.openMsg(attachment)
                save = msg.save()
                save_dir = save[1]
                os.remove(attachment)
                print('Renaming {} to {}'.format(save_dir, attachment))
                os.rename(save_dir, attachment)
            except Exception as e:
                print(e)
                print('Could not extract {}'.format(attachment))


def clean_partial_folders():
    files = glob.glob('*')
    directories = sorted([f for f in files if os.path.isdir(f)])
    for directory in directories:
        if directory in ['__pycache__', 'part-01', 'part-02']:
            continue
        shutil.rmtree(directory)


if __name__ == '__main__':
    extract()
    clean_partial_folders()
