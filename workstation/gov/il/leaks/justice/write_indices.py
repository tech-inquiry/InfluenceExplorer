#
# The following script is meant to be run within the part-01/Attachment/
# and part-02/Attachment subfolders on the webserver deploying the leaked files.
#
import glob
import os

from urllib.request import pathname2url


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text


def write_index(files, directory='.'):
    with open('{}/index.html'.format(directory), 'w') as outfile:
        outfile.write('<html><body>\n')
        outfile.write('  <ul>\n')
        for f in files:
            if not f:
                continue
            if os.path.basename(f) == 'index.html':
                continue
            outfile.write('    <li><a href="{}">{}</a></li>\n'.format(
                pathname2url(f), f))
        outfile.write('  </ul>\n')
        outfile.write('</body></html>')


if __name__ == '__main__':
    files = sorted(glob.glob('**', recursive=True))
    write_index(files, directory='.')

    directories = [f for f in files if os.path.isdir(f)]
    for directory in directories:
        print('Processing {}'.format(directory))
        subfiles = sorted(glob.glob('{}/**'.format(directory), recursive=True))
        relative_subfiles = [
            remove_prefix(f, directory + '/') for f in subfiles
        ]
        write_index(relative_subfiles, directory=directory)
