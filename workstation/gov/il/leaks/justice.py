#
# Copyright (c) 2024 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This utility is for processing leaked emails and attachments from Israel's
# Ministry of Justice, as intermediated by Distributed Denial of Secrets at
#
#   https://data.ddosecrets.com/Israel%20Ministry%20of%20Justice/
#
# This script is for uploading the extracted text from the leaks. The script
# used to generate that data will be merged into this file soon.
#
import codecs
import os
import pandas as pd

import util.postgres

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

TEXT_JSONL = '{}/il_leak_ministry_of_justice.jsonl'.format(DATA_DIR)

OUTPUT_CSV = '{}/il_leak_ministry_of_justice.csv'.format(EXPORT_DIR)

DTYPE_MAP = {'part': 'int32', 'id': 'str', 'text': 'str'}

COLUMN_MAP = {'id': 'email_id'}

MAX_CHARS = 1024 * 921

TABLE_TYPES = {
    'part': 'INT',
    'email_id': 'TEXT',
    'text': 'TEXT',
    'attachment_text': 'TEXT',
    'email_terms': 'TEXT',
    'emails': 'JSONB'
}

UNIQUE_COLUMNS = ['email_id', 'part']

JSON_COLUMNS = ['emails']

FORCE_NULL_COLUMNS = ['emails']

TABLE_NAME = 'il_leak_ministry_of_justice'
TABLE_TMP_NAME = '{}_tmp'.format(TABLE_NAME)

if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)


def export_df_to_csv(df, csv_filename):
    for column in ['text', 'attachment_text']:
        df[column] = df[column].replace('\x00', ' ', regex=True)
        df[column] = df[column].replace('\000', ' ', regex=True)
        df[column] = df[column].replace('\u0000', ' ', regex=True)
        df[column] = df[column].replace(r'\\', ' ', regex=True)
    df = util.postgres.preprocess_dataframe(df,
                                            unique_columns=UNIQUE_COLUMNS,
                                            json_columns=JSON_COLUMNS,
                                            run_regexes=False)
    util.postgres.df_to_csv(df, csv_filename)


def process_json(input_jsonl=TEXT_JSONL, csv_filename=OUTPUT_CSV, max_chars=MAX_CHARS):
    if os.path.exists(input_jsonl):
        df = pd.read_json(input_jsonl, lines=True, dtype=DTYPE_MAP)
    else:
        print('Did not find downloaded JSON at {}'.format(input_jsonl))
        return

    df = df.rename(columns=COLUMN_MAP)
    df['text'] = df['text'].str.slice(0,max_chars)
    export_df_to_csv(df, csv_filename)


def postgres_create(user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TABLE_NAME,
            TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (email_id, part)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_id_part_index ON {} (lower(email_id), part);
    '''.format(TABLE_NAME, TABLE_NAME))
    # The default by limit for tsvector input strings is 1024^2, but it has
    # been observed that the size in bytes can exceed the number of characters.
    # As a workaround, we use 90% of the byte length for the truncation via
    # Postgres's 'left' function. (0.90 * 1024 = 921.6)
    cursor.execute('''
    ALTER TABLE {} ADD COLUMN IF NOT EXISTS tsvector_col tsvector
      GENERATED ALWAYS AS (
        to_tsvector('simple',
          left(
            coalesce(lower(email_id),        '') || ' ' ||
            coalesce(lower(email_terms),     '') || ' ' ||
            coalesce(lower(text),            '') || ' ' ||
            coalesce(lower(attachment_text), ''),
            {}
          )
        )
      ) STORED;
    '''.format(TABLE_NAME, MAX_CHARS))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (tsvector_col);
    '''.format(TABLE_NAME, TABLE_NAME))


def postgres_update(export_csv=OUTPUT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_create(user, password, dbname, port, host)
    util.postgres.update(TABLE_NAME,
                         TABLE_TYPES,
                         UNIQUE_COLUMNS,
                         FORCE_NULL_COLUMNS,
                         TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)
