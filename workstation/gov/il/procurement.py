#
# This script scrapes Israeli government contracts from:
#
#   https://mr.gov.il/ilgstorefront/en/search/
#
# We previously retrieved tenders from
#
#  https://data.gov.il/dataset/exemptions
#
import json
import os
import pandas as pd
import time

import util.firefox
import util.opus
import util.postgres

from glob import glob
from selenium.webdriver.common.by import By
from util.entities import canonicalize, normalize, normalization_map_helper

MAX_PAGE = 1000000
METADATA_SCRAPE_DELAY = 10.
POST_SCRAPE_DELAY = 5.

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
DOWNLOAD_DIR = os.path.join(DATA_DIR, 'il_procurement')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

METADATA_FILE = '{}/metadata.json'.format(DATA_DIR)

EXEMPT_CSV = '{}/il_procurement_exempt.csv'.format(EXPORT_DIR)
TENDER_CSV = '{}/il_procurement_tender.csv'.format(EXPORT_DIR)

EXEMPT_TABLE_TYPES = {
    'exempt_type': 'text',
    'title': 'text',
    'title_en': 'text',
    'publisher': 'text',
    'publisher_en': 'text',
    'exemption_reasons': 'text',
    'exemption_reasons_en': 'text',
    'bid_number': 'text',
    'bid_status': 'text',
    'bid_status_en': 'text',
    'bid_process': 'text',
    'bid_publication_date': 'date',
    'bid_update_date': 'date',
    'bid_last_date': 'date',
    'bid_contact_name': 'text',
    'bid_contact_name_en': 'text',
    'bid_contact_mail': 'text',
    'subjects': 'jsonb',
    'subjects_en': 'jsonb',
    'engagements': 'jsonb',
    'related_documents': 'jsonb',
    'entities': 'jsonb',
    'entities_en': 'jsonb'
}
TENDER_TABLE_TYPES = {
    'tender_type': 'text',
    'title': 'text',
    'title_en': 'text',
    'publisher': 'text',
    'publisher_en': 'text',
    'bid_number': 'text',
    'bid_status': 'text',
    'bid_status_en': 'text',
    'bid_process': 'text',
    'bid_publication_date': 'date',
    'bid_update_date': 'date',
    'first_submission_date': 'date',
    'last_submission_date': 'date',
    'bid_contact_name': 'text',
    'bid_contact_name_en': 'text',
    'subjects': 'jsonb',
    'subjects_en': 'jsonb',
    'related_documents': 'jsonb',
    'entities': 'jsonb',
    'entities_en': 'jsonb'
}

EXEMPT_UNIQUE_COLUMNS = ['bid_number']
TENDER_UNIQUE_COLUMNS = ['bid_number']

EXEMPT_FORCE_NULL_COLUMNS = [
    'bid_publication_date', 'bid_update_date', 'bid_last_date', 'subjects',
    'subjects_en', 'engagements', 'related_documents', 'entities',
    'entities_en'
]
TENDER_FORCE_NULL_COLUMNS = [
    'bid_publication_date', 'bid_update_date', 'first_submission_date',
    'last_submission_date', 'subjects', 'subjects_en', 'related_documents',
    'entities', 'entities_en'
]

EXEMPT_JSON_COLUMNS = [
    'subjects', 'subjects_en', 'engagements', 'related_documents', 'entities',
    'entities_en'
]
TENDER_JSON_COLUMNS = [
    'subjects', 'subjects_en', 'related_documents', 'entities', 'entities_en'
]

EXEMPT_TABLE_NAME = 'il_procurement_exempt'
EXEMPT_TABLE_TMP_NAME = '{}_tmp'.format(EXEMPT_TABLE_NAME)

TENDER_TABLE_NAME = 'il_procurement_tender'
TENDER_TABLE_TMP_NAME = '{}_tmp'.format(EXEMPT_TABLE_NAME)


def get_procurement_url(start_date, end_date, page=None):
    url = 'https://mr.gov.il/ilgstorefront/en/search/?q=%3AupdateDate%3AupdateDate%3A{}T21%253A00%253A00Z---{}T21%253A00%253A00Z'.format(
        start_date, end_date)
    if page != None:
        return '{}&page={}'.format(url, page)
    else:
        return url


def retrieve_metadata(start_date,
                      end_date,
                      max_page=MAX_PAGE,
                      scrape_delay=METADATA_SCRAPE_DELAY,
                      browser=None,
                      verbose=False):
    if browser == None:
        browser = util.firefox.get_browser(headless=True)
        destroy_browser = True
    else:
        destroy_browser = False

    metadata = {}
    try:
        for page in range(1, max_page):
            if verbose:
                print('Grabbing page {}'.format(page))
            url = get_procurement_url(start_date, end_date, page)
            browser.get(url)
            results = browser.find_elements(By.CLASS_NAME, 'result-container')
            for result in results:
                details = result.find_element(
                    By.XPATH,
                    ".//div[contains(@class, 'content-wrapper')]/div[@class='details-main-wrapper']"
                )
                contract_type = details.find_element(
                    By.XPATH, ".//div/h3").get_attribute('textContent')
                link = details.find_element(By.XPATH, ".//div/a")
                title = link.text
                url = link.get_attribute('href')
                if verbose:
                    print('{}, {}, {}'.format(contract_type, title, url))
                if contract_type not in metadata:
                    metadata[contract_type] = []
                metadata[contract_type].append({'title': title, 'url': url})
            if len(results) < 20:
                break
            time.sleep(scrape_delay)
    finally:
        if destroy_browser:
            browser.quit()

    return metadata


def retrieve_day_metadata(date,
                          max_page=MAX_PAGE,
                          scrape_delay=METADATA_SCRAPE_DELAY,
                          browser=None,
                          verbose=False):
    return retrieve_metadata(date,
                             date,
                             max_page=max_page,
                             scrape_delay=scrape_delay,
                             browser=browser,
                             verbose=verbose)


# The 'Exempt' item is apparently a shortening of 'Exempt Contract' and can
# be found at https://mr.gov.il/ilgstorefront/en/p/4000566789.
EXEMPT_CONTRACT_TYPES = [
    'Exempt', 'Exempt Contract', 'Intent to Contract', 'Joint Venture'
]

TENDER_CONTRACT_TYPES = [
    'Bidders List', 'Central Tender', 'Public Tender', 'RFI'
]


# Works for Exempt Contracts, Intents to Contract, or Joint Ventures.
#
# Test cases:
#   https://mr.gov.il/ilgstorefront/en/p/4000578088 [multiple sums]
#   https://mr.gov.il/ilgstorefront/en/p/4000580610
#   https://mr.gov.il/ilgstorefront/en/p/4000580599
#
# Joint Ventures:
#   https://mr.gov.il/ilgstorefront/en/p/4000580602
#
def parse_exempt_contract(url, browser=None, retrieved_page=False):
    if browser == None:
        browser = util.firefox.get_browser(headless=True)
        destroy_browser = True
    else:
        destroy_browser = False

    info = {}
    try:
        if not retrieved_page:
            browser.get(url)
        top_section = browser.find_element(
            By.XPATH,
            ".//section[@class='tender-exemption-top-sec d-flex flex-column align-items-center']/div[@class='top-sec-container custom-container d-flex flex-column justify-content-start']/div[@class='p-sec flex-column flex-lg-row d-flex align-items-start']/div[@class='p-wrapper']"
        )
        info['title'] = top_section.find_element(
            By.XPATH, ".//h2[@class='main-p']").text.strip()

        main_section = browser.find_element(
            By.XPATH,
            ".//section[@id='mainContent']/div[@class='body-wrapper w-100']")
        bids = main_section.find_element(
            By.XPATH,
            ".//div[@class='bids-top-sec-wrapper custom-container']/div[@class='bids-body-top-sec d-lg-flex']"
        )

        bid_details = bids.find_element(
            By.XPATH,
            ".//div[@class='bid-details-wrapper w-lg-100 d-flex flex-column d-lg-block']"
        )
        info['publisher'] = bid_details.find_element(
            By.XPATH, ".//div[@class='details-head']/h2").get_attribute(
                'textContent').strip()

        bid_details_body = bid_details.find_element(
            By.XPATH,
            ".//ul[@class='details-body d-flex justify-content-between flex-wrap']"
        )
        info['bid_number'] = bid_details_body.find_element(
            By.XPATH,
            ".//li[@class='bid-num details-item-wrapper']/span").text.strip()

        info['bid_status'] = bid_details_body.find_element(
            By.XPATH,
            ".//li[@class='status details-item-wrapper']/span").text.strip()
        info['bid_process'] = bid_details_body.find_element(
            By.XPATH,
            ".//li[@class='bid-release-date details-item-wrapper']/span"
        ).text.strip()
        info['bid_publication_date'] = bid_details_body.find_element(
            By.XPATH,
            ".//li[@class='manof-num details-item-wrapper']/span").text.strip(
            )
        info['bid_update_date'] = bid_details_body.find_element(
            By.XPATH, ".//li[@class='update-date details-item-wrapper']/span"
        ).text.strip()
        bid_last_dates = bid_details.find_elements(
            By.XPATH, ".//div[@class='last-date-wrapper']/span")
        if len(bid_last_dates):
            info['bid_last_date'] = bid_last_dates[0].text.strip()

        bid_contact = bids.find_element(
            By.XPATH, ".//div[@class='bid-contact-wrapper']")
        info['bid_contact_name'] = bid_contact.find_element(
            By.XPATH, ".//div[@class='bid-contact-name']").text.strip()
        info['bid_contact_mail'] = bid_contact.find_element(
            By.XPATH,
            ".//div[@class='bid-contact-mail']/a").get_attribute('href')

        communication = main_section.find_element(
            By.XPATH, ".//div[@id='communication-details']")
        essence_wrappers = communication.find_elements(
            By.XPATH, ".//div[@class='essence-wrapper']/p")
        reasons_wrappers = communication.find_elements(
            By.XPATH, ".//div[@class='reasons-wrapper']/p")
        if len(essence_wrappers):
            info['exemption_reasons'] = essence_wrappers[0].get_attribute(
                'textContent').strip()
        elif len(reasons_wrappers):
            info['exemption_reasons'] = reasons_wrappers[0].get_attribute(
                'textContent').strip()
        engagements = communication.find_elements(
            By.XPATH, ".//div[@class='details-wrapper d-flex flex-lg-column']")
        info['engagements'] = []
        for engagement in engagements:
            item = {}
            list1 = engagement.find_element(By.XPATH, ".//ul[1]")
            item['amount'] = list1.find_element(By.XPATH,
                                                ".//li[1]/span").text.strip()
            item['currency'] = list1.find_element(
                By.XPATH, ".//li[2]/span").text.strip()
            item['start_date'] = list1.find_element(
                By.XPATH, ".//li[3]/span").text.strip()
            item['end_date'] = list1.find_element(
                By.XPATH, ".//li[4]/span").text.strip()
            list2 = engagement.find_element(By.XPATH, ".//ul[2]")
            item['regulation'] = list2.find_element(
                By.XPATH, ".//li[1]/span").text.strip()
            item['tax_id'] = list2.find_element(By.XPATH,
                                                ".//li[2]/span").text.strip()
            item['supplier'] = list2.find_element(
                By.XPATH, ".//li[3]/span").text.strip()
            info['engagements'].append(item)

        info['subjects'] = []
        for subjects_wrapper in browser.find_elements(By.ID, 'subjects'):
            for subject in subjects_wrapper.find_elements(
                    By.XPATH, './/div/div'):
                info['subjects'].append(subject.text)

        info['related_documents'] = []
        related_documents = main_section.find_elements(
            By.XPATH,
            ".//div[@id='related-documents']/div[@class='related-documents-wrapper-desktop d-none d-lg-flex custom-container']/div"
        )
        for doc in related_documents:
            doc_wrapper = doc.find_element(
                By.XPATH, ".//div[@class='related-document-wrapper h-100']")
            item = {}
            item['date'] = doc_wrapper.find_element(
                By.XPATH, ".//div[@class='document-date']").text.strip()
            item['head'] = doc_wrapper.find_element(
                By.XPATH, ".//div[@class='document-head']").text.strip()
            item['description'] = doc_wrapper.find_element(
                By.XPATH,
                ".//div[@class='attachment-description']").text.strip()
            item['url'] = doc_wrapper.find_element(
                By.XPATH,
                ".//div[@class='document-date']/div/a").get_attribute('href')
            info['related_documents'].append(item)

    finally:
        if destroy_browser:
            browser.quit()
    return info


# Works for either Public Tenders, RFIs, Central Tenders, or Bidders Lists
#
# Test cases:
#   https://mr.gov.il/ilgstorefront/en/p/4000579084
#   https://mr.gov.il/ilgstorefront/en/p/4000579608 [RFI]
#
# Central Tender:
#   https://mr.gov.il/ilgstorefront/en/p/4000580581
#
# Bidders List:
#   https://mr.gov.il/ilgstorefront/en/p/4000580163
#
def parse_public_tender(url, browser=None, retrieved_page=False):
    if browser == None:
        browser = util.firefox.get_browser(headless=True)
        destroy_browser = True
    else:
        destroy_browser = False

    info = {}
    try:
        if not retrieved_page:
            browser.get(url)

        info['title'] = browser.find_element(
            By.XPATH,
            ".//section[@class='bids-top-sec d-flex flex-column align-items-center']/div[@class='top-sec-container custom-container d-flex flex-column justify-content-center']/div[@class='bids-head-wrapper d-flex flex-column flex-lg-row']/div[@class='bids-h-wrapper']/h2"
        ).text.strip()

        main_section = browser.find_element(
            By.XPATH,
            ".//section[@id='mainContent']/div[@class='body-wrapper w-100']")

        bids = main_section.find_element(
            By.XPATH,
            ".//div[@class='bids-top-sec-wrapper custom-container']/div[@class='bids-body-top-sec d-lg-flex']"
        )
        bid_details = bids.find_element(
            By.XPATH, ".//div[@class='bid-details-wrapper w-lg-100']")
        info['publisher'] = bid_details.find_element(
            By.XPATH, ".//div[@class='details-head']/h2").text.strip()
        details_body = bids.find_element(
            By.XPATH,
            ".//div[@class='details-body d-flex justify-content-between flex-wrap']"
        )
        info['bid_number'] = details_body.find_element(
            By.XPATH,
            ".//div[@class='bid-num details-item-wrapper']/span").text.strip()
        info['bid_status'] = details_body.find_element(
            By.XPATH,
            ".//div[@class='status details-item-wrapper']/span").text.strip()
        info['bid_process'] = details_body.find_element(
            By.XPATH,
            ".//div[@class='bid-release-date details-item-wrapper']/span"
        ).text.strip()
        info['bid_publication_date'] = details_body.find_element(
            By.XPATH,
            ".//div[@class='date details-item-wrapper']/span").text.strip()
        info['bid_update_date'] = details_body.find_element(
            By.XPATH, ".//div[@class='update-date details-item-wrapper']/span"
        ).text.strip()
        last_date_wrapper = bids.find_element(
            By.XPATH,
            ".//div[@class='row']/div[@class='last-date-wrapper col-sm-8']")
        first_submitting_dates = last_date_wrapper.find_elements(
            By.XPATH, ".//span[@id='firstSubmittingDate']")
        if len(first_submitting_dates):
            info['first_submission_date'] = first_submitting_dates[
                0].text.strip()
        info['last_submission_date'] = last_date_wrapper.find_element(
            By.XPATH, ".//span[@id='lastDate']").text.strip()

        bid_contact = bids.find_element(
            By.XPATH, ".//div[@class='bid-contact-wrapper']")
        info['bid_contact_name'] = bid_contact.find_element(
            By.XPATH, ".//div[@class='bid-contact-name']").text.strip()
        bid_contact_mails = bid_contact.find_elements(
            By.XPATH, ".//div[@class='bid-contact-mail']/a")
        if len(bid_contact_mails):
            info['bid_contact_mail'] = bid_contact_mails[0].get_attribute(
                'href')

        info['subjects'] = []
        for subjects_wrapper in browser.find_elements(By.ID, 'subjects'):
            for subject in subjects_wrapper.find_elements(
                    By.XPATH, './/div/div'):
                info['subjects'].append(subject.text)

        info['related_documents'] = []
        related_documents = main_section.find_elements(
            By.XPATH,
            ".//div[@id='related-documents']/div[@class='related-documents-wrapper-desktop d-none d-lg-flex custom-container']/div"
        )
        for doc in related_documents:
            doc_wrapper = doc.find_element(
                By.XPATH, ".//div[@class='winning-bidder-wrapper h-100']")
            item = {}
            item['date'] = doc_wrapper.find_element(
                By.XPATH, ".//div[@class='document-date']").text.strip()
            item['head'] = doc_wrapper.find_element(
                By.XPATH, ".//div[@class='document-head']").text.strip()
            item['description'] = doc_wrapper.find_element(
                By.XPATH,
                ".//div[@class='attachment-description']").text.strip()
            item['url'] = doc_wrapper.find_element(
                By.XPATH,
                ".//div[@class='document-date']/a").get_attribute('href')
            info['related_documents'].append(item)

    finally:
        if destroy_browser:
            browser.quit()
    return info


def translate_public_tender_entry(path):
    if len(path) == 1 and path[0] in [
            'bid_contact_name', 'bid_status', 'publisher', 'title'
    ]:
        return True

    if len(path) == 2 and path[0] == 'subjects':
        return True

    if len(path) == 3 and path[0] == 'related_documents':
        if path[2] in ['head', 'description']:
            return True

    return False


def translate_exempt_contract_entry(path):
    if len(path) == 1 and path[0] in [
            'bid_contact_name', 'publisher', 'bid_status', 'exemption_reasons',
            'title'
    ]:
        return True

    if len(path) == 3 and path[0] == 'engagements' and path[2] in [
            'regulation', 'supplier'
    ]:
        return True

    if len(path) == 2 and path[0] == 'subjects':
        return True

    if len(path) == 3 and path[0] == 'related_documents':
        if path[2] in ['head', 'description']:
            return True

    return False


def is_ascii(value):
    try:
        value.decode('ascii')
        return True
    except AttributeError:
        return False
    except UnicodeDecodeError:
        return False


def detect_post_type(url, browser):
    post_type = None
    try:
        browser.get(url)
        # Try to detect an Intent to Contract, Exempt Contract, or Joint Venture.
        main_heads = browser.find_elements(By.CLASS_NAME, 'main-head')
        if len(main_heads):
            title = main_heads[0].text.strip()
            if title in EXEMPT_CONTRACT_TYPES:
                return title
        # Try to detect a Bidders List, Central Tender, Public Tender, or RFI.
        bids_heads = browser.find_elements(By.CLASS_NAME, 'bids-head')
        if len(bids_heads):
            title = bids_heads[0].text.strip()
            if title in TENDER_CONTRACT_TYPES:
                return title
    except Exception as e:
        print(e)
    return post_type


def parse_post(url, post_type=None, browser=None):
    if browser == None:
        browser = util.firefox.get_browser(headless=True)
        destroy_browser = True
    else:
        destroy_browser = False

    result = {}
    try:
        retrieved_page = False
        if post_type == None:
            post_type = detect_post_type(url, browser=browser)
            retrieved_page = True
        if post_type in EXEMPT_CONTRACT_TYPES:
            result = parse_exempt_contract(url,
                                           browser=browser,
                                           retrieved_page=retrieved_page)
        elif post_type in TENDER_CONTRACT_TYPES:
            result = parse_public_tender(url,
                                         browser=browser,
                                         retrieved_page=retrieved_page)
        else:
            print('ERROR: Invalid post type {}'.format(post_type))
    except Exception as e:
        print(e)
        print('WARNING: Could not parse {}'.format(url))
    finally:
        if destroy_browser:
            browser.quit()

    return post_type, result


def parse_post_dict(posts,
                    browser=None,
                    scrape_delay=POST_SCRAPE_DELAY,
                    verbose=False):
    if browser == None:
        browser = util.firefox.get_browser(headless=True)
        destroy_browser = True
    else:
        destroy_browser = False

    results = {}
    try:
        for post_type in posts:
            if post_type not in results:
                results[post_type] = []
            for info in posts[post_type]:
                if verbose:
                    print('Parsing {}, {}'.format(post_type, info['url']))
                _, result = parse_post(info['url'],
                                       post_type=post_type,
                                       browser=browser)
                results[post_type].append(result)
                time.sleep(scrape_delay)
    finally:
        if destroy_browser:
            browser.quit()

    return results


def incorporate_translations(results):
    # Batch together the entries to translate.
    inputs = []

    def append_if_not_ascii(value):
        if not is_ascii(value):
            inputs.append(value)

    for post_type in results:
        is_exempt = post_type in EXEMPT_CONTRACT_TYPES
        for item in results[post_type]:
            if is_exempt:
                for key in ['exemption_reasons']:
                    if key in item:
                        append_if_not_ascii(item[key])
                if 'engagements' in item:
                    for engagement in item['engagements']:
                        append_if_not_ascii(engagement['regulation'])
                        append_if_not_ascii(engagement['supplier'])
            for key in [
                    'bid_contact_name', 'bid_status', 'publisher', 'title'
            ]:
                if key in item:
                    append_if_not_ascii(item[key])
            if 'subjects' in item:
                for subject in item['subjects']:
                    append_if_not_ascii(subject)
            if 'related_documents' in item:
                for doc in item['related_documents']:
                    append_if_not_ascii(doc['head'])
                    append_if_not_ascii(doc['description'])

    outputs, runtime = util.opus.translate_to_english(inputs, 'hebrew')

    index = 0

    def incorporate_en_if_not_ascii(item, key):
        nonlocal index
        en_key = '{}_en'.format(key)
        if is_ascii(item[key]):
            item[en_key] = item[key]
        else:
            item[en_key] = outputs[index]
            index += 1

    for post_type in results:
        is_exempt = post_type in EXEMPT_CONTRACT_TYPES
        for item in results[post_type]:
            if is_exempt:
                for key in ['exemption_reasons']:
                    if key in item:
                        incorporate_en_if_not_ascii(item, key)
                if 'engagements' in item:
                    for engagement in item['engagements']:
                        incorporate_en_if_not_ascii(engagement, 'regulation')
                        incorporate_en_if_not_ascii(engagement, 'supplier')
            for key in [
                    'bid_contact_name', 'bid_status', 'publisher', 'title'
            ]:
                if key in item:
                    incorporate_en_if_not_ascii(item, key)
            if 'subjects' in item:
                item['subjects_en'] = []
                for subject in item['subjects']:
                    subject_en = subject
                    if not is_ascii(subject):
                        subject_en = outputs[index]
                        index += 1
                    item['subjects_en'].append(subject_en)
            if 'related_documents' in item:
                for doc in item['related_documents']:
                    incorporate_en_if_not_ascii(doc, 'head')
                    incorporate_en_if_not_ascii(doc, 'description')


def export_csvs(results, exempt_csv=EXEMPT_CSV, tender_csv=TENDER_CSV):
    exempt_items = []
    tender_items = []
    for post_type in results:
        is_exempt = post_type in EXEMPT_CONTRACT_TYPES
        for item in results[post_type]:
            if 'publisher' not in item:
                print(item)
                continue

            # TODO(Jack Poulson): Add an entities columns.
            if is_exempt:
                item['exempt_type'] = post_type
            else:
                item['tender_type'] = post_type

            entities_set = set()
            entity = canonicalize(item['publisher'])
            if entity:
                entities_set.add(entity)
            if is_exempt:
                if 'engagements' in item:
                    for engagement in item['engagements']:
                        entity = canonicalize(engagement['supplier'])
                        if entity:
                            entities_set.add(entity)
            item['entities'] = list(entities_set)

            entities_set = set()
            entity = canonicalize(item['publisher_en'])
            if entity:
                entities_set.add(entity)
            if is_exempt:
                if 'engagements' in item:
                    for engagement in item['engagements']:
                        entity = canonicalize(engagement['supplier_en'])
                        if entity:
                            entities_set.add(entity)
            item['entities_en'] = list(entities_set)

            if is_exempt:
                exempt_items.append(item)
            else:
                tender_items.append(item)

    EXEMPT_COLUMNS = [
        'exempt_type', 'title', 'title_en', 'publisher', 'publisher_en',
        'exemption_reasons', 'exemption_reasons_en', 'bid_number',
        'bid_status', 'bid_status_en', 'bid_process', 'bid_publication_date',
        'bid_update_date', 'bid_last_date', 'bid_contact_name',
        'bid_contact_name_en', 'bid_contact_mail', 'subjects', 'subjects_en',
        'engagements', 'related_documents', 'entities', 'entities_en'
    ]
    TENDER_COLUMNS = [
        'tender_type', 'title', 'title_en', 'publisher', 'publisher_en',
        'bid_number', 'bid_status', 'bid_status_en', 'bid_process',
        'bid_publication_date', 'bid_update_date', 'first_submission_date',
        'last_submission_date', 'bid_contact_name', 'bid_contact_name_en',
        'subjects', 'subjects_en', 'related_documents', 'entities',
        'entities_en'
    ]

    df_exempt = pd.DataFrame(exempt_items, columns=EXEMPT_COLUMNS)
    for column in ['bid_publication_date', 'bid_update_date', 'bid_last_date']:
        df_exempt[column] = pd.to_datetime(df_exempt[column],
                                           dayfirst=True,
                                           errors='coerce')
    df_tender = pd.DataFrame(tender_items, columns=TENDER_COLUMNS)
    for column in [
            'bid_publication_date', 'bid_update_date', 'first_submission_date',
            'last_submission_date'
    ]:
        df_tender[column] = pd.to_datetime(df_tender[column],
                                           dayfirst=True,
                                           errors='coerce')

    df_exempt = util.postgres.preprocess_dataframe(
        df_exempt,
        json_columns=EXEMPT_JSON_COLUMNS,
        unique_columns=EXEMPT_UNIQUE_COLUMNS)
    df_tender = util.postgres.preprocess_dataframe(
        df_tender,
        json_columns=TENDER_JSON_COLUMNS,
        unique_columns=TENDER_UNIQUE_COLUMNS)

    util.postgres.df_to_csv(df_exempt, exempt_csv)
    util.postgres.df_to_csv(df_tender, tender_csv)


def postgres_create_exempt(user=None,
                           password=None,
                           dbname=None,
                           port=None,
                           host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            EXEMPT_TABLE_NAME,
            EXEMPT_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (bid_number)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
   coalesce(lower(title),                   '') || ' ' ||
   coalesce(lower(title_en),                '') || ' ' ||
   coalesce(lower(publisher),               '') || ' ' ||
   coalesce(lower(publisher_en),            '') || ' ' ||
   coalesce(lower(exemption_reasons),       '') || ' ' ||
   coalesce(lower(exemption_reasons_en),    '') || ' ' ||
   coalesce(bid_number,                     '') || ' ' ||
   coalesce(lower(bid_status),              '') || ' ' ||
   coalesce(lower(bid_status_en),           '') || ' ' ||
   coalesce(lower(bid_contact_name),        '') || ' ' ||
   coalesce(lower(bid_contact_name_en),     '') || ' ' ||
   coalesce(lower(bid_contact_mail),        '') || ' ' ||
   coalesce(lower(subjects::text),          '') || ' ' ||
   coalesce(lower(subjects_en::text),       '') || ' ' ||
   coalesce(lower(engagements::text),       '') || ' ' ||
   coalesce(lower(related_documents::text), '')
  )
);
    '''.format(EXEMPT_TABLE_NAME, EXEMPT_TABLE_NAME))


def postgres_create_tender(user=None,
                           password=None,
                           dbname=None,
                           port=None,
                           host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            TENDER_TABLE_NAME,
            TENDER_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (bid_number)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
   coalesce(lower(title),                   '') || ' ' ||
   coalesce(lower(title_en),                '') || ' ' ||
   coalesce(lower(publisher),               '') || ' ' ||
   coalesce(lower(publisher_en),            '') || ' ' ||
   coalesce(bid_number,                     '') || ' ' ||
   coalesce(lower(bid_status),              '') || ' ' ||
   coalesce(lower(bid_status_en),           '') || ' ' ||
   coalesce(lower(bid_contact_name),        '') || ' ' ||
   coalesce(lower(bid_contact_name_en),     '') || ' ' ||
   coalesce(lower(subjects::text),          '') || ' ' ||
   coalesce(lower(subjects_en::text),       '') || ' ' ||
   coalesce(lower(related_documents::text), '')
  )
);
    '''.format(TENDER_TABLE_NAME, TENDER_TABLE_NAME))


def postgres_update_exempt(export_csv=EXEMPT_CSV,
                           user=None,
                           password=None,
                           dbname=None,
                           port=None,
                           host=None,
                           vacuum=True):
    postgres_create_exempt(user, password, dbname, port, host)
    util.postgres.update(EXEMPT_TABLE_NAME,
                         EXEMPT_TABLE_TYPES,
                         EXEMPT_UNIQUE_COLUMNS,
                         EXEMPT_FORCE_NULL_COLUMNS,
                         EXEMPT_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_update_tender(export_csv=TENDER_CSV,
                           user=None,
                           password=None,
                           dbname=None,
                           port=None,
                           host=None,
                           vacuum=True):
    postgres_create_tender(user, password, dbname, port, host)
    util.postgres.update(TENDER_TABLE_NAME,
                         TENDER_TABLE_TYPES,
                         TENDER_UNIQUE_COLUMNS,
                         TENDER_FORCE_NULL_COLUMNS,
                         TENDER_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_update():
    postgres_update_exempt()
    postgres_update_tender()


def retrieve(start_date, end_date, verbose=False):
    metadata_file = '{}/metadata_{}_{}'.format(DOWNLOAD_DIR, start_date,
                                               end_date)
    results_file = '{}/results_{}_{}'.format(DOWNLOAD_DIR, start_date,
                                             end_date)

    metadata = retrieve_metadata(start_date, end_date, verbose=verbose)
    with open(metadata_file, 'w') as outfile:
        json.dump(metadata, outfile, indent=2)
    results = parse_post_dict(metadata, verbose=verbose)
    incorporate_translations(results)
    with open(results_file, 'w') as outfile:
        json.dump(results, outfile, indent=2)
    export_csvs(results)
    postgres_update_exempt()
    postgres_update_tender()


def retrieve_month(year, month, verbose=False):
    end_month = month + 1 if month < 12 else 1
    end_year = year if month < 12 else year + 1

    start_year_month = '{}-{}'.format(year, str(month).zfill(2))
    end_year_month = '{}-{}'.format(end_year, str(end_month).zfill(2))
    start_date = '{}-01'.format(start_year_month)
    end_date = '{}-01'.format(end_year_month)

    metadata_file = '{}/metadata_{}.json'.format(DOWNLOAD_DIR,
                                                 start_year_month)
    results_file = '{}/results_{}.json'.format(DOWNLOAD_DIR, start_year_month)
    metadata = retrieve_metadata(start_date, end_date, verbose=verbose)
    with open(metadata_file, 'w') as outfile:
        json.dump(metadata, outfile, indent=2)
    results = parse_post_dict(metadata, verbose=verbose)
    incorporate_translations(results)
    with open(results_file, 'w') as outfile:
        json.dump(results, outfile, indent=2)
    export_csvs(results)
    postgres_update_exempt()
    postgres_update_tender()


def export(json_pattern, verbose=False):
    json_files = glob('{}/{}'.format(DOWNLOAD_DIR, json_pattern))
    if verbose:
        print(json_files)
    results = {}
    for json_file in json_files:
        items = json.load(open(json_file))
        for key in items:
            if key not in results:
                results[key] = []
            results[key] += items[key]
    export_csvs(results)


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['il', 'procurement'], entities)
