#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This utility aids processing of the Canadian lobbying data made available at:
#
#     https://lobbycanada.gc.ca/en/open-data/
#
import csv
import json
import os
import pandas as pd
import re

import util.postgres
import util.zip

from util.entities import canonicalize, normalize, normalization_map_helper

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
DOWNLOAD_DIR = os.path.join(DATA_DIR, 'ca_lobbying')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

CSV_ENCODING = 'latin-1'

# Monthly communications reports
COMMUNICATIONS_ZIP_URL = 'https://lobbycanada.gc.ca/media/mqbbmaqk/communications_ocl_cal.zip'
COMMUNICATIONS_ZIP_URL_OLD = 'https://lobbycanada.gc.ca/media/1863/communications_ocl_cal.zip'
COMMUNICATIONS_DATA_DICTIONARY_URL = 'https://lobbycanada.gc.ca/media/1862/communications_dictionary_dictionnaire_communication.xlsx'

# Lobbying registrations
REGISTRATIONS_ZIP_URL = 'https://lobbycanada.gc.ca/media/zwcjycef/registrations_enregistrements_ocl_cal.zip'
REGISTRATIONS_ZIP_URL_OLD = 'https://lobbycanada.gc.ca/media/1861/registrations_enregistrements_ocl_cal.zip'
REGISTRATIONS_DATA_DICTIONARY_URL = 'https://lobbycanada.gc.ca/media/1860/dictionary_registrations_dictionnaire_enregistrements.xlsx'

COMMUNICATION_PRIMARY_HEADER = [
    # Unique numeric identifier for monthly communication reports. For monthly
    # communication reports in the registry, this number represents the second
    # number set within the Communication number.
    'COMLOG_ID',
    # The unique numeric identifier which represents the client/organization/
    # corporation for the communication. For monthly communication reports in
    # the registry, this number represents the first number set within the
    # Communication number.
    'CLIENT_ORG_CORP_NUM',
    # English name of the client/organization/corporation.
    'EN_CLIENT_ORG_CORP_NM_AN',
    # French name of the client/organization/corporation.
    'FR_CLIENT_ORG_CORP_NM',
    # Registrant number of the account holder who certified the communication.
    'REGISTRANT_NUM_DECLARANT',
    # Last name of registrant.
    'RGSTRNT_LAST_NM_DCLRNT',
    # First name of registrant.
    'RGSTRNT_1ST_NM_PRENOM_DCLRNT',
    # Date on which the communication took place.
    'COMM_DATE',
    # Indicates the type of registration the communication is associated to:
    #
    #   1. Consultant registration,
    #   2. In-house Corporation registration,
    #   3. In-house Organization registration.
    #
    'REG_TYPE_ENR',
    # Date at which the communication was certified.
    'SUBMISSION_DATE_SOUMISSION',
    # Date at which the communication was posted to the registry.
    'POSTED_DATE_PUBLICATION',
    # If the communication is an amendment, this provides the COMLOG_ID of the
    # original communication.
    'PREV_COMLOG_ID_PRECEDNT'
]

COMMUNICATION_DPOH_HEADER = [
    # Unique numeric identifier for monthly communication reports. For monthly
    # communication reports in the registry, this number represents the second
    # number set within the Communication number.
    'COMLOG_ID',
    # Last name of the DPOH who participated in the communication.
    'DPOH_LAST_NM_TCPD',
    # First name of the DPOH who participated in the communication.
    'DPOH_FIRST_NM_PRENOM_TCPD',
    # Position title of the DPOH who participated in the communication.
    'DPOH_TITLE_TITRE_TCPD',
    # Branch or unit of the DPOH who participated in the communication.
    'BRANCH_UNIT_DIRECTION_SERVICE',
    # Manually entered government institution of the DPOH who participated in
    # the communication. Exists only if INSTITUTION=Other. Note: Manually
    # entered government institutions are no longer accepted during the
    # communication entry process.
    'OTHER_INSTITUTION_AUTRE',
    # Government institution of the DPOH who participated in the communication.
    'INSTITUTION'
]

COMMUNICATION_SUBJECT_MATTERS_HEADER = [
    # Unique numeric identifier for monthly communication reports. For monthly
    # communication reports in the registry, this number represents the second
    # number set within the Communication number.
    'COMLOG_ID',
    # Subject matter of the communication. Note: If OTHER is shown, see the
    # OTHER_SUBJ_MATTER_AUTRE_OBJET to obtain what was entered.
    #'SUBJ_MATTER_OBJET',
    'SUBJ_CODE_OBJET',
    # Subject matter of the communication -- including those manually entered.
    #'OTHER_SUBJ_MATTER_AUTRE_OBJET'
    'CUSTOM_SUBJ_OBJET_PERSO'
]

COMM_TABLE_NAME = 'ca_lobbying_communication'
COMM_TABLE_TMP_NAME = '{}_tmp'.format(COMM_TABLE_NAME)
COMM_TABLE_TYPES = {
    'comm_id': 'VARCHAR(63)',
    'type': 'VARCHAR(63)',
    'name': 'VARCHAR(255)',
    'client_id': 'VARCHAR(63)',
    'date': 'DATE',
    'submission_date': 'DATE',
    'posted_date': 'DATE',
    'parent_id': 'VARCHAR(63)',
    'registrant': 'JSONB',
    'dpoh': 'JSONB',
    'activity': 'JSONB',
    'entities': 'JSONB'
}
COMM_UNIQUE_COLUMNS = ['comm_id']
COMM_FORCE_NULL_COLUMNS = [
    'date', 'submission_date', 'posted_date', 'registrant', 'dpoh', 'activity',
    'entities'
]

# This is the header for 'Registration_PrimaryExport.csv'.
REGISTRATION_PRIMARY_HEADER = [
    # Unique numeric identifier for a registration record within the database.
    # The identifier is NOT the same as the registration number.
    'REG_ID_ENR',
    # Identifies the type of registration:
    #
    #  1. Consultant registration,
    #  2. In-house Corporation registration, or
    #  3. In-house Organization registration.
    #
    'REG_TYPE_ENR',
    # This is the registration number with the version number. A registration
    # number consists of three numbers separated by a dash:
    #
    #  1. Represents the account number for the registrant,
    #  2. Represents the client/organization/corporation identifier for the
    #     registration, and
    #  3. Represents the approved submissions version of the registration.
    #
    'REG_NUM_ENR',
    # The registry contains registrations from 1996. Since then, the application
    # which collects these registrations has gone through various iterations due
    # to technical and Act changes:
    #
    #  V2: Registrations under the 'Lobbyists Registration Act'. These are
    #      registrations with effective date on or before June 20, 2005.
    #
    #  V4: The 'Regulations Amending the Lobbyists Registration Regulations' and
    #      the 'Act to amend the Lobbyists Registration Act' came into force on
    #      June 20, 2005. These registrations were active until July 2, 2008.
    #
    #  V5: Registrations under the 'Lobbying Act' which came into force on
    #      July 2, 2008.
    'VERSION_CODE',
    # English name of consulting firm.
    'EN_FIRM_NM_FIRME_AN',
    # French name of consulting firm.
    'FR_FIRM_NM_FIRME',
    # Registrant position title at consulting firm (no longer collected as of
    # V5 VERSIONS_CODE registrations).
    'RGSTRNT_POS_POSTE_DCLRNT',
    # Street address of consulting firm.
    'FIRM_ADDRESS_ADRESSE_FIRME',
    # Phone number of consulting firm.
    'FIRM_TEL_FIRME',
    # Fax number of consulting firm.
    'FIRM_FAX_FIRME',
    # Account number for the registrant who filed the registration.
    'RGSTRNT_NUM_DECLARANT',
    # Last name of registrant.
    'RGSTRNT_LAST_NM_DCLRNT',
    # First name of registrant.
    'RGSTRNT_1ST_NM_PRENOM_DCLRNT',
    # This appears to be the position of the registrant, though the official
    # CSV documenting the column names excludes this field.
    'RO_POS_POSTE_AR',
    # Business address for the registrant.
    'RGSTRNT_ADDRESS_ADRESSE_DCLRNT',
    # Business phone number for the registrant.
    'RGSTRNT_TEL_DCLRNT',
    # Business fax number for the registrant.
    'RGSTRNT_FAX_DCLRNT',
    # Unique numeric identifier for the client/organization/corporation profile.
    'CLIENT_ORG_CORP_PROFIL_ID_PROFIL_CLIENT_ORG_CORP',
    # The second number used within the registration number representing the
    # client/organization/corporation for the registration.
    'CLIENT_ORG_CORP_NUM',
    # The English name of the client/organization/corporation.
    'EN_CLIENT_ORG_CORP_NM_AN',
    # The French name of the client/organization/corporation.
    'FR_CLIENT_ORG_CORP_NM',
    # Address of the client/organization/corporation.
    'CLIENT_ORG_CORP_ADDRESS_ADRESSE_CLIENT_ORG_CORP',
    # Phone number of the client/organization/corporation.
    'CLIENT_ORG_CORP_TEL',
    # Fax number of the client/organization/corporation.
    'CLIENT_ORG_CORP_FAX',
    # Last name for the principal representative. The principal representative
    # is the consultant lobbyist's contact person for the client listed within
    # the registration.
    'REP_LAST_NM_REP',
    # First name for the principal representative.
    'REP_1ST_NM_PRENOM_REP',
    # Position title for the principal representative.
    'REP_POSITION_POSTE_REP',
    # The date the registration became active.
    'EFFECTIVE_DATE_VIGUEUR',
    # The date the registration became inactive.
    'END_DATE_FIN',
    # Indicates whether the Client or Corporation in the registration has one
    # or more parent companies (Y=Yes, N=No).
    'PARENT_IND_SOC_MERE',
    # Indicates whether the Client or Corporation in the registration is a
    # coalition (Y=Yes, N=No).
    'COALITION_IND',
    # Indicates whether the Client or Corporation in the registration has one
    # or more subsidiaries (Y=Yes, N=No).
    'SUBSIDIARY_IND_FILIALE',
    # Indicates whether the Client in the registration has activities which are
    # controlled or directed by another person or organization with a direct
    # interest in the outcome of the undertaking (Y=Yes, N=No).
    'DIRECT_INT_IND_INT_DIRECT',
    # For V5 VERSION_CODE registrations:
    #   Indicates whether the client/organization/corporation has received
    #   funding from any domestic or foreign government institution in the last
    #   completed financial year, or if the client/organization/corporation
    #   expects to receive funding in the current financial year (Y=Yes, N=No).
    #
    # For V2 and V4 VERSION_CODE registrations:
    #   Indicates whether the client/organization/corporation has received
    #   Funding from any domestic or foreign government institution in the last
    #   completed financial year (Y=Yes, N=No).
    'GOVT_FUND_IND_FIN_GOUV',
    # The financial year-end date for the client/organization/corporation.
    # Only required if GOVT_FUND_IND_FIN_GOUV is marked 'Y'.
    'FY_END_DATE_FIN_EXERCICE',
    # Indicates whether or not the registrant would receive payment contingent
    # on the outcome of the undertaking (Y=Yes, N=No). Note: This information
    # was only collected against V2 and V4 registrations.
    'CONTG_FEE_IND_HON_COND',
    # The registration ID for the previous version of the registration. For
    # Updates and Reactiviation, this indicates which registration the current
    # registration was created from.
    'PREV_REG_ID_ENR_PRECEDNT',
    # Date at which the registration was posted to the registry. Note: This
    # information is only available for version code V5 registrations submitted
    # under the 'Lobbying Act' which came into force July 2, 2008.
    'POSTED_DATE_PUBLICATION'
]

# This is the header for 'Registration_BeneficiariesExport.csv'.
REGISTRATION_BENEFICIARIES_HEADER = [
    # Unique numeric identifier for a registration record within the database.
    # The identifier is NOT the same as the registration number.
    #
    # Note: The REG_ID_ENR column can *not* be treated as an index, as there can
    # be many beneficiaries for a single registration.
    'REG_ID_ENR',
    # Type of beneficiary:
    #
    #   1. Parent company,
    #   2. Coalition,
    #   3. Subsidiary, or
    #   4. Person or organization with a direct interest.
    #
    'BNF_TYPE',
    # English name of beneficiary.
    'EN_BNF_NM_AN',
    # French name of beneficiary.
    'FR_BNF_NM',
    # Street address (line 1) of beneficiary.
    'STREET_RUE_1',
    # Street address (line 2) of beneficiary.
    'STREET_RUE_2',
    # City of beneficiary.
    'CITY_VILLE',
    # Postal code of beneficiary.
    'POST_CODE_POSTAL',
    # Province or state of beneficiary.
    'PROV_STATE_PROV_ETAT',
    # Country of beneficiary.
    'COUNTRY_PAYS'
]

REGISTRATION_COMM_TECHNIQUES_HEADER = [
    # Unique numeric identifier for a registration record within the database.
    # The identifier is *not* the same as the registration number.
    'REG_ID_ENR',
    # Communication technique used.
    'COMM_TECHNIQUE',
    # Manually entered communication type required when COMM_TECHNIQUE=Other.
    'OTHER_COMM_TECHNIQUE_AUTRE'
]

REGISTRATION_GOV_ENTITIES_HEADER = [
    # Unique numeric identifier for a registration record within the database.
    # The identifier is *not* the same as the registration number.
    'REG_ID_ENR',
    # Federal departments or organizations which have been or will be
    # communited with during the course of the undertaking.
    'INSTITUTION'
]

REGISTRATION_MANUAL_GOV_ENTITIES_HEADER = [
    # Unique numeric identifier for a registration record within the database.
    # The identifier is *not* the same as the registration number.
    'REG_ID_ENR',
    # Manually entered federal departments or organizations which have been or
    # will be communicated with during the course of the undertaking.
    # Note: The application no longer accepts this field.
    'OTHER_INSTITUTION_AUTRE'
]

REGISTRATION_GOV_FUNDING_HEADER = [
    # Unique numeric identifier for a registration record within the database.
    # The identifier is *not* the same as the registration number.
    'REG_ID_ENR',
    # Government institution that provided the funding to the
    # client/organization/corporation.
    'INSTITUTION',
    # The amount of funding in CDN dollars in the last completed financial year
    # of the client/organization/corporation.
    'AMOUNT_MONTANT',
    # Indicates whether or not the client/organization/corporation is expecting
    # funding from the government institution in their current financial year.
    # Note: This information was only captured in V5 registrations
    # (Y=Yes, N=No).
    'FUNDS_EXP_FIN_ATTENDU',
    # In older systems, there was no validation on the amount of funding
    # entered and the user could just enter text. These values have been
    # converted into the AMOUNT_MONTANT column, but are kept in the report for
    # archival purposes.
    'TEXT_TEXTE'
]

REGISTRATION_SUBJECT_MATTER_DETAILS_HEADER = [
    # Unique numeric identifier for a registration record in the database.
    # The identifier is *not* the same as the registration number.
    'REG_ID_ENR',
    # Category associated with the subject matter description entered.
    # Note that for registration submitted prior to June 20, 2005, the subject
    # matter information was collected in a manner where in-house registrants
    # were required to enter a Retrospective and Prospective description of
    # their lobbying activities.
    #'CATEGORY_CATEGORIE',
    'ASSOC_CAT_CODES',
    # Subject matter description.
    'DESCRIPTION'
]

REGISTRATION_SUBJECT_MATTER_HEADER = [
    # Unique numeric identifier for a registration record in the database.
    # The identifier is *not* the same as the registration number.
    'REG_ID_ENR',
    # Subject matter topics.
    'SUBJ_MATTER_OBJET'
]

REGISTRATION_CONSULTANT_LOBBYISTS_HEADER = [
    # Account number for the registrant who filed the registration.
    'RGSTRNT_NUM_DECLARANT',
    # Unique numeric identifier for the client/organization/corporation profile.
    'CLIENT_ORG_CORP_PROFIL_ID_PROFIL_CLIENT_ORG_CORP',
    # Unique numeric identifier for the lobbyist profile.
    'LBBYST_ID_LBBYST',
    # Last name of consultant lobbyist.
    'LBBYST_LAST_NM_LBBYST',
    # First name of consultant lobbyist.
    'LBBYST_1ST_NM_PRENOM_LBBYST',
    # Indicates whether the lobbyist held a Designated Public Office.
    'DPOH_IND_TCPD',
    # Indicates whether the lobbyist held a Public Office.
    'POH_IND_TCP',
    # If the lobbyist did hold an ADM position, the last day they held it.
    'ADM_LAST_DATE_FIN_SMA',
    # If lobbyist held a Designated Public Office, the last day they held it.
    'DPOH_LAST_DATE_FIN_TCPD',
    # If the lobbyist was part of a transition team, the last day they were
    # part of it.
    'TRSN_TEAM_DATE_EQUIPE_TRSN',
    # Indicates if the consultant lobbyist is a member of the board of directors
    # for the corporation who is their client (Y=Yes, N=No).
    'BOARD_MEMBER_CORP_MEMBRE_C.A.',
    # Indicates if the consultant lobbyist is a member of the organization who
    # is their client (Y=Yes, N=No, null=No).
    'MEMBER_ORG_MEMBRE',
    # Indicates if the consultant lobbyist is a member of the board of directors
    # for the organization who is their client (Y=Yes, N=No, null=No).
    'BOARD_MEMBER_ORG_MEMBRE_C.A.'
]

REGISTRATION_INHOUSE_LOBBYISTS_HEADER = [
    # Unique identifier for the client/organization/corporation profile.
    'CLIENT_ORG_CORP_PROFIL_ID_PROFIL_CLIENT_ORG_CORP',
    # Unique identifier for the lobbyist profile.
    'LBBYST_ID_LBBYST',
    # Job title of the in-house lobbyist.
    'LBBYST_POSITION_POSTE_LBBYST',
    # Last name of the in-house lobbyist.
    'LBBYST_LAST_NM_LBBYST',
    # First name of the in-house lobbyist.
    'LBBYST_FIRST_NM_PRENOM_LBBYST',
    # Indicates whether the lobbyist held a Designated Public Office.
    'DPOH_IND_TCPD',
    # Indicates whether the lobbyist held a Public Office.
    'POH_IND_TCP',
    # If the lobbyist held an ADM position, the last day they held it.
    'ADM_LAST_DATE_FIN_SMA',
    # If the lobbyist held a Desig. Public Office, the last day they held it.
    'DPOH_LAST_DATE_FIN_TCPD',
    # If the lobbyist was on a transition team, the last day they took part.
    'TRSN_TEAM_DATE_EQUIPE_TRSN'
]

REGISTRATION_PUBLIC_OFFICER_HEADER = [
    # Unique identifier for the lobbyist profile.
    'LOBBYIST_ID_LOBBYISTE',
    # A numeric identifier for the public office.
    'POH_NUM_TCP',
    # Indicates whether the positioned was a designated public office:
    #
    #  1=Yes
    #  2=No
    #  N=Not applicable as this information was entered before the current
    #    Lobbying Act (prior to July 2, 2008).
    #
    'DPOH_IND_TCPD',
    # Title of the position held.
    'TITLE_TITRE',
    # Branch where the position was held.
    'BRANCH_DIRECTION',
    # Government institution where the position was held.
    'INSTITUTION',
    # Start date of the position held.
    'POS_START_DATE_DEBUT_POSTE',
    # End date of the position held.
    'POS_END_DATE_FIN_POSTE',
    # Indicates if the position held was an ADM.
    'ADM_EQUIVALENT_SMA',
    # If DPOH_IND_TCPD=1, then a date is required for the last day the position
    # was held.
    'DPOH_LAST_DATE_FIN_TCPD',
    # Indicates whether the DPOH position was held under an employment exchange
    # program.
    'DPOH_EXCH_PROG_ECH_TCPD',
    # For VERSION_CODE=V4 registrations (registrations entered in the system
    # after June 20, 2005 and before July 2, 2008) POH information was entered
    # as text. This column displays the text which was entered for POH during
    # that period.
    'POH_DETAILS_PRE_V5_INFO_TCP'
]

REGISTRATION_TABLE_NAME = 'ca_lobbying_registration'
REGISTRATION_TABLE_TMP_NAME = '{}_tmp'.format(REGISTRATION_TABLE_NAME)
REGISTRATION_TABLE_TYPES = {
    'registration_id': 'VARCHAR(63)',
    'registration': 'JSONB',
    'firm': 'JSONB',
    'registrant': 'JSONB',
    'client': 'JSONB',
    'principal_representative': 'JSONB',
    'beneficiaries': 'JSONB',
    'comm_techniques': 'JSONB',
    'comm_institutions': 'JSONB',
    'funder_institutions': 'JSONB',
    'activity': 'JSONB',
    'entities': 'JSONB'
}
REGISTRATION_UNIQUE_COLUMNS = ['registration_id']
REGISTRATION_FORCE_NULL_COLUMNS = [
    'registration', 'firm', 'registrant', 'client', 'principal_representative',
    'beneficiaries', 'comm_techniques', 'comm_institutions',
    'funder_institutions', 'activity', 'entities'
]

LOBBYIST_TABLE_NAME = 'ca_lobbying_lobbyist'
LOBBYIST_TABLE_TMP_NAME = '{}_tmp'.format(LOBBYIST_TABLE_NAME)
LOBBYIST_TABLE_TYPES = {
    'lobbyist_id': 'VARCHAR(63)',
    'clients': 'JSONB',
    'public_offices': 'JSONB'
}
LOBBYIST_UNIQUE_COLUMNS = ['lobbyist_id']
LOBBYIST_FORCE_NULL_COLUMNS = ['clients', 'public_offices']

if not os.path.exists(DOWNLOAD_DIR):
    os.makedirs(DOWNLOAD_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)

COMMUNICATION_EXPORT_CSV = '{}/{}.csv'.format(EXPORT_DIR, COMM_TABLE_NAME)
LOBBYIST_EXPORT_CSV = '{}/{}.csv'.format(EXPORT_DIR, LOBBYIST_TABLE_NAME)
REGISTRATION_EXPORT_CSV = '{}/{}.csv'.format(EXPORT_DIR,
                                             REGISTRATION_TABLE_NAME)


def combine_communications(communication_csv=COMMUNICATION_EXPORT_CSV,
                           download_dir=DOWNLOAD_DIR):
    communication_json = '{}/communication.json'.format(download_dir)

    labels = ['Primary', 'Dpoh', 'SubjectMatters']

    def label_to_file(label, extension):
        return '{}/Communication_{}Export.{}'.format(download_dir, label,
                                                     extension)

    for label in labels:
        csv_file = label_to_file(label, 'csv')
        json_file = label_to_file(label, 'json')
        pd.read_csv(csv_file, encoding=CSV_ENCODING).to_json(json_file, orient='records', indent=1)

    PRIMARY_IDS = {
        'comm_id': 'COMLOG_ID',
        'type': 'REG_TYPE_ENR',
        'client_id': 'CLIENT_ORG_CORP_NUM',
        'date': 'COMM_DATE',
        'submission_date': 'SUBMISSION_DATE_SOUMISSION',
        'posted_date': 'POSTED_DATE_PUBLICATION',
        'parent_id': 'PREV_COMLOG_ID_PRECEDNT'
    }

    PRIMARY_REGISTRANT_IDS = {
        'id': 'REGISTRANT_NUM_DECLARANT',
        'firstName': 'RGSTRNT_1ST_NM_PRENOM_DCLRNT',
        'lastName': 'RGSTRNT_LAST_NM_DCLRNT'
    }

    DPOH_IDS = {
        'firstName': 'DPOH_FIRST_NM_PRENOM_TCPD',
        'lastName': 'DPOH_LAST_NM_TCPD',
        'title': 'DPOH_TITLE_TITRE_TCPD',
        'branch': 'BRANCH_UNIT_DIRECTION_SERVICE',
        'institution': 'INSTITUTION',
        'manualInstitution': 'OTHER_INSTITUTION_AUTRE'
    }

    SUBJECT_MATTER_IDS = {
        'category': 'SUBJ_MATTER_OBJET',
        'text': 'OTHER_SUBJ_MATTER_AUTRE_OBJET'
    }

    def attach_entities(filing):
        def build_name(obj):
            name_keys = ['name', 'firstName', 'lastName']
            name_pieces = []
            for name_key in name_keys:
                if name_key in obj:
                    name_pieces.append(obj[name_key].lower().strip())
            return ' '.join(name_pieces)

        entities = set()
        if 'name' in filing:
            name = filing['name'].lower().strip()
            if name:
                entities.add(name)
        if 'registrant' in filing:
            registrant_name = build_name(filing['registrant'])
            if registrant_name:
                entities.add(registrant_name)
        if 'dpoh' in filing:
            dpoh_name = build_name(filing['dpoh'])
            if dpoh_name:
                entities.add(dpoh_name)
        else:
            print('No DPOH for filing {}'.format(filing))
        filing['entities'] = sorted(list(entities))

        return filing

    communications = {}
    for item in json.load(open(label_to_file('Primary', 'json'))):
        comm_id = item['COMLOG_ID']
        if comm_id in communications:
            print('WARNING: Overwriting communication {}'.format(comm_id))

        simple_item = {}
        if item['EN_CLIENT_ORG_CORP_NM_AN']:
            simple_item['name'] = item['EN_CLIENT_ORG_CORP_NM_AN']
        elif item['FR_CLIENT_ORG_CORP_NM']:
            simple_item['name'] = item['FR_CLIENT_ORG_CORP_NM']
        for key, value in PRIMARY_IDS.items():
            if item[value]:
                simple_item[key] = util.postgres.sanitize_entry(item[value])

        # Pull in a registration structure.
        registrant = {}
        for key, value in PRIMARY_REGISTRANT_IDS.items():
            if item[value]:
                registrant[key] = util.postgres.sanitize_entry(item[value])
        if len(registrant.keys()):
            simple_item['registrant'] = registrant

        communications[comm_id] = simple_item

    for item in json.load(open(label_to_file('Dpoh', 'json'))):
        comm_id = item['COMLOG_ID']
        simple_item = communications[comm_id]
        dpoh = {}
        for key, value in DPOH_IDS.items():
            if item[value]:
                dpoh[key] = util.postgres.sanitize_entry(item[value])
        if len(dpoh.keys()):
            simple_item['dpoh'] = dpoh
        communications[comm_id] = simple_item

    for item in json.load(open(label_to_file('SubjectMatters', 'json'))):
        comm_id = item['COMLOG_ID']
        if comm_id in communications:
            simple_item = communications[comm_id]
        else:
            simple_item = {}
        activity = {}
        for key, value in SUBJECT_MATTER_IDS.items():
            # DEBUG
            try:
                if item[value]:
                    activity[key] = util.postgres.sanitize_entry(item[value])
            except Exception as e:
                print(e)
                print('Actual keys for SUBJECT_MATTER_IDS')
                print(item.keys())
                return
        if len(activity.keys()):
            simple_item['activity'] = activity
        communications[comm_id] = simple_item

    for comm_id, filing in communications.items():
        communications[comm_id] = attach_entities(filing)

    communications = list(communications.values())
    with open(communication_json, 'w') as outfile:
        json.dump(communications, outfile, indent=1)
    with open(communication_csv, 'w', newline='') as outfile:
        fieldnames = [
            'comm_id', 'type', 'name', 'client_id', 'date', 'submission_date',
            'posted_date', 'parent_id', 'registrant', 'dpoh', 'activity',
            'entities'
        ]
        json_fields = ['registrant', 'dpoh', 'activity', 'entities']
        csv_writer = csv.DictWriter(outfile,
                                    fieldnames=fieldnames,
                                    delimiter='\t',
                                    quotechar='"',
                                    doublequote=False,
                                    escapechar='\\',
                                    quoting=csv.QUOTE_ALL)
        csv_writer.writeheader()
        for item in communications:
            for field in json_fields:
                if field in item:
                    item[field] = util.postgres.export_json(item[field])
            csv_writer.writerow(item)


def combine_registrations(registration_csv=REGISTRATION_EXPORT_CSV,
                          lobbyists_csv=LOBBYIST_EXPORT_CSV,
                          download_dir=DOWNLOAD_DIR):
    registration_json = '{}/registration.json'.format(download_dir)
    lobbyists_json = '{}/lobbyists.json'.format(download_dir)

    labels = [
        'Primary', 'Beneficiaries', 'CommunicationTechniques',
        'GovernmentInst', 'ManuallyEntered_GovernmentInst', 'GovtFunding',
        'SubjectMatterDetails', 'SubjectMatters', 'ConsultantLobbyists',
        'InHouseLobbyists', 'PublicOffice'
    ]

    def label_to_file(label, extension):
        return '{}/Registration_{}Export.{}'.format(download_dir, label,
                                                    extension)

    for label in labels:
        csv_file = label_to_file(label, 'csv')
        json_file = label_to_file(label, 'json')
        pd.read_csv(csv_file, encoding=CSV_ENCODING).to_json(json_file, orient='records', indent=1)

    REGISTRATION_IDS = {
        'number': 'REG_NUM_ENR',
        'type': 'REG_TYPE_ENR',
        'version': 'VERSION_CODE',
        'parentID': 'PREV_REG_ID_ENR_PRECEDNT'
    }
    REGISTRATION_DATE_IDS = {
        'startDate': 'EFFECTIVE_DATE_VIGUEUR',
        'endDate': 'END_DATE_FIN',
        'postedDate': 'POSTED_DATE_PUBLICATION'
    }

    FIRM_IDS = {
        'registrantPosition': 'RGSTRNT_POS_POSTE_DCLRNT',
        'address': 'FIRM_ADDRESS_ADRESSE_FIRME',
        'phone': 'FIRM_TEL_FIRME',
        'fax': 'FIRM_FAX_FIRME'
    }

    REGISTRANT_IDS = {
        'accountNumber': 'RGSTRNT_NUM_DECLARANT',
        'firstName': 'RGSTRNT_1ST_NM_PRENOM_DCLRNT',
        'lastName': 'RGSTRNT_LAST_NM_DCLRNT',
        'position': 'RO_POS_POSTE_AR',
        'address': 'RGSTRNT_ADDRESS_ADRESSE_DCLRNT',
        'phone': 'RGSTRNT_TEL_DCLRNT',
        'fax': 'RGSTRNT_FAX_DCLRNT',
        'contingentPayment': 'CONTG_FEE_IND_HON_COND'
    }

    CLIENT_IDS = {
        'primaryNumber': 'CLIENT_ORG_CORP_PROFIL_ID_PROFIL_CLIENT_ORG_CORP',
        'secondaryNumber': 'CLIENT_ORG_CORP_NUM',
        'address': 'CLIENT_ORG_CORP_ADDRESS_ADRESSE_CLIENT_ORG_CORP',
        'phone': 'CLIENT_ORG_CORP_TEL',
        'fax': 'CLIENT_ORG_CORP_FAX',
        'hasParents': 'PARENT_IND_SOC_MERE',
        'hasSubsidiaries': 'SUBSIDIARY_IND_FILIALE',
        'isCoalition': 'COALITION_IND',
        'isControlled': 'DIRECT_INT_IND_INT_DIRECT',
        'isForeignFunded': 'GOVT_FUND_IND_FIN_GOUV',
        'yearEndDate': 'FY_END_DATE_FIN_EXERCICE'
    }

    PRINCIPAL_REP_IDS = {
        'firstName': 'REP_1ST_NM_PRENOM_REP',
        'lastName': 'REP_LAST_NM_REP',
        'position': 'REP_POSITION_POSTE_REP'
    }

    BENEFICIARY_IDS = {
        'type': 'BNF_TYPE',
        'street1': 'STREET_RUE_1',
        'street2': 'STREET_RUE_2',
        'city': 'CITY_VILLE',
        'postal': 'POST_CODE_POSTAL',
        'state': 'PROV_STATE_PROV_ETAT',
        'country': 'COUNTRY_PAYS'
    }

    GOV_FUNDER_IDS = {
        'name': 'INSTITUTION',
        'amount': 'AMOUNT_MONTANT',
        'expectingFunds': 'FUNDS_EXP_FIN_ATTENDU'
    }

    SUBJECT_MATTER_DESCRIPTION_IDS = {
        'category': 'ASSOC_CAT_CODES',
        'description': 'DESCRIPTION'
    }

    SUBJECT_MATTER_IDS = {'topics': 'SUBJ_CODE_OBJET'}

    CONSULTANT_LOBBYIST_IDS = {
        'firstName': 'LBBYST_1ST_NM_PRENOM_LBBYST',
        'lastName': 'LBBYST_LAST_NM_LBBYST',
        'heldPublicOffice': 'POH_IND_TCP',
        'heldDesignatedPublicOffice': 'DPOH_IND_TCPD',
        'heldADMPosition': 'ADM_LAST_DATE_FIN_SMA',
        'lastDesignatedPublicOfficeDay': 'DPOH_LAST_DATE_FIN_TCPD',
        'lastTransitionDay': 'TRSN_TEAM_DATE_EQUIPE_TRSN',
        'isMemberOfCorpBoard': 'BOARD_MEMBER_CORP_MEMBRE_C.A.',
        'isMemberOfOrg': 'MEMBER_ORG_MEMBRE',
        'isMemberOfOrgBoard': 'BOARD_MEMBER_ORG_MEMBRE_C.A.'
    }

    INHOUSE_LOBBYIST_IDS = {
        'position': 'LBBYST_POSITION_POSTE_LBBYST',
        'firstName': 'LBBYST_FIRST_NM_PRENOM_LBBYST',
        'lastName': 'LBBYST_LAST_NM_LBBYST',
        'heldPublicOffice': 'POH_IND_TCP',
        'heldDesignatedPublicOffice': 'DPOH_IND_TCPD',
        'heldADMPosition': 'ADM_LAST_DATE_FIN_SMA',
        'lastDesignatedPublicOfficeDay': 'DPOH_LAST_DATE_FIN_TCPD',
        'lastTransitionDay': 'TRSN_TEAM_DATE_EQUIPE_TRSN'
    }

    PUBLIC_OFFICE_IDS = {
        'id': 'POH_NUM_TCP',
        'was_designated_public_office': 'DPOH_IND_TCPD',
        'title': 'TITLE_TITRE',
        'branch': 'BRANCH_DIRECTION',
        'institution': 'INSTITUTION',
        'startDate': 'POS_START_DATE_DEBUT_POSTE',
        'endDate': 'POS_END_DATE_FIN_POSTE',
        'wasADMPosition': 'ADM_EQUIVALENT_SMA',
        'lastDesignatedPublicOfficeDay': 'DPOH_LAST_DATE_FIN_TCPD',
        'dpohPositionWasEmploymentExchange': 'DPOH_EXCH_PROG_ECH_TCPD',
        'legacyDetails': 'POH_DETAILS_PRE_V5_INFO_TCP'
    }

    def attach_entities(filing):
        def build_name(obj):
            name_keys = ['name', 'firstName', 'lastName']
            name_pieces = []
            for name_key in name_keys:
                if name_key in obj:
                    name_pieces.append(obj[name_key].lower().strip())
            return ' '.join(name_pieces)

        entities = set()
        registrant_name = build_name(filing['registrant'])
        if registrant_name:
            entities.add(registrant_name)
        if 'firm' in filing:
            firm_name = build_name(filing['firm'])
            entities.add(firm_name)
        client_name = build_name(filing['client'])
        if client_name:
            entities.add(client_name)
        for beneficiary in filing['beneficiaries']:
            name = build_name(beneficiary)
            if name:
                entities.add(name)
        filing['entities'] = sorted(list(entities))

        return filing

    # Set up the main registration profiles.
    registrations = {}
    for item in json.load(open(label_to_file('Primary', 'json'))):
        reg_id = item['REG_ID_ENR']
        if reg_id in registrations:
            print('WARNING: Overwriting registration {}'.format(reg_id))

        simple_item = {'registration_id': reg_id}

        # Pull in a registration structure.
        registration = {}
        for key, value in REGISTRATION_IDS.items():
            if item[value]:
                registration[key] = util.postgres.sanitize_entry(item[value])
        for key, value in REGISTRATION_DATE_IDS.items():
            if item[value]:
                # TODO(Jack Poulson): Format date.
                registration[key] = item[value]
        if len(registration.keys()):
            simple_item['registration'] = registration

        # Pull in a firm structure.
        firm = {}
        if item['EN_FIRM_NM_FIRME_AN']:
            firm['name'] = item['EN_FIRM_NM_FIRME_AN']
        elif item['FR_FIRM_NM_FIRME']:
            firm['name'] = item['FR_FIRM_NM_FIRME']
        for key, value in FIRM_IDS.items():
            if item[value]:
                firm[key] = util.postgres.sanitize_entry(item[value])
        if len(firm.keys()):
            simple_item['firm'] = firm

        # Pull in a registrant structure.
        registrant = {}
        for key, value in REGISTRANT_IDS.items():
            if item[value]:
                registrant[key] = util.postgres.sanitize_entry(item[value])
        if len(registrant.keys()):
            simple_item['registrant'] = registrant

        # Pull in a client structure.
        client = {}
        if item['EN_CLIENT_ORG_CORP_NM_AN']:
            client['name'] = item['EN_CLIENT_ORG_CORP_NM_AN']
        elif item['FR_CLIENT_ORG_CORP_NM']:
            client['name'] = item['FR_CLIENT_ORG_CORP_NM']
        for key, value in CLIENT_IDS.items():
            if item[value]:
                client[key] = util.postgres.sanitize_entry(item[value])
        if len(client.keys()):
            simple_item['client'] = client

        # Pull in a principal representative structure.
        principal_rep = {}
        for key, value in PRINCIPAL_REP_IDS.items():
            if item[value]:
                principal_rep[key] = util.postgres.sanitize_entry(item[value])
        if len(principal_rep.keys()):
            simple_item['principal_representative'] = principal_rep

        simple_item['beneficiaries'] = []
        simple_item['comm_techniques'] = []
        simple_item['comm_institutions'] = []
        simple_item['funder_institutions'] = []

        registrations[reg_id] = simple_item

    # Incorporate the beneficiaries.
    for item in json.load(open(label_to_file('Beneficiaries', 'json'))):
        reg_id = item['REG_ID_ENR']
        if reg_id not in registrations:
            registrations[reg_id] = {
                'registration_id': reg_id,
                'beneficiaries': [],
                'comm_techniques': [],
                'comm_institutions': [],
                'funder_institutions': []
            }
        simple_item = registrations[reg_id]

        beneficiary = {}
        if item['EN_BNF_NM_AN']:
            beneficiary['name'] = item['EN_BNF_NM_AN']
        elif item['FR_BNF_NM']:
            beneficiary['name'] = item['FR_BNF_NM']
        for key, value in BENEFICIARY_IDS.items():
            if item[value]:
                beneficiary[key] = util.postgres.sanitize_entry(item[value])
        if len(beneficiary.keys()):
            simple_item['beneficiaries'].append(beneficiary)

    # Incorporate the communication techniques.
    for item in json.load(
            open(label_to_file('CommunicationTechniques', 'json'))):
        reg_id = item['REG_ID_ENR']
        if reg_id not in registrations:
            registrations[reg_id] = {
                'registration_id': reg_id,
                'beneficiaries': [],
                'comm_techniques': [],
                'comm_institutions': [],
                'funder_institutions': []
            }
        simple_item = registrations[reg_id]

        technique = {}
        if item['COMM_TECHNIQUE']:
            technique['type'] = item['COMM_TECHNIQUE']
        if item['OTHER_COMM_TECHNIQUE_AUTRE']:
            technique['text'] = item['OTHER_COMM_TECHNIQUE_AUTRE']
        if len(technique.keys()):
            simple_item['comm_techniques'].append(technique)

    # Incorporate the lobbied government institutions.
    for item in json.load(open(label_to_file('GovernmentInst', 'json'))):
        reg_id = item['REG_ID_ENR']
        simple_item = registrations[reg_id]
        if item['INSTITUTION']:
            simple_item['comm_institutions'].append(item['INSTITUTION'])
    for item in json.load(
            open(label_to_file('ManuallyEntered_GovernmentInst', 'json'))):
        reg_id = item['REG_ID_ENR']
        simple_item = registrations[reg_id]
        if item['OTHER_INSTITUTION_AUTRE']:
            simple_item['comm_institutions'].append(
                item['OTHER_INSTITUTION_AUTRE'])

    # Incorporate the funding government institutions.
    for item in json.load(open(label_to_file('GovtFunding', 'json'))):
        reg_id = item['REG_ID_ENR']
        simple_item = registrations[reg_id]

        funder = {}
        for key, value in GOV_FUNDER_IDS.items():
            if item[value]:
                funder[key] = util.postgres.sanitize_entry(item[value])
        if len(funder.keys()):
            simple_item['funder_institutions'].append(funder)

    for item in json.load(open(label_to_file('SubjectMatterDetails', 'json'))):
        reg_id = item['REG_ID_ENR']
        if reg_id not in registrations:
            print('ID from SubjectMatterDetails not in registrations'.format(
                reg_id))
            print(item)
            continue
        simple_item = registrations[reg_id]

        activity = {}
        for key, value in SUBJECT_MATTER_DESCRIPTION_IDS.items():
            # DEBUG
            try:
                if item[value]:
                    activity[key] = util.postgres.sanitize_entry(item[value])
            except Exception as e:
                print(e)
                print('Actual SUBJECT_MATTER_DESCRIPTION_IDS keys')
                print(item.keys())
                return
        if len(activity.keys()):
            simple_item['activity'] = activity
    for item in json.load(open(label_to_file('SubjectMatters', 'json'))):
        reg_id = item['REG_ID_ENR']
        if reg_id not in registrations:
            print('ID from SubjectMatters not in registrations'.format(reg_id))
            print(item)
            continue
        simple_item = registrations[reg_id]
        activity = (simple_item['activity']
                    if 'activity' in simple_item else {})
        for key, value in SUBJECT_MATTER_IDS.items():
            if item[value]:
                activity[key] = util.postgres.sanitize_entry(item[value])
        if len(activity.keys()):
            simple_item['activity'] = activity

    # Incorporate the entities
    for reg_id, filing in registrations.items():
        registrations[reg_id] = attach_entities(filing)

    registrations = list(registrations.values())
    with open(registration_json, 'w') as outfile:
        json.dump(registrations, outfile, indent=1)
    with open(registration_csv, 'w', newline='') as outfile:
        fieldnames = [
            'registration_id', 'registration', 'firm', 'registrant', 'client',
            'principal_representative', 'beneficiaries', 'comm_techniques',
            'comm_institutions', 'funder_institutions', 'activity', 'entities'
        ]
        json_fields = fieldnames
        csv_writer = csv.DictWriter(outfile,
                                    fieldnames=fieldnames,
                                    delimiter='\t',
                                    quotechar='"',
                                    doublequote=False,
                                    escapechar='\\',
                                    quoting=csv.QUOTE_ALL)
        csv_writer.writeheader()
        for item in registrations:
            for field in json_fields:
                if field in item:
                    item[field] = util.postgres.export_json(item[field])
            csv_writer.writerow(item)

    lobbyists = {}
    for item in json.load(open(label_to_file('ConsultantLobbyists', 'json'))):
        lobbyist_id = str(item['LBBYST_ID_LBBYST'])
        if lobbyist_id not in lobbyists:
            lobbyists[lobbyist_id] = {
                'lobbyist_id': lobbyist_id,
                'clients': [],
                'public_offices': []
            }

        client = {
            'clientID':
            item['CLIENT_ORG_CORP_PROFIL_ID_PROFIL_CLIENT_ORG_CORP'],
            'accountNumber': item['RGSTRNT_NUM_DECLARANT']
        }
        lobbyist = {}
        for key, value in CONSULTANT_LOBBYIST_IDS.items():
            if item[value]:
                lobbyist[key] = util.postgres.sanitize_entry(item[value])
        if len(lobbyist.keys()):
            client['lobbyist'] = lobbyist
        lobbyists[lobbyist_id]['clients'].append(client)
    for item in json.load(open(label_to_file('InHouseLobbyists', 'json'))):
        lobbyist_id = str(item['LBBYST_ID_LBBYST'])
        if lobbyist_id not in lobbyists:
            lobbyists[lobbyist_id] = {
                'lobbyist_id': lobbyist_id,
                'clients': [],
                'public_offices': []
            }

        client = {
            'clientID':
            item['CLIENT_ORG_CORP_PROFIL_ID_PROFIL_CLIENT_ORG_CORP'],
            'inHouse': True
        }
        lobbyist = {}
        for key, value in INHOUSE_LOBBYIST_IDS.items():
            if item[value]:
                lobbyist[key] = util.postgres.sanitize_entry(item[value])
        if len(lobbyist.keys()):
            client['lobbyist'] = lobbyist
        lobbyists[lobbyist_id]['clients'].append(client)
    for item in json.load(open(label_to_file('PublicOffice', 'json'))):
        lobbyist_id = str(item['LOBBYIST_ID_LOBBYISTE'])
        if lobbyist_id not in lobbyists:
            lobbyists[lobbyist_id] = {
                'lobbyist_id': lobbyist_id,
                'clients': [],
                'public_offices': []
            }

        public_office = {}
        for key, value in PUBLIC_OFFICE_IDS.items():
            if item[value]:
                public_office[key] = util.postgres.sanitize_entry(item[value])
        if len(public_office.keys()):
            lobbyists[lobbyist_id]['public_offices'].append(public_office)

    lobbyists = list(lobbyists.values())
    with open(lobbyists_json, 'w') as outfile:
        json.dump(lobbyists, outfile, indent=1)
    with open(lobbyists_csv, 'w', newline='') as outfile:
        fieldnames = ['lobbyist_id', 'clients', 'public_offices']
        json_fields = ['clients', 'public_offices']
        csv_writer = csv.DictWriter(outfile,
                                    fieldnames=fieldnames,
                                    delimiter='\t',
                                    quotechar='"',
                                    doublequote=False,
                                    escapechar='\\',
                                    quoting=csv.QUOTE_ALL)
        csv_writer.writeheader()
        for item in lobbyists:
            for field in json_fields:
                if field in item:
                    item[field] = util.postgres.export_json(item[field])
            csv_writer.writerow(item)


def retrieve(download_dir=DOWNLOAD_DIR):
    util.zip.download_and_unpack(COMMUNICATIONS_ZIP_URL, download_dir)
    util.zip.download_and_unpack(REGISTRATIONS_ZIP_URL, download_dir)

    print('Combining communications')
    combine_communications()
    print('Combining registrations')
    combine_registrations()


def postgres_create_communications(user=None,
                                   password=None,
                                   dbname=None,
                                   port=None,
                                   host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            COMM_TABLE_NAME,
            COMM_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (comm_id)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(comm_id),          '') || ' ' ||
    coalesce(lower(name),             '') || ' ' ||
    coalesce(lower(client_id),        '') || ' ' ||
    coalesce(lower(parent_id),        '') || ' ' ||
    coalesce(lower(registrant::TEXT), '') || ' ' ||
    coalesce(lower(dpoh::TEXT),       '') || ' ' ||
    coalesce(lower(activity::TEXT),   '')
  )
);
   '''.format(COMM_TABLE_NAME, COMM_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_entities_index ON {} USING GIN (
  (entities) jsonb_path_ops
);
    '''.format(COMM_TABLE_NAME, COMM_TABLE_NAME))


def postgres_update_communications(export_csv=COMMUNICATION_EXPORT_CSV,
                                   user=None,
                                   password=None,
                                   dbname=None,
                                   port=None,
                                   host=None,
                                   vacuum=True):
    postgres_create_communications(user, password, dbname, port, host)
    util.postgres.update(COMM_TABLE_NAME,
                         COMM_TABLE_TYPES,
                         COMM_UNIQUE_COLUMNS,
                         COMM_FORCE_NULL_COLUMNS,
                         COMM_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_create_lobbyists(user=None,
                              password=None,
                              dbname=None,
                              port=None,
                              host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            LOBBYIST_TABLE_NAME,
            LOBBYIST_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (lobbyist_id)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(lobbyist_id),          '') || ' ' ||
    coalesce(lower(clients::TEXT),        '') || ' ' ||
    coalesce(lower(public_offices::TEXT), '')
  )
);
    '''.format(LOBBYIST_TABLE_NAME, LOBBYIST_TABLE_NAME))


def postgres_update_lobbyists(export_csv=LOBBYIST_EXPORT_CSV,
                              user=None,
                              password=None,
                              dbname=None,
                              port=None,
                              host=None,
                              vacuum=True):
    postgres_create_lobbyists(user, password, dbname, port, host)
    util.postgres.update(LOBBYIST_TABLE_NAME,
                         LOBBYIST_TABLE_TYPES,
                         LOBBYIST_UNIQUE_COLUMNS,
                         LOBBYIST_FORCE_NULL_COLUMNS,
                         LOBBYIST_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_create_registrations(user=None,
                                  password=None,
                                  dbname=None,
                                  port=None,
                                  host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            REGISTRATION_TABLE_NAME,
            REGISTRATION_TABLE_TYPES,
            serial_id=True,
            constraints={'unique': 'UNIQUE (registration_id)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(registration_id),                '') || ' ' ||
    coalesce(lower(registration::TEXT),             '') || ' ' ||
    coalesce(lower(firm::TEXT),                     '') || ' ' ||
    coalesce(lower(registrant::TEXT),               '') || ' ' ||
    coalesce(lower(client::TEXT),                   '') || ' ' ||
    coalesce(lower(principal_representative::TEXT), '') || ' ' ||
    coalesce(lower(beneficiaries::TEXT),            '') || ' ' ||
    coalesce(lower(comm_techniques::TEXT),          '') || ' ' ||
    coalesce(lower(comm_institutions::TEXT),        '') || ' ' ||
    coalesce(lower(funder_institutions::TEXT),      '') || ' ' ||
    coalesce(lower(activity::TEXT),                 '')
  )
);
    '''.format(REGISTRATION_TABLE_NAME, REGISTRATION_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_entities_index ON {} USING GIN (
  (entities) jsonb_path_ops
);
    '''.format(REGISTRATION_TABLE_NAME, REGISTRATION_TABLE_NAME))


def postgres_update_registrations(export_csv=REGISTRATION_EXPORT_CSV,
                                  user=None,
                                  password=None,
                                  dbname=None,
                                  port=None,
                                  host=None,
                                  vacuum=True):
    postgres_create_registrations(user, password, dbname, port, host)
    util.postgres.update(REGISTRATION_TABLE_NAME,
                         REGISTRATION_TABLE_TYPES,
                         REGISTRATION_UNIQUE_COLUMNS,
                         REGISTRATION_FORCE_NULL_COLUMNS,
                         REGISTRATION_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_update(communication_csv=COMMUNICATION_EXPORT_CSV,
                    lobbyist_csv=LOBBYIST_EXPORT_CSV,
                    registration_csv=REGISTRATION_EXPORT_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_update_communications(export_csv=communication_csv,
                                   user=user,
                                   password=password,
                                   dbname=dbname,
                                   port=port,
                                   host=host,
                                   vacuum=vacuum)
    postgres_update_lobbyists(export_csv=lobbyist_csv,
                              user=user,
                              password=password,
                              dbname=dbname,
                              port=port,
                              host=host,
                              vacuum=vacuum)
    postgres_update_registrations(export_csv=registration_csv,
                                  user=user,
                                  password=password,
                                  dbname=dbname,
                                  port=port,
                                  host=host,
                                  vacuum=vacuum)


def get_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['ca', 'lobbying'], entities)
