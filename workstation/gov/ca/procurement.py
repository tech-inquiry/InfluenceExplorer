#
# Copyright (c) 2020-2021 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This utility provides interfaces to four separate categories of data, though
# we initially -- and primarily -- focus on the first three:
#
# 1. Proactive Disclosure of all Federal Contracts above 10,000 CAD. The dataset
#    is described at
#
#     https://open.canada.ca/data/en/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b
#
#    and the dataset itself at
#
#     https://open.canada.ca/data/en/datastore/dump/fac950c0-00d5-4ec1-a4d3-9cbebf98a305?bom=True
#
#    which was previously at
#
#       https://open.canada.ca/data/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b/resource/fac950c0-00d5-4ec1-a4d3-9cbebf98a305/download/contracts.csv
#
#    and its 'legacy' predecessor at
#
#     https://open.canada.ca/data/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b/resource/7f9b18ca-f627-4852-93d5-69adeb9437d6/download/load-contracts-2020-10-01.csv
#
#    For details on the data fields, see Appendix A of
#     https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=14676
#
# 2. A history of all contracts awarded by Public Works and Government Services
#    Canada (PWGSC) from 2009 to June 2023 is described at:
#
#     https://open.canada.ca/data/en/dataset/53753f06-8b28-42d7-89f7-04cd014323b0#wb-auto-6
#
#    and the complete file can be downloaded from
#
#     https://buyandsell.gc.ca/cds/public/contracts/tpsgc-pwgsc_co-ch_tous-all.csv
#    However, the post-June 2023 data is available at
#
#     https://open.canada.ca/data/en/dataset/4fe645a1-ffcd-40c1-9385-2c771be956a4
#    and the author (Jack Poulson) needs to create an updated parser for it.
#
# 3. All award notices published to buyandsell.gc.ca. Each such award notice
#    announces a contract being award as a result of a tender notice.
#
#    Said dataset is described at
#
#     https://open.canada.ca/data/en/dataset/ffd38960-1853-4c19-ba26-e50bea2cb2d5
#
#    and directly available at
#
#      https://buyandsell.gc.ca/procurement-data/csv/award/all
#
#    As we demonstrate for Babel Street, Inc. and Dataminr, Inc., this category
#    contains a small percentage of our contracts of interest. The covered
#    contracts are restricted to those awarded by Public Works and Government
#    Services Canada (PWGSC) and typically only cover the first modification of
#    any award series. Thus, while the features are different, the covered
#    contracts are roughly a subset of those from Category (3).
#
#    We export this dataset as ca_buyandsell_award_notice.csv.
#
# The coverage of awards to Babel Street, Inc. and Dataminr, Inc. by these three
# datasets is instructive.
#
#
#                Contracts Involving "Babel Street, Inc."
#   ======================================================================
#                                             Included in Dataset Category
#      Identifier   Modification  Start Date        (1)  (2)  (3)
#   -------------   ------------  ----------        ---  ---  ---
#    M7594-184225            001  2020-11-25         -    X    -
#    "          "            000  2020-09-02         -    X    X
#      2BSCS11898                 2020-03-27         X    -    -
#         7414185                 2019-11-27         X    -    -
#   OSINT9063-001                 2019-03-15         X    -    -
#      4741983638                 2018-03-01         X    -    -
#         7378892                 2017-10-10         X    -    -
#      M500064818                 2016-12-28         X    -    -
#
#
# Complete coverage of these awards requires categories (1) and (2), and, as
# suggested above, category (3) roughly covers a subset of category (2)
# (albeit with different features).
#
#
#                Contracts Involving "Dataminr, Inc."
#  =======================================================================
#                                             Included in Dataset Category
#     Identifier   Modification  Start Date         (1)  (2)  (3)
#  -------------   ------------  ----------         ---  ---  ---
#   08324-160630            005  2020-05-25          -    X    -
#   "          "            004  2019-08-14          -    X    -
#   "          "            003  2018-08-03          -    X    -
#   "          "            002  2018-08-02          -    X    -
#   "          "            001  2018-01-04          -    X    X
#   "          "            000  2017-08-31          -    X    -
#       216-0630   2020-2021-Q2  2017-09-01          X    -    -
#       "      "   2020-2021-Q1  "        "          X    -    -
#       "      "   2019-2020-Q2  "        "          X    -    -
#       "      "   2018-2019-Q2  "        "          X    -    -
#       "      "   2017-2018-Q2  "        "          X    -    -
#
#
# We now return to the fourth and final category of data:
#
# 4. Tender notices document the initial call for bids and are therefore
#    upstream of award notices (which are the transition from tender to
#    contract) and contract histories. They are made available at:
#
#     https://open.canada.ca/data/en/dataset/ffd38960-1853-4c19-ba26-e50bea2cb2d5
#
#    but are split into four categories:
#
#    a) Construction,
#    b) Goods,
#    c) Services, and
#    d) Services Related to Goods.
#
# We do not yet make use of tender notices -- partially because the CSV files
# do not properly quote-escape strings containing commas.
#
import json
import numpy as np
import os
import pandas as pd
import re

import util.dict_path
import util.postgres

from util.entities import canonicalize, normalize, normalization_map_helper

THIS_DIR = os.path.dirname(__file__)
WORKSTATION_DIR = os.path.join(THIS_DIR, '../..')
DATA_DIR = os.path.join(WORKSTATION_DIR, 'data')
AWARDS_DIR = os.path.join(DATA_DIR, 'ca_awards')
EXPORT_DIR = os.path.join(WORKSTATION_DIR, 'export')

# Proactive disclosure contract records -- category (1) -- dateset URL.
PROACTIVE_DISCLOSURE_URL = 'https://open.canada.ca/data/en/datastore/dump/fac950c0-00d5-4ec1-a4d3-9cbebf98a305?bom=True'
# This was the previous URL:
#PROACTIVE_DISCLOSURE_URL = 'https://open.canada.ca/data/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b/resource/fac950c0-00d5-4ec1-a4d3-9cbebf98a305/download/contracts.csv'
PROACTIVE_DISCLOSURE_LEGACY_URL = 'https://open.canada.ca/data/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b/resource/7f9b18ca-f627-4852-93d5-69adeb9437d6/download/load-contracts-2020-10-01.csv'

# PWGSC Contract history -- category (2) -- dataset URLs.
PWSGC_CONTRACTS_URL = 'https://buyandsell.gc.ca/cds/public/contracts/tpsgc-pwgsc_co-ch_tous-all.csv'
PWSGC_CONTRACTS_RECENT_URL = 'https://buyandsell.gc.ca/cds/public/contracts/tpsgc-pwgsc_co-ch_EF-FY-20-21.csv'

CANADA_BUYS_URL = 'https://canadabuys.canada.ca/opendata/pub/contractHistoryComplete-contratsOctroyesComplet.csv'

CANADA_BUYS_TABLE_TYPES = {
    'title': 'TEXT',
    'reference_number': 'TEXT',
    'amendment_number': 'TEXT',
    'procurement_number': 'TEXT',
    'solicitation_number': 'TEXT',
    'contract_number': 'TEXT',
    'number_of_records': 'INT',
    'publication_date': 'DATE',
    'contract_award_date': 'DATE',
    'amendment_date': 'DATE',
    'contract_start_date': 'DATE',
    'contract_end_date': 'DATE',
    'contract_amount': 'FLOAT',
    'total_contract_value': 'FLOAT',
    'contract_currency': 'TEXT',
    'contract_status': 'TEXT',
    'gsin': 'TEXT',
    'gsin_description': 'TEXT',
    'unspsc': 'TEXT',
    'unspsc_description': 'TEXT',
    'procurement_category': 'TEXT',
    'notice_type': 'TEXT',
    'procurement_method': 'TEXT',
    'selection_criteria': 'TEXT',
    'limited_tendering_reason': 'TEXT',
    'trade_agreements': 'TEXT',
    'regions_of_delivery': 'TEXT',
    'supplier_legal_name': 'TEXT',
    'supplier_standardized_name': 'TEXT',
    'supplier_operating_name': 'TEXT',
    'supplier_employee_count': 'TEXT',
    'supplier_address_line': 'TEXT',
    'supplier_address_city': 'TEXT',
    'supplier_address_province': 'TEXT',
    'supplier_address_postal_code': 'TEXT',
    'supplier_address_country': 'TEXT',
    'contracting_entity_name': 'TEXT',
    'contracting_entity_address_line': 'TEXT',
    'contracting_entity_address_city': 'TEXT',
    'contracting_entity_address_province': 'TEXT',
    'contracting_entity_address_postal_code': 'TEXT',
    'contracting_entity_address_country': 'TEXT',
    'end_user_entities_name': 'TEXT',
    'end_user_entities_office_name': 'TEXT',
    'end_user_entities_address': 'TEXT',
    'contact_info_name': 'TEXT',
    'contact_info_email': 'TEXT',
    'contact_info_phone': 'TEXT',
    'contact_info_fax': 'TEXT',
    'contact_info_address_line': 'TEXT',
    'contact_info_city': 'TEXT',
    'contact_info_province': 'TEXT',
    'contact_info_postal_code': 'TEXT',
    'contact_info_country': 'TEXT',
    'percentage_of_goods_by_country': 'TEXT',
    'tender_description': 'TEXT'
}

CANADA_BUYS_DROP_COLUMNS = [
    'title-titre-fra', 'contractStatus-statutContrat-fra',
    'gsinDescription-nibsDescription-fra', 'unspscDescription-fra',
    'noticeType-avisType-fra',
    'procurementMethod-methodeApprovisionnement-fra',
    'selectionCriteria-criteresSelection-fra',
    'limitedTenderingReason-raisonAppelOffresLimite-fra',
    'tradeAgreements-accordsCommerciaux-fra',
    'regionsOfDelivery-regionsLivraison-fra',
    'supplierLegalName-nomLegalFournisseur-fra',
    'supplierStandardizedName-nomNormaliseFournisseur-fra',
    'supplierOperatingName-nomCommercialFournisseur-fra',
    'supplierEmployeeCount-fournisseurNombreEmployes-fra',
    'supplierAddressLine-ligneAdresseFournisseur-fra',
    'supplierAddressCity-fournisseurAdresseVille-fra',
    'supplierAddressProvince-fournisseurAdresseProvince-fra',
    'supplierAddressCountry-fournisseurAdressePays-fra',
    'contractingEntityName-nomEntitContractante-fra',
    'contractingEntityAddressLine-ligneAdresseEntiteContractante-fra',
    'contractingEntityAddressCity-entiteContractanteAdresseVille-fra',
    'contractingEntityAddressProvince-entiteContractanteAdresseProvince-fra',
    'contractingEntityAddressCountry-entiteContractanteAdressePays-fra',
    'endUserEntitiesName-nomEntitesUtilisateurFinal-fra',
    'endUserEntitiesOfficeName-bureauNomEntitesUtilisateurFinal-fra',
    'endUserEntitiesAddress-adresseEntitesUtilisateurFinal-fra',
    'contactInfoAddressLine-contactInfoAdresseLigne-fra',
    'contactInfoCity-contacterInfoVille-fra',
    'contactInfoProvince-contacterInfoProvince-fra',
    'contactInfoCountry-contactInfoPays-fra',
    'tenderDescription-descriptionAppelOffres-fra'
]

CANADA_BUYS_RENAMING = {
    'title-titre-eng': 'title',
    'referenceNumber-numeroReference': 'reference_number',
    'amendmentNumber-numeroModification': 'amendment_number',
    'procurementNumber-numeroApprovisionnement': 'procurement_number',
    'solicitationNumber-numeroSollicitation': 'solicitation_number',
    'contractNumber-numeroContrat': 'contract_number',
    'numberOfRecords-nombreEnregistrements': 'number_of_records',
    'publicationDate-datePublication': 'publication_date',
    'contractAwardDate-dateAttributionContrat': 'contract_award_date',
    'amendmentDate-dateModification': 'amendment_date',
    'contractStartDate-contratDateDebut': 'contract_start_date',
    'contractEndDate-dateFinContrat': 'contract_end_date',
    'contractAmount-montantContrat': 'contract_amount',
    'totalContractValue-valeurTotaleContrat': 'total_contract_value',
    'contractCurrency-contratMonnaie': 'contract_currency',
    'contractStatus-statutContrat-eng': 'contract_status',
    'gsin-nibs': 'gsin',
    'gsinDescription-nibsDescription-eng': 'gsin_description',
    'unspsc': 'unspsc',
    'unspscDescription-eng': 'unspsc_description',
    'procurementCategory-categorieApprovisionnement': 'procurement_category',
    'noticeType-avisType-eng': 'notice_type',
    'procurementMethod-methodeApprovisionnement-eng': 'procurement_method',
    'selectionCriteria-criteresSelection-eng': 'selection_criteria',
    'limitedTenderingReason-raisonAppelOffresLimite-eng':
    'limited_tendering_reason',
    'tradeAgreements-accordsCommerciaux-eng': 'trade_agreements',
    'regionsOfDelivery-regionsLivraison-eng': 'regions_of_delivery',
    'supplierLegalName-nomLegalFournisseur-eng': 'supplier_legal_name',
    'supplierStandardizedName-nomNormaliseFournisseur-eng':
    'supplier_standardized_name',
    'supplierOperatingName-nomCommercialFournisseur-eng':
    'supplier_operating_name',
    'supplierEmployeeCount-fournisseurNombreEmployes-eng':
    'supplier_employee_count',
    'supplierAddressLine-ligneAdresseFournisseur-eng': 'supplier_address_line',
    'supplierAddressCity-fournisseurAdresseVille-eng': 'supplier_address_city',
    'supplierAddressProvince-fournisseurAdresseProvince-eng':
    'supplier_address_province',
    'supplierAddressPostalCode-fournisseurAdresseCodePostal':
    'supplier_address_postal_code',
    'supplierAddressCountry-fournisseurAdressePays-eng':
    'supplier_address_country',
    'contractingEntityName-nomEntitContractante-eng':
    'contracting_entity_name',
    'contractingEntityAddressLine-ligneAdresseEntiteContractante-eng':
    'contracting_entity_address_line',
    'contractingEntityAddressCity-entiteContractanteAdresseVille-eng':
    'contracting_entity_address_city',
    'contractingEntityAddressProvince-entiteContractanteAdresseProvince-eng':
    'contracting_entity_address_province',
    'contractingEntityAddressPostalCode-entiteContractanteAdresseCodePostal':
    'contracting_entity_address_postal_code',
    'contractingEntityAddressCountry-entiteContractanteAdressePays-eng':
    'contracting_entity_address_country',
    'endUserEntitiesName-nomEntitesUtilisateurFinal-eng':
    'end_user_entities_name',
    'endUserEntitiesOfficeName-bureauNomEntitesUtilisateurFinal-eng':
    'end_user_entities_office_name',
    'endUserEntitiesAddress-adresseEntitesUtilisateurFinal-eng':
    'end_user_entities_address',
    'contactInfoName-informationsContactNom': 'contact_info_name',
    'contactInfoEmail-informationsContactCourriel': 'contact_info_email',
    'contactInfoPhone-contactInfoTelephone': 'contact_info_phone',
    'contactInfoFax': 'contact_info_fax',
    'contactInfoAddressLine-contactInfoAdresseLigne-eng':
    'contact_info_address_line',
    'contactInfoCity-contacterInfoVille-eng': 'contact_info_city',
    'contactInfoProvince-contacterInfoProvince-eng': 'contact_info_province',
    'contactInfoPostalcode': 'contact_info_postal_code',
    'contactInfoCountry-contactInfoPays-eng': 'contact_info_country',
    'percentageOfGoodsByCountry-pourcentageDeBiensParPays':
    'percentage_of_goods_by_country',
    'tenderDescription-descriptionAppelOffres-eng': 'tender_description'
}

CANADA_BUYS_UNIQUE_COLUMNS = [
    'reference_number', 'amendment_number', 'procurement_number',
    'solicitation_number', 'contract_number'
]

CANADA_BUYS_DATE_COLUMNS = [
    'publication_date', 'contract_award_date', 'amendment_date',
    'contract_start_date', 'contract_end_date'
]

CANADA_BUYS_FLOAT_COLUMNS = ['contract_amount', 'total_contract_value']

CANADA_BUYS_FORCE_NULL_COLUMNS = CANADA_BUYS_DATE_COLUMNS + CANADA_BUYS_FLOAT_COLUMNS

PWSGC_TABLE_TYPES = {
    'contract_number': 'VARCHAR(63)',
    'amendment_number': 'VARCHAR(63)',
    'award_date': 'DATE',
    'expiry_date': 'DATE',
    'contract_value': 'FLOAT',
    'gsin_description': 'VARCHAR(255)',
    'competitive_tender': 'VARCHAR(3)',
    'limited_tender_reason_description': 'VARCHAR(127)',
    'solicitation_procedure': 'VARCHAR(3)',
    'solicitation_procedure_description': 'VARCHAR(31)',
    'trade_agreement_description': 'TEXT',
    'supplier_standardized_name': 'VARCHAR(255)',
    'supplier_operating_name': 'VARCHAR(255)',
    'supplier_legal_name': 'VARCHAR(255)',
    'supplier_address_city': 'VARCHAR(63)',
    'supplier_address_prov_state': 'VARCHAR(31)',
    'supplier_address_postal_code': 'VARCHAR(15)',
    'supplier_address_country': 'VARCHAR(63)',
    'organization_employee_count': 'VARCHAR(31)',
    'total_contract_value': 'FLOAT',
    'number_records': 'INT',
    'end_user_entity': 'VARCHAR(127)',
    'contracting_entity_office_name': 'VARCHAR(31)',
    'contracting_address_street_1': 'VARCHAR(63)',
    'contracting_address_street_2': 'VARCHAR(63)',
    'contracting_address_city': 'VARCHAR(31)',
    'contracting_address_prov_state': 'VARCHAR(31)',
    'contracting_address_postal_code': 'VARCHAR(7)',
    'contracting_address_country': 'VARCHAR(7)',
    'country_percentage': 'JSONB',
    'date_file_published': 'DATE'
}
PWSGC_UNIQUE_COLUMNS = [
    'contract_number', 'amendment_number', 'supplier_legal_name'
]
PWSGC_FORCE_NULL_COLUMNS = [
    'award_date', 'expiry_date', 'total_contract_value', 'country_percentage',
    'date_file_published'
]
PWSGC_TABLE_NAME = 'ca_pwsgc_contract_filing'
PWSGC_TABLE_TMP_NAME = '{}_tmp'.format(PWSGC_TABLE_NAME)

# Award notice -- category (3) -- dataset URL.
AWARDS_URL = 'https://buyandsell.gc.ca/procurement-data/csv/award/all'
AWARDS_FRENCH_TAG = 'Français'

# Tender notice -- category (4) -- dataset URLs.
TENDER_CONSTRUCTION_URL = 'https://buyandsell.gc.ca/procurement-data/csv/tender/construction'
TENDER_GOODS_URL = 'https://buyandsell.gc.ca/procurement-data/csv/tender/goods'
TENDER_SERVICES_URL = 'https://buyandsell.gc.ca/procurement-data/csv/tender/services'
TENDER_SERVICES_RELATED_TO_GOODS_URL = 'https://buyandsell.gc.ca/procurement-data/csv/tender/services-related-to-goods'

PROACTIVE_TABLE_TYPES = {
    'reference_number': 'VARCHAR(255)',
    'procurement_id': 'VARCHAR(63)',
    'vendor_name': 'VARCHAR(255)',
    'vendor_postal_code': 'VARCHAR(31)',
    'buyer_name': 'VARCHAR(255)',
    'contract_date': 'DATE',
    'description': 'TEXT',
    'contract_period_start': 'DATE',
    'delivery_date': 'DATE',
    'contract_value': 'FLOAT',
    'original_value': 'FLOAT',
    'amendment_value': 'FLOAT',
    'comments': 'TEXT',
    'additional_comments': 'TEXT',
    'agreement_type_code': 'VARCHAR(63)',
    'trade_agreement': 'VARCHAR(63)',
    'country_of_vendor': 'VARCHAR(31)',
    'solicitation_procedure': 'VARCHAR(31)',
    'limited_tendering_reason': 'VARCHAR(31)',
    'trade_agreement_exceptions': 'VARCHAR(63)',
    'intellectual_property': 'VARCHAR(31)',
    'contracting_entity': 'VARCHAR(31)',
    'standing_offer_number': 'VARCHAR(255)',
    'instrument_type': 'VARCHAR(31)',
    'number_of_bids': 'INT',
    'award_criteria': 'INT',
    'reporting_period': 'VARCHAR(31)',
    'owner_org': 'VARCHAR(31)',
    'owner_org_title': 'VARCHAR(255)'
}
PROACTIVE_UNIQUE_COLUMNS = [
    'reference_number', 'procurement_id', 'vendor_name', 'vendor_postal_code',
    'buyer_name', 'contract_date', 'owner_org'
]
PROACTIVE_FORCE_NULL_COLUMNS = [
    'contract_date', 'contract_period_start', 'delivery_date',
    'number_of_bids', 'award_criteria', 'amendment_value', 'original_value',
    'contract_value'
]
PROACTIVE_TABLE_NAME = 'ca_proactive_disclosure_filing'
PROACTIVE_TABLE_TMP_NAME = '{}_tmp'.format(PROACTIVE_TABLE_NAME)

PROACTIVE_CSV_ORIG = '{}/{}s_orig.csv'.format(AWARDS_DIR, PROACTIVE_TABLE_NAME)
PROACTIVE_JSON_ORIG = '{}/{}s_orig.json'.format(AWARDS_DIR,
                                                PROACTIVE_TABLE_NAME)
PROACTIVE_CSV = '{}/{}.csv'.format(EXPORT_DIR, PROACTIVE_TABLE_NAME)
PROACTIVE_UNIQUE_NAMES_JSON = (
    '{}/ca_proactive_disclosure_unique_vendors.json'.format(AWARDS_DIR))

CANADA_BUYS_TABLE_NAME = 'ca_canada_buys'
CANADA_BUYS_TABLE_TMP_NAME = '{}_tmp'.format(CANADA_BUYS_TABLE_NAME)
CANADA_BUYS_CSV_ORIG = '{}/{}_orig.csv'.format(AWARDS_DIR,
                                               CANADA_BUYS_TABLE_NAME)
CANADA_BUYS_JSON_ORIG = '{}/{}_orig.json'.format(AWARDS_DIR,
                                                 CANADA_BUYS_TABLE_NAME)
CANADA_BUYS_CSV = '{}/{}.csv'.format(EXPORT_DIR, CANADA_BUYS_TABLE_NAME)
CANADA_BUYS_UNIQUE_NAMES_JSON = '{}/ca_canada_buys_unique_vendors.json'.format(
    AWARDS_DIR)

PWSGC_CSV_ORIG = '{}/{}s_orig.csv'.format(AWARDS_DIR, PWSGC_TABLE_NAME)
PWSGC_JSON_ORIG = '{}/{}s_orig.json'.format(AWARDS_DIR, PWSGC_TABLE_NAME)
PWSGC_CSV = '{}/{}.csv'.format(EXPORT_DIR, PWSGC_TABLE_NAME)
PWSGC_UNIQUE_NAMES_JSON = '{}/ca_pwsgc_contract_unique_suppliers.json'.format(
    AWARDS_DIR)

if not os.path.exists(AWARDS_DIR):
    os.makedirs(AWARDS_DIR)
if not os.path.exists(EXPORT_DIR):
    os.makedirs(EXPORT_DIR)


def export_for_postgres(df, output_filename, unique_columns):
    df = util.postgres.preprocess_dataframe(df, unique_columns=unique_columns)
    util.postgres.df_to_csv(df, output_filename)


def retrieve_proactive_disclosures(
        csv_orig_filename=PROACTIVE_CSV_ORIG,
        json_orig_filename=PROACTIVE_JSON_ORIG,
        csv_filename=PROACTIVE_CSV,
        unique_vendors_filename=PROACTIVE_UNIQUE_NAMES_JSON):
    # Read in the CSV as a DataFrame
    df = pd.read_csv(PROACTIVE_DISCLOSURE_URL)

    # Drop the redundant French descriptions since we display in English.
    df = df.drop(
        columns=['description_fr', 'comments_fr', 'additional_comments_fr'])
    df = df.rename(
        columns={
            'description_en': 'description',
            'comments_en': 'comments',
            'additional_comments_en': 'additional_comments'
        })

    # Drop several fields which we do not yet use.
    df = df.drop(columns=[
        'economic_object_code', 'land_claims', 'commodity_type',
        'commodity_code', 'indigenous_business',
        'indigenous_business_excluding_psib',
        'potential_commercial_exploitation', 'former_public_servant',
        'ministers_office', 'article_6_exceptions', 'socioeconomic_indicator'
    ])

    # Convert relevant columns to datetime.
    date_columns = ['contract_date', 'contract_period_start', 'delivery_date']
    for date_column in date_columns:
        df[date_column] = pd.to_datetime(df[date_column],
                                         format='%Y-%m-%d',
                                         errors='coerce')

    df['number_of_bids'] = df['number_of_bids'].round().astype('Int32')
    df['award_criteria'] = pd.to_numeric(
        df['award_criteria'], errors='coerce').round().astype('Int32')

    # Strip the vendor name and replace double-spaces with a single space.
    df['vendor_name'] = df['vendor_name'].str.strip().replace('  ', ' ')

    df.to_csv(csv_orig_filename, index=False)
    df.to_json(json_orig_filename, orient='records', indent=1)

    unique_columns = [
        'reference_number', 'procurement_id', 'vendor_name', 'contract_date',
        'buyer_name', 'owner_org'
    ]
    export_for_postgres(df, csv_filename, unique_columns)

    unique_vendors = list(df['vendor_name'].fillna(value='').unique())
    unique_vendors_norm = []
    for vendor in unique_vendors:
        vendor = vendor.lower().strip()
        unique_vendors_norm.append(vendor)
    unique_vendors_norm = sorted(list(set(unique_vendors_norm)))
    with open(unique_vendors_filename, 'w') as outfile:
        json.dump(unique_vendors_norm, outfile, indent=1)


def retrieve_canada_buys(
        csv_orig_filename=CANADA_BUYS_CSV_ORIG,
        json_orig_filename=CANADA_BUYS_JSON_ORIG,
        csv_filename=CANADA_BUYS_CSV,
        unique_vendors_filename=CANADA_BUYS_UNIQUE_NAMES_JSON):
    # Read in the CSV as a DataFrame
    storage_options = {'User-Agent': 'Mozilla/9.0'}
    df = pd.read_csv(CANADA_BUYS_URL, storage_options=storage_options)

    # Drop the redundant French descriptions since we display in English.
    df = df.drop(columns=CANADA_BUYS_DROP_COLUMNS)
    df = df.rename(columns=CANADA_BUYS_RENAMING)

    # Coerce the numeric types out of strings.
    float_columns = CANADA_BUYS_FLOAT_COLUMNS
    for float_column in float_columns:
        df[float_column] = pd.to_numeric(df[float_column], errors='coerce')
        df[float_column] = df[float_column].astype(np.float32)

    # Convert relevant columns to datetime.
    date_columns = CANADA_BUYS_DATE_COLUMNS
    for date_column in date_columns:
        df[date_column] = pd.to_datetime(df[date_column],
                                         format='%Y-%m-%d',
                                         errors='coerce')

    df['number_of_records'] = df['number_of_records'].round().astype('Int32')

    for column in [
            'supplier_legal_name', 'supplier_standardized_name',
            'supplier_operating_name'
    ]:
        df[column] = df[column].str.strip().replace('  ', ' ')

    df.to_csv(csv_orig_filename, index=False)
    df.to_json(json_orig_filename, orient='records', indent=1)

    unique_columns = CANADA_BUYS_UNIQUE_COLUMNS
    export_for_postgres(df, csv_filename, unique_columns)

    unique_vendors = list(df['supplier_legal_name'].fillna(value='').unique())
    unique_vendors_norm = []
    for vendor in unique_vendors:
        vendor = vendor.lower().strip()
        unique_vendors_norm.append(vendor)
    unique_vendors_norm = sorted(list(set(unique_vendors_norm)))
    with open(unique_vendors_filename, 'w') as outfile:
        json.dump(unique_vendors_norm, outfile, indent=1)


def retrieve_pwsgc_contracts(
        csv_orig_filename=PWSGC_CSV_ORIG,
        json_orig_filename=PWSGC_JSON_ORIG,
        csv_filename=PWSGC_CSV,
        unique_suppliers_filename=PWSGC_UNIQUE_NAMES_JSON):
    # Read in the CSV as a DataFrame
    df = pd.read_csv(PWSGC_CONTRACTS_RECENT_URL, engine='c')

    # Drop the redundant French descriptions since we display in English.
    df = df.drop(columns=[
        'gsin-description_fr', 'competitive-tender_fr',
        'limited-tender-reason-description_fr',
        'solicitation-procedure-description_fr',
        'trade-agreement-description_fr', 'organization-employee-count_fr',
        'end-user-entity_fr', 'contracting-entity-office-name_fr',
        'procurement-entity-name_fr', 'country-description_fr'
    ])
    df = df.rename(
        columns={
            'gsin-description_en': 'gsin-description',
            'competitive-tender_en': 'competitive-tender',
            'limited-tender-reason-description_en':
            'limited-tender-reason-description',
            'solicitation-procedure-description_en':
            'solicitation-procedure-description',
            'trade-agreement-description_en': 'trade-agreement-description',
            'organization-employee-count_en': 'organization-employee-count',
            'end-user-entity_en': 'end-user-entity',
            'contracting-entity-office-name_en':
            'contracting-entity-office-name',
            'procurement-entity-name_en': 'procurement-entity-name',
            'country-description_en': 'country-description'
        })

    # Drop the procurement-entity-name column since it is always
    # 'Public Works and Government Services Canada'.
    # We also drop the 'country-description' column since we can infer it
    # from the country-percentage keys. Likewise for 'gsin',
    # 'limited-tender-reason',  and 'trade-agreement'
    df = df.drop(columns=[
        'procurement-entity-name', 'country-description', 'gsin',
        'limited-tender-reason', 'trade-agreement'
    ])

    # Convert relevant columns to datetime.
    date_columns = ['award-date', 'expiry-date', 'date-file-published']
    for date_column in date_columns:
        df[date_column] = pd.to_datetime(df[date_column],
                                         errors='coerce')

    df['number-records'] = df['number-records'].round().astype('Int32')

    # Convert hyphens in keys to underscores.
    keys = list(df.keys())
    remove_underscores_map = {}
    for key in keys:
        remove_underscores_map[key] = key.replace('-', '_')
    df = df.rename(columns=remove_underscores_map)

    # Strip the vendor name and replace double-spaces with a single space.
    df['supplier_legal_name'] = df['supplier_legal_name'].str.strip().replace(
        '  ', ' ')

    df.to_csv(csv_orig_filename, index=False)
    df.to_json(json_orig_filename, orient='records', indent=1)

    # Replace any Nan or empty string in the supplier_legal_name with the
    # supplier_standardized_name.
    df['supplier_legal_name'] = df['supplier_legal_name'].replace(np.nan,
                                                                  '',
                                                                  regex=True)
    df['supplier_legal_name'] = np.where(df['supplier_legal_name'] == '',
                                         df['supplier_standardized_name'],
                                         df['supplier_legal_name'])

    unique_columns = [
        'contract_number', 'amendment_number', 'supplier_legal_name'
    ]
    export_for_postgres(df, csv_filename, unique_columns)

    unique_suppliers = list(
        df['supplier_legal_name'].fillna(value='').unique())
    unique_suppliers_norm = []
    for supplier in unique_suppliers:
        supplier = supplier.lower().strip()
        unique_suppliers_norm.append(supplier)
    unique_suppliers_norm = sorted(list(set(unique_suppliers_norm)))
    with open(unique_suppliers_filename, 'w') as outfile:
        json.dump(unique_suppliers_norm, outfile, indent=1)


def retrieve_award_notices(
        csv_filename='ca_buyandsell_award_filings_orig.csv',
        postgres_csv_filename='ca_buyandsell_award_filing.csv',
        unique_suppliers_filename='ca_buyandsell_award_unique_suppliers.json'):
    # Read in the CSV as a DataFrame
    df = pd.read_csv(AWARDS_URL)

    # Drop the redundant French rows (only keeping the English)
    df = df[df['language'] != AWARDS_FRENCH_TAG]

    # Drop the 'procurement_entity_name' column (it should equal 'Public Works
    # and Government Services Canada'), the 'language' column (it should equal
    # 'English' in all entries), and the 'access_terms_of_use' column.
    df = df.drop(
        columns=['procurement_entity_name', 'language', 'access_terms_of_use'])

    # Convert relevant columns to datetime.
    date_columns = ['award_date', 'publication_date', 'amendment_date']
    for date_column in date_columns:
        df[date_column] = pd.to_datetime(df[date_column],
                                         format='%Y-%m-%d',
                                         errors='coerce')

    df.to_csv(csv_filename, index=False)

    unique_columns = [
        'reference_number', 'solicitation_number', 'contract_sequence_number',
        'contract_number'
    ]
    export_for_postgres(df, postgres_csv_filename, unique_columns)

    unique_suppliers = list(df['supplier_info'].fillna(value='').unique())
    unique_suppliers_norm = []
    for supplier in unique_suppliers:
        supplier = supplier.lower().strip()
        unique_suppliers_norm.append(supplier)
    unique_suppliers_norm = sorted(list(set(unique_suppliers_norm)))
    with open(unique_suppliers_filename, 'w') as outfile:
        json.dump(unique_suppliers_norm, outfile, indent=1)


# A simplified interface to retrieving and exporting the datasets we use.
def retrieve():
    retrieve_pwsgc_contracts()
    retrieve_proactive_disclosures()


def postgres_create_proactive_disclosures(user=None,
                                          password=None,
                                          dbname=None,
                                          port=None,
                                          host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(PROACTIVE_TABLE_NAME,
                                       PROACTIVE_TABLE_TYPES,
                                       serial_id=True,
                                       constraints={
                                           'unique':
                                           '''UNIQUE (
  reference_number, procurement_id, vendor_name, vendor_postal_code, buyer_name,
  contract_date, owner_org
)'''
                                       }))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_vendor_index ON {} (LOWER(vendor_name));
    '''.format(PROACTIVE_TABLE_NAME, PROACTIVE_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(reference_number),       '') || ' ' ||
    coalesce(lower(procurement_id),         '') || ' ' ||
    coalesce(lower(vendor_name),            '') || ' ' ||
    coalesce(lower(vendor_postal_code),     '') || ' ' ||
    coalesce(lower(buyer_name),             '') || ' ' ||
    coalesce(lower(description),            '') || ' ' ||
    coalesce(contract_value::TEXT,          '') || ' ' ||
    coalesce(original_value::TEXT,          '') || ' ' ||
    coalesce(amendment_value::TEXT,         '') || ' ' ||
    coalesce(lower(comments),               '') || ' ' ||
    coalesce(lower(additional_comments),    '') || ' ' ||
    coalesce(lower(trade_agreement),        '') || ' ' ||
    coalesce(lower(country_of_vendor),      '') || ' ' ||
    coalesce(lower(solicitation_procedure), '') || ' ' ||
    coalesce(lower(contracting_entity),     '') || ' ' ||
    coalesce(lower(reporting_period),       '') || ' ' ||
    coalesce(lower(owner_org),              '') || ' ' ||
    coalesce(lower(owner_org_title),        '')
  )
);
    '''.format(PROACTIVE_TABLE_NAME, PROACTIVE_TABLE_NAME))


def postgres_update_proactive_disclosures(export_csv=PROACTIVE_CSV,
                                          user=None,
                                          password=None,
                                          dbname=None,
                                          port=None,
                                          host=None,
                                          vacuum=True):
    postgres_create_proactive_disclosures(user, password, dbname, port, host)
    util.postgres.update(PROACTIVE_TABLE_NAME,
                         PROACTIVE_TABLE_TYPES,
                         PROACTIVE_UNIQUE_COLUMNS,
                         PROACTIVE_FORCE_NULL_COLUMNS,
                         PROACTIVE_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_create_canada_buys(user=None,
                                password=None,
                                dbname=None,
                                port=None,
                                host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(CANADA_BUYS_TABLE_NAME,
                                       CANADA_BUYS_TABLE_TYPES,
                                       serial_id=True,
                                       constraints={
                                           'unique':
                                           '''UNIQUE (
    reference_number, amendment_number, procurement_number,
    solicitation_number, contract_number
)'''
                                       }))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_vendor_index ON {} (LOWER(supplier_legal_name));
    '''.format(CANADA_BUYS_TABLE_NAME, CANADA_BUYS_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    coalesce(lower(title),                                  '') || ' ' ||
    coalesce(lower(reference_number),                       '') || ' ' ||
    coalesce(lower(amendment_number),                       '') || ' ' ||
    coalesce(lower(procurement_number),                     '') || ' ' ||
    coalesce(lower(solicitation_number),                    '') || ' ' ||
    coalesce(lower(contract_number),                        '') || ' ' ||
    coalesce(lower(contract_status),                        '') || ' ' ||
    coalesce(lower(gsin),                                   '') || ' ' ||
    coalesce(lower(gsin_description),                       '') || ' ' ||
    coalesce(lower(unspsc),                                 '') || ' ' ||
    coalesce(lower(unspsc_description),                     '') || ' ' ||
    coalesce(lower(procurement_category),                   '') || ' ' ||
    coalesce(lower(notice_type),                            '') || ' ' ||
    coalesce(lower(procurement_method),                     '') || ' ' ||
    coalesce(lower(selection_criteria),                     '') || ' ' ||
    coalesce(lower(limited_tendering_reason),               '') || ' ' ||
    coalesce(lower(trade_agreements),                       '') || ' ' ||
    coalesce(lower(regions_of_delivery),                    '') || ' ' ||
    coalesce(lower(supplier_legal_name),                    '') || ' ' ||
    coalesce(lower(supplier_standardized_name),             '') || ' ' ||
    coalesce(lower(supplier_operating_name),                '') || ' ' ||
    coalesce(lower(supplier_employee_count),                '') || ' ' ||
    coalesce(lower(supplier_address_line),                  '') || ' ' ||
    coalesce(lower(supplier_address_city),                  '') || ' ' ||
    coalesce(lower(supplier_address_province),              '') || ' ' ||
    coalesce(lower(supplier_address_postal_code),           '') || ' ' ||
    coalesce(lower(supplier_address_country),               '') || ' ' ||
    coalesce(lower(contracting_entity_name),                '') || ' ' ||
    coalesce(lower(contracting_entity_address_line),        '') || ' ' ||
    coalesce(lower(contracting_entity_address_city),        '') || ' ' ||
    coalesce(lower(contracting_entity_address_province),    '') || ' ' ||
    coalesce(lower(contracting_entity_address_postal_code), '') || ' ' ||
    coalesce(lower(contracting_entity_address_country),     '') || ' ' ||
    coalesce(lower(end_user_entities_name),                 '') || ' ' ||
    coalesce(lower(end_user_entities_office_name),          '') || ' ' ||
    coalesce(lower(end_user_entities_address),              '') || ' ' ||
    coalesce(lower(contact_info_name),                      '') || ' ' ||
    coalesce(lower(contact_info_email),                     '') || ' ' ||
    coalesce(lower(contact_info_phone),                     '') || ' ' ||
    coalesce(lower(contact_info_fax),                       '') || ' ' ||
    coalesce(lower(contact_info_address_line),              '') || ' ' ||
    coalesce(lower(contact_info_city),                      '') || ' ' ||
    coalesce(lower(contact_info_province),                  '') || ' ' ||
    coalesce(lower(contact_info_postal_code),               '') || ' ' ||
    coalesce(lower(contact_info_country),                   '') || ' ' ||
    coalesce(lower(tender_description),                     '')
  )
);
    '''.format(CANADA_BUYS_TABLE_NAME, CANADA_BUYS_TABLE_NAME))


def postgres_update_canada_buys(export_csv=CANADA_BUYS_CSV,
                                user=None,
                                password=None,
                                dbname=None,
                                port=None,
                                host=None,
                                vacuum=True):
    postgres_create_canada_buys(user, password, dbname, port, host)
    util.postgres.update(CANADA_BUYS_TABLE_NAME,
                         CANADA_BUYS_TABLE_TYPES,
                         CANADA_BUYS_UNIQUE_COLUMNS,
                         CANADA_BUYS_FORCE_NULL_COLUMNS,
                         CANADA_BUYS_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_create_pwsgc(user=None,
                          password=None,
                          dbname=None,
                          port=None,
                          host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(
            PWSGC_TABLE_NAME,
            PWSGC_TABLE_TYPES,
            serial_id=True,
            constraints={
                'unique':
                'UNIQUE (contract_number, amendment_number, supplier_legal_name)'
            }))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_supplier_index ON {} (
  LOWER(supplier_legal_name)
);
    '''.format(PWSGC_TABLE_NAME, PWSGC_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    COALESCE(LOWER(contract_number),                    '') || ' ' ||
    COALESCE(LOWER(amendment_number),                   '') || ' ' ||
    COALESCE(LOWER(gsin_description),                   '') || ' ' ||
    COALESCE(LOWER(limited_tender_reason_description),  '') || ' ' ||
    COALESCE(LOWER(solicitation_procedure_description), '') || ' ' ||
    COALESCE(LOWER(trade_agreement_description),        '') || ' ' ||
    COALESCE(LOWER(supplier_standardized_name),         '') || ' ' ||
    COALESCE(LOWER(supplier_operating_name),            '') || ' ' ||
    COALESCE(LOWER(supplier_legal_name),                '') || ' ' ||
    COALESCE(LOWER(supplier_address_city),              '') || ' ' ||
    COALESCE(LOWER(supplier_address_prov_state),        '') || ' ' ||
    COALESCE(LOWER(supplier_address_postal_code),       '') || ' ' ||
    COALESCE(LOWER(supplier_address_country),           '') || ' ' ||
    COALESCE(LOWER(organization_employee_count),        '') || ' ' ||
    COALESCE(LOWER(end_user_entity),                    '') || ' ' ||
    COALESCE(LOWER(contracting_entity_office_name),     '') || ' ' ||
    COALESCE(LOWER(contracting_address_street_1),       '') || ' ' ||
    COALESCE(LOWER(contracting_address_street_2),       '') || ' ' ||
    COALESCE(LOWER(contracting_address_city),           '') || ' ' ||
    COALESCE(LOWER(contracting_address_prov_state),     '') || ' ' ||
    COALESCE(LOWER(contracting_address_postal_code),    '') || ' ' ||
    COALESCE(LOWER(contracting_address_country),        '') || ' ' ||
    COALESCE(LOWER(country_percentage::TEXT),           '')
  )
);
    '''.format(PWSGC_TABLE_NAME, PWSGC_TABLE_NAME))


def postgres_update_pwsgc(export_csv=PWSGC_CSV,
                          user=None,
                          password=None,
                          dbname=None,
                          port=None,
                          host=None,
                          vacuum=True):
    postgres_create_pwsgc(user, password, dbname, port, host)
    util.postgres.update(PWSGC_TABLE_NAME,
                         PWSGC_TABLE_TYPES,
                         PWSGC_UNIQUE_COLUMNS,
                         PWSGC_FORCE_NULL_COLUMNS,
                         PWSGC_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_update(proactive_csv=PROACTIVE_CSV,
                    canada_buys_csv=CANADA_BUYS_CSV,
                    pwsgc_csv=PWSGC_CSV,
                    user=None,
                    password=None,
                    dbname=None,
                    port=None,
                    host=None,
                    vacuum=True):
    postgres_update_proactive_disclosures(proactive_csv,
                                          user=user,
                                          password=password,
                                          dbname=dbname,
                                          port=port,
                                          host=host,
                                          vacuum=vacuum)
    postgres_update_canada_buys(canada_buys_csv,
                                user=user,
                                password=password,
                                dbname=dbname,
                                port=port,
                                host=host,
                                vacuum=vacuum)
    postgres_update_pwsgc(pwsgc_csv,
                          user=user,
                          password=password,
                          dbname=dbname,
                          port=port,
                          host=host,
                          vacuum=vacuum)


def get_pwsgc_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['ca', 'procurement', 'pwsgc'], entities)


def get_pwsgc_agency_normalization_map(entities=None):
    '''Inverts the agency codes into a normalization map.'''
    return normalization_map_helper(['ca', 'procurement', 'pwsgc'],
                                    entities,
                                    primary_key='agencyNames')


def get_proactive_disclosure_normalization_map(entities=None):
    '''Inverts the entity alternate spellings into a normalization map.'''
    return normalization_map_helper(['ca', 'procurement', 'proactive'],
                                    entities)


def get_proactive_disclosure_agency_normalization_map(entities=None):
    '''Inverts the agency codes into a normalization map.'''
    return normalization_map_helper(['ca', 'procurement', 'proactive'],
                                    entities,
                                    primary_key='agencyNames')
