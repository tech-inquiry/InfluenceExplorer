#
# Copyright (c) 2020-2025 Jack Poulson <jack@techinquiry.org>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import glob
import json
import os
import pandas as pd
import requests
import urllib3
import util.postgres

from tqdm import tqdm
from util.entities import canonicalize

# The SSL certificate for the above has been known to be expired, requiring
# verification to be disabled.
TENDERS_URL = 'https://lb.api.prozorro.gov.ua/api/2.5/tenders'

# The number of seconds to wait for a request to complete before moving on.
REQUEST_TIMEOUT = 10

DATA_DIR = 'data/ua_procurement'
TENDER_IDS_JSON = '{}/tender_ids.json'.format(DATA_DIR)
TENDERS_JSON = '{}/tenders.json'.format(DATA_DIR)
TENDERS_STORED_JSON = '{}/tenders_stored.json'.format(DATA_DIR)

EXPORT_DIR = 'export'
TENDER_CSV = '{}/ua_procurement.csv'.format(EXPORT_DIR)

MAX_CHARS = 1024 * 805

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

TENDER_TABLE_TYPES = {
    'uid': 'text',
    'tender': 'jsonb',
    'buyers': 'jsonb',
    'suppliers': 'jsonb',
    'date_created': 'timestamp'
}
TENDER_UNIQUE_COLUMNS = ['uid']
TENDER_JSON_COLUMNS = ['tender', 'buyers', 'suppliers']
TENDER_FORCE_NULL_COLUMNS = ['tender', 'buyers', 'suppliers', 'date_created']
TENDER_TABLE_NAME = 'ua_tender'
TENDER_TABLE_TMP_NAME = '{}_tmp'.format(TENDER_TABLE_NAME)


def id_from_contract(contract):
    return contract['data']['id']


def date_created_from_contract(contract):
    return contract['data']['dateCreated']


def buyer_names_from_contract(contract):
    data = contract['data']
    entity = data['procuringEntity']
    names = set()
    names.add(canonicalize(entity['name']))
    if 'identifier' in entity and 'legalName' in entity['identifier']:
        names.add(canonicalize(entity['identifier']['legalName']))
    return sorted(list(names))


# Entities exist at the path:
#   data->contracts[]->suppliers[]->'name'
# for each entry.
def suppliers_from_contract(contract):
    suppliers = set()
    data = contract['data']
    for category in ['awards', 'contracts']:
        if category in data:
            for contract in data[category]:
                if 'suppliers' not in contract:
                    continue
                for supplier in contract['suppliers']:
                    suppliers.add(canonicalize(supplier['name']))
    return sorted(list(suppliers))


def get_tender_ids(max_pages=100000, offset=None, verify=False):
    with requests.session() as session:
        ids = []
        try:
            url = '{}?offset={}'.format(TENDERS_URL, offset) if offset else TENDERS_URL
            print('Querying {}'.format(url))
            raw_response = session.get(url, verify=verify)
            response = raw_response.json()
            print(response)
            ids = response['data']
            num_pages = 0
            while (num_pages < max_pages and 'next_page' in response
                   and response['next_page']['uri']):
                next_uri = response['next_page']['uri']
                print('  Requesting page {}'.format(next_uri))
                raw_response = session.get(next_uri, verify=verify)
                try:
                    response = raw_response.json()
                    ids += response['data']
                except Exception as e:
                    print(e)
                    break
                num_pages += 1
        except Exception as e:
            print(e)
        return ids


def get_tender(tender_id, session, verify=False):
    uri = '{}/{}'.format(TENDERS_URL, tender_id)
    raw_response = session.get(uri, verify=verify, timeout=REQUEST_TIMEOUT)
    return raw_response.json()


def get_new_tenders_from_ids(tender_ids,
                             stored_tenders,
                             max_requests=10000,
                             verify=False):
    tenders = {}
    with requests.session() as session:
        num_requests = 0
        with tqdm(total=max_requests) as pbar:
            # Grab the most recent first.
            for item in reversed(tender_ids):
                tender_id = item['id']
                if tender_id in stored_tenders:
                    continue
                try:
                    tenders[tender_id] = get_tender(tender_id,
                                                    session,
                                                    verify=verify)
                except requests.exceptions.Timeout:
                    print('Requests of tender {} timed out.'.format(tender_id))
                    continue
                except Exception as e:
                    print(e)
                    print('Could not retrieve tender {}'.format(tender_id))
                    continue
                num_requests += 1
                pbar.update(1)
                if num_requests == max_requests:
                    break
    return tenders


def store_tenders_by_day(tenders, data_dir=DATA_DIR, verbose=True):
    tenders_by_day = {}
    for uid in tenders:
        tender = tenders[uid]
        date_created = tender['data']['dateCreated'].split('T')[0]
        key = date_created[:10]
        if key not in tenders_by_day:
            tenders_by_day[key] = []
        tenders_by_day[key].append(tender)
    for key in tenders_by_day:
        day_json = '{}/tenders-{}.json'.format(data_dir, key)
        new_data = tenders_by_day[key]
        if os.path.exists(day_json):
            # TODO(Jack Poulson): Ensure that, in the case of corruptions in
            # the list of stored UIDs, we do not duplicate.
            old_data = json.load(open(day_json))
            if verbose:
                print('{}: Adding {} tenders into previous {}.'.format(
                    key, len(new_data), len(old_data)))
            combined_data = old_data + new_data
        else:
            if verbose:
                print('{}: Storing {} tenders.'.format(key, len(new_data)))
            combined_data = new_data
        with open(day_json, 'w') as outfile:
            json.dump(combined_data, outfile, indent=2)


def retrieve_tenders_from_ids(tender_ids=None,
                              tenders_json=TENDERS_JSON,
                              max_requests=10000,
                              verify=False):
    if tender_ids == None:
        tender_ids = json.load(open(TENDER_IDS_JSON))
    if os.path.exists(TENDERS_STORED_JSON):
        stored_tenders = set(json.load(open(TENDERS_STORED_JSON)))
    else:
        stored_tenders = set()
    for offset in range(0, len(tender_ids), max_requests):
        tenders = get_new_tenders_from_ids(tender_ids,
                                           stored_tenders,
                                           max_requests=max_requests,
                                           verify=verify)
        store_tenders_by_day(tenders)
        stored_tenders.update(list(tenders.keys()))
        with open(TENDERS_STORED_JSON, 'w') as outfile:
            json.dump(sorted(list(stored_tenders)), outfile, indent=2)


def postgres_create_tender(user=None,
                           password=None,
                           dbname=None,
                           port=None,
                           host=None):
    cursor = util.postgres.get_cursor(user=user,
                                      password=password,
                                      dbname=dbname,
                                      port=port,
                                      host=host)
    cursor.execute(
        util.postgres.create_table_sql(TENDER_TABLE_NAME,
                                       TENDER_TABLE_TYPES,
                                       serial_id=True,
                                       constraints={'unique': 'UNIQUE (uid)'}))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_suppliers_index ON {} USING GIN (suppliers);
    '''.format(TENDER_TABLE_NAME, TENDER_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_buyers_index ON {} USING GIN (buyers);
    '''.format(TENDER_TABLE_NAME, TENDER_TABLE_NAME))
    cursor.execute('''
CREATE INDEX IF NOT EXISTS {}_search_index ON {} USING GIN (
  to_tsvector('simple',
    left( 
      uid                 || ' ' ||
      lower(tender::text) || ' ' ||
      suppliers::text     || ' ' ||
      buyers::text,
      {}
    )
  )
);
    '''.format(TENDER_TABLE_NAME, TENDER_TABLE_NAME, MAX_CHARS))


def postgres_update_tender(export_csv=TENDER_CSV,
                           user=None,
                           password=None,
                           dbname=None,
                           port=None,
                           host=None,
                           vacuum=True):
    postgres_create_tender(user, password, dbname, port, host)
    util.postgres.update(TENDER_TABLE_NAME,
                         TENDER_TABLE_TYPES,
                         TENDER_UNIQUE_COLUMNS,
                         TENDER_FORCE_NULL_COLUMNS,
                         TENDER_TABLE_TMP_NAME,
                         export_csv,
                         user=user,
                         password=password,
                         dbname=dbname,
                         port=port,
                         host=host,
                         vacuum=vacuum)


def postgres_update():
    postgres_update_tender()


def export_to_csv(tenders, output_csv=TENDER_CSV):
    id_list = []
    suppliers_lists = []
    buyers_lists = []
    dates_created = []
    for tender in tenders:
        id_list.append(id_from_contract(tender))
        suppliers_lists.append(suppliers_from_contract(tender))
        buyers_lists.append(buyer_names_from_contract(tender))
        dates_created.append(date_created_from_contract(tender))
    df = pd.DataFrame({
        'uid': id_list,
        'tender': tenders,
        'buyers': buyers_lists,
        'suppliers': suppliers_lists,
        'date_created': dates_created
    })
    df = util.postgres.preprocess_dataframe(df,
                                            json_columns=TENDER_JSON_COLUMNS)
    util.postgres.df_to_csv(df, output_csv)


def export_and_update_tenders():
    filenames = glob.glob('data/ua_procurement/tenders-*-*.json')
    for filename in filenames:
        vacuum = filename == filenames[-1]
        print('Exporting {}'.format(filename))
        data = json.load(open(filename))
        export_to_csv(data)
        postgres_update_tender(vacuum=vacuum)
