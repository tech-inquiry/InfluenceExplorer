# Influence Explorer

![Covered Countries](webserver/node/public/logos/tech_inquiry_countries.png)

This repository is deployed on the last several years of procurement data
at [techinquiry.org/](https://techinquiry.org/)

## Overview

The Influence Exporer provides utilities for retrieving and navigating
international procurement and lobbying data. The current focus is on Five Eyes
nations (US, UK, Canada, Australia, and New Zealand) and Europe.
Perhaps the most unique feature of this codebase is an entity recommender
system built on top of these datasets.

## Prerequisites

The [python pip](https://pypi.org/project/pip/) packages used by the Influence
Explorer can be installed via:

```
cd workstation/
pip install -r pip_requirements.txt
```

In order to download US lobbying quarterly reports and political contributions
at a reasonable rate from the Senate OPR
[REST API](https://lda.senate.gov/api/),
it is highly recommended to
[register](https://lda.senate.gov/api/register/)
for an API key. Our python API makes use of the resulting bearer token.

## Setting up the Postgres tables

While this project makes use of a single `git` repository, one might typically
want to check out two copies: one on an offline 'workstation' meant to for
running the analytics and data collection in the `workstation/` folder, and
another on the -- usually lightweight -- 'webserver' meant for hosting the site
itself, using the `webserver/` folder.

It is on the 'webserver' where we maintain a
[PostgreSQL](https://www.postgresql.org/) database -- which we
historically named `contracts_and_lobbying` -- whose tables can be initialized
using two scripts (which we run from within `webserver/`).
The first creates composite types and needs to be executed as superuser:

```
\include sql/create_superuser.sql
```

and the main script can be executed as the owner of the `contracts_and_lobbying`
database:

```
\include sql/create.sql
```

### Setting up an SSH tunnel to Postgres

Opening an SSH tunnel for Postgres between the workstation and the webserver
allows a `psql` prompt on the workstation to connect to the webserver's Postgres
instance, so that updates can be transferred from local CSV files using scripts
built on top of `\copy` command (such as `webserver/sql/update_remote.sql`). If the environment variables `INF_EXP_POSTGRES_USER`, `INF_EXP_POSTGRES_PASSWORD`, `INF_EXP_POSTGRES_DATABASE`, `INF_EXP_POSTGRES_PORT`, and `INF_EXP_POSTGRES_HOST` are set to capture the local Postgres configuration, then the daily retrieval (`workstation/util/retrieve.py`) and embedding generation (`workstation/util/generate_embeddings.py`) will automatically perform the remote database updates (using `psycopg2` and its `copy_expert` utility).

Opening the SSH tunnel to map the Postgres port on the webserver (by default, 5432) to another port on the workstation (say, 6432), can be accomplished manually through the `ssh` command, e.g.:

```bash
ssh -L 6432:${WEBSERVER}:5432 ${USER}@${WEBSERVER}
```

or in a more daemonized fashion via `[autossh](http://www.harding.motd.ca/autossh/)` (see [superuser.com/a/37768](https://superuser.com/a/37768)).

Remote connections to Postgres may require changes to the webserver's Postgres configuration which are similar to those in Stack Exchange answer
[dba.stackexchange.com/a/84002](https://dba.stackexchange.com/a/84002).

After such configurations are in place (and Postgres is restarted), a connection can be established via:

```bash
psql -p 6432 -U tech_inquiry -d contracts_and_lobbying -h 127.0.0.1
```

## Government Data Feeds

This project was born out of a desire to understand the interplay between major
US tech companies and US federal military and law enforcement agencies: The
analysis in Tech Inquiry's
[first report](https://techinquiry.org/SiliconValley-Military/)
was performed using a collection of one-off tools which were merged and
extended into this project.
Our initial focus was on analyzing the
[IBM-run](https://fcw.com/articles/2010/02/18/integrate-acquisition-databases.aspx)
US
[Federal Procurement Data System](https://www.fpds.gov/fpdsng_cms/index.php/en/)
(FPDS), but we have since expanded to central government procurement across the
[Five Eyes](https://en.wikipedia.org/wiki/Five_Eyes)
intelligence alliance (the United States plus the United Kingdom and
three of its major commonwealths: Canada, Australia, and New Zealand),
Europe (through [Tenders Electronic Daily](https://ted.europa.eu/)),
and Israel.

We also integrate quasi-daily DoD
[contract announcements](https://www.defense.gov/Newsroom/Contracts/),
US federal lobbying quarterly reports (LD-1/LD-2 filings) and
political contributions (LD-203 filings) using the
[REST API](https://lda.senate.gov/api/)
maintained by the Senate
[Office of Public Records](https://www.cop.senate.gov/legislative/opr.htm).
And we scrape
[National Labor Relations Board](https://www.nlrb.gov/cases-decisions)
cases and
[Securities and Exchange Commission](https://www.sec.gov/edgar/searchedgar/companysearch.html)
metadata using
[Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/).

At the US state level, we mirror:

1. California state lobbying data from the [CALACCESS zipfile](https://www.sos.ca.gov/campaign-lobbying/cal-access-resources/raw-data-campaign-finance-and-lobbying-activity),
2. The [Florida Accountability Tracking System](https://facts.fldfs.com/Search/ContractAdvancedSearch.aspx), and
3. The New York [Office of General Services](https://ogs.ny.gov/) [statewide contracts](https://online.ogs.ny.gov/purchase/spg/pdfdocs/StatewideTC.pdf) -- extracting tables from the latter PDF using [Tabula](https://tabula.technology/).

Scraping Florida state procurement data, as well as downloading New Zealand
data without being blocked by a firewall, is accomplished through scripting a
headless Firefox browser via [Selenium](https://www.selenium.dev/).

Other than setting up the initial backfill of contracts, retrieval and import
into Postgres is accomplished through two scripts. On the workstation from the
`workstation/` folder:

```python
python util/retrieve.py
```

If the `INF_EXP_POSTGRES_USER`, `INF_EXP_POSTGRES_PASSWORD`, `INF_POSTGRES_DATABASE`, `INF_EXP_POSTGRES_PORT`, `INF_EXP_POSTGRES_HOST` environment variables were set to describe a Postgres connection configuration, then this script will have updated the Postgres database using the `psycopg2` Python interface to Postgres.

### United States

#### Central

##### Procurement [Updates Daily]

###### Procurement Retrieval

A single day of procurement summaries -- say, May 1, 2021 -- can be downloaded
from the XML ATOM feed of the Federal Procurement Data System (FPDS) via:

```bash
cd workstation
import gov.us.central.procurement as fpds_award
fpds.retrieve('2021-05-21', USE_MODIFIED)
```

where `USE_MODIFIED` should be `True` if all records modified that day are
requested, vs. `False` if only the smaller set of awards signed that day are
desired.

By default, the `fpds.retrieve` function exports the results into a CSV --
located at `export/us_federal_award.csv` -- meant for import into a Postgres
database. This can be disabled via an optional `export_csv=False` argument.

One can also easily store yesterday's modified contracts via:

```bash
fpds.retrieve()
```

###### Procurement Export to Postgres

We can also manually export a CSV which contains the records of arbitrary
date ranges via commands of the form:

```python
import gov.us.central.procurement as fpds
fpds.export(input_regexp, use_modification_date)
```

For example, to create a CSV at `export/us_federal_award.csv` containing our
previously retrieved batch of all awards modified in May, 2021:

```python
import gov.us.central.procurement as fpds
fpds.export('2021.05.*.json', True)
```

One can select more general groups of files with the style of regular
expressions expected by Python's `glob.glob`.

###### Import to Postgres

If the `INF_EXP` environment variables for the Postgres connection are set up,
the exported CSV can be uploaded into Postgres via:

```python
import gov.us.central.procurement as fpds
fpds.postgres.update()
```

###### DoD Announcement Retrieval [Updates Daily]

The most recent batch of DoD contract announcements (usually updated at 5PM ET
on weekdays) can be retrieved and exported into
`export/us_dod_announcements.csv` via:

```python
import gov.us.central.procurement as fpds
fpds.dod_announce.retrieve()
```

and then imported via:

```
fpds.dod_announce.postgres_update()
```

If a fully copy of all announcements is desired, then one can simply specify
a `max_page` argument to `retrieve` that is sufficiently large. At the time of
this writing (May 18, 2021), the appropriate value would be 172. For example:

```python
fpds.dod_announce.retrieve(max_page=172)
```

##### Lobbying [Updates Daily]

We can simultaneously download and form a Postgres-importable CSV file for the
US lobbying quarterly reports posted over a given date range via Python commands
of the form:

```python
import lobbying
data = lobbying.retrieve_filings(
    '2021-04-25', '2021-05-02', token=YOUR_BEARER_TOKEN)
```

The data can be uploaded into Postgres via:

```python
lobbying.postgres_update_filings()
```

A similar process can be executed for political contributions via:

```python
data = lobbying.retrieve_contributions(
    '2021-04-25', '2021-05-02', token=YOUR_BEARER_TOKEN)
lobbying.postgres_update_contributions()
```

Both results can be gathered and stored via the combined command

```python
data = lobbying.retrieve('2021-04-25', '2021-05-02', token=YOUR_BEARER_TOKEN)
lobbying.postgres_update()
```

and simply calling

```python
data = lobbying.retrieve()
```

will gather the last 31 days of data and attempt to load the bearer token from
the environment variable `US_SENATE_OPR_REST_TOKEN`.

##### Labor Relations [Updates Daily]

The CSV exports of recent NLRB cases linked from
[nlrb.gov/search/case](https://www.nlrb.gov/search/case) are currently broken.
Tech Inquiry's NLRB interface was built on this feed and we are currently
communicating with the NLRB to request a fix.

Nevertheless, one can retrieve and then export into Postgres via:
via:

```python
import gov.us.central.labor_relations as nlrb
nlrb.retrieve()
nlrb.postgres_update()
```

##### Securities [Updates Daily]

Due to a known memory leak in the `lxml` library used by `BeautifulSoup` for
properly parsing tables, our scraper for SEC filing metadata requires breaking
up large batch downloads into small chunks. This is accomplished through the
`retrieve` function in the `gov.us.central.securities` module. After executing:

```python
import gov.us.central.securities as sec
sec.retrieve()
sec.postgres_update()
```

#### California State

##### Lobbying [Updates Daily]

We automate the processing of the zipfile of lobbying disclosures made public
by [CALACCESS](https://cal-access.sos.ca.gov/Lobbying/):

```python
import gov.us.ca.lobbying as calaccess
calaccess.retrieve()
calaccess.postgres_update()
```

#### Florida State

##### Procurement [Updates Daily]

Manually downloading Florida State procurement data through their FL FACTS
system would require performing an advanced search at
[facts.fldfs.com/Search/ContractAdvancedSearch.aspx](https://facts.fldfs.com/Search/ContractAdvancedSearch.aspx)
with a Begin and End dates specified. After hitting 'Search', a CSV named 'ContractsExport.csv' can be downloaded by hitting the 'Download Results' button.

This process, along with export into Postgres, is automated via:

```python
import gov.us.fl.procurement as flfacts
flfacts.retrieve()
flfacts.postgres_update()
```

using a Selenium script which scripts Firefox browser interactions.

#### New York State

##### Procurement [Updates Roughly Monthly]

Our primary source is the PDF located at
[online.ogs.ny.gov/purchase/spg/pdfdocs/StatewideTC.pdf](https://online.ogs.ny.gov/purchase/spg/pdfdocs/StatewideTC.pdf),
which we extract tables from using the
[Python wrapper](https://pypi.org/project/tabula-py/)
for
[Tabula](https://tabula.technology/).

Extraction and export into Postgres can be performed via:

```python
import gov.us.ny.procurement as nyogs
nyogs.retrieve()
nyogs.postgres_update()
```

### United Kingdom

##### Procurement [Updates Daily]

###### Retrieval and Export of Single Day

A batch of procurement records over a given date range can be downloaded in the
original flattened format, unflattened, then exported for Postgres in
`export/uk_contract_filing.csv` via, for example:

```python
import gov.uk.procurement as uk_contracts
uk_contracts.retrieve('2021-04-25', '2021-05-02')
uk_contracts.postgres_update()
```

Alternatively, we can retrieve the last 31 days via:

```python
uk_contracts.retrieve()
```

### Canada

#### Lobbying

We automate the processing of the Canadian federal lobbying communications
and registrations made available through
[Lobby Canada](https://lobbycanada.gc.ca/en/open-data/):

```python
import gov.ca.lobbying as calobby
calobby.retrieve()
calobby.postgres_update()
```

#### Procurement [Updates Monthly/Quarterly]

Unlike other government entities, Canadian federal procurement data is split
between two separate datasets: quarterly-updated
['proactive disclosures'](https://open.canada.ca/data/en/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b)
of minimal information about all contracts over $10K CAD, and monthly-updated
listings of all contracts awarded by
[Public Works and Government Services Canada](https://open.canada.ca/data/en/dataset/53753f06-8b28-42d7-89f7-04cd014323b0#wb-auto-6).

Both datasets can be downloaded and then exported into Postgres via:

```python
import gov.ca.procurement as ca_tenders
ca_tenders.retrieve()
ca_tenders.postgres_update()
```

### Australia

##### Procurement [Updates Daily]

###### Retrieval and Export of a Date Range

The procurement records spanning a range of datetimes can be downloaded and
exported Postgres via, for example:

```python
import gov.au.procurement as au_tenders
au_tenders.retrieve('2021-04-25', '2021-05-02')
au_tenders.postgres_update()
```

As in the case of UK tenders, we can retrieve over the last 31 days via:

```python
au_tenders.retrieve()
```

### New Zealand

##### Procurement [Updates Roughly Weekly]

As in the case of Florida state procurement data, we make use of a Selenium
script to simplify New Zealand contracting retrieval:

```python
import gov.nz.procurement as nz_awards
nz_awards.retrieve()
nz_awards.postgres_update()
```

### European Union

TODO, along with API endpoing documentation.

## Generating embeddings and nearest neighbors

Nearest neighbors lists can be generated alongside an embedding model via
executing:

```bash
python util/generate_embeddings.py
```

As in the case of the daily retrieval script (`workstation/util/retrieve.py`),
if the `INF_EXP_POSTGRES` environment variables were set to describe a Postgres
connection configuration, then the produced entity and entity neighbors lists
will have been pushed into the Postgres database.

## Production Deployments

**The first time the server is being deployed to production after this MR is merged, do the following steps:**

1. pull down latest master on the server.
2. cd `webserver/node`
3. Run `npm run build:prod` to install the latest node packages and rebuild the FE.
4. Assuming step 3 had no issues, kill the existing server process (a brief one-time outage is needed for the restart this first time).
5. Run `npm run start:prod` to start the server via pm2.

After this point, the following will be in place:

- Zero-downtime rebuilds/redeploys with pm2 whenever a new change to `data/`, `webserver/node/src/`, `webserver/node/package-lock.json`, or '`webserver/node/react` is pulled from the git repository. (See `webserver/node/.husky/post-merge` for this change detection).
- automatic restarts if the Node server crashes with an unhandled error / promise rejection.

**For future deployments that don't involve pm2 configuration changes, deploying new changes has one step:**

1. pull down latest master on the server.

The post-merge hook will take care of rebuilding/reinstalling/restarting the server as needed.

### PM2 Commands

**These commands assume the current working directory is `webserver/node`.**

View running pm2 processes:

```
./runpm2.sh ls
```

View Node server logs:

```
./runpm2.sh logs
```

All other pm2 commands shown at the link below can be invoked via the ./runpm2.sh script:
https://pm2.io/docs/runtime/reference/pm2-cli/

### Manual Redeploys and Restarts

To trigger a manual zero-downtime redeploy:

```
./reload.sh
```

If a full restart/rebuild (with brief downtime) is needed again at some point:

```
./runpm2 delete all
npm run build-and-start:prod
```

## React Local Development

For local frontend development, webpack-dev-server can be used to serve the React assets.
It will automatically recompile React assets and reload the browser to reflect file changes immediately.

To use:

1. `npm run start:dev` to start the Node server (still needed to handle API requests).
2. `npm run start-fe:dev`
   - Both of the above can also be started together with a single command: `npm run start-all:dev`.
3. Open a browser and navigate to `http://localhost:9000`.

Note that the Node server will also still be serving the frontend at 127.0.0.1:3000, but that address will not receive updates from webpack-dev-server.

## Node Local Development

`npm run start:dev` starts the Node server with ts-node-dev, which will automatically restart when files in node/src change.

`npm run start:dev-pm2` can also be used for testing PM2 functionality in local dev (it will not reload on change however.)

Both scripts above will also start and connect to the development Postgres db (see docker-compose.yml).

## Local Dev with Docker

We can run the webserver's node application (exposed on port 3000) -- which
automatically loads a small sample of data (including a few for Palantir) via docker-compose from within the webserver/node directory:

```bash
docker-compose up node
```

After launching you can use the explorer (again, with a small sample dataset)
by pointing your browser to [localhost:3000](https://localhost:3000).

We can also run the workstation Docker image via:

```bash
docker-compose run workstation
```
