#
# A script to validate the data/entities.json entities file.
#
import json

# First ensure that the JSON parses.
entities = json.load(open('data/entities.json'))

office_data = json.load(open('data/us_federal_offices.json'))


errors = []


def note_error(msg):
    errors.append('ERROR: ' + msg)
    print(msg)


def validate_urls(name, profile):
    URL_LEAVES = [
        'about', 'blog', 'bhrrc', 'cage', 'casetext', 'collective actions',
        'court listener', 'crunchbase', 'dvids', 'facebook', 'flickr', 'gao',
        'github', 'gitlab', 'good jobs first', 'importyeti', 'influencewatch',
        'institutional', 'investor', 'journalism', 'lever', 'linkedin',
        'littlesis', 'locals', 'logo', 'logoSmall', 'medium', 'misc',
        'muckrock', 'lei', 'nlrb', 'opencorporates', 'opensecrets', 'nasdaq',
        'pinterest', 'pogo_fcmd', 'pogo_pentagon_revolving_door', 'propublica',
        'propublica_nonprofit', 'rdr', 'reddit', 'rumble', 'sbir', 'sec',
        'slideshare', 'snapchat', 'soundcloud', 'sourcewatch', 'spiceworks',
        'substack', 'transparency', 'tumblr', 'vimeo', 'whoprofits',
        'wikipedia', 'youtube'
    ]
    REFERENCE_LEAVES = [
        'org', 'title', 'authors', 'date', 'url', 'accessed'
    ]
    # The deprecated / unused extensions of the above.
    REFERENCE_LEAVES_DEPR = ['note']

    if 'urls' not in profile:
        return
    urls = profile['urls']
    for key in urls:
        if key not in URL_LEAVES:
            print('URL key "{}" invalid for "{}"'.format(key, name))
    if 'journalism' in urls:
        for item in urls['journalism']:
            for key in item:
                if key not in REFERENCE_LEAVES + REFERENCE_LEAVES_DEPR:
                    print('Journalism reference key "{}" invalid for "{}"'.
                          format(key, name))
    if 'misc' in urls:
        for item in urls['misc']:
            for key in item:
                if key not in REFERENCE_LEAVES + REFERENCE_LEAVES_DEPR:
                    print('Misc. reference key "{}" invalid for "{}"'.format(
                        key, name))


def validate_associations(name, profile, unrecognized_targets):
    ASSOCIATION_KEYS = [
        'text', 'textTemplate', 'from', 'to', 'citation', 'citations', 'accessed',
        'timespan', 'preventLift'
    ]

    # The deprecated / unused extensions of the above.
    ASSOCIATION_KEYS_DEPR = ['active', 'note']

    FROM_LEAVES = ['name', 'names', 'interface', 'text', 'weight', 'category', 'numCategoryLifts']

    TO_LEAVES = ['interface', 'text', 'weight', 'category', 'numCategoryLifts']

    if 'associations' not in profile:
        return

    for association in profile['associations']:
        for key, item in association.items():
            if key not in ASSOCIATION_KEYS + ASSOCIATION_KEYS_DEPR:
                print('WARNING: Association key "{}" invalid for "{}"'.format(
                    key, name))
            if key == 'from':
                for from_key in item:
                    if from_key not in FROM_LEAVES:
                        note_error('Association from key "{}" invalid for "{}"'.
                              format(from_key, name))

                if 'names' in item:
                    if 'textTemplate' not in association:
                      note_error('Missing textTemplate for {}, {}'.format(key, name))
                    for nameInfo in item['names']:
                        if isinstance(nameInfo, str):
                            continue
                        if 'text' not in nameInfo:
                            note_error('({}), Missing text field for {} in {}'.format(name, key, nameInfo))
                        target = nameInfo['name'].lower()
                        if target not in entities:
                            if target not in unrecognized_targets:
                                unrecognized_targets[target] = 0
                            unrecognized_targets[target] += 1
                else:
                    if 'textTemplate' in association:
                        note_error('Missing names for {}, {}'.format(key, name))
                    if 'name' not in item:
                        note_error('Missing from name key in "{}"'.format(name))
                    if not item['name']:
                        note_error('Missing name in {}\'s {}'.format(name, item))
                    target = item['name'].lower()
                    if target not in entities:
                        if target not in unrecognized_targets:
                            unrecognized_targets[target] = 0
                        unrecognized_targets[target] += 1
            if key == 'to':
                for to_key in item:
                    if to_key not in TO_LEAVES:
                        note_error('Association to key "{}" invalid for "{}"'.format(
                                to_key, name))
        if 'text' not in association and 'textTemplate' not in association:
            if 'to' not in association or 'text' not in association['to']:
                note_error('Invalid "to" text for "{}"'.format(name))
            if 'from' not in association or 'text' not in association['from']:
                note_error('Invalid "from" text for "{}"'.format(name))


def check_us_federal_agency_alternate(name, alternate):
    if 'dept' not in alternate:
        print('Missing dept key in FPDS agency alts for {}.'.format(name))
    dept = alternate['dept']
    if 'office' in alternate or 'offices' in alternate:
        agency = alternate['agency']
        offices = alternate['offices'] if 'offices' in alternate else [
            alternate['office']
        ]
        for office in offices:
            if dept not in office_data['office_codes']:
                print('Department "{}" not in office_codes.'.format(dept))
                return
            if agency not in office_data['office_codes'][dept]:
                print('Agency "{}" not in office_codes[{}].'.format(
                    agency, dept))
                return
            if office not in office_data['office_codes'][dept][agency]:
                print('Office "{}" not in office_codes[{}][{}].'.format(
                    office, dept, agency))
                return
    elif 'agency' in alternate:
        agency = alternate['agency']
        if dept not in office_data['office_codes']:
            print('Department "{}" not in office_codes.'.format(dept))
            return
        if agency not in office_data['office_codes'][dept]:
            print('Agency "{}" not in office_codes[{}].'.format(agency, dept))
            return
    else:
        if dept not in office_data['office_codes']:
            print('Department "{}" not in office_codes.'.format(dept))
            return


def validate_feeds(name, profile):
    # The types of feed option keys generally available.
    FEED_LEAVES = [
        'agencyNames', 'agencyIncludes', 'names', 'blockName',
        'haveItem', 'includes'
    ]

    AU_FEEDS = ['procurement']
    CA_FEEDS = ['lobbying', 'procurement']
    CA_LOBBYING_FEED_LEAVES = [
        'activity', 'agencyNames', 'agencyIncludes', 'names',
        'blockName', 'haveCommunication', 'haveRegistration', 'includes'
    ]
    EU_FEEDS = ['lobbying', 'procurement']
    UK_FEEDS = ['procurement']
    US_FEEDS = ['az', 'ca', 'central', 'fl', 'il', 'ny', 'tx']
    US_CENTRAL_FEEDS = [
        'cablegate', 'grants', 'laborRelations', 'lobbying', 'opportunities',
        'procurement', 'teixeira'
    ]
    US_CENTRAL_LOBBYING_FEED_KEYS = ['agencyNames', 'fec', 'opr']
    US_CENTRAL_PROCUREMENT_FEED_LEAVES = FEED_LEAVES + ['ueis', 'includeAgenciesFromDescendants']

    if 'feeds' not in profile:
        return

    feeds = profile['feeds']
    if 'au' in feeds:
        au_feeds = feeds['au']
        for key in au_feeds:
            if key not in AU_FEEDS:
                print('Australian key "{}" invalid for "{}"'.format(key, name))
        if 'procurement' in au_feeds:
            for key in au_feeds['procurement']:
                if key not in FEED_LEAVES:
                    print('Australian procurement key "{}" invalid for "{}"'.
                          format(key, name))
    if 'ca' in feeds:
        ca_feeds = feeds['ca']
        for key in ca_feeds:
            if key not in CA_FEEDS:
                print('Canadian key "{}" invalid for "{}"'.format(key, name))
        if 'lobbying' in ca_feeds:
            for key in ca_feeds['lobbying']:
                if key not in CA_LOBBYING_FEED_LEAVES:
                    print('Canadian lobbying key "{}" invalid for "{}"'.format(
                        key, name))
        if 'procurement' in ca_feeds:
            ca_proc = ca_feeds['procurement']
            for key in ca_proc:
                if key not in ['canadaBuys', 'proactive', 'pwsgc']:
                    print('Canadian procurement key "{}" invalid for "{}"'.
                          format(key, name))
                if 'canadaBuys' in ca_proc:
                    for key in ca_proc['canadaBuys']:
                        if key not in FEED_LEAVES:
                            print(
                                'Canada Buys procurement key "{}" invalid for "{}"'
                                .format(key, name))
                if 'proactive' in ca_proc:
                    for key in ca_proc['proactive']:
                        if key not in FEED_LEAVES:
                            print(
                                'Canadian proactive procurement key "{}" invalid for "{}"'
                                .format(key, name))
                if 'pwsgc' in ca_proc:
                    for key in ca_proc['pwsgc']:
                        if key not in FEED_LEAVES:
                            print(
                                'Canadian PWSGC procurement key "{}" invalid for "{}"'
                                .format(key, name))
    if 'uk' in feeds:
        uk_feeds = feeds['uk']
        for key in uk_feeds:
            if key not in UK_FEEDS:
                print('UK key "{}" invalid for "{}"'.format(key, name))
        if 'procurement' in uk_feeds:
            for key in uk_feeds['procurement']:
                if key not in FEED_LEAVES:
                    print('UK procurement key "{}" invalid for "{}"'.format(
                        key, name))
    if 'eu' in feeds:
        eu_feeds = feeds['eu']
        for key, feed in eu_feeds.items():
            for sub_key in feed:
                if sub_key not in EU_FEEDS:
                    print('EU {} key "{}" invalid for "{}"'.format(
                        key.upper(), sub_key, name))
            if 'procurement' in feed:
                for sub_key in feed['procurement']:
                    if sub_key not in FEED_LEAVES:
                        print('EU {} procurement key "{}" invalid for "{}"'.
                              format(key.upper(), sub_key, name))
    if 'us' in feeds:
        us_feeds = feeds['us']
        for key in us_feeds:
            if key not in US_FEEDS:
                print('US key "{}" invalid for "{}"'.format(key, name))
        if 'central' in us_feeds:
            us_central_feeds = us_feeds['central']
            for key in us_central_feeds:
                if key not in US_CENTRAL_FEEDS:
                    print('US central key "{}" invalid for "{}"'.format(
                        key, name))
            if 'laborRelations' in us_central_feeds:
                for key in us_central_feeds['laborRelations']:
                    if key not in FEED_LEAVES:
                        print('US central labor key "{}" invalid for "{}"'.
                              format(key, name))
            if 'lobbying' in us_central_feeds:
                for key in us_central_feeds['lobbying']:
                    if key not in US_CENTRAL_LOBBYING_FEED_KEYS:
                        print('US central lobbying key "{}" invalid for "{}"'.
                              format(key, name))
            if 'procurement' in us_central_feeds:
                for key in us_central_feeds['procurement']:
                    if key not in US_CENTRAL_PROCUREMENT_FEED_LEAVES:
                        print(
                            'US central procurement key "{}" invalid for "{}"'.
                            format(key, name))
                    if key == 'agencyNames' or key == 'agencyIncludes':
                        agencyNames = us_central_feeds['procurement'][key]
                        for alternate in agencyNames:
                            check_us_federal_agency_alternate(name, alternate)


def validate_parents(name, profile):
    if 'parents' not in profile:
        return
    for parent in profile['parents']:
        if parent == name:
            print('Infinite parent recursion for {}'.format(name))


num_entities = 0
num_entities_with_urls = 0
num_entities_with_logos = 0
num_entities_with_associations = 0
num_associations = 0
unrecognized_targets = {}
for name, profile in entities.items():
    validate_urls(name, profile)
    try:
        validate_associations(name, profile, unrecognized_targets)
    except Exception as e:
        print('Could not validate associations for {}'.format(name))
        print(e)

    validate_feeds(name, profile)
    validate_parents(name, profile)

    num_entities += 1
    if 'associations' in profile:
        num_entities_with_associations += 1
        num_associations += len(profile['associations'])
    if 'urls' in profile:
        num_entities_with_urls += 1
        if 'logo' in profile['urls']:
            num_entities_with_logos += 1
print()

unrecognized_targets = {
    k: v
    for k, v in sorted(
        unrecognized_targets.items(), key=lambda item: item[1], reverse=True)
}
max_unrecognized_warnings = 10
num_printed = 0
for target, target_count in unrecognized_targets.items():
    if num_printed >= max_unrecognized_warnings:
        break
    print('Target {} was unrecognized {} times.'.format(target, target_count))
    num_printed += 1
print()

print('Num validated entities: {}'.format(num_entities))
print('Num validated entities w/ URLs: {}'.format(num_entities_with_urls))
print('Num validated entities w/ logos: {}'.format(num_entities_with_logos))
print('Num validated entities w/ associations: {}'.format(
    num_entities_with_associations))
print('Num associations: {}'.format(num_associations))
if len(errors):
    print(errors)
else:
    print('No errors logged.')
