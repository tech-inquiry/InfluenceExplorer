#
# A script to validate the data/citations.json file.
#
import json

# First ensure that the JSON parses.
citations = json.load(open('data/citations.json'))

entities = json.load(open('data/entities.json'))

num_citations = 0
num_entity_tags = 0

num_citations = {'total': 0}

tags = {
  'book': ['entities'],
  'article': ['investigatedEntities', 'miscEntities'],
  'image': ['investigatedEntities', 'miscEntities'],
  'journal': ['investigatedEntities', 'miscEntities'],
  'report': ['investigatedEntities', 'miscEntities'],
  'video': ['investigatedEntities', 'miscEntities'],
  'webpage': ['investigatedEntities', 'miscEntities'],
  'citation': ['investigatedEntities', 'miscEntities']
}
for tag in tags:
    if tag not in num_citations:
        num_citations[tag] = 0
    url_set = set()
    for url, item in citations[tag].items():
        if url in url_set:
          print('ERROR: Duplicate URL {} for {}'.format(url, tag))
        url_set.add(url)        

        num_citations['total'] += 1
        num_citations[tag] += 1
        have_entities = False
        for entity_type in tags[tag]:
            if not entity_type in item:
                continue
            have_entities = True
            for entity in item[entity_type]:
                num_entity_tags += 1
                if type(entity) is str:
                  name = entity
                else:
                    name = entity['name']
                if name not in entities:
                    print('"{}" was not found in entities.'.format(name))
        if not have_entities:
            print('No entities for URL {}'.format(url))
    print('Num {} citations: {}'.format(tag, num_citations[tag]))

print('Num total citations: {}'.format(num_citations['total']))
print('Num entity tags: {}'.format(num_entity_tags))
