#!/usr/bin/env bash
# convenience script to use locally installed pm2 binary for arbitrary pm2 commands, e.g.
# /runpm2.sh stop all
# /runpm2.sh logs
# /runpm2.sh ls


./node_modules/.bin/pm2 "$@"




# pm2 resources:
# blog.appsignal.com/2022/03/09/a-complete-guide-to-nodejs-process-management-with-pm2.html
# production on DO: https://www.digitalocean.com/community/tutorials/how-to-use-pm2-to-setup-a-node-js-production-environment-on-an-ubuntu-vps
# https://sandeep.dev/how-to-achieve-truly-zero-downtime-deployment-using-pm2
# https://nodejs.org/docs/latest/api/fs.html#caveats
# https://github.com/Unitech/pm2/issues/3503 https://github.com/Unitech/pm2/issues/3503#issuecomment-507008202


# //TODO(lt): what are 8 cores used for?