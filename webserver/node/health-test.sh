#!/usr/bin/env bash

# send a request every second to test server health over a duration (e.g., rolling restart)
i=1
ok_resp="OK"
next=$(curl -s --fail-with-body http://127.0.0.1:3000/api/healthz)
while [ $ok_resp == $next ]
do
  sleep 1
  echo $i
  i=$((i+1))
  next=$(curl -s --fail-with-body http://127.0.0.1:3000/api/healthz)
done

echo "Failed. $i seconds up."
