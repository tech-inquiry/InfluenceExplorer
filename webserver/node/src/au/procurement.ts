import { dataSource } from '../App'
import { FeedInfo } from '../EntityState'
import { AUProcurement } from '../entity/au/procurement'
import * as tags from '../tags'
import * as util from '../util'

const kTable = AUProcurement
const kTableName = 'au_contract_filing'

const kTagDataset = 'au_procurement'

const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(filing_id)',
  'lower(publisher::text)',
  'lower(parties::text)',
  'lower(award::text)',
  'lower(contract::text)',
  'lower(tender::text)',
])

function getAgencyNames(
  name: string,
  entities: Map<string, Map<string, any>>
): string[] {
  let names = []
  if (!entities.has(name)) {
    return names
  }
  const profile = entities.get(name)
  const alternates = util.jsonGetFromPath(profile, [
    'feeds',
    'au',
    'procurement',
    'agencyNames',
  ])
  if (alternates != undefined) {
    names = alternates
  }
  const includes = util.jsonGetFromPath(profile, [
    'feeds',
    'au',
    'procurement',
    'agencyIncludes',
  ])
  if (includes != undefined) {
    names = names.concat(includes)
  }
  return names
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`date >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`date <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('date >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('date <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

// Set up the list of AU contract supplier names for the vendor. If
// subsidiaries were requested to be included, the same process is repeated for
// each descendant.
function expandNames(
  entityList: string[],
  entityAlternates: Map<string, string[]>,
  entities: Map<string, Map<string, any>>
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'au',
    'procurement',
  ])
}

function getPartyABN(party: any): string {
  let abn = undefined
  if (party.hasOwnProperty('additionalIdentifiers')) {
    for (const identifier of party['additionalIdentifiers']) {
      if (identifier['scheme'] == 'AU-ABN') {
        abn = identifier['id'].toLowerCase()
      }
    }
  }
  return abn
}

function normalizeVendorWithABN(
  name: string,
  abn: string,
  normalization: Map<string, string>
): string {
  name = util.canonicalText(name)
  let keys = []
  if (name && abn) {
    keys.push(JSON.stringify({ name: name, abn: abn }))
  }
  if (abn) {
    keys.push(JSON.stringify({ abn: abn }))
  }
  if (name) {
    keys.push(JSON.stringify({ name: name }))
  }
  for (const key of keys) {
    if (normalization.has(key)) {
      return normalization.get(key)
    }
  }
  return name
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateFiling(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  agencyNormalization: Map<string, string>
) {
  let vendors = []
  if ('parties' in filing) {
    const parties = filing['parties']
    for (const party of parties) {
      if (party['name']) {
        if (party['role'] == 'supplier') {
          const vendor = party['name']
          const abn = getPartyABN(party)
          const normalizedVendor = normalizeVendorWithABN(
            vendor,
            abn,
            entityNormalization
          )
          vendors.push({
            origText: vendor,
            text: normalizedVendor,
            stylizedText: util.getFullyStylizedName(normalizedVendor, entities),
            url: util.encodeEntity(normalizedVendor),
            logo: util.logoURLFromAncestors(normalizedVendor, entities),
          })
        } else {
          const agency = party['name']
          const normalizedAgency = util.normalizeEntity(
            agency,
            agencyNormalization
          )
          // TODO(Jack Poulson): Rename 'vendors' to 'entities' and then split
          // it into vendors and agencies.
          vendors.push({
            origText: agency,
            text: normalizedAgency,
            stylizedText: util.getFullyStylizedName(normalizedAgency, entities),
            url: util.encodeEntity(normalizedAgency),
            logo: util.logoURLFromAncestors(normalizedAgency, entities),
          })
        }
      } else {
        vendors.push({})
      }
    }
  }

  filing['annotation'] = { vendors: vendors }
}

function annotate(
  result,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  agencyNormalization: Map<string, string>
) {
  for (let filing of result.filings) {
    annotateFiling(filing, entities, entityNormalization, agencyNormalization)
  }
}

function compressNames(names: string[]): string[] {
  let nameSet = new Set<string>()
  for (const name of names) {
    nameSet.add(name)
  }
  return Array.from(nameSet.values())
}

async function haveForVendors(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  maxTargetsPerQuery = 20
): Promise<boolean> {
  const feedPath = ['feeds', 'au', 'procurement']
  if (util.canAssumeRecordsExist(names, dateRange, entities, feedPath)) {
    return true
  }

  names = compressNames(names)

  let jsonTargets = []
  for (const name of names) {
    jsonTargets.push(JSON.stringify([{ name: name, role: 'supplier' }]))
  }

  let manager = dataSource.manager

  for (
    let sliceBeg = 0;
    sliceBeg < jsonTargets.length;
    sliceBeg += maxTargetsPerQuery
  ) {
    const sliceEnd = Math.min(sliceBeg + maxTargetsPerQuery, jsonTargets.length)
    const subTargets = jsonTargets.slice(sliceBeg, sliceEnd)

    let params = []
    const anyMatch = `ANY(cast($${params.length + 1} as jsonb[]))`
    params.push(subTargets)

    const searchConstraint = `LOWER(parties::text)::jsonb @> ${anyMatch}`
    const dateConstraint = buildDateConstraint(dateRange, params)
    const constraintsStr = [searchConstraint, dateConstraint]
      .filter((e) => e)
      .join(' AND ')

    // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
    // selection oddities.
    const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 10000`

    const haveResult = await util.nonemptyQuery(manager, queryStr, params)
    if (haveResult) {
      return true
    }
  }
  return false
}

async function haveForAgencies(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  let params = []

  let jsonTargets = []
  for (const name of names) {
    // Note that we need to lowercase 'procuringEntity'.
    jsonTargets.push(JSON.stringify([{ name: name, role: 'procuringentity' }]))
  }
  const anyMatch = `ANY(cast($${params.length + 1} as jsonb[]))`
  params.push(jsonTargets)

  const searchConstraint = `LOWER(parties::text)::jsonb @> ${anyMatch}`
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForVendor(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await haveForVendors(expandedNames, dateRange, entities)
}

async function haveForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (
    util.canAssumeAgencyRecordsExistForName(name, dateRange, entities, [
      'feeds',
      'au',
      'procurement',
    ])
  ) {
    return true
  }
  const names = getAgencyNames(name, entities)
  return await haveForAgencies(names, dateRange, entities)
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const searchConstraint =
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getForVendors(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  names = compressNames(names)

  let anyMatches = []
  for (let name of names) {
    const json = JSON.stringify([{ name: name, role: 'supplier' }])
    const singleQuoteEscapedJSON = json.replace(/'/g, "''")
    anyMatches.push(`'${singleQuoteEscapedJSON}'::jsonb`)
  }
  const anyMatch = `ANY(ARRAY[${anyMatches.join(', ')}])`

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(`LOWER(parties::text)::jsonb @> ${anyMatch}`)

  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForAgencies(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let anyMatches = []
  for (let name of names) {
    // NOTE that we need to lowercase 'procuringEntity'.
    const json = JSON.stringify([{ name: name, role: 'procuringentity' }])
    const singleQuoteEscapedJSON = json.replace(/'/g, "''")
    anyMatches.push(`'${singleQuoteEscapedJSON}'::jsonb`)
  }
  const anyMatch = `ANY(ARRAY[${anyMatches.join(', ')}])`

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(`LOWER(parties::text)::jsonb @> ${anyMatch}`)

  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForVendor(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await getForVendors(
    expandedNames,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function getForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = getAgencyNames(name, entities)
  return await getForAgencies(names, dateRange, maxSearchResults, searchOffset)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })

  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['filing_id']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  // Build the query string.
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Boolean AU contracts w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have AU contracts w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean AU contracts w/ entity: ${name}`)
    result = await haveForVendor(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`Boolean AU contracts w/ agency: ${name}`)
    result = await haveForAgency(name, dateRange, entities)
    // DEBUG
    console.log(`Returning ${result} for AU contracts w/ agency: ${name}`)
  } else {
    console.log('Boolean AU contract API')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`AU contracts w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`AU contracts w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`AU contracts w/ entity: ${name}`)
    result = await getForVendor(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names,
      maxSearchResults,
      searchOffset
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`AU contracts w/ agency: ${name}`)
    result = await getForAgency(
      name,
      dateRange,
      entities,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('AU contract API')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(
      result,
      entities,
      feedInfo.normalization,
      feedInfo.agencyNormalization
    )
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/au/procurement'

  const feed = entityState.feeds.get('au').get('procurement')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether there are any AU contract filings for a web search or an
  // entity.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, feed)
  })

  // Returns JSON for AU contract filings for a web search or an entity.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(request, response, entities, children, feed)
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
