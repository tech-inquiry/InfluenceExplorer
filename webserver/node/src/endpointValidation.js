const Joi = require('joi')

module.exports = {
  postEntitySubmissionSchema: Joi.object({
    fullName: Joi.string().min(1).max(256).required(),
    shortName: Joi.string().min(1).max(128),
    institutionalUrl: Joi.string().uri().max(2000).required(),
    logoUrl: Joi.string().uri().max(2000),
    submitterEmail: Joi.string().email().max(500),
  }),
}
