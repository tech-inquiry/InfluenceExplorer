import { dataSource, getUsernames } from '../../../App'
import { FeedInfo } from '../../../EntityState'
import { USCALobbyingLobbyDiscCover } from '../../../entity/us/ca/lobbying/lobby_disc_cover'
import { USCALobbyingLobbyDiscPayment } from '../../../entity/us/ca/lobbying/lobby_disc_payment'
import * as calaccessUtil from './util'
import * as tags from '../../../tags'
import * as util from '../../../util'

const kCoverTable = USCALobbyingLobbyDiscCover
const kCoverTableName = 'us_calaccess_lobby_disc_cover'

const kPaymentTable = USCALobbyingLobbyDiscPayment
const kPaymentTableName = 'us_calaccess_lobby_disc_payment'

const kTagDataset = 'us_calaccess_activity'

const coverFullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(filing_id)',
  'lower(cover::text)',
  'lower(unmatched_memos::text)',
])

const paymentFullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(payment_table.filing_id)',
  'lower(payment_table.transaction_id)',
  'lower(payment_table.payment::text)',
])

function expandNames(
  entityList: string[],
  entityAlternates,
  entities
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'us',
    'ca',
    'lobbying',
    'activity',
  ])
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`(cover->>'period_end')::DATE >= $${params.length + 1}`)
    params.push(dateRange.start)
  }
  if (dateRange.hasEnd()) {
    constraints.push(`(cover->>'period_begin')::DATE <= $${params.length + 1}`)
    params.push(dateRange.end)
  }
  return constraints.join(' AND ')
}

function addBillLinks(text: string, year: number) {
  const legInfoBaseURL =
    'https://leginfo.legislature.ca.gov/faces/billNavClient.xhtml'

  const session = year % 2 ? `${year}-${year + 1}` : `${year - 1}-${year}`
  const sessionURLComp = year % 2 ? `${year}${year + 1}` : `${year - 1}${year}`

  function legInfoURL(billType: string, billNumber: string): string {
    return `${legInfoBaseURL}?bill_id=${sessionURLComp}0${billType}${billNumber}`
  }

  function assemblyBillURL(billNumber: string): string {
    return legInfoURL('AB', billNumber)
  }

  function assemblyConcurrentResolutionURL(billNumber: string): string {
    return legInfoURL('ACR', billNumber)
  }

  function senateBillURL(billNumber: string): string {
    return legInfoURL('SB', billNumber)
  }

  function senateConcurrentResolutionURL(billNumber: string): string {
    return legInfoURL('SCR', billNumber)
  }

  const endsWithComma = /,$/
  const endsWithCommaSpace = /,\s+$/
  const endsWithSpace = /\s+$/

  let assemblyBills = []
  const assemblyRegex = /ab ([\d+\s*,*]+)/gi
  function assemblyRegexReplace(match: string, billNumbersStr: string): string {
    let urls = []
    const billNumbers = billNumbersStr.split(/[, ]+/)
    for (let billNumber of billNumbers) {
      billNumber = billNumber.trim()
      if (!billNumber) {
        continue
      }
      const url = assemblyBillURL(billNumber)
      const label = `AB ${billNumber}`
      assemblyBills.push({ url: url, label: label })
      urls.push(`<a href=${url}>${label}</a>`)
    }
    let urlText = urls.join(', ')

    if (endsWithComma.test(billNumbersStr)) {
      urlText += ','
    } else if (endsWithCommaSpace.test(billNumbersStr)) {
      urlText += ', '
    } else if (endsWithSpace.test(billNumbersStr)) {
      urlText += ' '
    }

    return urlText
  }

  let assemblyConcurrentResolutions = []
  const assemblyConcurrentRegex = /acr ([\d+\s*,*]+)/gi
  function assemblyConcurrentRegexReplace(
    match: string,
    billNumbersStr: string
  ): string {
    let urls = []
    const billNumbers = billNumbersStr.split(/[, ]+/)
    for (let billNumber of billNumbers) {
      billNumber = billNumber.trim()
      if (!billNumber) {
        continue
      }
      const url = assemblyConcurrentResolutionURL(billNumber)
      const label = `ACR ${billNumber}`
      assemblyConcurrentResolutions.push({ url: url, label: label })
      urls.push(`<a href=${url}>${label}</a>`)
    }
    let urlText = urls.join(', ')

    if (endsWithComma.test(billNumbersStr)) {
      urlText += ','
    } else if (endsWithCommaSpace.test(billNumbersStr)) {
      urlText += ', '
    } else if (endsWithSpace.test(billNumbersStr)) {
      urlText += ' '
    }

    return urlText
  }

  let senateBills = []
  const senateRegex = /sb ([\d+\s*,*]+)/gi
  function senateRegexReplace(match: string, billNumbersStr: string): string {
    let urls = []
    const billNumbers = billNumbersStr.split(/[, ]+/)
    for (let billNumber of billNumbers) {
      billNumber = billNumber.trim()
      if (!billNumber) {
        continue
      }
      const url = senateBillURL(billNumber)
      const label = `SB ${billNumber}`
      senateBills.push({ url: url, label: label })
      urls.push(`<a href=${url}>${label}</a>`)
    }
    let urlText = urls.join(', ')

    if (endsWithComma.test(billNumbersStr)) {
      urlText += ','
    } else if (endsWithCommaSpace.test(billNumbersStr)) {
      urlText += ', '
    } else if (endsWithSpace.test(billNumbersStr)) {
      urlText += ' '
    }

    return urlText
  }

  let senateConcurrentResolutions = []
  const senateConcurrentRegex = /scr ([\d+\s*,*]+)/gi
  function senateConcurrentRegexReplace(
    match: string,
    billNumbersStr: string
  ): string {
    let urls = []
    const billNumbers = billNumbersStr.split(/[, ]+/)
    for (let billNumber of billNumbers) {
      billNumber = billNumber.trim()
      if (!billNumber) {
        continue
      }
      const url = senateConcurrentResolutionURL(billNumber)
      const label = `SCR ${billNumber}`
      senateConcurrentResolutions.push({ url: url, label: label })
      urls.push(`<a href=${url}>${label}</a>`)
    }
    let urlText = urls.join(', ')

    if (endsWithComma.test(billNumbersStr)) {
      urlText += ','
    } else if (endsWithCommaSpace.test(billNumbersStr)) {
      urlText += ', '
    } else if (endsWithSpace.test(billNumbersStr)) {
      urlText += ' '
    }

    return urlText
  }

  const richText = text
    .replace(assemblyRegex, assemblyRegexReplace)
    .replace(assemblyConcurrentRegex, assemblyConcurrentRegexReplace)
    .replace(senateRegex, senateRegexReplace)
    .replace(senateConcurrentRegex, senateConcurrentRegexReplace)

  return {
    richText: richText,
    session: session,
    assemblyBills: assemblyBills,
    assemblyConcurrentResolutions: assemblyConcurrentResolutions,
    senateBills: senateBills,
    senateConcurrentResolutions: senateConcurrentResolutions,
  }
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const limit = 10000

  const coverSearchConstraint =
    coverFullTextSearchString +
    `  @@ websearch_to_tsquery('simple', $${params.length + 1})`
  const paymentSearchConstraint =
    paymentFullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)

  const dateConstraint = buildDateConstraint(dateRange, params)

  const coverConstraintsStr = [coverSearchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')
  const paymentConstraintsStr = [paymentSearchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  const queryStr = `
    (
     SELECT 1 FROM (
      SELECT * FROM ${kCoverTableName}
      WHERE ${coverConstraintsStr} LIMIT ${limit}
     ) cover_table INNER JOIN ${kPaymentTableName} payment_table
     ON cover_table.filing_id = payment_table.filing_id
     LIMIT ${limit}
    ) UNION (
     SELECT 1 FROM ${kCoverTableName} cover_table
     INNER JOIN ${kPaymentTableName} payment_table
     ON cover_table.filing_id = payment_table.filing_id
     WHERE ${paymentConstraintsStr} LIMIT ${limit}
    )`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

function getYearFromFiling(filing): number {
  let year = -1
  if (filing.cover && filing.cover['period_begin']) {
    const periodBegin = new Date(filing.cover['period_begin'])
    year = periodBegin.getFullYear()
  }
  return year
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateFiling(filing, entities, feedInfo: FeedInfo): void {
  let annotation = {}

  // Normalize the entity names.
  let entityAnnotations = {}
  for (const entity of filing.entities) {
    const name = util.normalizeEntity(entity, feedInfo.normalization)
    entityAnnotations[entity] = {
      origText: entity,
      text: name,
      stylizedText: util.getFullyStylizedName(name, entities),
      logo: util.logoURLFromAncestors(name, entities),
    }
  }
  annotation['entities'] = entityAnnotations

  let assemblyBills = []
  let assemblyConcurrentResolutions = []
  let senateBills = []
  let senateConcurrentResolutions = []

  const year = getYearFromFiling(filing)

  let coverAnnotation = {}
  if (filing.cover['activity_description']) {
    const text = filing.cover['activity_description']
    const billInfo = addBillLinks(text, year)
    coverAnnotation['activityDescription'] = billInfo
    for (const bill of billInfo['assemblyBills']) {
      assemblyBills.push(bill)
    }
    for (const bill of billInfo['assemblyConcurrentResolutions']) {
      assemblyConcurrentResolutions.push(bill)
    }
    for (const bill of billInfo['senateBills']) {
      senateBills.push(bill)
    }
    for (const bill of billInfo['senateConcurrentResolutions']) {
      senateConcurrentResolutions.push(bill)
    }
  }
  if (filing.cover['memo'] && filing.cover['memo']['text']) {
    const text = filing.cover['memo']['text']
    const billInfo = addBillLinks(text, year)
    coverAnnotation['memo'] = { text: billInfo }
    for (const bill of billInfo['assemblyBills']) {
      assemblyBills.push(bill)
    }
    for (const bill of billInfo['assemblyConcurrentResolutions']) {
      assemblyConcurrentResolutions.push(bill)
    }
    for (const bill of billInfo['senateBills']) {
      senateBills.push(bill)
    }
    for (const bill of billInfo['senateConcurrentResolutions']) {
      senateConcurrentResolutions.push(bill)
    }
  }
  annotation['cover'] = coverAnnotation

  let paymentAnnotation = {}
  if (filing.payment['activity_description']) {
    const text = filing.payment['activity_description']
    const billInfo = addBillLinks(text, year)
    paymentAnnotation['activityDescription'] = billInfo
    for (const bill of billInfo['assemblyBills']) {
      assemblyBills.push(bill)
    }
    for (const bill of billInfo['assemblyConcurrentResolutions']) {
      assemblyConcurrentResolutions.push(bill)
    }
    for (const bill of billInfo['senateBills']) {
      senateBills.push(bill)
    }
    for (const bill of billInfo['senateConcurrentResolutions']) {
      senateConcurrentResolutions.push(bill)
    }
  }
  if (filing.payment['memo'] && filing.payment['memo']['text']) {
    const text = filing.payment['memo']['text']
    const billInfo = addBillLinks(text, year)
    paymentAnnotation['memo'] = { text: billInfo }
    for (const bill of billInfo['assemblyBills']) {
      assemblyBills.push(bill)
    }
    for (const bill of billInfo['assemblyConcurrentResolutions']) {
      assemblyConcurrentResolutions.push(bill)
    }
    for (const bill of billInfo['senateBills']) {
      senateBills.push(bill)
    }
    for (const bill of billInfo['senateConcurrentResolutions']) {
      senateConcurrentResolutions.push(bill)
    }
  }
  annotation['payment'] = paymentAnnotation

  let memoAnnotations = []
  for (const memo of filing.unmatched_memos) {
    let memoAnnotation = {}
    if (memo['text']) {
      const text = memo['text']
      const billInfo = addBillLinks(text, year)
      memoAnnotation['text'] = billInfo
      for (const bill of billInfo['assemblyBills']) {
        assemblyBills.push(bill)
      }
      for (const bill of billInfo['assemblyConcurrentResolutions']) {
        assemblyConcurrentResolutions.push(bill)
      }
      for (const bill of billInfo['senateBills']) {
        senateBills.push(bill)
      }
      for (const bill of billInfo['senateConcurrentResolutions']) {
        senateConcurrentResolutions.push(bill)
      }
    }
    memoAnnotations.push(memoAnnotation)
  }
  annotation['unmatchedMemos'] = memoAnnotations

  annotation['bills'] = {
    assembly: assemblyBills,
    assemblyConcurrent: assemblyConcurrentResolutions,
    senate: senateBills,
    senateConcurrent: senateConcurrentResolutions,
  }

  filing['annotation'] = annotation
}

function annotate(results, entities, feedInfo: FeedInfo): void {
  let maxAmount: number = 0
  let totalAmount: number = 0
  for (const filing of results.filings) {
    annotateFiling(filing, entities, feedInfo)
    const amount =
      filing['payment']['amount'] && 'fees' in filing['payment']['amount']
        ? Number(filing['payment']['amount']['fees'])
        : 0

    maxAmount = Math.max(maxAmount, amount)
    totalAmount += amount
  }
  results['amountSummaries'] = { max: maxAmount, total: totalAmount }
}

async function haveForNames(
  names: string[],
  dateRange: util.DateRange,
  entities,
  maxTargetsPerQuery = 20
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(names, dateRange, entities, [
      'feeds',
      'us',
      'ca',
      'lobbying',
      'activity',
    ])
  ) {
    return true
  }

  const limit = 10000

  const nameTargets = calaccessUtil.nameTargetsFromNames(names)

  let manager = dataSource.manager
  for (
    let sliceBeg = 0;
    sliceBeg < nameTargets.length;
    sliceBeg += maxTargetsPerQuery
  ) {
    const sliceEnd = Math.min(sliceBeg + maxTargetsPerQuery, nameTargets.length)
    const subTargets = nameTargets.slice(sliceBeg, sliceEnd)

    let params = []
    const coverSearchConstraint = `entities @> ANY(cast($${
      params.length + 1
    } as jsonb[]))`
    const paymentSearchConstraint = `payment_table.entities @> ANY(cast($${
      params.length + 1
    } as jsonb[]))`
    params.push(subTargets)

    const dateConstraint = buildDateConstraint(dateRange, params)

    const coverConstraintsStr = [coverSearchConstraint, dateConstraint]
      .filter((e) => e)
      .join(' AND ')
    const paymentConstraintsStr = [paymentSearchConstraint, dateConstraint]
      .filter((e) => e)
      .join(' AND ')

    const queryStr = `
      (
       SELECT 1 FROM (
        SELECT * FROM ${kCoverTableName}
        WHERE ${coverConstraintsStr} LIMIT ${limit}
       ) cover_table INNER JOIN ${kPaymentTableName} payment_table
       ON cover_table.filing_id = payment_table.filing_id
       LIMIT ${limit}
      ) UNION (
       SELECT 1 FROM ${kCoverTableName} cover_table
       INNER JOIN ${kPaymentTableName} payment_table
       ON cover_table.filing_id = payment_table.filing_id
       WHERE ${paymentConstraintsStr} LIMIT ${limit}
      )`

    const haveResult = await util.nonemptyQuery(manager, queryStr, params)
    if (haveResult) {
      return true
    }
  }
  return false
}

async function haveForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities,
  entityChildren,
  feedInfo: FeedInfo
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, feedInfo.names, entities)
  return await haveForNames(expandedNames, dateRange, entities)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const coverSearchConstraint =
    coverFullTextSearchString +
    `  @@ websearch_to_tsquery('simple', $${params.length + 1})`
  const paymentSearchConstraint =
    paymentFullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)

  const dateConstraint = buildDateConstraint(dateRange, params)

  const coverConstraintsStr = [coverSearchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')
  const paymentConstraintsStr = [paymentSearchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  let queryStr = `
    SELECT * FROM (
     (
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       payment_table.payment,
       payment_table.transaction_id,
       payment_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || payment_table.entities) as entities
      FROM (
       SELECT * FROM ${kCoverTableName}
       WHERE ${coverConstraintsStr}
       ORDER BY (cover->>'date_filed')::DATE DESC
       LIMIT $${params.length + 1}
      ) cover_table
      INNER JOIN ${kPaymentTableName} payment_table
      ON cover_table.filing_id = payment_table.filing_id
      ORDER BY (cover->>'date_filed')::DATE DESC
      LIMIT $${params.length + 1}
     ) UNION (
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       payment_table.payment,
       payment_table.transaction_id,
       payment_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || payment_table.entities) as entities
      FROM ${kCoverTableName} cover_table
      INNER JOIN ${kPaymentTableName} payment_table
      ON cover_table.filing_id = payment_table.filing_id
      WHERE ${paymentConstraintsStr}
      ORDER BY (cover->>'date_filed')::DATE DESC
      LIMIT $${params.length + 1}
     )
    ) q ORDER BY (cover->>'date_filed')::DATE DESC
    LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const usernames = getUsernames(token)

  let params = []

  let queryStr = `
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       payment_table.payment,
       payment_table.transaction_id,
       payment_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || payment_table.entities) as entities
      FROM ${kCoverTableName} cover_table
      INNER JOIN ${kPaymentTableName} payment_table
       ON cover_table.filing_id = payment_table.filing_id
      WHERE ROW(payment_table.filing_id, payment_table.transaction_id, payment_table.line_item) IN (
        SELECT
          unique_data->>'filing_id',
          unique_data->>'transaction_id',
          unique_data->>'line_item' FROM tags
          WHERE dataset = $${params.length + 1}
            AND username = ANY($${params.length + 2})
            AND LOWER(tag) @@ websearch_to_tsquery($${params.length + 3})
      ) ORDER BY (cover->>'date_filed')::DATE DESC LIMIT $${params.length + 4}`
  params.push(kTagDataset)
  params.push(usernames)
  params.push(webSearch)
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  const queryStr = `
    SELECT 1 FROM ${kCoverTableName} cover_table
    INNER JOIN ${kPaymentTableName} payment_table
     ON cover_table.filing_id = payment_table.filing_id
    ${whereStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  let queryStr = `
    SELECT
     cover_table.filing_id,
     cover_table.cover,
     payment_table.payment,
     payment_table.transaction_id,
     payment_table.line_item,
     cover_table.unmatched_memos,
     (cover_table.entities || payment_table.entities) as entities
    FROM ${kCoverTableName} cover_table
    INNER JOIN ${kPaymentTableName} payment_table
     ON cover_table.filing_id = payment_table.filing_id
    ${whereStr}
    ORDER BY (cover->>'date_filed')::DATE DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getForNames(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const nameTargets = calaccessUtil.nameTargetsFromNames(names)

  const coverSearchConstraint = `entities @> ANY(cast($${
    params.length + 1
  } as jsonb[]))`
  const paymentSearchConstraint = `payment_table.entities @> ANY(cast($${
    params.length + 1
  } as jsonb[]))`
  params.push(nameTargets)

  const dateConstraint = buildDateConstraint(dateRange, params)

  const coverConstraintsStr = [coverSearchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')
  const paymentConstraintsStr = [paymentSearchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  let queryStr = `
    SELECT * FROM (
     (
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       payment_table.payment,
       payment_table.transaction_id,
       payment_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || payment_table.entities) as entities
      FROM (
       SELECT * FROM ${kCoverTableName}
       WHERE ${coverConstraintsStr}
       ORDER BY (cover->>'date_filed')::DATE DESC LIMIT $${params.length + 1}
      ) AS cover_table
      INNER JOIN ${kPaymentTableName} payment_table
      ON cover_table.filing_id = payment_table.filing_id
      ORDER BY (cover->>'date_filed')::DATE DESC LIMIT $${params.length + 1}
     ) UNION (
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       payment_table.payment,
       payment_table.transaction_id,
       payment_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || payment_table.entities) as entities
      FROM ${kCoverTableName} cover_table
      INNER JOIN ${kPaymentTableName} payment_table
       ON cover_table.filing_id = payment_table.filing_id
      WHERE ${paymentConstraintsStr}
      ORDER BY (cover->>'date_filed')::DATE DESC LIMIT $${params.length + 1}
     )
    ) q ORDER BY (cover->>'date_filed')::DATE DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities,
  entityChildren,
  feedInfo: FeedInfo,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, feedInfo.names, entities)
  return await getForNames(
    expandedNames,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function haveAPI(
  request,
  response,
  entities,
  entityChildren,
  feedInfo: FeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Boolean CALACCESS lobbying activity w/ text: ${text}`)
    const minDigitLength = 5
    if (util.isShortDigitSequence(text, minDigitLength)) {
      console.log(
        `Avoiding out-of-memory error from short digit sequence: ${text}`
      )
    } else {
      result = await haveFromSearch(text, dateRange)
    }
  } else if (tagText) {
    console.log(`CALACCESS lobbying activity w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean CALACCESS lobbying activity w/ entity: ${name}`)
    result = await haveForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo
    )
  } else {
    console.log('Boolean CALACCESS lobbying activity over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities,
  entityChildren,
  feedInfo: FeedInfo
) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`CALACCESS lobbying activity w/ text: ${text}`)
    const minDigitLength = 5
    if (util.isShortDigitSequence(text, minDigitLength)) {
      console.log(
        `Avoiding out-of-memory error from short digit sequence: ${text}`
      )
    } else {
      result = await getFromSearch(
        text,
        dateRange,
        maxSearchResults,
        searchOffset
      )
    }
  } else if (tagText) {
    console.log(`CALACCESS lobbying activity w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`CALACCESS lobbying activity w/ entity: ${name}`)
    result = await getForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('CALACCESS lobbying activity over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, feedInfo)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/us/ca/lobbying/activity'

  const feed = entityState.feeds
    .get('us')
    .get('ca')
    .get('lobbying')
    .get('activity')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether there are results for a web search or entity.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, feed)
  })

  // Returns JSON for a web search or entity.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(request, response, entities, children, feed)
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
