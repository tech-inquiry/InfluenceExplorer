import * as util from '../../../util'

export function escapeString(value: string): string {
  return value.replace(/"/g, '\\"')
}

export function nameTargetsFromNames(names: string[]): string[] {
  let targets = []
  if (names) {
    for (const name of names) {
      targets.push(`["${escapeString(name)}"]`)
    }
  }
  return targets
}
