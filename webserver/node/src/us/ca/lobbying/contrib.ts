import { dataSource, getUsernames } from '../../../App'
import { FeedInfo } from '../../../EntityState'
import { USCALobbyingCampaignDiscCover } from '../../../entity/us/ca/lobbying/campaign_disc_cover'
import { USCALobbyingCampaignContrib } from '../../../entity/us/ca/lobbying/campaign_contrib'
import { USCALobbyingPreElectExpend } from '../../../entity/us/ca/lobbying/pre_elect_expend'
import * as calaccessUtil from './util'
import * as tags from '../../../tags'
import * as util from '../../../util'

const kCoverTable = USCALobbyingCampaignDiscCover
const kCoverTableName = 'us_calaccess_campaign_disc_cover'

const kContribTable = USCALobbyingCampaignContrib
const kContribTableName = 'us_calaccess_campaign_contrib'

const kPreElectExpendTable = USCALobbyingPreElectExpend
const kPreElectExpendTableName = 'us_calaccess_pre_elect_expend'

const kContribTagDataset = 'us_calaccess_campaign_contrib'
const kPreElectExpendTagDataset = 'us_calaccess_pre_elect_expend'

const coverFullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(filing_id)',
  'lower(cover::text)',
  'lower(unmatched_memos::text)',
])

const contribFullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(contrib_table.filing_id)',
  'lower(contrib_table.transaction_id)',
  'lower(contrib_table.campaign_contribution::text)',
])

const expendFullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(expend_table.filing_id)',
  'lower(expend_table.transaction_id)',
  'lower(expend_table.pre_election_expenditure::text)',
])

async function forceNestedLoopJoin(manager) {
  // We force our joins to use the nested loop approach.
  // This leads to an order of magnitude speedup for California lobbying.
  await manager.query('SET enable_hashjoin = false;')
  await manager.query('SET enable_mergejoin = false;')
}

function expandNames(
  entityList: string[],
  entityAlternates,
  entities = new Map<string, Map<string, any>>()
): any[] {
  const feedPath = ['feeds', 'us', 'ca', 'lobbying', 'contrib']
  let nameSet = new Set<string>()
  for (const v of entityList) {
    const feedInfo = entities.has(v)
      ? util.jsonGetFromPath(entities.get(v), feedPath)
      : undefined
    if (feedInfo == undefined || !feedInfo.get('blockName')) {
      nameSet.add(JSON.stringify({ name: v }))
    }
    if (entityAlternates.has(v)) {
      for (const alternate of entityAlternates.get(v)) {
        nameSet.add(alternate)
      }
    }
    if (feedInfo != undefined && feedInfo.has('includes')) {
      for (const alternate of feedInfo.get('includes')) {
        nameSet.add(
          JSON.stringify(
            util.isString(alternate) ? { name: alternate } : alternate
          )
        )
      }
    }
  }
  return Array.from(nameSet.values()).map((blob) => JSON.parse(blob))
}

function getQueryTargets(targets: any[]): string[] {
  if (!targets) {
    return []
  }

  let queryTargets = []
  for (const target of targets) {
    let targetObj = { name: target['name'] }
    if ('employer' in target) {
      targetObj['employer'] = target['employer']
    }
    queryTargets.push(JSON.stringify([targetObj]))
  }
  return queryTargets
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let dateWhere = ''
  if (dateRange.hasStart()) {
    dateWhere += ` AND (cover->>'period_end') >= $${params.length + 1}`
    params.push(dateRange.start)
  }
  if (dateRange.hasEnd()) {
    dateWhere += ` AND (cover->>'period_begin') <= $${params.length + 1}`
    params.push(dateRange.end)
  }
  return dateWhere
}

async function handleGetContributionsTags(request, response) {
  return await tags.handleGetTags(request, response, kContribTagDataset)
}

async function handleAddContributionsTag(request, response) {
  return await tags.handleAddTag(request, response, kContribTagDataset)
}

async function handleDeleteContributionsTag(request, response) {
  return await tags.handleDeleteTag(request, response, kContribTagDataset)
}

async function handleGetPreElectExpendTags(request, response) {
  return await tags.handleGetTags(request, response, kPreElectExpendTagDataset)
}

async function handleAddPreElectExpendTag(request, response) {
  return await tags.handleAddTag(request, response, kPreElectExpendTagDataset)
}

async function handleDeletePreElectExpendTag(request, response) {
  return await tags.handleDeleteTag(
    request,
    response,
    kPreElectExpendTagDataset
  )
}

async function haveContributionsFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const limit = 10000

  const dateWhere = buildDateConstraint(dateRange, params)

  const queryStr = `
    (
     SELECT 1 FROM (
      SELECT * FROM ${kCoverTableName}
      WHERE ${coverFullTextSearchString}
       @@ websearch_to_tsquery('simple', $${params.length + 1})
       ${dateWhere} LIMIT ${limit}
     ) cover_table INNER JOIN ${kContribTableName} contrib_table
     ON cover_table.filing_id = contrib_table.filing_id
     LIMIT ${limit}
    ) UNION (
     SELECT 1 FROM ${kCoverTableName} cover_table
     INNER JOIN ${kContribTableName} contrib_table
      ON cover_table.filing_id = contrib_table.filing_id
     WHERE ${contribFullTextSearchString}
      @@ websearch_to_tsquery('simple', $${params.length + 1})
      ${dateWhere} LIMIT ${limit}
    )`
  params.push(webSearch)

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)
  return await util.nonemptyQuery(manager, queryStr, params)
}

async function haveExpenditureFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  const limit = 100000

  const queryStr = `
    (
     SELECT 1 FROM (
      SELECT * FROM ${kCoverTableName}
      WHERE ${coverFullTextSearchString}
       @@ websearch_to_tsquery('simple', $${params.length + 1})
       ${dateWhere} LIMIT ${limit}
     ) cover_table INNER JOIN ${kPreElectExpendTableName} expend_table
     ON cover_table.filing_id = expend_table.filing_id
     LIMIT ${limit}
    ) UNION (
     SELECT 1 FROM ${kCoverTableName} cover_table
     INNER JOIN ${kPreElectExpendTableName} expend_table
      ON cover_table.filing_id = expend_table.filing_id
     WHERE ${expendFullTextSearchString}
      @@ websearch_to_tsquery('simple', $${params.length + 1})
      ${dateWhere} LIMIT ${limit}
    )`
  params.push(webSearch)

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)
  return await util.nonemptyQuery(manager, queryStr, params)
}

async function haveContributionsFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kContribTagDataset)
}

async function haveExpenditureFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(
    webSearch,
    token,
    kPreElectExpendTagDataset
  )
}

function annotateFiling(filing, entities, feedInfo: FeedInfo): void {
  let annotation = {}

  // Normalize the entity names.
  let entityAnnotations = {}
  for (let entity of filing.entities) {
    if (util.isString(entity)) {
      entity = { name: entity }
    }
    const name = util.normalizeEntityWithEmployer(
      entity['name'],
      entity['employer'],
      feedInfo.normalization
    )
    entityAnnotations[util.canonicalText(entity['name'])] = {
      origText: entity['name'],
      text: name,
      stylizedText: util.getFullyStylizedName(name, entities),
      logo: util.logoURLFromAncestors(name, entities),
    }
  }
  annotation['entities'] = entityAnnotations

  filing['annotation'] = annotation
}

function annotate(results, entities, feedInfo: FeedInfo): void {
  let maxAmount: number = 0
  let totalAmount: number = 0
  for (const filing of results.filings) {
    annotateFiling(filing, entities, feedInfo)

    let amount: number = 0
    if ('campaign_contribution' in filing) {
      const contribution = filing['campaign_contribution']
      if ('amount' in contribution && 'received' in contribution['amount']) {
        amount = contribution['amount']['received']
      }
    } else if ('pre_election_expenditure' in filing) {
      const expenditure = filing['pre_election_expenditure']
      if ('amount' in expenditure) {
        amount = expenditure['amount']
      }
    }

    maxAmount = Math.max(maxAmount, amount)
    totalAmount += amount
  }
  results['amountSummaries'] = { max: maxAmount, total: totalAmount }
}

async function haveContributionsForNames(
  targets: any[],
  dateRange: util.DateRange,
  entities,
  maxTargetsPerQuery = 20
): Promise<boolean> {
  for (const target of targets) {
    if ('employer' in target) {
      continue
    }
    const name = target['name']
    if (
      util.canAssumeRecordsExistForName(
        name,
        dateRange,
        entities,
        ['feeds', 'us', 'ca', 'lobbying', 'contrib'],
        'haveContribution'
      )
    ) {
      return true
    }
  }

  const queryTargets = getQueryTargets(targets)

  // This appears to need to be so large to prevent a Postgres performance
  // bug of not selecting the index for the EU lookup.
  const limit = 100000

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)

  for (
    let sliceBeg = 0;
    sliceBeg < queryTargets.length;
    sliceBeg += maxTargetsPerQuery
  ) {
    const sliceEnd = Math.min(
      sliceBeg + maxTargetsPerQuery,
      queryTargets.length
    )
    const subTargets = queryTargets.slice(sliceBeg, sliceEnd)

    let params = []
    const dateWhere = buildDateConstraint(dateRange, params)

    const queryStr = `
      (
       SELECT 1 FROM (
        SELECT * FROM ${kCoverTableName} WHERE
         entities @> ANY(cast($${params.length + 1} as jsonb[]))
         ${dateWhere} LIMIT ${limit}
       ) cover_table INNER JOIN ${kContribTableName} contrib_table
       ON cover_table.filing_id = contrib_table.filing_id
       LIMIT ${limit}
      ) UNION (
       SELECT 1 FROM ${kCoverTableName} cover_table
       INNER JOIN ${kContribTableName} contrib_table
        ON cover_table.filing_id = contrib_table.filing_id
       WHERE
        contrib_table.entities @> ANY(cast($${params.length + 1} as jsonb[]))
        ${dateWhere} LIMIT ${limit}
      )`
    params.push(subTargets)

    const haveResult = await util.nonemptyQuery(manager, queryStr, params)
    if (haveResult) {
      return true
    }
  }
  return false
}

async function haveExpenditureForNames(
  targets: any[],
  dateRange: util.DateRange,
  entities,
  maxTargetsPerQuery = 20
): Promise<boolean> {
  for (const target of targets) {
    if ('employer' in target) {
      continue
    }
    const name = target['name']
    if (
      util.canAssumeRecordsExistForName(
        name,
        dateRange,
        entities,
        ['feeds', 'us', 'ca', 'lobbying', 'contrib'],
        'haveExpenditure'
      )
    ) {
      return true
    }
  }

  const limit = 10000

  const queryTargets = getQueryTargets(targets)

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)

  for (
    let sliceBeg = 0;
    sliceBeg < queryTargets.length;
    sliceBeg += maxTargetsPerQuery
  ) {
    const sliceEnd = Math.min(
      sliceBeg + maxTargetsPerQuery,
      queryTargets.length
    )
    const subTargets = queryTargets.slice(sliceBeg, sliceEnd)

    let params = []
    const dateWhere = buildDateConstraint(dateRange, params)

    const queryStr = `
      (
       SELECT 1 FROM (
        SELECT * FROM ${kCoverTableName} WHERE
         entities @> ANY(cast($${params.length + 1} as jsonb[]))
         ${dateWhere} LIMIT ${limit}
       ) cover_table INNER JOIN ${kPreElectExpendTableName} expend_table
       ON cover_table.filing_id = expend_table.filing_id
       LIMIT ${limit}
      ) UNION (
       SELECT 1 FROM ${kCoverTableName} cover_table
       INNER JOIN ${kPreElectExpendTableName} expend_table
        ON cover_table.filing_id = expend_table.filing_id
       WHERE
        expend_table.entities @> ANY(cast($${params.length + 1} as jsonb[]))
        ${dateWhere} LIMIT ${limit}
      )`
    params.push(subTargets)

    const haveResult = await util.nonemptyQuery(manager, queryStr, params)
    if (haveResult) {
      return true
    }
  }
  return false
}

async function haveContributionsForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities,
  entityChildren,
  feedInfo: FeedInfo
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const targets = expandNames(names, feedInfo.names, entities)
  return await haveContributionsForNames(targets, dateRange, entities)
}

async function haveExpenditureForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities,
  entityChildren,
  feedInfo: FeedInfo
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const targets = expandNames(names, feedInfo.names, entities)
  return await haveExpenditureForNames(targets, dateRange, entities)
}

async function getContributionsFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  let queryStr = `
    SELECT * FROM (
     (
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       contrib_table.campaign_contribution,
       contrib_table.transaction_id,
       contrib_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || contrib_table.entities) as entities
      FROM (
       SELECT * FROM ${kCoverTableName}
       WHERE ${coverFullTextSearchString}
        @@ websearch_to_tsquery('simple', $${params.length + 1})
        ${dateWhere} ORDER BY (cover->>'filed_date') DESC
       LIMIT $${params.length + 2}
      ) cover_table INNER JOIN ${kContribTableName} contrib_table
      ON cover_table.filing_id = contrib_table.filing_id
       ORDER BY (cover->>'filed_date') DESC LIMIT $${params.length + 2}
     ) UNION (
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       contrib_table.campaign_contribution,
       contrib_table.transaction_id,
       contrib_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || contrib_table.entities) as entities
      FROM ${kCoverTableName} cover_table
      INNER JOIN ${kContribTableName} contrib_table
       ON cover_table.filing_id = contrib_table.filing_id
      WHERE ${contribFullTextSearchString}
       @@ websearch_to_tsquery('simple', $${params.length + 1})
       ${dateWhere} ORDER BY (cover->>'filed_date') DESC
       LIMIT $${params.length + 2}
     )
    ) q ORDER BY (cover->>'filed_date') DESC LIMIT $${params.length + 2}`
  params.push(webSearch)
  params.push(maxSearchResults)

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)

  return await util.getQueryFilings(manager, queryStr, params, maxSearchResults)
}

async function getContributionsFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const usernames = getUsernames(token)

  let params = []

  let queryStr = `
    SELECT
     cover_table.filing_id,
     cover_table.cover,
     contrib_table.campaign_contribution,
     contrib_table.transaction_id,
     contrib_table.line_item,
     cover_table.unmatched_memos,
     (cover_table.entities || contrib_table.entities) as entities
    FROM ${kCoverTableName} cover_table
    INNER JOIN ${kContribTableName} contrib_table
     ON cover_table.filing_id = contrib_table.filing_id
    WHERE
     ROW(contrib_table.filing_id, contrib_table.transaction_id, contrib_table.line_item) IN (
        SELECT
          unique_data->>'filing_id',
          unique_data->>'transaction_id',
          unique_data->>'line_item' FROM tags
          WHERE dataset = $${params.length + 1}
            AND username = ANY($${params.length + 2})
            AND LOWER(tag) @@ websearch_to_tsquery($${params.length + 3})
      )
    ORDER BY (cover->>'filed_date') DESC LIMIT $${params.length + 4}`
  params.push(kContribTagDataset)
  params.push(usernames)
  params.push(webSearch)
  params.push(maxSearchResults)

  let manager = dataSource.manager
  return await util.getQueryFilings(manager, queryStr, params, maxSearchResults)
}

async function haveContributionsOverDateRange(
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const limit = 10000

  const dateWhere = buildDateConstraint(dateRange, params)
  const whereStr = dateWhere ? `WHERE ${dateWhere}` : ''

  const queryStr = `
    (
     SELECT 1 FROM (
      SELECT * FROM ${kCoverTableName} ${whereStr} LIMIT ${limit}
     ) cover_table INNER JOIN ${kContribTableName} contrib_table
     ON cover_table.filing_id = contrib_table.filing_id
     LIMIT ${limit}
    ) UNION (
     SELECT 1 FROM ${kCoverTableName} cover_table
     INNER JOIN ${kContribTableName} contrib_table
      ON cover_table.filing_id = contrib_table.filing_id
     ${whereStr} LIMIT ${limit}
    )`

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)
  return await util.nonemptyQuery(manager, queryStr, params)
}

async function getContributionsOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  let queryStr = `
    SELECT * FROM (
     SELECT cover_table.filing_id,
     cover_table.cover,
     contrib_table.campaign_contribution,
     contrib_table.transaction_id,
     contrib_table.line_item,
     cover_table.unmatched_memos,
     (cover_table.entities || contrib_table.entities) as entities
     FROM (
      SELECT * FROM ${kCoverTableName}
      WHERE ${dateWhere} ORDER BY (cover->>'filed_date') DESC
      LIMIT $${params.length + 1}
     ) cover_table INNER JOIN ${kContribTableName} contrib_table
     ON cover_table.filing_id = contrib_table.filing_id
    ) q ORDER BY (cover->>'filed_date') DESC
    LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)
  return await util.getQueryFilings(manager, queryStr, params, maxSearchResults)
}

async function getExpendituresFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  let queryStr = `
    SELECT * FROM (
     (
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       expend_table.pre_election_expenditure,
       expend_table.transaction_id,
       expend_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || expend_table.entities) as entities
      FROM (
       SELECT * FROM ${kCoverTableName}
       WHERE ${coverFullTextSearchString}
        @@ websearch_to_tsquery('simple', $${params.length + 1})
        ${dateWhere} ORDER BY (cover->>'filed_date') DESC
       LIMIT $${params.length + 2}
      ) cover_table INNER JOIN ${kPreElectExpendTableName} expend_table
      ON cover_table.filing_id = expend_table.filing_id
       ORDER BY (cover->>'filed_date') DESC LIMIT $${params.length + 2}
     ) UNION (
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       expend_table.pre_election_expenditure,
       expend_table.transaction_id,
       expend_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || expend_table.entities) as entities
      FROM ${kCoverTableName} cover_table
      INNER JOIN ${kPreElectExpendTableName} expend_table
       ON cover_table.filing_id = expend_table.filing_id
      WHERE ${expendFullTextSearchString}
       @@ websearch_to_tsquery('simple', $${params.length + 1})
       ${dateWhere} ORDER BY (cover->>'filed_date') DESC
      LIMIT $${params.length + 2}
     )
    ) q ORDER BY (cover->>'filed_date') DESC LIMIT $${params.length + 2}`
  params.push(webSearch)
  params.push(maxSearchResults)

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)
  return await util.getQueryFilings(manager, queryStr, params)
}

async function getExpendituresFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const username = getUsernames(token)

  let params = []

  let queryStr = `
    SELECT
     cover_table.filing_id,
     cover_table.cover,
     expend_table.pre_election_expenditure,
     expend_table.transaction_id,
     expend_table.line_item,
     cover_table.unmatched_memos,
     (cover_table.entities || expend_table.entities) as entities
    FROM ${kCoverTableName} cover_table
    INNER JOIN ${kPreElectExpendTableName} expend_table
     ON cover_table.filing_id = expend_table.filing_id
    WHERE
     ROW(expend_table.filing_id, expend_table.transaction_id, expend_table.line_item) IN (
        SELECT
          unique_data->>'filing_id',
          unique_data->>'transaction_id',
          unique_data->>'line_item' FROM tags
          WHERE dataset = $${params.length + 1}
            AND username = ANY($${params.length + 2})
            AND LOWER(tag) @@ websearch_to_tsquery($${params.length + 3})
      )
    ORDER BY (cover->>'filed_date') DESC LIMIT $${params.length + 4}`
  params.push(kPreElectExpendTagDataset)
  params.push(usernames)
  params.push(webSearch)
  params.push(maxSearchResults)

  let manager = dataSource.manager
  return await util.getQueryFilings(manager, queryStr, params, maxSearchResults)
}

async function haveExpenditureOverDateRange(
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const whereStr = dateWhere ? `WHERE ${dateWhere}` : ''

  const limit = 100000

  const queryStr = `
    (
     SELECT 1 FROM (
      SELECT * FROM ${kCoverTableName} ${whereStr} LIMIT ${limit}
     ) cover_table INNER JOIN ${kPreElectExpendTableName} expend_table
     ON cover_table.filing_id = expend_table.filing_id
     LIMIT ${limit}
    ) UNION (
     SELECT 1 FROM ${kCoverTableName} cover_table
     INNER JOIN ${kPreElectExpendTableName} expend_table
      ON cover_table.filing_id = expend_table.filing_id
     ${whereStr} LIMIT ${limit}
    )`

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)
  return await util.nonemptyQuery(manager, queryStr, params)
}

async function getExpendituresOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  let queryStr = `
    SELECT * FROM (
     SELECT cover_table.filing_id,
     cover_table.cover,
     expend_table.pre_election_expenditure,
     expend_table.transaction_id,
     expend_table.line_item,
     cover_table.unmatched_memos,
     (cover_table.entities || expend_table.entities) as entities
     FROM (
      SELECT * FROM ${kCoverTableName}
      WHERE ${dateWhere} ORDER BY (cover->>'filed_date') DESC
      LIMIT $${params.length + 1}
     ) cover_table INNER JOIN ${kPreElectExpendTableName} expend_table
     ON cover_table.filing_id = expend_table.filing_id
    ) q ORDER BY (cover->>'filed_date') DESC
    LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)
  return await util.getQueryFilings(manager, queryStr, params)
}

async function getContributionsForNames(
  targets: any[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const queryTargets = getQueryTargets(targets)

  let queryStr = `
    SELECT * FROM (
     (
      SELECT
      cover_table.filing_id,
      cover_table.cover,
      contrib_table.campaign_contribution,
      contrib_table.transaction_id,
      contrib_table.line_item,
      cover_table.unmatched_memos,
      (cover_table.entities || contrib_table.entities) as entities
      FROM (
       SELECT * FROM ${kCoverTableName} WHERE
        entities @> ANY(cast($${params.length + 1} as jsonb[]))
        ${dateWhere} ORDER BY (cover->>'filed_date') DESC
       LIMIT $${params.length + 2}
      ) AS cover_table
      INNER JOIN ${kContribTableName} contrib_table
      ON cover_table.filing_id = contrib_table.filing_id
       ORDER BY (cover->>'filed_date') DESC LIMIT $${params.length + 2}
     ) UNION (
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       contrib_table.campaign_contribution,
       contrib_table.transaction_id,
       contrib_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || contrib_table.entities) as entities
      FROM ${kCoverTableName} cover_table
      INNER JOIN ${kContribTableName} contrib_table
       ON cover_table.filing_id = contrib_table.filing_id
      WHERE
       contrib_table.entities @> ANY(cast($${params.length + 1} as jsonb[]))
       ${dateWhere} ORDER BY (cover->>'filed_date') DESC
      LIMIT $${params.length + 2}
     )
    ) q ORDER BY (cover->>'filed_date') DESC LIMIT $${params.length + 2}`
  params.push(queryTargets)
  params.push(maxSearchResults)

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)
  return await util.getQueryFilings(manager, queryStr, params, maxSearchResults)
}

async function getExpendituresForNames(
  targets: any[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const queryTargets = getQueryTargets(targets)

  let queryStr = `
    SELECT * FROM (
     (
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       expend_table.pre_election_expenditure,
       expend_table.transaction_id,
       expend_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || expend_table.entities) as entities
      FROM (
       SELECT * FROM ${kCoverTableName}
       WHERE
        entities @> ANY(cast($${params.length + 1} as jsonb[]))
        ${dateWhere} ORDER BY (cover->>'filed_date') DESC
       LIMIT $${params.length + 2}
      ) AS cover_table INNER JOIN ${kPreElectExpendTableName} expend_table
      ON cover_table.filing_id = expend_table.filing_id
       ORDER BY (cover->>'filed_date') DESC LIMIT $${params.length + 2}
     ) UNION (
      SELECT
       cover_table.filing_id,
       cover_table.cover,
       expend_table.pre_election_expenditure,
       expend_table.transaction_id,
       expend_table.line_item,
       cover_table.unmatched_memos,
       (cover_table.entities || expend_table.entities) as entities
      FROM ${kCoverTableName} cover_table
      INNER JOIN ${kPreElectExpendTableName} expend_table
       ON cover_table.filing_id = expend_table.filing_id
      WHERE
       expend_table.entities @> ANY(cast($${params.length + 1} as jsonb[]))
       ${dateWhere} ORDER BY (cover->>'filed_date') DESC
      LIMIT $${params.length + 2}
     )
    ) q ORDER BY (cover->>'filed_date') DESC LIMIT $${params.length + 2}`
  params.push(queryTargets)
  params.push(maxSearchResults)

  let manager = dataSource.manager
  await forceNestedLoopJoin(manager)
  return await util.getQueryFilings(manager, queryStr, params, maxSearchResults)
}

async function getContributionsForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities,
  entityChildren,
  feedInfo: FeedInfo,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const targets = expandNames(names, feedInfo.names, entities)
  return await getContributionsForNames(
    targets,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function getExpendituresForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities,
  entityChildren,
  feedInfo: FeedInfo,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const targets = expandNames(names, feedInfo.names, entities)
  return await getExpendituresForNames(
    targets,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function contributionsHaveAPI(
  request,
  response,
  entities,
  entityChildren,
  feedInfo: FeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Have CALACCESS campaign contribution w/ text: ${text}`)
    const minDigitLength = 5
    if (util.isShortDigitSequence(text, minDigitLength)) {
      console.log(
        `Avoiding out-of-memory error from short digit sequence: ${text}`
      )
    } else {
      result = await haveContributionsFromSearch(text, dateRange)
    }
  } else if (tagText) {
    console.log(`Have CALACCESS campaign contribution w/ tag: ${tagText}`)
    result = await haveContributionsFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Have CALCCESS campaign contributions w/ vendor: ${name}`)
    result = await haveContributionsForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo
    )
  } else {
    console.log('Have CALACCESS campaign contributions over date range')
    result = await haveContributionsOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function contributionsGetAPI(
  request,
  response,
  entities,
  entityChildren,
  feedInfo: FeedInfo
) {
  const params = request.query

  const maxSearchResultsDefault = 1000
  const maxSearchResultsCap = 2000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`CALACCESS campaign contributions w/ text: ${text}`)
    const minDigitLength = 5
    if (util.isShortDigitSequence(text, minDigitLength)) {
      console.log(
        `Avoiding out-of-memory error from short digit sequence: ${text}`
      )
    } else {
      result = await getContributionsFromSearch(
        text,
        dateRange,
        maxSearchResults,
        searchOffset
      )
    }
  } else if (tagText) {
    console.log(`CALACCESS campaign contributions w/ tag: ${tagText}`)
    result = await getContributionsFromTagSearch(
      tagText,
      token,
      maxSearchResults
    )
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`CALCCESS campaign contributions w/ vendor: ${name}`)
    result = await getContributionsForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('CALACCESS Campaign Contributions over date range')
    result = await getContributionsOverDateRange(
      dateRange,
      maxSearchResults,
      searchOffset
    )
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, feedInfo)
  }

  util.setJSONResponse(response, result)
}

async function expendituresHaveAPI(
  request,
  response,
  entities,
  entityChildren,
  feedInfo: FeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Have CALACCESS pre-election expenditure w/ text: ${text}`)
    const minDigitLength = 5
    if (util.isShortDigitSequence(text, minDigitLength)) {
      console.log(
        `Avoiding out-of-memory error from short digit sequence: ${text}`
      )
    } else {
      result = await haveExpenditureFromSearch(text, dateRange)
    }
  } else if (tagText) {
    console.log(`Have CALACCESS pre-election expenditure w/ tag: ${tagText}`)
    result = await haveExpenditureFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Have CALCCESS pre-election expenditure w/ vendor: ${name}`)
    result = await haveExpenditureForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo
    )
  } else {
    console.log('Have CALACCESS Pre-election Expenditures over date range')
    result = await haveExpenditureOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function expendituresGetAPI(
  request,
  response,
  entities,
  entityChildren,
  feedInfo: FeedInfo
) {
  const params = request.query

  const maxSearchResultsDefault = 1000
  const maxSearchResultsCap = 2000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`CALACCESS pre-election expenditures w/ text: ${text}`)
    const minDigitLength = 5
    if (util.isShortDigitSequence(text, minDigitLength)) {
      console.log(
        `Avoiding out-of-memory error from short digit sequence: ${text}`
      )
    } else {
      result = await getExpendituresFromSearch(
        text,
        dateRange,
        maxSearchResults,
        searchOffset
      )
    }
  } else if (tagText) {
    console.log(`CALACCESS pre-election expenditures w/ tag: ${tagText}`)
    result = await getExpendituresFromTagSearch(
      tagText,
      token,
      maxSearchResults
    )
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`CALCCESS pre-election expenditures w/ vendor: ${name}`)
    result = await getExpendituresForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('CALACCESS pre-election expenditures over date range')
    result = await getExpendituresOverDateRange(
      dateRange,
      maxSearchResults,
      searchOffset
    )
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, feedInfo)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/us/ca/lobbying'
  const contribRoute = `${route}/contributions`
  const preElectExpendRoute = `${route}/pre-election-expenditure`

  const feed = entityState.feeds
    .get('us')
    .get('ca')
    .get('lobbying')
    .get('contrib')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether campaign contributions exist.
  router.get(`${contribRoute}/have`, async function (request, response) {
    await contributionsHaveAPI(request, response, entities, children, feed)
  })

  // Returns JSON for campaign contributions.
  router.get(`${contribRoute}/get`, async function (request, response) {
    await contributionsGetAPI(request, response, entities, children, feed)
  })

  // Returns whether pre-election expenditures exist.
  router.get(`${preElectExpendRoute}/have`, async function (request, response) {
    await expendituresHaveAPI(request, response, entities, children, feed)
  })

  // Returns JSON for pre-election expenditures.
  router.get(`${preElectExpendRoute}/get`, async function (request, response) {
    await expendituresGetAPI(request, response, entities, children, feed)
  })

  router.post(`${contribRoute}/getTags`, handleGetContributionsTags)
  router.post(`${contribRoute}/addTag`, handleAddContributionsTag)
  router.post(`${contribRoute}/deleteTag`, handleDeleteContributionsTag)

  router.post(`${preElectExpendRoute}/getTags`, handleGetPreElectExpendTags)
  router.post(`${preElectExpendRoute}/addTag`, handleAddPreElectExpendTag)
  router.post(`${preElectExpendRoute}/deleteTag`, handleDeletePreElectExpendTag)
}
