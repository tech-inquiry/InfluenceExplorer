import { dataSource } from '../../App'
import { USFLProcurement } from '../../entity/us/fl/procurement'
import * as tags from '../../tags'
import * as util from '../../util'

const kTable = USFLProcurement
const kTableName = 'us_fl_facts_filing'

const kTagDataset = 'us_fl_facts_filing'

const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(agency)',
  'lower(vendor)',
  'lower(type)',
  'lower(agency_contract_id)',
  'lower(po_number)',
  'lower(grant_award_id)',
  'lower(commodity_type_code)',
  'lower(commodity_type_description)',
  'lower(short_title)',
  'lower(long_title)',
  'lower(status)',
  'lower(flair_contract_id)',
  'lower(agency_service_area)',
  'lower(authorized_advanced_payment)',
  'lower(method_of_procurement)',
  'lower(state_term_contract_id)',
  'lower(agency_reference_number)',
  'lower(contract_exemption_explanation)',
  'lower(statutory_authority)',
  'lower(recipient_type)',
  'lower(involves_gov_aid)',
  'lower(business_case_study)',
  'lower(legal_challenge_description)',
  'lower(considered_for_insourcing)',
  'lower(improvement_description)',
  'lower(comment)',
  'lower(cfda_code)',
  'lower(cfda_description)',
  'lower(csfa_code)',
  'lower(csfa_description)',
])

function getAgencyNames(
  name: string,
  entities: Map<string, Map<string, any>>
): string[] {
  let names = []
  if (!entities.has(name)) {
    return names
  }
  const profile = entities.get(name)
  const alternates = util.jsonGetFromPath(profile, [
    'feeds',
    'us',
    'fl',
    'procurement',
    'agencyNames',
  ])
  if (alternates != undefined) {
    names = alternates
  }
  const includes = util.jsonGetFromPath(profile, [
    'feeds',
    'us',
    'fl',
    'procurement',
    'agencyIncludes',
  ])
  if (includes != undefined) {
    names = names.concat(includes)
  }
  return names
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`begin_date >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`begin_date <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('begin_date >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('begin_date <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

// Set up the list of FL FACTS names for the vendor. If subsidiaries were
// requested to be included, the same process is repeated for each descendant.
function expandNames(
  entityList: string[],
  entityAlternates,
  entities
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'us',
    'fl',
    'procurement',
  ])
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateFiling(
  filing,
  entities,
  entityNormalization,
  agencyNormalization
) {
  const vendor = filing['vendor']
  const agency = filing['agency']
  const normalizedVendor = util.normalizeEntity(vendor, entityNormalization)
  const normalizedAgency = util.normalizeEntity(agency, agencyNormalization)

  // An example deep link for a 2016 award of Babel X to Florida's Department of
  // Law Enforcement is given at:
  //
  //   https://facts.fldfs.com/Search/PurchaseOrderDetails.aspx?AgencyId=710000&PONo=AE3731
  //
  // The relevant parameters are the AgencyId, 710000, which corresponds to the
  // Department of Law Enforcement, and the PO Number, AE3731.

  const agencyIDs = {
    'JUSTICE ADMINISTRATION': 210000,
    'STATE COURTS SYSTEM': 220000,
    'EXECUTIVE OFFICE OF THE GOVERNOR': 310000,
    'DEPARTMENT OF ENVIRONMENTAL PROTECTION': 370000,
    'DEPARTMENT OF ECONOMIC OPPORTUNITY': 400000,
    'DEPARTMENT OF FINANCIAL SERVICES': 430000,
    'DEPARTMENT OF STATE': 450000,
    'DEPARTMENT OF EDUCATION': 480000,
    'FLORIDA SCHOOL FOR THE DEAF AND THE BLIND': 489000,
    'DEPARTMENT OF TRANSPORTATION': 550000,
    'DEPARTMENT OF CHILDREN AND FAMILIES': 600000,
    'DEPARTMENT OF HEALTH': 640000,
    'AGENCY FOR PERSONS WITH DISABILITIES': 670000,
    'AGENCY FOR HEALTH CARE ADMINISTRATION': 680000,
    'DEPARTMENT OF CORRECTIONS': 700000,
    'DEPARTMENT OF LAW ENFORCEMENT': 710000,
    'DEPARTMENT OF MANAGEMENT SERVICES': 720000,
    'DIVISION OF ADMINISTRATIVE HEARINGS': 729700,
    'AGENCY FOR STATE TECHNOLOGY (FORMERLY SSRC/NSRC)': 729800,
    'DEPARTMENT OF REVENUE': 730000,
    'DEPARTMENT OF HIGHWAY SAFETY AND MOTOR VEHICLES': 760000,
    'FISH AND WILDLIFE CONSERVATION COMMISSION': 770000,
    'DEPT OF BUSINESS AND PROFESSIONAL REGULATION': 790000,
    'DEPARTMENT OF JUVENILE JUSTICE': 800000,
  }

  filing['annotation'] = {
    vendor: {
      origText: vendor,
      text: normalizedVendor,
      stylizedText: util.getFullyStylizedName(normalizedVendor, entities),
      url: util.encodeEntity(normalizedVendor),
      logo: util.logoURLFromAncestors(normalizedVendor, entities),
    },
    agency: {
      origText: agency,
      text: normalizedAgency,
      stylizedText: util.getFullyStylizedName(normalizedAgency, entities),
      url: util.encodeEntity(normalizedAgency),
      logo: util.logoURLFromAncestors(normalizedAgency, entities),
    },
  }

  if (
    (filing['po_number'] || filing['flair_contract_id']) &&
    filing['agency'] in agencyIDs
  ) {
    let url = 'https://facts.fldfs.com/Search/'
    const agencyID = agencyIDs[filing['agency']]
    if (filing['po_number']) {
      const poNumber = filing['po_number']
      url +=
        'PurchaseOrderDetails.aspx?AgencyId=' + agencyID + '&PONo=' + poNumber
    } else if (filing['flair_contract_id']) {
      const contractID = filing['flair_contract_id']
      url +=
        'ContractDetail.aspx?AgencyId=' + agencyID + '&ContractId=' + contractID
    }
    filing['annotation']['awardURL'] = url
  }
}

function annotate(result, entities, entityNormalization, agencyNormalization) {
  for (let filing of result.filings) {
    annotateFiling(filing, entities, entityNormalization, agencyNormalization)
  }
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const searchConstraint =
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function haveForNames(
  names: string[],
  dateRange: util.DateRange,
  entities
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(names, dateRange, entities, [
      'feeds',
      'us',
      'fl',
      'procurement',
    ])
  ) {
    return true
  }

  let params = []

  const searchConstraint = `LOWER(vendor) = ANY($${params.length + 1})`
  params.push(names)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForAgencies(
  names: string[],
  dateRange: util.DateRange,
  entities
): Promise<boolean> {
  if (!dateRange.hasStart() && !dateRange.hasEnd() && names.length) {
    return true
  }

  let params = []

  const searchConstraint = `LOWER(agency) = ANY($${params.length + 1})`
  params.push(names)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities,
  entityChildren,
  entityAlternates
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await haveForNames(expandedNames, dateRange, entities)
}

async function haveForAgency(
  name: string,
  dateRange: util.DateRange,
  entities
): Promise<boolean> {
  const names = getAgencyNames(name, entities)
  return await haveForAgencies(names, dateRange, entities)
}

async function getForNames(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where('LOWER(vendor) = ANY(ARRAY[:...names])', { names: names })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('begin_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForAgencies(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where('LOWER(agency) = ANY(ARRAY[:...names])', { names: names })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('begin_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities,
  entityChildren,
  entityAlternates,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await getForNames(
    expandedNames,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function getForAgency(
  name: string,
  dateRange: util.DateRange,
  entities,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = getAgencyNames(name, entities)
  return await getForAgencies(names, dateRange, maxSearchResults, searchOffset)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('begin_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = [
    'agency',
    'vendor',
    'type',
    'agency_contract_id',
    'po_number',
    'grant_award_id',
    'commodity_type_code',
    'flair_contract_id',
    'state_term_contract_id',
    'status',
  ]
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('begin_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities,
  entityChildren,
  entityAlternates
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Boolean Florida Awards w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have Florida contracts w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean Florida Awards w/ entity: ${name}`)
    result = await haveForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      entityAlternates
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`Boolean Florida awards w/ agency: ${name}`)
    result = await haveForAgency(name, dateRange, entities)
  } else {
    console.log('Boolean Florida Awards over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities,
  entityChildren,
  entityNormalization,
  entityAlternates,
  agencyNormalization
) {
  const params = request.query

  const maxSearchResultsDefault = 5000
  const maxSearchResultsCap = 25000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const dateRange = util.getDateRange(params)

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`Florida awards w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`US FL contracts w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Florida awards w/ entity: ${name}`)
    result = await getForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      entityAlternates,
      maxSearchResults,
      searchOffset
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`Florida awards w/ agency: ${name}`)
    result = await getForAgency(
      name,
      dateRange,
      entities,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('Florida awards over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, entityNormalization, agencyNormalization)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/us/fl/procurement'

  const feed = entityState.feeds.get('us').get('fl').get('procurement')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether there are results for a web search or an entity.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, feed.names)
  })

  // Returns JSON for a web search or an entity.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(
      request,
      response,
      entities,
      children,
      feed.normalization,
      feed.names,
      feed.agencyNormalization
    )
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
