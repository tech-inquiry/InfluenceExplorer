import { dataSource } from '../../App'
import * as util from '../../util'

import { USCentralLaborRelations } from '../../entity/us/central/labor_relations'

const kTable = USCentralLaborRelations
const kTableName = 'us_nlrb_filing'

const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(case_number)',
  'lower(status)',
  'lower(location)',
  'lower(region_assigned)',
  'lower(reason_closed)',
  'lower(docket_activity::TEXT)',
  'lower(related_documents::TEXT)',
  'lower(allegations::TEXT)',
  'lower(participants::TEXT)',
  'lower(related_cases::TEXT)',
  'lower(title)',
])

function buildDateConstraint(dateRange: util.DateRange, params) {
  let dateWhere = ''
  if (dateRange.hasStart()) {
    dateWhere += ` AND date_filed >= $${params.length + 1}`
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    dateWhere += ` AND date_filed <= $${params.length + 1}`
    params.push(dateRange.end.toISOString())
  }
  return dateWhere
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('date_filed >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('date_filed <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

// Set up the list of NLRB names for the vendor. If subsidiaries were
// requested to be included, the same process is repeated for each descendant.
function expandNames(
  entityList: string[],
  entityAlternates: Map<string, string[]>,
  entities: Map<string, Map<string, any>>
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'us',
    'central',
    'laborRelations',
  ])
}

function annotateCase(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>
) {
  const isChargedParty = function (chargingType) {
    const chargedTypes = ['charged party / respondent', 'employer']
    return chargedTypes.indexOf(chargingType) > -1
  }
  const isChargingParty = function (chargingType) {
    const chargingTypes = ['charging party', 'petitioner']
    return chargingTypes.indexOf(chargingType) > -1
  }
  const isUndecidedChargingParty = function (chargingType) {
    return !isChargingParty(chargingType) && !isChargedParty(chargingType)
  }

  let chargedParties = []
  let chargingParties = []
  let undecidedChargingParties = []
  for (let participant of filing.participants) {
    if (participant['employer']) {
      const employer = participant['employer']
      const normalizedEmployer = util.normalizeEntity(
        employer,
        entityNormalization
      )
      participant['employer'] = {
        origText: employer,
        text: normalizedEmployer,
        stylizedText: util.getFullyStylizedName(normalizedEmployer, entities),
        url: util.encodeEntity(normalizedEmployer),
        logo: util.logoURLFromAncestors(normalizedEmployer, entities),
      }
    }

    if (participant['name']) {
      const name = participant['name']
      const normalizedName = util.normalizeEntity(name, entityNormalization)
      participant['name'] = {
        origText: name,
        text: normalizedName,
        stylizedText: util.getFullyStylizedName(normalizedName, entities),
        url: util.encodeEntity(normalizedName),
        logo: util.logoURLFromAncestors(normalizedName, entities),
      }
    }

    if (isChargedParty(participant.charging_type)) {
      chargedParties.push(participant)
    } else if (isChargingParty(participant.charging_type)) {
      chargingParties.push(participant)
    } else {
      undecidedChargingParties.push(participant)
    }
  }

  const allegationsMap = {
    '8(a)(1) Coercive Actions (Surveillance, etc)':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/coercion-of-employees-section-8b1a',
    '8(a)(1) Coercive Actions (Threats, Promises of Benefits, etc.)':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/coercion-of-employees-section-8b1a',
    '8(a)(1) Coercive Rules':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/coercion-of-employees-section-8b1a',
    '8(a)(1) Coercive Statements (Threats, Promises of Benefits, etc.)':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/coercion-of-employees-section-8b1a',
    '8(a)(1) Concerted Activities (Retaliation, Discharge, Discipline)':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/interfering-with-employee-rights-section-7-8a1',
    '8(a)(1) Discharge of supervisor (Parker-Robb Chevrolet)':
      'https://www.repository.law.indiana.edu/cgi/viewcontent.cgi?referer=https://www.google.com/&httpsredir=1&article=1953&context=facpub',
    '8(a)(1) Interrogation (including Polling)':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/interfering-with-employee-rights-section-7-8a1',
    '8(a)(1) Weingarten':
      'https://www.natlawreview.com/article/labor-board-union-s-inclusion-weingarten-rights-statement-collective-bargaining',
    '8(a)(2) Assistance':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/interfering-with-or-dominating-a-union-section-8a2',
    '8(a)(2) Domination':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/interfering-with-or-dominating-a-union-section-8a2',
    '8(a)(2) Unlawful Recognition':
      'https://www.nlrb.gov/sites/default/files/attachments/pages/node-184/basicguide.pdf',
    '8(a)(3) Changes in Terms and Conditions of Employment':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/discriminating-against-employees-because-of-their-union',
    '8(a)(3) Discharge (Including Layoff and Refusal to Hire (not salting))':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/discriminating-against-employees-because-of-their-union',
    '8(a)(3) Discharge (including Layoff and Refusal to Hire)':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/discriminating-against-employees-because-of-their-union',
    '8(a)(3) Discharge (Including Layoff and Refusal to Hire)':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/discriminating-against-employees-because-of-their-union',
    '8(a)(3) Discipline':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/discriminating-against-employees-because-of-their-union',
    "8(a)(3) Refusal to Reinstate E'ee/Striker (e.g. Laidlaw)":
      'https://law.justia.com/cases/federal/appellate-courts/F2/414/99/84593/',
    '8(a)(4) Discharge (including Layoff and Refusal to Hire)':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/discriminating-against-employees-for-nlrb-activity-section-8a4',
    '8(a)(4) Discipline':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/discriminating-against-employees-for-nlrb-activity-section-8a4',
    "8(a)(5) Refusal to Bargain/Bad Faith Bargaining (incl'g surface bargaining/direct dealing)":
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/bargaining-in-good-faith-with-employees-union-representative',
    '8(a)(5) Refusal to Furnish Information':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/bargaining-in-good-faith-with-employees-union-representative',
    '8(a)(5) Refusal to Recognize':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/bargaining-in-good-faith-with-employees-union-representative',
    '8(a)(5) Repudiation/Modification of Contract [Sec 8(d)/Unilateral Changes]':
      'https://www.nlrb.gov/about-nlrb/rights-we-protect/the-law/bargaining-in-good-faith-with-employees-union-representative',
  }

  let allegations = []
  for (const allegation of filing.allegations) {
    let item = { stylizedText: allegation }
    if (allegation in allegationsMap) {
      item['url'] = allegationsMap[allegation]
    }
    allegations.push(item)
  }

  filing['annotation'] = {
    chargedParties: chargedParties,
    chargingParties: chargingParties,
    undecidedChargingParties: undecidedChargingParties,
    allegations: allegations,
  }
}

// Annotate the NLRB filings with their last update, then sort with it.
function addLastUpdate(result) {
  for (let filing of result['filings']) {
    util.addLastUpdateAnnotationToNLRBFiling(filing)
  }
  util.sortNLRBFilingsByLastUpdate(result['filings'])
}

function annotate(
  result,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>
) {
  for (let filing of result['filings']) {
    annotateCase(filing, entities, entityNormalization)
  }
}

async function haveForNames(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
) {
  if (
    util.canAssumeRecordsExist(names, dateRange, entities, [
      'feeds',
      'us',
      'central',
      'laborRelations',
    ])
  ) {
    return true
  }

  let params = []

  let jsonTargets = []
  for (const name of names) {
    const escapedName = name.replace(/"/g, '\\"')
    jsonTargets.push(JSON.stringify([{ name: name }]))
    jsonTargets.push(JSON.stringify([{ employer: name }]))
  }
  const anyMatch = `ANY(cast($${params.length + 1} as jsonb[]))`
  params.push(jsonTargets)

  const dateWhere = buildDateConstraint(dateRange, params)

  const queryStr = `
    SELECT 1 FROM ${kTableName} WHERE (
     LOWER(participants::TEXT)::JSONB @> ${anyMatch} OR
     LOWER(title) = ANY($${params.length + 1})
    ) ${dateWhere}  LIMIT 10000`
  params.push(names)

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await haveForNames(expandedNames, dateRange, entities)
}

async function getForNames(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let nameKeys = []
  for (let name of names) {
    name = name.replace(/'/g, "''").replace(/"/g, '\\"')
    nameKeys.push(
      `'[{"name": "${name}"}]'::jsonb, ` + `'[{"employer": "${name}"}]'::jsonb`
    )
  }
  const anyMatch = `ANY(ARRAY[${nameKeys.join(', ')}])`

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(`LOWER(participants::TEXT)::JSONB @> ${anyMatch}`)
    .orWhere('LOWER(title) = ANY(ARRAY[:...names])', { names: names })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('date_filed', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await getForNames(
    expandedNames,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function haveFromSearch(webSearch: string, dateRange: util.DateRange) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  const queryStr = `
    SELECT 1 FROM ${kTableName} WHERE
     ${fullTextSearchString}
     @@ websearch_to_tsquery('simple', $${params.length + 1})
     ${dateWhere} LIMIT 1000`
  params.push(webSearch)

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('date_filed', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveOverDateRange(dateRange: util.DateRange) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const whereStr = dateWhere ? `WHERE ${dateWhere}` : ''

  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('date_filed', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)

  let result = false
  if (text) {
    console.log(`Boolean NLRB w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean NLRB w/ entity: ${name}`)
    result = await haveForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      entityAlternates
    )
  } else {
    console.log('Boolean NLRB over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityNormalization: Map<string, string>,
  entityAlternates: Map<string, string[]>
) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`NLRB w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`NLRB w/ entity: ${name}`)
    result = await getForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      entityAlternates,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('NLRB over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  addLastUpdate(result)
  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, entityNormalization)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const parentRoute = '/api/us/central'

  const feed = entityState.feeds.get('us').get('central').get('laborRelations')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether filings exist for a given entity or web search.
  router.get(
    `${parentRoute}/haveLaborRelations`,
    async function (request, response) {
      await haveAPI(request, response, entities, children, feed.names)
    }
  )

  // Returns JSON for filings for a web search or an entity.
  router.get(
    `${parentRoute}/laborRelations`,
    async function (request, response) {
      await getAPI(
        request,
        response,
        entities,
        children,
        feed.normalization,
        feed.names
      )
    }
  )
}
