import { dataSource } from '../../App'
import { Cablegate } from '../../entity/us/central/cablegate'
import * as tags from '../../tags'
import * as util from '../../util'

const signatureMap = require('../../../data/cablegate_signatures.json')

const kTable = Cablegate
const kTableName = 'cablegate'

const kTagDataset = 'cablegate'

const fullTextSearchString = 'tsvector_col'

function getEmbassies(name: string, entities: Map<string, Map<string, any>>) {
  if (!entities.has(name)) {
    return []
  }
  const profile = entities.get(name)
  let embassies = []
  const alternates = util.jsonGetFromPath(profile, [
    'feeds',
    'us',
    'central',
    'cablegate',
    'embassyAlternates',
  ])
  if (alternates) {
    embassies = alternates
  }
  const includes = util.jsonGetFromPath(profile, [
    'feeds',
    'us',
    'central',
    'cablegate',
    'embassyIncludes',
  ])
  if (includes) {
    embassies = embassies.concat(includes)
  }
  return embassies
}

function getReferences(
  name: string,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>
) {
  const descendants = util.expandToEntitySubtree(name, false, entityChildren)

  let references = new Set<string>()
  for (const descendant of descendants) {
    if (!entities.has(descendant)) {
      continue
    }
    const profile = entities.get(descendant)
    const alternates = util.jsonGetFromPath(profile, [
      'feeds',
      'us',
      'central',
      'cablegate',
      'references',
    ])
    if (alternates) {
      for (const alternate of alternates) {
        if (util.isString(alternate)) {
          references.add(alternate)
        } else if (alternate.has('id')) {
          references.add(alternate.get('id'))
        } else if (alternate.has('ids')) {
          for (const id of alternate.get('ids')) {
            references.add(id)
          }
        }
      }
    }
  }

  return Array.from(references.values())
}

function getTags(
  name: string,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  useSubsidiaries: boolean
) {
  const descendants = util.expandToEntitySubtree(
    name,
    useSubsidiaries,
    entityChildren
  )

  let tagSet = new Set<string>()
  for (const descendant of descendants) {
    if (!entities.has(descendant)) {
      continue
    }
    const profile = entities.get(descendant)
    const tags = util.jsonGetFromPath(profile, [
      'feeds',
      'us',
      'central',
      'cablegate',
      'tags',
    ])
    if (tags) {
      for (const tag of tags) {
        tagSet.add(tag)
      }
    }
  }

  return Array.from(tagSet.values())
}

function getSpans(name: string, entities: Map<string, Map<string, any>>) {
  if (!entities.has(name)) {
    return []
  }
  const profile = entities.get(name)
  const spans = util.jsonGetFromPath(profile, [
    'feeds',
    'us',
    'central',
    'cablegate',
    'authorSpans',
  ])
  if (spans) {
    return spans
  }
  return []
}

// TODO(Jack Poulson): Avoid duplication within us/central/procurement.ts
function union(a: any[], b: any[]): any[] {
  let filings = []
  let idSet = new Set<number>()
  for (const filing of a) {
    filings.push({ ...filing })
    idSet.add(filing.id)
  }
  for (const filing of b) {
    if (!idSet.has(filing.id)) {
      filings.push({ ...filing })
      idSet.add(filing.id)
    }
  }
  return filings
}

function sortByDate(filings: any[]) {
  const dateKey = 'date'
  filings.sort(function (a, b) {
    if (a[dateKey] > b[dateKey]) {
      return -1
    } else if (a[dateKey] < b[dateKey]) {
      return 1
    } else {
      return 0
    }
  })
}

function combineAndTruncate(a: any[], b: any[], maxEntries: number): any[] {
  console.log('    Concatenating and removing duplicates...')
  let combined = union(a, b)
  console.log('    Sorting by date...')
  sortByDate(combined)

  return [...combined.slice(0, maxEntries)]
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let dateWhere = ''
  if (dateRange.hasStart()) {
    dateWhere += ` AND date >= $${params.length + 1}`
    params.push(dateRange.start)
  }
  if (dateRange.hasEnd()) {
    dateWhere += ` AND date < $${params.length + 1}`
    params.push(dateRange.end)
  }
  return dateWhere
}

function dateToString(value: Date): string {
  return (
    value.getFullYear() +
    '-' +
    ('0' + (value.getMonth() + 1)).slice(-2) +
    '-' +
    ('0' + value.getDate()).slice(-2) +
    ' ' +
    ('0' + value.getHours()).slice(-2) +
    ':' +
    ('0' + value.getMinutes()).slice(-2)
  )
}

function dateToUTCString(value: Date): string {
  return (
    value.getUTCFullYear() +
    '-' +
    ('0' + (value.getUTCMonth() + 1)).slice(-2) +
    '-' +
    ('0' + value.getUTCDate()).slice(-2) +
    ' ' +
    ('0' + value.getUTCHours()).slice(-2) +
    ':' +
    ('0' + value.getUTCMinutes()).slice(-2)
  )
}

function getSignatureFromLines(lines: string[], referenceID: string): string {
  // Certain lines occasionally appear beneath cable signatures. For example,
  // Ralph Frank's 04ZAGREB1483 cable has the line 'NNNN' two lines beneath
  // 'FRANK'. And cable 09BRUSSELS1647 contains a '.' line beneath the
  // signature 'Kennard'.
  const skipLines = [
    '"',
    '.',
    '...',
    ': NOT PASSED TO ABOVE ADDRESSEE(S)',
    '_____________________________________________ ____________________',
    '- 1 -',
    '=======================CABLE ENDS============================',
    '#',
    '^',
    '>',
    '/',
    '1',
    '2',
    '3',
    '4',
    '4.',
    '5',
    '6',
    '10',
    'A',
    'AND',
    'BRUSSELS 00000659 004 OF 004',
    'comment.',
    'Comment.',
    'CONFIDENTIAL',
    'D',
    'EC-LXI/APP_WP 3.2, APPENDIX A',
    'End Cable Text',
    'ND',
    'NNNN',
    'PAGE 04 HANOI 01882 070254Z',
    'RLAND',
    'SIPDIS',
    'UNCLASSIFIED',
    'UNCLASSIFIED 2',
    'UNCLASSIFIED 3',
  ]
  const skipBegins = [
    "“Visit Ankara's Classified Web Site at",
    '"Visit Ankara\'s Classified Web Site at',
    'gov.gov/wiki/Portal:Turkey"',
    'http://www.intelink.s gov.gov/wiki/',
    'http://www.state.sgov.gov/',
    'To view the entire SMART message, go to URL',
    "Visit Embassy Singapore's",
  ]

  if (signatureMap.hasOwnProperty(referenceID)) {
    return signatureMap[referenceID]
  }

  // In cables such as 10MANAMA15, the signature (i.e., ERELI) follows
  // quotes.
  //
  // In cables such as 10MADRID154 and 10KYIV190, the signature
  // (i.e., SOLOMONT and TEFFT) is at the end of the line after a period
  // rather than on its own line.
  //
  // The last line of cable 09MANAMA713 ends with '(BDF notetaker) ERELI',
  // and so we add support for splitting on a closing parenthesis.
  //
  // Cable 08MANAMA592 ends with numerous asterisks followed by ERELI.
  const splitStrings = ['XXX ', '." ', '.) ', '" ', '. ', ') ', '* ']

  const assumedMaxSignatureTokens = 3
  for (let i = lines.length - 1; i >= 0; i -= 1) {
    let line = lines[i].trim()
    if (line) {
      if (skipLines.includes(line)) {
        continue
      }

      let skipLine = false
      for (const skipBegin of skipBegins) {
        if (line.startsWith(skipBegin)) {
          skipLine = true
          break
        }
      }
      if (skipLine) {
        continue
      }

      // Cable 10KYIV241 ends with 'Comment.' and contains no signature.
      if (line == 'Comment.') {
        return ''
      }

      // In cables such as 10MANAMA52, the signature (i.e., ERELI) is at the
      // end of a line directly after 'End comment.'.
      const endCommentVariants = [
        'End comment.',
        'End Comment.',
        'END COMMENT.',
        'End note.)',
        'End text.',
        'End comment',
        'End Comment',
      ]
      for (const endComment of endCommentVariants) {
        if (line.includes(endComment)) {
          const tokens = line.split(endComment)
          const remainder = tokens[tokens.length - 1].trim()
          if (remainder.split(' ').length <= assumedMaxSignatureTokens) {
            return remainder
          } else {
            line = remainder
          }
        }
      }

      for (const splitString of splitStrings) {
        if (line.includes(splitString)) {
          const tokens = line.split(splitString)
          const remainder = tokens[tokens.length - 1].trim()
          if (remainder.split(' ').length <= assumedMaxSignatureTokens) {
            return remainder
          }
        }
      }

      // Cable 10KYIV275 is truncated just after its subject line and ends
      // with "Classified By: Ambassador Jo..."
      if (line.includes('Classified By: ')) {
        return ''
      }

      // Cable 10VIENNA176 has no signature -- though it does name a point of
      // contact for the TIP report -- and ends with
      // "Political Specialist FSN-11: 25 hours".
      if (line.includes('Political Specialist ')) {
        return ''
      }

      if (line.split(' ').length > assumedMaxSignatureTokens) {
        return ''
      }

      // Cable 09MANAMA711 ends with the line '#ERELI', and 08ISLAMABAD3837
      // with '"Patterson', so we strip out all pound signs and quotations and
      // then trim.
      return line.replace(/#/g, '').replace(/"/g, '').replace(/\\/g, '').trim()
    }
  }
  return ''
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

// In addition to the usual entity tagging functionality, we also force the
// returned date into its UTC string.
function annotateFiling(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  entityRefs: Map<string, any[]>,
  preambleTags: Map<string, string[]>,
  authorSpans: Map<string, any[]>
) {
  if (!filing) {
    return
  }

  // Apparently due to technicalities in the manner in which TypeORM interprets
  // Postgres timestamp columns, we preserve the original string via
  // dateToString rather than dateToUTCString.
  filing.date = dateToString(filing.date)

  const embassy = util.normalizeEntity(filing.embassy, entityNormalization)
  let annotation = {
    embassy: {
      origText: filing.embassy,
      text: embassy,
      stylizedText: util.getFullyStylizedName(embassy, entities),
      logo: util.logoURLFromAncestors(embassy, entities),
    },
    staffEntities: [],
    recipientEntities: [],
    manualEntities: [],
  }

  if (authorSpans.has(filing.embassy)) {
    for (const span of authorSpans.get(filing.embassy)) {
      const startDate = span.startDate
      const endDate = span.endDate
      const dateStr = filing.date
      if (dateStr >= startDate && dateStr < endDate) {
        const entity = span.entity

        let attributed = false
        const lines = filing.text.split('\n')
        const cableSignature = getSignatureFromLines(lines, filing.reference_id)
        if (!span.hasOwnProperty('signatures')) {
          console.log(
            `Did not have a signatures member in an author span for embassy ${filing.embassy}.`
          )
          continue
        }
        for (const signature of span.signatures) {
          if (cableSignature.toLowerCase() == signature.toLowerCase()) {
            attributed = true
          }
        }

        annotation.staffEntities.push({
          text: entity,
          stylizedText: util.getFullyStylizedName(entity, entities),
          logo: util.logoURLFromAncestors(entity, entities),
          attributed: attributed,
          note: attributed ? span.noteAttributed : span.noteUnattributed,
        })
      }
    }
  }

  const reference = filing.reference_id.toLowerCase()
  if (entityRefs.has(reference)) {
    for (const refObj of entityRefs.get(reference)) {
      const entity = util.isString(refObj) ? refObj : refObj.name
      const note = util.isString(refObj) ? undefined : refObj.note
      const attributed = util.isString(refObj) ? undefined : refObj.attributed
      annotation.manualEntities.push({
        text: entity,
        stylizedText: util.getFullyStylizedName(entity, entities),
        logo: util.logoURLFromAncestors(entity, entities),
        attributed: attributed,
        note: note,
      })
    }
  }

  for (let [entity, tags] of preambleTags) {
    // We avoid redundant inclusions of the embassy, especiall if it is named
    // in a 'FROM' line in the preamble.
    if (entity == embassy) {
      continue
    }

    for (const tag of tags) {
      const pattern = new RegExp(`\\b${tag}\\b`)
      if (filing.preamble.match(pattern)) {
        const stylizedText = util.getFullyStylizedName(entity, entities)
        const note = `Tech Inquiry automatically tagged ${stylizedText} in this cable because of the occurrence of '${tag}' in the preamble text.`

        annotation.recipientEntities.push({
          text: entity,
          stylizedText: stylizedText,
          logo: util.logoURLFromAncestors(entity, entities),
          note: note,
        })
        break
      }
    }
  }

  filing['annotation'] = annotation
}

function annotate(
  results,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  entityRefs: Map<string, any[]>,
  preambleTags: Map<string, string[]>,
  authorSpans: Map<string, any[]>
) {
  for (let filing of results.filings) {
    annotateFiling(
      filing,
      entities,
      entityNormalization,
      entityRefs,
      preambleTags,
      authorSpans
    )
  }
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  const queryStr = `
    SELECT 1 FROM ${kTableName} WHERE ${fullTextSearchString}
    @@ websearch_to_tsquery('simple', $${params.length + 1})
    ${dateWhere} LIMIT 1000`
  params.push(webSearch)

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function haveForEmbassiesReferencesOrTags(
  embassies: string[],
  references: string[],
  tags: string[],
  authorSpans: any[],
  dateRange: util.DateRange
): Promise<boolean> {
  if (!(dateRange.hasStart() || dateRange.hasEnd())) {
    return (
      embassies.length > 0 ||
      references.length > 0 ||
      tags.length > 0 ||
      authorSpans.length > 0
    )
  }

  const manager = dataSource.manager
  if (embassies.length) {
    let params = []
    const dateWhere = buildDateConstraint(dateRange, params)
    const queryStr = `
      SELECT 1 FROM ${kTableName} WHERE
      LOWER(embassy) = ANY($${params.length + 1})
      ${dateWhere} LIMIT 10000`
    params.push(embassies)
    const haveResult = await util.nonemptyQuery(manager, queryStr, params)
    if (haveResult) {
      return true
    }
  }
  if (references.length) {
    let params = []
    const dateWhere = buildDateConstraint(dateRange, params)
    const queryStr = `
      SELECT 1 FROM ${kTableName} WHERE
      LOWER(reference_id) = ANY($${params.length + 1})
      ${dateWhere} LIMIT 10000`
    params.push(references)
    const haveResult = await util.nonemptyQuery(manager, queryStr, params)
    if (haveResult) {
      return true
    }
  }
  for (const tag of tags) {
    let params = []
    const dateWhere = buildDateConstraint(dateRange, params)
    const queryStr = `
      SELECT 1 FROM ${kTableName} WHERE
      to_tsvector('simple', preamble) @@ phraseto_tsquery($${params.length + 1})
      ${dateWhere} LIMIT 10000`
    params.push(tag)
    const haveResult = await util.nonemptyQuery(manager, queryStr, params)
    if (haveResult) {
      return true
    }
  }
  for (const span of authorSpans) {
    const dateRange = new util.DateRange(
      util.nullableStringToDate(span.get('startDate')),
      util.nullableStringToDate(span.get('endDate'))
    )

    let params = []
    const dateWhere = buildDateConstraint(dateRange, params)
    const queryStr = `
      SELECT 1 FROM ${kTableName} WHERE
      lower(embassy) = $${params.length + 1}
      ${dateWhere} LIMIT 10000`
    params.push(span.get('source').toLowerCase())
    const haveResult = await util.nonemptyQuery(manager, queryStr, params)
    if (haveResult) {
      return true
    }
  }

  return false
}

async function haveForEntity(
  name: string,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  dateRange: util.DateRange
): Promise<boolean> {
  const embassies = getEmbassies(name, entities)
  const references = getReferences(name, entities, entityChildren)
  const tags = getTags(name, entities, entityChildren, true)
  const spans = getSpans(name, entities)
  return haveForEmbassiesReferencesOrTags(
    embassies,
    references,
    tags,
    spans,
    dateRange
  )
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  if (dateRange.hasStart()) {
    query = query.andWhere('date >= :startDate', {
      startDate: dateRange.start,
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('date < :endDate', { endDate: dateRange.end })
  }
  query = query.orderBy('date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['reference_id']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function getForEmbassiesReferencesOrTags(
  embassies: string[],
  references: string[],
  tags: string[],
  spans: any[],
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  if (!(embassies.length || references.length || tags.length || spans.length)) {
    return { filings: [], hitSearchCap: false }
  }
  let params = []

  let manager = dataSource.manager
  let results = { filings: [], hitSearchCap: false }
  try {
    let subqueries = []
    let subqueryParamsList = []
    if (embassies.length) {
      let params = []
      const dateWhere = buildDateConstraint(dateRange, params)
      subqueries.push(
        `SELECT * FROM ${kTableName} WHERE
        LOWER(embassy) = ANY($${params.length + 1})
        ${dateWhere} ORDER BY date DESC LIMIT $${params.length + 2}`
      )
      params.push(embassies)
      params.push(maxSearchResults)
      subqueryParamsList.push(params)
    }
    if (references.length) {
      let params = []
      const dateWhere = buildDateConstraint(dateRange, params)
      subqueries.push(
        `SELECT * FROM ${kTableName} WHERE
        LOWER(reference_id) = ANY($${params.length + 1})
        ${dateWhere} ORDER BY date DESC LIMIT $${params.length + 2}`
      )
      params.push(references)
      params.push(maxSearchResults)
      subqueryParamsList.push(params)
    }
    for (const tag of tags) {
      let params = []
      const dateWhere = buildDateConstraint(dateRange, params)
      subqueries.push(
        `SELECT * FROM ${kTableName} WHERE
        to_tsvector('simple', preamble) @@ phraseto_tsquery($${
          params.length + 1
        })
        ${dateWhere} ORDER BY date DESC LIMIT $${params.length + 2}`
      )
      params.push(tag)
      params.push(maxSearchResults)
      subqueryParamsList.push(params)
    }
    for (const span of spans) {
      const dateRange = new util.DateRange(
        util.nullableStringToDate(span.get('startDate')),
        util.nullableStringToDate(span.get('endDate'))
      )

      let params = []
      const dateWhere = buildDateConstraint(dateRange, params)
      subqueries.push(
        `SELECT * FROM ${kTableName} WHERE
        lower(embassy) = $${params.length + 1}
        ${dateWhere} ORDER BY date DESC LIMIT $${params.length + 2}`
      )
      params.push(span.get('source').toLowerCase())
      params.push(maxSearchResults)
      subqueryParamsList.push(params)
    }

    let filings = []
    for (let index = 0; index < subqueries.length; ++index) {
      const subquery = subqueries[index]
      const subqueryParams = subqueryParamsList[index]
      if (index == 0) {
        filings = await manager.query(subquery, subqueryParams)
      } else {
        console.log(`  Subquery ${index} of ${subqueries.length}.`)
        console.log(subquery)
        console.log(subqueryParams)
        const additionalFilings = await manager.query(subquery, subqueryParams)
        filings = combineAndTruncate(
          filings,
          additionalFilings,
          maxSearchResults
        )
      }
    }
    console.log('Finished cablegate lookups.')
    results = {
      filings: filings,
      hitSearchCap: filings.length == maxSearchResults,
    }
  } catch (err) {
    console.log(err)
  }
  return results
}

async function getForName(
  name: string,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  if (
    name == 'government of the united states' ||
    name == 'united states department of state' ||
    name == 'united states embassies'
  ) {
    return getOverDateRange(dateRange, maxSearchResults)
  }

  const embassies = getEmbassies(name, entities)
  const references = getReferences(name, entities, entityChildren)
  const tags = getTags(name, entities, entityChildren, true)
  const spans = getSpans(name, entities)
  return getForEmbassiesReferencesOrTags(
    embassies,
    references,
    tags,
    spans,
    dateRange,
    maxSearchResults
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const whereStr = dateWhere ? `WHERE ${dateWhere}` : ''

  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  if (dateRange.hasStart()) {
    query = query.andWhere('date >= :startDate', {
      startDate: dateRange.start,
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('date < :endDate', { endDate: dateRange.end })
  }
  query = query.orderBy('date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getCable(referenceID: string) {
  let params = []
  const queryStr = `
    SELECT * FROM ${kTableName} WHERE
    LOWER(reference_id) = $${params.length + 1}
    LIMIT 1`
  params.push(referenceID.toLowerCase())
  const filings = await dataSource.manager.query(queryStr, params)
  return { filings: filings, hitSearchCap: false }
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (params.entity) {
    const name = util.canonicalText(params.entity)
    console.log(`Have Cablegate w/ entity: ${name}`)
    result = await haveForEntity(name, entities, entityChildren, dateRange)
  } else if (text) {
    console.log(`Have Cablegate w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have Cablegate w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else {
    console.log('Have Cablegate over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getCableAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  entityRefs: Map<string, any[]>,
  preambleTags: Map<string, string[]>,
  authorSpans: Map<string, any[]>
) {
  const params = request.query

  const referenceID = params.reference_id
  let result = await getCable(referenceID)
  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(
      result,
      entities,
      entityNormalization,
      entityRefs,
      preambleTags,
      authorSpans
    )
  }
  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityNormalization: Map<string, string>,
  entityRefs: Map<string, any[]>,
  preambleTags: Map<string, string[]>,
  authorSpans: Map<string, any[]>
) {
  const params = request.query

  const maxSearchResultsDefault = 2500
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (params.entity) {
    const name = util.canonicalText(params.entity)
    console.log(`Cablegate w/ entity: ${name}`)
    result = await getForName(
      name,
      entities,
      entityChildren,
      dateRange,
      maxSearchResults
    )
  } else if (text) {
    console.log(`Cablegate w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`Cablegate w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else {
    console.log('Cablegate over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(
      result,
      entities,
      entityNormalization,
      entityRefs,
      preambleTags,
      authorSpans
    )
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const parentRoute = '/api/us/central'

  const feed = entityState.feeds.get('us').get('central').get('cablegate')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether results exist for a web search or entity.
  router.get(
    `${parentRoute}/haveCablegate`,
    async function (request, response) {
      await haveAPI(request, response, entities, children)
    }
  )

  // Returns JSON for a web search or entity lookup.
  router.get(`${parentRoute}/cablegate`, async function (request, response) {
    await getAPI(
      request,
      response,
      entities,
      children,
      feed.embassyNormalization,
      feed.entityRefs,
      feed.preambleTags,
      feed.authorSpans
    )
  })

  // Returns JSON for a particular cable.
  router.get(
    `${parentRoute}/cablegate/cable`,
    async function (request, response) {
      await getCableAPI(
        request,
        response,
        entities,
        feed.embassyNormalization,
        feed.entityRefs,
        feed.preambleTags,
        feed.authorSpans
      )
    }
  )

  router.post(`${parentRoute}/cablegate/getTags`, handleGetTags)
  router.post(`${parentRoute}/cablegate/addTag`, handleAddTag)
  router.post(`${parentRoute}/cablegate/deleteTag`, handleDeleteTag)
}
