import { dataSource, getUsernames } from '../../../../App'
import * as tags from '../../../../tags'
import * as util from '../../../../util'

import { FeedInfo } from '../../../../EntityState'

import { USCentralFECIndividual } from '../../../../entity/us/central/lobbying/fec/individual'

const kIndvTable = USCentralFECIndividual
const kIndvTableName = 'us_fec_indv'
const kCandTableName = 'us_fec_cand'
const kCommTableName = 'us_fec_comm'

const kTagDataset = 'us_fec_indv'

// The concatenation of fields from a row of the us_fec_indv Postgres table to
// match with full-text searches.
const fullTextIndvSearchString = `
  to_tsvector('simple',
    coalesce(contributor_name,        '') || ' ' ||
    coalesce(lower(city),             '') || ' ' ||
    coalesce(employer,                '') || ' ' ||
    coalesce(lower(occupation),       '') || ' ' ||
    coalesce(lower(transaction_id) ,  '') || ' ' ||
    coalesce(file_number::TEXT,       '') || ' ' ||
    coalesce(memo_text,               '') || ' ' ||
    coalesce(fec_record_number::TEXT, '') || ' ' ||
    coalesce(entities::TEXT,          '')
  )`

const fullTextCandSearchString = `
  to_tsvector('simple',
    coalesce(lower(candidate_id),                           '') || ' ' ||
    coalesce(candidate_name,                                '') || ' ' ||
    coalesce(election_year::TEXT,                           '') || ' ' ||
    coalesce(candidate_office_state,                        '') || ' ' ||
    coalesce(lower(candidate_principal_campaign_committee), '') || ' ' ||
    coalesce(candidate_street1,                             '') || ' ' ||
    coalesce(candidate_street2,                             '') || ' ' ||
    coalesce(candidate_city,                                '') || ' ' ||
    coalesce(candidate_state,                               '') || ' ' ||
    coalesce(candidate_zip,                                 '')
  )`

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`transaction_date >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`transaction_date <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('transaction_date >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('transaction_date <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

function normalizeCandidate(
  name: string,
  normalization: Map<string, string>
): string {
  if (!name) {
    return ''
  }

  const canonicalName: string = util.canonicalText(name)
  const key: string = JSON.stringify({ candidate: canonicalName })
  if (normalization.has(key)) {
    return normalization.get(key)
  }

  return canonicalName
}

function expandNames(entityList: string[], entities, entityAlternates): any[] {
  const feedPath = ['feeds', 'us', 'central', 'lobbying', 'fec']
  let nameSet = new Set<string>()
  for (const v of entityList) {
    const feedInfo = entities.has(v)
      ? util.jsonGetFromPath(entities.get(v), feedPath)
      : undefined
    if (feedInfo == undefined || !feedInfo.get('blockName')) {
      nameSet.add(JSON.stringify({ name: v }))
    }
    if (entityAlternates.has(v)) {
      for (const alternate of entityAlternates.get(v)) {
        nameSet.add(alternate)
      }
    }
    if (feedInfo?.has('includes')) {
      for (const alternate of feedInfo.get('includes')) {
        nameSet.add(
          JSON.stringify(
            util.isString(alternate)
              ? { name: alternate }
              : util.mapToObject(alternate)
          )
        )
      }
    }
  }
  return Array.from(nameSet.values()).map((blob) => JSON.parse(blob))
}

function getQueryTargets(targets: any[]): string[] {
  if (!targets) {
    return []
  }

  let queryTargets = []
  const keyOrder = ['candidate', 'name', 'city', 'employer', 'contributor']
  for (const target of targets) {
    let targetObj = {}
    for (const key of keyOrder) {
      if (target[key] != undefined) {
        targetObj[key] = target[key]
      }
    }
    queryTargets.push(JSON.stringify([targetObj]))
  }
  return queryTargets
}

function normalizeEmployer(
  name: string,
  normalization: Map<string, string>
): string {
  if (!name) {
    return ''
  }
  const canonicalName: string = util.canonicalText(name)
  const key = JSON.stringify({ employer: canonicalName })

  if (normalization.has(key)) {
    return normalization.get(key)
  }

  return canonicalName
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateContribution(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>
): void {
  const contributor = util.normalizeEntityWithCityAndEmployer(
    filing.contributor_name,
    filing.city,
    filing.state,
    filing.employer,
    entityNormalization
  )
  const employer = normalizeEmployer(filing.employer, entityNormalization)
  const candidate = normalizeCandidate(
    filing.candidate_name,
    entityNormalization
  )
  const committee = util.normalizeEntityWithEmployer(
    filing.committee_name,
    undefined,
    entityNormalization
  )
  const treasurer = util.normalizeEntityWithEmployer(
    filing.treasurer_name,
    undefined,
    entityNormalization
  )
  const connectedOrganization = util.normalizeEntityWithEmployer(
    filing.connected_organization_name,
    undefined,
    entityNormalization
  )

  function buildAnnotation(origName: string, normalizedName: string) {
    return {
      origText: origName ? origName : '',
      text: normalizedName,
      stylizedText: util.getFullyStylizedName(normalizedName, entities),
      logo: util.logoURLFromAncestors(normalizedName, entities),
      url:
        normalizedName in entities
          ? util.encodeEntity(normalizedName)
          : util.encodeSearch(normalizedName),
    }
  }

  const annotation = {
    contributor: buildAnnotation(filing.contributor_name, contributor),
    employer: buildAnnotation(filing.employer, employer),
    candidate: buildAnnotation(filing.candidate_name, candidate),
    committee: buildAnnotation(filing.committee_name, committee),
    treasurer: buildAnnotation(filing.treasurer_name, treasurer),
    connectedOrganization: buildAnnotation(
      filing.connected_organization_name,
      connectedOrganization
    ),
  }

  filing['annotation'] = annotation
}

function annotate(
  contributions,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>
) {
  for (let filing of contributions.filings) {
    annotateContribution(filing, entities, entityNormalization)
  }
}

async function haveForNames(
  targets: any[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  maxTargetsPerQuery = 10
): Promise<boolean> {
  for (const target of targets) {
    if ('employer' in target) {
      continue
    }
    const name = target['name']
    if (
      util.canAssumeRecordsExistForName(name, dateRange, entities, [
        'feeds',
        'us',
        'central',
        'lobbying',
        'fec',
      ])
    ) {
      return true
    }
  }

  let baseParams = []
  const dateWhere = buildDateConstraint(dateRange, baseParams)
  const dateConstraint = dateWhere ? `AND ${dateWhere}` : ''
  const paramOffset = baseParams.length + 1

  const header = `SELECT 1 FROM ${kIndvTableName} WHERE `

  const queryTargets = getQueryTargets(targets)

  // NOTE: When this limit was only 1000, it seems to have exposed a
  // performance bug in Postgres for entities such as Ted Cruz.
  const footer = `${dateConstraint} LIMIT 20000`

  if (queryTargets.length > maxTargetsPerQuery) {
    console.log(
      `Breaking up ${queryTargets.length} query targets into chunks of ${maxTargetsPerQuery} for FEC entities existence check.`
    )
  }
  let manager = dataSource.manager
  for (
    let sliceBeg = 0;
    sliceBeg < queryTargets.length;
    sliceBeg += maxTargetsPerQuery
  ) {
    const sliceEnd = Math.min(
      sliceBeg + maxTargetsPerQuery,
      queryTargets.length
    )
    if (queryTargets.length > maxTargetsPerQuery) {
      console.log(`  Performing FEC check over [${sliceBeg}, ${sliceEnd}).`)
    }
    const params = [...baseParams, queryTargets.slice(sliceBeg, sliceEnd)]
    const query =
      header + `entities @> ANY(cast($${paramOffset} as jsonb[]))` + footer
    const haveResult = await util.nonemptyQuery(manager, query, params)
    if (haveResult) {
      return true
    }
  }
  return false
}

async function haveForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const targets = expandNames(names, entities, entityAlternates)
  return await haveForNames(targets, dateRange, entities)
}

function sortByDate(filings: any[]) {
  const dateKey = 'transaction_date'
  filings.sort(function (a, b) {
    if (a[dateKey] > b[dateKey]) {
      return -1
    } else if (a[dateKey] < b[dateKey]) {
      return 1
    } else {
      return 0
    }
  })
}

function union(a: any[], b: any[]): any[] {
  let filings = []
  let idSet = new Set<number>()
  for (const filing of a) {
    filings.push({ ...filing })
    idSet.add(filing.transaction_id)
  }
  for (const filing of b) {
    if (!idSet.has(filing.transaction_id)) {
      filings.push({ ...filing })
      idSet.add(filing.transaction_id)
    }
  }
  return filings
}

function combineAndTruncate(a: any[], b: any[], maxEntries: number): any[] {
  console.log('    Concatenating and removing duplicates...')
  let combined = union(a, b)
  console.log('    Sorting by date...')
  sortByDate(combined)

  return [...combined.slice(0, maxEntries)]
}

async function getForNames(
  targets: any[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  maxTargetsPerQuery = 20
) {
  let params = []

  const unsortedLimit = Math.max(100000, 2 * maxSearchResults)

  const dateWhere = buildDateConstraint(dateRange, params)
  const dateConstraint = dateWhere ? `AND ${dateWhere}` : ''

  const queryTargets = getQueryTargets(targets)

  const queryStr = `SELECT
    DISTINCT(indv.fec_record_number),
    indv.filer_id,
    indv.amendment_indicator,
    indv.report_type,
    indv.primary_general_indicator,
    indv.transaction_type,
    indv.entity_type,
    indv.contributor_name,
    indv.city,
    indv.state,
    indv.zip,
    indv.employer,
    indv.occupation,
    indv.transaction_date,
    indv.transaction_amount,
    indv.memo_text,
    indv.cycle,
    cand.candidate_id,
    cand.candidate_name,
    cand.candidate_party_affiliation,
    cand.election_year,
    cand.candidate_office_state,
    cand.candidate_office,
    cand.candidate_office_district,
    cand.candidate_incumbant_status,
    cand.candidate_status,
    cand.candidate_street1,
    cand.candidate_street2,
    cand.candidate_city,
    cand.candidate_state,
    cand.candidate_zip,
    comm.committee_name,
    comm.treasurer_name,
    comm.committee_street1,
    comm.committee_street2,
    comm.committee_city,
    comm.committee_state,
    comm.committee_zip,
    comm.committee_designation,
    comm.committee_type,
    comm.committee_party_affiliation,
    comm.committee_filing_frequency,
    comm.interest_group_category,
    comm.connected_organization_name
  FROM (
    SELECT * FROM (
      SELECT * FROM us_fec_indv WHERE
        entities @> ANY(cast($${params.length + 1} as jsonb[]))
        ${dateConstraint}
      LIMIT $${params.length + 2}
    ) s ORDER BY transaction_date DESC LIMIT $${params.length + 3}
  ) as indv
    LEFT JOIN us_fec_cand cand ON
      (indv.filer_id, indv.cycle) = (cand.candidate_principal_campaign_committee, cand.cycle)
    LEFT JOIN us_fec_comm comm ON
      (indv.filer_id, indv.cycle) = (comm.committee_id, comm.cycle)
    ORDER BY transaction_date DESC`

  const maxTargetsPerQuery = 20
  let manager = dataSource.manager
  let result = { filings: [], hitSearchCap: false }
  let subqueryParamsList = []
  for (
    let sliceBeg = 0;
    sliceBeg < queryTargets.length;
    sliceBeg += maxTargetsPerQuery
  ) {
    const sliceEnd = Math.min(
      sliceBeg + maxTargetsPerQuery,
      queryTargets.length
    )
    if (queryTargets.length > maxTargetsPerQuery) {
      console.log(`  Performing query over [${sliceBeg}, ${sliceEnd}).`)
    }
    let subqueryParams = [...params]
    subqueryParams.push(queryTargets.slice(sliceBeg, sliceEnd))
    subqueryParams.push(unsortedLimit)
    subqueryParams.push(maxSearchResults)
    subqueryParamsList.push(subqueryParams)
  }

  try {
    let filings = []
    for (let index = 0; index < subqueryParamsList.length; ++index) {
      const subquery = queryStr
      const subqueryParams = subqueryParamsList[index]
      if (index == 0) {
        filings = await manager.query(subquery, subqueryParams)
      } else {
        console.log(`  Subquery ${index} of ${subqueryParamsList.length}.`)
        const additionalFilings = await manager.query(subquery, subqueryParams)
        filings = combineAndTruncate(
          filings,
          additionalFilings,
          maxSearchResults
        )
      }
    }
    result = {
      filings: filings,
      hitSearchCap: filings.length == maxSearchResults,
    }
  } catch (err) {
    console.log(err)
  }

  return result
}

async function getForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  maxSearchResults: number
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const targets = expandNames(names, entities, entityAlternates)

  return await getForNames(targets, dateRange, maxSearchResults)
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const dateConstraint = dateWhere ? `AND ${dateWhere}` : ''

  const queryStrings = [
    `SELECT 1 FROM ${kIndvTableName} WHERE ${fullTextIndvSearchString}
     @@ websearch_to_tsquery('simple', $${params.length + 1})
     ${dateConstraint} LIMIT 1000`,
    `SELECT 1 FROM ${kCandTableName} WHERE ${fullTextCandSearchString}
     @@ websearch_to_tsquery('simple', $${params.length + 1})
     ${dateConstraint} LIMIT 1000`,
  ]
  params.push(webSearch)

  let manager = dataSource.manager
  for (const queryStr of queryStrings) {
    const haveResult = await util.nonemptyQuery(manager, queryStr, params)
    if (haveResult) {
      return true
    }
  }

  return false
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const dateConstraint = dateWhere ? `AND ${dateWhere}` : ''

  const unsortedLimit = Math.max(100000, 2 * maxSearchResults)

  let indvQueryStr = `SELECT
    DISTINCT(indv.fec_record_number),
    indv.filer_id,
    indv.amendment_indicator,
    indv.report_type,
    indv.primary_general_indicator,
    indv.transaction_type,
    indv.entity_type,
    indv.contributor_name,
    indv.city,
    indv.state,
    indv.zip,
    indv.employer,
    indv.occupation,
    indv.transaction_date,
    indv.transaction_amount,
    indv.memo_text,
    indv.cycle,
    cand.candidate_id,
    cand.candidate_name,
    cand.candidate_party_affiliation,
    cand.election_year,
    cand.candidate_office_state,
    cand.candidate_office,
    cand.candidate_office_district,
    cand.candidate_incumbant_status,
    cand.candidate_status,
    cand.candidate_street1,
    cand.candidate_street2,
    cand.candidate_city,
    cand.candidate_state,
    cand.candidate_zip,
    comm.committee_name,
    comm.treasurer_name,
    comm.committee_street1,
    comm.committee_street2,
    comm.committee_city,
    comm.committee_state,
    comm.committee_zip,
    comm.committee_designation,
    comm.committee_type,
    comm.committee_party_affiliation,
    comm.committee_filing_frequency,
    comm.interest_group_category,
    comm.connected_organization_name
  FROM (
    SELECT * FROM (
      SELECT * FROM us_fec_indv WHERE
        ${fullTextIndvSearchString} @@ websearch_to_tsquery(
          'simple', $${params.length + 1})
        ${dateConstraint}
      LIMIT $${params.length + 2}
    ) s ORDER BY transaction_date DESC LIMIT $${params.length + 3}
  ) as indv
    LEFT JOIN us_fec_cand cand ON
      (indv.filer_id, indv.cycle) = (cand.candidate_principal_campaign_committee, cand.cycle)
    LEFT JOIN us_fec_comm comm ON
      (indv.filer_id, indv.cycle) = (comm.committee_id, comm.cycle)
    ORDER BY transaction_date DESC`

  const candQueryStr = `SELECT
    DISTINCT(indv.fec_record_number),
    indv.filer_id,
    indv.amendment_indicator,
    indv.report_type,
    indv.primary_general_indicator,
    indv.transaction_type,
    indv.entity_type,
    indv.contributor_name,
    indv.city,
    indv.state,
    indv.zip,
    indv.employer,
    indv.occupation,
    indv.transaction_date,
    indv.transaction_amount,
    indv.memo_text,
    indv.cycle,
    cand.candidate_id,
    cand.candidate_name,
    cand.candidate_party_affiliation,
    cand.election_year,
    cand.candidate_office_state,
    cand.candidate_office,
    cand.candidate_office_district,
    cand.candidate_incumbant_status,
    cand.candidate_status,
    cand.candidate_street1,
    cand.candidate_street2,
    cand.candidate_city,
    cand.candidate_state,
    cand.candidate_zip,
    comm.committee_name,
    comm.treasurer_name,
    comm.committee_street1,
    comm.committee_street2,
    comm.committee_city,
    comm.committee_state,
    comm.committee_zip,
    comm.committee_designation,
    comm.committee_type,
    comm.committee_party_affiliation,
    comm.committee_filing_frequency,
    comm.interest_group_category,
    comm.connected_organization_name
  FROM (
    SELECT * FROM (
      SELECT * FROM us_fec_cand WHERE
        ${fullTextCandSearchString} @@ websearch_to_tsquery(
          'simple', $${params.length + 1})
        ${dateConstraint}
      LIMIT $${params.length + 2}
    ) s LIMIT $${params.length + 3}
  ) as cand
    LEFT JOIN us_fec_indv indv ON
      (cand.candidate_principal_campaign_committee, cand.cycle) = (indv.filer_id, indv.cycle)
    LEFT JOIN us_fec_comm comm ON
      (cand.candidate_principal_campaign_committee, cand.cycle) = (comm.committee_id, comm.cycle)
    ORDER BY transaction_date DESC`
  const queryStr = `SELECT * FROM (
    (${indvQueryStr}) UNION (${candQueryStr})
  ) q ORDER BY transaction_date DESC LIMIT $${params.length + 3}`

  params.push(webSearch)
  params.push(unsortedLimit)
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const usernames = getUsernames(token)

  // TODO(Jack Poulson): Improve the SQL query to filter on the candidate_id
  // so that there is no need to do so manually afterwards.

  // Because the below SQL query does not enforce a filter on the candidate_id
  // due to the LEFT JOIN, we grab the list of tags and manually filter the
  // results on the condition that they match a tag.
  let fecRecordTags = {}
  {
    let params = []
    const queryStr = `SELECT * FROM tags WHERE
      dataset = $${params.length + 1} AND username = ANY($${
      params.length + 2
    }) AND
      LOWER(tag) @@ websearch_to_tsquery($${params.length + 3})`
    params.push(kTagDataset)
    params.push(usernames)
    params.push(webSearch)
    const tags = await dataSource.manager.query(queryStr, params)
    for (const tag of tags) {
      const uniqueData = tag['unique_data']
      const recordNumber = uniqueData['fec_record_number']
      const filerID = uniqueData['filer_id']
      const cycle = uniqueData['cycle']
      const candidateID = uniqueData['candidate_id']
      if (!fecRecordTags.hasOwnProperty(recordNumber)) {
        fecRecordTags[recordNumber] = []
      }
      fecRecordTags[recordNumber].push({
        filerID: filerID,
        cycle: cycle,
        candidateID: candidateID,
      })
    }
  }

  let params = []

  const queryStr = `SELECT
    DISTINCT(indv.fec_record_number),
    indv.filer_id,
    indv.amendment_indicator,
    indv.report_type,
    indv.primary_general_indicator,
    indv.transaction_type,
    indv.entity_type,
    indv.contributor_name,
    indv.city,
    indv.state,
    indv.zip,
    indv.employer,
    indv.occupation,
    indv.transaction_date,
    indv.transaction_amount,
    indv.memo_text,
    indv.cycle,
    cand.candidate_id,
    cand.candidate_name,
    cand.candidate_party_affiliation,
    cand.election_year,
    cand.candidate_office_state,
    cand.candidate_office,
    cand.candidate_office_district,
    cand.candidate_incumbant_status,
    cand.candidate_status,
    cand.candidate_street1,
    cand.candidate_street2,
    cand.candidate_city,
    cand.candidate_state,
    cand.candidate_zip,
    comm.committee_name,
    comm.treasurer_name,
    comm.committee_street1,
    comm.committee_street2,
    comm.committee_city,
    comm.committee_state,
    comm.committee_zip,
    comm.committee_designation,
    comm.committee_type,
    comm.committee_party_affiliation,
    comm.committee_filing_frequency,
    comm.interest_group_category,
    comm.connected_organization_name
  FROM (
    SELECT * FROM (
      SELECT * FROM us_fec_indv WHERE ROW(filer_id, fec_record_number, cycle) IN (
        SELECT
          unique_data->>'filer_id',
          (unique_data->>'fec_record_number')::NUMERIC(19),
          (unique_data->>'cycle')::INT FROM tags
          WHERE dataset = $${params.length + 1}
            AND username = ANY($${params.length + 2})
            AND LOWER(tag) @@ websearch_to_tsquery($${params.length + 3})
      ) LIMIT $${params.length + 4}
    ) s ORDER BY transaction_date DESC LIMIT $${params.length + 5}
  ) as indv
    LEFT JOIN us_fec_cand cand ON
      (indv.filer_id, indv.cycle) = (cand.candidate_principal_campaign_committee, cand.cycle)
    LEFT JOIN us_fec_comm comm ON
      (indv.filer_id, indv.cycle) = (comm.committee_id, comm.cycle)
    ORDER BY transaction_date DESC`
  params.push(kTagDataset)
  params.push(usernames)
  params.push(webSearch)
  params.push(maxSearchResults)
  params.push(maxSearchResults)

  const initialResults = await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )

  // Perform the post-processing to add the missing candidate ID filter.
  let results = { filings: [], hitSearchCap: initialResults['hitSearchCap'] }
  for (const filing of initialResults.filings) {
    for (const tagData of fecRecordTags[filing.fec_record_number]) {
      if (
        filing.filer_id == tagData['filerID'] &&
        filing.cycle == tagData['cycle'] &&
        filing.candidate_id == tagData['candidateID']
      ) {
        results.filings.push(filing)
        break
      }
    }
  }

  return results
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const whereStr = dateWhere ? `WHERE ${dateWhere}` : ''

  const queryStr = `SELECT 1 FROM ${kIndvTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  let query = dataSource.getRepository(kIndvTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('transaction_date', 'DESC').limit(maxSearchResults)
  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Have US FEC individual contribution w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have US FEC individual contribution w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Have US FEC individual contribution w/ entity: ${name}`)
    result = await haveForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names
    )
  } else {
    console.log('Have US FEC individiual contribution over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const maxSearchResultsDefault = 2000
  const maxSearchResultsCap = 5000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`US FEC individual contributions w/ text: ${text}`)
    result = await getFromSearch(text, dateRange, maxSearchResults)
  } else if (tagText) {
    console.log(`US FEC individual contributions w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`US FEC individual contributions w/ entity: ${name}`)
    result = await getForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names,
      maxSearchResults
    )
  } else {
    console.log('US FEC individual contributions over date range')
    result = await getOverDateRange(dateRange, maxSearchResults)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, feedInfo.normalization)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/us/central/lobbying/fec/individual'

  const feed = entityState.feeds
    .get('us')
    .get('central')
    .get('lobbying')
    .get('fec')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether individual contributions exist.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, feed)
  })

  // Returns JSON for individual contributions.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(request, response, entities, children, feed)
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
