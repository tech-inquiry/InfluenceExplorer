import { Brackets } from 'typeorm'

import { dataSource } from '../../../../App'
import { USCentralOPRContributions } from '../../../../entity/us/central/lobbying/opr/contributions'
import { USCentralOPRFeedInfo } from '../../../../EntityState'
import * as lobbyingUtil from './util'
import * as tags from '../../../../tags'
import * as util from '../../../../util'

const kTable = USCentralOPRContributions
const kTableName = 'us_opr_contributions_filing'

const kTagDataset = 'us_opr_contributions'

// The concatenation of fields from a row of the
// us_opr_contributions_filing Postgres table to match with full-text
// searches.
const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(filing_uuid)',
  'lower(contact_name)',
  'lower(comments)',
  'lower(address_1)',
  'lower(address_2)',
  'lower(city)',
  'lower(state_display)',
  'lower(zip)',
  'lower(country_display)',
  'lower(registrant::TEXT)',
  'lower(lobbyist::TEXT)',
  'lower(pacs::TEXT)',
  'lower(contribution_items::TEXT)',
])

function maxPeriods(month: number): string[] {
  let periods = ['mid_year']
  if (month >= 6) {
    periods.push('year_end')
  }
  return periods
}

function minPeriods(month: number): string[] {
  let periods = ['year_end']
  if (month < 6) {
    periods.push('mid_year')
  }
  return periods
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let lines = []
  if (dateRange.hasStart()) {
    const startMonth = dateRange.start.getMonth()
    const startYear = dateRange.start.getFullYear()
    if (startMonth >= 6) {
      const startPeriods = minPeriods(startMonth)
      lines.push(`
        dt_posted >= $${params.length + 1} AND
        (filing_year > $${params.length + 2} OR
        (filing_year = $${params.length + 2} AND
        filing_period = ANY($${params.length + 3})))`)
      params.push(dateRange.start)
      params.push(startYear)
      params.push(startPeriods)
    } else {
      lines.push(`
        dt_posted >= $${params.length + 1} AND
        filing_year >= $${params.length + 2}`)
      params.push(dateRange.start)
      params.push(startYear)
    }
  }
  if (dateRange.hasEnd()) {
    const endMonth = dateRange.end.getMonth()
    const endYear = dateRange.end.getFullYear()
    if (endMonth < 6) {
      const endPeriods = maxPeriods(endMonth)
      lines.push(`
        filing_year < $${params.length + 1} OR
        (filing_year = $${params.length + 1} AND
        filing_period = ANY($${params.length + 2}))`)
      params.push(endYear)
      params.push(endPeriods)
    } else {
      lines.push(`filing_year <= $${params.length + 1}`)
      params.push(endYear)
    }
  }
  if (lines.length) {
    return `(${lines.join(' AND ')})`
  } else {
    return ''
  }
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    const startMonth = dateRange.start.getMonth()
    const startYear = dateRange.start.getFullYear()

    const dateYearMatch = function (qb) {
      if (startMonth >= 6) {
        const periods = minPeriods(startMonth)
        const innerDateMatch = function (qbInner) {
          return qbInner
            .where('filing_year = :startYear', { startYear: startYear })
            .andWhere('filing_period = ANY(ARRAY[:...periods])', {
              periods: periods,
            })
        }
        return qb
          .where('filing_year > :startYear', { startYear: startYear })
          .orWhere(new Brackets(innerDateMatch))
      } else {
        return qb.where('filing_year >= :startYear', { startYear: startYear })
      }
    }

    query = query
      .andWhere('dt_posted >= :startDate', { startDate: dateRange.start })
      .andWhere(new Brackets(dateYearMatch))
  }
  if (dateRange.hasEnd()) {
    const endMonth = dateRange.end.getMonth()
    const endYear = dateRange.end.getFullYear()

    const dateYearMatch = function (qb) {
      if (endMonth < 6) {
        const periods = maxPeriods(endMonth)
        const innerDateMatch = function (qbInner) {
          return qbInner
            .where('filing_year = :endYear', { endYear: endYear })
            .andWhere('filing_period = ANY(ARRAY[:...periods])', {
              periods: periods,
            })
        }
        return qb
          .where('filing_year < :endYear', { endYear: endYear })
          .orWhere(new Brackets(innerDateMatch))
      } else {
        return qb.where('filing_year <= :endYear', { endYear: endYear })
      }
    }

    query = query.andWhere(new Brackets(dateYearMatch))
  }
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateContribution(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  lobbyistIDNormalization: Map<number, string>
): void {
  const registrant = util.normalizeEntity(
    filing.registrant['name'],
    entityNormalization
  )
  let annotation = {
    registrant: {
      origText: filing.registrant['name'],
      text: registrant,
      stylizedText: util.getFullyStylizedName(registrant, entities),
      logo: util.logoURLFromAncestors(registrant, entities),
      url:
        registrant in entities
          ? util.encodeEntity(registrant)
          : util.encodeSearch(registrant),
    },
  }

  if (filing.registrant['contact_name']) {
    const contact = util.normalizeEntity(
      filing.registrant.contact_name,
      entityNormalization
    )
    annotation['contact'] = {
      origText: filing.registrant.contact_name,
      text: contact,
      stylizedText: util.getFullyStylizedName(contact, entities),
      logo: util.logoURLFromAncestors(contact, entities),
    }
  }

  annotation['lobbyist'] = {}
  if (filing.lobbyist?.['id']) {
    const id = filing.lobbyist['id']
    if (id in lobbyistIDNormalization) {
      let name = lobbyistIDNormalization[id]
      annotation['lobbyist'] = {
        origText: util.usLobbyistNameFromObject(filing.lobbyist),
        text: name,
        stylizedText: util.getFullyStylizedName(name, entities),
        logo: util.logoURLFromAncestors(name, entities),
        url:
          name in entities ? util.encodeEntity(name) : util.encodeSearch(name),
      }
    }
  }

  let contributionAnnotations = []
  for (const contribution of filing.contribution_items) {
    let contributionAnnotation = {}

    const honoree = util.normalizeEntity(
      contribution['honoree_name'],
      entityNormalization
    )
    contributionAnnotation['honoree'] = {
      origText: contribution['honoree_name'],
      text: honoree,
      stylizedText: util.getFullyStylizedName(honoree, entities),
      logo: util.logoURLFromAncestors(honoree, entities),
      url:
        honoree in entities
          ? util.encodeEntity(honoree)
          : util.encodeSearch(honoree),
    }

    const payee = util.normalizeEntity(
      contribution['payee_name'],
      entityNormalization
    )
    contributionAnnotation['payee'] = {
      origText: contribution['payee_name'],
      text: payee,
      stylizedText: util.getFullyStylizedName(payee, entities),
      logo: util.logoURLFromAncestors(payee, entities),
      url:
        payee in entities ? util.encodeEntity(payee) : util.encodeSearch(payee),
    }

    if (contribution['contributor_name']) {
      const contributor = util.normalizeEntity(
        contribution['contributor_name'],
        entityNormalization
      )
      contributionAnnotation['contributor'] = {
        origText: contribution['contributor_name'],
        text: contributor,
        stylizedText: util.getFullyStylizedName(contributor, entities),
        logo: util.logoURLFromAncestors(contributor, entities),
        url:
          contributor in entities
            ? util.encodeEntity(contributor)
            : util.encodeSearch(contributor),
      }
    }

    if (contribution['contribution_type']) {
      const type = contribution['contribution_type']
      let stylizedText = contribution['contribution_type_display']
      let url =
        'https://lobbyingdisclosure.house.gov/amended_lda_guide.html#section7'
      if (type == 'feca') {
        stylizedText = 'FECA'
        url = 'https://en.wikipedia.org/wiki/Federal_Election_Campaign_Act'
      } else if (type == 'honorary expenses') {
        stylizedText = 'Honorary Expenses'
      } else if (type == 'meeting expenses') {
        stylizedText = 'Meeting Expenses'
      }
      contributionAnnotation['contributionType'] = {
        stylizedText: stylizedText,
        url: url,
      }
    }

    contributionAnnotations.push(contributionAnnotation)
  }
  annotation['contributions'] = contributionAnnotations

  filing['annotation'] = annotation
}

function annotate(
  contributions,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  lobbyistIDNormalization: Map<number, string>
) {
  let maxAmount: number = 0
  let totalAmount: number = 0
  let totalAmountByYear = {}
  let totalAmountByHonoree = {}
  let totalAmountByPayee = {}
  let totalAmountByRegistrant = {}
  let totalAmountByLobbyist = {}
  for (const filing of contributions.filings) {
    annotateContribution(
      filing,
      entities,
      entityNormalization,
      lobbyistIDNormalization
    )
    if (!filing.contribution_items) {
      continue
    }

    const year = filing.filing_year.toString()
    const annotation = filing.annotation
    for (const [index, contribution] of filing.contribution_items.entries()) {
      const contributionAnnotation =
        filing['annotation']['contributions'][index]

      const amount = Number(contribution.amount)
      if (isNaN(amount)) {
        continue
      }

      if (year in totalAmountByYear) {
        totalAmountByYear[year] += amount
      } else {
        totalAmountByYear[year] = amount
      }

      const honoree = contributionAnnotation.honoree.stylizedText
      if (honoree in totalAmountByHonoree) {
        totalAmountByHonoree[honoree] += amount
      } else {
        totalAmountByHonoree[honoree] = amount
      }

      const payee = contributionAnnotation.payee.stylizedText
      if (payee in totalAmountByPayee) {
        totalAmountByPayee[payee] += amount
      } else {
        totalAmountByPayee[payee] = amount
      }

      const registrant = annotation.registrant.stylizedText
      if (registrant in totalAmountByRegistrant) {
        totalAmountByRegistrant[registrant] += amount
      } else {
        totalAmountByRegistrant[registrant] = amount
      }

      const lobbyist = annotation.lobbyist.stylizedText
      if (lobbyist in totalAmountByLobbyist) {
        totalAmountByLobbyist[lobbyist] += amount
      } else if (lobbyist) {
        totalAmountByLobbyist[lobbyist] = amount
      }

      maxAmount = Math.max(maxAmount, amount)
      totalAmount += amount
    }
  }
  contributions['amountSummaries'] = {
    max: maxAmount,
    total: totalAmount,
    totalByYear: totalAmountByYear,
    totalByHonoree: totalAmountByHonoree,
    totalByPayee: totalAmountByPayee,
    totalByRegistrant: totalAmountByRegistrant,
    totalByLobbyist: totalAmountByLobbyist,
  }
}

async function haveForNames(
  names: string[],
  ids: number[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(
      names,
      dateRange,
      entities,
      ['feeds', 'us', 'central', 'lobbying', 'opr'],
      'haveContribution'
    )
  ) {
    return true
  }

  let baseParams = []
  const dateWhere = buildDateConstraint(dateRange, baseParams)
  const dateConstraint = dateWhere ? `AND ${dateWhere}` : ''
  const paramOffset = baseParams.length + 1

  const header = `SELECT 1 FROM ${kTableName} WHERE `

  // NOTE: When this limit was only 1000, it seems to have exposed a
  // performance bug in Postgres for entities such as Ted Cruz.
  const footer = `${dateConstraint} LIMIT 10000`

  let manager = dataSource.manager

  let params = [...baseParams, names]
  let query =
    header + `lower(registrant->>'name') = ANY($${paramOffset})` + footer
  if (await util.nonemptyQuery(manager, query, params)) {
    return true
  }

  params = [...baseParams, ids]
  query = header + `lobbyist->>'id' = ANY($${paramOffset})` + footer
  if (await util.nonemptyQuery(manager, query, params)) {
    return true
  }

  const contributorNames = lobbyingUtil.jsonTargets(names, 'contributor_name')
  params = [...baseParams, contributorNames]
  query =
    header +
    `lower(contribution_items::TEXT)::JSONB @>
      ANY(cast($${paramOffset} as jsonb[])) ` +
    footer
  if (await util.nonemptyQuery(manager, query, params)) {
    return true
  }

  const payeeNames = lobbyingUtil.jsonTargets(names, 'payee_name')
  params = [...baseParams, payeeNames]
  query =
    header +
    `lower(contribution_items::TEXT)::JSONB @>
      ANY(cast($${paramOffset} as jsonb[])) ` +
    footer
  if (await util.nonemptyQuery(manager, query, params)) {
    return true
  }

  const honoreeNames = lobbyingUtil.jsonTargets(names, 'honoree_name')
  params = [...baseParams, honoreeNames]
  query =
    header +
    `lower(contribution_items::TEXT)::JSONB @>
      ANY(cast($${paramOffset} as jsonb[])) ` +
    footer
  if (await util.nonemptyQuery(manager, query, params)) {
    return true
  }

  return false
}

async function haveForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  lobbyistIDAlternates: Map<string, number[]>
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const lobbyingNames = lobbyingUtil.expandNames(
    names,
    entities,
    entityAlternates
  )
  const lobbyistIDs = lobbyingUtil.lobbyistIDsFromNames(
    names,
    lobbyistIDAlternates
  )
  return await haveForNames(lobbyingNames, lobbyistIDs, dateRange, entities)
}

async function getForNames(
  names: string[],
  ids: number[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const dateConstraint = dateWhere ? `AND ${dateWhere}` : ''

  let outerLimit = `ORDER BY dt_posted DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults)
  if (searchOffset > 0) {
    outerLimit += ` OFFSET $${params.length + 1}`
    params.push(searchOffset)
  }
  const innerLimit = `ORDER BY dt_posted DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults + searchOffset)

  const contributorNames = lobbyingUtil.jsonTargets(names, 'contributor_name')
  const payeeNames = lobbyingUtil.jsonTargets(names, 'payee_name')
  const honoreeNames = lobbyingUtil.jsonTargets(names, 'honoree_name')

  params.push(names)
  const namesIndex = params.length
  params.push(ids)
  const idsIndex = params.length
  params.push(contributorNames)
  const contributorIndex = params.length
  params.push(payeeNames)
  const payeeIndex = params.length
  params.push(honoreeNames)
  const honoreeIndex = params.length

  const subqueryHeader = `SELECT * FROM ${kTableName} WHERE `

  const registrantQuery =
    subqueryHeader +
    `lower(registrant->>'name') = ANY($${namesIndex})
    ${dateConstraint} ${innerLimit}`

  const lobbyistQuery =
    subqueryHeader +
    `lobbyist->>'id' = ANY($${idsIndex})
    ${dateConstraint} ${innerLimit}`

  const contributorQuery =
    subqueryHeader +
    `lower(contribution_items::TEXT)::JSONB @>
    ANY(cast($${contributorIndex} as jsonb[]))
    ${dateConstraint} ${innerLimit}`

  const payeeQuery =
    subqueryHeader +
    `lower(contribution_items::TEXT)::JSONB @>
    ANY(cast($${payeeIndex} as jsonb[]))
    ${dateConstraint} ${innerLimit}`

  const honoreeQuery =
    subqueryHeader +
    `lower(contribution_items::TEXT)::JSONB @>
    ANY(cast($${honoreeIndex} as jsonb[]))
    ${dateConstraint} ${innerLimit}`

  let queryStr = `
    SELECT * FROM (
     (${registrantQuery}) UNION
     (${lobbyistQuery}) UNION
     (${contributorQuery}) UNION
     (${payeeQuery}) UNION
     (${honoreeQuery})
    ) s ${outerLimit}`

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  lobbyistIDAlternates: Map<string, number[]>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const lobbyingNames = lobbyingUtil.expandNames(
    names,
    entities,
    entityAlternates
  )
  const lobbyistIDs = lobbyingUtil.lobbyistIDsFromNames(
    names,
    lobbyistIDAlternates
  )

  return await getForNames(
    lobbyingNames,
    lobbyistIDs,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const dateConstraint = dateWhere ? `AND ${dateWhere}` : ''

  const queryStr = `
    SELECT 1 FROM ${kTableName} WHERE ${fullTextSearchString}
     @@ websearch_to_tsquery('simple', $${params.length + 1})
     ${dateConstraint} LIMIT 1000`
  params.push(webSearch)

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('dt_posted', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['filing_uuid']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const whereStr = dateWhere ? `WHERE ${dateWhere}` : ''

  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('dt_posted', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: USCentralOPRFeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Have US OPR contributions w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have US OPR contributions w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Have US OPR contributions w/ entity: ${name}`)
    result = await haveForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names,
      feedInfo.lobbyistIDAlternates
    )
  } else {
    console.log('Have US OPR contributions over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: USCentralOPRFeedInfo
) {
  const params = request.query

  const maxSearchResultsDefault = 500
  const maxSearchResultsCap = 1000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`US OPR contributions w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`US OPR contributions w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`US OPR contributions w/ entity: ${name}`)
    result = await getForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names,
      feedInfo.lobbyistIDAlternates,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('US OPR contributions over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(
      result,
      entities,
      feedInfo.normalization,
      feedInfo.lobbyistIDNormalization
    )
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/us/central/lobbying/opr/contributions'

  const feed = entityState.feeds
    .get('us')
    .get('central')
    .get('lobbying')
    .get('opr')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether lobbying contributions exist for terms or an entity.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, feed)
  })

  // Returns JSON for lobbying contributions for a web search or an entity.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(request, response, entities, children, feed)
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
