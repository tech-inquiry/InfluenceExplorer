import * as util from '../../../../util'

export function expandNames(
  entityList: string[],
  entities: Map<string, Map<string, any>>,
  entityAlternates: Map<string, string[]>
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'us',
    'central',
    'lobbying',
    'opr',
  ])
}

export function lobbyistIDsFromNames(
  names: string[],
  lobbyistIDAlternates: Map<string, number[]>
): number[] {
  let lobbyistIDs: number[] = []
  for (const name of names) {
    if (lobbyistIDAlternates.has(name)) {
      for (const alternate of lobbyistIDAlternates.get(name)) {
        lobbyistIDs.push(alternate)
      }
    }
  }
  return lobbyistIDs
}

export function jsonTargets(
  values: string[],
  key: string,
  upperCase = false
): string[] {
  let targets: string[] = []
  for (const value of values) {
    let obj = {}
    obj[key] = upperCase ? value.toUpperCase() : value
    targets.push(JSON.stringify([obj]))
  }
  return targets
}
