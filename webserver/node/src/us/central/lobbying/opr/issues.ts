import { Brackets } from 'typeorm'

import { dataSource } from '../../../../App'
import { USCentralOPRFeedInfo } from '../../../../EntityState'
import { USCentralOPRIssues } from '../../../../entity/us/central/lobbying/opr/issues'
import * as lobbyingUtil from './util'
import * as tags from '../../../../tags'
import * as util from '../../../../util'

const kTable = USCentralOPRIssues
const kTableName = 'us_opr_issues_filing'

const kTagDataset = 'us_opr_issues_filing'

// The concatenation of fields from a row of the us_opr_issues_filing
// Postgres table to match with full-text searches.
const fullTextSearchString = "to_tsvector('simple', search_text)"

function affiliatedOrgTargetsFromNames(names: string[]): string[] {
  return lobbyingUtil.jsonTargets(names, 'name', true)
}

function idTargetsFromIDs(ids: number[]): string[] {
  if (!ids) {
    return []
  }
  return ids.map((id) => `[${id}]`)
}

function maxPeriods(month: number): string[] {
  // These periods are always acceptable.
  let periods = ['', 'undetermined', 'first_quarter', 'mid_year']

  if (month >= 3) {
    periods.push('second_quarter')
  }
  if (month >= 6) {
    periods.push('third_quarter')
    periods.push('year_end')
  }
  if (month >= 9) {
    periods.push('fourth_quarter')
  }

  return periods
}

function minPeriods(month: number): string[] {
  // These periods are always acceptable.
  let periods = ['', 'undetermined', 'fourth_quarter', 'year_end']

  if (month < 9) {
    periods.push('third_quarter')
  }
  if (month < 6) {
    periods.push('second_quarter')
    periods.push('mid_year')
  }
  if (month < 3) {
    periods.push('first_quarter')
  }

  return periods
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let lines = []
  if (dateRange.hasStart()) {
    const startMonth = dateRange.start.getMonth()
    const startYear = dateRange.start.getFullYear()
    if (startMonth >= 3) {
      const startPeriods = minPeriods(startMonth)
      lines.push(`
        dt_posted >= $${params.length + 1} AND
        (filing_year > $${params.length + 2} OR
        (filing_year = $${params.length + 2} AND
        filing_period = ANY($${params.length + 3})))`)
      params.push(dateRange.start)
      params.push(startYear)
      params.push(startPeriods)
    } else {
      lines.push(`
        dt_posted >= $${params.length + 1} AND
        filing_year >= $${params.length + 2}`)
      params.push(dateRange.start)
      params.push(startYear)
    }
  }
  if (dateRange.hasEnd()) {
    const endMonth = dateRange.end.getMonth()
    const endYear = dateRange.end.getFullYear()
    if (endMonth < 9) {
      const endPeriods = maxPeriods(endMonth)
      lines.push(`
        filing_year < $${params.length + 1} OR
        (filing_year = $${params.length + 1} AND
        filing_period = ANY($${params.length + 2}))`)
      params.push(endYear)
      params.push(endPeriods)
    } else {
      lines.push(`filing_year <= $${params.length + 1}`)
      params.push(endYear)
    }
  }
  if (lines.length) {
    return `(${lines.join(' AND ')})`
  } else {
    return ''
  }
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateFiling(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  lobbyistIDNormalization: Map<number, string>,
  governmentEntityNormalization: Map<string, string>
): void {
  const registrant = util.normalizeEntity(
    filing.registrant.name,
    entityNormalization
  )
  const client = util.normalizeEntity(filing.client.name, entityNormalization)
  const contact = util.normalizeEntity(
    filing.registrant.contact_name,
    entityNormalization
  )
  filing['annotation'] = {
    registrant: {
      origText: filing.registrant.name,
      text: registrant,
      stylizedText: util.getFullyStylizedName(registrant, entities),
      logo: util.logoURLFromAncestors(registrant, entities),
    },
    client: {
      origText: filing.client.name,
      text: client,
      stylizedText: util.getFullyStylizedName(client, entities),
      logo: util.logoURLFromAncestors(client, entities),
    },
    contact: {
      origText: filing.registrant.contact_name,
      text: contact,
      stylizedText: util.getFullyStylizedName(contact, entities),
      logo: util.logoURLFromAncestors(contact, entities),
    },
  }

  filing['annotation']['affiliatedOrgs'] = []
  for (const org of filing['affiliated_organizations']) {
    const normalizedOrg = util.normalizeEntity(org['name'], entityNormalization)
    filing['annotation']['affiliatedOrgs'].push({
      origText: org['name'],
      text: normalizedOrg,
      stylizedText: util.getFullyStylizedName(normalizedOrg, entities),
      logo: util.logoURLFromAncestors(normalizedOrg, entities),
    })
  }

  let activities = []
  for (const activity of filing['lobbying_activities']) {
    let activityAnnotation = {}

    let lobbyistAnnotations = []
    for (const lobbyist of activity['lobbyists']) {
      let lobbyistAnnotation = {}
      const id = lobbyist['lobbyist']['id']
      if (lobbyistIDNormalization.has(id)) {
        let name = lobbyistIDNormalization.get(id)
        lobbyistAnnotation = {
          origText: util.usLobbyistNameFromObject(lobbyist['lobbyist']),
          text: name,
          stylizedText: util.getFullyStylizedName(name, entities),
          logo: util.logoURLFromAncestors(name, entities),
        }
      }
      lobbyistAnnotations.push(lobbyistAnnotation)
    }
    activityAnnotation['lobbyists'] = lobbyistAnnotations

    activities.push(activityAnnotation)
  }
  filing['annotation']['lobbyingActivities'] = activities
}

function annotate(
  issues,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  lobbyistIDNormalization: Map<number, string>,
  governmentEntityNormalization: Map<string, string>
): void {
  let maxAmount: number = 0
  let totalAmount: number = 0
  for (const filing of issues.filings) {
    annotateFiling(
      filing,
      entities,
      entityNormalization,
      lobbyistIDNormalization,
      governmentEntityNormalization
    )

    if (!isNaN(filing.income)) {
      maxAmount = Math.max(maxAmount, filing.income)
      totalAmount += filing.income
    }
    if (!isNaN(filing.expenses)) {
      maxAmount = Math.max(maxAmount, filing.expenses)
      totalAmount += filing.expenses
    }
  }

  issues['amountSummaries'] = {
    max: maxAmount,
    total: totalAmount,
  }
}

async function haveForNames(
  names: string[],
  ids: number[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(
      names,
      dateRange,
      entities,
      ['feeds', 'us', 'central', 'lobbying', 'opr'],
      'haveIssue'
    )
  ) {
    return true
  }

  const affiliatedOrgTargets = affiliatedOrgTargetsFromNames(names)
  const idTargets = idTargetsFromIDs(ids)

  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const dateConstraint = dateWhere ? `AND ${dateWhere}` : ''

  const subqueryHeader = `SELECT 1 FROM ${kTableName} WHERE `
  // NOTE: When this limit was only 1000, it seems to have exposed a performance
  // bug in Postgres for entities such as Ted Cruz.
  const subqueryFooter = `${dateConstraint} LIMIT 10000`

  let manager = dataSource.manager

  const subqueryPairs = [
    [
      [
        `lower(client->>'name') = ANY($${params.length + 1})`,
        `lower(registrant->>'contact_name') = ANY($${params.length + 1})`,
        `lower(registrant->>'name') = ANY($${params.length + 1})`,
      ],
      params.concat([names]),
    ],
    [
      [`lobbyist_ids @> ANY(cast($${params.length + 1} as jsonb[]))`],
      params.concat([idTargets]),
    ],
    [
      [
        `affiliated_organizations @> ANY(cast($${
          params.length + 1
        } as jsonb[]))`,
        `foreign_entities @> ANY(cast($${params.length + 1} as jsonb[]))`,
      ],
      params.concat([affiliatedOrgTargets]),
    ],
  ]
  for (const [subqueryList, subparams] of subqueryPairs) {
    for (const subquery of subqueryList) {
      const queryStr = subqueryHeader + subquery + subqueryFooter
      if (await util.nonemptyQuery(manager, queryStr, subparams)) {
        return true
      }
    }
  }

  return false
}

async function haveForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  lobbyistIDAlternates: Map<string, number[]>
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const lobbyingNames = lobbyingUtil.expandNames(
    names,
    entities,
    entityAlternates
  )
  const lobbyistIDs = lobbyingUtil.lobbyistIDsFromNames(
    names,
    lobbyistIDAlternates
  )
  return await haveForNames(lobbyingNames, lobbyistIDs, dateRange, entities)
}

async function getForNames(
  names: string[],
  ids: number[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const affiliatedOrgTargets = affiliatedOrgTargetsFromNames(names)
  const idTargets = idTargetsFromIDs(ids)

  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const dateConstraint = dateWhere ? `AND ${dateWhere}` : ''

  let outerLimit = `ORDER BY dt_posted DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults)
  if (searchOffset > 0) {
    outerLimit += ` OFFSET $${params.length + 1}`
    params.push(searchOffset)
  }

  const innerLimit = ` ORDER BY dt_posted DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults + searchOffset)

  const subqueryHeader = `SELECT * FROM ${kTableName} WHERE `
  const subqueryFooter = `${dateConstraint} ${innerLimit}`

  const clientQuery =
    subqueryHeader +
    `lower(client->>'name') = ANY($${params.length + 1})` +
    subqueryFooter
  const contactQuery =
    subqueryHeader +
    `lower(registrant->>'contact_name') = ANY($${params.length + 1})` +
    subqueryFooter
  const registrantQuery =
    subqueryHeader +
    `lower(registrant->>'name') = ANY($${params.length + 1})` +
    subqueryFooter
  params.push(names)

  const lobbyistIDsQuery =
    subqueryHeader +
    `lobbyist_ids @> ANY(cast($${params.length + 1} as jsonb[]))` +
    subqueryFooter
  params.push(idTargets)

  const affiliatedOrgsQuery =
    subqueryHeader +
    `affiliated_organizations @> ANY(cast($${params.length + 1} as jsonb[]))` +
    subqueryFooter
  const foreignEntitiesQuery =
    subqueryHeader +
    `foreign_entities @> ANY(cast($${params.length + 1} as jsonb[]))` +
    subqueryFooter
  params.push(affiliatedOrgTargets)

  const queryStr = `
    SELECT * FROM (
     (${clientQuery}) UNION
     (${contactQuery}) UNION
     (${registrantQuery}) UNION
     (${lobbyistIDsQuery}) UNION
     (${affiliatedOrgsQuery}) UNION
     (${foreignEntitiesQuery})
    ) s ${outerLimit}`

  return await util.getQueryFilings(dataSource.manager, queryStr, params)
}

async function getForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  lobbyistIDAlternates: Map<string, number[]>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const lobbyingNames = lobbyingUtil.expandNames(
    names,
    entities,
    entityAlternates
  )
  const lobbyistIDs = lobbyingUtil.lobbyistIDsFromNames(
    names,
    lobbyistIDAlternates
  )

  return await getForNames(
    lobbyingNames,
    lobbyistIDs,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const dateConstraint = dateWhere ? `AND ${dateWhere}` : ''

  const queryStr = `
    SELECT 1 FROM ${kTableName} WHERE ${fullTextSearchString}
     @@ websearch_to_tsquery('simple', $${params.length + 1})
     ${dateConstraint} LIMIT 1000`
  params.push(webSearch)

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })

  if (dateRange.hasStart()) {
    const startMonth = dateRange.start.getMonth()
    const startYear = dateRange.start.getFullYear()

    const dateYearMatch = function (qb) {
      if (startMonth >= 3) {
        const periods = minPeriods(startMonth)
        const innerDateMatch = function (qbInner) {
          return qbInner
            .where('filing_year = :startYear', { startYear: startYear })
            .andWhere('filing_period = ANY(ARRAY[:...periods])', {
              periods: periods,
            })
        }
        return qb
          .where('filing_year > :startYear', { startYear: startYear })
          .orWhere(new Brackets(innerDateMatch))
      } else {
        return qb.where('filing_year >= :startYear', { startYear: startYear })
      }
    }

    query = query
      .andWhere('dt_posted >= :startDate', { startDate: dateRange.start })
      .andWhere(new Brackets(dateYearMatch))
  }
  if (dateRange.hasEnd()) {
    const endMonth = dateRange.end.getMonth()
    const endYear = dateRange.end.getFullYear()

    const dateYearMatch = function (qb) {
      if (endMonth < 9) {
        const periods = maxPeriods(endMonth)
        const innerDateMatch = function (qbInner) {
          return qbInner
            .where('filing_year = :endYear', { endYear: endYear })
            .andWhere('filing_period = ANY(ARRAY[:...periods])', {
              periods: periods,
            })
        }
        return qb
          .where('filing_year < :endYear', { endYear: endYear })
          .orWhere(new Brackets(innerDateMatch))
      } else {
        return qb.where('filing_year <= :endYear', { endYear: endYear })
      }
    }

    query = query.andWhere(new Brackets(dateYearMatch))
  }

  query = query.orderBy('dt_posted', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['filing_uuid']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const whereStr = dateWhere ? `WHERE ${dateWhere}` : ''

  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()

  if (dateRange.hasStart()) {
    const startMonth = dateRange.start.getMonth()
    const startYear = dateRange.start.getFullYear()

    const dateYearMatch = function (qb) {
      if (startMonth >= 3) {
        const periods = minPeriods(startMonth)
        const innerDateMatch = function (qbInner) {
          return qbInner
            .where('filing_year = :startYear', { startYear: startYear })
            .andWhere('filing_period = ANY(ARRAY[:...periods])', {
              periods: periods,
            })
        }
        return qb
          .where('filing_year > :startYear', { startYear: startYear })
          .orWhere(new Brackets(innerDateMatch))
      } else {
        return qb.where('filing_year >= :startYear', { startYear: startYear })
      }
    }

    query = query
      .andWhere('dt_posted >= :startDate', { startDate: dateRange.start })
      .andWhere(new Brackets(dateYearMatch))
  }
  if (dateRange.hasEnd()) {
    const endMonth = dateRange.end.getMonth()
    const endYear = dateRange.end.getFullYear()

    const dateYearMatch = function (qb) {
      if (endMonth < 9) {
        const periods = maxPeriods(endMonth)
        const innerDateMatch = function (qbInner) {
          return qbInner
            .where('filing_year = :endYear', { endYear: endYear })
            .andWhere('filing_period = ANY(ARRAY[:...periods])', {
              periods: periods,
            })
        }
        return qb
          .where('filing_year < :endYear', { endYear: endYear })
          .orWhere(new Brackets(innerDateMatch))
      } else {
        return qb.where('filing_year <= :endYear', { endYear: endYear })
      }
    }

    query = query.andWhere(new Brackets(dateYearMatch))
  }

  query = query.orderBy('dt_posted', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: USCentralOPRFeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Have US OPR issues API: text='${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have US OPR issues w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Have US Issues API: entity='${name}'`)
    result = await haveForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names,
      feedInfo.lobbyistIDAlternates
    )
  } else {
    console.log('Have US OPR issues API over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: USCentralOPRFeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  const maxSearchResultsDefault = 1000
  const maxSearchResultsCap = 5000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`US OPR issues API: text='${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`US OPR issues w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`US OPR issues API: entity='${name}'`)
    result = await getForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names,
      feedInfo.lobbyistIDAlternates,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('US OPR issues API over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(
      result,
      entities,
      feedInfo.normalization,
      feedInfo.lobbyistIDNormalization,
      feedInfo.agencyNormalization
    )
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/us/central/lobbying/opr/issues'

  const feed = entityState.feeds
    .get('us')
    .get('central')
    .get('lobbying')
    .get('opr')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether lobbying issues exist for a web search or an entity.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, feed)
  })

  // Returns JSON for lobbying issues for a web search or an entity.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(request, response, entities, children, feed)
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
