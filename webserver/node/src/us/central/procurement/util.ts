import * as util from '../../../util'

function convertDepartmentCodeForGrants(code: string): string {
  if (code.length == 4 && code.endsWith('00')) {
    return '0' + code.substr(0, 2)
  } else {
    return code
  }
}

export function getAgencyCodes(
  name: string,
  entities: Map<string, Map<string, any>>,
  usFederalOffices: Map<string, Map<string, any>>,
  convertForGrants = false
): Map<string, string[]> {
  let codes = new Map<string, string[]>()
  codes.set('dept', [])
  codes.set('agency', [])
  codes.set('office', [])
  if (!entities.has(name)) {
    return codes
  }
  const profile = entities.get(name)
  const alternates = util.jsonGetFromPath(profile, [
    'feeds',
    'us',
    'central',
    'procurement',
    'agencyNames',
  ])
  const includes = util.jsonGetFromPath(profile, [
    'feeds',
    'us',
    'central',
    'procurement',
    'agencyIncludes',
  ])
  const listOfLists = [alternates, includes]

  let departmentSet = new Set<string>()
  let agencySet = new Set<string>()
  let officeSet = new Set<string>()

  for (const list of listOfLists) {
    if (list == undefined) {
      continue
    }
    for (const alternate of list) {
      const dept = alternate.get('dept')
      if (alternate.has('office') || alternate.has('offices')) {
        const agency = alternate.get('agency')
        const offices = alternate.has('offices')
          ? alternate.get('offices')
          : [alternate.get('office')]
        if (!usFederalOffices.get('office_codes').has(dept)) {
          console.log(`Could not retrieve office code for dept="${dept}"`)
          continue
        }
        if (!usFederalOffices.get('office_codes').get(dept).has(agency)) {
          console.log(
            `Could not retrieve office code for dept="${dept}", agency="${agency}"`
          )
          continue
        }
        for (const office of offices) {
          if (
            !usFederalOffices
              .get('office_codes')
              .get(dept)
              .get(agency)
              .has(office)
          ) {
            ;`Could not retrieve office code for office="${office}", dept="${dept}", agency="${agency}"`
            continue
          }
          for (const code of usFederalOffices
            .get('office_codes')
            .get(dept)
            .get(agency)
            .get(office)) {
            if (convertForGrants) {
              officeSet.add(util.coerceUpper(code))
            } else {
              officeSet.add(code)
            }
          }
        }
      } else if (alternate.has('agency')) {
        const agency = alternate.get('agency')
        const agencyCodes = usFederalOffices.get('agency_codes')
        if (!agencyCodes.has(dept)) {
          console.log(`Could not find ${dept} in agency codes.`)
          continue
        }
        if (!agencyCodes.get(dept).has(agency)) {
          console.log(`Could not find ${agency} in agencyCodes[${dept}].`)
          continue
        }
        for (const code of usFederalOffices
          .get('agency_codes')
          .get(dept)
          .get(agency)) {
          agencySet.add(code)
        }
      } else if (alternate.has('dept')) {
        if (!usFederalOffices.get('dept_codes').has(dept)) {
          console.log(`Could not find ${dept} in usFederalOffices.`)
          continue
        }
        for (const code of usFederalOffices.get('dept_codes').get(dept)) {
          if (convertForGrants) {
            departmentSet.add(convertDepartmentCodeForGrants(code))
          } else {
            departmentSet.add(code)
          }
        }
      }
    }
  }

  codes.set('dept', Array.from(departmentSet))
  codes.set('agency', Array.from(agencySet))
  codes.set('office', Array.from(officeSet))

  return codes
}

// Ideally, we would be able to execute a query similar to:
//
//   const awards = await typeorm.getRepository(USFederalAward)
//       .createQueryBuilder()
//       .where('vendor IN (:...entityList)',
//           {entityList: entityList})
//       .orWhere('uei IN (:...ueiList)',
//           {ueiList: ueiList})
//       .orWhere('(vendor,uei) IN (:...entityUEIList)',
//           {entityUEIList: entityUEIList})
//       .orderBy('signed_date', 'DESC')
//       .addOrderBy('total_base_and_all_options_value', 'DESC')
//       .limit(maxSearchResults)
//       .getMany()
//
// but Postgres's query planner often needs to be coaxed into using the
// associated indices through unioning three separate queries (one for each
// WHERE subclause).
export function getNameAndUEILists(
  name: string,
  names: string[],
  entities: Map<string, Map<string, any>>
) {
  let ueiSet = new Set<string>()
  let nameSet = new Set<string>()
  let nameAndAlternates = new Set<string>()
  let contractorSet = new Set<string>()
  nameAndAlternates.add(name)
  contractorSet.add(name)
  for (const v of names) {
    nameSet.add(v)
    contractorSet.add(v)
    if (!entities.has(v)) {
      continue
    }
    const profile = entities.get(v)
    const nameAlternates = util.jsonGetFromPath(profile, [
      'feeds',
      'us',
      'central',
      'procurement',
      'names',
    ])
    if (nameAlternates != undefined) {
      for (const alternate of nameAlternates) {
        const map = util.isString(alternate)
          ? new Map<string, string>([['name', alternate]])
          : alternate
        if (map.has('name')) {
          nameSet.add(map.get('name'))
          if (v == name) {
            nameAndAlternates.add(map.get('name'))
          }
        }
      }
    }

    const ueiAlternates = util.jsonGetFromPath(profile, [
      'feeds',
      'us',
      'central',
      'procurement',
      'ueis',
    ])
    if (ueiAlternates != undefined) {
      for (const alternate of ueiAlternates) {
        if (util.isString(alternate)) {
          ueiSet.add(alternate)
        } else if (alternate.has('uei')) {
          ueiSet.add(alternate.get('uei'))
        }
      }
    }

    const contractors = util.jsonGetFromPath(profile, [
      'feeds',
      'us',
      'central',
      'procurement',
      'contractorNames',
    ])
    if (contractors != undefined) {
      for (const alternate of contractors) {
        contractorSet.add(alternate)
      }
    }
  }

  return {
    nameList: Array.from(nameSet.values()),
    ueiList: Array.from(ueiSet.values()),
    nameAndAlternates: Array.from(nameAndAlternates.values()),
    contractors: Array.from(contractorSet.values()),
  }
}

export function getAgencyNormalizers(
  name: string,
  entities: Map<string, Map<string, any>>
) {
  let agencies = []
  if (!entities.has(name)) {
    return agencies
  }
  const profile = entities.get(name)
  const alternates = util.jsonGetFromPath(profile, [
    'feeds',
    'us',
    'central',
    'procurement',
    'agencyNames',
  ])
  if (alternates != undefined) {
    agencies = alternates
  }
  const includes = util.jsonGetFromPath(profile, [
    'feeds',
    'us',
    'central',
    'procurement',
    'agencyIncludes',
  ])
  if (includes != undefined) {
    agencies = agencies.concat(includes)
  }

  let expandedAgencies = []
  for (const alternate of agencies) {
    if (alternate.has('offices')) {
      for (const office of alternate.get('offices')) {
        let obj = new Map<string, string>()
        obj.set('agency', alternate.get('agency'))
        obj.set('office', office)
        expandedAgencies.push(obj)
      }
    } else if (alternate.has('office')) {
      let obj = new Map<string, string>()
      obj.set('agency', alternate.get('agency'))
      obj.set('office', alternate.get('office'))
      expandedAgencies.push(obj)
    } else {
      let obj = new Map<string, string>()
      obj.set('agency', alternate.get('agency'))
      expandedAgencies.push(obj)
    }
  }

  return expandedAgencies
}

// Split the agency descriptions between agency and [agency, office] formats.
export function getAgencyAndOfficeLists(
  agencies: Map<string, string>[]
): Map<string, string[]> {
  let officeSet = new Set<string>()
  let agenciesWithOfficeSet = new Set<string>()
  let agenciesWithoutOfficeSet = new Set<string>()

  for (const agency of agencies) {
    if (agency.has('office')) {
      officeSet.add(agency.get('office'))
      agenciesWithOfficeSet.add(agency.get('agency'))
    } else {
      agenciesWithoutOfficeSet.add(agency.get('agency'))
    }
  }

  let lists = new Map<string, string[]>()
  lists.set('offices', Array.from(officeSet))
  lists.set('agenciesWithOffice', Array.from(agenciesWithOfficeSet))
  lists.set('agenciesWithoutOffice', Array.from(agenciesWithoutOfficeSet))

  return lists
}
