import { dataSource } from '../../../App'
import * as procurementUtil from './util'
import * as tags from '../../../tags'
import * as util from '../../../util'

import { USCentralProcurementSubaward } from '../../../entity/us/central/procurement/subaward'

const kTable = USCentralProcurementSubaward
const kTableName = 'us_central_procurement_subaward'

const kTagDataset = 'us_central_procurement_subaward'

const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(prime_key)',
  'lower(prime_piid)',
  'lower(prime_parent_piid)',
  'lower(prime_awarding_agency)',
  'lower(prime_awarding_sub_agency)',
  'lower(prime_awarding_office)',
  'lower(prime_funding_agency)',
  'lower(prime_funding_sub_agency)',
  'lower(prime_funding_office)',
  'prime_uei',
  'prime_name',
  'prime_dba',
  'prime_parent_uei',
  'prime_parent_name',
  'lower(sub_number)',
  'sub_uei',
  'sub_name',
  'sub_dba',
  'sub_parent_uei',
  'sub_parent_name',
  'lower(sub_pop_address_line_1)',
  'lower(sub_pop_city)',
  'lower(sub_pop_state)',
  'lower(sub_pop_zipcode)',
  'lower(sub_pop_country)',
  'lower(sub_description)',
])

function buildDateConstraint(dateRange: util.DateRange, params) {
  let dateWhere = ''
  if (dateRange.hasStart()) {
    dateWhere += ` AND sub_action_date >= $${params.length + 1}`
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    dateWhere += ` AND sub_action_date <= $${params.length + 1}`
    params.push(dateRange.end.toISOString())
  }
  return dateWhere
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('sub_action_date >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('sub_action_date <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

function getAgencyCodes(
  name: string,
  entities: Map<string, Map<string, any>>,
  usFederalOffices: Map<string, Map<string, any>>,
  subawardDepartments: Map<string, Map<string, string>>
): Map<string, string[]> {
  let codes = new Map<string, string[]>()
  codes.set('dept', [])
  codes.set('agency', [])
  codes.set('office', [])
  if (!entities.has(name)) {
    return codes
  }
  const profile = entities.get(name)
  const alternates = util.jsonGetFromPath(profile, [
    'feeds',
    'us',
    'central',
    'procurement',
    'agencyNames',
  ])
  const includes = util.jsonGetFromPath(profile, [
    'feeds',
    'us',
    'central',
    'procurement',
    'agencyIncludes',
  ])
  const listOfLists = [alternates, includes]

  let departmentSet = new Set<string>()
  let agencySet = new Set<string>()
  let officeSet = new Set<string>()

  for (const list of listOfLists) {
    if (list == undefined) {
      continue
    }
    for (const alternate of list) {
      const dept = alternate.get('dept')
      if (alternate.has('office') || alternate.has('offices')) {
        const agency = alternate.get('agency')
        const offices = alternate.has('offices')
          ? alternate.get('offices')
          : [alternate.get('office')]
        if (!usFederalOffices.get('office_codes').has(dept)) {
          console.log(`Could not retrieve office code for dept="${dept}"`)
          continue
        }
        if (!usFederalOffices.get('office_codes').get(dept).has(agency)) {
          console.log(
            `Could not retrieve office code for dept="${dept}", agency="${agency}"`
          )
          continue
        }
        for (const office of offices) {
          if (
            !usFederalOffices
              .get('office_codes')
              .get(dept)
              .get(agency)
              .has(office)
          ) {
            console.log(
              `Could not retrieve office code for dept="${dept}", agency="${agency}", office="${office}".`
            )
            continue
          }
          for (const code of usFederalOffices
            .get('office_codes')
            .get(dept)
            .get(agency)
            .get(office)) {
            officeSet.add(code)
          }
        }
      } else if (alternate.has('agency')) {
        const agency = alternate.get('agency')
        if (!usFederalOffices.get('agency_codes').has(dept)) {
          console.log(`Could not retrieve agency code for dept="${dept}".`)
          continue
        }
        if (!usFederalOffices.get('agency_codes').get(dept).has(agency)) {
          console.log(
            `Could not retrieve agency code for dept="${dept}", agency="${agency}".`
          )
          continue
        }
        for (const code of usFederalOffices
          .get('agency_codes')
          .get(dept)
          .get(agency)) {
          agencySet.add(code)
        }
      } else if (alternate.has('dept')) {
        if (!usFederalOffices.get('dept_codes').get(dept)) {
          console.log(`Could not retrieve dept code for dept="${dept}".`)
          continue
        }
        for (const code of usFederalOffices.get('dept_codes').get(dept)) {
          if (subawardDepartments.get('inverseCode').has(code)) {
            const subCode = subawardDepartments.get('inverseCode').get(code)
            departmentSet.add(subCode)
          } else {
            departmentSet.add(code)
          }
        }
      }
    }
  }

  codes.set('dept', Array.from(departmentSet))
  codes.set('agency', Array.from(agencySet))
  codes.set('office', Array.from(officeSet))

  return codes
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateSubaward(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  governmentEntityNormalization: Map<string, Map<string, string>>,
  subawardDepartments: Map<string, Map<string, string>>
) {
  // Get the vendor annotations.
  const primeAwardee = util.normalizeEntityWithUEI(
    filing.prime_name,
    filing.prime_uei,
    entityNormalization
  )
  const subawardee = util.normalizeEntityWithUEI(
    filing.sub_name,
    filing.sub_uei,
    entityNormalization
  )
  let annotation = {
    prime: {
      text: primeAwardee,
      stylizedText: util.getFullyStylizedName(primeAwardee, entities),
      logo: util.logoURLFromAncestors(primeAwardee, entities),
    },
    sub: {
      text: subawardee,
      stylizedText: util.getFullyStylizedName(subawardee, entities),
      logo: util.logoURLFromAncestors(subawardee, entities),
    },
  }

  function agencyAnnotationFromComponents(
    department,
    departmentCode,
    agency,
    agencyCode,
    office,
    officeCode
  ) {
    departmentCode = departmentCode ? departmentCode.toLowerCase() : ''
    agencyCode = agencyCode ? agencyCode.toLowerCase() : ''
    officeCode = officeCode ? officeCode.toLowerCase() : ''

    let agencyNorm = agency
    if (governmentEntityNormalization.get('office').has(officeCode)) {
      agencyNorm = governmentEntityNormalization.get('office').get(officeCode)
    } else if (governmentEntityNormalization.get('agency').has(agencyCode)) {
      agencyNorm = governmentEntityNormalization.get('agency').get(agencyCode)
    } else {
      let code = departmentCode
      if (subawardDepartments.get('code').has(departmentCode)) {
        code = subawardDepartments.get('code').get(departmentCode)
      }
      if (governmentEntityNormalization.get('dept').has(code)) {
        agencyNorm = governmentEntityNormalization.get('dept').get(code)
      }
    }

    return {
      origDept: department,
      origAgency: agency,
      office: office,
      agencyNorm: agencyNorm,
      agencyStylized: util.getFullyStylizedName(agencyNorm, entities),
      agencyLogo: util.logoURLFromAncestors(agencyNorm, entities),
    }
  }

  annotation['funding'] = agencyAnnotationFromComponents(
    filing.prime_funding_agency,
    filing.prime_funding_agency_code,
    filing.prime_funding_sub_agency,
    filing.prime_funding_sub_agency_code,
    filing.prime_funding_office,
    filing.prime_funding_office_code
  )
  annotation['contracting'] = agencyAnnotationFromComponents(
    filing.prime_awarding_agency,
    filing.prime_awarding_agency_code,
    filing.prime_awarding_sub_agency,
    filing.prime_awarding_sub_agency_code,
    filing.prime_awarding_office,
    filing.prime_awarding_office_code
  )

  filing['annotation'] = annotation
}

function annotate(
  results,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  governmentEntityNormalization: Map<string, Map<string, string>>,
  subawardDepartments: Map<string, Map<string, string>>
) {
  let maxObligated: number = 0
  let totalObligated: number = 0

  for (const filing of results.filings) {
    annotateSubaward(
      filing,
      entities,
      entityNormalization,
      governmentEntityNormalization,
      subawardDepartments
    )
    const obligated = Number(filing.sub_amount)
    if (!isNaN(obligated)) {
      totalObligated += obligated
      maxObligated = Math.max(maxObligated, obligated)
    }
  }

  results['amountSummaries'] = {
    maxObligated: maxObligated,
    totalObligated: totalObligated,
  }
}

async function haveFromSearch(webSearch: string, dateRange: util.DateRange) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  const queryStr = `
    SELECT 1 FROM ${kTableName} WHERE ${fullTextSearchString}
     @@ websearch_to_tsquery('simple', $${params.length + 1})
     ${dateWhere} LIMIT 1000`
  params.push(webSearch)

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(webSearch: string, token: string) {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('sub_action_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['prime_key', 'sub_number', 'sub_name']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const whereStr = dateWhere ? `WHERE ${dateWhere}` : ''

  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('sub_action_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveForVendors(
  names: string[],
  normalizationLists,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
) {
  if (
    util.canAssumeRecordsExist(
      names,
      dateRange,
      entities,
      ['feeds', 'us', 'central', 'procurement'],
      'haveSubaward'
    )
  ) {
    return true
  }

  const nameList = normalizationLists['nameList']
  const ueiList = normalizationLists['ueiList']

  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  const subqueryHeader = `SELECT 1 FROM ${kTableName} WHERE `
  const subqueryFooter = `${dateWhere} LIMIT 1`

  let subqueries = []
  if (nameList.length) {
    subqueries.push(
      subqueryHeader +
        `prime_name = ANY($${params.length + 1})` +
        subqueryFooter
    )
    subqueries.push(
      subqueryHeader + `sub_name = ANY($${params.length + 1})` + subqueryFooter
    )
    params.push(nameList)
  }
  if (ueiList.length) {
    subqueries.push(
      subqueryHeader + `prime_uei = ANY($${params.length + 1})` + subqueryFooter
    )
    subqueries.push(
      subqueryHeader + `sub_uei = ANY($${params.length + 1})` + subqueryFooter
    )
    params.push(ueiList)
  }

  const innerQuery = subqueries
    .map((subquery) => `(${subquery})`)
    .join(' UNION ')

  // TODO(Jack Poulson): Check that the SELECT EXISTS isn't preferred.
  return await util.nonemptyQuery(dataSource.manager, innerQuery, params)
}

async function haveForAgencies(
  codes: Map<string, string[]>,
  dateRange: util.DateRange
) {
  if (
    !dateRange.hasStart() &&
    !dateRange.hasEnd() &&
    (codes.get('dept').length ||
      codes.get('agency').length ||
      codes.get('office').length)
  ) {
    return true
  }

  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  const subqueryHeader = `SELECT 1 FROM ${kTableName} WHERE `
  const subqueryFooter = dateWhere + ' LIMIT 10000'

  let subqueries = []
  let subqueryParamsList = []
  if (codes.get('office').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('office'))

    subqueries.push(
      `${subqueryHeader}
       lower(prime_awarding_office_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       lower(prime_funding_office_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }
  if (codes.get('agency').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('agency'))

    subqueries.push(
      `${subqueryHeader}
       lower(prime_awarding_sub_agency_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       lower(prime_funding_sub_agency_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }
  if (codes.get('dept').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('dept'))

    subqueries.push(
      `${subqueryHeader}
       lower(prime_awarding_agency_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       lower(prime_funding_agency_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }

  let manager = dataSource.manager
  for (const [index, subquery] of subqueries.entries()) {
    const subqueryParams = subqueryParamsList[index]
    const haveResult = await util.nonemptyQuery(
      manager,
      subquery,
      subqueryParams
    )
    if (haveResult) {
      return true
    }
  }
  return false
}

async function haveForVendor(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const normalizationLists = procurementUtil.getNameAndUEILists(
    name,
    names,
    entities
  )
  return await haveForVendors(names, normalizationLists, dateRange, entities)
}

async function haveForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  usFederalOffices: Map<string, Map<string, any>>,
  subawardDepartments: Map<string, Map<string, string>>
) {
  const codes = getAgencyCodes(
    name,
    entities,
    usFederalOffices,
    subawardDepartments
  )
  return await haveForAgencies(codes, dateRange)
}

async function getForVendors(
  names: string[],
  normalizationLists,
  dateRange: util.DateRange,
  maxSearchResults: number,
  onlyAsPrime = false,
  onlyAsSub = false,
  searchOffset = 0
) {
  const nameList = normalizationLists['nameList']
  const ueiList = normalizationLists['ueiList']

  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  const orderBy = ' ORDER BY sub_action_date DESC'

  let outerLimit = orderBy + ` LIMIT $${params.length + 1}`
  params.push(maxSearchResults)
  if (searchOffset > 0) {
    outerLimit += ` OFFSET $${params.length + 1}`
    params.push(searchOffset)
  }

  const innerLimit = orderBy + ` LIMIT $${params.length + 1}`
  params.push(maxSearchResults + searchOffset)

  const subqueryHeader = `SELECT * FROM ${kTableName} WHERE `
  const subqueryFooter = dateWhere + innerLimit

  let subqueries = []
  if (nameList.length) {
    if (!onlyAsSub) {
      subqueries.push(
        subqueryHeader +
          `prime_name = ANY($${params.length + 1})` +
          subqueryFooter
      )
    }
    if (!onlyAsPrime) {
      subqueries.push(
        subqueryHeader +
          `sub_name = ANY($${params.length + 1})` +
          subqueryFooter
      )
    }
    params.push(nameList)
  }
  if (ueiList.length) {
    if (!onlyAsSub) {
      subqueries.push(
        subqueryHeader +
          `prime_uei = ANY($${params.length + 1})` +
          subqueryFooter
      )
    }
    if (!onlyAsPrime) {
      subqueries.push(
        subqueryHeader + `sub_uei = ANY($${params.length + 1})` + subqueryFooter
      )
    }
    params.push(ueiList)
  }

  const innerQuery = subqueries
    .map((subquery) => `(${subquery})`)
    .join(' UNION ')

  if (!innerQuery) {
    console.log('Empty innerQuery in US federal subawards getForVendors.')
    return { filings: [], hitSearchCap: false }
  }

  const queryStr = `SELECT * FROM (${innerQuery}) AS subqueries ${outerLimit}`

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

// TODO(Jack Poulson): Add awardOrderingType
async function getForAgencies(
  codes: Map<string, string[]>,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  const orderBy = ' ORDER BY sub_action_date DESC'

  let outerLimit = orderBy + ` LIMIT $${params.length + 1}`
  params.push(maxSearchResults)
  if (searchOffset > 0) {
    outerLimit += ` OFFSET $${params.length + 1}`
    params.push(searchOffset)
  }

  const innerLimit = orderBy + ` LIMIT $${params.length + 1}`
  params.push(maxSearchResults + searchOffset)

  const subqueryHeader = `SELECT * FROM ${kTableName} WHERE `
  const subqueryFooter = dateWhere + innerLimit

  let subqueries = []
  if (codes.get('office').length) {
    subqueries.push(
      `${subqueryHeader}
       lower(prime_awarding_office_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueries.push(
      `${subqueryHeader}
       lower(prime_funding_office_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    params.push(codes.get('office'))
  }
  if (codes.get('agency').length) {
    subqueries.push(
      `${subqueryHeader}
       lower(prime_awarding_sub_agency_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueries.push(
      `${subqueryHeader}
       lower(prime_funding_sub_agency_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    params.push(codes.get('agency'))
  }
  if (codes.get('dept').length) {
    subqueries.push(
      `${subqueryHeader}
       lower(prime_awarding_agency_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueries.push(
      `${subqueryHeader}
       lower(prime_funding_agency_code) = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    params.push(codes.get('dept'))
  }

  const innerQuery = subqueries
    .map((subquery) => `(${subquery})`)
    .join(' UNION ')

  if (!innerQuery) {
    console.log('Empty innerQuery in US federal subawards getForAgencies.')
    return { filings: [], hitSearchCap: false }
  }

  const queryStr = `SELECT * FROM (${innerQuery}) AS subqueries ${outerLimit}`

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getForVendor(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  maxSearchResults: number,
  onlyAsPrime = false,
  onlyAsSub = false,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const normalizationLists = procurementUtil.getNameAndUEILists(
    name,
    names,
    entities
  )
  return await getForVendors(
    names,
    normalizationLists,
    dateRange,
    maxSearchResults,
    onlyAsPrime,
    onlyAsSub,
    searchOffset
  )
}

async function getForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  usFederalOffices: Map<string, Map<string, any>>,
  subawardDepartments: Map<string, Map<string, string>>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const codes = getAgencyCodes(
    name,
    entities,
    usFederalOffices,
    subawardDepartments
  )
  return await getForAgencies(codes, dateRange, maxSearchResults, searchOffset)
}

function simplifyDates(filings) {
  // Drop the timestamps from several date columns.
  const datesToSimplify = ['prime_base_action_date', 'sub_action_date']
  for (const dateName of datesToSimplify) {
    for (let filing of filings) {
      if (filing[dateName]) {
        filing[dateName] = filing[dateName].toISOString().substring(0, 10)
      }
    }
  }
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  usFederalOffices: Map<string, Map<string, any>>,
  subawardDepartments: Map<string, Map<string, string>>
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Binary US subawards w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have subawards w/ tag-text: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Binary US subawards w/ entity: ${name}`)
    result = await haveForVendor(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`Binary US subawards w/ agency: ${name}`)
    result = await haveForAgency(
      name,
      dateRange,
      entities,
      usFederalOffices,
      subawardDepartments
    )
  } else {
    console.log('Binary US subawards over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityNormalization: Map<string, string>,
  governmentEntityNormalization: Map<string, Map<string, string>>,
  usFederalOffices: Map<string, Map<string, any>>,
  subawardDepartments: Map<string, Map<string, string>>
) {
  const params = request.query

  const exportCSV = util.readBoolean(params.csv)

  const maxSearchResultsDefault = 5000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`US subawards w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`Subawards w/ tag text: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    const onlyAsPrime = util.readBoolean(params.onlyAsPrime)
    const onlyAsSub = util.readBoolean(params.onlyAsSub)
    console.log(`US subawards w/ entity: ${name}`)
    result = await getForVendor(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      maxSearchResults,
      onlyAsPrime,
      onlyAsSub,
      searchOffset
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`US subawards w/ agency: ${name}`)
    result = await getForAgency(
      name,
      dateRange,
      entities,
      usFederalOffices,
      subawardDepartments,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('US subawards over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(
      result,
      entities,
      entityNormalization,
      governmentEntityNormalization,
      subawardDepartments
    )
  }

  const csvFilename = 'us_central_subawards.csv'
  if (exportCSV) {
    simplifyDates(result['filings'])
  }
  util.setAPIResponse(response, result, exportCSV, csvFilename)
}

export function setRoutes(router, entityState) {
  const route = '/api/us/central/procurement/subaward'

  const feed = entityState.feeds.get('us').get('central').get('procurement')
  const entities = entityState.entities
  const children = entityState.children
  const usFederalOffices = entityState.usFederalOffices

  // Returns whether results exist for an entity or web search.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(
      request,
      response,
      entities,
      children,
      usFederalOffices,
      feed.subawardDepartments
    )
  })

  // Returns JSON for an entity or web search.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(
      request,
      response,
      entities,
      children,
      feed.normalization,
      feed.agencyNormalization,
      usFederalOffices,
      feed.subawardDepartments
    )
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
