import { dataSource } from '../../../App'
import { USCentralOpportunities } from '../../../entity/us/central/procurement/opportunities'
import * as tags from '../../../tags'
import * as util from '../../../util'

const kTable = USCentralOpportunities
const kTableName = 'us_central_opportunities'

const kTagDataset = 'us_central_opportunities'

const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(notice_id)',
  'lower(title)',
  'lower(solicitation_number)',
  'lower(department)',
  'lower(fpds_code)',
  'lower(office)',
  'lower(naics_code)',
  'lower(pop_street_address)',
  'lower(pop_city)',
  'lower(pop_state)',
  'lower(pop_zip)',
  'lower(pop_country)',
  'lower(award_number)',
  'lower(awardee)',
  'lower(primary_contact_title)',
  'lower(primary_contact_name)',
  'lower(primary_contact_email)',
  'lower(primary_contact_phone)',
  'lower(primary_contact_fax)',
  'lower(secondary_contact_title)',
  'lower(secondary_contact_name)',
  'lower(secondary_contact_email)',
  'lower(secondary_contact_phone)',
  'lower(secondary_contact_fax)',
  'lower(state)',
  'lower(city)',
  'lower(zip)',
  'lower(country_code)',
  'lower(description)',
  'text',
])

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let dateWhere = ''
  if (dateRange.hasStart()) {
    dateWhere += ` AND posted_date >= $${params.length + 1}`
    params.push(dateRange.start)
  }
  if (dateRange.hasEnd()) {
    dateWhere += ` AND posted_date <= $${params.length + 1}`
    params.push(dateRange.end)
  }
  return dateWhere
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  const queryStr = `
    SELECT 1 FROM ${kTableName} WHERE ${fullTextSearchString}
    @@ websearch_to_tsquery('simple', $${params.length + 1})
    ${dateWhere} LIMIT 10000`
  params.push(webSearch)

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(webSearch: string, token: string) {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateFiling(
  filing,
  entities: Map<string, Map<string, any>>,
  normalization: Map<string, string>,
  agencyNormalization: Map<string, string>
) {
  function convertOfficeCode(code) {
    // The office code can contain letters.
    return code.toLowerCase()
  }

  function convertAgencyCode(code) {
    // The agency code can contain letters.
    return code.toLowerCase()
  }

  function convertDepartmentCode(code) {
    // At the moment, the strings come in a numeric format, apparently due to
    // backend Pandas inference. Once that inference is fixed, we should change
    // this code accordingly.
    return code.split('.', 1)[0] + '00'
  }

  function agencyAnnotationFromComponents(
    department: string,
    departmentCode: string,
    agency: string,
    agencyCode: string,
    office: string,
    officeCode: string
  ) {
    const convertedDepartmentCode = convertDepartmentCode(departmentCode)
    const convertedAgencyCode = convertAgencyCode(agencyCode)
    const convertedOfficeCode = convertOfficeCode(officeCode)
    let agencyNorm = agency
    if (agencyNormalization.get('office').has(convertedOfficeCode)) {
      agencyNorm = agencyNormalization.get('office').get(convertedOfficeCode)
    } else if (agencyNormalization.get('agency').has(convertedAgencyCode)) {
      agencyNorm = agencyNormalization.get('agency').get(convertedAgencyCode)
    } else if (agencyNormalization.get('dept').has(convertedDepartmentCode)) {
      agencyNorm = agencyNormalization.get('dept').get(convertedDepartmentCode)
    }

    return {
      department: department,
      agency: agency,
      office: office,
      agencyNorm: agencyNorm,
      agencyStylized: util.getFullyStylizedName(agencyNorm, entities),
      agencyLogo: util.logoURLFromAncestors(agencyNorm, entities),
    }
  }

  filing['annotation'] = {}
  if (filing.awardee) {
    const vendor = filing.awardee
    const normalizedVendor = util.normalizeEntity(vendor, normalization)
    filing['annotation']['awardee'] = {
      origText: vendor,
      text: normalizedVendor,
      stylizedText: util.getFullyStylizedName(normalizedVendor, entities),
      url: util.encodeEntity(normalizedVendor),
      logo: util.logoURLFromAncestors(normalizedVendor, entities),
    }
  }
  if (filing.department) {
    const dept = util.coerceLower(filing.department)
    const agency = util.coerceLower(filing.subtier)
    const office = util.coerceLower(filing.office)
    filing['annotation']['contracting'] = agencyAnnotationFromComponents(
      filing.department,
      filing.cgac,
      filing.subtier,
      filing.fpds_code,
      filing.office,
      filing.aac_code
    )
  }
}

function annotate(
  result,
  entities: Map<string, Map<string, any>>,
  normalization: Map<string, string>,
  agencyNormalization: Map<string, string>
) {
  for (let filing of result['filings']) {
    annotateFiling(filing, entities, normalization, agencyNormalization)
  }
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + " @@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  if (dateRange.hasStart()) {
    query = query.andWhere('posted_date >= :startDate', {
      startDate: dateRange.start,
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('posted_date <= :endDate', {
      endDate: dateRange.end,
    })
  }
  query = query.orderBy('posted_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['notice_id', 'title']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const whereStr = dateWhere ? `WHERE ${dateWhere}` : ''

  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  if (dateRange.hasStart()) {
    query = query.andWhere('posted_date >= :startDate', {
      startDate: dateRange.start,
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('posted_date <= :endDate', {
      endDate: dateRange.end,
    })
  }
  query = query.orderBy('posted_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(request, response) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Have opportunities w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have opportunities w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    // TODO(Jack Poulson)
  } else {
    console.log('Have opportunities over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  normalization: Map<string, string>,
  agencyNormalization: Map<string, string>
) {
  const params = request.query

  const maxSearchResultsDefault = 1000
  const maxSearchResultsCap = 10000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`Contract opportunities w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`Opportunities w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    // TODO(Jack Poulson)
  } else {
    console.log('Contract opportunities over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, normalization, agencyNormalization)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/us/central/procurement/opportunity'
  const entities = entityState.entities
  const usCentralFeeds = entityState.feeds.get('us').get('central')
  const feed = usCentralFeeds.get('opportunities')
  const agencyNormalization =
    usCentralFeeds.get('procurement').agencyNormalization

  // Returns whether results exist for a web search.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response)
  })

  // Returns JSON for a web search.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(
      request,
      response,
      entities,
      feed.normalization,
      agencyNormalization
    )
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
