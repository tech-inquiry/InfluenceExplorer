import { dataSource } from '../../App'
import * as procurementUtil from './procurement/util'
import * as tags from '../../tags'
import * as util from '../../util'

import { USCentralProcurementPrime } from '../../entity/us/central/procurement/prime'

const kTable = USCentralProcurementPrime
const kTableName = 'us_central_procurement'

const kTagDataset = 'us_central_procurement'

const fullTextSearchString = 'tsvector_col'

// A map from potential filing.contract_action_type values to their stylized
// display and associated URL.
const actionTypeMap = {
  idc: {
    stylizedText: 'Indefinite Delivery Contract',
    url: 'https://www.fpds.gov/help/Indefinite_Delivery_Contract.htm',
  },
  bpa: {
    stylizedText: 'Blanket Purchase Agreement (BPA)',
    url: 'https://www.fpds.gov/help/Create_a_BPA.htm',
  },
  'bpa call': {
    stylizedText: 'Blanket Purchase Agreement (BPA) Call',
    url: 'https://www.fpds.gov/help/Create_a_BPA_Call.htm',
  },
  'delivery order': {
    stylizedText: 'Delivery Order',
    url: 'https://www.fpds.gov/help_V1_0/Indefinite_Delivery_Contract.htm',
  },
  'definitive contract': {
    stylizedText: 'Definitive Contract',
    url: 'https://www.acquisition.gov/far/4.601',
  },
  fss: {
    stylizedText: 'Federal Supply Schedule (FSS)',
    url: 'https://www.fpds.gov/help/Create_an_FSS.htm',
  },
  'other transaction agreement': {
    stylizedText: 'Other Transaction Agreement',
    url: 'https://www.fpds.gov/help/Create_an_OTAgreement.htm',
  },
  'other transaction idv': {
    stylizedText: 'Other Transaction Indefinite Delivery Vehicle (OT IDV)',
    url: 'https://www.fpds.gov/help/Create_an_OTIDV.htm',
  },
  'purchase order': {
    stylizedText: 'Purchase Order',
    url: 'https://www.fpds.gov/help/Create_a_PO.htm',
  },
}

function union(a: any[], b: any[]): any[] {
  let filings = []
  let idSet = new Set<number>()
  for (const filing of a) {
    filings.push({ ...filing })
    idSet.add(filing.id)
  }
  for (const filing of b) {
    if (!idSet.has(filing.id)) {
      filings.push({ ...filing })
      idSet.add(filing.id)
    }
  }
  return filings
}

function sortByDate(filings: any[], awardOrderingType: string) {
  const dateKey =
    awardOrderingType == util.AwardOrderingType.MODIFIED_DATE
      ? 'modified_date'
      : 'signed_date'
  filings.sort(function (a, b) {
    if (a[dateKey] > b[dateKey]) {
      return -1
    } else if (a[dateKey] < b[dateKey]) {
      return 1
    } else {
      return 0
    }
  })
}

function combineAndTruncate(
  a: any[],
  b: any[],
  maxEntries: number,
  awardOrderingType: string
): any[] {
  console.log('    Concatenating and removing duplicates...')
  let combined = union(a, b)
  console.log('    Sorting by date...')
  sortByDate(combined, awardOrderingType)

  return [...combined.slice(0, maxEntries)]
}

function buildDateConstraint(
  dateRange: util.DateRange,
  awardOrderingType,
  params
): string {
  const lines = []
  const dateColumn =
    awardOrderingType == util.AwardOrderingType.MODIFIED_DATE
      ? 'modified_date'
      : 'signed_date'
  if (dateRange.hasStart()) {
    lines.push(`${dateColumn} >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    lines.push(`${dateColumn} <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return lines.join(' AND ')
}

function incorporateDateConstraint(
  dateRange: util.DateRange,
  awardOrderingType,
  query
) {
  const dateColumn =
    awardOrderingType == util.AwardOrderingType.MODIFIED_DATE
      ? 'modified_date'
      : 'signed_date'
  if (dateRange.hasStart()) {
    query = query.andWhere(`${dateColumn} >= :startDate`, {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere(`${dateColumn} <= :endDate`, {
      endDate: dateRange.end.toISOString(),
    })
  }
}

function incorporateDateOrdering(awardOrderingType, query) {
  const dateColumn =
    awardOrderingType == util.AwardOrderingType.MODIFIED_DATE
      ? 'modified_date'
      : 'signed_date'
  query = query.orderBy(dateColumn, 'DESC')
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateAward(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  contractorNormalization: Map<string, string>,
  governmentEntityNormalization: Map<string, Map<string, string>>,
  fpdsProductOrServiceCodes: Map<string, Map<string, string[]>>
) {
  // Get the vendor annotations.
  const vendor = util.normalizeEntityWithUEI(
    filing.vendor,
    filing.uei,
    entityNormalization
  )
  let annotation = {
    vendor: {
      origText: filing.vendor,
      text: vendor,
      stylizedText: util.getFullyStylizedName(vendor, entities),
      logo: util.logoURLFromAncestors(vendor, entities),
    },
  }
  if (filing.contractor) {
    let contractor = filing.contractor
    if (contractorNormalization.has(contractor)) {
      contractor = util.normalizeEntity(contractor, contractorNormalization)
    } else {
      contractor = util.normalizeEntityWithUEI(
        contractor,
        undefined,
        entityNormalization
      )
    }
    annotation['contractor'] = {
      origText: filing.contractor,
      text: contractor,
      stylizedText: util.getFullyStylizedName(contractor, entities),
      logo: util.logoURLFromAncestors(contractor, entities),
    }
  }

  if (filing.product_or_service_code) {
    const code = filing.product_or_service_code
    if (fpdsProductOrServiceCodes.get('product_or_service_codes').has(code)) {
      annotation['product_or_service'] = fpdsProductOrServiceCodes
        .get('product_or_service_codes')
        .get(code)[0]
    }
  }
  if (filing.principal_naics_code) {
    const code = filing.principal_naics_code
    if (fpdsProductOrServiceCodes.get('naics_codes').has(code)) {
      annotation['naics'] = fpdsProductOrServiceCodes
        .get('naics_codes')
        .get(code)[0]
    }
  }

  // NOTE: This is minutely different from the version for subawards.
  function agencyAnnotationFromComponents(
    department: string,
    departmentCode: string,
    agency: string,
    agencyCode: string,
    office: string,
    officeCode: string
  ) {
    let agencyNorm = agency
    if (governmentEntityNormalization.get('office').has(officeCode)) {
      agencyNorm = governmentEntityNormalization.get('office').get(officeCode)
    } else if (governmentEntityNormalization.get('agency').has(agencyCode)) {
      agencyNorm = governmentEntityNormalization.get('agency').get(agencyCode)
    } else if (governmentEntityNormalization.get('dept').has(departmentCode)) {
      agencyNorm = governmentEntityNormalization.get('dept').get(departmentCode)
    }

    return {
      department: department,
      agency: agency,
      office: office,
      agencyNorm: agencyNorm,
      agencyStylized: util.getFullyStylizedName(agencyNorm, entities),
      agencyLogo: util.logoURLFromAncestors(agencyNorm, entities),
    }
  }

  annotation['contracting'] = agencyAnnotationFromComponents(
    filing.contracting_department,
    filing.contracting_department_code,
    filing.contracting_agency,
    filing.contracting_agency_code,
    filing.contracting_office,
    filing.contracting_office_code
  )
  annotation['funding'] = agencyAnnotationFromComponents(
    filing.funding_department,
    filing.funding_department_code,
    filing.funding_agency,
    filing.funding_agency_code,
    filing.funding_office,
    filing.funding_office_code
  )

  if (filing.place_of_performance) {
    let str: string = ''
    const pop = filing.place_of_performance
    const haveZip =
      'placeOfPerformanceZIPCode' in pop && pop['placeOfPerformanceZIPCode']
    const havePPOP =
      'principalPlaceOfPerformance' in pop && pop['principalPlaceOfPerformance']
    if (haveZip && havePPOP) {
      const zip = pop['placeOfPerformanceZIPCode']
      const ppop = pop['principalPlaceOfPerformance']

      // Add in the city.
      if (!util.isString(zip) && '@city' in zip && zip['@city']) {
        str += zip['@city']
      }

      // Add in the county.
      if (!util.isString(zip) && '@county' in zip && zip['@county']) {
        const county = zip['@county']
        if (county.endsWith('COUNTY')) {
          str += ` (${county})`
        } else {
          str += ` (${county} COUNTY)`
        }
      }

      // Add in the state.
      if ('stateCode' in ppop) {
        const state = ppop['stateCode']
        if (str) {
          str += ', '
        }
        if (util.isString(state)) {
          if (state) {
            str += state + ' '
          }
        } else if ('@name' in state) {
          if (state['@name']) {
            str += state['@name'] + ' '
          }
        }
      } else if (str) {
        str += ', '
      }

      // Add in the country.
      if ('countryCode' in ppop) {
        const country = ppop['countryCode']
        if (!util.isString(zip) && '#text' in zip && zip['#text']) {
          str += zip['#text'] + ' '
        }
        if (util.isString(country)) {
          str += country
        } else if ('@name' in country) {
          str += country['@name']
        }
      }
    } else if (havePPOP) {
      const ppop = pop['principalPlaceOfPerformance']

      // Add in the state.
      if ('stateCode' in ppop) {
        const state = ppop['stateCode']
        if (state) {
          if (util.isString(state)) {
            str = state + ', '
          } else if ('@name' in state) {
            str = state['@name'] + ', '
          }
        }
      }

      // Add in the country.
      if ('countryCode' in ppop) {
        const country = ppop['countryCode']
        if (util.isString(country)) {
          str += country
        } else if ('@name' in country) {
          str += country['@name']
        }
      }
    } else {
      str = JSON.stringify(pop, null, 1)
    }

    annotation['placeOfPerformance'] = str
  }

  if (filing.contract_action_type) {
    if (filing.contract_action_type in actionTypeMap) {
      annotation['actionType'] = actionTypeMap[filing.contract_action_type]
    } else {
      annotation['actionType'] = { stylizedText: filing.contract_action_type }
    }
  }

  filing['annotation'] = annotation
}

function annotate(
  awards,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  contractorNormalization: Map<string, string>,
  governmentEntityNormalization: Map<string, Map<string, string>>,
  fpdsProductOrServiceCodes: Map<string, Map<string, string[]>>
) {
  // TODO(Jack Poulson): Re-enable a summation of the potential values after
  // accounting for parent award duplication.

  let maxObligated: number = 0
  let totalObligated: number = 0
  let totalObligatedByContracting = {}
  let totalObligatedByFunding = {}
  let maxCurrent: number = 0
  let totalCurrent: number = 0
  let maxPotential: number = 0
  let mostRecentUEI: string = undefined
  let mostRecentUEISignedDate: Date = undefined
  for (const filing of awards['filings']) {
    annotateAward(
      filing,
      entities,
      entityNormalization,
      contractorNormalization,
      governmentEntityNormalization,
      fpdsProductOrServiceCodes
    )

    const obligated = Number(filing.obligated_amount)
    if (!isNaN(obligated)) {
      totalObligated += obligated
      maxObligated = Math.max(maxObligated, obligated)

      const contractingEntity =
        filing.annotation['contracting']['agencyStylized']
      if (!totalObligatedByContracting.hasOwnProperty(contractingEntity)) {
        totalObligatedByContracting[contractingEntity] = 0
      }
      totalObligatedByContracting[contractingEntity] += obligated

      const fundingEntity = filing.annotation['funding']['agencyStylized']
      if (!totalObligatedByFunding.hasOwnProperty(fundingEntity)) {
        totalObligatedByFunding[fundingEntity] = 0
      }
      totalObligatedByFunding[fundingEntity] += obligated
    }

    const current = Number(filing.base_and_exercised_options_value)
    if (!isNaN(current)) {
      totalCurrent += current
      maxCurrent = Math.max(maxCurrent, current)
    }

    const potential = Number(filing.base_and_all_options_value)
    if (!isNaN(potential)) {
      maxPotential = Math.max(maxPotential, potential)
    }

    if (
      mostRecentUEI == undefined ||
      (filing.uei !== undefined && filing.signed_date > mostRecentUEISignedDate)
    ) {
      mostRecentUEI = filing.uei
      mostRecentUEISignedDate = filing.signed_date
    }
  }

  function sortDictDescending(origDict) {
    return Object.entries(origDict).sort((a, b) => b[1] - a[1])
  }

  awards['amountSummaries'] = {
    maxObligated: maxObligated,
    totalObligated: totalObligated,
    totalObligatedByContractingEntity: sortDictDescending(
      totalObligatedByContracting
    ),
    totalObligatedByFundingEntity: sortDictDescending(totalObligatedByFunding),
    maxCurrent: maxCurrent,
    totalCurrent: totalCurrent,
    maxPotential: maxPotential,
  }
  awards['mostRecentUEI'] = mostRecentUEI
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  awardOrderingType: string
) {
  let params = []

  const searchConstraint = `${fullTextSearchString}
    @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)
  const dateConstraint = buildDateConstraint(
    dateRange,
    awardOrderingType,
    params
  )
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  let queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(webSearch: string, token: string) {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  awardOrderingType: string,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, awardOrderingType, query)
  incorporateDateOrdering(awardOrderingType, query)
  query = query
    .addOrderBy('total_base_and_all_options_value', 'DESC')
    .limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = [
    'uei',
    'piid',
    'mod_number',
    'parent_piid',
    'parent_mod_number',
  ]
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(
  dateRange: util.DateRange,
  awardOrderingType: string
) {
  let params = []

  const dateConstraint = buildDateConstraint(
    dateRange,
    awardOrderingType,
    params
  )
  const constraintStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  let queryStr = `SELECT 1 FROM ${kTableName} ${constraintStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  awardOrderingType: string,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, awardOrderingType, query)
  incorporateDateOrdering(awardOrderingType, query)
  query = query
    .addOrderBy('total_base_and_all_options_value', 'DESC')
    .limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveForVendors(
  names: string[],
  normalizationLists,
  awardOrderingType: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
) {
  if (
    util.canAssumeRecordsExist(
      names,
      dateRange,
      entities,
      ['feeds', 'us', 'central', 'procurement'],
      'havePrimeAward'
    )
  ) {
    return true
  }

  const nameList = normalizationLists['nameList']
  const ueiList = normalizationLists['ueiList']
  const contractors = normalizationLists['contractors']

  let params = []

  const dateWhere = buildDateConstraint(dateRange, awardOrderingType, params)

  // TODO(Jack Poulson): Refactor to execute individual subqueries rather
  // than performing a UNION.

  const subqueryHeader = `SELECT 1 FROM ${kTableName} WHERE`
  const subqueryFooter =
    (dateWhere ? `AND (${dateWhere}) ` : '') + 'LIMIT 10000'

  let subqueries = []
  if (nameList.length) {
    subqueries.push(
      `${subqueryHeader} vendor = ANY($${params.length + 1}) ${subqueryFooter}`
    )
    params.push(nameList)
  }
  if (ueiList.length) {
    subqueries.push(
      `${subqueryHeader} uei = ANY($${params.length + 1}) ${subqueryFooter}`
    )
    params.push(ueiList)
  }
  if (contractors.length) {
    subqueries.push(
      `${subqueryHeader} contractor = ANY($${
        params.length + 1
      }) ${subqueryFooter}`
    )
    params.push(contractors)
  }

  const innerQuery = subqueries
    .map((subquery) => `(${subquery})`)
    .join(' UNION ')

  // TODO(Jack Poulson): Check if the SELECT EXISTS was preferred.
  return await util.nonemptyQuery(dataSource.manager, innerQuery, params)
}

// Get whether the named dept/agency/office codes have any contracts.
async function haveForAgencies(
  codes: Map<string, string[]>,
  awardOrderingType: string,
  dateRange: util.DateRange
) {
  if (
    !dateRange.hasStart() &&
    !dateRange.hasEnd() &&
    (codes.get('dept').length ||
      codes.get('agency').length ||
      codes.get('office').length)
  ) {
    return true
  }

  let params = []

  const dateWhere = buildDateConstraint(dateRange, awardOrderingType, params)

  const subqueryHeader = `SELECT 1 FROM ${kTableName} WHERE `
  const subqueryFooter =
    (dateWhere ? ` AND (${dateWhere}) ` : '') + ' LIMIT 10000'

  let subqueries = []
  let subqueryParamsList = []
  if (codes.get('office').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('office'))

    subqueries.push(
      `${subqueryHeader}
       contracting_office_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       funding_office_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }
  if (codes.get('agency').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('agency'))

    subqueries.push(
      `${subqueryHeader}
       contracting_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       funding_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }
  if (codes.get('dept').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('dept'))

    subqueries.push(
      `${subqueryHeader}
       contracting_department_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       funding_department_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }

  let manager = dataSource.manager
  for (const [index, subquery] of subqueries.entries()) {
    const subqueryParams = subqueryParamsList[index]
    const haveResult = await util.nonemptyQuery(
      manager,
      subquery,
      subqueryParams
    )
    if (haveResult) {
      return true
    }
  }
  return false
}

async function haveForVendor(
  name: string,
  excludeSubsidiaries: boolean,
  awardOrderingType: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const normalizationLists = procurementUtil.getNameAndUEILists(
    name,
    names,
    entities
  )
  return await haveForVendors(
    names,
    normalizationLists,
    awardOrderingType,
    dateRange,
    entities
  )
}

async function haveForAgency(
  name: string,
  awardOrderingType: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  usFederalOffices: Map<string, Map<string, any>>
) {
  const codes = procurementUtil.getAgencyCodes(name, entities, usFederalOffices)
  return await haveForAgencies(codes, awardOrderingType, dateRange)
}

// Get the list of contracts where the named entities were vendors.
async function getForVendors(
  names: string[],
  normalizationLists,
  awardOrderingType: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const nameList = normalizationLists['nameList']
  const ueiList = normalizationLists['ueiList']
  const contractors = normalizationLists['contractors']

  let params = []

  const dateWhere = buildDateConstraint(dateRange, awardOrderingType, params)

  let orderBy = ''
  if (awardOrderingType == util.AwardOrderingType.MODIFIED_DATE) {
    orderBy = 'ORDER BY modified_date DESC'
  } else {
    orderBy = 'ORDER BY signed_date DESC'
  }

  let outerLimit = `${orderBy} LIMIT $${params.length + 1}`
  params.push(maxSearchResults)
  if (searchOffset > 0) {
    outerLimit += ` OFFSET $${params.length + 1}`
    params.push(searchOffset)
  }

  const innerLimit = `${orderBy} LIMIT $${params.length + 1}`
  params.push(maxSearchResults + searchOffset)

  const subqueryHeader = `SELECT * FROM ${kTableName} WHERE`
  const subqueryFooter = (dateWhere ? `AND (${dateWhere}) ` : '') + innerLimit

  let subqueries = []
  if (nameList.length) {
    subqueries.push(
      `${subqueryHeader} vendor = ANY($${params.length + 1}) ${subqueryFooter}`
    )
    params.push(nameList)
  }
  if (ueiList.length) {
    subqueries.push(
      `${subqueryHeader} uei = ANY($${params.length + 1}) ${subqueryFooter}`
    )
    params.push(ueiList)
  }
  if (contractors.length) {
    subqueries.push(
      `${subqueryHeader} contractor = ANY($${
        params.length + 1
      }) ${subqueryFooter}`
    )
    params.push(contractors)
  }

  const innerQuery = subqueries
    .map((subquery) => `(${subquery})`)
    .join(' UNION ')

  let queryStr = `SELECT * FROM (${innerQuery}) AS subqueries ${outerLimit}`

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

// Get the list of contracts where the named entities were contracting agencies.
async function getForAgencies(
  codes: Map<string, string[]>,
  awardOrderingType: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  maxTargetsPerQuery = 20
) {
  let params = []
  const dateWhere = buildDateConstraint(dateRange, awardOrderingType, params)

  const sortKey =
    awardOrderingType == util.AwardOrderingType.MODIFIED_DATE
      ? 'modified_date'
      : 'signed_date'
  const innerLimit = `ORDER BY ${sortKey} DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  const subqueryHeader = `SELECT * FROM ${kTableName} WHERE`
  const subqueryFooter = (dateWhere ? `AND (${dateWhere}) ` : '') + innerLimit

  let manager = dataSource.manager
  let result = { filings: [], hitSearchCap: false }
  let subqueries = []
  let subqueryParamsList = []
  if (codes.get('office').length) {
    const offices = codes.get('office')
    for (
      let sliceBeg = 0;
      sliceBeg < offices.length;
      sliceBeg += maxTargetsPerQuery
    ) {
      const sliceEnd = Math.min(sliceBeg + maxTargetsPerQuery, offices.length)
      if (offices.length > maxTargetsPerQuery) {
        console.log(
          `  Performing office query over [${sliceBeg}, ${sliceEnd}).`
        )
      }
      let subqueryParams = [...params]
      subqueryParams.push(offices.slice(sliceBeg, sliceEnd))

      subqueries.push(
        `${subqueryHeader}
         contracting_office_code = ANY($${params.length + 1})
         ${subqueryFooter}`
      )
      subqueryParamsList.push(subqueryParams)

      subqueries.push(
        `${subqueryHeader}
         funding_office_code = ANY($${params.length + 1})
         ${subqueryFooter}`
      )
      subqueryParamsList.push(subqueryParams)
    }
  }
  if (codes.get('agency').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('agency'))

    subqueries.push(
      `${subqueryHeader}
       contracting_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       funding_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }
  if (codes.get('dept').length) {
    // We force one department code at a time because, in the case of the GSA,
    // which has department code '4700', there is a two-order-of-magnitude
    // slowdown when querying with ANY(ARRAY['4700']) instead of '4700'.
    for (const code of codes.get('dept')) {
      let subqueryParams = [...params]
      subqueryParams.push(code)

      subqueries.push(
        `${subqueryHeader}
         contracting_department_code = $${params.length + 1}
         ${subqueryFooter}`
      )
      subqueryParamsList.push(subqueryParams)

      subqueries.push(
        `${subqueryHeader}
         funding_department_code = $${params.length + 1}
         ${subqueryFooter}`
      )
      subqueryParamsList.push(subqueryParams)
    }
  }

  try {
    let filings = []
    for (let index = 0; index < subqueries.length; ++index) {
      const subquery = subqueries[index]
      const subqueryParams = subqueryParamsList[index]
      if (index == 0) {
        filings = await manager.query(subquery, subqueryParams)
      } else {
        console.log(`  Subquery ${index} of ${subqueries.length}.`)
        const additionalFilings = await manager.query(subquery, subqueryParams)
        filings = combineAndTruncate(
          filings,
          additionalFilings,
          maxSearchResults,
          awardOrderingType
        )
      }
    }

    result = {
      filings: filings,
      hitSearchCap: filings.length == maxSearchResults,
    }
  } catch (err) {
    console.log(err)
  }

  return result
}

async function getForVendor(
  name: string,
  excludeSubsidiaries: boolean,
  awardOrderingType: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const normalizationLists = procurementUtil.getNameAndUEILists(
    name,
    names,
    entities
  )
  return await getForVendors(
    names,
    normalizationLists,
    awardOrderingType,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function getForAgency(
  name: string,
  awardOrderingType: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  usFederalOffices: Map<string, Map<string, any>>,
  maxSearchResults: number,
  maxTargetsPerQuery = 20
) {
  const codes = procurementUtil.getAgencyCodes(name, entities, usFederalOffices)
  return await getForAgencies(
    codes,
    awardOrderingType,
    dateRange,
    maxSearchResults,
    maxTargetsPerQuery
  )
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  usFederalOffices: Map<string, Map<string, any>>
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const awardOrderingType = util.readAwardOrderingType(params.useModifiedDate)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Have US federal prime awards w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange, awardOrderingType)
  } else if (tagText) {
    console.log(`Have US federal prime awards w/ tag-text: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Have US federal prime awards w/ entity: ${name}`)
    result = await haveForVendor(
      name,
      excludeSubsidiaries,
      awardOrderingType,
      dateRange,
      entities,
      entityChildren
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`Have US federal prime awards w/ agency: ${name}`)
    result = await haveForAgency(
      name,
      awardOrderingType,
      dateRange,
      entities,
      usFederalOffices
    )
  } else {
    console.log('Have US federal prime awards over date range')
    result = await haveOverDateRange(dateRange, awardOrderingType)
  }

  util.setJSONResponse(response, result)
}

function simplifyDates(filings) {
  // Drop the timestamps from several date columns.
  const datesToSimplify = [
    'signed_date',
    'effective_date',
    'modified_date',
    'current_completion_date',
    'ultimate_completion_date',
    'last_date_to_order',
  ]
  for (const dateName of datesToSimplify) {
    for (let filing of filings) {
      if (filing[dateName]) {
        filing[dateName] = filing[dateName].toISOString().substring(0, 10)
      }
    }
  }
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityNormalization: Map<string, string>,
  contractorNormalization: Map<string, string>,
  governmentEntityNormalization: Map<string, Map<string, string>>,
  usFederalOffices: Map<string, Map<string, any>>,
  fpdsProductOrServiceCodes: Map<string, Map<string, string[]>>
) {
  const params = request.query

  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token
  const exportCSV = util.readBoolean(params.csv)
  const dateRange = util.getDateRange(params)
  const awardOrderingType = util.readAwardOrderingType(params.useModifiedDate)

  const maxSearchResultsDefault = 2500
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`US federal prime awards w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      awardOrderingType,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`US federal prime awards w/ tag text: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`US federal prime awards w/ entity: ${name}`)
    result = await getForVendor(
      name,
      excludeSubsidiaries,
      awardOrderingType,
      dateRange,
      entities,
      entityChildren,
      maxSearchResults,
      searchOffset
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`US federal prime awards w/ agency: ${name}`)
    result = await getForAgency(
      name,
      awardOrderingType,
      dateRange,
      entities,
      usFederalOffices,
      maxSearchResults
    )
  } else {
    console.log('US federal prime awards over date range temporarily disabled')
    if (false) {
      result = await getOverDateRange(
        dateRange,
        awardOrderingType,
        maxSearchResults,
        searchOffset
      )
    }
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(
      result,
      entities,
      entityNormalization,
      contractorNormalization,
      governmentEntityNormalization,
      fpdsProductOrServiceCodes
    )
  }

  const csvFilename = 'us_central_procurement.csv'
  if (exportCSV) {
    simplifyDates(result['filings'])
  }
  util.setAPIResponse(response, result, exportCSV, csvFilename)
}

export function setRoutes(router, entityState, fpdsProductOrServiceCodes) {
  const route = '/api/us/central/procurement'
  const feed = entityState.feeds.get('us').get('central').get('procurement')
  const entities = entityState.entities
  const children = entityState.children
  const usFederalOffices = entityState.usFederalOffices

  // Returns JSON for a web search or an entity.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(
      request,
      response,
      entities,
      children,
      feed.normalization,
      feed.contractorNormalization,
      feed.agencyNormalization,
      usFederalOffices,
      fpdsProductOrServiceCodes
    )
  })

  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, usFederalOffices)
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
