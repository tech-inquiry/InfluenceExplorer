import { dataSource } from '../../App'
import * as tags from '../../tags'
import * as util from '../../util'

import { USTeixeira } from '../../entity/us/central/teixeira'

const kTable = USTeixeira
const kTableName = 'us_teixeira'

const kTagDataset = 'us_teixeira'

const fullTextSearchString = "to_tsvector('simple', lower(text))"

function getReferences(
  name: string,
  entities: Map<string, Map<string, any>>,
  children: Map<string, string[]>
) {
  const descendants = util.expandToEntitySubtree(name, false, children)

  let references = new Set<string>()
  for (const descendant of descendants) {
    if (!entities.has(descendant)) {
      continue
    }
    const profile = entities.get(descendant)
    const entityRefs = util.jsonGetFromPath(profile, [
      'feeds',
      'us',
      'central',
      'teixeira',
      'references',
    ])
    if (entityRefs) {
      for (const emailID of entityRefs) {
        if (util.isString(emailID)) {
          references.add(emailID)
        } else if (emailID.has('id')) {
          references.add(emailID.get('id'))
        }
      }
    }
  }

  return Array.from(references)
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

async function haveFromSearch(webSearch: string): Promise<boolean> {
  let params = []

  const queryStr = `
    SELECT 1 FROM ${kTableName} WHERE ${fullTextSearchString}
    @@ websearch_to_tsquery('simple', $${params.length + 1})
    LIMIT 1000`
  params.push(webSearch)

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function haveForName(
  name: string,
  entities: Map<string, Map<string, any>>,
  children: Map<string, string[]>
): Promise<boolean> {
  const references = getReferences(name, entities, children)
  return references.length > 0
}

async function getFromSearch(
  webSearch: string,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + " @@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  query = query.orderBy('reference_id', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['reference_id']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function getForName(
  name: string,
  entities: Map<string, Map<string, any>>,
  children: Map<string, string[]>,
  maxSearchResults: number
) {
  const references = getReferences(name, entities, children)

  let params = []

  const searchConstraint = `reference_id = ANY($${params.length + 1})`
  params.push(references)

  const queryStr = `SELECT * FROM ${kTableName} WHERE ${searchConstraint} ORDER BY reference_id DESC LIMIT $${
    params.length + 1
  }`
  params.push(maxSearchResults)

  const filings = await dataSource.manager.query(queryStr, params)
  return { filings: filings, hitSearchCap: filings.length == maxSearchResults }
}

async function get(maxSearchResults: number, searchOffset = 0) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  query = query.limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(request, response, entities, children) {
  const params = request.query

  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Have Teixeira leak w/ text: ${text}`)
    result = await haveFromSearch(text)
  } else if (tagText) {
    console.log(`Have Teixeira w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const entity = params.entity
    console.log(`Have Teixeira leak w/ entity: ${entity}`)
    result = await haveForName(entity, entities, children)
  } else {
    result = true
  }

  util.setJSONResponse(response, result)
}

async function getAPI(request, response, entities, children) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`Teixeira leak w/ text: ${text}`)
    result = await getFromSearch(text, maxSearchResults, searchOffset)
  } else if (tagText) {
    console.log(`Teixeira w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const entity = params.entity
    console.log(`Teixeira leak w/ entity: ${entity}`)
    result = await getForName(entity, entities, children, maxSearchResults)
  } else {
    console.log('Teixeira leak')
    result = await get(maxSearchResults, searchOffset)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const parentRoute = '/api/us/central'

  const entities = entityState.entities
  const children = entityState.children

  // Returns whether data exists for a web search.
  router.get(`${parentRoute}/haveTeixeira`, async function (request, response) {
    await haveAPI(request, response, entities, children)
  })

  // Returns JSON for a web search.
  router.get(`${parentRoute}/teixeira`, async function (request, response) {
    await getAPI(request, response, entities, children)
  })

  router.post(`${parentRoute}/teixeira/getTags`, handleGetTags)
  router.post(`${parentRoute}/teixeira/addTag`, handleAddTag)
  router.post(`${parentRoute}/teixeira/deleteTag`, handleDeleteTag)
}
