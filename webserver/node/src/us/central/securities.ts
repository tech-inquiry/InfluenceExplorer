import { dataSource } from '../../App'
import * as util from '../../util'

import { USCentralSecuritiesFiling } from '../../entity/us/central/securities/filing'
import { USCentralSecuritiesMeta } from '../../entity/us/central/securities/meta'

const kFilingTable = USCentralSecuritiesFiling
const kFilingTableName = 'us_securities_filing'

const kMetaTable = USCentralSecuritiesMeta
const kMetaTableName = 'us_securities_meta'

const metaFullTextSearchString = util.getFullTextSearchString('simple', [
  'cik',
  'ein',
  'lower(sic_description)',
  'lower(name)',
  'lower(description)',
  'lower(addresses::TEXT)',
])

function buildFilingDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`filing_date >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`filing_date <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function incorporateFilingDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('filing_date >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('filing_date <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

async function getMetaForCIKs(ciks: string[]) {
  if (!ciks.length) {
    return { filings: [], hitSearchCap: false }
  }

  const query = dataSource
    .getRepository(kMetaTable)
    .createQueryBuilder()
    .where('cik IN (:...ciks)', { ciks: ciks })

  const maxSearchResults = 10000
  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFilingsForCIKs(ciks: string[], dateRange: util.DateRange) {
  if (!ciks.length) {
    return { filings: [], hitSearchCap: false }
  }

  let query = dataSource
    .getRepository(kFilingTable)
    .createQueryBuilder()
    .where('cik IN (:...ciks)', { ciks: ciks })
    .orderBy('filing_date', 'DESC')
  incorporateFilingDateConstraint(dateRange, query)

  const maxSearchResults = 10000
  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveMetaForName(
  name: string,
  excludeSubsidiaries: boolean,
  entities: Map<string, Map<string, any>>
) {
  let ciks = []
  if (entities.has(name) && entities.get(name).has('cik')) {
    ciks.push(entities.get(name).get('cik'))
  }
  return ciks.length > 0
}

async function getMetaForName(
  name: string,
  excludeSubsidiaries: boolean,
  entities: Map<string, Map<string, any>>
) {
  let ciks = []
  if (entities.has(name) && entities.get(name).has('cik')) {
    ciks.push(entities.get(name).get('cik'))
  }
  return await getMetaForCIKs(ciks)
}

async function getFilingsForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
) {
  let ciks = []
  if (entities.has(name) && entities.get(name).has('cik')) {
    ciks.push(entities.get(name).get('cik'))
  }
  return await getFilingsForCIKs(ciks, dateRange)
}

async function haveMetaFromSearch(webSearch: string) {
  let params = []

  const queryStr = `
    SELECT 1 FROM ${kMetaTableName} WHERE
     ${metaFullTextSearchString}
     @@ websearch_to_tsquery('simple', $${params.length + 1}) LIMIT 100`
  params.push(webSearch)

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getMetaFromSearch(
  webSearch: string,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    metaFullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kMetaTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  query = query.limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveMetaAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)

  let result = false
  if (text) {
    console.log(`Boolean SEC meta API w/ text: ${text}`)
    result = await haveMetaFromSearch(text)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean SEC meta API w/ vendor: ${name}`)
    result = await haveMetaForName(name, excludeSubsidiaries, entities)
  }

  util.setJSONResponse(response, result)
}

async function getMetaAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>
) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`SEC meta API w/ text: ${text}, offset: ${searchOffset}`)
    result = await getMetaFromSearch(text, maxSearchResults, searchOffset)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`SEC meta API w/ vendor: ${name}, offset: ${searchOffset}`)
    result = await getMetaForName(name, excludeSubsidiaries, entities)
  }

  util.setJSONResponse(response, result)
}

async function getFilingsAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>
) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)

  let result = { filings: [], hitSearchCap: false }
  if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`SEC filings API w/ vendor: ${name}, offset: ${searchOffset}`)
    result = await getFilingsForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities
    )
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const parentRoute = '/api/us/central'
  const entities = entityState.entities

  // Returns whether filings exist for a web search or an entity.
  router.get(
    `${parentRoute}/haveSecurities`,
    async function (request, response) {
      await haveMetaAPI(request, response, entities)
    }
  )

  // Returns JSON for filings for a web search or an entity.
  router.get(
    `${parentRoute}/securities/meta`,
    async function (request, response) {
      await getMetaAPI(request, response, entities)
    }
  )
  router.get(
    `${parentRoute}/securities/filings`,
    async function (request, response) {
      await getFilingsAPI(request, response, entities)
    }
  )
}
