import { dataSource } from '../../App'
import * as tags from '../../tags'
import * as util from '../../util'
import * as procurementUtil from './procurement/util.ts'

import { USCentralGrants } from '../../entity/us/central/grants'

const kTable = USCentralGrants
const kTableName = 'us_central_grants'

const kTagDataset = 'us_central_grants'

const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(transaction_unique_key)',
  'lower(unique_key)',
  'lower(id_fain)',
  'lower(id_uri)',
  'lower(sai_number)',
  'lower(emergency_fund_codes)',
  'lower(awarding_agency_name)',
  'lower(awarding_sub_agency_name)',
  'lower(awarding_office_name)',
  'lower(funding_agency_name)',
  'lower(funding_sub_agency_name)',
  'lower(funding_office_name)',
  'lower(recipient_uei)',
  'lower(recipient_duns)',
  'lower(recipient_name)',
  'lower(recipient_name_raw)',
  'lower(recipient_parent_uei)',
  'lower(recipient_parent_duns)',
  'lower(recipient_parent_name)',
  'lower(recipient_parent_name_raw)',
  'lower(recipient_country_name)',
  'lower(recipient_address_line_1)',
  'lower(recipient_address_line_2)',
  'lower(recipient_city_name)',
  'lower(recipient_county_name)',
  'lower(recipient_state_name)',
  'lower(recipient_zip_code)',
  'lower(recipient_foreign_city_name)',
  'lower(recipient_foreign_province_name)',
  'lower(recipient_foreign_postal_code)',
  'lower(pop_scope)',
  'lower(pop_country_name)',
  'lower(pop_city_name)',
  'lower(pop_county_name)',
  'lower(pop_state_name)',
  'lower(pop_zip_4)',
  'lower(pop_foreign_location)',
  'lower(cfda_title)',
  'lower(funding_opportunity_goals)',
  'lower(assistance_type_description)',
  'lower(transaction_description)',
  'lower(base_transaction_description)',
  'lower(funds_indicator_description)',
  'lower(business_types_description)',
  'lower(action_type_description)',
  'lower(record_type_description)',
  'lower(officer_1_name)',
  'lower(officer_2_name)',
  'lower(officer_3_name)',
  'lower(officer_4_name)',
  'lower(officer_5_name)',
])

function union(a: any[], b: any[]): any[] {
  let filings = []
  let idSet = new Set<string>()
  for (const filing of a) {
    filings.push({ ...filing })
    idSet.add(filing.unique_key)
  }
  for (const filing of b) {
    if (!idSet.has(filing.unique_key)) {
      filings.push({ ...filing })
      idSet.add(filing.unique_key)
    }
  }
  return filings
}

function sortByDate(filings: any[]) {
  const dateKey = 'action_date'
  filings.sort(function (a, b) {
    if (a[dateKey] > b[dateKey]) {
      return -1
    } else if (a[dateKey] < b[dateKey] || (!a[dateKey] && b[dateKey])) {
      return 1
    } else {
      return 0
    }
  })
}

function combineAndTruncate(a: any[], b: any[], maxEntries: number): any[] {
  console.log('    Concatenating and removing duplicates...')
  let combined = union(a, b)
  console.log('    Sorting by date...')
  sortByDate(combined)

  return [...combined.slice(0, maxEntries)]
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`end_date >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`action_date <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('end_date >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('action_date <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

// Set up the list of grants names for the vendor. If subsidiaries were
// requested to be included, the same process is repeated for each descendant.
function expandNames(
  entityList: string[],
  entityAlternates,
  entities
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'us',
    'central',
    'grants',
  ])
}

// NOTE: These two maps are not yet in use.
const departmentMap = {
  'Department of Energy': 'energy, department of',
}

const agencyMap = {
  'Department of Energy': 'energy, department of',
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateFiling(
  filing,
  entities,
  entityNormalization,
  agencyNormalization
) {
  const recipient = util.coerce(
    filing['recipient_name_raw'],
    filing['recipient_name']
  )
  let normalizedRecipient = util.normalizeEntity(recipient, entityNormalization)
  const normalizedRecipientBackup = util.normalizeEntity(
    filing['recipient_name'],
    entityNormalization
  )
  if (
    entities.has(normalizedRecipientBackup) &&
    !entities.has(normalizedRecipient)
  ) {
    normalizedRecipient = normalizedRecipientBackup
  }

  const recipientParent = util.coerce(
    filing['recipient_parent_name_raw'],
    filing['recipient_parent_name']
  )
  const normalizedRecipientParent = util.normalizeEntity(
    recipientParent,
    entityNormalization
  )

  function convertOfficeCode(code) {
    // Only the office codes have letters; the others are numeric.
    return code.toLowerCase()
  }

  function convertAgencyCode(code) {
    // No transformation needs to take place on the (numeric) agency codes.
    return code
  }

  function convertDepartmentCode(code) {
    // We need to rearrange the numeric department codes.
    if (code.length == 3 && code.startsWith('0')) {
      return code.substr(1, 2) + '00'
    } else {
      return code
    }
  }

  function agencyAnnotationFromComponents(
    department: string,
    departmentCode: string,
    agency: string,
    agencyCode: string,
    office: string,
    officeCode: string
  ) {
    const convertedDepartmentCode = convertDepartmentCode(departmentCode)
    const convertedAgencyCode = convertAgencyCode(agencyCode)
    const convertedOfficeCode = convertOfficeCode(officeCode)

    let agencyNorm = agency
    if (agencyNormalization.get('office').has(convertedOfficeCode)) {
      agencyNorm = agencyNormalization.get('office').get(convertedOfficeCode)
    } else if (agencyNormalization.get('agency').has(convertedAgencyCode)) {
      agencyNorm = agencyNormalization.get('agency').get(convertedAgencyCode)
    } else if (agencyNormalization.get('dept').has(convertedDepartmentCode)) {
      agencyNorm = agencyNormalization.get('dept').get(convertedDepartmentCode)
    } else if (office.toLowerCase() == 'american institute in taiwan') {
      // TODO(Jack Poulson): Formalize this patching if more instances are
      // found where grant offices do not have equivalents in procurement.
      agencyNorm = 'american institute in taiwan'
    } else if (office.toLowerCase() == 'office to monitor trafkg in persons') {
      agencyNorm =
        'office of the under secretary of state for civilian security, democracy, and human rights'
    } else if (department.toLowerCase() == 'u.s. agency for global media') {
      agencyNorm = 'united states agency for global media'
    }

    return {
      department: department,
      agency: agency,
      office: office,
      agencyNorm: agencyNorm,
      agencyStylized: util.getFullyStylizedName(agencyNorm, entities),
      agencyLogo: util.logoURLFromAncestors(agencyNorm, entities),
    }
  }

  filing['annotation'] = {
    recipient: {
      origText: recipient,
      text: normalizedRecipient,
      stylizedText: util.getFullyStylizedName(normalizedRecipient, entities),
      url: util.encodeEntity(normalizedRecipient),
      logo: util.logoURLFromAncestors(normalizedRecipient, entities),
    },
    recipientParent: {
      origText: recipientParent,
      text: normalizedRecipientParent,
      stylizedText: util.getFullyStylizedName(
        normalizedRecipientParent,
        entities
      ),
      url: util.encodeEntity(normalizedRecipientParent),
      logo: util.logoURLFromAncestors(normalizedRecipientParent, entities),
    },
    awarding: agencyAnnotationFromComponents(
      filing.awarding_agency_name,
      filing.awarding_agency_code,
      filing.awarding_sub_agency_name,
      filing.awarding_sub_agency_code,
      filing.awarding_office_name,
      filing.awarding_office_code
    ),
    funding: agencyAnnotationFromComponents(
      filing.funding_agency_name,
      filing.funding_agency_code,
      filing.funding_sub_agency_name,
      filing.funding_sub_agency_code,
      filing.funding_office_name,
      filing.funding_office_code
    ),
  }
}

function annotate(result, entities, entityNormalization, agencyNormalization) {
  for (let filing of result.filings) {
    annotateFiling(filing, entities, entityNormalization, agencyNormalization)
  }
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const searchConstraint =
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function haveForNames(
  names: string[],
  dateRange: util.DateRange,
  entities
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(names, dateRange, entities, [
      'feeds',
      'us',
      'central',
      'grants',
    ])
  ) {
    return true
  }

  let params = []

  const searchConstraint = `
    LOWER(recipient_name_raw) = ANY($${params.length + 1}) OR
    LOWER(recipient_name) = ANY($${params.length + 1})`
  params.push(names)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities,
  entityChildren,
  entityAlternates
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await haveForNames(expandedNames, dateRange, entities)
}

// Get whether the named dept/agency/office codes have any contracts.
async function haveForAgencies(
  codes: Map<string, string[]>,
  dateRange: util.DateRange
) {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  const subqueryHeader = `SELECT 1 FROM ${kTableName} WHERE `
  const subqueryFooter =
    (dateWhere ? ` AND (${dateWhere}) ` : '') + ' LIMIT 10000'

  let subqueries = []
  let subqueryParamsList = []
  if (codes.get('office').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('office'))

    subqueries.push(
      `${subqueryHeader}
       awarding_office_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       funding_office_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }
  if (codes.get('agency').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('agency'))

    subqueries.push(
      `${subqueryHeader}
       awarding_sub_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       funding_sub_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }
  if (codes.get('dept').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('dept'))

    subqueries.push(
      `${subqueryHeader}
       awarding_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       funding_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }

  let manager = dataSource.manager
  for (const [index, subquery] of subqueries.entries()) {
    const subqueryParams = subqueryParamsList[index]
    const haveResult = await util.nonemptyQuery(
      manager,
      subquery,
      subqueryParams
    )
    if (haveResult) {
      return true
    }
  }
  return false
}

async function haveForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  usFederalOffices: Map<string, Map<string, any>>
) {
  const convertForGrants = true
  const codes = procurementUtil.getAgencyCodes(
    name,
    entities,
    usFederalOffices,
    convertForGrants
  )
  return await haveForAgencies(codes, dateRange)
}

async function getForNames(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where('LOWER(recipient_name_raw) = ANY(ARRAY[:...names])', {
      names: names,
    })
    .orWhere('LOWER(recipient_name) = ANY(ARRAY[:...names])', { names: names })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('action_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities,
  entityChildren,
  entityAlternates,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await getForNames(
    expandedNames,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

// Get the list of contracts where the named entities were contracting agencies.
async function getForAgencies(
  codes: Map<string, string[]>,
  dateRange: util.DateRange,
  maxSearchResults: number,
  maxTargetsPerQuery = 20
) {
  let params = []
  const dateWhere = buildDateConstraint(dateRange, params)

  const innerLimit = `ORDER BY action_date DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  const subqueryHeader = `SELECT * FROM ${kTableName} WHERE`
  const subqueryFooter = (dateWhere ? `AND (${dateWhere}) ` : '') + innerLimit

  let manager = dataSource.manager
  let result = { filings: [], hitSearchCap: false }
  let subqueries = []
  let subqueryParamsList = []
  if (codes.get('office').length) {
    const offices = codes.get('office')
    for (
      let sliceBeg = 0;
      sliceBeg < offices.length;
      sliceBeg += maxTargetsPerQuery
    ) {
      const sliceEnd = Math.min(sliceBeg + maxTargetsPerQuery, offices.length)
      if (offices.length > maxTargetsPerQuery) {
        console.log(
          `  Performing office query over [${sliceBeg}, ${sliceEnd}).`
        )
      }
      let subqueryParams = [...params]
      subqueryParams.push(offices.slice(sliceBeg, sliceEnd))

      subqueries.push(
        `${subqueryHeader}
         awarding_office_code = ANY($${params.length + 1})
         ${subqueryFooter}`
      )
      subqueryParamsList.push(subqueryParams)

      subqueries.push(
        `${subqueryHeader}
         funding_office_code = ANY($${params.length + 1})
         ${subqueryFooter}`
      )
      subqueryParamsList.push(subqueryParams)
    }
  }
  if (codes.get('agency').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('agency'))

    subqueries.push(
      `${subqueryHeader}
       awarding_sub_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       funding_sub_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }
  if (codes.get('dept').length) {
    let subqueryParams = [...params]
    subqueryParams.push(codes.get('dept'))

    subqueries.push(
      `${subqueryHeader}
       awarding_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)

    subqueries.push(
      `${subqueryHeader}
       funding_agency_code = ANY($${params.length + 1})
       ${subqueryFooter}`
    )
    subqueryParamsList.push(subqueryParams)
  }

  try {
    let filings = []
    for (let index = 0; index < subqueries.length; ++index) {
      const subquery = subqueries[index]
      const subqueryParams = subqueryParamsList[index]
      if (index == 0) {
        filings = await manager.query(subquery, subqueryParams)
      } else {
        console.log(`  Subquery ${index} of ${subqueries.length}.`)
        const additionalFilings = await manager.query(subquery, subqueryParams)
        filings = combineAndTruncate(
          filings,
          additionalFilings,
          maxSearchResults
        )
      }
    }

    result = {
      filings: filings,
      hitSearchCap: filings.length == maxSearchResults,
    }
  } catch (err) {
    console.log(err)
  }

  return result
}

async function getForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  usFederalOffices: Map<string, Map<string, any>>,
  maxSearchResults: number,
  maxTargetsPerQuery = 20
) {
  const convertForGrants = true
  const codes = procurementUtil.getAgencyCodes(
    name,
    entities,
    usFederalOffices,
    convertForGrants
  )
  return await getForAgencies(
    codes,
    dateRange,
    maxSearchResults,
    maxTargetsPerQuery
  )
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('action_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['unique_key']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('action_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities,
  entityChildren,
  entityAlternates,
  usFederalOffices: Map<string, Map<string, any>>
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Boolean US central grants w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Boolean US federal grants w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean US central grants w/ entity: ${name}`)
    result = await haveForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      entityAlternates
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`Have US central grants w/ agency: ${name}`)
    result = await haveForAgency(name, dateRange, entities, usFederalOffices)
  } else {
    console.log('Boolean US central grants over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities,
  entityChildren,
  entityNormalization,
  entityAlternates,
  agencyNormalization,
  usFederalOffices: Map<string, Map<string, any>>
) {
  const params = request.query

  const exportCSV = util.readBoolean(params.csv)

  const maxSearchResultsDefault = 2500
  const maxSearchResultsCap = 25000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const dateRange = util.getDateRange(params)

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`US federal grants w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`US federal grants w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`US federal grants w/ entity: ${name}`)
    result = await getForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      entityAlternates,
      maxSearchResults,
      searchOffset
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`US federal grants w/ agency: ${name}`)
    result = await getForAgency(
      name,
      dateRange,
      entities,
      usFederalOffices,
      maxSearchResults
    )
  } else {
    console.log('US federal grants over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  // This pushes any empty start dates to the back.
  sortByDate(result.filings)
  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, entityNormalization, agencyNormalization)
  }

  util.setAPIResponse(response, result, exportCSV, 'us_central_grants.csv')
}

export function setRoutes(router, entityState) {
  const route = '/api/us/central/grant'

  const usCentralFeeds = entityState.feeds.get('us').get('central')
  const feed = usCentralFeeds.get('grants')
  const agencyNormalization =
    usCentralFeeds.get('procurement').agencyNormalization

  const entities = entityState.entities
  const children = entityState.children
  const usFederalOffices = entityState.usFederalOffices

  // Returns whether results exist for an entity or text search.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(
      request,
      response,
      entities,
      children,
      feed.names,
      usFederalOffices
    )
  })

  // Returns JSON for a web search or an entity.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(
      request,
      response,
      entities,
      children,
      feed.normalization,
      feed.names,
      agencyNormalization,
      usFederalOffices
    )
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
