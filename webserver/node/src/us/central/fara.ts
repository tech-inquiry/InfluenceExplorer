import { dataSource } from '../../App'
import * as util from '../../util'

import { USCentralFARAFiling } from '../../entity/us/central/fara'

const kTable = USCentralFARAFiling
const kTableName = 'us_fara'

function incorporateFilingDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('date_stamped >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('date_stamped <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

async function getFilingsForIDs(ids: string[], dateRange: util.DateRange) {
  if (!ids.length) {
    return { filings: [], hitSearchCap: false }
  }

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where('registrant_number IN (:...ids)', { ids: ids })
  incorporateFilingDateConstraint(dateRange, query)

  const maxSearchResults = 10000
  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveFilingForName(
  name: string,
  excludeSubsidiaries: boolean,
  entities: Map<string, Map<string, any>>
) {
  let ids = []
  if (entities.has(name) && entities.get(name).has('fara')) {
    ids.push(entities.get(name).get('fara'))
  }
  return ids.length > 0
}

async function getFilingsForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
) {
  let ids = []
  if (entities.has(name) && entities.get(name).has('fara')) {
    ids.push(entities.get(name).get('fara'))
  }
  return await getFilingsForIDs(ids, dateRange)
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>
) {
  const params = request.query

  const dateRange = util.getDateRange(params)

  let result = false
  if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean FARA API w/ entity: ${name}`)
    result = await haveFilingForName(name, excludeSubsidiaries, entities)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>
) {
  const params = request.query

  const dateRange = util.getDateRange(params)

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  let result = { filings: [], hitSearchCap: false }
  if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`FARA API w/ entity: ${name}, offset: ${searchOffset}`)
    result = await getFilingsForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities
    )
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const parentRoute = '/api/us/central'
  const entities = entityState.entities

  // Returns whether filings exist for an entity.
  router.get(`${parentRoute}/haveFARA`, async function (request, response) {
    await haveAPI(request, response, entities)
  })

  // Returns JSON for filings for an entity.
  router.get(`${parentRoute}/fara`, async function (request, response) {
    await getAPI(request, response, entities)
  })
}
