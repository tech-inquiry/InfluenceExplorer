import { dataSource } from '../../App'
import { USCentralProcurementDODAnnounce } from '../../entity/us/central/procurement/dod_announce'
import * as tags from '../../tags'
import * as util from '../../util'

const kTable = USCentralProcurementDODAnnounce
const kTableName = 'us_dod_announcements'

const kTagDataset = 'us_dod_announcements'

const fullTextSearchString = "to_tsvector('simple', lower(text))"

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let dateWhere = ''
  if (dateRange.hasStart()) {
    dateWhere += ` AND date >= $${params.length + 1}`
    params.push(dateRange.start)
  }
  if (dateRange.hasEnd()) {
    dateWhere += ` AND date <= $${params.length + 1}`
    params.push(dateRange.end)
  }
  return dateWhere
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)

  const queryStr = `
    SELECT 1 FROM ${kTableName} WHERE ${fullTextSearchString}
    @@ websearch_to_tsquery('simple', $${params.length + 1})
    ${dateWhere} LIMIT 1000`
  params.push(webSearch)

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(webSearch: string, token: string) {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + " @@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  if (dateRange.hasStart()) {
    query = query.andWhere('date >= :startDate', {
      startDate: dateRange.start,
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('date <= :endDate', { endDate: dateRange.end })
  }
  query = query.orderBy('date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['article_id']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateWhere = buildDateConstraint(dateRange, params)
  const whereStr = dateWhere ? `WHERE ${dateWhere}` : ''

  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  if (dateRange.hasStart()) {
    query = query.andWhere('date >= :startDate', {
      startDate: dateRange.start,
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('date <= :endDate', { endDate: dateRange.end })
  }
  query = query.orderBy('date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(request, response) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Have DOD Announcements w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have DOD Announcements w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    // TODO(Jack Poulson)
  } else {
    console.log('Have DOD Announcements over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(request, response) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`DOD Announcements w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`DOD Announcements w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    // TODO(Jack Poulson)
  } else {
    console.log('DOD Announcements over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router) {
  const route = '/api/us/central/procurement/dod-announce'

  // Returns whether data exists for a web search.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response)
  })

  // Returns JSON for a web search.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(request, response)
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
