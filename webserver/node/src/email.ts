const nodemailer = require('nodemailer')

const pass = process.env.SEND_EMAIL_PASSWORD
const user = process.env.SEND_ENTITY_EMAIL_USER
const recipient = process.env.SEND_ENTITY_EMAIL_RECIPIENT

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user,
    pass,
  },
})

export const sendEntitySubmissionEmail = (submitterEmail, entityInput) => {
  const { institutionalUrl, logoUrl, shortName, fullName } = entityInput
  const topLevelEntitiesKey = fullName.toLowerCase()
  const formattedForEntitiesFile = {
    [topLevelEntitiesKey]: {
      name: {
        full: fullName,
        short: shortName,
      },
      urls: {
        institutional: institutionalUrl,
        logo: logoUrl,
      },
    },
  }
  const jsonStr = JSON.stringify(formattedForEntitiesFile, null, 2).slice(1, -1)
  const textSections = [
    [`Submitter email:`, submitterEmail],
    [
      `Submitted entity JSON (paste everything below into top level of entities.json object):`,
      jsonStr,
    ],
  ]

  const content = textSections.reduce(
    (acc, curr) => acc + curr.join('\n') + '\n\n',
    ''
  )
  const mailOptions = {
    from: user,
    to: recipient,
    subject: 'New Influence Explorer Entity Submission',
    text: content,
  }
  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        //send error email? probably won't work
        console.log(error)
        reject(error)
      } else {
        console.log('Email sent: ' + info.response)
        resolve(info.response)
      }
    })
  })
}
