import { dataSource } from './App'
import * as util from './util'

async function getEntitySuggestions(
  hint: string,
  maxRetrievedSuggestions: number,
  maxShownSuggestions: number,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>
) {
  let items = []
  try {
    // The results which are extensions of what the user has typed so far
    // come first.
    let startingItems = []
    {
      const likeFilter = hint + '%'
      let params = []
      const queryStr = `SELECT * FROM vendor WHERE vendor LIKE $1 OR dba LIKE $1
        ORDER BY dba COLLATE "C" ASC, vendor COLLATE "C" ASC LIMIT $2`
      params.push(likeFilter)
      params.push(maxRetrievedSuggestions)
      startingItems = await dataSource.manager.query(queryStr, params)
    }

    // Results where a later term begins with the user string will be
    // appended until we run out of space.
    let middleItems = []
    {
      const likeFilter = '%' + hint + '%'
      const regexpFilter = '[[:<:]]' + hint
      let params = []
      const queryStr = `SELECT * FROM vendor WHERE (
          vendor LIKE $1 AND vendor ~* $2
        ) OR (dba LIKE $1 and dba ~* $2)
          ORDER BY dba COLLATE "C" ASC, vendor COLLATE "C" ASC LIMIT $3`
      params.push(likeFilter)
      params.push(regexpFilter)
      params.push(maxRetrievedSuggestions)
      middleItems = await dataSource.manager.query(queryStr, params)
    }

    const getSimpleAncestors = function (entity: string): Object[] {
      let simpleAncestors = []
      let node: string = entity
      while (
        entities.has(node) &&
        entities.get(node).get('parents').length == 1
      ) {
        const parentNode = entities.get(node).get('parents')[0]

        let ancestorItem = {
          vendor: parentNode,
          stylizedVendor: util.getFullyStylizedName(parentNode, entities),
        }

        const ancestorLogoLink = util.logoURLFromAncestors(
          parentNode,
          entities,
          true
        )
        if (ancestorLogoLink) {
          ancestorItem['logoLink'] = ancestorLogoLink
        }

        simpleAncestors.push(ancestorItem)
        node = parentNode
      }
      return simpleAncestors
    }

    let entitySet = new Set<string>()

    // Add entities with logo links and/or ancestors first.
    for (const item of startingItems) {
      if (items.length >= maxShownSuggestions) {
        continue
      }
      const alternateStr = JSON.stringify({ name: item.vendor })
      if (entityNormalization.has(alternateStr)) {
        item.vendor = entityNormalization.get(alternateStr)
      }
      if (entitySet.has(item.vendor)) {
        continue
      }

      const logoLink = util.logoURLFromAncestors(item.vendor, entities, true)
      const ancestors = getSimpleAncestors(item.vendor)
      if (!(logoLink || ancestors.length)) {
        continue
      }

      if (logoLink) {
        item['logoLink'] = logoLink
      }
      item['stylizedVendor'] = util.getFullyStylizedName(item.vendor, entities)
      item['simpleAncestors'] = ancestors

      items.push(item)
      entitySet.add(item.vendor)
    }
    for (const item of middleItems) {
      if (items.length >= maxShownSuggestions) {
        continue
      }

      const alternateStr = JSON.stringify({ name: item.vendor })
      if (entityNormalization.has(alternateStr)) {
        item.vendor = entityNormalization.get(alternateStr)
      }
      if (entitySet.has(item.vendor)) {
        continue
      }

      const logoLink = util.logoURLFromAncestors(item.vendor, entities, true)
      if (!logoLink) {
        continue
      }
      item['logoLink'] = logoLink

      item['stylizedVendor'] = util.getFullyStylizedName(item.vendor, entities)
      item['simpleAncestors'] = getSimpleAncestors(item.vendor)

      items.push(item)
      entitySet.add(item.vendor)
    }

    for (const item of startingItems) {
      if (items.length >= maxShownSuggestions) {
        continue
      }
      if (entitySet.has(item.vendor)) {
        continue
      }

      item['stylizedVendor'] = util.getFullyStylizedName(item.vendor, entities)
      item['simpleAncestors'] = getSimpleAncestors(item.vendor)

      items.push(item)
      entitySet.add(item.vendor)
    }
    for (const item of middleItems) {
      if (items.length >= maxShownSuggestions) {
        continue
      }
      if (entitySet.has(item.vendor)) {
        continue
      }

      item['stylizedVendor'] = util.getFullyStylizedName(item.vendor, entities)
      item['simpleAncestors'] = getSimpleAncestors(item.vendor)

      items.push(item)
      entitySet.add(item.vendor)
    }
  } catch (err) {
    console.log(err)
    console.log('Could not retrieve suggestions for: ' + hint)
  }
  return items
}

async function mainAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>
) {
  const hint = util.readEntitySuggestionHint(request.query.hint)

  const maxRetrievedSuggestionsDefault = 100
  const maxRetrievedSuggestions = util.readNumber(
    request.query.maxRetrievedSuggestions,
    maxRetrievedSuggestionsDefault
  )

  const maxShownSuggestionsDefault = 10
  const maxShownSuggestions = util.readNumber(
    request.query.maxShownSuggestions,
    maxShownSuggestionsDefault
  )

  const items = await getEntitySuggestions(
    hint,
    maxRetrievedSuggestions,
    maxShownSuggestions,
    entities,
    entityNormalization
  )
  response.json(items)
}

export function setRoutes(router, entityState) {
  const feed = entityState.feeds.get('us').get('central').get('procurement')
  const entities = entityState.entities

  // The autocomplete suggestions for entities (and their ancestors).
  router.get('/api/entitySuggestions', async function (request, response) {
    await mainAPI(request, response, entities, feed.normalization)
  })

  // We redirect to the new autocomplete interface.
  router.get('/explorer', async function (request, response) {
    response.writeHead(302, {
      Location: '/',
    })
    response.end()
  })
}
