import { Parser } from 'json2csv'

const fs = require('fs')

const kHostname: string =
  process.env.EXPLORER_HOSTNAME || 'https://techinquiry.org'
const kExplorerRoute: string = process.env.EXPLORER_ROUTE || '/explorer'

export const AwardOrderingType = {
  SIGNED_DATE: 'signed_date',
  MODIFIED_DATE: 'modified_date',
  DATABASE_ID: 'database_id',
}

export class DateRange {
  start: Date
  end: Date

  constructor(startDate: Date, endDate: Date) {
    this.start = startDate
    this.end = endDate
  }

  // Given -- possibly undefined, possibly equal -- starting and end dates,
  // return whether we should enforce the given start date as the beginning of
  // an admissible time window.
  hasStart(): boolean {
    let haveStart: boolean = false
    if (this.start !== undefined) {
      haveStart = +this.start !== +this.end && !isNaN(this.start.getTime())
    }
    return haveStart
  }

  // Given -- possibly undefined, possibly equal -- starting and end dates,
  // return whether we should enforce the given end date as the end of an
  // admissible time window.
  hasEnd(): boolean {
    let haveEnd: boolean = false
    if (this.end !== undefined) {
      haveEnd = +this.start !== +this.end && !isNaN(this.end.getTime())
    }
    return haveEnd
  }
}

function prefixLogoURL(url) {
  if (url.startsWith('/')) {
    return kHostname + url
  } else {
    return url
  }
}

export function objectToMap(obj) {
  if (Array.isArray(obj)) {
    let res = []
    for (const item of obj) {
      res.push(objectToMap(item))
    }
    return res
  } else if (obj instanceof Object) {
    let map = new Map()
    for (let k of Object.keys(obj)) {
      map.set(k, objectToMap(obj[k]))
    }
    return map
  } else {
    return obj
  }
}

export function mapToObject(map) {
  if (Array.isArray(map)) {
    let res = []
    for (const item of map) {
      res.push(mapToObject(item))
    }
    return res
  } else if (map instanceof Map) {
    let obj = {}
    map.forEach(function (v, k) {
      obj[k] = mapToObject(v)
    })
    return obj
  } else {
    return map
  }
}

export function setJSONResponse(response, result): void {
  response.setHeader('Content-Type', 'application/json')
  response.end(JSON.stringify(result, null, 1))
}

export function setAPIResponse(
  response,
  result,
  exportCSV: boolean,
  csvFilename = 'tech_inquiry.csv'
): void {
  if (exportCSV && result['filings'].length) {
    const json2csv = new Parser()
    const csv = json2csv.parse(result['filings'])
    response.setHeader('Content-Type', 'text/csv')
    response.attachment(csvFilename)
    response.send(csv)
  } else {
    setJSONResponse(response, result)
  }
}

export function escapeHTML(str: string): string {
  const htmlEscapeMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;',
  }

  return String(str).replace(/[&<>"'`=\/]/g, function (s) {
    return htmlEscapeMap[s]
  })
}

export function unescapeHTML(str: string): string {
  return String(str)
    .replace(/&amp;/g, '&')
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>')
    .replace(/&quot;/g, '"')
    .replace(/&#39;/g, "'")
    .replace(/&#x2F;/g, '/')
    .replace(/&#x60;/g, '`')
    .replace(/&#x3D;/g, '=')
}

export function appendURLPath(
  urlBase: string,
  urlAddition: string,
  absolute = true
): string {
  urlBase = urlBase.replace(/\/$/g, '')
  urlAddition = urlAddition.replace(/^\//g, '')
  if (absolute || urlBase) {
    return urlBase + '/' + urlAddition
  } else {
    return urlAddition
  }
}

export function explorerURLPath(urlAddition: string): string {
  return appendURLPath(kHostname + kExplorerRoute, urlAddition)
}

// Iterate over the tokens and create a new line at each first break crossing
// the splitLength.
export function splitLabelIntoLines(label: string, splitLength: number) {
  if (label.length <= splitLength) {
    return [label]
  }
  const numLinesEst = Math.ceil(label.length / splitLength)
  const targetLength = label.length / numLinesEst

  const tokens = label.split(' ')
  let lines = []
  let lineStart = 0
  let lineLength = 0
  for (const [index, token] of tokens.entries()) {
    if (lineLength >= targetLength) {
      lines.push(tokens.slice(lineStart, index).join(' '))
      lineStart = index
      lineLength = 0
    }
    lineLength += token.length
  }
  lines.push(tokens.slice(lineStart, tokens.length).join(' '))
  return lines
}

// Appends a given item to a list at the specified key of a dictionary. If the
// list does not already exist, it is created before the append.
export function appendAtKey(dictionary, key: string, item): void {
  if (dictionary.has(key)) {
    dictionary.get(key).push(item)
  } else {
    dictionary.set(key, [item])
  }
}

// Returns whether a given string contains every member of a given 'tokens'
// array as a substring.
export function stringMatchesTokens(
  str: string,
  tokens: Array<string>
): boolean {
  if (tokens == undefined) {
    return false
  }
  let matched: boolean = true
  for (const token of tokens) {
    if (!str.includes(token)) {
      matched = false
    }
  }
  return matched
}

// Converts strings to Date and undefined to undefined.
export function nullableStringToDate(value: string): Date {
  return value ? new Date(value) : undefined
}

export function getDateRange(params): DateRange {
  return new DateRange(
    nullableStringToDate(params.startDate),
    nullableStringToDate(params.endDate)
  )
}

// Returns whether a string is a short sequence of digits.
export function isShortDigitSequence(value: string, minLength = 5): boolean {
  value = value.trim()
  return value.length < minLength && /^\d+$/.test(value)
}

// Formats a Date as string of form '[month] [date], [year]'.
export function dateWithoutTime(date: Date): string {
  if (date == null || isNaN(date.getTime())) {
    return 'N/A'
  }
  const month = new Intl.DateTimeFormat('en-US', { month: 'long' }).format(date)
  return `${month} ${date.getDate()}, ${date.getFullYear()}`
}

// Since encodeURIComponent does not encode forward slashes, this routine
// calls said function after encoding all forward slashes as '%2F'.
export function encodeURIComponentIncludingSlashes(component: string): string {
  return encodeURIComponent(component).replace(/\//g, '%2F')
}

// Formats a number into a string with two digits after the period and a comma
// between each three signifant digits to the left of it.
export function numberWithCommas(x: number): string {
  if (isNaN(x) || x == undefined) {
    return ''
  } else {
    // Even if the input is loaded via TypeORM as a 'number', if the column is
    // nullable, we must still do a conversion to ensure the member function
    // 'toFixed' exists.
    return Number(x)
      .toFixed(2)
      .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }
}

// Maps a given vendor string into the name of its ultimate ancestor. The
// recursion stops at any joint venture.
export function ultimateParent(vendor: string, parents): string {
  let ancestor: string = vendor
  while (parents.has(ancestor) && parents.get(ancestor).length == 1) {
    ancestor = parents.get(ancestor)[0]
  }
  return ancestor
}

function ancestorsHelper(
  node: string,
  parents: Map<string, string[]>,
  ancestors: Set<string>
): void {
  if (parents.has(node)) {
    for (const parent of parents.get(node)) {
      ancestors.add(parent)
      ancestorsHelper(parent, parents, ancestors)
    }
  }
}

export function ancestors(
  vendor: string,
  parents: Map<string, string[]>
): string[] {
  let ancestors = new Set<string>()
  ancestorsHelper(vendor, parents, ancestors)
  return Array.from(ancestors)
}

export function descendants(
  vendor: string,
  children: Map<string, string[]>
): string[] {
  return ancestors(vendor, children)
}

// Return lower-case variant of string which also merges all whitespace into a
// single space.
export function canonicalText(name: string): string {
  if (!name) {
    return ''
  }
  return unescapeHTML(name).trim().toLowerCase().replace(/\s\s+/g, ' ')
}

export function normalizeEntity(
  entity: string,
  normalization: Map<string, string>
): string {
  if (!entity) {
    return ''
  }

  // Replace all multi-spaces with a single space.
  entity = canonicalText(entity)

  // Retrieve the normalization, if it exists.
  if (normalization.has(entity)) {
    entity = normalization.get(entity)
  }

  return entity
}

export function normalizeEntityWithAttribute(
  entity: string,
  attribute: string,
  attributeKey: string,
  normalization: Map<string, string>
): string {
  if (!entity) {
    entity = ''
  }
  if (!attribute) {
    attribute = ''
  }

  const canonicalEntity: string = canonicalText(entity)
  const keyList = [
    JSON.stringify({
      name: canonicalEntity,
      [attributeKey]: canonicalText(attribute),
    }),
    JSON.stringify({ name: canonicalEntity }),
  ]

  for (const key of keyList) {
    if (normalization.has(key)) {
      return normalization.get(key)
    }
  }

  return canonicalEntity
}

export function normalizeEntityWithEmployer(
  entity: string,
  employer: string,
  normalization: Map<string, string>
): string {
  return normalizeEntityWithAttribute(
    entity,
    employer,
    'employer',
    normalization
  )
}

export function normalizeEntityWithCityAndEmployer(
  entity: string,
  city: string,
  state: string,
  employer: string,
  normalization: Map<string, string>
): string {
  if (!entity) {
    entity = ''
  }

  let cityState = ''
  if (city && state) {
    cityState = `${canonicalText(city)}, ${canonicalText(state)}`
  } else if (city) {
    cityState = canonicalText(city)
  } else if (state) {
    cityState = canonicalText(state)
  }

  if (!employer) {
    employer = ''
  }

  const canonicalEntity: string = canonicalText(entity)

  const keyList = [
    JSON.stringify({
      name: canonicalEntity,
      city: cityState,
      employer: canonicalText(employer),
    }),
    JSON.stringify({
      name: canonicalEntity,
      city: cityState,
    }),
    JSON.stringify({
      name: canonicalEntity,
      employer: canonicalText(employer),
    }),
    JSON.stringify({ name: canonicalEntity }),
  ]

  for (const key of keyList) {
    if (normalization.has(key)) {
      return normalization.get(key)
    }
  }

  return canonicalEntity
}

export function normalizeEntityWithContributor(
  entity: string,
  contributor: string,
  normalization: Map<string, string>
): string {
  return normalizeEntityWithAttribute(
    entity,
    contributor,
    'contributor',
    normalization
  )
}

export function normalizeEntityWithUEI(
  entity: string,
  uei: string,
  entityNormalization: Map<string, string>
): string {
  const canonicalEntity: string = canonicalText(entity)

  const keyList = [
    JSON.stringify({ name: canonicalEntity, uei: uei }),
    JSON.stringify({ uei: uei }),
    JSON.stringify({ name: canonicalEntity }),
  ]

  for (const key of keyList) {
    if (entityNormalization.has(key)) {
      return entityNormalization.get(key)
    }
  }

  return canonicalEntity
}

export function normalizeAgencyWithOffice(
  agency: string,
  office: string,
  agencyNormalization: Map<string, string>
): string {
  agency = canonicalText(agency)
  office = canonicalText(office)

  const keyList = [JSON.stringify([agency, office]), JSON.stringify([agency])]

  for (const key of keyList) {
    if (agencyNormalization.has(key)) {
      return agencyNormalization.get(key)
    }
  }

  return agency
}

export function coerce(value, defaultValue) {
  return value ? value : defaultValue
}

export function coerceLower(value) {
  return value ? value.toLowerCase() : ''
}

export function coerceUpper(value) {
  return value ? value.toUpperCase() : ''
}

export function coerceAmount(value: number): number {
  return value == undefined || isNaN(value) ? 0 : value
}

function getProfileURL(key: string, profile: Map<string, any>) {
  const kURLsKey = 'urls'
  if (profile == undefined) {
    return undefined
  }
  if (!profile.has(kURLsKey)) {
    return undefined
  }
  const urls = profile.get(kURLsKey) as Map<string, any>
  if (!urls.has(key)) {
    return undefined
  }
  return urls.get(key)
}

export function getURL(
  key: string,
  vendor: string,
  profiles: Map<string, any>
) {
  if (!profiles.has(vendor)) {
    return undefined
  }
  const profile = profiles.get(vendor)
  return getProfileURL(key, profile)
}

export function getLogoLink(profile) {
  const kKey = 'logo'
  return getProfileURL(kKey, profile)
}

export function getURLList(
  key: string,
  vendor: string,
  profiles: Map<string, Map<string, any>>
) {
  const kURLsKey = 'urls'
  if (!profiles.has(vendor)) {
    return []
  }
  const profile = profiles.get(vendor)
  if (!profile.has(kURLsKey)) {
    return []
  }
  const urls = profile.get(kURLsKey)
  if (!urls.has(key)) {
    return []
  }
  return urls.get(key)
}

export function getAKAName(
  vendor: string,
  profiles: Map<string, Map<string, any>>
) {
  if (!profiles.has(vendor)) {
    return undefined
  }
  const profile = profiles.get(vendor)
  if (profile.has('name')) {
    if (profile.get('name').has('aka')) {
      return profile.get('name').get('aka')
    }
  }
  return undefined
}

export function getDBAName(
  vendor: string,
  profiles: Map<string, Map<string, any>>
) {
  if (!profiles.has(vendor)) {
    return undefined
  }
  const profile = profiles.get(vendor)
  if (profile.has('name')) {
    if (profile.get('name').has('dba')) {
      return profile.get('name').get('dba')
    }
  }
  return undefined
}

export function getFullName(
  vendor: string,
  profiles: Map<string, Map<string, any>>
) {
  if (!profiles.has(vendor)) {
    return undefined
  }
  const profile = profiles.get(vendor)
  if (!(profile.has('name') && profile.get('name').get('full'))) {
    return undefined
  }
  return profile.get('name').get('full')
}

// Mirror the CSS 'text-transform: capitalize;' procedure in JavaScript.
function capitalize(text: string): string {
  if (text != undefined) {
    return text.replace(/\b\w/g, function (m) {
      return m.toUpperCase()
    })
  } else {
    return text
  }
}

// Cut off a string with ellipses if it exceeds a given length.
export function trimLabel(label: string, maxLength: number): string {
  if (label.length > maxLength) {
    label = label.slice(0, maxLength - 3) + '...'
  }
  return label
}

// Create a function to map a vendor name into its full stylized + DBA form.
export function getFullyStylizedName(
  vendor: string,
  profiles: Map<string, Map<string, any>>
) {
  const aka = getAKAName(vendor, profiles)
  const dba = getDBAName(vendor, profiles)
  const fullName = getFullName(vendor, profiles)
  let fullyStylized: string = fullName ? fullName : capitalize(vendor)
  if (dba) {
    fullyStylized += ` (dba ${dba})`
  } else if (aka) {
    fullyStylized += ` (aka ${aka})`
  }
  return fullyStylized
}

// Create a function to map a vendor name into its full stylized + DBA form
// with possible HTML wrappers for inactive companies.
export function getFullyStylizedNameWithTags(
  vendor: string,
  profiles: Map<string, Map<string, any>>
) {
  const fullyStylized = getFullyStylizedName(vendor, profiles)
  if (
    profiles.has(vendor) &&
    profiles.get(vendor).has('inactive') &&
    profiles.get(vendor).get('inactive')
  ) {
    return `<strike>${escapeHTML(fullyStylized)}</strike>`
  } else {
    return fullyStylized
  }
}

export function primaryURL(
  vendor: string,
  profiles: Map<string, Map<string, any>>
): string {
  vendor = canonicalText(vendor)
  if (!profiles.has(vendor)) {
    return undefined
  }
  const profile = profiles.get(vendor)
  if (!profile.has('urls') || !profile.get('urls').has('institutional')) {
    return undefined
  }
  const urls = profile.get('urls')
  return urls.get('institutional')
}

export function logoURL(
  vendor: string,
  profiles: Map<string, Map<string, any>>,
  preferSmall = false
): string {
  vendor = canonicalText(vendor)
  if (!profiles.has(vendor)) {
    return undefined
  }
  const profile = profiles.get(vendor)
  if (!profile.has('urls')) {
    return undefined
  }
  const urls = profile.get('urls')
  if (preferSmall && urls.has('logoSmall')) {
    return prefixLogoURL(urls.get('logoSmall'))
  }
  if (urls.has('logo')) {
    return prefixLogoURL(urls.get('logo'))
  }
  return undefined
}

export function logoURLFromAncestors(
  vendor: string,
  profiles: Map<string, Map<string, any>>,
  preferSmall = false
): string {
  let url = logoURL(vendor, profiles, preferSmall)
  if (url || !profiles.has(vendor)) {
    return url
  }
  const profile = profiles.get(vendor)
  if (!profile.has('parents') || profile.get('parents').length != 1) {
    return url
  }
  return logoURLFromAncestors(profile.get('parents')[0], profiles, preferSmall)
}

export function sliceFromEnd(array, endLength) {
  if (array.length > endLength) {
    array = array.slice(array.length - endLength, array.length)
  }
}

// Add an annotation for the most recent activity of an NLRB filing.
export function addLastUpdateAnnotationToNLRBFiling(filing) {
  let lastUpdateDate = filing.date_filed
  let lastUpdateType = 'Filed'
  for (const activity of filing.docket_activity) {
    if (!activity['date']) {
      continue
    }
    // Parse the date into month/day/year.
    let [month, day, year] = activity['date'].split('/')

    // Note that the month is zero-indexed.
    let updateDate = new Date(year, month - 1, day)
    if (updateDate >= lastUpdateDate) {
      lastUpdateDate = updateDate
      lastUpdateType = activity['document_type']
    }
  }
  filing['lastUpdateDate'] = lastUpdateDate
  filing['lastUpdateType'] = lastUpdateType
}

// Sort the filings by their most recent activity: whether filing or in the
// docket activity list.
export function sortNLRBFilingsByLastUpdate(filings) {
  filings.sort((a, b) => (a['lastUpdateDate'] > b['lastUpdateDate'] ? -1 : 1))
}

// Returns whether the given path exists within a JSON object. For example,
// given:
//
//   {
//    'something': {
//     'else': {
//      'entirely': 'here'
//     }
//    }
//   }
//
// the path ['something', 'else', 'entirely'] would return a true result while
// ['something', 'entirely', 'else'] would not.
//
export function jsonPathExists(json: Map<string, any>, path: string[]) {
  if (json == undefined) {
    return false
  }
  if (!path.length) {
    return true
  }

  let innerObj: Map<string, any> = json
  for (var index = 0; index < path.length - 1; index++) {
    const key = path[index]
    if (innerObj.has(key)) {
      innerObj = innerObj.get(key) as Map<string, any>
    } else {
      return false
    }
  }

  const key = path[path.length - 1]
  return innerObj.has(key)
}

// Returns the JSON object at the specified path (or undefined instead).
//
// NOTE: Due to nesting, this routine is not type safe. Care should be
// be taken to ensure that the 'path' array does not contain members which
// collide with member functions, e.g., 'clear', which would result in
// functions being returned instead of JSON members.
//
export function jsonGetFromPath(json: Map<string, any>, path: string[]) {
  if (json == undefined) {
    return undefined
  }
  if (!path.length) {
    return json
  }

  let innerObj: Map<string, any> = json
  for (var index = 0; index < path.length - 1; index++) {
    const key = path[index]
    if (innerObj.has(key)) {
      innerObj = innerObj.get(key) as Map<string, any>
    } else {
      return undefined
    }
  }

  const key = path[path.length - 1]
  if (innerObj.has(key)) {
    return innerObj.get(key)
  } else {
    return undefined
  }
}

// Returns whether a given number is greater than zero.
function isPositive(value: number): boolean {
  return value !== undefined && !isNaN(value) && value > 0
}

export function isString(object): boolean {
  return typeof object === 'string' || object instanceof String
}

export function formatPhoneNumber(phoneNumberString: string): string {
  const cleaned = ('' + phoneNumberString).replace(/\D/g, '')
  const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
  if (match) {
    return `(${match[1]}) ${match[2]}-${match[3]}`
  }
  return phoneNumberString
}

// Our server has shut down numerous times due to malicious users sending
// extremely long inputs.
export function truncateInput(value: string): string {
  const MAX_INPUT_LENGTH = 255
  if (!value) {
    return value
  }
  if (value.length > MAX_INPUT_LENGTH) {
    return value.slice(0, MAX_INPUT_LENGTH)
  } else {
    return value
  }
}

// Reads a boolean from a string with default value false.
export function readBoolean(value: string, name = ''): boolean {
  let result: boolean = false
  if (value) {
    result = Boolean(truncateInput(value))
    if (name) {
      console.log('Requested ' + name + ' = ' + result)
    }
  }
  return result
}

// Reads a number from a string with a given default value.
export function readNumber(
  value: string,
  defaultValue: number,
  name = ''
): number {
  let result: number = defaultValue
  if (value) {
    result = Number(truncateInput(value))
    if (name) {
      console.log('Requested ' + name + ' = ' + result)
    }
  }
  return result
}

// Reads a positive number from a string with a given default value.
export function readPositiveNumber(
  value: string,
  defaultValue: number,
  name = ''
): number {
  let result: number = defaultValue
  if (value) {
    const candidate: number = Number(truncateInput(value))
    if (isPositive(candidate)) {
      result = candidate
      if (name) {
        console.log('Requested ' + name + ' = ' + result)
      }
    }
  }
  return result
}

// Reads a number in [0, bound] from a string with a given default value.
export function readBoundedPositiveNumber(
  value: string,
  defaultValue: number,
  bound: number,
  name = ''
): number {
  let result: number = defaultValue
  if (value) {
    const candidate: number = Number(truncateInput(value))
    if (isPositive(candidate)) {
      result = Math.min(candidate, bound)
      if (name) {
        console.log('Requested ' + name + ' = ' + result)
      }
    }
  }
  return result
}

// Replaces URL encodings of a forward slash with a forward slash.
export function materializeForwardSlashes(value: string): string {
  return truncateInput(value).replace(/%2F/gi, '/')
}

// Pre-processes a URL-encoded web search phrase.
export function readWebSearchQuery(value: string): string {
  if (!value) {
    return ''
  }

  // We seem to get spammed with this query, which is both meaningless and
  // expensive to execute, leading to our autocomplete to cease to function.
  // The IP addresses executing the query tend to be associated with Hetzner
  // Online GmbH (according to ipgeolocation.io).
  if (truncateInput(value).toLowerCase() == 'dept') {
    console.log('Ignoring potentially malicious "dept" query.')
    return ''
  }

  return canonicalText(materializeForwardSlashes(value).replace(/\\/g, ''))
}

// Pre-processes a URL-encoded entity suggestion hint.
export function readEntitySuggestionHint(value: string): string {
  let hint: string = ''
  if (value) {
    // Remove any backslashes to prevent escapes and errors. We also force the
    // result into lowercase to simplify searching.
    hint = truncateInput(value)
      .replace(/\\/g, '')
      .replace(/'/g, "\\'")
      .replace(/\)/g, '')
      .replace(/\(/g, '')
      .replace(/=/g, '')
      .replace(/\*/g, '')
      .toLowerCase()
  }
  return hint
}

// Forms an AwardOrderingType value from a given string representing a boolean
// of whether the ordering should be MODIFIED_DATE. The defautl value is
// SIGNED_DATE.
export function readAwardOrderingType(value: string): string {
  let result: string = AwardOrderingType.SIGNED_DATE
  if (value) {
    result = AwardOrderingType.MODIFIED_DATE
  }
  return result
}

export function encodeEntity(entity: string): string {
  const encodedEntity = encodeURIComponentIncludingSlashes(entity)
  return explorerURLPath(`vendor/${encodedEntity}/`)
}

export function encodeSearch(term: string): string {
  const encodedTerm = encodeURIComponentIncludingSlashes(term)
  return explorerURLPath(`search?text=${encodedTerm}`)
}

export function encodeUSAgencySearch(
  department: string,
  agency: string,
  office: string
): string {
  return explorerURLPath(
    `usAgencySearch?department=${department}&agency=${agency}&office=${office}`
  )
}

export function encodeUSOfficeSearch(
  department: string,
  agency: string,
  office: string
): string {
  return explorerURLPath(
    `usOfficeSearch?department=${department}&agency=${agency}&office=${office}`
  )
}

export function expandToEntitySubtree(
  entity: string,
  excludeSubsidiaries: boolean,
  entityChildren
): string[] {
  let entitySubtree = new Set<string>()
  entitySubtree.add(entity)
  if (!excludeSubsidiaries) {
    for (const descendant of descendants(entity, entityChildren)) {
      entitySubtree.add(descendant)
    }
  }
  return Array.from(entitySubtree.values())
}

export function expandNames(
  entityList: string[],
  entityAlternates,
  entities = new Map<string, Map<string, any>>(),
  feedPath = undefined
): string[] {
  let nameSet = new Set<string>()
  for (const v of entityList) {
    const feedInfo = entities.has(v)
      ? jsonGetFromPath(entities.get(v), feedPath)
      : undefined
    if (feedInfo == undefined || !feedInfo.get('blockName')) {
      nameSet.add(v)
    }
    if (entityAlternates.has(v)) {
      for (const alternate of entityAlternates.get(v)) {
        nameSet.add(alternate)
      }
    }
    if (feedInfo != undefined && feedInfo.has('includes')) {
      for (const alternate of feedInfo.get('includes')) {
        nameSet.add(alternate)
      }
    }
  }
  return Array.from(nameSet.values())
}

export function canAssumeRecordsExistForName(
  name: string,
  dateRange: DateRange,
  entities,
  feedPath: string[],
  haveKey = undefined
): boolean {
  if (dateRange.hasStart() || dateRange.hasEnd()) {
    return false
  }
  if (!entities.has(name)) {
    return false
  }
  const profile = entities.get(name)
  const feedInfo = jsonGetFromPath(profile, feedPath)
  if (feedInfo != undefined) {
    if (haveKey) {
      if (feedInfo.get(haveKey)) {
        return true
      }
    } else {
      if (feedInfo.get('haveItem')) {
        return true
      }
      if (feedInfo.has('names') && feedInfo.get('names').length) {
        return true
      }
      if (feedInfo.has('includes') && feedInfo.get('includes').length) {
        return true
      }
    }
  }
  return false
}

export function canAssumeAgencyRecordsExistForName(
  name: string,
  dateRange: DateRange,
  entities,
  feedPath: string[]
): boolean {
  if (dateRange.hasStart() || dateRange.hasEnd()) {
    return false
  }
  if (!entities.has(name)) {
    return false
  }
  const profile = entities.get(name)
  const feedInfo = jsonGetFromPath(profile, feedPath)
  if (feedInfo != undefined) {
    if (feedInfo.has('agencyNames') && feedInfo.get('agencyNames').length) {
      return true
    }
    if (
      feedInfo.has('agencyIncludes') &&
      feedInfo.get('agencyIncludes').length
    ) {
      return true
    }
  }
  return false
}

export function canAssumeRecordsExist(
  names: string[],
  dateRange: DateRange,
  entities,
  feedPath: string[],
  haveKey = undefined
): boolean {
  for (const name of names) {
    if (
      canAssumeRecordsExistForName(name, dateRange, entities, feedPath, haveKey)
    ) {
      return true
    }
  }
  return false
}

export function canAssumeAgencyRecordsExist(
  names: string[],
  dateRange: DateRange,
  entities,
  feedPath: string[]
): boolean {
  for (const name of names) {
    if (
      canAssumeAgencyRecordsExistForName(name, dateRange, entities, feedPath)
    ) {
      return true
    }
  }
  return false
}

export function usLobbyistNameFromObject(lobbyist): string {
  let pieces = []
  if (lobbyist.prefix_display) {
    pieces.push(lobbyist.prefix_display)
  }
  if (lobbyist.first_name) {
    pieces.push(lobbyist.first_name)
  }
  if (lobbyist.nickname) {
    pieces.push(lobbyist.nickname)
  }
  if (lobbyist.last_name) {
    pieces.push(lobbyist.last_name)
  }
  if (lobbyist.suffix_display) {
    pieces.push(lobbyist.suffix_display)
  }
  return pieces.join(' ')
}

export function getCoalesceChain(inputs: string[]): string {
  return inputs.map((input) => `coalesce(${input}, '')`).join(" || ' ' || ")
}

export function getFullTextSearchString(
  config: string,
  inputs: string[]
): string {
  return `to_tsvector('${config}', ${getCoalesceChain(inputs)})`
}

// returns a function that composes all the passed in functions
// (invoked in order from right to left)
export const compose =
  (...fns) =>
  (x) =>
    fns.reduceRight((y, f) => f(y), x)

export const jsonObjectFileToMap = (path): Map<string, Map<string, any>> => {
  //minify to reduce memory footprint, as this is likely to be held onto in-mem

  const jsonContent = JSON.parse(fs.readFileSync(path, 'utf8'))
  return objectToMap(jsonContent)
}

export const timeoutPromise = (duration) =>
  new Promise((resolve) => {
    setTimeout(resolve, duration)
  })

export const getTimer = (name) => {
  //on first call, start timing- return object to stop timer
  let startTimestamp, stopTimestamp
  return {
    start: () => {
      startTimestamp = Date.now()
    },
    stop: () => {
      stopTimestamp = Date.now()
      const duration = stopTimestamp - startTimestamp
      console.log(`${name} timing duration: ${duration}`)
    },
  }
}

const nodeEnv = process.env.NODE_ENV
export const isDevelopmentBuild = () => {
  return nodeEnv === 'development'
}
export const ifDev = (devVal, prodVal) =>
  isDevelopmentBuild() ? devVal : prodVal

export async function nonemptyQuery(
  manager,
  queryStr: string,
  params: any[]
): Promise<boolean> {
  try {
    const result = await manager.query(queryStr, params)
    return result.length > 0
  } catch (err) {
    console.log(err)
  }
  return false
}

export async function getQueryFilings(
  manager,
  queryStr,
  params,
  maxSearchResults
) {
  let result = { filings: [], hitSearchCap: false }
  try {
    const filings = await manager.query(queryStr, params)
    const hitCap = filings.length == maxSearchResults
    result = { filings: filings, hitSearchCap: hitCap }
  } catch (err) {
    console.log(err)
  }
  return result
}

export async function getQueryBuilderFilings(query, maxSearchResults) {
  let result = { filings: [], hitSearchCap: false }
  try {
    const filings = await query.getMany()
    const hitCap = filings.length == maxSearchResults
    result = { filings: filings, hitSearchCap: hitCap }
  } catch (err) {
    console.log(err)
  }
  return result
}
