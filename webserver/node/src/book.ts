import * as util from './util'

async function bookAPI(request, response, entities, books, bookRedirects) {
  const tag = request.query.tag
  console.log(`Querying book info for ${tag}`)

  let result = {}
  if (books.has(tag)) {
    result = util.mapToObject(books.get(tag))
  } else if (bookRedirects.has(tag)) {
    console.log(`Had a redirect for ${tag} to ${bookRedirects.get(tag)}`)
    result = util.mapToObject(books.get(bookRedirects.get(tag)))
  } else {
    console.log(`No book page found for ${tag}`)
    util.setJSONResponse(response, undefined)
    return
  }

  function convertSalienceMap(m) {
    if (Array.isArray(m)) {
      return m.map(function (name) {
        return { name: name, logo: util.logoURLFromAncestors(name, entities) }
      })
    }
    let obj = {}
    for (const k of Object.keys(m)) {
      obj[k] = convertSalienceMap(m[k])
    }
    return obj
  }

  if (result.salienceMap != undefined) {
    result.annotatedSalienceMap = convertSalienceMap(result.salienceMap)
  }

  let entitiesToAnnotateSet = new Set<string>()
  for (const entity of result.entities) {
    if (util.isString(entity)) {
      entitiesToAnnotateSet.add(entity)
    } else {
      entitiesToAnnotateSet.add(entity.name)
    }
  }

  result['entityAnnotations'] = {}
  for (const entityName of entitiesToAnnotateSet) {
    const annotation = {
      stylizedText: util.getFullyStylizedName(entityName, entities),
      logo: util.logoURLFromAncestors(entityName, entities),
      logoSmall: util.logoURLFromAncestors(entityName, entities, true),
      url: util.primaryURL(entityName, entities),
    }
    result['entityAnnotations'][entityName] = annotation
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  router.get('/api/book', async function (request, response) {
    await bookAPI(
      request,
      response,
      entityState.entities,
      entityState.books,
      entityState.bookRedirects
    )
  })
}
