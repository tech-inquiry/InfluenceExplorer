import { dataSource } from '../App'
import * as tags from '../tags'
import * as util from '../util'

import { FeedInfo } from '../EntityState'

import {
  ILProcurementExempt,
  ILProcurementTender,
} from '../entity/il/procurement'

const kExemptTable = ILProcurementExempt
const kTenderTable = ILProcurementTender
const kExemptTableName = 'il_procurement_exempt'
const kTenderTableName = 'il_procurement_tender'

const kExemptTagDataset = 'il_procurement_exempt'
const kTenderTagDataset = 'il_procurement_tender'

const exemptFullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(title)',
  'lower(title_en)',
  'lower(publisher)',
  'lower(publisher_en)',
  'lower(exemption_reasons)',
  'lower(exemption_reasons_en)',
  'bid_number',
  'lower(bid_status)',
  'lower(bid_status_en)',
  'lower(bid_contact_name)',
  'lower(bid_contact_name_en)',
  'lower(bid_contact_mail)',
  'lower(subjects::text)',
  'lower(subjects_en::text)',
  'lower(engagements::text)',
  'lower(related_documents::text)',
])
const tenderFullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(title)',
  'lower(title_en)',
  'lower(publisher)',
  'lower(publisher_en)',
  'bid_number',
  'lower(bid_status)',
  'lower(bid_status_en)',
  'lower(bid_contact_name)',
  'lower(bid_contact_name_en)',
  'lower(subjects::text)',
  'lower(subjects_en::text)',
  'lower(related_documents::text)',
])

function nameTargetsFromNames(names: string[]): string[] {
  if (!names) {
    return []
  }
  return names.map((name) => `["${name.replace(/"/g, '\\"')}"]`)
}

function getExemptAgencyNames(
  name: string,
  entities: Map<string, Map<string, any>>
): string[] {
  let names = []
  if (!entities.has(name)) {
    return names
  }
  const profile = entities.get(name)
  const alternates = util.jsonGetFromPath(profile, [
    'feeds',
    'il',
    'procurement',
    'exempt',
    'agencyNames',
  ])
  if (alternates != undefined) {
    names = alternates
  }
  const includes = util.jsonGetFromPath(profile, [
    'feeds',
    'il',
    'procurement',
    'exempt',
    'agencyIncludes',
  ])
  if (includes != undefined) {
    names = names.concat(includes)
  }
  return names
}

function getTenderAgencyNames(
  name: string,
  entities: Map<string, Map<string, any>>
): string[] {
  let names = []
  if (!entities.has(name)) {
    return names
  }
  const profile = entities.get(name)
  const alternates = util.jsonGetFromPath(profile, [
    'feeds',
    'il',
    'procurement',
    'tender',
    'agencyNames',
  ])
  if (alternates != undefined) {
    names = alternates
  }
  const includes = util.jsonGetFromPath(profile, [
    'feeds',
    'il',
    'procurement',
    'tender',
    'agencyIncludes',
  ])
  if (includes != undefined) {
    names = names.concat(includes)
  }
  return names
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`bid_publication_date >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`bid_publication_date <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('bid_publication_date >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('bid_publication_date <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

function expandExemptNames(
  entityList: string[],
  entityAlternates: Map<string, string[]>,
  entities: Map<string, Map<string, any>>
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'il',
    'procurement',
    'export',
  ])
}

function expandTenderNames(
  entityList: string[],
  entityAlternates: Map<string, string[]>,
  entities: Map<string, Map<string, any>>
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'il',
    'procurement',
    'tender',
  ])
}

async function handleGetExemptTags(request, response) {
  return await tags.handleGetTags(request, response, kExemptTagDataset)
}

async function handleGetTenderTags(request, response) {
  return await tags.handleGetTags(request, response, kTenderTagDataset)
}

async function handleAddExemptTag(request, response) {
  return await tags.handleAddTag(request, response, kExemptTagDataset)
}

async function handleAddTenderTag(request, response) {
  return await tags.handleAddTag(request, response, kTenderTagDataset)
}

async function handleDeleteExemptTag(request, response) {
  return await tags.handleDeleteTag(request, response, kExemptTagDataset)
}

async function handleDeleteTenderTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTenderTagDataset)
}

function annotateFiling(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  agencyNormalization: Map<string, string>
) {
  filing['annotation'] = { vendors: [] }

  const publisher = filing['publisher']
  const normalizedPublisher = util.normalizeEntity(
    publisher,
    agencyNormalization
  )
  filing['annotation']['publisher'] = {
    origText: publisher,
    text: normalizedPublisher,
    stylizedText: util.getFullyStylizedName(normalizedPublisher, entities),
    stylizedTextEN: util.getFullyStylizedName(filing['publisher_en'], entities),
    logo: util.logoURLFromAncestors(normalizedPublisher, entities),
  }

  if (filing.hasOwnProperty('engagements')) {
    for (const engagement of filing['engagements']) {
      const vendor = engagement.supplier
      const normalizedVendor = util.normalizeEntity(vendor, entityNormalization)
      let annotation = {
        origText: vendor,
        text: normalizedVendor,
        stylizedText: util.getFullyStylizedName(normalizedVendor, entities),
        stylizedTextEN: util.getFullyStylizedName(
          engagement.supplier_en,
          entities
        ),
        logo: util.logoURLFromAncestors(normalizedVendor, entities),
      }
      filing['annotation']['vendors'].push(annotation)
    }
  }
}

function annotate(
  result,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  agencyNormalization: Map<string, string>
) {
  for (let filing of result.filings) {
    annotateFiling(filing, entities, entityNormalization, agencyNormalization)
  }
}

async function haveExemptFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const searchConstraint =
    exemptFullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kExemptTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveTenderFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const searchConstraint =
    tenderFullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTenderTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveExemptFromTagSearch(webSearch: string, token: string) {
  return await tags.haveFromTagSearch(webSearch, token, kExemptTagDataset)
}

async function haveTenderFromTagSearch(webSearch: string, token: string) {
  return await tags.haveFromTagSearch(webSearch, token, kTenderTagDataset)
}

async function haveExemptForNames(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(names, dateRange, entities, [
      'feeds',
      'il',
      'procurement',
      'exempt',
    ])
  ) {
    return true
  }

  let params = []

  const nameTargets = nameTargetsFromNames(names)
  const searchConstraint = `entities @> ANY(cast($${
    params.length + 1
  } as jsonb[]))`
  params.push(nameTargets)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kExemptTableName} WHERE ${constraintsStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveTenderForNames(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(names, dateRange, entities, [
      'feeds',
      'il',
      'procurement',
      'tender',
    ])
  ) {
    return true
  }

  let params = []

  const nameTargets = nameTargetsFromNames(names)
  const searchConstraint = `entities @> ANY(cast($${
    params.length + 1
  } as jsonb[]))`
  params.push(nameTargets)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTenderTableName} WHERE ${constraintsStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveExemptForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandExemptNames(names, entityAlternates, entities)
  return await haveExemptForNames(expandedNames, dateRange, entities)
}

async function haveTenderForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandTenderNames(names, entityAlternates, entities)
  return await haveTenderForNames(expandedNames, dateRange, entities)
}

async function haveExemptForAgencies(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  let params = []

  const searchConstraint = `lower(publisher) = ANY($${params.length + 1})`
  params.push(names)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kExemptTableName} WHERE ${constraintsStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveTenderForAgencies(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  let params = []

  const searchConstraint = `lower(publisher) = ANY($${params.length + 1})`
  params.push(names)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTenderTableName} WHERE ${constraintsStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveExemptForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (
    util.canAssumeAgencyRecordsExistForName(name, dateRange, entities, [
      'feeds',
      'il',
      'procurement',
      'exempt',
    ])
  ) {
    return true
  }

  const names = getExemptAgencyNames(name, entities)
  return await haveExemptForAgencies(names, dateRange, entities)
}

async function haveTenderForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (
    util.canAssumeAgencyRecordsExistForName(name, dateRange, entities, [
      'feeds',
      'il',
      'procurement',
      'tender',
    ])
  ) {
    return true
  }

  const names = getTenderAgencyNames(name, entities)
  return await haveTenderForAgencies(names, dateRange, entities)
}

async function getExemptFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  const fullTextQuery =
    exemptFullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kExemptTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('bid_update_date', 'DESC').limit(maxSearchResults)

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getTenderFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  const fullTextQuery =
    tenderFullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTenderTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('bid_publication_date', 'DESC').limit(maxSearchResults)

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getExemptFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['bid_number']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kExemptTableName,
    kExemptTagDataset,
    uniqueKeys
  )
}

async function getTenderFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['bid_number']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTenderTableName,
    kTenderTagDataset,
    uniqueKeys
  )
}

async function getExemptForNames(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  let params = []

  const nameTargets = nameTargetsFromNames(names)
  const searchConstraint = `entities @> ANY(cast($${
    params.length + 1
  } as jsonb[]))`
  params.push(nameTargets)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  let queryStr = `
    SELECT * FROM ${kExemptTableName} WHERE ${constraintsStr}
    ORDER BY bid_update_date DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getTenderForNames(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  let params = []

  const nameTargets = nameTargetsFromNames(names)
  const searchConstraint = `entities @> ANY(cast($${
    params.length + 1
  } as jsonb[]))`
  params.push(nameTargets)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  let queryStr = `
    SELECT * FROM ${kTenderTableName} WHERE ${constraintsStr}
    ORDER BY bid_publication_date DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getExemptForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  maxSearchResults: number
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandExemptNames(names, entityAlternates, entities)
  return await getExemptForNames(expandedNames, dateRange, maxSearchResults)
}

async function getTenderForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  maxSearchResults: number
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandTenderNames(names, entityAlternates, entities)
  return await getTenderForNames(expandedNames, dateRange, maxSearchResults)
}

async function getExemptForAgencies(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  let params = []

  const searchConstraint = `lower(publisher) = ANY($${params.length + 1})`
  params.push(names)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  let queryStr = `
    SELECT * FROM ${kExemptTableName} WHERE ${constraintsStr}
    ORDER BY bid_update_date DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getTenderForAgencies(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  let params = []

  const searchConstraint = `lower(publisher) = ANY($${params.length + 1})`
  params.push(names)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  let queryStr = `
    SELECT * FROM ${kTenderTableName} WHERE ${constraintsStr}
    ORDER BY bid_publication_date DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getExemptForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  maxSearchResults: number
) {
  const names = getExemptAgencyNames(name, entities)
  return await getExemptForAgencies(names, dateRange, maxSearchResults)
}

async function getTenderForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  maxSearchResults: number
) {
  const names = getTenderAgencyNames(name, entities)
  return await getTenderForAgencies(names, dateRange, maxSearchResults)
}

async function haveExemptOverDateRange(
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kExemptTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveTenderOverDateRange(
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTenderTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getExemptOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  let query = dataSource.getRepository(kExemptTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('bid_update_date', 'DESC').limit(maxSearchResults)

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getTenderOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  let query = dataSource.getRepository(kTenderTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('bid_publication_date', 'DESC').limit(maxSearchResults)

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveExemptAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Boolean IL Exempt contracts w/ text: ${text}`)
    result = await haveExemptFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have IL exempt w/ tag: ${tagText}`)
    result = await haveExemptFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean IL exempt contracts w/ entity: ${name}`)
    result = await haveExemptForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean IL exempt contracts w/ agency: ${name}`)
    result = await haveExemptForAgency(name, dateRange, entities)
  } else {
    console.log('IL Exempt contract API')
    result = await haveExemptOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function haveTenderAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Boolean IL Tender contracts w/ text: ${text}`)
    result = await haveTenderFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have IL tender w/ tag: ${tagText}`)
    result = await haveTenderFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean IL tenders w/ entity: ${name}`)
    result = await haveTenderForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean IL tenders w/ agency: ${name}`)
    result = await haveTenderForAgency(name, dateRange, entities)
  } else {
    console.log('IL Tender contract API')
    result = await haveTenderOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getExemptAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`IL Exempt contracts w/ text: ${text}`)
    result = await getExemptFromSearch(text, dateRange, maxSearchResults)
  } else if (tagText) {
    console.log(`IL exempt w/ tag: ${tagText}`)
    result = await getExemptFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`IL Exempt contract w/ entity: ${name}`)
    result = await getExemptForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names,
      maxSearchResults
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`IL Exempt contract w/ agency: ${name}`)
    result = await getExemptForAgency(
      name,
      dateRange,
      entities,
      maxSearchResults
    )
  } else {
    console.log('IL Exempt contract API')
    result = await getExemptOverDateRange(dateRange, maxSearchResults)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(
      result,
      entities,
      feedInfo.normalization,
      feedInfo.agencyNormalization
    )
  }

  util.setJSONResponse(response, result)
}

async function getTenderAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`IL Tender contracts w/ text: ${text}`)
    result = await getTenderFromSearch(text, dateRange, maxSearchResults)
  } else if (tagText) {
    console.log(`IL tender w/ tag: ${tagText}`)
    result = await getTenderFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`IL Tender w/ entity: ${name}`)
    result = await getTenderForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names,
      maxSearchResults
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`IL Tenders w/ agency: ${name}`)
    result = await getTenderForAgency(
      name,
      dateRange,
      entities,
      maxSearchResults
    )
  } else {
    console.log('IL Tender contract API')
    result = await getTenderOverDateRange(dateRange, maxSearchResults)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(
      result,
      entities,
      feedInfo.normalization,
      feedInfo.agencyNormalization
    )
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/il/procurement'

  const procurementFeeds = entityState.feeds.get('il').get('procurement')
  const exemptFeed = procurementFeeds.get('exempt')
  const tenderFeed = procurementFeeds.get('tender')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether there are any exempt contracts
  router.get(`${route}/exempt/have`, async function (request, response) {
    await haveExemptAPI(request, response, entities, children, exemptFeed)
  })

  // Returns whether there are any tenders
  router.get(`${route}/tender/have`, async function (request, response) {
    await haveTenderAPI(request, response, entities, children, tenderFeed)
  })

  // Returns JSON for exempt contract filings.
  router.get(`${route}/exempt/get`, async function (request, response) {
    await getExemptAPI(request, response, entities, children, exemptFeed)
  })

  // Returns JSON for tenders.
  router.get(`${route}/tender/get`, async function (request, response) {
    await getTenderAPI(request, response, entities, children, tenderFeed)
  })

  router.post(`${route}/exempt/getTags`, handleGetExemptTags)
  router.post(`${route}/tender/getTags`, handleGetTenderTags)

  router.post(`${route}/exempt/addTag`, handleAddExemptTag)
  router.post(`${route}/tender/addTag`, handleAddTenderTag)

  router.post(`${route}/exempt/deleteTag`, handleDeleteExemptTag)
  router.post(`${route}/tender/deleteTag`, handleDeleteTenderTag)
}
