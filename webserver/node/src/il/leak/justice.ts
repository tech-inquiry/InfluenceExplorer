import { dataSource, getUsernames } from '../../App'
import * as tags from '../../tags'
import * as util from '../../util'

// This is not yet used.
import { FeedInfo } from '../../EntityState'

import { ILLeakMinistryOfJustice } from '../../entity/il/leak/justice'

const kTable = ILLeakMinistryOfJustice
const kTableName = 'il_leak_ministry_of_justice'

const kTagDataset = 'il_leak_ministry_of_justice'

const fullTextSearchString = 'tsvector_col'

function getEmailTargets(emails: string[]): string[] {
  if (!emails) {
    return []
  }
  return emails.map((email) => `["${email.replace(/"/g, '\\"')}"]`)
}

function getEmails(
  name: string,
  entities: Map<string, Map<string, any>>,
  children: Map<string, string[]>
) {
  const descendants = util.expandToEntitySubtree(name, false, children)

  let emailSet = new Set<string>()
  for (const descendant of descendants) {
    if (!entities.has(descendant)) {
      continue
    }
    const profile = entities.get(descendant)
    const emails = util.jsonGetFromPath(profile, [
      'feeds',
      'il',
      'leak',
      'justice',
      'emails',
    ])
    if (emails) {
      for (const email of emails) {
        emailSet.add(email)
      }
    }
  }

  return Array.from(emailSet)
}

function getReferences(
  name: string,
  entities: Map<string, Map<string, any>>,
  children: Map<string, string[]>
) {
  const descendants = util.expandToEntitySubtree(name, false, children)

  let references = [new Set<string>(), new Set<string>()]
  for (const descendant of descendants) {
    if (!entities.has(descendant)) {
      continue
    }
    const profile = entities.get(descendant)
    const entityRefs = util.jsonGetFromPath(profile, [
      'feeds',
      'il',
      'leak',
      'justice',
      'references',
    ])
    if (entityRefs) {
      for (const part of [0, 1]) {
        if (!Array.isArray(entityRefs[part])) {
          console.log(`entityRefs was not an array for ${name}`)
          continue
        }
        for (const emailID of entityRefs[part]) {
          if (util.isString(emailID)) {
            references[part].add(emailID)
          } else if (emailID.has('id')) {
            references[part].add(emailID.get('id'))
          }
        }
      }
    }
  }

  return [Array.from(references[0]), Array.from(references[1])]
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

async function haveFromSearch(webSearch: string): Promise<boolean> {
  let params = []

  const searchConstraint =
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${searchConstraint} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function haveForName(
  name: string,
  entities: Map<string, Map<string, any>>,
  children: Map<string, string[]>
): Promise<boolean> {
  const emails = getEmails(name, entities, children)
  const references = getReferences(name, entities, children)
  return emails.length || references[0].length > 0 || references[1].length > 0
}

async function getFromSearch(
  webSearch: string,
  maxSearchResults: number,
  maxTextLength = 1000
) {
  let params = []

  const searchConstraint =
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)

  const queryStr = `SELECT email_id, part, LEFT(text, ${maxTextLength}) AS text_start FROM ${kTableName} WHERE ${searchConstraint} ORDER BY email_id DESC LIMIT $${
    params.length + 1
  }`
  params.push(maxSearchResults)

  const filings = await dataSource.manager.query(queryStr, params)
  return { filings: filings, hitSearchCap: filings.length == maxSearchResults }
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number,
  maxTextLength = 1000
) {
  const usernames = getUsernames(token)

  let params = []

  const queryStr = `SELECT email_id, part, LEFT(text, ${maxTextLength}) AS text_start FROM ${kTableName} WHERE ROW(email_id, part::text) in (
    SELECT unique_data->>'email_id', unique_data->>'part'
      FROM tags WHERE dataset = $${params.length + 1}
      AND username = ANY($${params.length + 2})
      AND LOWER(tag) @@ websearch_to_tsquery($${params.length + 3}))
      LIMIT $${params.length + 4}`
  params.push(kTagDataset)
  params.push(usernames)
  params.push(webSearch)
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getForName(
  name: string,
  entities: Map<string, Map<string, any>>,
  children: Map<string, string[]>,
  maxSearchResults: number,
  maxTextLength = 1000
) {
  const emails = getEmails(name, entities, children)
  const references = getReferences(name, entities, children)

  const emailTargets = getEmailTargets(emails)

  let params = []

  const searchConstraint = `
    (emails @> ANY(cast($${params.length + 1} as jsonb[]))) OR
    (part = 0 AND email_id = ANY($${params.length + 2})) OR
    (part = 1 AND email_id = ANY($${params.length + 3}))`
  params.push(emailTargets)
  params.push(references[0])
  params.push(references[1])

  const queryStr = `SELECT email_id, part, LEFT(text, ${maxTextLength}) AS text_start FROM ${kTableName} WHERE ${searchConstraint} ORDER BY email_id DESC LIMIT $${
    params.length + 1
  }`
  params.push(maxSearchResults)

  const filings = await dataSource.manager.query(queryStr, params)
  return { filings: filings, hitSearchCap: filings.length == maxSearchResults }
}

async function haveAPI(request, response, entities, children) {
  const params = request.query

  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (params.entity) {
    const name = util.canonicalText(params.entity)
    console.log(`Boolean IL Ministry of Justice emails w/ name: ${name}`)
    result = await haveForName(name, entities, children)
  } else if (text) {
    console.log(`Boolean IL Ministry of Justice emails w/ text: ${text}`)
    result = await haveFromSearch(text)
  } else if (tagText) {
    console.log(`Boolean IL Ministry of Justice emails w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(request, response, entities, children) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  const maxTextLengthDefault = 1000
  const maxTextLength = util.readPositiveNumber(
    params.maxTextLength,
    maxTextLengthDefault
  )

  let result = { filings: [], hitSearchCap: false }
  if (params.entity) {
    const name = util.canonicalText(params.entity)
    console.log(`IL Ministry of Justice emails w/ entity: ${name}`)
    result = await getForName(
      name,
      entities,
      children,
      maxSearchResults,
      maxTextLength
    )
  } else if (text) {
    console.log(`IL Ministry of Justice emails w/ text: ${text}`)
    result = await getFromSearch(text, maxSearchResults, maxTextLength)
  } else if (tagText) {
    console.log(`IL Ministry of Justice emails w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const parentRoute = '/api/il/leak'

  const entities = entityState.entities
  const children = entityState.children

  // Returns whether there are any emails matching a search.
  router.get(
    `${parentRoute}/haveJusticeEmail`,
    async function (request, response) {
      await haveAPI(request, response, entities, children)
    }
  )

  // Returns JSON for emails.
  router.get(`${parentRoute}/justiceEmail`, async function (request, response) {
    await getAPI(request, response, entities, children)
  })

  router.post(`${parentRoute}/justice/getTags`, handleGetTags)
  router.post(`${parentRoute}/justice/addTag`, handleAddTag)
  router.post(`${parentRoute}/justice/deleteTag`, handleDeleteTag)
}
