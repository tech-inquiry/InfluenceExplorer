import { dataSource, decodeToken, kGlobalUsername, verifyToken } from './App'
import { Tag } from './entity/tag'
import * as util from './util'

export function getUsernames(token: string): string[] {
  let usernames = [kGlobalUsername]
  if (verifyToken(token)) {
    const decodedToken = decodeToken(token)
    if (decodedToken.username != kGlobalUsername) {
      usernames.push(decodedToken.username)
    }
  }
  return usernames
}

export async function handleGetTags(request, response, dataset: string) {
  const { token, uniqueData } = request.body

  const usernames = getUsernames(token)

  let params = []

  // TODO(Jack Poulson): Add an ORDER BY? Shouldn't be needed for tags.
  let queryStr = `SELECT tag, is_entity, username FROM tags WHERE
    dataset = $${params.length + 1} AND username = ANY($${params.length + 2})`
  params.push(dataset)
  params.push(usernames)

  for (let uniqueKey in uniqueData) {
    queryStr += ` AND unique_data->>'${uniqueKey}' = $${params.length + 1}`
    params.push(uniqueData[uniqueKey])
  }

  try {
    const tags = await dataSource.manager.query(queryStr, params)
    return response.status(200).json({ message: 'success', tags: tags })
  } catch (err) {
    console.log(err)
    return response.status(401).json({ message: 'error' })
  }
}

export async function handleAddTag(request, response, dataset: string) {
  const { token, uniqueData, tag, isEntity } = request.body
  if (!verifyToken(token)) {
    console.log(
      `Attempted update to tag prime filing ${uniqueData} with tag ${tag} using invalid token ${token}`
    )
    return response.status(401).json({ message: 'Invalid token' })
  }
  const decodedToken = decodeToken(token)
  const username = decodedToken.username
  const updateTime = new Date().toISOString()

  try {
    const item = {
      username: username,
      tagged_date: updateTime,
      dataset: dataset,
      unique_data: uniqueData,
      tag: tag,
      is_entity: isEntity,
    }

    const repo = dataSource.getRepository(Tag)
    const suggestion = repo.create(item)
    await repo.save(suggestion)
    return response.status(200).json({ message: 'success', item: item })
  } catch (err) {
    console.log(err)
    return response.status(401).json({ message: 'error' })
  }
}

export async function handleDeleteTag(request, response, dataset: string) {
  const { token, uniqueData, tag, isEntity } = request.body
  if (!verifyToken(token)) {
    console.log(
      `Attempted update to tag prime filing ${uniqueData} with tag ${tag} using invalid token ${token}`
    )
    return response.status(401).json({ message: 'Invalid token' })
  }
  const decodedToken = decodeToken(token)
  const username = decodedToken.username
  const updateTime = new Date().toISOString()

  try {
    let params = []

    // TODO(Jack Poulson): Add an ORDER BY? Shouldn't be needed for tags.
    let queryStr = `DELETE FROM tags WHERE
      dataset = $${params.length + 1}
      AND username = $${params.length + 2}
      AND tag = $${params.length + 3}
      AND is_entity = $${params.length + 4}`
    params.push(dataset)
    params.push(decodedToken.username)
    params.push(tag)
    params.push(isEntity)

    for (let uniqueKey in uniqueData) {
      queryStr += ` AND unique_data->>'${uniqueKey}' = $${params.length + 1}`
      params.push(uniqueData[uniqueKey])
    }

    await dataSource.manager.query(queryStr, params)

    return response.status(200).json({ message: 'success' })
  } catch (err) {
    console.log(err)
    return response.status(401).json({ message: 'error' })
  }
}

export async function haveFromTagSearch(
  webSearch: string,
  token: string,
  dataset: string
) {
  const usernames = getUsernames(token)

  let params = []

  const queryStr = `SELECT 1 FROM tags WHERE
    dataset = $${params.length + 1} AND username = ANY($${
    params.length + 2
  }) AND
    LOWER(tag) @@ websearch_to_tsquery($${params.length + 3})`
  params.push(dataset)
  params.push(usernames)
  params.push(webSearch)

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

export async function getUniqueDataFromSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number,
  dataset: string
) {
  const usernames = getUsernames(token)

  let params = []

  // TODO(Jack Poulson): Add an ORDER BY? Shouldn't be needed for tags.
  const queryStr = `SELECT unique_data FROM tags WHERE dataset = $${
    params.length + 1
  } AND username = ANY($${
    params.length + 2
  }) AND LOWER(tag) @@ websearch_to_tsquery($${params.length + 3}) LIMIT $${
    params.length + 4
  }`
  params.push(dataset)
  params.push(usernames)
  params.push(webSearch)
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

export async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number,
  tableName: string,
  dataset: string,
  uniqueKeys = ['id']
) {
  const usernames = getUsernames(token)

  let params = []

  let queryStr = undefined
  if (uniqueKeys.length == 1) {
    const uniqueKey = uniqueKeys[0]
    queryStr = `SELECT * FROM ${tableName} WHERE ${uniqueKey}::text in (
      SELECT unique_data->>'${uniqueKey}' FROM tags WHERE
        dataset = $${params.length + 1} AND
        username = ANY($${params.length + 2}) AND
        LOWER(tag) @@ websearch_to_tsquery($${params.length + 3})
    ) LIMIT $${params.length + 4}`
    params.push(dataset)
    params.push(usernames)
    params.push(webSearch)
    params.push(maxSearchResults)
  } else if (uniqueKeys.length == 2) {
    // This branch is now redundant given the existence of arbitrary length
    // alternative below.
    queryStr = `SELECT * FROM ${tableName} WHERE ROW(${uniqueKeys[0]}::text, ${
      uniqueKeys[1]
    }::text
    ) in (
      SELECT unique_data->>'${uniqueKeys[0]}', unique_data->>'${uniqueKeys[1]}'
        FROM tags WHERE dataset = $${params.length + 1}
        AND username = ANY($${params.length + 2})
        AND LOWER(tag) @@ websearch_to_tsquery($${params.length + 3}))
        LIMIT $${params.length + 4}`
    params.push(dataset)
    params.push(usernames)
    params.push(webSearch)
    params.push(maxSearchResults)
  } else {
    let lhsEntries = []
    let rhsEntries = []
    for (const uniqueKey of uniqueKeys) {
      lhsEntries.push(`${uniqueKey}::text`)
      rhsEntries.push(`unique_data->>'${uniqueKey}'`)
    }
    const lhsRow = lhsEntries.join(', ')
    const rhsRow = rhsEntries.join(', ')

    queryStr = `SELECT * FROM ${tableName} WHERE ROW(${lhsRow}) IN (
      SELECT ${rhsRow} FROM tags WHERE dataset = $${params.length + 1}
        AND username = ANY($${params.length + 2})
        AND LOWER(tag) @@ websearch_to_tsquery($${params.length + 3})
    ) LIMIT $${params.length + 4}`
    params.push(dataset)
    params.push(usernames)
    params.push(webSearch)
    params.push(maxSearchResults)
  }

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}
