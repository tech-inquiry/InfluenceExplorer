//must have this require before any other code executes
require('dotenv').config({
  path: __dirname + '/../' + '.env',
})

import { app, dataSource } from './App'

//https://pm2.keymetrics.io/docs/usage/signals-clean-restart/
process.on('SIGINT', async () => {
  try {
    await dataSource.close()
  } catch (e) {
    console.log('error on shutting down server:', e)
  } finally {
    process.exit(0)
  }
})

const handleUncaught = (e) => {
  console.log(
    'An error has occured. error is: %s and stack trace is: %s',
    e,
    e.stack
  )
  console.log('Process will be restarted by pm2 now.')
  process.exit(1)
}

process.on('uncaughtException', handleUncaught)
process.on('unhandledRejection', handleUncaught)

const kPort = Number(process.env.NODE_PORT) || 3000
const kServer = process.env.NODE_SERVER || '127.0.0.1'
console.log(kPort, process.env.NODE_PORT, process.env.NODE_ENV)
app.start(kPort, kServer)
