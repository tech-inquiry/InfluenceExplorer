import { dataSource } from '../App'
import { NZProcurementFeedInfo } from '../EntityState'
import { NZProcurement } from '../entity/nz/procurement'
import * as tags from '../tags'
import * as util from '../util'

// TODO(Jack Poulson): Incorporate 'alternates' keywords in entity lookups since
// New Zealand's procurement records do not label the awarded vendors in a
// column.

const kTable = NZProcurement
const kTableName = 'nz_award_notice'

const kTagDataset = 'nz_award_notice'

const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(posting_agency)',
  'lower(notice_id)',
  'lower(notice_type)',
  'lower(competition_type)',
  'lower(title)',
  'lower(reference_number)',
  'lower(department)',
  'lower(alternate_address)',
  'lower(overview)',
  'lower(comments)',
])

function buildDateConstraint(dateRange: util.DateRange, params): string {
  const constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`awarded_date >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`awarded_date <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('awarded_date >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('awarded_date <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

function getNoticeIDs(
  entityList: string[],
  entityNoticeIDs: Map<string, string[]>
): string[] {
  let noticeIDSet = new Set<string>()
  for (const v of entityList) {
    if (entityNoticeIDs.has(v)) {
      for (const noticeID of entityNoticeIDs.get(v)) {
        noticeIDSet.add(noticeID)
      }
    }
  }
  return Array.from(noticeIDSet.values())
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateFiling(
  filing,
  entities: Map<string, Map<string, any>>,
  feedInfo: NZProcurementFeedInfo
): void {
  let annotation = {}
  const noticeID = filing.notice_id.toLowerCase()
  if (noticeID in feedInfo.noticeIDNormalization) {
    const normalizedVendors = feedInfo.noticeIDNormalization[noticeID]
    annotation['vendors'] = []
    for (const normalizedVendor of normalizedVendors) {
      annotation['vendors'].push({
        text: normalizedVendor,
        stylizedText: util.getFullyStylizedName(normalizedVendor, entities),
        url: util.encodeEntity(normalizedVendor),
        logo: util.logoURLFromAncestors(normalizedVendor, entities),
      })
    }
  }
  filing['annotation'] = annotation
}

function annotate(
  result,
  entities: Map<string, Map<string, any>>,
  feedInfo: NZProcurementFeedInfo
): void {
  for (let filing of result.filings) {
    annotateFiling(filing, entities, feedInfo)
  }
}

async function haveForNoticeIDs(
  noticeIDs: string[],
  dateRange: util.DateRange
): Promise<boolean> {
  if (!noticeIDs.length) {
    return false
  }
  if (!(dateRange.hasStart() || dateRange.hasEnd())) {
    return true
  }

  let params = []

  const searchConstraint = `LOWER(notice_id) = ANY($${params.length + 1})`
  params.push(noticeIDs)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 100`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entityChildren: Map<string, string[]>,
  feedInfo: NZProcurementFeedInfo
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const noticeIDs = getNoticeIDs(names, feedInfo.noticeIDAlternates)
  return await haveForNoticeIDs(noticeIDs, dateRange)
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const searchConstraint =
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 100`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getForNoticeIDs(
  noticeIDs: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let result = { filings: [], hitSearchCap: false }
  if (!noticeIDs.length) {
    return result
  }

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where('LOWER(notice_id) = ANY(ARRAY[:...noticeIDs])', {
      noticeIDs: noticeIDs,
    })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('awarded_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entityChildren: Map<string, string[]>,
  feedInfo: NZProcurementFeedInfo,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const noticeIDs = getNoticeIDs(names, feedInfo.noticeIDAlternates)
  return await getForNoticeIDs(
    noticeIDs,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('awarded_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['notice_id']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 100`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('awarded_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: NZProcurementFeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Boolean NZ Awards w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have NZ contracts w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean NZ Awards w/ entity: ${name}`)
    result = await haveForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entityChildren,
      feedInfo
    )
  } else {
    console.log('Boolean NZ Awards over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: NZProcurementFeedInfo
) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`NZ Awards w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`NZ contracts w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`NZ Awards w/ entity: ${name}`)
    result = await getForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entityChildren,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('NZ Awards over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, feedInfo)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/nz/procurement'

  const feed = entityState.feeds.get('nz').get('procurement')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether award notices exist for a web search or an entity.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, feed)
  })

  // Returns JSON for award notices for a web search or an entity.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(request, response, entities, children, feed)
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
