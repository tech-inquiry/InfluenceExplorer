import { dataSource } from './App'
import * as util from './util'

import { Webtext } from './entity/Webtext'

const fullTextSearchString =
  " to_tsvector('simple', lower(text) || ' ' || lower(entities::TEXT))"

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  // TODO(Jack Poulson): Add date filtering.
  const dateWhere = ''

  const queryStr =
    'SELECT 1 FROM webtext WHERE ' +
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})` +
    dateWhere +
    ' LIMIT 1000'
  params.push(webSearch)

  const query = dataSource.manager.query(queryStr, params)

  let haveResult = false
  try {
    const result = await query
    haveResult = result.length > 0
  } catch (err) {
    console.log(err)
  }

  return haveResult
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []
  const queryStr =
    'SELECT url, entities FROM webtext WHERE ' +
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1}) ` +
    ` LIMIT $${params.length + 2}`
  params.push(webSearch)
  params.push(maxSearchResults)

  const query = dataSource.manager.query(queryStr, params)

  let results = { filings: [], hitSearchCap: false }
  try {
    const filings = await query
    const hitCap = filings.length == maxSearchResults
    results = { filings: filings, hitSearchCap: hitCap }
  } catch (err) {
    console.log(err)
  }
  return results
}

async function mainAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>
) {
  const params = request.query

  const maxSearchResultsDefault = 100
  const maxSearchResultsCap = 1000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const annotateResults = !util.readBoolean(params.disableAnnotations)
  const text = util.readWebSearchQuery(params.text)

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log('Webtext API w/ text: ' + text + ', offset: ' + searchOffset)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else {
    // N/A
  }

  if (annotateResults) {
    for (const filing of result.filings) {
      filing.entityAnnotations = []
      for (const entity of filing.entities) {
        filing.entityAnnotations.push({
          text: entity,
          stylizedText: util.getFullyStylizedName(entity, entities),
          logo: util.logoURLFromAncestors(entity, entities),
        })
      }
    }
  }

  response.setHeader('Content-Type', 'application/json')
  response.end(JSON.stringify(result, null, 1))
}

export function setRoutes(router, entityState) {
  // Returns JSON for webtext matches.
  router.get('/api/webtext', async function (request, response) {
    await mainAPI(request, response, entityState.entities)
  })
}
