import * as fs from 'fs'
import * as express from 'express'
import * as jwt from 'jsonwebtoken'
import * as typeorm from 'typeorm'

import * as autocomplete from './autocomplete'
import * as book from './book'
import * as auProcurement from './au/procurement'
import * as cablegate from './us/central/cablegate'
import * as caCanadaBuys from './ca/procurement/canada_buys'
import * as calaccessLobbyDisc from './us/ca/lobbying/activity'
import * as calaccessReceipts from './us/ca/lobbying/contrib'
import * as caLobbyingComm from './ca/lobbying/communication'
import * as caLobbyingLobbyist from './ca/lobbying/lobbyist'
import * as caLobbyingRegistration from './ca/lobbying/registration'
import * as caProactive from './ca/procurement/proactive'
import * as caPWSGC from './ca/procurement/pwsgc'
import * as euLobbying from './eu/central/lobbying'
import * as euProcurement from './eu/procurement'
import * as fara from './us/central/fara'
import * as ilLeakDefense from './il/leak/defense'
import * as ilLeakIDF from './il/leak/idf'
import * as ilLeakJustice from './il/leak/justice'
import * as ilProcurement from './il/procurement'
import * as nlrb from './us/central/laborRelations'
import * as nzProcurement from './nz/procurement'
import * as sec from './us/central/securities'
import * as tableUpdates from './tableUpdates'
import * as uaTender from './ua/procurement/tender'
import * as ukProcurement from './uk/procurement'
import * as usAZProcurement from './us/az/procurement'
import * as usCAProcurement from './us/ca/procurement'
import * as usCentralFECIndividual from './us/central/lobbying/fec/individual'
import * as usCentralGrants from './us/central/grants'
import * as usCentralOpportunities from './us/central/procurement/opportunities'
import * as usCentralOPRContributions from './us/central/lobbying/opr/contributions'
import * as usCentralOPRIssues from './us/central/lobbying/opr/issues'
import * as usCentralProcurement from './us/central/procurement'
import * as usCentralSubawards from './us/central/procurement/subaward'
import * as usCentralSubgrants from './us/central/subgrants'
import * as usDODAnnouncements from './us/central/dod'
import * as usFLProcurement from './us/fl/procurement'
import * as usNYProcurement from './us/ny/procurement'
import * as usTeixeira from './us/central/teixeira'
import * as usTXDPSProcurement from './us/tx/dps_procurement'
import * as util from './util'
import * as vendor from './vendor'
import * as webtext from './webtext'

import * as credentials from '../secrets/credentials.json'

import { postEntitySubmissionSchema } from './endpointValidation'
import { sendEntitySubmissionEmail } from './email'
import { EntityState } from './EntityState'
import path = require('path')

const compression = require('compression')
const cors = require('cors')

const kHostname: string =
  process.env.EXPLORER_HOSTNAME || 'https://techinquiry.org'
const kExplorerRoute: string = process.env.EXPLORER_ROUTE || '/'

const kDataPathRelToSource = '../../../data/'

const kEntitiesFilename = 'entities.json'
const kEntitiesFileRelSource = kDataPathRelToSource + kEntitiesFilename

const kUSFederalOfficesFilename = 'us_federal_offices.json'
const kUSFederalOfficesFileRelSource =
  kDataPathRelToSource + kUSFederalOfficesFilename

const kUSFederalSubawardDepartmentsFilename = 'us_federal_subaward_depts.json'
const kUSFederalSubawardDepartmentsFileRelSource =
  kDataPathRelToSource + kUSFederalSubawardDepartmentsFilename

const kCitationsFilename = 'citations.json'
const kCitationsFileRelSource = kDataPathRelToSource + kCitationsFilename

let kEntityState = new EntityState(
  util.objectToMap(require(kEntitiesFileRelSource)),
  util.objectToMap(require(kCitationsFileRelSource)),
  util.objectToMap(require(kUSFederalOfficesFileRelSource)),
  util.objectToMap(require(kUSFederalSubawardDepartmentsFileRelSource))
)

const kFPDSProductOrServiceCodesFilename = 'fpds_product_or_service_codes.json'
const kFPDSProductOrServiceCodesFileRelSource =
  kDataPathRelToSource + kFPDSProductOrServiceCodesFilename

const kFPDSProductOrServiceCodes = util.objectToMap(
  require(kFPDSProductOrServiceCodesFileRelSource)
)

// TODO(Jack Poulson): Allow for different database sources for different
// tables. This is significantly easier since switching from
// typeorm.getConnection to typeorm.DataSource.
export let dataSource: typeorm.DataSource = undefined

export const kGlobalUsername = 'global-username'

function validCredentials(user: string, password: string): boolean {
  return credentials.hasOwnProperty(user) && password == credentials[user]
}

export function verifyToken(token: string, maxAgeInHours = 3): boolean {
  if (!token || token == undefined || token == 'undefined') {
    return false
  }

  const secretKey = process.env.JWT_SECRET_KEY
  if (!secretKey) {
    return false
  }

  try {
    const decodedToken = jwt.verify(token, secretKey)
    if (!decodedToken) {
      return false
    }

    const hour = 1000 * 60 * 60
    if (decodedToken.signInTime < Date.now() - maxAgeInHours * hour) {
      return false
    }

    return true
  } catch (error) {
    console.log(`Invalid token=${token}`)
    return false
  }
}

export function decodeToken(token: string) {
  if (!token) {
    return undefined
  }

  const secretKey = process.env.JWT_SECRET_KEY
  if (!secretKey) {
    return undefined
  }

  try {
    return jwt.verify(token, secretKey)
  } catch (error) {
    console.log(error)
    return undefined
  }
}

function handleCheckTokenPost(req, res) {
  const token = req.body.token
  if (verifyToken(token)) {
    return res.status(200).json({ message: 'success', token: token })
  } else {
    return res.status(401).json({ message: 'Invalid token' })
  }
}

function handleAuthPost(req, res) {
  const secretKey = process.env.JWT_SECRET_KEY
  if (!secretKey) {
    return res.status(401).json({ message: 'Invalid secret key' })
  }

  const { username, password } = req.body
  if (!username || !password) {
    return res.status(401).json({ message: 'Empty username and/or password' })
  }

  // confirm if password is valid
  if (!validCredentials(username, password)) {
    return res.status(401).json({ message: 'Invalid username and password' })
  }

  let data = {
    signInTime: Date.now(),
    username,
  }

  const token = jwt.sign(data, secretKey)
  res.status(200).json({ message: 'success', token: token })
}

function authToIsraelLeak(response, redirectPath, redirectFile) {
  response.redirect(
    `/login?isIsraelLeak=true&redirectPath=${redirectPath}&redirectFile=${encodeURIComponent(
      redirectFile
    )}`
  )
}

function israelLeakWrapper(
  token: string,
  leakPath: string,
  leakFile: string,
  response
) {
  // If this is a folder, redirect to the contained index.html
  const indexPath = path.join(
    __dirname,
    '..',
    leakPath,
    decodeURIComponent(leakFile),
    'index.html'
  )
  if (fs.existsSync(indexPath)) {
    response.redirect(`${leakPath}/${leakFile}/index.html`)
  }

  if (verifyToken(token)) {
    const filepath = path.join(
      __dirname,
      '..',
      leakPath,
      decodeURIComponent(leakFile)
    )
    response.sendFile(filepath)
  } else {
    authToIsraelLeak(response, leakPath, leakFile)
  }
}

function authToRequestEntityUpdate(
  response,
  entity: string,
  stylizedEntity: string
) {
  const redirectParams = `entity=${entity}&stylizedEntity=${stylizedEntity}`
  response.redirect(
    `/login?redirectPath=/requestEntityUpdate&redirectParams=${encodeURIComponent(
      redirectParams
    )}`
  )
}

function sendReactPage(page: string, response) {
  response.sendFile(path.join(__dirname, '../react', page))
}

// Our main web server application.
class App {
  public express

  constructor() {
    this.express = express()

    this.express.use(express.static('public'))

    this.express.use(express.json())
    this.express.use(express.urlencoded({ extended: true }))

    // Allow all origins.
    this.express.use(cors({ origin: true }))

    // Enable compression.
    this.express.use(compression())

    // TODO(Jack Poulson): Determine how to use gzipped webpack bundles.

    this.express.get('/login', (request, response) => {
      sendReactPage('login.html', response)
    })

    this.express.get('/requestEntityUpdate', (request, response) => {
      const params = request.query
      const token = params.token as string
      const entity = params.entity
      const stylizedEntity = params.stylizedEntity
      if (verifyToken(token)) {
        sendReactPage('requestEntityUpdate.html', response)
      } else {
        authToRequestEntityUpdate(response, entity, stylizedEntity)
      }
    })

    this.express.get('/teixeira/:leakFile', (request, response) => {
      const leakFile = request.params.leakFile
      response.sendFile(
        path.join(__dirname, '../../../data/teixeira', leakFile)
      )
    })

    this.express.get(
      '/israel-leaks/idf/:leakPart/:leakFile',
      (request, response) => {
        const token = request.query.token as string
        const leakPart = request.params.leakPart
        const leakFile = request.params.leakFile
        const leakPath = `/israel-leaks/idf/${leakPart}`
        israelLeakWrapper(token, leakPath, leakFile, response)
      }
    )
    this.express.get(
      '/israel-leaks/idf/:leakPart/:leakFolder1/:leakFile',
      (request, response) => {
        const token = request.query.token as string
        const leakPart = request.params.leakPart
        const leakFolder1 = request.params.leakFolder1
        const leakFile = request.params.leakFile
        const leakPath = `/israel-leaks/idf/${leakPart}/${leakFolder1}`
        israelLeakWrapper(token, leakPath, leakFile, response)
      }
    )
    this.express.get(
      '/israel-leaks/idf/:leakPart/:leakFolder1/:leakFolder2/:leakFile',
      (request, response) => {
        const token = request.query.token as string
        const leakPart = request.params.leakPart
        const leakFolder1 = request.params.leakFolder1
        const leakFolder2 = request.params.leakFolder2
        const leakFile = request.params.leakFile
        const leakPath = `/israel-leaks/idf/${leakPart}/${leakFolder1}/${leakFolder2}`
        israelLeakWrapper(token, leakPath, leakFile, response)
      }
    )
    this.express.get(
      '/israel-leaks/idf/:leakPart/:leakFolder1/:leakFolder2/:leakFolder3/:leakFile',
      (request, response) => {
        const token = request.query.token as string
        const leakPart = request.params.leakPart
        const leakFolder1 = request.params.leakFolder1
        const leakFolder2 = request.params.leakFolder2
        const leakFolder3 = request.params.leakFolder3
        const leakFile = request.params.leakFile
        const leakPath = `/israel-leaks/idf/${leakPart}/${leakFolder1}/${leakFolder2}/${leakFolder3}`
        israelLeakWrapper(token, leakPath, leakFile, response)
      }
    )
    this.express.get(
      '/israel-leaks/idf/:leakPart/:leakFolder1/:leakFolder2/:leakFolder3/:leakFolder4/:leakFile',
      (request, response) => {
        const token = request.query.token as string
        const leakPart = request.params.leakPart
        const leakFolder1 = request.params.leakFolder1
        const leakFolder2 = request.params.leakFolder2
        const leakFolder3 = request.params.leakFolder3
        const leakFolder4 = request.params.leakFolder4
        const leakFile = request.params.leakFile
        const leakPath = `/israel-leaks/idf/${leakPart}/${leakFolder1}/${leakFolder2}/${leakFolder3}/${leakFolder4}`
        israelLeakWrapper(token, leakPath, leakFile, response)
      }
    )

    this.express.get('/israel-leaks/defense/:leakFile', (request, response) => {
      const token = request.query.token as string
      const leakFile = request.params.leakFile
      const leakPath = '/israel-leaks/defense'
      israelLeakWrapper(token, leakPath, leakFile, response)
    })
    this.express.get(
      '/israel-leaks/defense/Attachment/:leakFile',
      (request, response) => {
        const token = request.query.token as string
        const leakFile = request.params.leakFile
        const leakPath = '/israel-leaks/defense/Attachment'
        israelLeakWrapper(token, leakPath, leakFile, response)
      }
    )
    this.express.get(
      '/israel-leaks/defense/Attachment/:subfolder/:leakFile',
      (request, response) => {
        const token = request.query.token as string
        const subfolder = request.params.subfolder
        const leakFile = request.params.leakFile
        const leakPath = `/israel-leaks/defense/Attachment/${subfolder}`
        israelLeakWrapper(token, leakPath, leakFile, response)
      }
    )

    this.express.get(
      '/israel-leaks/justice/:leakPart/:leakFile',
      (request, response) => {
        const token = request.query.token as string
        const leakPart = request.params.leakPart
        const leakFile = request.params.leakFile
        const leakPath = `/israel-leaks/justice/${leakPart}`
        israelLeakWrapper(token, leakPath, leakFile, response)
      }
    )
    this.express.get(
      '/israel-leaks/justice/:leakPart/Attachment/:leakFile',
      (request, response) => {
        const token = request.query.token as string
        const leakPart = request.params.leakPart
        const leakFile = request.params.leakFile
        const leakPath = `/israel-leaks/justice/${leakPart}/Attachment`
        israelLeakWrapper(token, leakPath, leakFile, response)
      }
    )
    this.express.get(
      '/israel-leaks/justice/:leakPart/Attachment/:subfolder/:leakFile',
      (request, response) => {
        const token = request.query.token as string
        const leakPart = request.params.leakPart
        const subfolder = request.params.subfolder
        const leakFile = request.params.leakFile
        const leakPath = `/israel-leaks/justice/${leakPart}/Attachment/${subfolder}`
        israelLeakWrapper(token, leakPath, leakFile, response)
      }
    )

    this.express.get('/leaks.html', (request, response) => {
      sendReactPage('leaks.html', response)
    })

    // We serve different pages based upon the request parameters to help
    // minimize the javascript bundle sizes for any particular page.
    this.express.get('/', (request, response) => {
      const params = request.query

      // John Doherty appears to have been a misspelling of John Dougherty.
      if (params.entity == 'john doherty (cia)') {
        params.entity = 'john dougherty (cia)'
      }

      // NOTE: The 'bookURL' parameter is deprecated as of 2024-12-31.
      const bookParam = params.bookURL ? params.bookURL : params.book
      const bookRedirects = kEntityState.bookRedirects
      if (bookParam && bookRedirects.has(bookParam)) {
        response.redirect(`/?book=${bookRedirects.get(bookParam)}`)
        return
      } else if (params.bookURL) {
        response.redirect(`/?book=${params.bookURL}`)
        return
      }

      const useBookApp = bookParam
      const useCableApp = params.cable_reference
      const useFullApp = params.text || params.tagText || params.entity
      if (useBookApp) {
        sendReactPage('book.html', response)
      } else if (useCableApp) {
        sendReactPage('cable.html', response)
      } else if (useFullApp) {
        sendReactPage('main.html', response)
      } else {
        sendReactPage('homepage.html', response)
      }
    })

    this.express.post('/api/auth', handleAuthPost)
    this.express.post('/api/checkToken', handleCheckTokenPost)
  }

  async start(port: number, server: string) {
    if (util.isDevelopmentBuild() && !process.env.IS_DOCKER) {
      const baseConfig = require('../ormconfig.json')
      const finalConfig = {
        ...baseConfig,
        password: process.env.INF_EXP_POSTGRES_PASSWORD,
        port: process.env.INF_EXP_POSTGRES_PORT,
      }
      dataSource = new typeorm.DataSource(finalConfig)
      await dataSource.connect()
    } else {
      const baseConfig = require('../ormconfig.json')
      dataSource = new typeorm.DataSource(baseConfig)
      await dataSource.connect()
    }

    this.mountRoutes()

    this.express.listen(port, server, function (err) {
      if (err) {
        console.log(err)
        return
      }

      console.log(
        `server ${server} is listening on ${port}` +
          ` with explorer route ${kExplorerRoute}`
      )
      // Alert pm2 that this instance is ready to handle requests.
      process.send?.('ready')
    })
  }

  private mountRoutes() {
    function feedFromPath(path: string[]) {
      return util.jsonGetFromPath(kEntityState.feeds, path)
    }

    const router = express.Router()
    const entities = kEntityState.entities
    const children = kEntityState.children
    const parents = kEntityState.parents
    const usFederalOffices = kEntityState.usFederalOffices

    router.get('/data/cpv.json', function (request, response) {
      response.sendFile(path.join(__dirname, '../data', 'cpv.json'))
    })

    // The old leadership page is still a top Google result.
    router.get(
      '/leadership.html',
      // We redirect to the new interface.
      async function (request, response) {
        response.redirect('/?about=board')
      }
    )

    // We no longer host this file.
    router.get(
      '/docs/BlackmanPoliceReport.pdf',
      async function (request, response) {
        response.redirect(
          'https://jackpoulson.substack.com/p/the-covert-gig-work-surveillance'
        )
      }
    )

    // Redirection from old mechanism for searching.
    router.get('/explorer/search', async function (request, response) {
      response.redirect(`/?text=${request.query.text}`)
    })

    auProcurement.setRoutes(router, kEntityState)
    autocomplete.setRoutes(router, kEntityState)
    book.setRoutes(router, kEntityState)
    cablegate.setRoutes(router, kEntityState)
    caCanadaBuys.setRoutes(router, kEntityState)
    calaccessLobbyDisc.setRoutes(router, kEntityState)
    calaccessReceipts.setRoutes(router, kEntityState)
    caLobbyingComm.setRoutes(router, kEntityState)
    caLobbyingLobbyist.setRoutes(router, kEntityState)
    caLobbyingRegistration.setRoutes(router, kEntityState)
    caProactive.setRoutes(router, kEntityState)
    caPWSGC.setRoutes(router, kEntityState)
    euLobbying.setRoutes(router, kEntityState)
    euProcurement.setRoutes(router, kEntityState)
    fara.setRoutes(router, kEntityState)
    ilLeakDefense.setRoutes(router, kEntityState)
    ilLeakIDF.setRoutes(router, kEntityState)
    ilLeakJustice.setRoutes(router, kEntityState)
    ilProcurement.setRoutes(router, kEntityState)
    nlrb.setRoutes(router, kEntityState)
    nzProcurement.setRoutes(router, kEntityState)
    sec.setRoutes(router, kEntityState)
    uaTender.setRoutes(router, kEntityState)
    ukProcurement.setRoutes(router, kEntityState)
    usAZProcurement.setRoutes(router, kEntityState)
    usCAProcurement.setRoutes(router, kEntityState)
    usCentralProcurement.setRoutes(
      router,
      kEntityState,
      kFPDSProductOrServiceCodes
    )
    usDODAnnouncements.setRoutes(router)
    usCentralFECIndividual.setRoutes(router, kEntityState)
    usCentralGrants.setRoutes(router, kEntityState)
    usCentralOpportunities.setRoutes(router, kEntityState)
    usCentralOPRContributions.setRoutes(router, kEntityState)
    usCentralOPRIssues.setRoutes(router, kEntityState)
    usCentralSubawards.setRoutes(router, kEntityState)
    usCentralSubgrants.setRoutes(router, kEntityState)
    usFLProcurement.setRoutes(router, kEntityState)
    usNYProcurement.setRoutes(router, kEntityState)
    usTeixeira.setRoutes(router, kEntityState)
    usTXDPSProcurement.setRoutes(router, kEntityState)
    vendor.setRoutes(router, kEntityState)
    webtext.setRoutes(router, kEntityState)

    router.get('/api/lastUpdate', async function (request, response) {
      const tableName = request.query.tableName

      let params = []
      const queryStr = `SELECT last_update FROM table_updates WHERE table_name = $${
        params.length + 1
      } LIMIT 1`
      params.push(tableName)

      const query = dataSource.manager.query(queryStr, params)
      let lastUpdate = { tableUpdate: undefined }
      try {
        const filings = await query
        if (filings.length) {
          lastUpdate = filings[0]
        }
      } catch (err) {
        console.log(err)
      }

      util.setJSONResponse(response, lastUpdate)
    })

    // Health check endpoint.
    router.get('/api/healthz', function (request, response) {
      response.sendStatus(200)
    })

    // The primary prefix to run the server on.
    this.express.use(kExplorerRoute, router)

    // The deprecated prefixes to run the explorer on.
    this.express.use('/awards', router)
    this.express.use('/lobbying', router)
  }
}

export let app = new App()
