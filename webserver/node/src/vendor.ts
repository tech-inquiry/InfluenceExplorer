import { dataSource, verifyToken, decodeToken } from './App'
import * as tags from './tags'
import * as util from './util'

import { Vendor } from './entity/Vendor'
import { VendorNeighbors } from './entity/VendorNeighbors'
import { VendorSuggestion } from './entity/VendorSuggestion'

const kTableName = 'vendor'
const kTagDataset = 'vendor'

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

async function handleAddEntitySuggestion(request, response) {
  const { token, entity, data } = request.body
  if (!verifyToken(token)) {
    console.log(
      `Attempted update to ${entity} of ${data} with invalid token ${token}`
    )
    return response.status(401).json({ message: 'Invalid token' })
  }
  const decodedToken = decodeToken(token)
  const username = decodedToken.username
  const updateTime = new Date().toISOString()

  try {
    const item = {
      username: username,
      date: updateTime,
      vendor: entity,
      data: data,
    }

    const repo = dataSource.getRepository(VendorSuggestion)
    const suggestion = repo.create(item)
    await repo.save(suggestion)
    return response.status(200).json({ message: 'success', item: item })
  } catch (err) {
    console.log(err)
    return response.status(401).json({ message: 'error' })
  }
}

async function haveFromSearch(query: string) {
  let haveResult = false
  try {
    let params = []
    const queryStr = `SELECT 1 FROM vendor WHERE to_tsvector('simple', vendor || ' ' || dba) @@ websearch_to_tsquery('simple', $${
      params.length + 1
    }) LIMIT 10`
    params.push(query)
    const results = await dataSource.manager.query(queryStr, params)
    haveResult = results.length > 0
  } catch (err) {
    console.log(err)
  }
  return haveResult
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getFromSearch(query: string, maxSearchResults = 100) {
  let results = { filings: [], hitSearchCap: false }
  try {
    let params = []
    const queryStr = `SELECT * FROM vendor WHERE to_tsvector('simple', vendor || ' ' || dba) @@ websearch_to_tsquery('simple', $${
      params.length + 1
    }) ORDER BY vendor ASC LIMIT $${params.length + 2}`
    params.push(query)
    params.push(maxSearchResults)
    const filings = await dataSource.manager.query(queryStr, params)
    results = {
      filings: filings,
      hitSearchCap: filings.length == maxSearchResults,
    }
  } catch (err) {
    console.log(err)
  }
  return results
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  let results = await tags.getUniqueDataFromSearch(
    webSearch,
    token,
    maxSearchResults,
    kTagDataset
  )
  for (let filing of results.filings) {
    filing['vendor'] = filing['unique_data']['entity']
    delete filing['unique_data']
  }
  return results
}

async function getEntitySuggestions(entity: string) {
  let result = []
  try {
    const query = dataSource
      .getRepository(VendorSuggestion)
      .createQueryBuilder()
      .where('vendor = :entity', { entity: entity })
    result = await query.getMany()
  } catch (err) {
    console.log(err)
  }
  return result
}

async function handleEntitySuggestions(request, response) {
  const entity = request.query.entity
  const result = await getEntitySuggestions(entity)
  util.setJSONResponse(response, result)
}

// Retrieve the vendors with similar contracting patterns.
async function getNeighbors(entity: string, entityNormalization) {
  const neighborsResult = await dataSource
    .getRepository(VendorNeighbors)
    .findOne({ where: { vendor: entity } })
  let neighborsString: string = '[]'
  if (neighborsResult != undefined) {
    neighborsString = neighborsResult.neighbors
  }
  const neighborsOrig: string[] = JSON.parse(neighborsString)

  let neighbors: string[] = []
  for (const neighbor of neighborsOrig) {
    if (entityNormalization.has(neighbor)) {
      neighbor = entityNormalization.get(neighbor)
    }
    neighbors.push(neighbor)
  }

  return neighbors
}

async function neighborsAPI(request, response, entityNormalization) {
  // Normalize the name.
  let name: string = util.canonicalText(request.query.entity)
  const key = JSON.stringify({ name: name })
  if (entityNormalization.has(key)) {
    name = entityNormalization.get(key)
  }
  console.log('Neighbors API w/ vendor: ' + name)

  // Attempt to retrieve neighbors.
  const result = await getNeighbors(name, entityNormalization)
  response.setHeader('Content-Type', 'application/json')
  response.end(JSON.stringify(result, null, 1))
}

async function profileAPI(
  request,
  response,
  entities,
  children,
  parents,
  entityNormalization,
  books,
  nonBookCitations
) {
  // TODO(Jack Poulson): Avoid redundantly defining this list.
  const nonBookCategories = [
    'article',
    'image',
    'journal',
    'report',
    'video',
    'webpage',
  ]

  function replaceCitation(citation: string): string {
    if (citation.startsWith('book:')) {
      const tag = citation.substring(5)
      if (books.has(tag)) {
        let item = books.get(tag)
        item.set('type', 'book')
        return util.mapToObject(item)
      }
      return citation
    }

    for (const category of nonBookCategories) {
      const prefix = category + ':'
      if (citation.startsWith(prefix)) {
        const tag = citation.substring(prefix.length)
        if (nonBookCitations.get(category).has(tag)) {
          let item = nonBookCitations.get(category).get(tag)
          item.set('type', category)
          return util.mapToObject(item)
        }
        return citation
      }
    }
    return citation
  }

  const params = request.query

  const entity = util.canonicalText(params.entity)
  console.log(`Querying entity profile for ${entity}`)
  let result = {}
  if (entities.has(entity)) {
    result = util.mapToObject(entities.get(entity))
  }

  const disableChildren = util.readBoolean(params.disableChildren)
  if (!disableChildren) {
    if (children.has(entity)) {
      result['children'] = util.mapToObject(children.get(entity))
    }
  }

  const disableDescendants = util.readBoolean(params.disableDescendants)
  if (!disableChildren && !disableDescendants) {
    result['descendants'] = util.descendants(entity, children)
  }

  // The parents are filled in by default.
  const disableParents = util.readBoolean(params.disableParents)
  const disableAncestors = util.readBoolean(params.disableAncestors)
  if (!disableParents && !disableAncestors) {
    result['ancestors'] = util.ancestors(entity, parents)
  }

  const disableNeighbors = util.readBoolean(params.disableNeighbors)
  if (!disableNeighbors) {
    result['neighbors'] = await getNeighbors(entity, entityNormalization)
  }

  // Add annotations for the primary entities.
  let entitiesToAnnotateSet = new Set<string>()
  entitiesToAnnotateSet.add(entity)
  const entityLists = [
    'neighbors',
    'parents',
    'ancestors',
    'children',
    'descendants',
  ]
  for (const listName of entityLists) {
    if (result.hasOwnProperty(listName)) {
      for (const entityName of result[listName]) {
        entitiesToAnnotateSet.add(entityName)
      }
    }
  }
  if (result.hasOwnProperty('associations')) {
    // TODO(Jack Poulson): Determine how to incorporate book tags rather than
    // just raw URLs.
    for (let association of result['associations']) {
      if (association.hasOwnProperty('citation')) {
        association['citation'] = replaceCitation(association['citation'])
      } else if (association.hasOwnProperty('citations')) {
        association['citations'] = association['citations'].map(function (
          citationItem
        ) {
          if (Array.isArray(citationItem)) {
            return citationItem.map(replaceCitation)
          } else {
            return replaceCitation(citationItem)
          }
        })
      }
    }
    for (const association of result['associations']) {
      if (association.hasOwnProperty('from')) {
        const from = association['from']
        if (from.hasOwnProperty('name')) {
          entitiesToAnnotateSet.add(util.canonicalText(from['name']))
        }
      }
    }
  }
  result['entityAnnotations'] = {}
  for (const entityName of entitiesToAnnotateSet) {
    let annotation = {
      stylizedText: util.getFullyStylizedName(entityName, entities),
      logo: util.logoURLFromAncestors(entityName, entities),
      logoSmall: util.logoURLFromAncestors(entityName, entities, true),
      url: util.primaryURL(entityName, entities),
    }
    result['entityAnnotations'][entityName] = annotation
  }

  result['suggestions'] = await getEntitySuggestions(entity)

  util.setJSONResponse(response, result)
}

async function haveAPI(request, response) {
  const params = request.query

  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let haveResult = false
  if (text) {
    console.log(`Boolean entities query w/ text: ${text}`)
    haveResult = await haveFromSearch(text)
  } else if (tagText) {
    console.log(`Boolean entities query w/ tag: ${tagText}`)
    haveResult = await haveFromTagSearch(tagText, token)
  }

  util.setJSONResponse(response, haveResult)
}

async function getAPI(request, response, entities) {
  const params = request.query

  const maxSearchResultsDefault = 100
  const maxSearchResultsCap = 1000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let results = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`Entities query w/ text: ${text}`)
    results = await getFromSearch(text, maxSearchResults)
  } else if (tagText) {
    console.log(`Entities query w/ tag: ${tagText}`)
    results = await getFromTagSearch(tagText, token, maxSearchResults)
  }

  for (let filing of results.filings) {
    // TODO(Jack Poulson): Eliminate redundancy of origText and text.
    filing['annotation'] = {
      origText: filing.vendor,
      text: filing.vendor,
      stylizedText: util.getFullyStylizedName(filing.vendor, entities),
      url: util.encodeEntity(filing.vendor),
      logo: util.logoURLFromAncestors(filing.vendor, entities),
    }
  }

  util.setJSONResponse(response, results)
}

export function setRoutes(router, entityState) {
  const parentRoute = '/api'

  const feed = entityState.feeds.get('us').get('central').get('procurement')
  const entities = entityState.entities
  const children = entityState.children
  const parents = entityState.parents

  // Returns whether there are entities for a web search.
  router.get(`${parentRoute}/haveEntities`, async function (request, response) {
    await haveAPI(request, response)
  })

  // Returns JSON for entities for a web search.
  router.get(`${parentRoute}/entities`, async function (request, response) {
    await getAPI(request, response, entities)
  })

  router.get(
    `${parentRoute}/entityProfile`,
    async function (request, response) {
      await profileAPI(
        request,
        response,
        entities,
        children,
        parents,
        feed.normalization,
        entityState.books,
        entityState.nonBookCitations
      )
    }
  )

  router.get(`${parentRoute}/entitySuggestion`, handleEntitySuggestions)
  router.post(`${parentRoute}/addEntitySuggestion`, handleAddEntitySuggestion)

  // Returns JSON for neighbors for an entity.
  router.get(`${parentRoute}/neighbors`, async function (request, response) {
    await neighborsAPI(request, response, feed.normalization)
  })

  // We redirect to the new interface.
  router.get('/explorer/vendor/:vendor', async function (request, response) {
    response.redirect(`/?entity=${request.params.vendor}`)
  })

  router.post(`${parentRoute}/entity/getTags`, handleGetTags)
  router.post(`${parentRoute}/entity/addTag`, handleAddTag)
  router.post(`${parentRoute}/entity/deleteTag`, handleDeleteTag)
}
