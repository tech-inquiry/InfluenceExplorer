import { dataSource } from '../App'
import { FeedInfo } from '../EntityState'
import { UKProcurement } from '../entity/uk/procurement'
import * as tags from '../tags'
import * as util from '../util'

const kTable = UKProcurement
const kTableName = 'uk_contract_filing'

const kTagDataset = 'uk_procurement'

const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(uri)',
  'lower(publisher::text)',
  'lower(tender::text)',
  'lower(buyer::text)',
  'lower(award::text)',
])

function getAgencyNames(
  name: string,
  entities: Map<string, Map<string, any>>
): string[] {
  let names = []
  if (!entities.has(name)) {
    return names
  }
  const profile = entities.get(name)
  const alternates = util.jsonGetFromPath(profile, [
    'feeds',
    'uk',
    'procurement',
    'agencyNames',
  ])
  if (alternates != undefined) {
    names = alternates
  }
  const includes = util.jsonGetFromPath(profile, [
    'feeds',
    'uk',
    'procurement',
    'agencyIncludes',
  ])
  if (includes != undefined) {
    names = names.concat(includes)
  }
  return names
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`published_date >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`published_date <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('published_date >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('published_date <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

// Set up the list of UK contract supplier names for the vendor. If
// subsidiaries were requested to be included, the same process is repeated for
// each descendant.
function expandNames(
  entityList: string[],
  entities: Map<string, Map<string, any>>,
  entityAlternates: Map<string, string[]>
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'uk',
    'procurement',
  ])
}

// Formats a GB-COH Company House ID to 8 digits, prepending with zeros.
function prependZerosToGBCOH(id) {
  return ('00000000' + id).slice(-8)
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateFiling(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  agencyNormalization: Map<string, string>
): void {
  let vendors = {}
  if ('parties' in filing && filing['parties'] != undefined) {
    for (const party of filing['parties']) {
      if (!('name' in party)) {
        continue
      }
      let annotation = {}

      const vendor = party['name']
      const canonicalVendor = util.canonicalText(vendor)
      if (vendors.hasOwnProperty(canonicalVendor)) {
        continue
      }
      const normalizedVendor = util.normalizeEntity(vendor, entityNormalization)

      annotation['origText'] = vendor
      annotation['text'] = normalizedVendor
      annotation['stylizedText'] = util.getFullyStylizedName(
        normalizedVendor,
        entities
      )
      annotation['url'] = util.encodeEntity(normalizedVendor)
      annotation['logo'] = util.logoURLFromAncestors(normalizedVendor, entities)

      if ('identifier' in party) {
        const identifier = party['identifier']
        const scheme = identifier['scheme']
        const id = identifier['id']
        if (scheme == 'GB-COH') {
          const prefixedID = prependZerosToGBCOH(id)
          annotation['companyHouseURL'] =
            'https://find-and-update.company-information.service.gov.uk/' +
            'company/' +
            prefixedID
        }
      }
      vendors[canonicalVendor] = annotation
    }
  }
  if ('award' in filing && filing['award']) {
    const award = filing['award']
    if ('suppliers' in award) {
      for (const supplier of award['suppliers']) {
        if (!('name' in supplier)) {
          continue
        }
        let annotation = {}

        const vendor = supplier['name']
        const canonicalVendor = util.canonicalText(vendor)
        if (vendors.hasOwnProperty(canonicalVendor)) {
          continue
        }
        const normalizedVendor = util.normalizeEntity(
          vendor,
          entityNormalization
        )
        annotation['origText'] = vendor
        annotation['text'] = normalizedVendor
        annotation['stylizedText'] = util.getFullyStylizedName(
          normalizedVendor,
          entities
        )
        annotation['url'] = util.encodeEntity(normalizedVendor)
        annotation['logo'] = util.logoURLFromAncestors(
          normalizedVendor,
          entities
        )

        if ('identifier' in supplier) {
          const identifier = supplier['identifier']
          const scheme = identifier['scheme']
          const id = identifier['id']
          if (scheme == 'GB-COH') {
            const prefixedID = prependZerosToGBCOH(id)
            annotation['companyHouseURL'] =
              'https://find-and-update.company-information.service.gov.uk/' +
              'company/' +
              prefixedID
          }
        }
        vendors[canonicalVendor] = annotation
      }
    }
  }

  filing['annotation'] = { parties: vendors }

  if (filing['buyer'] && filing['buyer']['name']) {
    const buyer = filing['buyer']['name']
    const normalizedBuyer = util.normalizeEntity(buyer, agencyNormalization)

    filing['annotation']['buyer'] = {
      origText: buyer,
      text: normalizedBuyer,
      stylizedText: util.getFullyStylizedName(normalizedBuyer, entities),
      url: util.encodeEntity(normalizedBuyer),
      logo: util.logoURLFromAncestors(normalizedBuyer, entities),
    }
  }
}

function annotate(
  result,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  agencyNormalization: Map<string, string>
): void {
  for (const filing of result.filings) {
    annotateFiling(filing, entities, entityNormalization, agencyNormalization)
  }
}

async function haveForVendors(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  maxTargetsPerQuery = 20
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(names, dateRange, entities, [
      'feeds',
      'uk',
      'procurement',
    ])
  ) {
    return true
  }

  let jsonTargets = []
  for (const name of names) {
    jsonTargets.push(JSON.stringify([{ name: name }]))
  }

  let manager = dataSource.manager

  for (
    let sliceBeg = 0;
    sliceBeg < jsonTargets.length;
    sliceBeg += maxTargetsPerQuery
  ) {
    const sliceEnd = Math.min(sliceBeg + maxTargetsPerQuery, jsonTargets.length)
    const subTargets = jsonTargets.slice(sliceBeg, sliceEnd)

    let params = []
    const anyMatch = `ANY(cast($${params.length + 1} as jsonb[]))`
    params.push(subTargets)

    const searchConstraint = `LOWER(award->>'suppliers')::jsonb @> ${anyMatch}`
    const dateConstraint = buildDateConstraint(dateRange, params)
    const constraintsStr = [searchConstraint, dateConstraint]
      .filter((e) => e)
      .join(' AND ')

    // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
    // selection oddities.
    const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 10000`

    const haveResult = await util.nonemptyQuery(manager, queryStr, params)
    if (haveResult) {
      return true
    }
  }
  return false
}

async function haveForAgencies(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (!dateRange.hasStart() && !dateRange.hasEnd() && names.length) {
    return true
  }

  let params = []

  const searchConstraint = `LOWER(buyer->>'name') = ANY($${params.length + 1})`
  params.push(names)

  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForVendor(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entities, entityAlternates)
  return await haveForVendors(expandedNames, dateRange, entities)
}

async function haveForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  const names = getAgencyNames(name, entities)
  return await haveForAgencies(names, dateRange, entities)
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const searchConstraint =
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(webSearch: string, token: string) {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getForVendors(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let anyMatches = []
  for (let name of names) {
    const json = JSON.stringify([{ name: name }])
    const singleQuoteEscapedJSON = json.replace(/'/g, "''")
    anyMatches.push(`'${singleQuoteEscapedJSON}'::jsonb`)
  }
  const anyMatch = `ANY(ARRAY[${anyMatches.join(', ')}])`

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(`LOWER(award->>'suppliers')::jsonb @> ${anyMatch}`)
  incorporateDateConstraint(dateRange, query)
  query = query
    .orderBy('published_date', 'DESC', 'NULLS LAST')
    .limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForAgencies(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where("lower(buyer->>'name') = ANY(ARRAY[:...names])", { names: names })
  incorporateDateConstraint(dateRange, query)
  query = query
    .orderBy('published_date', 'DESC', 'NULLS LAST')
    .limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForVendor(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entities, entityAlternates)
  return await getForVendors(
    expandedNames,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function getForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = getAgencyNames(name, entities)
  return await getForAgencies(names, dateRange, maxSearchResults, searchOffset)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query
    .orderBy('published_date', 'DESC', 'NULLS LAST')
    .limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['uri']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query
    .orderBy('published_date', 'DESC', 'NULLS LAST')
    .limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Boolean UK contracts w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have UK contracts w/ tag-text: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean UK contracts w/ entity: ${name}`)
    result = await haveForVendor(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`Boolean UK contracts w/ agency: ${name}`)
    result = await haveForAgency(name, dateRange, entities)
  } else {
    console.log('Boolean UK contracts over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const maxSearchResultsDefault = 2000
  const maxSearchResultsCap = 10000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`UK contracts w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`UK contracts w/ tag text: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`UK contracts w/ entity: ${name}`)
    result = await getForVendor(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names,
      maxSearchResults,
      searchOffset
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`UK contracts w/ agency: ${name}`)
    result = await getForAgency(
      name,
      dateRange,
      entities,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('UK contracts over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(
      result,
      entities,
      feedInfo.normalization,
      feedInfo.agencyNormalization
    )
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/uk/procurement'

  const feed = entityState.feeds.get('uk').get('procurement')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether there are any filings for a web search or entity.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, feed)
  })

  // Returns JSON for filings for a web search or an entity.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(request, response, entities, children, feed)
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
