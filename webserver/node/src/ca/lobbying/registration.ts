import { dataSource } from '../../App'
import { CALobbyingRegistration } from '../../entity/ca/lobbying/registration'
import * as tags from '../../tags'
import * as util from '../../util'
import * as caLobby from './util'

const kTable = CALobbyingRegistration
const kTableName = 'ca_lobbying_registration'

const kTagDataset = 'ca_lobbying_registration'

const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(registration_id)',
  'lower(registration::TEXT)',
  'lower(firm::TEXT)',
  'lower(registrant::TEXT)',
  'lower(client::TEXT)',
  'lower(principal_representative::TEXT)',
  'lower(beneficiaries::TEXT)',
  'lower(comm_techniques::TEXT)',
  'lower(comm_institutions::TEXT)',
  'lower(funder_institutions::TEXT)',
  'lower(activity::TEXT)',
])

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`registration->>'endDate' >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`registration->>'startDate' <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere("registration->>'endDate' >= :startDate", {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere("registration->>'startDate' <= :endDate", {
      endDate: dateRange.end.toISOString(),
    })
  }
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateFiling(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>
) {
  let annotation = {}

  // Normalize the entity names.
  let entityAnnotations = {}
  for (const entity of filing.entities) {
    const name = util.normalizeEntity(entity, entityNormalization)
    entityAnnotations[entity] = {
      origText: entity,
      text: name,
      stylizedText: util.getFullyStylizedName(name, entities),
      logo: util.logoURLFromAncestors(name, entities),
    }
  }
  annotation['entities'] = entityAnnotations

  filing['annotation'] = annotation
}

function annotate(
  result,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>
) {
  for (let filing of result.filings) {
    annotateFiling(filing, entities, entityNormalization)
  }
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const searchConstraint =
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function haveForNames(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(
      names,
      dateRange,
      entities,
      ['feeds', 'ca', 'lobbying'],
      'haveRegistration'
    )
  ) {
    return true
  }

  let params = []

  const nameTargets = caLobby.nameTargetsFromNames(names)
  const searchConstraint = `entities @> ANY(cast($${
    params.length + 1
  } as jsonb[]))`
  params.push(nameTargets)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = caLobby.expandNames(names, entityAlternates, entities)
  return await haveForNames(expandedNames, dateRange, entities)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query
    .orderBy(
      "coalesce(registration->>'postedDate', registration->>'startDate')",
      'DESC'
    )
    .limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['registration_id']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function getForNames(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let params = []

  const nameTargets = caLobby.nameTargetsFromNames(names)
  const searchConstraint = `entities @> ANY(cast($${
    params.length + 1
  } as jsonb[]))`
  params.push(nameTargets)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  let queryStr = `
    SELECT * FROM ${kTableName} WHERE ${constraintsStr}
    ORDER BY registration->>'endDate' DESC LIMIT $${params.length + 1}`
  params.push(maxSearchResults)
  if (searchOffset > 0) {
    queryStr += ` OFFSET $${params.length + 1}`
    params.push(searchOffset)
  }

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = caLobby.expandNames(names, entityAlternates, entities)
  return await getForNames(
    expandedNames,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query
    .orderBy(
      "coalesce(registration->>'postedDate', registration->>'startDate')",
      'DESC'
    )
    .limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Boolean CA lobbying registration w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Boolean CA lobbying registration w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean CA lobbying registration w/ entity: ${name}`)
    result = await haveForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      entityAlternates
    )
  } else {
    console.log(`Boolean CA lobbying registration date range`)
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityNormalization: Map<string, string>,
  entityAlternates: Map<string, string[]>
) {
  const params = request.query

  const maxSearchResultsDefault = 1000
  const maxSearchResultsCap = 5000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const dateRange = util.getDateRange(params)
  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`CA lobbying registration w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`CA lobbying registration w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`CA lobbying registration w/ entity: ${name}`)
    result = await getForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      entityAlternates,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('CA lobbying registration date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, entityNormalization)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/ca/lobbying/registration'

  const feed = entityState.feeds.get('ca').get('lobbying')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether there are lobbying registrations.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, feed.names)
  })

  // Returns JSON for lobbying registrations.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(
      request,
      response,
      entities,
      children,
      feed.normalization,
      feed.names
    )
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
