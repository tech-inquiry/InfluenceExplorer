import * as util from '../../util'

export function expandNames(
  entityList: string[],
  entityAlternates: Map<string, string[]>,
  entities: Map<string, Map<string, any>>
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'ca',
    'lobbying',
  ])
}

export function nameTargetsFromNames(names: string[]): string[] {
  if (!names) {
    return []
  }
  return names.map((name) => `["${name.replace(/"/g, '\\"')}"]`)
}
