import { dataSource } from '../../App'
import { CAProcurementPWSGC } from '../../entity/ca/procurement/pwsgc'
import * as tags from '../../tags'
import * as util from '../../util'

const kTable = CAProcurementPWSGC
const kTableName = 'ca_pwsgc_contract_filing'

const kTagDataset = 'ca_pwsgc_contract_filing'

const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(contract_number)',
  'lower(amendment_number)',
  'lower(gsin_description)',
  'lower(limited_tender_reason_description)',
  'lower(solicitation_procedure_description)',
  'lower(trade_agreement_description)',
  'lower(supplier_standardized_name)',
  'lower(supplier_operating_name)',
  'lower(supplier_legal_name)',
  'lower(supplier_address_city)',
  'lower(supplier_address_prov_state)',
  'lower(supplier_address_postal_code)',
  'lower(supplier_address_country)',
  'lower(organization_employee_count)',
  'lower(end_user_entity)',
  'lower(contracting_entity_office_name)',
  'lower(contracting_address_street_1)',
  'lower(contracting_address_street_2)',
  'lower(contracting_address_city)',
  'lower(contracting_address_prov_state)',
  'lower(contracting_address_postal_code)',
  'lower(contracting_address_country)',
  'lower(country_percentage::text)',
])

function getAgencyNames(
  name: string,
  entities: Map<string, Map<string, any>>
): string[] {
  let names = []
  if (!entities.has(name)) {
    return names
  }
  const profile = entities.get(name)
  const alternates = util.jsonGetFromPath(profile, [
    'feeds',
    'ca',
    'procurement',
    'pwsgc',
    'agencyNames',
  ])
  if (alternates != undefined) {
    names = alternates
  }
  const includes = util.jsonGetFromPath(profile, [
    'feeds',
    'ca',
    'procurement',
    'pwsgc',
    'agencyIncludes',
  ])
  if (includes != undefined) {
    names = names.concat(includes)
  }
  return names
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`award_date >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`award_date <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('award_date >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('award_date <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

// Set up the list of names for the vendor. If
// subsidiaries were requested to be included, the same process is repeated for
// each descendant.
function expandNames(
  entityList: string[],
  entityAlternates: Map<string, string[]>,
  entities: Map<string, Map<string, any>>
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'ca',
    'procurement',
    'pwsgc',
  ])
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateFiling(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  agencyNormalization: Map<string, string>
): void {
  const vendor = filing['supplier_legal_name']
  const normalizedVendor = util.normalizeEntity(vendor, entityNormalization)

  const endUser = filing['end_user_entity']
  const normalizedEndUser = util.normalizeEntity(endUser, agencyNormalization)

  filing['annotation'] = {
    vendor: {
      origText: vendor,
      text: normalizedVendor,
      stylizedText: util.getFullyStylizedName(normalizedVendor, entities),
      url: util.encodeEntity(normalizedVendor),
      logo: util.logoURLFromAncestors(normalizedVendor, entities),
    },
    agency: {
      origText: endUser,
      text: normalizedEndUser,
      stylizedText: util.getFullyStylizedName(normalizedEndUser, entities),
      url: util.encodeEntity(normalizedEndUser),
      logo: util.logoURLFromAncestors(normalizedEndUser, entities),
    },
  }
}

function annotate(
  result,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>,
  agencyNormalization: Map<string, string>
): void {
  for (const filing of result.filings) {
    annotateFiling(filing, entities, entityNormalization, agencyNormalization)
  }
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const searchConstraint =
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForVendors(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(names, dateRange, entities, [
      'feeds',
      'ca',
      'procurement',
      'pwsgc',
    ])
  ) {
    return true
  }

  let params = []

  const searchConstraint = `lower(supplier_legal_name) = ANY($${
    params.length + 1
  })`
  params.push(names)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForAgencies(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (!dateRange.hasStart() && !dateRange.hasEnd() && names.length) {
    return true
  }

  let params = []

  const searchConstraint = `lower(end_user_entity) = ANY($${params.length + 1})`
  params.push(names)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForVendor(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await haveForVendors(expandedNames, dateRange, entities)
}

async function haveForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  const names = getAgencyNames(name, entities)
  return await haveForAgencies(names, dateRange, entities)
}

async function haveFromTagSearch(webSearch: string, token: string) {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getForVendors(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where('lower(supplier_legal_name) = ANY(ARRAY[:...names])', {
      names: names,
    })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('award_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForAgencies(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where('lower(end_user_entity) = ANY(ARRAY[:...names])', {
      names: names,
    })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('award_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForVendor(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await getForVendors(
    expandedNames,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function getForAgency(
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = getAgencyNames(name, entities)
  return await getForAgencies(names, dateRange, maxSearchResults, searchOffset)
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('award_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['contract_number', 'amendment_number']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('award_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Boolean CA PWSGC Contract w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have CA PWSGC w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean CA PWSGC Contract w/ entity: ${name}`)
    result = await haveForVendor(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      entityAlternates
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`Boolean CA PWSGC contract w/ agency: ${name}`)
    result = await haveForAgency(name, dateRange, entities)
  } else {
    console.log('Boolean CA PWSGC Contract over date range')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityNormalization: Map<string, string>,
  entityAlternates: Map<string, string[]>,
  agencyNormalization: Map<string, string>
) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const dateRange = util.getDateRange(params)

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`CA PWSGC Contract w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`CA PWSGC w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`CA PWSGC Contract w/ entity: ${name}`)
    result = await getForVendor(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      entityAlternates,
      maxSearchResults,
      searchOffset
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`CA PWSGC contract w/ agency: ${name}`)
    result = await getForAgency(
      name,
      dateRange,
      entities,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('CA PWSGC Contract over date range')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, entityNormalization, agencyNormalization)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/ca/procurement/pwsgc'

  const feed = entityState.feeds.get('ca').get('procurement').get('pwsgc')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether there are PWSGC contract filings for a web search or an
  // entity.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, feed.names)
  })

  // Returns JSON for PWSGC contract filings for a web search or an entity.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(
      request,
      response,
      entities,
      children,
      feed.normalization,
      feed.names,
      feed.agencyNormalization
    )
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
