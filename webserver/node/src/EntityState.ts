import * as util from './util'
import * as euProcurement from './eu/procurement'

const nonBookCategories = [
  'article',
  'image',
  'journal',
  'report',
  'video',
  'webpage',
]

export class FeedInfo {
  alternates: Map<string, string[]>
  agencyNames: Map<string, string[]>
  normalization: Map<string, string>
  agencyNormalization: Map<string, string>
  block: Set<string>

  constructor() {
    this.normalization = new Map<string, string>()
    this.names = new Map<string, string[]>()

    this.agencyNormalization = new Map<string, string>()
    this.agencyNames = new Map<string, string[]>()

    this.block = new Set<string>()
  }
}

export class NZProcurementFeedInfo extends FeedInfo {
  noticeIDAlternates: Map<string, string[]>
  noticeIDNormalization: Map<string, string>

  constructor() {
    super()
    this.noticeIDAlternates = new Map<string, string[]>()
    this.noticeIDNormalization = new Map<string, string>()
  }
}

export class USCentralOPRFeedInfo extends FeedInfo {
  lobbyistIDAlternates: Map<string, number[]>
  lobbyistIDNormalization: Map<number, string>
  agencyNormalization: Map<string, string>

  constructor() {
    super()
    this.lobbyistIDAlternates = new Map<string, number[]>()
    this.lobbyistIDNormalization = new Map<number, string>()
    this.agencyNormalization = new Map<string, string>()
  }
}

export class USCentralOpportunitiesFeedInfo {
  alternates: Map<string, string[]>
  normalization: Map<string, string>
  agencyNormalization: Map<string, Map<string, string>>
  block: Set<string>

  constructor(subawardDepts: Map<string, Map<string, string>>) {
    this.normalization = new Map<string, string>()
    this.agencyNormalization = new Map<string, string>()
    this.block = new Set<string>()
  }
}

export class USCentralProcurementFeedInfo {
  alternates: Map<string, string[]>
  normalization: Map<string, string>
  contractorNormalization: Map<string, string>
  agencyNormalization: Map<string, Map<string, string>>
  block: Set<string>

  subawardDepartments: Map<string, Map<string, string>>

  constructor(subawardDepts: Map<string, Map<string, string>>) {
    this.normalization = new Map<string, string>()
    this.contractorNormalization = new Map<string, string>()

    this.agencyNormalization = new Map<string, Map<string, string>>()
    this.agencyNormalization.set('dept', new Map<string, string>())
    this.agencyNormalization.set('agency', new Map<string, string>())
    this.agencyNormalization.set('office', new Map<string, string>())

    this.block = new Set<string>()

    this.subawardDepartments = subawardDepts

    // Form the inverse of the map from subaward department codes to
    // prime department codes.
    this.subawardDepartments.set('inverseCode', new Map<string, string>())
    for (const [subCode, primeCode] of subawardDepts.get('code').entries()) {
      this.subawardDepartments.get('inverseCode').set(primeCode, subCode)
    }
  }
}

export class CablegateFeedInfo {
  embassyAlternates: Map<string, string[]>
  embassyNormalization: Map<string, string>

  // A map from (lowercase) reference IDs to tagged entities.
  // We are setting the value type to 'any[]' instead of 'string[]' to support
  // both string and {name: 'NAME', note: 'NOTE'} objects.
  entityRefs: Map<string, any[]>

  // A map from entity names to the list of (typically upper-case) preamble
  // tags that they are associated with, such as 'RUEAIIA/CIA' for the CIA.
  preambleTags: Map<string, string[]>

  // A map from sources to date ranges assigned to different entities.
  authorSpans: Map<string, any[]>

  constructor() {
    this.embassyNormalization = new Map<string, string>()
    this.embassyAlternates = new Map<string, string[]>()
    this.entityRefs = new Map<string, any[]>()
    this.preambleTags = new Map<string, string[]>()
    this.authorSpans = new Map<string, any[]>()
  }
}

export class IsraelLeakMinistryOfDefenseFeedInfo {
  // A map from email IDs to tagged entities.
  // We are setting the value type to 'any[]' instead of 'string[]' to support
  // both string and {name: 'NAME', note: 'NOTE'} objects.
  entityRefs: Map<string, any[]>

  constructor() {
    this.entityRefs = new Map<string, any[]>()
  }
}

export class IsraelLeakMinistryOfJusticeFeedInfo {
  // For each of the two parts, a map from email IDs to tagged entities.
  // We are setting the value type to 'any[]' instead of 'string[]' to support
  // both string and {name: 'NAME', note: 'NOTE'} objects.
  entityRefs: any[]

  constructor() {
    this.entityRefs = [new Map<string, any[]>(), new Map<string, any[]>()]
  }
}

function associationFromName(association) {
  return association.get('from').get('name').toLowerCase()
}

function associationToText(association) {
  return association.has('text')
    ? association.get('text')
    : association.get('to')
    ? association.get('to').get('text')
    : 'N/A'
}

function associationFromText(association) {
  return association.has('text')
    ? association.get('text')
    : association.get('from')
    ? association.get('from').get('text')
    : 'N/A'
}

function coerceWeight(weight) {
  return weight == undefined ? 1 : weight
}

function associationToWeight(association) {
  const weight = util.jsonGetFromPath(association, ['to', 'weight'])
  return coerceWeight(weight)
}

function associationFromWeight(association) {
  const weight = util.jsonGetFromPath(association, ['from', 'weight'])
  return coerceWeight(weight)
}

function copyTimespan(source, dest) {
  if (!source.has('timespan')) {
    return
  }
  const timespan = source.get('timespan')

  dest.set('timespan', new Map())
  let destTimespan = dest.get('timespan')

  if (timespan.has('start')) {
    destTimespan.set('start', timespan.get('start'))
  }
  if (timespan.has('end')) {
    destTimespan.set('end', timespan.get('end'))
  }
  if (timespan.has('circa')) {
    destTimespan.set('circa', timespan.get('circa'))
  }
}

function copyCitations(source, dest) {
  if (source.has('citation')) {
    dest.set('citation', source.get('citation'))
  }
  if (source.has('citations')) {
    dest.set('citations', source.get('citations'))
  }
  dest.set('accessed', source.get('accessed'))
}

function swappedAssociation(association, entity: string) {
  let swappedAssn = new Map<string, any>()

  // Build the 'timespan' field.
  copyTimespan(association, swappedAssn)

  // Build the 'to' field.
  swappedAssn.set('to', new Map())
  let swappedTo = swappedAssn.get('to')
  swappedTo.set('text', associationFromText(association))
  if (association.has('from')) {
    const from = association.get('from')
    if (from.has('weight')) {
      swappedTo.set('weight', coerceWeight(from.get('weight')))
    }
    for (const key of ['category', 'numCategoryLifts']) {
      if (from.has(key)) {
        swappedTo.set(key, from.get(key))
      }
    }
  }

  // Build the 'from' field.
  swappedAssn.set('from', new Map())
  let swappedFrom = swappedAssn.get('from')
  swappedFrom.set('name', entity)
  swappedFrom.set('text', associationToText(association))
  if (association.has('to')) {
    const to = association.get('to')
    if (to.has('weight')) {
      swappedFrom.set('weight', coerceWeight(to.get('weight')))
    }
    for (const key of ['category', 'numCategoryLifts']) {
      if (to.has(key)) {
        swappedFrom.set(key, to.get(key))
      }
    }
  }

  // Build the URL fields.
  copyCitations(association, swappedAssn)

  return swappedAssn
}

function formDescendants(
  entity: string,
  children: Map<string, string[]>
): Set<string> {
  let descendants = new Set<string>()
  if (children.has(entity)) {
    for (const child of children.get(entity)) {
      descendants.add(child)
      const childDescendants = formDescendants(child, children)
      for (const descendant of childDescendants) {
        descendants.add(descendant)
      }
    }
  }
  return descendants
}

function formAgencyIncludesOverDescendants(
  entity: string,
  entities: Map<string, Map<string, any>>,
  children: Map<string, string[]>
) {
  if (!entities.has(entity)) {
    return
  }

  // Create the inclusive descendants
  let descendants = formDescendants(entity, children)
  descendants.add(entity)

  // Maintain a map of stringified (department) and (department, agency) tuples
  // which don't have associated offices. Also maintain a map from stringified
  // (department, agency) pairs into its set of offices. At the end, we flatten
  // the maps.

  let agenciesWithoutOffices = new Set<string>()
  let agenciesWithOffices = new Map<string, Set<string>>()

  const innerPath = ['us', 'central', 'procurement']
  const alternatePath = ['feeds'].concat(innerPath, ['agencyNames'])
  const includePath = ['feeds'].concat(innerPath, ['agencyIncludes'])
  for (const descendant of descendants) {
    if (!entities.has(descendant)) {
      continue
    }
    const profile = entities.get(descendant)
    const alts = util.jsonGetFromPath(profile, alternatePath)
    const includes = util.jsonGetFromPath(profile, includePath)
    const valuesList = descendant == entity ? [includes] : [alts, includes]
    for (const values of valuesList) {
      if (values == undefined) {
        continue
      }
      for (const value of values) {
        if (value.has('offices') || value.has('office')) {
          const key = JSON.stringify({
            dept: value.get('dept'),
            agency: value.get('agency'),
          })
          if (!agenciesWithOffices.has(key)) {
            agenciesWithOffices.set(key, new Set<string>())
          }
          const offices = value.has('offices')
            ? value.get('offices')
            : [value.get('office')]
          for (const office of offices) {
            agenciesWithOffices.get(key).add(office)
          }
        } else if (value.has('agency')) {
          const key = JSON.stringify({
            dept: value.get('dept'),
            agency: value.get('agency'),
          })
          agenciesWithoutOffices.add(key)
        } else {
          const key = JSON.stringify({ dept: value.get('dept') })
          agenciesWithoutOffices.add(key)
        }
      }
    }
  }

  if (!agenciesWithoutOffices.size && !agenciesWithOffices.size) {
    return
  }

  let profile = entities.get(entity)
  // Ensure that ['feeds', 'us', 'central', 'procurement'] exists.
  if (!profile.has('feeds')) {
    profile.set('feeds', new Map<string, Map<string, any>>())
  }
  let feeds = profile.get('feeds')
  for (let i = 0; i < innerPath.length; i += 1) {
    if (!util.jsonPathExists(feeds, innerPath.slice(0, i + 1))) {
      util
        .jsonGetFromPath(feeds, innerPath.slice(0, i))
        .set(innerPath[i], new Map<string, Map<string, any>>())
    }
  }
  const usProc = util.jsonGetFromPath(feeds, ['us', 'central', 'procurement'])

  usProc.set('agencyIncludes', [])
  for (const agencyString of agenciesWithoutOffices) {
    const agencyObj = JSON.parse(agencyString)
    const agencyMap = util.objectToMap(agencyObj)
    usProc.get('agencyIncludes').push(agencyMap)
  }
  for (const agencyString of agenciesWithOffices.keys()) {
    if (agenciesWithoutOffices.has(agencyString)) {
      // If the entire agency is to be included, we don't need to specify the
      // individual offices.
      continue
    }
    const agencyObj = JSON.parse(agencyString)
    let agencyMap = util.objectToMap(agencyObj)
    agencyMap.set('offices', Array.from(agenciesWithOffices.get(agencyString)))
    usProc.get('agencyIncludes').push(agencyMap)
  }
}

export class EntityState {
  entities: Map<string, Map<string, any>>
  children: Map<string, string[]>
  parents: Map<string, string[]>

  books: Map<string, any>
  nonBookCitations: Map<string, Map<string, any>>

  // TODO(Jack Poulson): Eliminate this after finishing conversion of book
  // tags and allowing a sufficient deprecation period for the old URLs.
  bookRedirects: Map<string, string>

  usFederalOffices: Map<string, Map<string, any>>
  feeds: Map<string, any>

  constructor(
    entitiesNew: Map<string, Map<string, any>>,
    citations: Map<string, Map<string, any>>,
    usFederalOfficesNew: Map<string, Map<string, any>>,
    usFederalSubawardDepartments: Map<string, Map<string, string>>
  ) {
    const timer = util.getTimer('EntityState constructor')
    timer.start()

    this.entities = entitiesNew
    this.children = new Map<string, string[]>()
    this.parents = new Map<string, string[]>()

    this.books = new Map<string, any>()
    this.bookRedirects = new Map<string, string>()

    this.nonBookCitations = new Map<string, Map<string, any>>()
    for (const category of nonBookCategories) {
      this.nonBookCitations.set(category, new Map<string, any>())
    }

    this.usFederalOffices = usFederalOfficesNew

    this.initializeFeeds(usFederalSubawardDepartments)

    // Expand the templates first.
    for (let [entity, profile] of this.entities.entries()) {
      this.expandEntityAssociationTemplates(entity, profile)
    }

    // Invert the parent links and form vendor alternates.
    for (let [entity, profile] of this.entities.entries()) {
      if (!profile.has('parents')) {
        profile.set('parents', [])
      }

      // Invert the parent links.
      this.parents.set(entity, profile.get('parents'))
      for (const entityParent of profile.get('parents')) {
        util.appendAtKey(this.children, entityParent, entity)
      }
    }

    // Form vendor alternates.
    for (let [entity, profile] of this.entities.entries()) {
      this.initializeEntityNormalizations(entity, profile)
      this.initializeEntityAssociations(entity, profile)
    }

    this.initializeAlternates()
    this.initializeAncestorAssociations()
    this.initializeCitations(citations)

    timer.stop()
  }

  initializeFeeds(
    usFederalSubawardDepartments: Map<string, Map<string, string>>
  ) {
    this.feeds = new Map<string, any>()

    this.feeds.set('au', new Map<string, any>())
    this.feeds.get('au').set('procurement', new FeedInfo())

    this.feeds.set('ca', new Map<string, any>())
    this.feeds.get('ca').set('lobbying', new FeedInfo())
    this.feeds.get('ca').set('procurement', new Map<string, any>())
    this.feeds.get('ca').get('procurement').set('canadaBuys', new FeedInfo())
    this.feeds.get('ca').get('procurement').set('proactive', new FeedInfo())
    this.feeds.get('ca').get('procurement').set('pwsgc', new FeedInfo())

    this.feeds.set('il', new Map<string, any>())
    this.feeds.get('il').set('leak', new Map<string, any>())
    this.feeds
      .get('il')
      .get('leak')
      .set('defense', new IsraelLeakMinistryOfDefenseFeedInfo())
    this.feeds
      .get('il')
      .get('leak')
      .set('justice', new IsraelLeakMinistryOfJusticeFeedInfo())
    this.feeds.get('il').set('procurement', new Map<string, any>())
    this.feeds.get('il').get('procurement').set('exempt', new FeedInfo())
    this.feeds.get('il').get('procurement').set('tender', new FeedInfo())

    this.feeds.set('nz', new Map<string, any>())
    this.feeds.get('nz').set('procurement', new NZProcurementFeedInfo())

    this.feeds.set('ua', new Map<string, any>())
    this.feeds.get('ua').set('procurement', new Map<string, any>())
    this.feeds.get('ua').get('procurement').set('tender', new FeedInfo())

    this.feeds.set('uk', new Map<string, any>())
    this.feeds.get('uk').set('procurement', new FeedInfo())

    this.feeds.set('eu', new Map<string, any>())
    this.feeds.get('eu').set('central', new Map<string, any>())
    this.feeds.get('eu').get('central').set('lobbying', new FeedInfo())
    for (const countryCode of euProcurement.CountryCodes) {
      this.feeds.get('eu').set(countryCode, new Map<string, any>())
      this.feeds.get('eu').get(countryCode).set('procurement', new FeedInfo())
    }

    this.feeds.set('us', new Map<string, any>())
    this.feeds.get('us').set('az', new Map<string, any>())
    this.feeds.get('us').get('az').set('procurement', new FeedInfo())
    this.feeds.get('us').set('ca', new Map<string, any>())
    this.feeds.get('us').get('ca').set('lobbying', new Map<string, any>())
    this.feeds
      .get('us')
      .get('ca')
      .get('lobbying')
      .set('activity', new FeedInfo())
    this.feeds
      .get('us')
      .get('ca')
      .get('lobbying')
      .set('contrib', new FeedInfo())
    this.feeds.get('us').get('ca').set('procurement', new FeedInfo())
    this.feeds.get('us').set('central', new Map<string, any>())
    this.feeds.get('us').get('central').set('laborRelations', new FeedInfo())
    this.feeds.get('us').get('central').set('lobbying', new Map<string, any>())
    this.feeds
      .get('us')
      .get('central')
      .get('lobbying')
      .set('fec', new FeedInfo())
    this.feeds
      .get('us')
      .get('central')
      .get('lobbying')
      .set('opr', new USCentralOPRFeedInfo())
    this.feeds
      .get('us')
      .get('central')
      .set('cablegate', new CablegateFeedInfo())
    this.feeds.get('us').get('central').set('grants', new FeedInfo())
    this.feeds
      .get('us')
      .get('central')
      .set('opportunities', new USCentralOpportunitiesFeedInfo())
    this.feeds
      .get('us')
      .get('central')
      .set(
        'procurement',
        new USCentralProcurementFeedInfo(usFederalSubawardDepartments)
      )
    this.feeds.get('us').set('fl', new Map<string, any>())
    this.feeds.get('us').get('fl').set('procurement', new FeedInfo())
    this.feeds.get('us').set('ny', new Map<string, any>())
    this.feeds.get('us').get('ny').set('procurement', new FeedInfo())
    this.feeds.get('us').set('tx', new Map<string, any>())
    this.feeds.get('us').get('tx').set('dps', new Map<string, any>())
    this.feeds.get('us').get('tx').get('dps').set('procurement', new FeedInfo())
  }

  initializeEntityNormalizations(entity: string, profile: any) {
    const that = this
    const formVendorNormalization = function (innerPath: string[]) {
      const blockNamePath = ['feeds'].concat(innerPath, ['blockName'])
      const alternatePath = ['feeds'].concat(innerPath, ['names'])
      let feed = util.jsonGetFromPath(that.feeds, innerPath)
      if (util.jsonGetFromPath(profile, blockNamePath)) {
        feed.block.add(entity)
      } else {
        feed.normalization.set(entity, entity)
      }
      const alts = util.jsonGetFromPath(profile, alternatePath)
      if (alts != undefined) {
        for (const alternate of alts) {
          feed.normalization.set(alternate, entity)
        }
      }
    }

    const formVendorObjectNormalization = function (innerPath: string[]) {
      const blockNamePath = ['feeds'].concat(innerPath, ['blockName'])
      const alternatePath = ['feeds'].concat(innerPath, ['names'])
      let feed = util.jsonGetFromPath(that.feeds, innerPath)
      if (util.jsonGetFromPath(profile, blockNamePath)) {
        feed.block.add(entity)
      } else {
        feed.normalization.set(JSON.stringify({ name: entity }), entity)
      }
      const alts = util.jsonGetFromPath(profile, alternatePath)
      if (alts != undefined) {
        for (const alternate of alts) {
          const json = util.isString(alternate)
            ? { name: alternate }
            : util.mapToObject(alternate)
          const key = JSON.stringify(json)
          feed.normalization.set(key, entity)
        }
      }
    }

    const formUSCentralVendorNormalization = function (innerPath: string[]) {
      const blockNamePath = ['feeds'].concat(innerPath, ['blockName'])
      const alternatePath = ['feeds'].concat(innerPath, ['names'])
      const ueiPath = ['feeds'].concat(innerPath, ['ueis'])
      let feed = util.jsonGetFromPath(that.feeds, innerPath)
      if (util.jsonGetFromPath(profile, blockNamePath)) {
        feed.block.add(entity)
      } else {
        feed.normalization.set(JSON.stringify({ name: entity }), entity)
      }

      const nameAlts = util.jsonGetFromPath(profile, alternatePath)
      if (nameAlts != undefined) {
        for (const alternate of nameAlts) {
          const json = util.isString(alternate)
            ? { name: alternate }
            : util.mapToObject(alternate)
          const key = JSON.stringify(json)
          feed.normalization.set(key, entity)
        }
      }

      const ueiAlts = util.jsonGetFromPath(profile, ueiPath)
      if (ueiAlts != undefined) {
        for (const alternate of ueiAlts) {
          const key = JSON.stringify({ uei: alternate })
          feed.normalization.set(key, entity)
        }
      }
    }

    const formAgencyNormalization = function (innerPath: string[]) {
      const alternatePath = ['feeds'].concat(innerPath, ['agencyNames'])
      let feed = util.jsonGetFromPath(that.feeds, innerPath)
      const alts = util.jsonGetFromPath(profile, alternatePath)
      if (alts != undefined) {
        for (const alternate of alts) {
          feed.agencyNormalization.set(alternate, entity)
        }
      }
    }

    const formNormalization = function (innerPath: string[]) {
      formVendorNormalization(innerPath)
      formAgencyNormalization(innerPath)
    }

    formNormalization(['au', 'procurement'])
    formNormalization(['ca', 'lobbying'])
    formNormalization(['ca', 'procurement', 'canadaBuys'])
    formNormalization(['ca', 'procurement', 'proactive'])
    formNormalization(['ca', 'procurement', 'pwsgc'])
    formNormalization(['il', 'procurement', 'exempt'])
    formNormalization(['il', 'procurement', 'tender'])
    formNormalization(['ua', 'procurement', 'tender'])
    formNormalization(['uk', 'procurement'])
    formNormalization(['us', 'az', 'procurement'])
    formNormalization(['us', 'ca', 'lobbying', 'activity'])
    formNormalization(['us', 'ca', 'procurement'])
    formNormalization(['us', 'central', 'grants'])
    formNormalization(['us', 'central', 'laborRelations'])
    formNormalization(['us', 'fl', 'procurement'])
    formNormalization(['us', 'ny', 'procurement'])
    formNormalization(['us', 'tx', 'dps', 'procurement'])
    for (const countryCode of euProcurement.CountryCodes) {
      formNormalization(['eu', countryCode, 'procurement'])
    }

    {
      const innerPath = ['nz', 'procurement']
      let feed = that.feeds.get('nz').get('procurement')

      const alternatePath = ['feeds'].concat(innerPath, ['names'])
      const alts = util.jsonGetFromPath(profile, alternatePath)
      if (alts != undefined) {
        // Invert the New Zealand alternates terms.
        const normalization = feed.normalization
        for (const alternate of alts) {
          if (!normalization.has(alternate)) {
            normalization.set(alternate, [])
          }
          normalization.get(alternate).push(entity)
        }
      }

      const noticeIDPath = ['feeds'].concat(innerPath, ['noticeIDs'])
      const noticeIDs = util.jsonGetFromPath(profile, noticeIDPath)
      if (noticeIDs != undefined) {
        // Invert the New Zealand notice IDs.
        const normalization = feed.noticeIDNormalization
        for (const noticeID of noticeIDs) {
          if (!(noticeID in normalization)) {
            normalization.set(noticeID, [])
          }
          normalization.get(noticeID).push(entity)
        }
      }
    }

    {
      const innerPath = ['au', 'procurement']
      formVendorObjectNormalization(innerPath)
      formAgencyNormalization(innerPath)
    }

    const nameEmployerPaths = [
      ['eu', 'central', 'lobbying'],
      ['us', 'ca', 'lobbying', 'contrib'],
      ['us', 'central', 'lobbying', 'fec'],
    ]
    for (const innerPath of nameEmployerPaths) {
      formVendorObjectNormalization(innerPath)
    }

    {
      const innerPath = ['us', 'central', 'cablegate']
      let feed = util.jsonGetFromPath(that.feeds, innerPath)

      const alternatePath = ['feeds'].concat(innerPath, ['embassyAlternates'])
      const alts = util.jsonGetFromPath(profile, alternatePath)
      if (alts != undefined) {
        for (const alternate of alts) {
          feed.embassyNormalization.set(alternate, entity)
        }
      }

      const referencesPath = ['feeds'].concat(innerPath, ['references'])
      let references = util.jsonGetFromPath(profile, referencesPath)
      if (references != undefined) {
        for (const referenceObj of references) {
          const reference = util.isString(referenceObj)
            ? referenceObj
            : referenceObj.get('id')
          const note = util.isString(referenceObj)
            ? undefined
            : referenceObj.get('note')
          const attributed = util.isString(referenceObj)
            ? undefined
            : referenceObj.get('attributed')

          if (!feed.entityRefs.has(reference)) {
            feed.entityRefs.set(reference, [])
          }
          let ref = undefined
          if (attributed && note) {
            ref = { name: entity, attributed: attributed, note: note }
          } else if (attributed) {
            ref = { name: entity, attributed: attributed }
          } else if (note) {
            ref = { name: entity, note: note }
          } else {
            ref = entity
          }
          feed.entityRefs.get(reference).push(ref)
        }
      }

      const preambleTagsPath = ['feeds'].concat(innerPath, ['tags'])
      let tags = util.jsonGetFromPath(profile, preambleTagsPath)
      if (tags != undefined) {
        feed.preambleTags.set(entity, tags)
      }

      const authorSpansPath = ['feeds'].concat(innerPath, ['authorSpans'])
      let authorSpans = util.jsonGetFromPath(profile, authorSpansPath)
      if (authorSpans != undefined) {
        for (const authorSpan of authorSpans) {
          const source = authorSpan.get('source')
          if (!feed.authorSpans.has(source)) {
            feed.authorSpans.set(source, [])
          }
          feed.authorSpans.get(source).push({
            entity: entity,
            noteAttributed: authorSpan.get('noteAttributed'),
            noteUnattributed: authorSpan.get('noteUnattributed'),
            signatures: authorSpan.get('signatures'),
            startDate: authorSpan.get('startDate'),
            endDate: authorSpan.get('endDate'),
          })
        }
      }
    }

    {
      const innerPath = ['il', 'leak', 'defense']
      let feed = util.jsonGetFromPath(that.feeds, innerPath)

      const referencesPath = ['feeds'].concat(innerPath, ['references'])
      let references = util.jsonGetFromPath(profile, referencesPath)
      if (references != undefined && false) {
        for (const referenceObj of references) {
          const reference = util.isString(referenceObj)
            ? referenceObj
            : referenceObj.get('id')
          const note = util.isString(referenceObj)
            ? undefined
            : referenceObj.get('note')
          const ref = note ? { name: entity, note: note } : entity

          if (!feed.entityRefs.has(reference)) {
            feed.entityRefs.set(reference, [])
          }
          feed.entityRefs.get(reference).push(ref)
        }
      }
    }

    {
      const innerPath = ['il', 'leak', 'justice']
      let feed = util.jsonGetFromPath(that.feeds, innerPath)

      const referencesPath = ['feeds'].concat(innerPath, ['references'])
      let references = util.jsonGetFromPath(profile, referencesPath)
      if (references != undefined && false) {
        if (references.length < 2) {
          console.log(`Invalid number of reference parts for "${entity}"`)
        }
        for (const part of [0, 1]) {
          for (const referenceObj of references[part]) {
            const reference = util.isString(referenceObj)
              ? referenceObj
              : referenceObj.get('id')
            const note = util.isString(referenceObj)
              ? undefined
              : referenceObj.get('note')
            const ref = note ? { name: entity, note: note } : entity

            if (!feed.entityRefs[part].has(reference)) {
              feed.entityRefs[part].set(reference, [])
            }
            feed.entityRefs[part].get(reference).push(ref)
          }
        }
      }
    }

    {
      const innerPath = ['us', 'central', 'opportunities']
      let feed = util.jsonGetFromPath(that.feeds, innerPath)

      // Form the US central opportunities vendor normalization.
      // (Agency normalization is mirrored from US federal procurement.)
      formVendorNormalization(innerPath)
    }

    {
      const innerPath = ['us', 'central', 'procurement']
      let feed = util.jsonGetFromPath(that.feeds, innerPath)

      // Form the US central procurement vendor normalization.
      formUSCentralVendorNormalization(innerPath)

      // Form the US central procurement contractor normalization.
      {
        const alternatePath = ['feeds'].concat(innerPath, ['contractorNames'])
        feed.contractorNormalization.set(entity, entity)
        const alts = util.jsonGetFromPath(profile, alternatePath)
        if (alts != undefined) {
          for (const alternate of alts) {
            feed.contractorNormalization.set(alternate, entity)
          }
        }
      }

      // Form the US central procurement agency normalization.
      {
        const alternatePath = ['feeds'].concat(innerPath, ['agencyNames'])
        const alternates = util.jsonGetFromPath(profile, alternatePath)
        if (alternates != undefined) {
          for (const alternate of alternates) {
            const dept = alternate.get('dept')
            if (alternate.has('offices') || alternate.has('office')) {
              const agency = alternate.get('agency')
              const offices = alternate.has('offices')
                ? alternate.get('offices')
                : [alternate.get('office')]
              for (const office of offices) {
                if (!this.usFederalOffices.get('office_codes').has(dept)) {
                  console.log(`No ${dept} in usFederalOffices[office_codes]`)
                  continue
                }
                if (
                  !this.usFederalOffices
                    .get('office_codes')
                    .get(dept)
                    .has(agency)
                ) {
                  console.log(
                    `No ${agency} in usFederalOffices[office_codes][${dept}]`
                  )
                  continue
                }
                if (
                  !this.usFederalOffices
                    .get('office_codes')
                    .get(dept)
                    .get(agency)
                    .has(office)
                ) {
                  console.log(
                    `No ${office} in usFederalOffices[office_codes][${dept}][${agency}]`
                  )
                  continue
                }
                for (const code of this.usFederalOffices
                  .get('office_codes')
                  .get(dept)
                  .get(agency)
                  .get(office)) {
                  feed.agencyNormalization.get('office').set(code, entity)
                }
              }
            } else if (alternate.has('agency')) {
              const agency = alternate.get('agency')
              if (!this.usFederalOffices.get('agency_codes').has(dept)) {
                console.log(`No ${dept} in usFederalOffices[agency_codes]`)
                continue
              }
              if (
                !this.usFederalOffices.get('agency_codes').get(dept).has(agency)
              ) {
                console.log(
                  `No ${agency} in usFederalOffices[agency_codes][${dept}]`
                )
                continue
              }
              for (const code of this.usFederalOffices
                .get('agency_codes')
                .get(dept)
                .get(agency)) {
                feed.agencyNormalization.get('agency').set(code, entity)
              }
            } else {
              if (!this.usFederalOffices.get('dept_codes').has(dept)) {
                console.log(`No ${dept} in usFederalOffices[dept_codes]`)
                continue
              }
              for (const code of this.usFederalOffices
                .get('dept_codes')
                .get(dept)) {
                feed.agencyNormalization.get('dept').set(code, entity)
              }
            }
          }
        }
      }

      const includeDescendantsPath = ['feeds'].concat(innerPath, [
        'includeAgenciesFromDescendants',
      ])
      if (util.jsonGetFromPath(profile, includeDescendantsPath)) {
        formAgencyIncludesOverDescendants(entity, that.entities, that.children)
      }
    }

    {
      const innerPath = ['us', 'central', 'lobbying', 'opr']
      const blockNamePath = ['feeds'].concat(innerPath, ['blockName'])
      const alternatePath = ['feeds'].concat(innerPath, ['names'])
      let feed = that.feeds.get('us').get('central').get('lobbying').get('opr')

      // Form the US central lobbying vendor normalization.
      {
        if (util.jsonGetFromPath(profile, blockNamePath)) {
          feed.block.add(entity)
        } else {
          feed.normalization.set(entity, entity)
        }
        const alts = util.jsonGetFromPath(profile, alternatePath)
        if (alts != undefined) {
          for (const alternate of alts) {
            if (util.isString(alternate)) {
              feed.normalization.set(alternate, entity)
            } else {
              feed.lobbyistIDNormalization.set(
                alternate.get('lobbyistID'),
                entity
              )
            }
          }
        }
      }

      // Form the US central lobbying agency normalization.
      formAgencyNormalization(innerPath)
    }
  }

  // Expand the entity association templates.
  expandEntityAssociationTemplates(entity: string, profile: any) {
    if (!profile.has('associations')) {
      return
    }
    const origAssociations = Object.assign([], profile.get('associations'))

    // Expand out any text templates.
    let associations = []
    for (const association of origAssociations) {
      if (association.has('textTemplate')) {
        const textTemplate = association.get('textTemplate')
        const toName = entity
        const toWeight = associationToWeight(association)
        const fromWeight = associationFromWeight(association)
        for (const nameInfo of association.get('from').get('names')) {
          const fromName = util.isString(nameInfo)
            ? nameInfo
            : nameInfo.get('name')
          const text = util.isString(nameInfo)
            ? textTemplate
            : textTemplate.replace('${VALUE}', nameInfo.get('text'))

          // Build and store the instantiated association.
          let assn = new Map<string, any>()

          // Build the 'timespan' field.
          copyTimespan(association, assn)

          // Build the 'to' field.
          assn.set('to', new Map())
          let newTo = assn.get('to')
          newTo.set('text', text)
          newTo.set('name', entity)
          if (association.has('to')) {
            const to = association.get('to')
            if (to.has('weight')) {
              newTo.set('weight', coerceWeight(to.get('weight')))
            }
            for (const key of ['category', 'numCategoryLifts']) {
              if (to.has(key)) {
                newTo.set(key, to.get(key))
              }
            }
          }

          // Build the 'from' field.
          assn.set('from', new Map())
          let newFrom = assn.get('from')
          newFrom.set('name', fromName)
          newFrom.set('text', text)
          if (association.has('from')) {
            const from = association.get('from')
            if (from.has('weight')) {
              newFrom.set('weight', coerceWeight(from.get('weight')))
            }
            for (const key of ['category', 'numCategoryLifts']) {
              if (from.has(key)) {
                newFrom.set(key, from.get(key))
              }
            }
          }

          // Build the URL fields.
          copyCitations(association, assn)

          associations.push(assn)
        }
      } else {
        associations.push(association)
      }
    }
    profile.set('associations', associations)
  }

  // Perform the initial pass over the associations and form the
  // complementary associations when they do not already exist.
  initializeEntityAssociations(entity: string, profile: any) {
    if (!profile.has('associations')) {
      return
    }
    const associations = Object.assign([], profile.get('associations'))
    for (const association of associations) {
      const toName = entity
      const fromName = associationFromName(association)
      const toText = associationToText(association)
      const fromText = associationFromText(association)
      const toWeight = associationToWeight(association)
      const fromWeight = associationFromWeight(association)

      if (this.entities.has(fromName)) {
        let fromProfile = this.entities.get(fromName)
        if (!fromProfile.has('associations')) {
          fromProfile.set('associations', [])
        }

        const isMatch = function (assn) {
          const toTextMatch = associationToText(assn) == fromText
          const fromNameMatch = associationFromName(assn) == toName
          const fromTextMatch = associationFromText(assn) == toText
          return toTextMatch && fromNameMatch && fromTextMatch
        }

        let fromAssociations = fromProfile.get('associations')
        const foundIndex = fromAssociations.findIndex(isMatch)
        const haveMatch = foundIndex > -1
        if (!haveMatch) {
          const swappedAssn = swappedAssociation(association, entity)
          fromAssociations.push(swappedAssn)
        }
      }
    }
  }

  initializeAlternates() {
    const that = this
    const formAlternates = function (innerPath) {
      let feed = util.jsonGetFromPath(that.feeds, innerPath)
      for (const [key, value] of feed.normalization.entries()) {
        util.appendAtKey(feed.names, value, key)
      }
      for (const [key, value] of feed.agencyNormalization.entries()) {
        util.appendAtKey(feed.agencyNames, value, key)
      }
    }

    formAlternates(['au', 'procurement'])
    formAlternates(['ca', 'lobbying'])
    formAlternates(['ca', 'procurement', 'canadaBuys'])
    formAlternates(['ca', 'procurement', 'proactive'])
    formAlternates(['ca', 'procurement', 'pwsgc'])
    formAlternates(['eu', 'central', 'lobbying'])
    formAlternates(['il', 'procurement', 'exempt'])
    formAlternates(['il', 'procurement', 'tender'])
    formAlternates(['nz', 'procurement'])
    formAlternates(['ua', 'procurement', 'tender'])
    formAlternates(['uk', 'procurement'])
    formAlternates(['us', 'az', 'procurement'])
    formAlternates(['us', 'central', 'grants'])
    formAlternates(['us', 'central', 'laborRelations'])
    formAlternates(['us', 'central', 'lobbying', 'fec'])
    formAlternates(['us', 'central', 'lobbying', 'opr'])
    formAlternates(['us', 'ca', 'lobbying', 'activity'])
    formAlternates(['us', 'ca', 'lobbying', 'contrib'])
    formAlternates(['us', 'ca', 'procurement'])
    formAlternates(['us', 'fl', 'procurement'])
    formAlternates(['us', 'ny', 'procurement'])
    formAlternates(['us', 'tx', 'dps', 'procurement'])
    for (const countryCode of euProcurement.CountryCodes) {
      formAlternates(['eu', countryCode, 'procurement'])
    }

    // Invert the New Zealand notice ID normalization map into lists.
    {
      let feed = that.feeds.get('nz').get('procurement')
      for (const [key, value] of feed.noticeIDNormalization.entries()) {
        util.appendAtKey(feed.noticeIDAlternates, value, key)
      }
    }

    // Invert the OPR lobbyist ID normalization map into lists.
    {
      let feed = that.feeds.get('us').get('central').get('lobbying').get('opr')
      for (const [key, value] of feed.lobbyistIDNormalization.entries()) {
        util.appendAtKey(feed.lobbyistIDAlternates, value, key)
      }
    }

    // Invert the cablegate embassy/source normalizers.
    {
      const innerPath = ['us', 'central', 'cablegate']
      let feed = util.jsonGetFromPath(that.feeds, innerPath)
      for (const [key, value] of feed.embassyNormalization.entries()) {
        util.appendAtKey(feed.embassyAlternates, value, key)
      }
    }
  }

  initializeAncestorAssociations() {
    const that = this
    function append_ancestors_and_weight_exponents(
      entity: string,
      ancestorList,
      exponent,
      damp
    ) {
      if (!that.parents.has(entity)) {
        return
      }
      const numParents = that.parents.get(entity).length

      let manualMultiplier = undefined
      if (that.entities.has(entity)) {
        const profile = that.entities.get(entity)
        if (profile.has('liftExponent')) {
          manualMultiplier = profile.get('liftExponent')
        }
      }

      for (const parent of that.parents.get(entity)) {
        const numChildren = that.children.get(parent).length
        const multiplier =
          manualMultiplier != undefined
            ? manualMultiplier
            : (numChildren * numParents + 1) ** damp
        const newExponent = exponent * multiplier
        let entry = new Map()
        entry.set('name', parent)
        entry.set('exponent', newExponent)
        ancestorList.push(entry)
        append_ancestors_and_weight_exponents(
          parent,
          ancestorList,
          newExponent,
          damp
        )
      }
    }

    function get_ancestors_and_weight_exponents(entity, damp = 0.25) {
      let ancestors = []
      let exponent = 1
      append_ancestors_and_weight_exponents(entity, ancestors, exponent, damp)
      return ancestors
    }

    function lift_score(score, exponent) {
      if (score > 1) {
        return score ** (1 / exponent)
      } else {
        return score
      }
    }

    // Add the ancestor associations for each entity.
    for (let [entity, profile] of that.entities.entries()) {
      if (!profile.has('associations')) {
        continue
      }
      const ancestorsAndExponents = get_ancestors_and_weight_exponents(entity)
      const associations = profile.get('associations')

      let ancestorSet = new Set<string>()
      for (const ancestorAndExponent of ancestorsAndExponents) {
        const ancestor = ancestorAndExponent.get('name')
        const exponent = ancestorAndExponent.get('exponent')
        if (!that.entities.has(ancestor)) {
          continue
        }
        if (ancestorSet.has(ancestor)) {
          continue
        }
        ancestorSet.add(ancestor)

        let ancestorProfile = that.entities.get(ancestor)
        if (!ancestorProfile.has('associations')) {
          ancestorProfile.set('associations', [])
        }
        let ancestorAssociations = ancestorProfile.get('associations')

        for (const association of associations) {
          if (
            association.has('preventLift') &&
            association.get('preventLift')
          ) {
            continue
          }

          const isMatch = function (assn) {
            return (
              associationFromName(assn) == associationFromName(association) &&
              associationToText(assn) == associationToText(association) &&
              associationFromText(assn) == associationFromText(association)
            )
          }
          const haveMatch = ancestorAssociations.findIndex(isMatch) > -1
          const fromName = associationFromName(association)
          const fromAncestors = util.ancestors(fromName, that.parents)
          const alsoFromAncestor = fromAncestors.indexOf(ancestor) > -1
          if (!haveMatch && !alsoFromAncestor) {
            let ancestorAssociation = new Map()

            copyCitations(association, ancestorAssociation)
            copyTimespan(association, ancestorAssociation)

            // Lift the 'to' field.
            ancestorAssociation.set('to', new Map())
            let ancestorTo = ancestorAssociation.get('to')
            ancestorTo.set('text', associationToText(association))
            ancestorTo.set('weight', associationToWeight(association))
            if (
              association.has('to') &&
              association.get('to').has('numCategoryLifts') &&
              association.get('to').get('numCategoryLifts') > 0
            ) {
              ancestorTo.set('category', association.get('to').get('category'))
              ancestorTo.set(
                'numCategoryLifts',
                association.get('to').get('numCategoryLifts') - 1
              )
            }

            // Lift the 'from' field.
            ancestorAssociation.set('from', new Map())
            let ancestorFrom = ancestorAssociation.get('from')
            ancestorFrom.set('name', associationFromName(association))
            ancestorFrom.set('text', associationFromText(association))
            ancestorFrom.set(
              'weight',
              lift_score(associationFromWeight(association), exponent)
            )
            if (
              association.has('from') &&
              association.get('from').has('numCategoryLifts') &&
              association.get('from').get('numCategoryLifts') > 0
            ) {
              ancestorFrom.set(
                'category',
                association.get('from').get('category')
              )
              ancestorFrom.set(
                'numCategoryLifts',
                association.get('from').get('numCategoryLifts') - 1
              )
            }

            ancestorAssociations.push(ancestorAssociation)
          }
        }
      }
    }
  }

  initializeCitations(citations) {
    function entityName(entityTag): string {
      return util.isString(entityTag) ? entityTag : entityTag.get('name')
    }

    function entityNote(allEntitiesNote: string, entityTag): string {
      return util.isString(entityTag)
        ? allEntitiesNote
        : allEntitiesNote
        ? `${entityTag.get('note')} (${allEntitiesNote})`
        : entityTag.get('note')
    }

    // Add in the book citations.
    const bookFields = [
      'authors',
      'cover',
      'coverSmall',
      'date',
      'dateLex',
      'org',
      'subtitle',
      'tag',
      'title',
      'url',
    ]
    if (citations.has('book')) {
      for (let [tag, item] of citations.get('book').entries()) {
        item.set('tag', tag)
        const url = item.get('url')
        this.bookRedirects.set(item.get('url'), tag)

        // Save the book into the main citation dictionary.
        this.books.set(tag, item)

        const allEntitiesNote = item.get('note')
        for (const entityTag of item.get('entities')) {
          const name = entityName(entityTag)
          const note = entityNote(allEntitiesNote, entityTag)
          let profile = this.entities.get(name)
          if (!profile) {
            console.log(`Did not find profile for "${name}"`)
            continue
          }
          if (!profile.has('book')) {
            profile.set('book', [])
          }
          let newItem = new Map<string, any>()
          for (const field of bookFields) {
            if (item.has(field) && item.get(field)) {
              newItem.set(field, item.get(field))
            }
          }
          if (note) {
            newItem.set('note', note)
          }
          profile.get('book').push(newItem)
        }
      }
    }

    const entityTypes = {
      investigatedEntities: 'journalism',
      miscEntities: 'misc',
    }

    const nonBookFields = [
      'accessed',
      'authors',
      'cover',
      'coverSmall',
      'date',
      'dateLex',
      'number',
      'org',
      'pages',
      'subtitle',
      'tag',
      'title',
      'url',
      'volume',
    ]
    for (const category of nonBookCategories) {
      if (citations.has(category)) {
        for (let [tag, item] of citations.get(category).entries()) {
          item.set('tag', tag)
          const url = item.get('url')

          // Save the item into the main citation dictionary.
          this.nonBookCitations.get(category).set(tag, item)

          for (const entityType in entityTypes) {
            if (!item.has(entityType)) {
              continue
            }

            const allEntitiesNote = item.get('note')

            const citationLabel = entityTypes[entityType]
            for (const entityTag of item.get(entityType)) {
              const name = entityName(entityTag)
              const note = entityNote(allEntitiesNote, entityTag)
              let profile = this.entities.get(name)
              if (!profile) {
                console.log(`Did not find profile for "${name}"`)
                continue
              }

              let newItem = new Map<string, any>()
              newItem.set('type', category)
              for (const field of nonBookFields) {
                if (item.has(field) && item.get(field)) {
                  newItem.set(field, item.get(field))
                }
              }
              if (note) {
                newItem.set('note', note)
              }
              if (!profile.has(citationLabel)) {
                profile.set(citationLabel, [])
              }
              profile.get(citationLabel).push(newItem)
            }
          }
        }
      }
    }

    // Add in the legacy citations.
    // TODO(Jack Poulson): Eliminate this after conversion of the entries.
    if (citations.has('citation')) {
      for (let [url, item] of citations.get('citation').entries()) {
        for (const entityType in entityTypes) {
          if (!item.has(entityType)) {
            continue
          }

          const allEntitiesNote = item.get('note')

          const citationLabel = entityTypes[entityType]
          for (const entityTag of item.get(entityType)) {
            const name = entityName(entityTag)
            const note = entityNote(allEntitiesNote, entityTag)

            let profile = this.entities.get(name)
            if (!profile) {
              console.log(`Did not find profile for "${name}"`)
              continue
            }

            let newItem = new Map<string, any>()
            newItem.set('url', url)
            for (const field of nonBookFields) {
              if (item.has(field) && item.get(field)) {
                newItem.set(field, item.get(field))
              }
            }
            if (note) {
              newItem.set('note', note)
            }

            if (!profile.has(citationLabel)) {
              profile.set(citationLabel, [])
            }
            profile.get(citationLabel).push(newItem)
          }
        }
      }
    }

    // Sort the citations by date.
    for (let [entity, profile] of this.entities.entries()) {
      for (let citationLabel of ['book', 'journalism', 'misc']) {
        if (profile.has(citationLabel)) {
          profile.get(citationLabel).sort(function (a, b) {
            const aHasDate = a.has('date') || a.has('dateLex')
            const bHasDate = b.has('date') || b.has('dateLex')
            if (aHasDate && bHasDate) {
              const aDate = a.has('dateLex') ? a.get('dateLex') : a.get('date')
              const bDate = b.has('dateLex') ? b.get('dateLex') : b.get('date')
              return aDate >= bDate ? -1 : 1
            } else if (aHasDate) {
              return 1
            } else if (bHasDate) {
              return -1
            } else {
              return 0
            }
          })
        }
      }
    }
  }
}
