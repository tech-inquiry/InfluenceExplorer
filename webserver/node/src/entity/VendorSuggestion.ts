import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity('vendor_suggestion')
export class VendorSuggestion {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 63 })
  username: string

  @Column('date')
  date: Date

  @Column('varchar', { length: 255 })
  vendor: string

  @Column('jsonb')
  data: Object
}
