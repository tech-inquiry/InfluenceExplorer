import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('ua_tender')
export class UATender {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  uid: string

  @Column('jsonb')
  tender: object

  @Column('jsonb')
  suppliers: object[]

  @Column('jsonb')
  buyers: object[]

  @Column('timestamp')
  date_created: Date
}
