import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity('webtext')
export class Webtext {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 1023 })
  url: string

  @Column('text')
  text: string

  @Column('jsonb')
  entities: string[]
}
