import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity('tags')
export class Tag {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 63 })
  username: string

  @Column('timestamp')
  tagged_date: Date

  @Column('varchar', { length: 63 })
  dataset: string

  @Column('jsonb')
  unique_data: Object[]

  @Column('varchar', { length: 255 })
  tag: string

  @Column('boolean')
  is_entity: boolean
}
