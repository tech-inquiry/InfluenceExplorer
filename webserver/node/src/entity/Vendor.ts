import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity('vendor')
export class Vendor {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 255, nullable: true })
  vendor: string

  @Column('varchar', { length: 255, nullable: true })
  dba: string
}
