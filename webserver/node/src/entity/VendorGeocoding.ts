import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity('vendor_geocoding')
export class VendorGeocoding {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 255, nullable: true })
  vendor: string

  @Column('varchar', { length: 255, nullable: true })
  street_address: string

  @Column('varchar', { length: 127, nullable: true })
  city: string

  @Column('varchar', { length: 63, nullable: true })
  state: string

  @Column('varchar', { length: 63, nullable: true })
  zipcode: string

  @Column('varchar', { length: 127, nullable: true })
  country: string

  @Column('varchar', { length: 63, nullable: true })
  phone_number: string

  @Column('varchar', { length: 63, nullable: true })
  fax_number: string

  @Column('varchar', { length: 63, nullable: true })
  congressional_district: string

  @Column('float', { nullable: true })
  longitude: number

  @Column('float', { nullable: true })
  latitude: number

  @Column('timestamp', { nullable: true })
  signed_date: string
}
