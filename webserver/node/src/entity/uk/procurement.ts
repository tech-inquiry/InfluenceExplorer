import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('uk_contract_filing')
export class UKProcurement {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  uri: string

  @Column('jsonb')
  publisher: object

  @Column('timestamp')
  published_date: Date

  @Column('jsonb')
  tender: object

  @Column('jsonb')
  buyer: object

  @Column('jsonb')
  award: object

  @Column('timestamp')
  deadline_date: Date

  @Column('jsonb')
  parties: object[]
}
