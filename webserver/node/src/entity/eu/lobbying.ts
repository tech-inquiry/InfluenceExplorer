import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('eu_lobbying')
export class EULobbying {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  identification_code: string

  @Column('timestamp')
  registration_date: Date

  @Column('timestamp')
  last_update_date: Date

  @Column('text')
  name: string

  @Column('text')
  acronym: string

  @Column('text')
  entity_form: string

  @Column('text')
  website_url: string

  @Column('text')
  registration_category: string

  @Column('jsonb')
  head_office: object

  @Column('jsonb')
  eu_office: Object

  @Column('text')
  goals: string

  @Column('jsonb')
  levels_of_interest: string[]

  @Column('jsonb')
  legislative_proposals: string[]

  @Column('jsonb')
  supported_forums: Object[]

  @Column('text')
  communications: string

  @Column('jsonb')
  members: Object

  @Column('jsonb')
  interests: string[]

  @Column('jsonb')
  structure: Object

  @Column('text')
  interest_represented: string

  @Column('jsonb')
  financial_data: Object

  @Column('text')
  groupings: string
}
