import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('eu_co_contract_filing')
export class EUCOProcurement {
  @PrimaryGeneratedColumn()
  id: number

  @Column('boolean', { default: true })
  is_legacy: boolean

  @Column('varchar', { length: 15 })
  doc_id: string

  @Column('jsonb')
  data: object

  @Column('jsonb')
  entities: string[]

  @Column('date')
  first_action_date: Date

  @Column('date')
  last_action_date: Date
}
