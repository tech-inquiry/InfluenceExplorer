import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity('table_updates')
export class TableUpdates {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  table_name: string

  @Column('date')
  last_update: Date
}
