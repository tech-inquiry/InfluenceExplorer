import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('ca_proactive_disclosure_filing')
export class CAProcurementProactive {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 255 })
  reference_number: string

  @Column('varchar', { length: 63 })
  procurement_id: string

  @Column('varchar', { length: 255 })
  vendor_name: string

  @Column('varchar', { length: 31 })
  vendor_postal_code: string

  @Column('varchar', { length: 255 })
  buyer_name: string

  @Column('timestamp')
  contract_date: Date

  @Column('text')
  description: string

  @Column('timestamp')
  contract_period_start: Date

  @Column('timestamp')
  delivery_date: Date

  @Column('float')
  contract_value: number

  @Column('float')
  original_value: number

  @Column('float')
  amendment_value: number

  @Column('text')
  comments: string

  @Column('text')
  additional_comments: string

  @Column('varchar', { length: 63 })
  agreement_type_code: string

  @Column('varchar', { length: 63 })
  trade_agreement: string

  @Column('varchar', { length: 31 })
  country_of_vendor: string

  @Column('varchar', { length: 31 })
  solicitation_procedure: string

  @Column('varchar', { length: 31 })
  limited_tendering_reason: string

  @Column('varchar', { length: 63 })
  trade_agreement_exceptions: string

  @Column('varchar', { length: 31 })
  intellectual_property: string

  @Column('varchar', { length: 31 })
  contracting_entity: string

  @Column('varchar', { length: 255 })
  standing_offer_number: string

  @Column('varchar', { length: 31 })
  instrument_type: string

  @Column('int')
  number_of_bids: number

  @Column('int')
  award_criteria: number

  @Column('varchar', { length: 31 })
  reporting_period: string

  @Column('varchar', { length: 31 })
  owner_org: string

  @Column('varchar', { length: 255 })
  owner_org_title: string
}
