import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('ca_pwsgc_contract_filing')
export class CAProcurementPWSGC {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 63 })
  contract_number: string

  @Column('varchar', { length: 63 })
  amendment_number: string

  @Column('timestamp')
  award_date: Date

  @Column('timestamp')
  expiry_date: Date

  @Column('float')
  contract_value: number

  @Column('varchar', { length: 255 })
  gsin_description: string

  @Column('varchar', { length: 3 })
  competitive_tender: string

  @Column('varchar', { length: 127 })
  limited_tender_reason_description: string

  @Column('varchar', { length: 3 })
  solicitation_procedure: string

  @Column('varchar', { length: 31 })
  solicitation_procedure_description: string

  @Column('text')
  trade_agreement_description: string

  @Column('varchar', { length: 255 })
  supplier_standardized_name: string

  @Column('varchar', { length: 255 })
  supplier_operating_name: string

  @Column('varchar', { length: 255 })
  supplier_legal_name: string

  @Column('varchar', { length: 63 })
  supplier_address_city: string

  @Column('varchar', { length: 31 })
  supplier_address_prov_state: string

  @Column('varchar', { length: 15 })
  supplier_address_postal_code: string

  @Column('varchar', { length: 63 })
  supplier_address_country: string

  @Column('varchar', { length: 31 })
  organization_employee_count: string

  @Column('float')
  total_contract_value: number

  @Column('int')
  number_records: number

  @Column('varchar', { length: 127 })
  end_user_entity: string

  @Column('varchar', { length: 31 })
  contracting_entity_office_name: string

  @Column('varchar', { length: 63 })
  contracting_address_street_1: string

  @Column('varchar', { length: 63 })
  contracting_address_street_2: string

  @Column('varchar', { length: 31 })
  contracting_address_city: string

  @Column('varchar', { length: 31 })
  contracting_address_prov_state: string

  @Column('varchar', { length: 7 })
  contracting_address_postal_code: string

  @Column('varchar', { length: 7 })
  contracting_address_country: string

  @Column('jsonb')
  country_percentage: object[]

  @Column('timestamp')
  date_file_published: Date
}
