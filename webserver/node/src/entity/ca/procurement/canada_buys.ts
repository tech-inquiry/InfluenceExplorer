import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('ca_canada_buys')
export class CACanadaBuys {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  title: string

  @Column('text')
  reference_number: string

  @Column('text')
  amendment_number: string

  @Column('text')
  procurement_number: string

  @Column('text')
  solicitation_number: string

  @Column('text')
  contract_number: string

  @Column('int')
  number_of_records: number

  @Column('date')
  publication_date: Date

  @Column('date')
  contract_award_date: Date

  @Column('date')
  amendment_date: Date

  @Column('date')
  contract_start_date: Date

  @Column('date')
  contract_end_date: Date

  @Column('float')
  contract_amount: number

  @Column('float')
  total_contract_value: number

  @Column('text')
  contract_currency: string

  @Column('text')
  contract_status: string

  @Column('text')
  gsin: string

  @Column('text')
  gsin_description: string

  @Column('text')
  unspsc: string

  @Column('text')
  unspsc_description: string

  @Column('text')
  procurement_category: string

  @Column('text')
  notice_type: string

  @Column('text')
  procurement_method: string

  @Column('text')
  selection_criteria: string

  @Column('text')
  limited_tendering_reason: string

  @Column('text')
  trade_agreements: string

  @Column('text')
  regions_of_delivery: string

  @Column('text')
  supplier_legal_name: string

  @Column('text')
  supplier_standardized_name: string

  @Column('text')
  supplier_operating_name: string

  @Column('text')
  supplier_employee_count: string

  @Column('text')
  supplier_address_line: string

  @Column('text')
  supplier_address_city: string

  @Column('text')
  supplier_address_province: string

  @Column('text')
  supplier_address_postal_code: string

  @Column('text')
  supplier_address_country: string

  @Column('text')
  contracting_entity_name: string

  @Column('text')
  contracting_entity_address_line: string

  @Column('text')
  contracting_entity_address_city: string

  @Column('text')
  contracting_entity_address_province: string

  @Column('text')
  contracting_entity_address_postal_code: string

  @Column('text')
  contracting_entity_address_country: string

  @Column('text')
  end_user_entities_name: string

  @Column('text')
  end_user_entities_office_name: string

  @Column('text')
  end_user_entities_address: string

  @Column('text')
  contact_info_name: string

  @Column('text')
  contact_info_email: string

  @Column('text')
  contact_info_phone: string

  @Column('text')
  contact_info_fax: string

  @Column('text')
  contact_info_address_line: string

  @Column('text')
  contact_info_city: string

  @Column('text')
  contact_info_province: string

  @Column('text')
  contact_info_postal_code: string

  @Column('text')
  contact_info_country: string

  @Column('text')
  percentage_of_goods_by_country: string

  @Column('text')
  tender_description: string
}
