import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('ca_lobbying_lobbyist')
@Index('ca_lobbying_lobbyist_unique', ['lobbyist_id'], { unique: true })
export class CALobbyingLobbyist {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 63 })
  lobbyist_id: string

  @Column('jsonb')
  clients: Object[]

  @Column('jsonb')
  public_offices: Object[]
}
