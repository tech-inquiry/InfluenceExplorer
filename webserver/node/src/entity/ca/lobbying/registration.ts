import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('ca_lobbying_registration')
@Index('ca_lobbying_registration_unique', ['registration_id'], { unique: true })
export class CALobbyingRegistration {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 63 })
  registration_id: string

  @Column('jsonb')
  registration: Object

  @Column('jsonb')
  firm: Object

  @Column('jsonb')
  registrant: Object

  @Column('jsonb')
  client: Object

  @Column('jsonb')
  principal_representative: Object

  @Column('jsonb')
  beneficiaries: Object[]

  @Column('jsonb')
  comm_techniques: Object[]

  @Column('jsonb')
  comm_institutions: string[]

  @Column('jsonb')
  funder_institutions: Object[]

  @Column('jsonb')
  activity: Object

  @Column('jsonb')
  entities: string[]
}
