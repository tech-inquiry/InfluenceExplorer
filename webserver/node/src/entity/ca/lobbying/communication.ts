import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('ca_lobbying_communication')
@Index('ca_lobbying_communication_unique', ['comm_id'], { unique: true })
export class CALobbyingCommunication {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 63 })
  comm_id: string

  @Column('varchar', { length: 63 })
  type: string

  @Column('varchar', { length: 255 })
  name: string

  @Column('varchar', { length: 63 })
  client_id: string

  @Column('date')
  date: Date

  @Column('date')
  submission_date: Date

  @Column('date')
  posted_date: Date

  @Column('varchar', { length: 63 })
  parent_id: string

  @Column('jsonb')
  registrant: Object

  @Column('jsonb')
  dpoh: Object

  @Column('jsonb')
  activity: Object

  @Column('jsonb')
  entities: string[]
}
