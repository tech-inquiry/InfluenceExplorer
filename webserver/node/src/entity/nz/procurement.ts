import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('nz_award_notice')
@Index('nz_award_notice_unique', ['notice_id'], { unique: true })
export class NZProcurement {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 255 })
  posting_agency: string

  @Column('varchar', { length: 63 })
  notice_id: string

  @Column('varchar', { length: 63 })
  notice_type: string

  @Column('varchar', { length: 63 })
  competition_type: string

  @Column('varchar', { length: 255 })
  title: string

  @Column('varchar', { length: 31 })
  reference_number: string

  @Column('timestamp')
  open_date: Date

  @Column('timestamp')
  close_date: Date

  @Column('timestamp')
  awarded_date: Date

  @Column('varchar', { length: 127 })
  department: string

  @Column('varchar', { length: 31 })
  tender_coverage: string

  @Column('varchar', { length: 31 })
  prequalification: string

  @Column('varchar', { length: 255 })
  alternate_address: string

  @Column('text')
  overview: string

  @Column('varchar', { length: 31 })
  award_type: string

  @Column('text')
  comments: string

  @Column('float')
  awarded_amount: number

  @Column('timestamp')
  report_date: Date
}
