import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('il_leak_idf')
export class ILLeakIDF {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  reference_id: string

  @Column('int')
  part: number

  @Column('text')
  text: string

  @Column('text')
  email_terms: string

  @Column('jsonb')
  emails: string[]
}
