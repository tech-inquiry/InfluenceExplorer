import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('il_leak_ministry_of_defense')
export class ILLeakMinistryOfDefense {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  email_id: string

  @Column('text')
  text: string

  @Column('text')
  attachment_text: string

  @Column('text')
  email_terms: string

  @Column('jsonb')
  emails: string[]
}
