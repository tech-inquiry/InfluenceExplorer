import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('il_procurement_exempt')
@Index('il_procurement_exempt_unique', ['bid_number'], { unique: true })
export class ILProcurementExempt {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  exempt_type: string

  @Column('text')
  title: string

  @Column('text')
  title_en: string

  @Column('text')
  publisher: string

  @Column('text')
  publisher_en: string

  @Column('text')
  exemption_reasons: string

  @Column('text')
  exemption_reasons_en: string

  @Column('text')
  bid_number: string

  @Column('text')
  bid_status: string

  @Column('text')
  bid_status_en: string

  @Column('text')
  bid_process: string

  @Column('date')
  bid_publication_date: Date

  @Column('date')
  bid_update_date: Date

  @Column('date')
  bid_last_date: Date

  @Column('text')
  bid_contact_name: string

  @Column('text')
  bid_contact_name_en: string

  @Column('text')
  bid_contact_mail: string

  @Column('jsonb')
  subjects: string[]

  @Column('jsonb')
  subjects_en: string[]

  @Column('jsonb')
  engagements: Object[]

  @Column('jsonb')
  related_documents: Object[]

  @Column('jsonb')
  entities: string[]

  @Column('jsonb')
  entities_en: string[]
}

@Entity('il_procurement_tender')
@Index('il_procurement_tender_unique', ['bid_number'], { unique: true })
export class ILProcurementTender {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  tender_type: string

  @Column('text')
  title: string

  @Column('text')
  title_en: string

  @Column('text')
  publisher: string

  @Column('text')
  publisher_en: string

  @Column('text')
  bid_number: string

  @Column('text')
  bid_status: string

  @Column('text')
  bid_status_en: string

  @Column('text')
  bid_process: string

  @Column('date')
  bid_publication_date: Date

  @Column('date')
  bid_update_date: Date

  @Column('date')
  first_submission_date: Date

  @Column('date')
  last_submission_date: Date

  @Column('text')
  bid_contact_name: string

  @Column('text')
  bid_contact_name_en: string

  @Column('jsonb')
  subjects: string[]

  @Column('jsonb')
  subjects_en: string[]

  @Column('jsonb')
  related_documents: Object[]

  @Column('jsonb')
  entities: string[]

  @Column('jsonb')
  entities_en: string[]
}

@Entity('il_procurement')
@Index('il_procurement_unique', ['publication_id'], { unique: true })
export class ILProcurement {
  @PrimaryGeneratedColumn()
  id: number

  @Column('bigint')
  publication_id: number

  @Column('varchar', { length: 255 })
  vendor_name: string

  @Column('varchar', { length: 255 })
  vendor_name_en: string

  @Column('bigint')
  vendor_number: number

  @Column('varchar', { length: 255 })
  buyer: string

  @Column('varchar', { length: 255 })
  buyer_en: string

  @Column('text')
  buyer_detailed: string

  @Column('text')
  buyer_detailed_en: string

  @Column('float')
  amount: number

  @Column('varchar', { length: 15 })
  currency: string

  @Column('timestamp')
  date_published: Date

  @Column('timestamp')
  date_updated: Date

  @Column('timestamp')
  objection_deadline_day: Date

  @Column('varchar', { length: 63 })
  objection_deadline_time: string

  @Column('timestamp')
  contract_period_begin: Date

  @Column('timestamp')
  contract_period_end: Date

  @Column('varchar', { length: 255 })
  procedure_type: string

  @Column('varchar', { length: 255 })
  procedure_type_en: string

  @Column('varchar', { length: 255 })
  procedure_name: string

  @Column('text')
  procedure_name_en: string

  @Column('varchar', { length: 255 })
  status: string

  @Column('varchar', { length: 255 })
  status_en: string

  @Column('varchar', { length: 255 })
  decision: string

  @Column('varchar', { length: 255 })
  decision_en: string

  @Column('varchar', { length: 255 })
  confirmation: string

  @Column('varchar', { length: 255 })
  confirmation_en: string

  @Column('varchar', { length: 255 })
  regulation: string

  @Column('varchar', { length: 255 })
  regulation_en: string

  @Column('varchar', { length: 63 })
  link_to_texts: string

  @Column('varchar', { length: 63 })
  link_to_texts_en: string

  @Column('text')
  topics: string

  @Column('text')
  topics_en: string
}
