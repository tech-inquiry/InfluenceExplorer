import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity('vendor_neighbors')
export class VendorNeighbors {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 255 })
  vendor: string

  @Column('text')
  neighbors: string
}
