import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('au_contract_filing')
@Index('au_contract_filing_unique', ['filing_id'], { unique: true })
export class AUProcurement {
  @PrimaryGeneratedColumn()
  id: number

  @Column('jsonb')
  publisher: object

  @Column('timestamp')
  published_date: Date

  @Column('varchar', { length: 255 })
  filing_id: string

  @Column('timestamp')
  date: Date

  @Column('varchar', { length: 63 })
  initiation_type: string

  @Column('jsonb')
  parties: object

  @Column('jsonb')
  award: object

  @Column('jsonb')
  contract: object

  @Column('varchar', { length: 63 })
  tag: string

  @Column('jsonb')
  tender: object
}
