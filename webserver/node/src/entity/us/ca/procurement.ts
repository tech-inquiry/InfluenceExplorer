import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_ca_procurement')
export class USCAProcurement {
  @PrimaryGeneratedColumn()
  id: number

  @Column('int')
  department: number

  @Column('varchar', { length: 127 })
  department_name: string

  @Column('varchar', { length: 63 })
  document_id: string

  @Column('text')
  associated_purchase_orders: string

  @Column('varchar', { length: 511 })
  first_item_title: string

  @Column('timestamp')
  start_date: Date

  @Column('timestamp')
  end_date: Date

  @Column('float')
  grand_total: number

  @Column('varchar', { length: 63 })
  supplier_id: string

  @Column('varchar', { length: 255 })
  supplier_name: string

  @Column('varchar', { length: 31 })
  certification_type: string

  @Column('varchar', { length: 255 })
  acquisition_type: string

  @Column('varchar', { length: 255 })
  acquisition_method: string

  @Column('varchar', { length: 31 })
  lpa_contract_id: string

  @Column('varchar', { length: 63 })
  buyer_name: string

  @Column('varchar', { length: 63 })
  buyer_email: string

  @Column('varchar', { length: 15 })
  status: string

  @Column('varchar', { length: 7 })
  version: string
}
