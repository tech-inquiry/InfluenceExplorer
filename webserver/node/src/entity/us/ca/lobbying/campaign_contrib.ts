import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_calaccess_campaign_contrib')
export class USCALobbyingCampaignContrib {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 63 })
  filing_id: number

  @Column('varchar', { length: 63 })
  transaction_id: string

  @Column('varchar', { length: 15 })
  line_item: number

  @Column('jsonb')
  campaign_contribution: Object

  @Column('jsonb')
  entities: object[]
}
