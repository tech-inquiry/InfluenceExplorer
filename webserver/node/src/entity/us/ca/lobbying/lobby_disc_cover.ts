import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_calaccess_lobby_disc_cover')
export class USCALobbyingLobbyDiscCover {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 63 })
  filing_id: number

  @Column('jsonb')
  cover: Object

  @Column('jsonb')
  unmatched_memos: Object[]

  @Column('jsonb')
  entities: object[]
}
