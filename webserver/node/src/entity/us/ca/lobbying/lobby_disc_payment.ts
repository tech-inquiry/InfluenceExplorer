import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_calaccess_lobby_disc_payment')
export class USCALobbyingLobbyDiscPayment {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 63 })
  filing_id: number

  @Column('varchar', { length: 63 })
  transaction_id: string

  @Column('varchar', { length: 15 })
  line_item: number

  @Column('jsonb')
  payment: Object

  @Column('jsonb')
  entities: string[]
}
