import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_calaccess_pre_elect_expend')
export class USCALobbyingPreElectExpend {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 63 })
  filing_id: number

  @Column('varchar', { length: 63 })
  transaction_id: string

  @Column('varchar', { length: 15 })
  line_item: number

  @Column('jsonb')
  pre_election_expenditure: Object

  @Column('jsonb')
  entities: object[]
}
