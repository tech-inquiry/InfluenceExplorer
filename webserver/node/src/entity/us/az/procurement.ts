import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_az_procurement')
export class USAZProcurement {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 255 })
  image: string

  @Column('varchar', { length: 255 })
  commodity: string

  @Column('varchar', { length: 255 })
  product_code: string

  @Column('varchar', { length: 255 })
  supplier: string

  @Column('varchar', { length: 255 })
  item_label: string

  @Column('text')
  summary: string

  @Column('float')
  negotiated_price: number

  @Column('varchar', { length: 7 })
  currency: string

  @Column('varchar', { length: 255 })
  contract: string

  @Column('varchar', { length: 15 })
  unit: string

  @Column('timestamp')
  start_date: Date

  @Column('timestamp')
  end_date: Date
}
