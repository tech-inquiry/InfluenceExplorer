import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_tx_dps_procurement')
export class USTXDPSProcurement {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 31 })
  contract: str

  @Column('varchar', { length: 127 })
  description: str

  @Column('varchar', { length: 127 })
  supplier: str

  @Column('date')
  start_date: Date

  @Column('date')
  end_date: Date

  @Column('varchar', { length: 31 })
  contract_maximum: str
}
