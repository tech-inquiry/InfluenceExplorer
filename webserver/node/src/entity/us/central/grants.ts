import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_central_grants')
export class USCentralGrants {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  transaction_unique_key: string

  @Column('text')
  unique_key: string

  @Column('text')
  id_fain: string

  @Column('text')
  mod_number: string

  @Column('text')
  id_uri: string

  @Column('text')
  sai_number: string

  @Column('double precision')
  federal_obligation: number

  @Column('double precision')
  total_obligated: number

  @Column('double precision')
  total_outlayed: number

  @Column('double precision')
  indirect_cost_federal_share: number

  @Column('double precision')
  non_federal_funding: number

  @Column('double precision')
  total_non_federal_funding: number

  @Column('double precision')
  face_value_of_loan: number

  @Column('double precision')
  original_loan_subsidy_cost: number

  @Column('double precision')
  total_face_value_of_loan: number

  @Column('double precision')
  total_loan_subsidy_cost: number

  @Column('double precision')
  pragmatic_obligations: number

  @Column('text')
  emergency_fund_codes: string

  @Column('double precision')
  covid_outlayed: number

  @Column('double precision')
  covid_obligated: number

  @Column('double precision')
  iija_outlayed: number

  @Column('double precision')
  iija_obligated: number

  @Column('timestamp')
  action_date: Date

  @Column('text')
  action_fiscal_year: string

  @Column('timestamp')
  start_date: Date

  @Column('timestamp')
  end_date: Date

  @Column('text')
  awarding_agency_code: string

  @Column('text')
  awarding_agency_name: string

  @Column('text')
  awarding_sub_agency_code: string

  @Column('text')
  awarding_sub_agency_name: string

  @Column('text')
  awarding_office_code: string

  @Column('text')
  awarding_office_name: string

  @Column('text')
  funding_agency_code: string

  @Column('text')
  funding_agency_name: string

  @Column('text')
  funding_sub_agency_code: string

  @Column('text')
  funding_sub_agency_name: string

  @Column('text')
  funding_office_code: string

  @Column('text')
  funding_office_name: string

  @Column('text')
  treasury_accounts: string

  @Column('text')
  federal_accounts: string

  @Column('text')
  object_classes: string

  @Column('text')
  program_activities: string

  @Column('text')
  recipient_uei: string

  @Column('text')
  recipient_duns: string

  @Column('text')
  recipient_name: string

  @Column('text')
  recipient_name_raw: string

  @Column('text')
  recipient_country_code: string

  @Column('text')
  recipient_country_name: string

  @Column('text')
  recipient_address_line_1: string

  @Column('text')
  recipient_address_line_2: string

  @Column('text')
  recipient_city_code: string

  @Column('text')
  recipient_city_name: string

  @Column('text')
  recipient_county_fips: string

  @Column('text')
  recipient_county_name: string

  @Column('text')
  recipient_state_fips: string

  @Column('text')
  recipient_state_code: string

  @Column('text')
  recipient_state_name: string

  @Column('text')
  recipient_zip_code: string

  @Column('text')
  recipient_zip_last_4: string

  @Column('text')
  recipient_cd_original: string

  @Column('text')
  recipient_cd_current: string

  @Column('text')
  recipient_foreign_city_name: string

  @Column('text')
  recipient_foreign_province_name: string

  @Column('text')
  recipient_foreign_postal_code: string

  @Column('text')
  recipient_parent_uei: string

  @Column('text')
  recipient_parent_duns: string

  @Column('text')
  recipient_parent_name: string

  @Column('text')
  recipient_parent_name_raw: string

  @Column('text')
  pop_scope: string

  @Column('text')
  pop_country_code: string

  @Column('text')
  pop_country_name: string

  @Column('text')
  pop_code: string

  @Column('text')
  pop_city_name: string

  @Column('text')
  pop_county_fips: string

  @Column('text')
  pop_county_name: string

  @Column('text')
  pop_state_fips: string

  @Column('text')
  pop_state_name: string

  @Column('text')
  pop_zip_4: string

  @Column('text')
  pop_cd_original: string

  @Column('text')
  pop_cd_current: string

  @Column('text')
  pop_foreign_location: string

  @Column('text')
  cfda_number: string

  @Column('text')
  cfda_title: string

  @Column('text')
  funding_opportunity_number: string

  @Column('text')
  funding_opportunity_goals: string

  @Column('text')
  assistance_type_code: string

  @Column('text')
  assistance_type_description: string

  @Column('text')
  transaction_description: string

  @Column('text')
  base_transaction_description: string

  @Column('text')
  funds_indicator_code: string

  @Column('text')
  funds_indicator_description: string

  @Column('text')
  business_types_code: string

  @Column('text')
  business_types_description: string

  @Column('text')
  correction_delete_indicator_code: string

  @Column('text')
  correction_delete_indicator_description: string

  @Column('text')
  action_type_code: string

  @Column('text')
  action_type_description: string

  @Column('text')
  record_type_code: string

  @Column('text')
  record_type_description: string

  @Column('text')
  officer_1_name: string

  @Column('double precision')
  officer_1_amount: number

  @Column('text')
  officer_2_name: string

  @Column('double precision')
  officer_2_amount: number

  @Column('text')
  officer_3_name: string

  @Column('double precision')
  officer_3_amount: number

  @Column('text')
  officer_4_name: string

  @Column('double precision')
  officer_4_amount: number

  @Column('text')
  officer_5_name: string

  @Column('double precision')
  officer_5_amount: number

  @Column('timestamp')
  initial_report_date: Date

  @Column('timestamp')
  last_modified_date: Date
}
