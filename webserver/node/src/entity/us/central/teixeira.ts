import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_teixeira')
export class USTeixeira {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  reference_id: string

  @Column('text')
  text: string
}
