import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('cablegate')
export class Cablegate {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 35, nullable: false })
  reference_id: string

  @Column('timestamp', { nullable: true })
  date: Date

  @Column('varchar', { length: 127, nullable: true })
  classification: string

  @Column('varchar', { length: 40, nullable: true })
  embassy: string

  @Column('text')
  preamble: string

  @Column('text')
  text: string
}
