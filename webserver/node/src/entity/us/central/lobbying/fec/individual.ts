import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_fec_indv')
export class USCentralFECIndividual {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 9 })
  filer_id: string

  @Column('varchar', { length: 1 })
  amendment_indicator: string

  @Column('varchar', { length: 3 })
  report_type: string

  @Column('varchar', { length: 5 })
  primary_general_indicator: string

  @Column('varchar', { length: 18 })
  image_number: string

  @Column('varchar', { length: 3 })
  transaction_type: string

  @Column('varchar', { length: 3 })
  entity_type: string

  @Column('varchar', { length: 200 })
  contributor_name: string

  @Column('varchar', { length: 30 })
  city: string

  @Column('varchar', { length: 2 })
  state: string

  @Column('varchar', { length: 9 })
  zip: string

  @Column('varchar', { length: 38 })
  employer: string

  @Column('varchar', { length: 38 })
  occupation: string

  @Column('timestamp', { nullable: true })
  transaction_date: Date

  @Column('decimal', { precision: 14, scale: 2 })
  transaction_amount: number

  @Column('varchar', { length: 9 })
  other_id: string

  @Column('varchar', { length: 32 })
  transaction_id: string

  @Column('decimal', { precision: 22 })
  file_number: number

  @Column('varchar', { length: 1 })
  memo_code: string

  @Column('varchar', { length: 100 })
  memo_text: string

  @Column('decimal', { precision: 19 })
  fec_record_number: number

  @Column('int')
  cycle: number

  @Column('jsonb')
  entities: object[]
}
