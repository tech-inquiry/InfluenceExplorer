import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_fec_cand')
export class USCentralFECCandidate {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 9 })
  candidate_id: string

  @Column('varchar', { length: 200 })
  candidate_name: string

  @Column('varchar', { length: 3 })
  candidate_party_affiliation: string

  @Column('decimal', { precision: 4 })
  election_year: number

  @Column('varchar', { length: 2 })
  candidate_office_state: string

  @Column('varchar', { length: 1 })
  candidate_office: string

  @Column('varchar', { length: 2 })
  candidate_office_district: string

  @Column('varchar', { length: 1 })
  candidate_incumbant_status: string

  @Column('varchar', { length: 1 })
  candidate_status: string

  @Column('varchar', { length: 9 })
  candidate_principal_campaign_committee: string

  @Column('varchar', { length: 34 })
  candidate_street1: string

  @Column('varchar', { length: 34 })
  candidate_street2: string

  @Column('varchar', { length: 30 })
  candidate_city: string

  @Column('varchar', { length: 2 })
  candidate_state: string

  @Column('varchar', { length: 9 })
  candidate_zip: string

  @Column('int')
  cycle: number
}
