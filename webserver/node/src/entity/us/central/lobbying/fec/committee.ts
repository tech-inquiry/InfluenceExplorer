import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_fec_comm')
export class USCentralFECCommittee {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 9 })
  committee_id: string

  @Column('varchar', { length: 200 })
  committee_name: string

  @Column('varchar', { length: 90 })
  treasurer_name: string

  @Column('varchar', { length: 34 })
  committee_street1: string

  @Column('varchar', { length: 34 })
  committee_street2: string

  @Column('varchar', { length: 30 })
  committee_city: string

  @Column('varchar', { length: 2 })
  committee_state: string

  @Column('varchar', { length: 9 })
  committee_zip: string

  @Column('varchar', { length: 1 })
  committee_designation: string

  @Column('varchar', { length: 1 })
  committee_type: string

  @Column('varchar', { length: 3 })
  committee_party_affiliation: string

  @Column('varchar', { length: 1 })
  committee_filing_frequency: string

  @Column('varchar', { length: 1 })
  interest_group_category: string

  @Column('varchar', { length: 200 })
  connected_organization_name: string

  @Column('varchar', { length: 9 })
  candidate_id: string

  @Column('int')
  cycle: number
}
