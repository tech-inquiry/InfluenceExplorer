import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_opr_issues_filing')
export class USCentralOPRIssues {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 123 })
  filing_uuid: string

  @Column('varchar', { length: 3 })
  filing_type: string

  @Column('int')
  filing_year: number

  @Column('varchar', { length: 15 })
  filing_period: string

  @Column('float')
  income: number

  @Column('float')
  expenses: number

  @Column('varchar', { length: 3 })
  expenses_method: string

  @Column('varchar', { length: 255 })
  posted_by_name: string

  @Column('timestamp', { nullable: true })
  dt_posted: Date

  @Column('timestamp', { nullable: true })
  termination_date: Date

  @Column('jsonb', { nullable: true })
  registrant: object

  @Column('jsonb', { nullable: true })
  client: object

  @Column('jsonb', { nullable: true })
  lobbying_activities: object[]

  @Column('jsonb', { nullable: true })
  conviction_disclosures: object[]

  @Column('jsonb', { nullable: true })
  foreign_entities: object[]

  @Column('jsonb', { nullable: true })
  affiliated_organizations: object[]

  @Column('jsonb', { nullable: true })
  lobbyist_ids: number[]

  @Column('text')
  search_text: string
}
