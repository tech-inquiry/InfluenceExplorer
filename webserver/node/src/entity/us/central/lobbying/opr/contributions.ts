import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_opr_contributions_filing')
export class USCentralOPRContributions {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 123, nullable: true })
  filing_uuid: string

  @Column('varchar', { length: 3, nullable: true })
  filing_type: string

  @Column('int')
  filing_year: number

  @Column('varchar', { length: 15, nullable: true })
  filing_period: string

  @Column('varchar', { length: 15, nullable: true })
  filer_type: string

  @Column('timestamp', { nullable: true })
  dt_posted: Date

  @Column('varchar', { length: 255, nullable: true })
  contact_name: string

  @Column('text', { nullable: true })
  comments: string

  @Column('varchar', { length: 255, nullable: true })
  address_1: string

  @Column('varchar', { length: 255, nullable: true })
  address_2: string

  @Column('varchar', { length: 255, nullable: true })
  city: string

  @Column('varchar', { length: 63, nullable: true })
  state_display: string

  @Column('varchar', { length: 63, nullable: true })
  zip: string

  @Column('varchar', { length: 63, nullable: true })
  country_display: string

  @Column('jsonb', { nullable: true })
  registrant: object

  @Column('jsonb', { nullable: true })
  lobbyist: object

  @Column('jsonb', { nullable: true })
  pacs: object[]

  @Column('jsonb', { nullable: true })
  contribution_items: object[]
}
