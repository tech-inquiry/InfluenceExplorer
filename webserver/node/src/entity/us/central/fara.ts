import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_fara')
export class USCentralFARAFiling {
  @PrimaryGeneratedColumn()
  id: number

  @Column('timestamp', { nullable: true })
  date_stamped: Date

  @Column('varchar', { length: 255, nullable: false })
  registrant_name: string

  @Column('varchar', { length: 15, nullable: false })
  registrant_number: string

  @Column('varchar', { length: 255, nullable: false })
  document_type: string

  @Column('varchar', { length: 255, nullable: false })
  short_form_name: string

  @Column('varchar', { length: 255, nullable: false })
  foreign_principal_name: string

  @Column('varchar', { length: 255, nullable: false })
  foreign_principal_country: string

  @Column('varchar', { length: 255, nullable: false })
  url: string
}
