import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_nlrb_filing')
export class USCentralLaborRelations {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 15, nullable: false })
  case_number: string

  @Column('timestamp', { nullable: true })
  date_filed: Date

  @Column('varchar', { length: 63, nullable: true })
  status: string

  @Column('varchar', { length: 127, nullable: true })
  location: string

  @Column('varchar', { length: 127, nullable: true })
  region_assigned: string

  @Column('varchar', { length: 63, nullable: true })
  reason_closed: string

  @Column('jsonb', { nullable: true })
  docket_activity: object[]

  @Column('jsonb', { nullable: true })
  related_documents: object[]

  @Column('jsonb', { nullable: true })
  allegations: string[]

  @Column('jsonb', { nullable: true })
  participants: object[]

  @Column('jsonb', { nullable: true })
  related_cases: object[]

  @Column('varchar', { length: 1023, nullable: true })
  title: string
}
