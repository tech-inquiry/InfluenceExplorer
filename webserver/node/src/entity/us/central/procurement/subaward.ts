import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_central_procurement_subaward')
export class USCentralProcurementSubaward {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 123 })
  prime_key: string

  @Column('varchar', { length: 63 })
  prime_piid: string

  @Column('varchar', { length: 63 })
  prime_parent_piid: string

  @Column('timestamp')
  prime_base_action_date: Date

  @Column('varchar', { length: 123 })
  prime_awarding_agency: string

  @Column('varchar', { length: 4 })
  prime_awarding_agency_code: string

  @Column('varchar', { length: 123 })
  prime_awarding_sub_agency: string

  @Column('varchar', { length: 4 })
  prime_awarding_sub_agency_code: string

  @Column('varchar', { length: 123 })
  prime_awarding_office: string

  @Column('varchar', { length: 6 })
  prime_awarding_office_code: string

  @Column('varchar', { length: 123 })
  prime_funding_agency: string

  @Column('varchar', { length: 4 })
  prime_funding_agency_code: string

  @Column('varchar', { length: 123 })
  prime_funding_sub_agency: string

  @Column('varchar', { length: 4 })
  prime_funding_sub_agency_code: string

  @Column('varchar', { length: 123 })
  prime_funding_office: string

  @Column('varchar', { length: 6 })
  prime_funding_office_code: string

  @Column('varchar', { length: 15 })
  prime_uei: string

  @Column('varchar', { length: 255 })
  prime_name: string

  @Column('varchar', { length: 15 })
  prime_parent_uei: string

  @Column('varchar', { length: 255 })
  prime_parent_name: string

  @Column('varchar', { length: 255 })
  prime_dba: string

  @Column('int')
  sub_fsrs_year: number

  @Column('int')
  sub_fsrs_month: number

  @Column('varchar', { length: 63 })
  sub_number: string

  @Column('float')
  sub_amount: number

  @Column('timestamp')
  sub_action_date: Date

  @Column('varchar', { length: 15 })
  sub_uei: string

  @Column('varchar', { length: 255 })
  sub_name: string

  @Column('varchar', { length: 255 })
  sub_dba: string

  @Column('varchar', { length: 15 })
  sub_parent_uei: string

  @Column('varchar', { length: 255 })
  sub_parent_name: string

  @Column('varchar', { length: 255 })
  sub_pop_address_line_1: string

  @Column('varchar', { length: 255 })
  sub_pop_city: string

  @Column('varchar', { length: 255 })
  sub_pop_state: string

  @Column('varchar', { length: 255 })
  sub_pop_zipcode: string

  @Column('varchar', { length: 255 })
  sub_pop_district: string

  @Column('varchar', { length: 255 })
  sub_pop_country: string

  @Column('text')
  sub_description: string

  @Column('timestamp')
  sub_fsrs_last_modified: Date
}
