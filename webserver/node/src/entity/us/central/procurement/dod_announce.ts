import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_dod_announcements')
export class USCentralProcurementDODAnnounce {
  @PrimaryGeneratedColumn()
  id: number

  @Column('int')
  article_id: number

  @Column('timestamp')
  date: Date

  @Column('text')
  text: string
}
