import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity('us_central_procurement')
export class USCentralProcurementPrime {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 63 })
  piid: string

  @Column('varchar', { length: 63 })
  mod_number: string

  @Column('int')
  transaction_number: number

  @Column('varchar', { length: 63 })
  parent_piid: string

  @Column('varchar', { length: 63 })
  parent_mod_number: string

  @Column('varchar', { length: 255 })
  title: string

  @Column('varchar', { length: 255 })
  fpds_link: string

  @Column('varchar', { length: 127, nullable: true })
  contracting_department: string

  @Column('varchar', { length: 4, nullable: true })
  contracting_department_code: string

  @Column('varchar', { length: 127, nullable: true })
  contracting_agency: string

  @Column('varchar', { length: 4, nullable: true })
  contracting_agency_code: string

  @Column('varchar', { length: 127, nullable: true })
  contracting_office: string

  @Column('varchar', { length: 6, nullable: true })
  contracting_office_code: string

  @Column('varchar', { length: 127, nullable: true })
  funding_department: string

  @Column('varchar', { length: 4, nullable: true })
  funding_department_code: string

  @Column('varchar', { length: 127, nullable: true })
  funding_agency: string

  @Column('varchar', { length: 4, nullable: true })
  funding_agency_code: string

  @Column('varchar', { length: 127, nullable: true })
  funding_office: string

  @Column('varchar', { length: 6, nullable: true })
  funding_office_code: string

  @Column('varchar', { length: 63, nullable: true })
  contract_action_type: string

  @Column('varchar', { length: 255, nullable: true })
  vendor: string

  @Column('varchar', { length: 15 })
  uei: string

  @Column('varchar', { length: 7 })
  cage_code: string

  @Column('varchar', { length: 255 })
  ultimate_parent: string

  @Column('varchar', { length: 15 })
  ultimate_parent_uei: string

  @Column('timestamp')
  signed_date: Date

  @Column('timestamp', { nullable: true })
  effective_date: Date

  @Column('timestamp', { nullable: true })
  modified_date: Date

  @Column({
    type: 'decimal',
    precision: 16,
    scale: 2,
    nullable: true,
  })
  obligated_amount: number

  @Column({
    type: 'decimal',
    precision: 16,
    scale: 2,
    nullable: true,
  })
  base_and_exercised_options_value: number

  @Column({
    type: 'decimal',
    precision: 16,
    scale: 2,
    nullable: true,
  })
  base_and_all_options_value: number

  @Column({
    type: 'decimal',
    precision: 16,
    scale: 2,
    nullable: true,
  })
  total_obligated_amount: number

  @Column({
    type: 'decimal',
    precision: 16,
    scale: 2,
    nullable: true,
  })
  total_base_and_exercised_options_value: number

  @Column({
    type: 'decimal',
    precision: 16,
    scale: 2,
    nullable: true,
  })
  total_base_and_all_options_value: number

  @Column('text')
  description: string

  @Column('varchar', { length: 255, nullable: true })
  major_program_code: string

  @Column('varchar', { length: 255, nullable: true })
  niac_description: string

  @Column('varchar', { length: 255, nullable: true })
  niac_text: string

  @Column('jsonb', { nullable: true })
  place_of_performance: object

  @Column('varchar', { length: 255, nullable: true })
  contractor: string

  @Column('varchar', { length: 7, nullable: true })
  principal_naics_code: string

  @Column('varchar', { length: 7, nullable: true })
  product_or_service_code: string

  @Column('timestamp')
  current_completion_date: Date

  @Column('timestamp')
  ultimate_completion_date: Date

  @Column('timestamp')
  last_date_to_order: Date

  @Column('varchar', { length: 63 })
  solicitation_id: string
}
