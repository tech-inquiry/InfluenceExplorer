import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_central_opportunities')
export class USCentralOpportunities {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  notice_id: str

  @Column('text')
  title: str

  @Column('text')
  solicitation_number: str

  @Column('text')
  department: str

  @Column('text')
  cgac: str

  @Column('text')
  subtier: str

  @Column('text')
  fpds_code: str

  @Column('text')
  office: str

  @Column('text')
  aac_code: str

  @Column('timestamp')
  posted_date: Date

  @Column('text')
  type: string

  @Column('text')
  base_type: string

  @Column('text')
  archive_type: string

  @Column('timestamp')
  archive_date: Date

  @Column('text')
  set_aside_code: string

  @Column('text')
  set_aside: string

  @Column('timestamp')
  deadline: Date

  @Column('text')
  naics_code: string

  @Column('text')
  classification_code: string

  @Column('text')
  pop_street_address: string

  @Column('text')
  pop_city: string

  @Column('text')
  pop_state: string

  @Column('text')
  pop_zip: string

  @Column('text')
  pop_country: string

  @Column('text')
  active: string

  @Column('text')
  award_number: string

  @Column('timestamp')
  award_date: Date

  @Column('text')
  award_amount: string

  @Column('text')
  awardee: string

  @Column('text')
  primary_contact_title: string

  @Column('text')
  primary_contact_name: string

  @Column('text')
  primary_contact_email: string

  @Column('text')
  primary_contact_phone: string

  @Column('text')
  primary_contact_fax: string

  @Column('text')
  secondary_contact_title: string

  @Column('text')
  secondary_contact_name: string

  @Column('text')
  secondary_contact_email: string

  @Column('text')
  secondary_contact_phone: string

  @Column('text')
  secondary_contact_fax: string

  @Column('text')
  organization_type: string

  @Column('text')
  state: string

  @Column('text')
  city: string

  @Column('text')
  zip: string

  @Column('text')
  country_code: string

  @Column('text')
  additional_info_link: string

  @Column('text')
  link: string

  @Column('text')
  description: string

  @Column('text')
  text: string
}
