import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_securities_filing')
export class USCentralSecuritiesFiling {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 15 })
  cik: string

  @Column('varchar', { length: 31 })
  accession: string

  @Column('date')
  filing_date: Date

  @Column('date')
  report_date: Date

  @Column('timestamp')
  acceptance_date_time: Date

  @Column('varchar', { length: 7 })
  act: string

  @Column('varchar', { length: 31 })
  form: string

  @Column('varchar', { length: 63 })
  file_number: string

  @Column('varchar', { length: 63 })
  film_number: string

  @Column('varchar', { length: 63 })
  items: string

  @Column('bigint')
  size: Number

  @Column('boolean')
  is_xbrl: Boolean

  @Column('boolean')
  is_inline_xbrl: Boolean

  @Column('varchar', { length: 63 })
  primary_document: string

  @Column('varchar', { length: 127 })
  primary_doc_description: string
}
