import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_securities_meta')
export class USCentralSecuritiesMeta {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 15 })
  cik: string

  @Column('varchar', { length: 15 })
  entity_type: string

  @Column('varchar', { length: 15 })
  sic: string

  @Column('varchar', { length: 255 })
  sic_description: string

  @Column('boolean')
  insider_owner_transaction: Boolean

  @Column('boolean')
  insider_issuer_transaction: Boolean

  @Column('varchar', { length: 255 })
  name: string

  @Column('jsonb')
  tickers: string[]

  @Column('jsonb')
  exchanges: string[]

  @Column('varchar', { length: 15 })
  ein: string

  @Column('varchar', { length: 255 })
  description: string

  @Column('varchar', { length: 255 })
  website: string

  @Column('varchar', { length: 255 })
  category: string

  @Column('varchar', { length: 15 })
  fiscal_year_end: string

  @Column('varchar', { length: 7 })
  state_of_incorporation: string

  @Column('varchar', { length: 63 })
  state_of_incorporation_description: string

  @Column('jsonb')
  addresses: Object

  @Column('varchar', { length: 63 })
  phone: string

  @Column('varchar', { length: 15 })
  flags: string

  @Column('jsonb')
  former_names: string[]
}
