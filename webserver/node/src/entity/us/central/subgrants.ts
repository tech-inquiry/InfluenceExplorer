import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_central_subgrants')
export class USCentralSubgrants {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  prime_unique_key: string

  @Column('text')
  prime_fain: string

  @Column('double precision')
  prime_amount: number

  @Column('text')
  prime_emergency_fund_codes: string

  @Column('double precision')
  prime_covid_outlayed: number

  @Column('double precision')
  prime_covid_obligated: number

  @Column('double precision')
  prime_iija_outlayed: number

  @Column('double precision')
  prime_iija_obligated: number

  @Column('double precision')
  prime_total_outlayed: number

  @Column('timestamp')
  prime_base_action_date: Date

  @Column('text')
  prime_base_action_fiscal_year: string

  @Column('timestamp')
  prime_latest_action_date: Date

  @Column('text')
  prime_latest_action_fiscal_year: string

  @Column('timestamp')
  prime_start_date: Date

  @Column('timestamp')
  prime_end_date: Date

  @Column('text')
  prime_awarding_agency_code: string

  @Column('text')
  prime_awarding_agency_name: string

  @Column('text')
  prime_awarding_sub_agency_code: string

  @Column('text')
  prime_awarding_sub_agency_name: string

  @Column('text')
  prime_awarding_office_code: string

  @Column('text')
  prime_awarding_office_name: string

  @Column('text')
  prime_funding_agency_code: string

  @Column('text')
  prime_funding_agency_name: string

  @Column('text')
  prime_funding_sub_agency_code: string

  @Column('text')
  prime_funding_sub_agency_name: string

  @Column('text')
  prime_funding_office_code: string

  @Column('text')
  prime_funding_office_name: string

  @Column('text')
  prime_treasury_accounts: string

  @Column('text')
  prime_federal_accounts: string

  @Column('text')
  prime_object_classes: string

  @Column('text')
  prime_program_activities: string

  @Column('text')
  prime_uei: string

  @Column('text')
  prime_duns: string

  @Column('text')
  prime_name: string

  @Column('text')
  prime_dba: string

  @Column('text')
  prime_parent_uei: string

  @Column('text')
  prime_parent_duns: string

  @Column('text')
  prime_parent_name: string

  @Column('text')
  prime_country_code: string

  @Column('text')
  prime_country_name: string

  @Column('text')
  prime_address_line_1: string

  @Column('text')
  prime_city_name: string

  @Column('text')
  prime_county_fips: string

  @Column('text')
  prime_county_name: string

  @Column('text')
  prime_state_fips: string

  @Column('text')
  prime_state_code: string

  @Column('text')
  prime_state_name: string

  @Column('text')
  prime_zip_code: string

  @Column('text')
  prime_cd_original: string

  @Column('text')
  prime_cd_current: string

  @Column('text')
  prime_foreign_postal_code: string

  @Column('text')
  prime_business_types: string

  @Column('text')
  prime_pop_scope: string

  @Column('text')
  prime_pop_city_name: string

  @Column('text')
  prime_pop_county_fips: string

  @Column('text')
  prime_pop_county_name: string

  @Column('text')
  prime_pop_state_fips: string

  @Column('text')
  prime_pop_state_code: string

  @Column('text')
  prime_pop_state_name: string

  @Column('text')
  prime_pop_zip_code: string

  @Column('text')
  prime_pop_cd_original: string

  @Column('text')
  prime_pop_cd_current: string

  @Column('text')
  prime_pop_country_code: string

  @Column('text')
  prime_pop_country_name: string

  @Column('text')
  prime_base_transaction_description: string

  @Column('text')
  prime_cfda_numbers_and_titles: string

  @Column('text')
  sub_type: string

  @Column('text')
  sub_fsrs_report_id: string

  @Column('text')
  sub_fsrs_report_year: string

  @Column('text')
  sub_fsrs_report_month: string

  @Column('text')
  sub_number: string

  @Column('double precision')
  sub_amount: number

  @Column('timestamp')
  sub_action_date: Date

  @Column('text')
  sub_action_fiscal_year: string

  @Column('text')
  sub_uei: string

  @Column('text')
  sub_duns: string

  @Column('text')
  sub_name: string

  @Column('text')
  sub_dba: string

  @Column('text')
  sub_parent_uei: string

  @Column('text')
  sub_parent_duns: string

  @Column('text')
  sub_parent_name: string

  @Column('text')
  sub_country_code: string

  @Column('text')
  sub_country_name: string

  @Column('text')
  sub_address_line_1: string

  @Column('text')
  sub_city_name: string

  @Column('text')
  sub_state_code: string

  @Column('text')
  sub_state_name: string

  @Column('text')
  sub_zip_code: string

  @Column('text')
  sub_recipient_cd_original: string

  @Column('text')
  sub_recipient_cd_current: string

  @Column('text')
  sub_foreign_postal_code: string

  @Column('text')
  sub_business_types: string

  @Column('text')
  sub_pop_city_name: string

  @Column('text')
  sub_pop_state_code: string

  @Column('text')
  sub_pop_state_name: string

  @Column('text')
  sub_pop_zip_code: string

  @Column('text')
  sub_pop_cd_original: string

  @Column('text')
  sub_pop_cd_current: string

  @Column('text')
  sub_pop_country_code: string

  @Column('text')
  sub_pop_country_name: string

  @Column('text')
  sub_description: string

  @Column('text')
  sub_officer_1_name: string

  @Column('double precision')
  sub_officer_1_amount: number

  @Column('text')
  sub_officer_2_name: string

  @Column('double precision')
  sub_officer_2_amount: number

  @Column('text')
  sub_officer_3_name: string

  @Column('double precision')
  sub_officer_3_amount: number

  @Column('text')
  sub_officer_4_name: string

  @Column('double precision')
  sub_officer_4_amount: number

  @Column('text')
  sub_officer_5_name: string

  @Column('double precision')
  sub_officer_5_amount: number

  @Column('timestamp')
  sub_fsrs_report_last_modified_date: Date
}
