import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_fl_facts_filing')
@Index(
  'unique_us_fl_facts_filing',
  [
    'agency',
    'vendor',
    'type',
    'agency_contract_id',
    'po_number',
    'grant_award_id',
    'commodity_type_code',
    'flair_contract_id',
    'state_term_contract_id',
    'status',
  ],
  { unique: true }
)
export class USFLProcurement {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar', { length: 127 })
  agency: string

  @Column('varchar', { length: 127 })
  vendor: string

  @Column('varchar', { length: 127 })
  type: string

  @Column('varchar', { length: 63 })
  agency_contract_id: string

  @Column('varchar', { length: 63 })
  po_number: string

  @Column('varchar', { length: 63 })
  grant_award_id: string

  @Column('float')
  orig_contract_amount: number

  @Column('float')
  total_amount: number

  @Column('varchar', { length: 63 })
  recurring_budgetary_amount: string

  @Column('varchar', { length: 63 })
  non_recurring_budgetary_amount: string

  @Column('float')
  po_budget_amount: number

  @Column('varchar', { length: 63 })
  commodity_type_code: string

  @Column('varchar', { length: 63 })
  commodity_type_description: string

  @Column('varchar', { length: 255 })
  short_title: string

  @Column('varchar', { length: 255 })
  long_title: string

  @Column('varchar', { length: 127 })
  status: string

  @Column('varchar', { length: 63 })
  flair_contract_id: string

  @Column('timestamp')
  begin_date: Date

  @Column('timestamp')
  original_end_date: Date

  @Column('timestamp')
  new_end_date: Date

  @Column('timestamp')
  contract_execution_date: Date

  @Column('timestamp')
  grant_award_date: Date

  @Column('timestamp')
  po_order_date: Date

  @Column('varchar', { length: 127 })
  agency_service_area: string

  @Column('varchar', { length: 127 })
  authorized_advanced_payment: string

  @Column('varchar', { length: 511 })
  method_of_procurement: string

  @Column('varchar', { length: 63 })
  state_term_contract_id: string

  @Column('varchar', { length: 127 })
  agency_reference_number: string

  @Column('text')
  contract_exemption_explanation: string

  @Column('varchar', { length: 127 })
  statutory_authority: string

  @Column('varchar', { length: 63 })
  recipient_type: string

  @Column('varchar', { length: 63 })
  involves_gov_aid: string

  @Column('varchar', { length: 63 })
  provide_admin_cost: string

  @Column('varchar', { length: 63 })
  admin_cost_percent: string

  @Column('varchar', { length: 63 })
  provide_periodic_increase: string

  @Column('varchar', { length: 63 })
  periodic_increase_percent: string

  @Column('varchar', { length: 63 })
  business_case_study: string

  @Column('timestamp')
  business_case_date: Date

  @Column('varchar', { length: 63 })
  legal_challenges: string

  @Column('text')
  legal_challenge_description: string

  @Column('varchar', { length: 63 })
  previously_done_by_state: string

  @Column('varchar', { length: 63 })
  considered_for_insourcing: string

  @Column('varchar', { length: 63 })
  improvement_on_state_property: string

  @Column('text')
  improvement_description: string

  @Column('float')
  improvement_value: number

  @Column('varchar', { length: 255 })
  improvement_unamortized_value: string

  @Column('text')
  comment: string

  @Column('varchar', { length: 63 })
  cfda_code: string

  @Column('varchar', { length: 255 })
  cfda_description: string

  @Column('varchar', { length: 63 })
  csfa_code: string

  @Column('varchar', { length: 127 })
  csfa_description: string
}
