import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm'

@Entity('us_ny_ogs_filing')
@Index(
  'unique_us_ny_ogs_filing',
  [
    'group_number',
    'description',
    'award_number',
    'contract_number',
    'contractor',
  ],
  { unique: true }
)
export class USNYProcurement {
  @PrimaryGeneratedColumn()
  id: number

  @Column('int')
  group_number: number

  @Column('varchar', { length: 255 })
  description: string

  @Column('int')
  award_number: number

  @Column('varchar', { length: 63 })
  contract_number: string

  @Column('varchar', { length: 123 })
  contractor: string

  @Column('timestamp')
  start_date: Date

  @Column('timestamp')
  end_date: Date
}
