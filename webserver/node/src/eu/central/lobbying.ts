import { dataSource } from '../../App'
import * as tags from '../../tags'
import * as util from '../../util'

import { FeedInfo } from '../../EntityState'

import { EULobbying } from '../../entity/eu/lobbying'

const kTable = EULobbying
const kTableName = 'eu_lobbying'

const kTagDataset = 'eu_lobbying'

const fullTextSearchString = util.getFullTextSearchString('simple', [
  'lower(identification_code)',
  'lower(name)',
  'lower(acronym)',
  'lower(entity_form)',
  'lower(registration_category)',
  'lower(head_office::TEXT)',
  'lower(eu_office::TEXT)',
  'lower(goals)',
  'lower(levels_of_interest::TEXT)',
  'lower(legislative_proposals::TEXT)',
  'lower(supported_forums::TEXT)',
  'lower(communications)',
  'lower(members::TEXT)',
  'lower(interests::TEXT)',
  'lower(structure::TEXT)',
  'lower(interest_represented)',
  'lower(financial_data::TEXT)',
  'lower(groupings)',
])

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`last_update_date >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`last_update_date <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function incorporateDateConstraint(dateRange: util.DateRange, query) {
  if (dateRange.hasStart()) {
    query = query.andWhere('last_update_date >= :startDate', {
      startDate: dateRange.start.toISOString(),
    })
  }
  if (dateRange.hasEnd()) {
    query = query.andWhere('last_update_date <= :endDate', {
      endDate: dateRange.end.toISOString(),
    })
  }
}

// Set up the list of EU lobbying names for the vendor. If
// subsidiaries were requested to be included, the same process is repeated for
// each descendant.
function expandNames(
  entityList: string[],
  entityAlternates: Map<string, string[]>,
  entities: Map<string, Map<string, any>>
): string[] {
  return util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'eu',
    'central',
    'lobbying',
  ])
}

async function handleGetTags(request, response) {
  return await tags.handleGetTags(request, response, kTagDataset)
}

async function handleAddTag(request, response) {
  return await tags.handleAddTag(request, response, kTagDataset)
}

async function handleDeleteTag(request, response) {
  return await tags.handleDeleteTag(request, response, kTagDataset)
}

function annotateFiling(
  filing,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>
) {
  filing['annotation'] = {}
  const vendor = filing['name']
  if (vendor) {
    const normalizedVendor = util.normalizeEntityWithAttribute(
      vendor,
      filing['website_url'],
      'website',
      entityNormalization
    )
    filing['annotation']['vendor'] = {
      origText: vendor,
      text: normalizedVendor,
      stylizedText: util.getFullyStylizedName(normalizedVendor, entities),
      logo: util.logoURLFromAncestors(normalizedVendor, entities),
    }
  }
}

function annotate(
  filings,
  entities: Map<string, Map<string, any>>,
  entityNormalization: Map<string, string>
) {
  for (let filing of filings.filings) {
    annotateFiling(filing, entities, entityNormalization)
  }
}

async function haveForNames(
  names: string[],
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(names, dateRange, entities, [
      'feeds',
      'eu',
      'central',
      'lobbying',
    ])
  ) {
    return true
  }

  let nameList = []
  let websiteList = []
  for (const item of names) {
    if (item.startsWith('{"')) {
      const data = JSON.parse(item)
      if (data.hasOwnProperty('website')) {
        websiteList.push(data['website'])
      } else {
        nameList.push(data['name'])
      }
    } else {
      nameList.push(item)
    }
  }

  let params = []

  let searchConstraint = ''
  if (nameList.length && websiteList.length) {
    searchConstraint = `lower(name) = ANY($${
      params.length + 1
    }) OR website_url = ANY($${params.length + 2})`
    params.push(nameList)
    params.push(websiteList)
  } else if (nameList.length) {
    searchConstraint = `lower(name) = ANY($${params.length + 1})`
    params.push(nameList)
  } else if (websiteList.length) {
    searchConstraint = `website_url = ANY($${params.length + 1})`
    params.push(websiteList)
  }

  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr}  LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await haveForNames(expandedNames, dateRange, entities)
}

async function haveFromSearch(
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const searchConstraint =
    fullTextSearchString +
    ` @@ websearch_to_tsquery('simple', $${params.length + 1})`
  params.push(webSearch)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintsStr = [searchConstraint, dateConstraint]
    .filter((e) => e)
    .join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  webSearch: string,
  token: string
): Promise<boolean> {
  return await tags.haveFromTagSearch(webSearch, token, kTagDataset)
}

async function getForNames(
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let nameList = []
  let websiteList = []
  for (const item of names) {
    if (item.startsWith('{"')) {
      const data = JSON.parse(item)
      if (data.hasOwnProperty('website')) {
        websiteList.push(data['website'])
      } else {
        nameList.push(data['name'])
      }
    } else {
      nameList.push(item)
    }
  }

  let query = dataSource.getRepository(kTable).createQueryBuilder()

  if (nameList.length && websiteList.length) {
    query = query
      .where('lower(name) = ANY(ARRAY[:...names])', { names: nameList })
      .orWhere('website_url = ANY(ARRAY[:...websites])', {
        websites: websiteList,
      })
  } else if (nameList.length) {
    query = query.where('lower(name) = ANY(ARRAY[:...names])', {
      names: nameList,
    })
  } else if (websiteList.length) {
    query = query.where('website_url = ANY(ARRAY[:...websites])', {
      websites: websiteList,
    })
  }

  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('last_update_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getForName(
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  maxSearchResults: number,
  searchOffset = 0
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(names, entityAlternates, entities)
  return await getForNames(
    expandedNames,
    dateRange,
    maxSearchResults,
    searchOffset
  )
}

async function getFromSearch(
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  const fullTextQuery =
    fullTextSearchString + "@@ websearch_to_tsquery('simple', :webSearch)"

  let query = dataSource
    .getRepository(kTable)
    .createQueryBuilder()
    .where(fullTextQuery, { webSearch: webSearch })
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('last_update_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function getFromTagSearch(
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const uniqueKeys = ['identification_code']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    kTableName,
    kTagDataset,
    uniqueKeys
  )
}

async function haveOverDateRange(dateRange: util.DateRange): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${kTableName} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function getOverDateRange(
  dateRange: util.DateRange,
  maxSearchResults: number,
  searchOffset = 0
) {
  let query = dataSource.getRepository(kTable).createQueryBuilder()
  incorporateDateConstraint(dateRange, query)
  query = query.orderBy('last_update_date', 'DESC').limit(maxSearchResults)
  if (searchOffset > 0) {
    query = query.offset(searchOffset)
  }

  return await util.getQueryBuilderFilings(query, maxSearchResults)
}

async function haveAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(`Boolean EU lobbying w/ text: ${text}`)
    result = await haveFromSearch(text, dateRange)
  } else if (tagText) {
    console.log(`Have EU lobbying w/ tag: ${tagText}`)
    result = await haveFromTagSearch(tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`Boolean EU lobbying w/ entity: ${name}`)
    result = await haveForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names
    )
  } else {
    console.log('EU lobbying API')
    result = await haveOverDateRange(dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const maxSearchResultsDefault = 10000
  const maxSearchResultsCap = 50000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  const searchOffset = util.readPositiveNumber(
    params.searchOffset,
    0,
    'searchOffset'
  )

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`EU lobbying w/ text: ${text}`)
    result = await getFromSearch(
      text,
      dateRange,
      maxSearchResults,
      searchOffset
    )
  } else if (tagText) {
    console.log(`EU lobbying w/ tag: ${tagText}`)
    result = await getFromTagSearch(tagText, token, maxSearchResults)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`EU lobbying w/ entity: ${name}`)
    result = await getForName(
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names,
      maxSearchResults,
      searchOffset
    )
  } else {
    console.log('EU lobbying API')
    result = await getOverDateRange(dateRange, maxSearchResults, searchOffset)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(result, entities, feedInfo.normalization)
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const route = '/api/eu/central/lobbying'

  const feed = entityState.feeds.get('eu').get('central').get('lobbying')
  const entities = entityState.entities
  const children = entityState.children

  // Returns whether there are any filings for text or an entity.
  router.get(`${route}/have`, async function (request, response) {
    await haveAPI(request, response, entities, children, feed)
  })

  // Returns JSON for a web search or an entity.
  router.get(`${route}/get`, async function (request, response) {
    await getAPI(request, response, entities, children, feed)
  })

  router.post(`${route}/getTags`, handleGetTags)
  router.post(`${route}/addTag`, handleAddTag)
  router.post(`${route}/deleteTag`, handleDeleteTag)
}
