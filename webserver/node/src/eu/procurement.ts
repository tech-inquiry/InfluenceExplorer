import { dataSource } from '../App'
import { FeedInfo } from '../EntityState'
import * as tags from '../tags'
import * as util from '../util'

import { EU1AProcurement } from '../entity/eu/1a/procurement'
import { EUALProcurement } from '../entity/eu/al/procurement'
import { EUAMProcurement } from '../entity/eu/am/procurement'
import { EUARProcurement } from '../entity/eu/ar/procurement'
import { EUATProcurement } from '../entity/eu/at/procurement'
import { EUBAProcurement } from '../entity/eu/ba/procurement'
import { EUBDProcurement } from '../entity/eu/bd/procurement'
import { EUBEProcurement } from '../entity/eu/be/procurement'
import { EUBGProcurement } from '../entity/eu/bg/procurement'
import { EUBJProcurement } from '../entity/eu/bj/procurement'
import { EUBOProcurement } from '../entity/eu/bo/procurement'
import { EUBYProcurement } from '../entity/eu/by/procurement'
import { EUCGProcurement } from '../entity/eu/cg/procurement'
import { EUCHProcurement } from '../entity/eu/ch/procurement'
import { EUCIProcurement } from '../entity/eu/ci/procurement'
import { EUCMProcurement } from '../entity/eu/cm/procurement'
import { EUCNProcurement } from '../entity/eu/cn/procurement'
import { EUCOProcurement } from '../entity/eu/co/procurement'
import { EUCYProcurement } from '../entity/eu/cy/procurement'
import { EUCZProcurement } from '../entity/eu/cz/procurement'
import { EUDEProcurement } from '../entity/eu/de/procurement'
import { EUDKProcurement } from '../entity/eu/dk/procurement'
import { EUDZProcurement } from '../entity/eu/dz/procurement'
import { EUECProcurement } from '../entity/eu/ec/procurement'
import { EUEEProcurement } from '../entity/eu/ee/procurement'
import { EUEGProcurement } from '../entity/eu/eg/procurement'
import { EUESProcurement } from '../entity/eu/es/procurement'
import { EUETProcurement } from '../entity/eu/et/procurement'
import { EUFIProcurement } from '../entity/eu/fi/procurement'
import { EUFRProcurement } from '../entity/eu/fr/procurement'
import { EUGEProcurement } from '../entity/eu/ge/procurement'
import { EUGNProcurement } from '../entity/eu/gn/procurement'
import { EUGPProcurement } from '../entity/eu/gp/procurement'
import { EUGRProcurement } from '../entity/eu/gr/procurement'
import { EUHRProcurement } from '../entity/eu/hr/procurement'
import { EUHTProcurement } from '../entity/eu/ht/procurement'
import { EUHUProcurement } from '../entity/eu/hu/procurement'
import { EUIEProcurement } from '../entity/eu/ie/procurement'
import { EUILProcurement } from '../entity/eu/il/procurement'
import { EUINProcurement } from '../entity/eu/in/procurement'
import { EUISProcurement } from '../entity/eu/is/procurement'
import { EUITProcurement } from '../entity/eu/it/procurement'
import { EUKEProcurement } from '../entity/eu/ke/procurement'
import { EUKGProcurement } from '../entity/eu/kg/procurement'
import { EUKNProcurement } from '../entity/eu/kn/procurement'
import { EUKZProcurement } from '../entity/eu/kz/procurement'
import { EULBProcurement } from '../entity/eu/lb/procurement'
import { EULIProcurement } from '../entity/eu/li/procurement'
import { EULTProcurement } from '../entity/eu/lt/procurement'
import { EULUProcurement } from '../entity/eu/lu/procurement'
import { EULVProcurement } from '../entity/eu/lv/procurement'
import { EUMAProcurement } from '../entity/eu/ma/procurement'
import { EUMDProcurement } from '../entity/eu/md/procurement'
import { EUMEProcurement } from '../entity/eu/me/procurement'
import { EUMGProcurement } from '../entity/eu/mg/procurement'
import { EUMKProcurement } from '../entity/eu/mk/procurement'
import { EUMLProcurement } from '../entity/eu/ml/procurement'
import { EUMQProcurement } from '../entity/eu/mq/procurement'
import { EUMTProcurement } from '../entity/eu/mt/procurement'
import { EUMWProcurement } from '../entity/eu/mw/procurement'
import { EUNIProcurement } from '../entity/eu/ni/procurement'
import { EUNLProcurement } from '../entity/eu/nl/procurement'
import { EUNOProcurement } from '../entity/eu/no/procurement'
import { EUNPProcurement } from '../entity/eu/np/procurement'
import { EUPLProcurement } from '../entity/eu/pl/procurement'
import { EUPSProcurement } from '../entity/eu/ps/procurement'
import { EUPTProcurement } from '../entity/eu/pt/procurement'
import { EUPYProcurement } from '../entity/eu/py/procurement'
import { EUREProcurement } from '../entity/eu/re/procurement'
import { EUROProcurement } from '../entity/eu/ro/procurement'
import { EURSProcurement } from '../entity/eu/rs/procurement'
import { EURUProcurement } from '../entity/eu/ru/procurement'
import { EUSEProcurement } from '../entity/eu/se/procurement'
import { EUSIProcurement } from '../entity/eu/si/procurement'
import { EUSKProcurement } from '../entity/eu/sk/procurement'
import { EUSLProcurement } from '../entity/eu/sl/procurement'
import { EUSNProcurement } from '../entity/eu/sn/procurement'
import { EUTDProcurement } from '../entity/eu/td/procurement'
import { EUTHProcurement } from '../entity/eu/th/procurement'
import { EUTJProcurement } from '../entity/eu/tj/procurement'
import { EUTNProcurement } from '../entity/eu/tn/procurement'
import { EUTRProcurement } from '../entity/eu/tr/procurement'
import { EUTZProcurement } from '../entity/eu/tz/procurement'
import { EUUAProcurement } from '../entity/eu/ua/procurement'
import { EUUGProcurement } from '../entity/eu/ug/procurement'
import { EUUKProcurement } from '../entity/eu/uk/procurement'
import { EUUSProcurement } from '../entity/eu/us/procurement'
import { EUYTProcurement } from '../entity/eu/yt/procurement'
import { EUZMProcurement } from '../entity/eu/zm/procurement'

// For the most part, these country codes follow ISO 3166, but Kosovo strangely
// seems to be modified to "1A".
export const CountryCodes = [
  '1a',
  'al',
  'am',
  'ar',
  'at',
  'ba',
  'bd',
  'be',
  'bg',
  'bj',
  'bo',
  'by',
  'cg',
  'ch',
  'ci',
  'cm',
  'cn',
  'co',
  'cy',
  'cz',
  'de',
  'dk',
  'dz',
  'ec',
  'ee',
  'eg',
  'es',
  'et',
  'fi',
  'fr',
  'ge',
  'gn',
  'gp',
  'gr',
  'hr',
  'ht',
  'hu',
  'ie',
  'il',
  'in',
  'is',
  'it',
  'ke',
  'kg',
  'kn',
  'kz',
  'la',
  'lb',
  'li',
  'lt',
  'lu',
  'lv',
  'ma',
  'md',
  'me',
  'mg',
  'mk',
  'ml',
  'mq',
  'mt',
  'mw',
  'ni',
  'nl',
  'no',
  'np',
  'pl',
  'ps',
  'pt',
  'py',
  're',
  'ro',
  'rs',
  'ru',
  'se',
  'si',
  'sk',
  'sl',
  'sn',
  'td',
  'th',
  'tj',
  'tn',
  'tr',
  'tz',
  'ua',
  'ug',
  'uk',
  'us',
  'yt',
  'zm',
]

// Maps a two letter country code (ISO 3166-1 alpha-2) to its corresponding
// European Union contract filing repository object -- when it exists.
function getCountryRepository(countryCode: string) {
  let repoMap = {
    '1a': EU1AProcurement,
    al: EUALProcurement,
    am: EUAMProcurement,
    ar: EUARProcurement,
    at: EUATProcurement,
    ba: EUBAProcurement,
    bd: EUBDProcurement,
    be: EUBEProcurement,
    bg: EUBGProcurement,
    bj: EUBJProcurement,
    bo: EUBOProcurement,
    by: EUBYProcurement,
    cg: EUCGProcurement,
    ch: EUCHProcurement,
    ci: EUCIProcurement,
    cm: EUCMProcurement,
    cn: EUCNProcurement,
    co: EUCOProcurement,
    cy: EUCYProcurement,
    cz: EUCZProcurement,
    de: EUDEProcurement,
    dk: EUDKProcurement,
    dz: EUDZProcurement,
    ec: EUECProcurement,
    ee: EUEEProcurement,
    eg: EUEGProcurement,
    es: EUESProcurement,
    et: EUETProcurement,
    fi: EUFIProcurement,
    fr: EUFRProcurement,
    ge: EUGEProcurement,
    gn: EUGNProcurement,
    gp: EUGPProcurement,
    gr: EUGRProcurement,
    hr: EUHRProcurement,
    ht: EUHTProcurement,
    hu: EUHUProcurement,
    ie: EUIEProcurement,
    il: EUILProcurement,
    in: EUINProcurement,
    is: EUISProcurement,
    it: EUITProcurement,
    ke: EUKEProcurement,
    kg: EUKGProcurement,
    kn: EUKNProcurement,
    kz: EUKZProcurement,
    lb: EULBProcurement,
    li: EULIProcurement,
    lt: EULTProcurement,
    lu: EULUProcurement,
    lv: EULVProcurement,
    ma: EUMAProcurement,
    md: EUMDProcurement,
    me: EUMEProcurement,
    mg: EUMGProcurement,
    mk: EUMKProcurement,
    ml: EUMLProcurement,
    mq: EUMQProcurement,
    mt: EUMTProcurement,
    mw: EUMWProcurement,
    ni: EUNIProcurement,
    nl: EUNLProcurement,
    no: EUNOProcurement,
    np: EUNPProcurement,
    pl: EUPLProcurement,
    ps: EUPSProcurement,
    pt: EUPTProcurement,
    py: EUPYProcurement,
    re: EUREProcurement,
    ro: EUROProcurement,
    rs: EURSProcurement,
    ru: EURUProcurement,
    se: EUSEProcurement,
    si: EUSIProcurement,
    sk: EUSKProcurement,
    sl: EUSLProcurement,
    sn: EUSNProcurement,
    td: EUTDProcurement,
    th: EUTHProcurement,
    tj: EUTJProcurement,
    tn: EUTNProcurement,
    tr: EUTRProcurement,
    tz: EUTZProcurement,
    ua: EUUAProcurement,
    ug: EUUGProcurement,
    uk: EUUKProcurement,
    us: EUUSProcurement,
    yt: EUYTProcurement,
    zm: EUZMProcurement,
  }
  if (countryCode in repoMap) {
    return dataSource.getRepository(repoMap[countryCode])
  } else {
    return undefined
  }
}

function getLanguageCodes(countryCode: string): string[] {
  const nonEnglishLanguages = {
    '1a': ['sq', 'sr'],
    al: ['sq'],
    am: ['hy'],
    ar: ['es'],
    at: ['de', 'it'],
    ba: ['bs'],
    bd: ['bn'],
    be: ['fr'],
    bg: ['bg'],
    bj: ['fr'],
    bo: ['es'],
    by: ['ru'],
    cg: ['fr'],
    ch: ['de'],
    ci: ['fr'],
    cm: ['fr'],
    cn: ['cn'],
    co: ['es'],
    cy: ['el', 'tr'],
    cz: ['cs'],
    de: ['de', 'pl'],
    dk: ['da'],
    dz: ['ar'],
    ec: ['es'],
    ee: ['et'],
    eg: ['ar'],
    es: ['es'],
    et: ['am'],
    fi: ['fi'],
    fr: ['fr'],
    ge: ['ka'],
    gn: ['fr'],
    gp: ['fr'],
    gr: ['el'],
    hr: ['hr'],
    ht: ['fr'],
    hu: ['hu'],
    ie: ['ga'],
    il: ['he'],
    in: ['hi'],
    is: ['is'],
    it: ['it'],
    ke: [],
    kg: ['ru'],
    kn: [],
    kz: ['kk'],
    lb: ['ar'],
    li: ['de'],
    lt: ['lt'],
    lu: ['lu'],
    lv: ['de', 'fr', 'lb'],
    ma: ['ar'],
    md: ['ro'],
    me: ['cnr'],
    mg: ['fr'],
    mk: [],
    ml: ['fr'],
    mq: ['fr'],
    mt: [],
    mw: [],
    ni: ['es'],
    nl: ['nl'],
    no: [],
    np: ['ne'],
    pl: ['pl'],
    ps: ['ar'],
    pt: ['pt'],
    py: ['es'],
    re: ['fr'],
    ro: ['ro'],
    rs: ['ro'],
    ru: ['ru'],
    se: ['sv'],
    si: ['sl'],
    sk: ['sk'],
    sl: [],
    sn: ['fr'],
    td: ['fr'],
    th: ['th'],
    tj: ['tg'],
    tn: ['aeb'],
    tr: ['tr'],
    tz: [],
    ua: ['uk'],
    ug: [],
    uk: [],
    yt: ['fr'],
    zm: [],
  }
  let langCodes = ['en']
  if (countryCode in nonEnglishLanguages) {
    langCodes = langCodes.concat(nonEnglishLanguages[countryCode])
  }
  return langCodes
}

function getLanguageConfig(languageCode: string): string {
  const langConfigMap = {
    cs: 'simple',
    da: 'danish',
    de: 'german',
    en: 'english',
    es: 'spanish',
    fr: 'french',
    hu: 'hungarian',
    it: 'italian',
    nl: 'dutch',
    no: 'norwegian',
    pl: 'simple',
    pt: 'portuguese',
    ro: 'romanian',
    ru: 'russian',
    sv: 'swedish',
    tr: 'turkish',
  }
  // For now, we are using the 'simple' configuration instead of
  // language-specific (when they exist) stemming rules.
  /*
  if (languageCode in langConfigMap) {
    return langConfigMap[languageCode]
  } else {
    return 'simple'
  }
  */
  return 'simple'
}

function getTableName(countryCode: string): string {
  return `eu_${countryCode}_contract_filing`
}

function getDatasetName(countryCode: string): string {
  return `eu_${countryCode.toLowerCase()}_procurement`
}

function getAgencyNames(
  countryCode: string,
  name: string,
  entities: Map<string, Map<string, any>>
): string[] {
  let names = []
  if (!entities.has(name)) {
    return names
  }
  const profile = entities.get(name)
  const alternates = util.jsonGetFromPath(profile, [
    'feeds',
    'eu',
    countryCode,
    'procurement',
    'agencyNames',
  ])
  if (alternates != undefined) {
    names = alternates
  }
  const includes = util.jsonGetFromPath(profile, [
    'feeds',
    'eu',
    countryCode,
    'procurement',
    'agencyIncludes',
  ])
  if (includes != undefined) {
    names = names.concat(includes)
  }
  return names
}

// Set up the list of contract supplier names for the vendor. If subsidiaries
// were requested to be included, the same process is repeated for each
// descendant.
function expandNames(
  countryCode: string,
  entityList: string[],
  entities: Map<string, Map<string, any>>,
  entityAlternates: Map<string, string[]>
): string[] {
  const names = util.expandNames(entityList, entityAlternates, entities, [
    'feeds',
    'eu',
    countryCode,
    'procurement',
  ])
  return names
}

function nameTargetsFromNames(names: string[]): string[] {
  if (!names) {
    return []
  }
  return names.map((name) => `["${name.replace(/"/g, '\\"')}"]`)
}

function buildDateConstraint(dateRange: util.DateRange, params): string {
  let constraints = []
  if (dateRange.hasStart()) {
    constraints.push(`last_action_date >= $${params.length + 1}`)
    params.push(dateRange.start.toISOString())
  }
  if (dateRange.hasEnd()) {
    constraints.push(`first_action_date <= $${params.length + 1}`)
    params.push(dateRange.end.toISOString())
  }
  return constraints.join(' AND ')
}

function buildDateSort(descending = true): string {
  let sortStr = `ORDER BY COALESCE(
    to_char(last_action_date, 'yyyy-mm-dd'),
    data->'en'->'coded_data'->'notice_data'->>'no_doc_ojs'
  )`
  if (descending) {
    sortStr += ' DESC'
  } else {
    sortStr += ' ASC'
  }
  return sortStr
}

function buildSearchConstraint(webSearch, params): string {
  const fullTextQuery = function () {
    const query = `
      tsvector_col
      @@ websearch_to_tsquery('simple', $${params.length + 1})`
    return query
  }

  let queryStr = ' ('
  queryStr += fullTextQuery()
  queryStr += ') '
  params.push(webSearch)

  return queryStr
}

async function haveFromSearch(
  countryCode: string,
  webSearch: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const searchConstraint = buildSearchConstraint(webSearch, params)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `
    SELECT 1 FROM ${getTableName(countryCode)}
    WHERE ${constraintsStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveFromTagSearch(
  countryCode: string,
  webSearch: string,
  token: string
) {
  const dataset = getDatasetName(countryCode)
  return await tags.haveFromTagSearch(webSearch, token, dataset)
}

async function haveForNames(
  countryCode: string,
  names: string[],
  dateRange: util.DateRange,
  entities
): Promise<boolean> {
  if (
    util.canAssumeRecordsExist(names, dateRange, entities, [
      'feeds',
      'eu',
      countryCode,
      'procurement',
    ])
  ) {
    return true
  }

  let params = []

  const nameTargets = nameTargetsFromNames(names)
  const searchConstraint = `entities @> ANY(cast($${
    params.length + 1
  } as jsonb[]))`
  params.push(nameTargets)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `
    SELECT 1 FROM ${getTableName(countryCode)}
    WHERE ${constraintsStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForAgencies(
  countryCode: string,
  names: string[],
  dateRange: util.DateRange,
  entities
): Promise<boolean> {
  if (
    util.canAssumeAgencyRecordsExist(names, dateRange, entities, [
      'feeds',
      'eu',
      countryCode,
      'procurement',
    ])
  ) {
    return true
  }

  let params = []

  const nameTargets = nameTargetsFromNames(names)
  const searchConstraint = `entities @> ANY(cast($${
    params.length + 1
  } as jsonb[]))`
  params.push(nameTargets)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `
    SELECT 1 FROM ${getTableName(countryCode)}
    WHERE ${constraintsStr} LIMIT 10000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveForName(
  countryCode: string,
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>
): Promise<boolean> {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(
    countryCode,
    names,
    entities,
    entityAlternates
  )
  return await haveForNames(countryCode, expandedNames, dateRange, entities)
}

async function haveForAgency(
  countryCode: string,
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>
): Promise<boolean> {
  const names = getAgencyNames(countryCode, name, entities)
  return await haveForAgencies(countryCode, names, dateRange, entities)
}

function annotateFiling(
  countryCode: string,
  filing,
  entities: Map<string, Map<string, any>>,
  normalization: Map<string, string>,
  agencyNormalization: Map<string, string>
): void {
  let annotation = {}

  // Normalize the entity names.
  let entityAnnotations = {}
  for (const entity of filing.entities) {
    const name = util.normalizeEntity(
      entity,
      agencyNormalization.has(entity) ? agencyNormalization : normalization
    )
    entityAnnotations[entity] = {
      origText: entity,
      text: name,
      stylizedText: util.getFullyStylizedName(name, entities),
      logo: util.logoURLFromAncestors(name, entities),
    }
  }
  annotation['entities'] = entityAnnotations

  filing['annotation'] = annotation
}

function annotate(
  countryCode: string,
  result,
  entities: Map<string, Map<string, any>>,
  normalization: Map<string, string>,
  agencyNormalization: Map<string, string>
): void {
  for (let filing of result.filings) {
    annotateFiling(
      countryCode,
      filing,
      entities,
      normalization,
      agencyNormalization
    )
  }
}

async function getOverDateRange(
  countryCode: string,
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraintStr = dateConstraint ? `WHERE ${dateConstraint}` : ''
  const dateSort = buildDateSort()

  const queryStr =
    `SELECT * FROM ${getTableName(countryCode)} ` +
    `${constraintStr} ${dateSort} LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getFromSearch(
  countryCode: string,
  webSearch: string,
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  let params = []

  const searchConstraint = `tsvector_col @@ websearch_to_tsquery('simple', $${
    params.length + 1
  })`
  params.push(webSearch)

  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  const dateSort = buildDateSort()

  const queryStr =
    `SELECT * FROM ${getTableName(countryCode)} ` +
    `WHERE ${constraintsStr} ${dateSort} LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getFromTagSearch(
  countryCode: string,
  webSearch: string,
  token: string,
  maxSearchResults: number
) {
  const dataset = getDatasetName(countryCode)
  const tableName = getTableName(countryCode)
  const uniqueKeys = ['doc_id']
  return await tags.getFromTagSearch(
    webSearch,
    token,
    maxSearchResults,
    tableName,
    dataset,
    uniqueKeys
  )
}

async function getForNames(
  countryCode: string,
  names: string[],
  dateRange: util.DateRange,
  maxSearchResults: number
) {
  let params = []

  const nameTargets = nameTargetsFromNames(names)
  const searchConstraint = `entities @> ANY(cast($${
    params.length + 1
  } as jsonb[]))`
  params.push(nameTargets)
  const dateConstraint = buildDateConstraint(dateRange, params)
  const constraints = [searchConstraint, dateConstraint].filter((e) => e)
  const constraintsStr = constraints.join(' AND ')

  const dateSort = buildDateSort()

  const queryStr =
    `SELECT * FROM ${getTableName(countryCode)} ` +
    `WHERE ${constraintsStr} ${dateSort} LIMIT $${params.length + 1}`
  params.push(maxSearchResults)

  return await util.getQueryFilings(
    dataSource.manager,
    queryStr,
    params,
    maxSearchResults
  )
}

async function getForName(
  countryCode: string,
  name: string,
  excludeSubsidiaries: boolean,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  entityAlternates: Map<string, string[]>,
  maxSearchResults: number
) {
  const names = util.expandToEntitySubtree(
    name,
    excludeSubsidiaries,
    entityChildren
  )
  const expandedNames = expandNames(
    countryCode,
    names,
    entities,
    entityAlternates
  )
  return await getForNames(
    countryCode,
    expandedNames,
    dateRange,
    maxSearchResults
  )
}

async function getForAgency(
  countryCode: string,
  name: string,
  dateRange: util.DateRange,
  entities: Map<string, Map<string, any>>,
  maxSearchResults: number
) {
  const names = getAgencyNames(countryCode, name, entities)
  return await getForNames(countryCode, names, dateRange, maxSearchResults)
}

async function haveOverDateRange(
  countryCode: string,
  dateRange: util.DateRange
): Promise<boolean> {
  let params = []

  const dateConstraint = buildDateConstraint(dateRange, params)
  const whereStr = dateConstraint ? `WHERE ${dateConstraint}` : ''

  // We use a `SELECT 1` instead of a `SELECT EXISTS` due to Postgres index
  // selection oddities.
  const queryStr = `SELECT 1 FROM ${getTableName(
    countryCode
  )} ${whereStr} LIMIT 1000`

  return await util.nonemptyQuery(dataSource.manager, queryStr, params)
}

async function haveAPI(
  request,
  response,
  countryCode: string,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  let result = false
  if (text) {
    console.log(
      `Boolean EU ${countryCode.toUpperCase()} contracts w/ text: ${text}`
    )
    result = await haveFromSearch(countryCode, text, dateRange)
  } else if (tagText) {
    console.log(
      `Boolean EU ${countryCode.toUpperCase()} contracts w/ tag: ${tagText}`
    )
    result = await haveFromTagSearch(countryCode, tagText, token)
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(
      `Boolean EU ${countryCode.toUpperCase()} Contracts w/ entity: ${name}`
    )
    result = await haveForName(
      countryCode,
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(
      `Boolean EU ${countryCode.toUpperCase()} contracts w/ agency: ${name}`
    )
    result = await haveForAgency(countryCode, name, dateRange, entities)
  } else {
    console.log(
      `Boolean EU ${countryCode.toUpperCase()} Contracts over date range`
    )
    result = await haveOverDateRange(countryCode, dateRange)
  }

  util.setJSONResponse(response, result)
}

async function getAPI(
  request,
  response,
  countryCode: string,
  entities: Map<string, Map<string, any>>,
  entityChildren: Map<string, string[]>,
  feedInfo: FeedInfo
) {
  const params = request.query

  const dateRange = util.getDateRange(params)
  const text = util.readWebSearchQuery(params.text)
  const tagText = util.readWebSearchQuery(params.tagText)
  const token = params.token

  const maxSearchResultsDefault = 2000
  const maxSearchResultsCap = 10000
  const maxSearchResults = util.readBoundedPositiveNumber(
    params.maxSearchResults,
    maxSearchResultsDefault,
    maxSearchResultsCap,
    'maxSearchResults'
  )

  let result = { filings: [], hitSearchCap: false }
  if (text) {
    console.log(`EU ${countryCode.toUpperCase()} Contracts w/ text: ${text}`)
    result = await getFromSearch(countryCode, text, dateRange, maxSearchResults)
  } else if (tagText) {
    console.log(`EU ${countryCode.toUpperCase()} contracts w/ tag: ${tagText}`)
    result = await getFromTagSearch(
      countryCode,
      tagText,
      token,
      maxSearchResults
    )
  } else if (params.entity) {
    const name = util.canonicalText(params.entity)
    const excludeSubsidiaries = util.readBoolean(params.excludeSubsidiaries)
    console.log(`EU ${countryCode.toUpperCase()} Contracts w/ entity: ${name}`)
    result = await getForName(
      countryCode,
      name,
      excludeSubsidiaries,
      dateRange,
      entities,
      entityChildren,
      feedInfo.names,
      maxSearchResults
    )
  } else if (params.agency) {
    const name = util.canonicalText(params.agency)
    console.log(`EU ${countryCode.toUpperCase()} contracts w/ agency: ${name}`)
    result = await getForAgency(
      countryCode,
      name,
      dateRange,
      entities,
      maxSearchResults
    )
  } else {
    console.log(`EU ${countryCode.toUpperCase()} Contracts over date range`)
    result = await getOverDateRange(countryCode, dateRange, maxSearchResults)
  }

  if (!util.readBoolean(params.disableAnnotations)) {
    annotate(
      countryCode,
      result,
      entities,
      feedInfo.normalization,
      feedInfo.agencyNormalization
    )
  }

  util.setJSONResponse(response, result)
}

export function setRoutes(router, entityState) {
  const entities = entityState.entities
  const children = entityState.children

  for (const countryCode of CountryCodes) {
    const route = `/api/eu/${countryCode}/procurement`

    const feed = entityState.feeds.get('eu').get(countryCode).get('procurement')

    // Returns whether there are country contracts for a search or an entity.
    router.get(`${route}/have`, async function (request, response) {
      await haveAPI(request, response, countryCode, entities, children, feed)
    })

    // Returns JSON for country contracts for a web search or an entity.
    router.get(`${route}/get`, async function (request, response) {
      await getAPI(request, response, countryCode, entities, children, feed)
    })

    const dataset = getDatasetName(countryCode)

    router.post(`${route}/getTags`, async function (request, response) {
      return await tags.handleGetTags(request, response, dataset)
    })

    router.post(`${route}/addTag`, async function (request, response) {
      return await tags.handleAddTag(request, response, dataset)
    })

    router.post(`${route}/deleteTag`, async function (request, response) {
      return await tags.handleDeleteTag(request, response, dataset)
    })
  }
}
