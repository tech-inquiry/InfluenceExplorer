module.exports = {
  apps: [
    {
      watch: false,
      autorestart: true,
      name: 'explorer',
      script: 'node_modules/.bin/ts-node',
      args: '-T ./src/index.ts',
      automation: false, //https://stackoverflow.com/a/35764114/5812448s
      wait_ready: true,
      exec_mode: 'cluster',
      out_file: '/dev/null',
      error_file: '/dev/null',
      instances: 1,
      listen_timeout: 60000,
      kill_timeout: 3000,
      exp_backoff_restart_delay: 200,
      stop_exit_codes: [0],
    },
  ],
}
