function TwitterList(profile, children, twitterType) {
  let twitter = {}
  if (
    profile !== undefined &&
    'twitter' in profile &&
    twitterType in profile.twitter
  ) {
    for (const [handle, name] of Object.entries(profile.twitter[twitterType])) {
      twitter[handle] = name
    }
  }
  for (const child of children) {
    if (!('twitter' in child) || !(twitterType in child.twitter)) {
      continue
    }
    for (const [handle, name] of Object.entries(child.twitter[twitterType])) {
      twitter[handle] = name
    }
  }
  return twitter
}

function TwitterCommunity(profile, children) {
  return TwitterList(profile, children, 'community')
}

function TwitterInstitutional(profile, children) {
  return TwitterList(profile, children, 'institutional')
}

function HaveTweets(profile, children) {
  const twitterComm = TwitterCommunity(profile, children)
  const twitterInst = TwitterInstitutional(profile, children)
  return (
    Object.entries(twitterComm).length || Object.entries(twitterInst).length
  )
}

function TwitterFeedURL(handle) {
  return `https://twitter.com/${handle}?ref_src=twsrc%5Etfw`
}

function TwitterTab(tabsState, profile, children) {
  const label = 'twitter'
  const tabLogo = '/logos/twitter_inc.svg'
  const tabTitle = 'Twitter Feeds'
  NavTab(tabsState.navID(), label, tabLogo, tabTitle)

  let tab = $('<div>')
    .addClass('content')
    .attr('id', `filing-tab-${label}`)
    .css('display', 'none')
    .css('padding-top', '0')
    .css('padding-left', '0')
  $(`#${tabsState.id()}`).append(tab)

  let tabs = $('<div>').addClass('filing_tabs').attr('id', `${label}-tabs`)
  tab.append(tabs)

  let nav = $('<ul>').addClass('nav nav-tabs').css('font-size', '175%')
  tabs.append(nav)
  for (const [handle, name] of Object.entries(
    TwitterCommunity(profile, children)
  )) {
    const tabID = `${label}-tab-${handle}`

    // Add the nav link.
    let li = $('<li>').addClass('nav-item')
    nav.append(li)
    li.append(
      $('<a>')
        .addClass('nav-link')
        .attr('href', `#${tabID}`)
        .text(`Community: ${handle}`)
    )

    // Add the feed itself.
    const feedURL = TwitterFeedURL(handle)
    let feed = $('<div>')
      .addClass('content')
      .attr('id', tabID)
      .css('display', 'none')
      .css('font-size', '100%')
    tabs.append(feed)
    feed.append(
      $('<div>')
        .addClass('twitter-timeline-wrap')
        .append(
          $('<a>')
            .addClass('twitter-timeline')
            .attr('data-align', 'center')
            .attr('data-width', '800')
            .attr('href', feedURL)
            .text(`Community Tweets by ${name}`)
        )
    )
  }
  for (const [handle, name] of Object.entries(
    TwitterInstitutional(profile, children)
  )) {
    const tabID = `${label}-tab-${handle}`

    // Add the nav link.
    let li = $('<li>').addClass('nav-item')
    nav.append(li)
    li.append(
      $('<a>')
        .addClass('nav-link')
        .attr('href', `#${tabID}`)
        .text(`Institutional: ${handle}`)
    )

    // Add the feed itself.
    const feedURL = TwitterFeedURL(handle)
    let feed = $('<div>')
      .addClass('content')
      .attr('id', tabID)
      .css('display', 'none')
      .css('font-size', '100%')
    tabs.append(feed)
    feed.append(
      $('<div>')
        .addClass('twitter-timeline-wrap')
        .append(
          $('<a>')
            .addClass('twitter-timeline')
            .attr('data-align', 'center')
            .attr('data-width', '800')
            .attr('href', feedURL)
            .text(`Institutional Tweets by ${name}`)
        )
    )
  }

  const id = `${label}-tabs`
  tabsState.addTab({ id: id })

  // Activate the Twitter feed embeddings.
  //
  // See https://developer.twitter.com/en/docs/twitter-for-websites/javascript-api/guides/set-up-twitter-for-websites
  window.twttr = (function (d, s, id) {
    var js,
      fjs = d.getElementsByTagName(s)[0],
      t = window.twttr || {}
    if (d.getElementById(id)) {
      return t
    }
    js = d.createElement(s)
    js.id = id
    js.src = 'https://platform.twitter.com/widgets.js'
    fjs.parentNode.insertBefore(js, fjs)

    t._e = []
    t.ready = function (f) {
      t._e.push(f)
    }

    return t
  })(document, 'script', 'twitter-wjs')
}
