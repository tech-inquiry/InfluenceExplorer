function accordionItem(
  USE_BOOTSTRAP_5,
  parentID,
  headerID,
  collapseID,
  buttonHTML,
  expanded = false,
  buttonColor = undefined
) {
  const itemClass = USE_BOOTSTRAP_5 ? 'accordion-item' : 'card'
  const headerClass = USE_BOOTSTRAP_5 ? 'accordion-header' : 'card-header'
  const bodyClass = USE_BOOTSTRAP_5 ? 'accordion-body' : 'card-body'
  let item = $('<div>').addClass(itemClass)

  let buttonClass = USE_BOOTSTRAP_5
    ? 'accordion-button'
    : 'btn btn-link btn-block text-left'
  let collapseClass = USE_BOOTSTRAP_5
    ? 'accordion-collapse collapse'
    : 'collapse'
  if (expanded) {
    collapseClass += ' show'
  } else {
    buttonClass += ' collapsed'
  }

  let buttonCSS = {
    'font-size': '100%',
  }
  if (buttonColor) {
    buttonCSS['background-color'] = buttonColor
  }

  let headerButton = $('<button>')
    .addClass(buttonClass)
    .attr('type', 'button')
    .attr('aria-expanded', expanded)
    .attr('aria-controls', collapseID)
  if (USE_BOOTSTRAP_5) {
    headerButton
      .attr('data-bs-toggle', 'collapse')
      .attr('data-bs-target', `#${collapseID}`)
  } else {
    headerButton
      .attr('data-toggle', 'collapse')
      .attr('data-target', `#${collapseID}`)
  }
  for (const [key, value] of Object.entries(buttonCSS)) {
    headerButton.css(key, value)
  }
  headerButton.html(buttonHTML)
  const headerContent = USE_BOOTSTRAP_5
    ? headerButton
    : $('<h3>').addClass('mb-0').html(headerButton)

  item.append(
    $('<h3>').addClass(headerClass).attr('id', headerID).html(headerContent)
  )
  let collapse = $('<div>')
    .attr('id', collapseID)
    .addClass(collapseClass)
    .attr('aria-labelledby', headerID)
  if (USE_BOOTSTRAP_5) {
    collapse.attr('data-bs-parent', `#${parentID}`)
  } else {
    collapse.attr('data-parent', `#${parentID}`)
  }

  item.append(collapse)
  let collapseBody = $('<div>').addClass(bodyClass)
  collapse.append(collapseBody)

  return { item: item, collapseBody: collapseBody }
}

function appendModalSimpleFeature(
  parentHTML,
  title,
  html,
  secondaryGuard = false,
  guard = undefined,
  buttonColor = undefined
) {
  if (secondaryGuard) {
    if (!guard) {
      return
    }
  } else {
    if (!html) {
      return
    }
  }

  let headerCSS = {}
  if (buttonColor) {
    headerCSS['background-color'] = buttonColor
  }

  let card = $('<div>').addClass('card').css('margin', '1em 0')
  parentHTML.append(card)
  if (title) {
    let titleDiv = $('<div>')
      .addClass('card-header modal-card-header')
      .html(title)
    for (const [key, value] of Object.entries(headerCSS)) {
      titleDiv.css(key, value)
    }
    card.append(titleDiv)
  }
  let cardBody = $('<div>').addClass('card-body')
  cardBody.append($('<p>').addClass('card-text modal-card-text').html(html))
  card.append(cardBody)
}

function modalHeader(title, titleID) {
  let header = $('<div>').addClass('modal-header')
  header.append(
    $('<h3>').addClass('modal-title').attr('id', titleID).html(title)
  )
  let headerButton = $('<button>')
    .attr('type', 'button')
    .attr('aria-label', 'close')
    .html($('<span>').attr('aria-hidden', 'true').html('&times;'))
  if (USE_BOOTSTRAP_5) {
    headerButton.addClass('btn-close').attr('data-bs-dismiss', 'modal')
  } else {
    headerButton.addClass('close').attr('data-dismiss', 'modal')
  }
  header.append(headerButton)
  return header
}

function modalBody() {
  return $('<div>').addClass('modal-body')
}

function modalFooter() {
  let footer = $('<div>').addClass('modal-footer')
  let footerButton = $('<button>')
    .addClass('btn btn-primary')
    .css('font-size', '200%')
    .text('Close')
  if (USE_BOOTSTRAP_5) {
    footerButton.attr('data-bs-dismiss', 'modal')
  } else {
    footerButton.attr('data-dismiss', 'modal')
  }
  footer.append(footerButton)
  return footer
}

class Accordion {
  constructor(
    parentHTML,
    parentID,
    titleHTML,
    expanded,
    label,
    labelIndex = undefined,
    titleColor = undefined
  ) {
    this.headerID_ =
      labelIndex == undefined
        ? `${label}Header`
        : `${label}Header-${labelIndex}`
    this.collapseID_ =
      labelIndex == undefined
        ? `${label}Collapse`
        : `${label}Collapse-${labelIndex}`
    this.accordion_ = accordionItem(
      USE_BOOTSTRAP_5,
      parentID,
      this.headerID_,
      this.collapseID_,
      titleHTML,
      expanded,
      titleColor
    )
    parentHTML.append(this.accordion_['item'])
  }

  item() {
    return this.accordion_['item']
  }
  collapseBody() {
    return this.accordion_['collapseBody']
  }
  collapseID() {
    return this.collapseID_
  }
  headerID() {
    return this.headerID_
  }

  appendFeature(title, html, useGuard = false, guard = undefined) {
    appendModalSimpleFeature(
      this.accordion_['collapseBody'],
      title,
      html,
      useGuard,
      guard
    )
  }

  appendDate(title, dateObj) {
    this.appendFeature(title, dateWithoutTime(new Date(dateObj)), true, dateObj)
  }

  appendPeriod(title, startObj, endObj) {
    this.appendFeature(
      title,
      dateWithoutTime(new Date(startObj)) +
        ' - ' +
        dateWithoutTime(new Date(endObj))
    )
  }

  appendPhone(title, phoneStr) {
    this.appendFeature(title, formatPhoneNumber(phoneStr), true, phoneStr)
  }

  appendURL(title, url, text = undefined) {
    if (text) {
      this.appendFeature(title, $('<a>').attr('href', url).text(text))
    } else {
      this.appendFeature(title, $('<a>').attr('href', url).text(url), true, url)
    }
  }
}
