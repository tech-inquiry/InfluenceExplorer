class TabsState {
  constructor(idVal, navIDVal) {
    this.id_ = idVal
    this.navID_ = navIDVal
    this.tabs_ = []
  }

  id() {
    return this.id_
  }
  navID() {
    return this.navID_
  }

  addTab(tab) {
    this.tabs_.push(tab)
  }

  render() {
    this.tabs_.forEach(function (tab, index) {
      if ('id' in tab && 'render' in tab) {
        let tabObj = $(`#${tab.id}`)
        tabObj.tabs({ activate: tab.render })
      } else if ('id' in tab) {
        let tabObj = $(`#${tab.id}`)
        tabObj.tabs()
      }
    })

    const tabsID = this.id_
    const tabList = this.tabs_
    console.log(`Starting up tabs with ID ${tabsID}`)
    $(`#${tabsID}`).tabs({
      create: function (event, ui) {
        const activeTabIndex = $(`#${tabsID}`).tabs('option', 'active')
        const activeTab = tabList[activeTabIndex]
        if (activeTab && 'create' in activeTab) {
          console.log(`Creating tab ${activeTab.id}`)
          activeTab.create(event, ui)
        }
        if (activeTab && 'render' in activeTab) {
          console.log(`Rendering tab ${activeTab.id}`)
          activeTab.render(event, ui)
        }
      },
      activate: function (event, ui) {
        const activeTabIndex = $(`#${tabsID}`).tabs('option', 'active')
        const activeTab = tabList[activeTabIndex]
        if ('render' in activeTab) {
          console.log(`Rendering tab ${activeTab.id}`)
          activeTab.render(event, ui)
        }
      },
    })
  }
}
