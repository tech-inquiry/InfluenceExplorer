function FinancialsChart(
  parentID,
  financialProfile,
  chartSubject,
  relativeParentID
) {
  const maxLabels = 30

  const minBarHeight = 4
  const maxBarWidth = 120

  function fillAmountArray(unprocessedArray) {
    let array = []
    if (unprocessedArray == undefined) {
      return array
    }
    unprocessedArray.forEach(function (item) {
      array.push({
        label: item.year,
        dateReported: item.dateReported,
        amount: parseFloat(item.amount.replace(/,/g, '')),
      })
    })
    return array
  }

  function getTCJAExceptions(TCJAExceptions) {
    if (TCJAExceptions == undefined) {
      return []
    }
    let array = []
    TCJAExceptions.forEach(function (item) {
      const year = item.year
      const benefit = parseFloat(item.totalBenefit.replace(/,/g, ''))
      array.push({
        label: year,
        amount: benefit,
      })
    })
    return array
  }

  function adjustForTCJA(origArray, TCJAExceptions) {
    if (origArray == [] || TCJAExceptions == undefined) {
      return []
    }

    let array = []
    origArray.forEach(function (item) {
      array.push({
        label: item.label,
        dateReported: item.dateReported,
        amount: item.amount,
      })
    })

    TCJAExceptions.forEach(function (item) {
      const year = item.label
      const benefit = parseFloat(item.totalBenefit.replace(/,/g, ''))
      array.forEach(function (itemAdjusted) {
        if (itemAdjusted.label == year) {
          itemAdjusted.amount += benefit
        }
      })
    })
    return array
  }

  function getMarketCaps(shares) {
    if (shares == undefined) {
      return [[], []]
    }
    let basicMarketCapDict = {}
    let dilutedMarketCapDict = {}
    for (var shareClass in shares) {
      shares[shareClass].forEach(function (item) {
        const year = item.year
        const basicCount = parseFloat(item.basicCount.replace(/,/g, ''))
        const dilutedCount = parseFloat(item.dilutedCount.replace(/,/g, ''))
        const price = parseFloat(item.price.replace(/,/g, ''))
        if (!(year in basicMarketCapDict)) {
          basicMarketCapDict[year] = 0
          dilutedMarketCapDict[year] = 0
        }
        basicMarketCapDict[year] += basicCount * price
        dilutedMarketCapDict[year] += dilutedCount * price
      })
    }
    let basicMarketCaps = []
    let dilutedMarketCaps = []
    for (var year in basicMarketCapDict) {
      basicMarketCaps.push({ label: year, amount: basicMarketCapDict[year] })
    }
    for (var year in dilutedMarketCapDict) {
      dilutedMarketCaps.push({
        label: year,
        amount: dilutedMarketCapDict[year],
      })
    }
    return [basicMarketCaps, dilutedMarketCaps]
  }

  const formatAmount = function (amount) {
    if (Math.abs(amount) > 1e12) {
      return (
        numberWithCommas((amount / 1e12).toFixed(2)).toString() + ' Trillion'
      )
    } else if (Math.abs(amount) > 1e9) {
      return numberWithCommas((amount / 1e9).toFixed(2)).toString() + ' Billion'
    } else if (Math.abs(amount) > 1e6) {
      return numberWithCommas((amount / 1e6).toFixed(2)).toString() + ' Million'
    } else {
      return numberWithCommas(amount).toString()
    }
  }

  let revenue = fillAmountArray(financialProfile.revenue)
  let grossIncome = fillAmountArray(financialProfile.gross_income)
  let primaryOperatingIncome = fillAmountArray(
    financialProfile.primaryOperatingIncome
  )
  let operatingIncome = fillAmountArray(financialProfile.operatingIncome)
  let primaryIncomeBeforeIncomeTaxes = fillAmountArray(
    financialProfile.primaryIncomeBeforeIncomeTaxes
  )
  let incomeBeforeIncomeTaxes = fillAmountArray(
    financialProfile.incomeBeforeIncomeTaxes
  )
  let TCJAExceptions = getTCJAExceptions(financialProfile.TCJAExceptions)
  let primaryProvisionForIncomeTaxes = fillAmountArray(
    financialProfile.primaryProvisionForIncomeTaxes
  )
  let primaryProvisionForIncomeTaxesMinusTCJA = adjustForTCJA(
    primaryProvisionForIncomeTaxes,
    financialProfile.TCJAExceptions
  )
  let provisionForIncomeTaxes = fillAmountArray(
    financialProfile.provisionForIncomeTaxes
  )
  let provisionForIncomeTaxesMinusTCJA = adjustForTCJA(
    provisionForIncomeTaxes,
    financialProfile.TCJAExceptions
  )
  let primaryNetIncome = fillAmountArray(financialProfile.primaryNetIncome)
  let netIncome = fillAmountArray(financialProfile.netIncome)
  let marketCaps = getMarketCaps(financialProfile.shares)

  // A quadratic algorithm is okay even though we can do much better given that
  // we know our data is already sorted...
  let effectiveTaxRate = []
  let effectiveTaxRateMinusTCJA = []
  let primaryEffectiveTaxRate = []
  let primaryEffectiveTaxRateMinusTCJA = []
  let totalIncomeBeforeIncomeTaxes = 0
  let totalPrimaryIncomeBeforeIncomeTaxes = 0
  let totalProvisionForIncomeTaxes = 0
  let totalProvisionForIncomeTaxesMinusTCJA = 0
  let totalPrimaryProvisionForIncomeTaxes = 0
  let totalPrimaryProvisionForIncomeTaxesMinusTCJA = 0
  let firstYearForEffectiveTaxRate = undefined
  let lastYearForEffectiveTaxRate = undefined
  let firstYearForPrimaryEffectiveTaxRate = undefined
  let lastYearForPrimaryEffectiveTaxRate = undefined
  if (incomeBeforeIncomeTaxes.length) {
    let firstYearInt = undefined
    for (var i = incomeBeforeIncomeTaxes.length - 1; i >= 0; i--) {
      const ibitItem = incomeBeforeIncomeTaxes[i]
      for (var j = provisionForIncomeTaxes.length - 1; j >= 0; j--) {
        const pfitItem = provisionForIncomeTaxes[j]
        if (pfitItem.label == ibitItem.label) {
          // Incorporate the unadjusted amount
          const ibitItemYearInt = parseInt(ibitItem.label)
          if (lastYearForEffectiveTaxRate == undefined) {
            lastYearForEffectiveTaxRate = ibitItem.label
          }
          if (
            firstYearForEffectiveTaxRate == undefined ||
            ibitItemYearInt == firstYearInt - 1
          ) {
            firstYearForEffectiveTaxRate = ibitItem.label
            firstYearInt = ibitItemYearInt
          }
          totalIncomeBeforeIncomeTaxes += ibitItem.amount
          totalProvisionForIncomeTaxes += pfitItem.amount
          effectiveTaxRate.unshift({
            label: ibitItem.label,
            dateReported: ibitItem.dateReported,
            amount: (pfitItem.amount / ibitItem.amount) * 100,
          })

          for (
            var k = provisionForIncomeTaxesMinusTCJA.length - 1;
            k >= 0;
            k--
          ) {
            const pfitTCJAItem = provisionForIncomeTaxesMinusTCJA[k]
            if (pfitTCJAItem.label == ibitItem.label) {
              totalProvisionForIncomeTaxesMinusTCJA += pfitTCJAItem.amount
              effectiveTaxRateMinusTCJA.unshift({
                label: ibitItem.label,
                dateReported: ibitItem.dateReported,
                amount: (pfitTCJAItem.amount / ibitItem.amount) * 100,
              })
            }
          }

          break
        }
      }
    }
  }
  if (primaryIncomeBeforeIncomeTaxes.length) {
    let firstYearInt = undefined
    for (var i = primaryIncomeBeforeIncomeTaxes.length - 1; i >= 0; i--) {
      const ibitItem = primaryIncomeBeforeIncomeTaxes[i]
      for (var j = primaryProvisionForIncomeTaxes.length - 1; j >= 0; j--) {
        const pfitItem = primaryProvisionForIncomeTaxes[j]
        if (ibitItem.label == pfitItem.label) {
          const ibitItemYearInt = parseInt(ibitItem.label)
          if (lastYearForPrimaryEffectiveTaxRate == undefined) {
            lastYearForPrimaryEffectiveTaxRate = ibitItem.label
          }
          if (
            firstYearForPrimaryEffectiveTaxRate == undefined ||
            ibitItemYearInt == firstYearInt - 1
          ) {
            firstYearForPrimaryEffectiveTaxRate = ibitItem.label
            firstYearInt = ibitItemYearInt
          }
          totalPrimaryIncomeBeforeIncomeTaxes += ibitItem.amount
          totalPrimaryProvisionForIncomeTaxes += pfitItem.amount
          primaryEffectiveTaxRate.unshift({
            label: ibitItem.label,
            dateReported: ibitItem.dateReported,
            amount: (pfitItem.amount / ibitItem.amount) * 100,
          })

          for (
            var k = primaryProvisionForIncomeTaxesMinusTCJA.length - 1;
            k >= 0;
            k--
          ) {
            const pfitTCJAItem = primaryProvisionForIncomeTaxesMinusTCJA[k]
            if (pfitTCJAItem.label == ibitItem.label) {
              totalPrimaryProvisionForIncomeTaxesMinusTCJA +=
                pfitTCJAItem.amount
              primaryEffectiveTaxRateMinusTCJA.unshift({
                label: ibitItem.label,
                dateReported: ibitItem.dateReported,
                amount: (pfitTCJAItem.amount / ibitItem.amount) * 100,
              })
            }
          }

          break
        }
      }
    }
  }
  const totalEffectiveTaxRate =
    effectiveTaxRate.length > 0
      ? (totalProvisionForIncomeTaxes / totalIncomeBeforeIncomeTaxes) * 100
      : undefined
  const totalEffectiveTaxRateMinusTCJA =
    effectiveTaxRateMinusTCJA.length > 0
      ? (totalProvisionForIncomeTaxesMinusTCJA / totalIncomeBeforeIncomeTaxes) *
        100
      : undefined
  const totalPrimaryEffectiveTaxRate =
    primaryEffectiveTaxRate.length > 0
      ? (totalPrimaryProvisionForIncomeTaxes /
          totalPrimaryIncomeBeforeIncomeTaxes) *
        100
      : undefined
  const totalPrimaryEffectiveTaxRateMinusTCJA =
    primaryEffectiveTaxRateMinusTCJA.length > 0
      ? (totalPrimaryProvisionForIncomeTaxesMinusTCJA /
          totalPrimaryIncomeBeforeIncomeTaxes) *
        100
      : undefined

  sliceFromEnd(revenue, maxLabels)
  sliceFromEnd(grossIncome, maxLabels)
  sliceFromEnd(primaryOperatingIncome, maxLabels)
  sliceFromEnd(operatingIncome, maxLabels)
  sliceFromEnd(primaryIncomeBeforeIncomeTaxes, maxLabels)
  sliceFromEnd(incomeBeforeIncomeTaxes, maxLabels)
  sliceFromEnd(TCJAExceptions, maxLabels)
  sliceFromEnd(primaryProvisionForIncomeTaxes, maxLabels)
  sliceFromEnd(primaryProvisionForIncomeTaxesMinusTCJA, maxLabels)
  sliceFromEnd(provisionForIncomeTaxes, maxLabels)
  sliceFromEnd(provisionForIncomeTaxesMinusTCJA, maxLabels)
  sliceFromEnd(primaryNetIncome, maxLabels)
  sliceFromEnd(netIncome, maxLabels)
  sliceFromEnd(marketCaps[0], maxLabels)
  sliceFromEnd(marketCaps[1], maxLabels)
  sliceFromEnd(effectiveTaxRate, maxLabels)
  sliceFromEnd(effectiveTaxRateMinusTCJA, maxLabels)
  sliceFromEnd(primaryEffectiveTaxRate, maxLabels)
  sliceFromEnd(primaryEffectiveTaxRateMinusTCJA, maxLabels)

  const secondaryFromPrimary = function (primaryArray, array) {
    if (!primaryArray.length || !array.length) {
      return []
    }

    // We use a quadratic algorithm for now.
    let secondaryArray = []
    for (var i = 0; i < primaryArray.length; i++) {
      for (var j = 0; j < array.length; j++) {
        if (
          array[j].label == primaryArray[i].label &&
          array[j].dateReported == primaryArray[i].dateReported
        ) {
          secondaryArray.push({
            label: array[j].label,
            dateReported: array[j].dateReported,
            amount: array[j].amount - primaryArray[i].amount,
          })
        }
      }
    }

    return secondaryArray
  }
  let secondaryOperatingIncome = secondaryFromPrimary(
    primaryOperatingIncome,
    operatingIncome
  )
  let secondaryIncomeBeforeIncomeTaxes = secondaryFromPrimary(
    primaryIncomeBeforeIncomeTaxes,
    incomeBeforeIncomeTaxes
  )
  let secondaryProvisionForIncomeTaxes = secondaryFromPrimary(
    primaryProvisionForIncomeTaxes,
    provisionForIncomeTaxes
  )
  let secondaryNetIncome = secondaryFromPrimary(primaryNetIncome, netIncome)

  // Decide which class we will default to.
  let initialFinancialType = undefined
  if (primaryProvisionForIncomeTaxes.length > 0) {
    initialFinancialType = 'primaryProvisionForIncomeTaxes'
  } else if (provisionForIncomeTaxes.length > 0) {
    initialFinancialType = 'provisionForIncomeTaxes'
  } else if (revenue.length > 0) {
    initialFinancialType = 'revenue'
  } else if (grossIncome.length > 0) {
    initialFinancialType = 'grossIncome'
  } else if (incomeBeforeIncomeTaxes.length > 0) {
    initialFinancialType = 'incomeBeforeIncomeTaxes'
  } else if (operatingIncome.length > 0) {
    initialFinancialType = 'operatingIncome'
  } else if (netIncome.length > 0) {
    initialFinancialType = 'netIncome'
  }

  if (effectiveTaxRate.length || primaryEffectiveTaxRate.length) {
    let headerDiv = $('<div>')
      .css('text-align', 'center')
      .css('font-size', '175%')
      .css('margin-bottom', '2em')
    $(`#${parentID}`).append(headerDiv)
    if (primaryEffectiveTaxRate.length) {
      const primaryEffectiveTaxRatePeriodStr =
        'FY' +
        firstYearForPrimaryEffectiveTaxRate +
        '-' +
        lastYearForPrimaryEffectiveTaxRate

      let table = $('<table>').addClass('taxes')
      headerDiv.append(table)
      let thead = $('<thead>')
      table.append(thead)
      thead.append(
        $('<tr>').append(
          $('<th>').text(
            '(Primary) Effective Taxation over ' +
              primaryEffectiveTaxRatePeriodStr
          )
        )
      )
      let tbody = $('<tbody>')
      table.append(tbody)
      tbody
        .append(
          $('<tr>')
            .append(
              $('<td>')
                .css('font-weight', '700')
                .text('(Primary) Income before income taxes')
            )
            .append(
              $('<td>').text(
                formatAmount(totalPrimaryIncomeBeforeIncomeTaxes) +
                  ' ' +
                  financialProfile.currency
              )
            )
        )
        .append(
          $('<tr>')
            .append(
              $('<td>')
                .css('font-weight', '700')
                .text('(Primary) Provision for income taxes')
            )
            .append(
              $('<td>').text(
                formatAmount(totalPrimaryProvisionForIncomeTaxes) +
                  ' ' +
                  financialProfile.currency
              )
            )
        )
      if (primaryEffectiveTaxRateMinusTCJA.length) {
        tbody
          .append(
            $('<tr>')
              .append(
                $('<td>')
                  .css('font-weight', '700')
                  .text(
                    '(Primary) Provision for income taxes ' +
                      '(w/o one-time TCJA payments)'
                  )
              )
              .append(
                $('<td>').text(
                  formatAmount(totalPrimaryProvisionForIncomeTaxesMinusTCJA) +
                    ' ' +
                    financialProfile.currency
                )
              )
          )
          .append(
            $('<tr>')
              .append(
                $('<td>')
                  .css('font-weight', '700')
                  .text('(Primary) Effective tax rate')
              )
              .append(
                $('<td>').text(totalPrimaryEffectiveTaxRate.toFixed(2) + '%')
              )
          )
      }
      if (primaryEffectiveTaxRateMinusTCJA.length) {
        tbody.append(
          $('<tr>')
            .append(
              $('<td>')
                .css('font-weight', '700')
                .text(
                  '(Primary) Effective tax rate (w/o one-time TCJA payments)'
                )
            )
            .append(
              $('<td>').text(
                totalPrimaryEffectiveTaxRateMinusTCJA.toFixed(2) + '%'
              )
            )
        )
      }
    }
    if (effectiveTaxRate.length) {
      const effectiveTaxRatePeriodStr =
        'FY' + firstYearForEffectiveTaxRate + '-' + lastYearForEffectiveTaxRate

      if (primaryEffectiveTaxRate.length) {
        headerDiv.append('<br/>')
      }

      let table = $('<table>').addClass('taxes')
      headerDiv.append(table)

      let thead = $('<thead>')
      table.append(thead)
      thead.append(
        $('<tr>').append(
          $('<th>').text(`Effective Taxation over ${effectiveTaxRatePeriodStr}`)
        )
      )

      let tbody = $('<tbody>')
      table.append(tbody)
      tbody
        .append(
          $('<tr>')
            .append(
              $('<td>')
                .css('font-weight', '700')
                .text('Income before income taxes')
            )
            .append(
              $('<td>').text(
                formatAmount(totalIncomeBeforeIncomeTaxes) +
                  ' ' +
                  financialProfile.currency
              )
            )
        )
        .append(
          $('<tr>')
            .append(
              $('<td>')
                .css('font-weight', '700')
                .text('Provision for income taxes')
            )
            .append(
              $('<td>').text(
                formatAmount(totalProvisionForIncomeTaxes) +
                  ' ' +
                  financialProfile.currency
              )
            )
        )
      if (effectiveTaxRateMinusTCJA.length) {
        tbody
          .append(
            $('<tr>')
              .append(
                $('<td>')
                  .css('font-weight', '700')
                  .text(
                    'Provision for income taxes (w/o one-time TCJA payments)'
                  )
              )
              .append(
                $('<td>').text(
                  formatAmount(totalProvisionForIncomeTaxesMinusTCJA) +
                    ' ' +
                    financialProfile.currency
                )
              )
          )
          .append(
            $('<tr>')
              .append(
                $('<td>').css('font-weight', '700').text('Effective tax rate')
              )
              .append($('<td>').text(totalEffectiveTaxRate.toFixed(2) + '%'))
          )
      }
      if (effectiveTaxRateMinusTCJA.length) {
        tbody.append(
          $('<tr>')
            .append(
              $('<td>')
                .css('font-weight', '700')
                .text('Effective tax rate (w/o one-time TCJA payments)')
            )
            .append(
              $('<td>').text(totalEffectiveTaxRateMinusTCJA.toFixed(2) + '%')
            )
        )
      }
    }
  }

  // Display the main financials chart.
  {
    let chartSelect = $('<div>').addClass('d3-chart-select')
    $(`#${parentID}`).append(chartSelect)
    chartSelect.append(
      $('<span>')
        .addClass('bold')
        .css('font-size', '140%')
        .css('margin-right', '1em')
        .text('Amount Type: ')
    )
    let select = $('<select>')
      .attr('id', 'financialsType')
      .css('font-size', '140%')
    chartSelect.append(select)

    let options = {}
    if (revenue.length) {
      options['revenue'] = 'Revenue'
    }
    if (grossIncome.length) {
      options['grossIncome'] = 'Gross Income'
    }
    if (primaryOperatingIncome.length) {
      options['primaryOperatingIncome'] = '(Primary) Operating Income'
    }
    if (secondaryOperatingIncome.length) {
      options['secondaryOperatingIncome'] = '(Secondary) Operating Income'
    }
    if (operatingIncome.length) {
      options['operatingIncome'] = 'Operating Income'
    }
    if (primaryIncomeBeforeIncomeTaxes.length) {
      options['primaryIncomeBeforeIncomeTaxes'] =
        '(Primary) Income Before Income Taxes'
    }
    if (secondaryIncomeBeforeIncomeTaxes.length) {
      options['secondaryIncomeBeforeIncomeTaxes'] =
        '(Secondary) Income Before Income Taxes'
    }
    if (incomeBeforeIncomeTaxes.length) {
      options['incomeBeforeIncomeTaxes'] = 'Income Before Income Taxes'
    }
    if (TCJAExceptions.length) {
      options['TCJAExceptions'] = 'One-Time TCJA Benefits'
    }
    if (primaryProvisionForIncomeTaxes.length) {
      options['primaryProvisionForIncomeTaxes'] =
        '(Primary) Provision for Income Taxes'
    }
    if (primaryProvisionForIncomeTaxesMinusTCJA.length) {
      options['primaryProvisionForIncomeTaxesMinusTCJA'] =
        '(Primary) Provision for Income Taxes w/o one-time TCJA'
    }
    if (secondaryProvisionForIncomeTaxes.length) {
      options['secondaryProvisionForIncomeTaxes'] =
        '(Secondary) Provision for Income Taxes'
    }
    if (provisionForIncomeTaxes.length) {
      options['provisionForIncomeTaxes'] = 'Provision for Income Taxes'
    }
    if (provisionForIncomeTaxesMinusTCJA.length) {
      options['provisionForIncomeTaxesMinusTCJA'] =
        'Provision for Income Taxes w/o one-time TCJA'
    }
    if (primaryNetIncome.length) {
      options['primaryNetIncome'] = '(Primary) Net Income'
    }
    if (secondaryNetIncome.length) {
      options['secondaryNetIncome'] = '(Secondary) Net Income'
    }
    if (netIncome.length) {
      options['netIncome'] = 'Net Income'
    }
    for (const [key, value] of Object.entries(options)) {
      select.append(
        $('<option>')
          .attr('value', key)
          .attr('selected', initialFinancialType == key)
          .text(value)
      )
    }

    let chartContainer = $('<div>')
      .addClass('d3-chart-container')
      .attr('id', 'financialsChartContainer')
    chartContainer.append(
      $('<div>').attr('id', 'financialsChart').css('display', 'inline-block')
    )
    $(`#${parentID}`).append(chartContainer)

    const amountTypes = {
      revenue: {
        data: revenue,
        title: `Revenue for ${chartSubject}`,
        sliceFromEnd: true,
      },
      grossIncome: {
        data: grossIncome,
        title: `Gross Income for ${chartSubject}`,
        sliceFromEnd: true,
      },
      operatingIncome: {
        data: operatingIncome,
        title: `Operating Income for ${chartSubject}`,
        sliceFromEnd: true,
      },
      primaryOperatingIncome: {
        data: primaryOperatingIncome,
        title: `(Primary) Operating Income for ${chartSubject}`,
        sliceFromEnd: true,
      },
      secondaryOperatingIncome: {
        data: secondaryOperatingIncome,
        title: `(Secondary) Operating Income for ${chartSubject}`,
        sliceFromEnd: true,
      },
      incomeBeforeIncomeTaxes: {
        data: incomeBeforeIncomeTaxes,
        title: `Income Before Income Taxes for ${chartSubject}`,
        sliceFromEnd: true,
      },
      primaryIncomeBeforeIncomeTaxes: {
        data: primaryIncomeBeforeIncomeTaxes,
        title: `(Primary) Income Before Income Taxes for ${chartSubject}`,
        sliceFromEnd: true,
      },
      secondaryIncomeBeforeIncomeTaxes: {
        data: secondaryIncomeBeforeIncomeTaxes,
        title: `(Secondary) Income Before Income Taxes for ${chartSubject}`,
        sliceFromEnd: true,
      },
      TCJAExceptions: {
        data: TCJAExceptions,
        title: `One-Time TCJA Benefits for ${chartSubject}`,
        sliceFromEnd: true,
      },
      provisionForIncomeTaxes: {
        data: provisionForIncomeTaxes,
        title: `Provision for Income Taxes for ${chartSubject}`,
        sliceFromEnd: true,
      },
      provisionForIncomeTaxesMinusTCJA: {
        data: provisionForIncomeTaxesMinusTCJA,
        title:
          'Provision for Income Taxes for w/o one-time TCJA for ' +
          chartSubject,
        sliceFromEnd: true,
      },
      primaryProvisionForIncomeTaxes: {
        data: primaryProvisionForIncomeTaxes,
        title: `(Primary) Provision for Income Taxes for ${chartSubject}`,
        sliceFromEnd: true,
      },
      primaryProvisionForIncomeTaxesMinusTCJA: {
        data: primaryProvisionForIncomeTaxesMinusTCJA,
        title:
          '(Primary) Provision for Income Taxes w/o one-time TCJA for ' +
          chartSubject,
        sliceFromEnd: true,
      },
      secondaryProvisionForIncomeTaxes: {
        data: secondaryProvisionForIncomeTaxes,
        title: `(Secondary) Provision for Income Taxes for ${chartSubject}`,
        sliceFromEnd: true,
      },
      netIncome: {
        data: netIncome,
        title: `Net Income for ${chartSubject}`,
        sliceFromEnd: true,
      },
      primaryNetIncome: {
        data: primaryNetIncome,
        title: `(Primary) Net Income for ${chartSubject}`,
        sliceFromEnd: true,
      },
      secondaryNetIncome: {
        data: secondaryNetIncome,
        title: `(Secondary) Net Income for ${chartSubject}`,
        sliceFromEnd: true,
      },
    }

    const chartConfig = {
      // The ID of the select for the award type.
      selectID: '#financialsType',

      // The ID of the chart to draw the SVG in.
      chartID: '#financialsChart',

      // The ID of the relative parent div we will compute mouse positions
      // relative to.
      relativeParentID: relativeParentID,

      // The font size of the title.
      titleFontSize: '28px',

      // The font size of the title on small screens.
      titleFontSizeSmallScreen: '22px',

      // The font weight of the title.
      titleFontWeight: '700',

      // The title of the y axis.
      yAxisTitle: 'Amount (USD)',

      // The font size of the y-axis title.
      yAxisTitleFontSize: '25px',

      // The font size of the y-axis title on small screens.
      yAxisTitleFontSizeSmallScreen: '20px',

      // The font weight of the y-axis title.
      yAxisTitleFontWeight: '700',

      // The font size of the x-axis labels.
      xLabelFontSize: '12px',

      // The font weight of the x-axis labels.
      xLabelFontWeight: '700',

      // The font size of the y-axis labels.
      yLabelFontSize: '16px',

      // The font weight of the y-axis labels.
      yLabelFontWeight: '700',

      // How many pixels left of the mouse to place the top-left corner of the
      // tooltip.
      tooltipHorizontalOffset: 60,

      // How many pixels above the mouse to place the top-left corner of the
      // tooltip.
      tooltipVerticalOffset: 55,

      // The maximum width (in pixels) of any bar in the bar chart.
      maxBarWidth: 120,

      // The minimum height (in pixels) of any bar in the bar chart.
      minBarHeight: 4,

      // The height (in pixels) of the inner portion of the chart.
      innerChartHeight: 400,

      // The chart width (in pixels) for large screens.
      chartWidth: 1250,

      // The chart width (in pixels) for small screens.
      smallChartWidth: 800,

      // The largest number of width pixels a screen can have before it's not
      // considered 'small'.
      smallScreenWidth: 1500,

      // The maximum number of agencies to keep in the bar chart.
      maxSmallScreenLabels: 15,

      // The number of pixels in the top margin.
      marginTop: 100,

      // The number of pixels in the right margin.
      marginRight: 60,

      // The minimum number of pixels in the left margin.
      minMarginLeft: 120,

      // The maximum number of pixels in the left margin.
      maxMarginLeft: 200,

      // The number of pixels the left-most text should begin to the right of
      // the chart.
      marginLeftOffset: -20,

      // The minimum number of pixels in the bottom margin of the chart.
      minMarginBottom: 70,

      // The number of margin pixels per character of the labels.
      marginPerChar: 7.5,

      // The interior color of the bars in the chart.
      barColor: '#007FFF',

      // The interior color of the bars in the chart when moused over.
      barColorMouseover: 'blue',

      // The color of the borders of the bars in the chart.
      barBorderColor: '#000000',

      // The number of pixels of the borders of the bars in the chart.
      barBorderWidth: 2,

      xLabelFormat: function (d) {
        return d3.format('.3s')(d).replace('G', 'B')
      },

      tooltipHTML: function (d) {
        var htmlStr = '$' + d3.format('.3s')(d.amount).replace('G', 'B')
        if (d.dateReported) {
          htmlStr +=
            '<br/>' +
            `<span style="font-size: 80%;">(Reported: ${d.dateReported})</span>`
        }
        return htmlStr
      },
    }

    BarChart(amountTypes, chartConfig)
  }

  if (effectiveTaxRate.length || primaryEffectiveTaxRate.length) {
    let chartSelect = $('<div>').addClass('d3-chart-select')
    $(`#${parentID}`).append(chartSelect)

    if (effectiveTaxRate.length && primaryEffectiveTaxRate.length) {
      let span = $('<span>')
        .addClass('bold')
        .css('font-size', '140%')
        .css('margin-right', '1em')
        .text('Effective Tax Rate Type: ')
      chartSelect.append(span)

      let select = $('<select>')
        .attr('id', 'effectiveTaxRateType')
        .css('font-size', '140%')
      select.append($('<option>').attr('value', 'total').text('All Income'))
      if (effectiveTaxRateMinusTCJA.length) {
        select.append(
          $('<option>')
            .attr('value', 'totalMinusTCJA')
            .text('All Income, w/o one-time TCJA payments')
        )
      }
      select.append(
        $('<option>')
          .attr('value', 'primary')
          .attr('selected', true)
          .text('Primary Income')
      )
      if (primaryEffectiveTaxRateMinusTCJA.length) {
        $('<option>')
          .attr('value', 'primaryMinusTCJA')
          .text('Primary Income, w/o one-time TCJA payments')
      }
      chartSelect.append(select)
    } else if (effectiveTaxRate.length && effectiveTaxRateMinusTCJA.length) {
      let span = $('<span>')
        .addClass('bold')
        .css('font-size', '140%')
        .css('margin-right', '1em')
        .text('Effective Tax Rate Type: ')
      chartSelect.append(span)

      let select = $('<select>')
        .attr('id', 'effectiveTaxRateType')
        .css('font-size', '140%')
      select
        .append(
          $('<option>')
            .attr('value', 'total')
            .attr('selected', true)
            .text('All Income')
        )
        .append(
          $('<option>')
            .attr('value', 'totalMinusTCJA')
            .text('All Income, w/o one-time TCJA payments')
        )
      chartSelect.append(select)
    } else if (effectiveTaxRate.length) {
      let select = $('<select>')
        .attr('id', 'effectiveTaxRateType')
        .css('font-size', '140%')
        .css('display', 'none')
      select.append(
        $('<option>')
          .attr('value', 'total')
          .attr('selected', true)
          .text('All Income')
      )
      chartSelect.append(select)
    } else if (
      primaryEffectiveTaxRate.length &&
      primaryEffectiveTaxRateMinusTCJA.length
    ) {
      let span = $('<span>')
        .addClass('bold')
        .css('font-size', '140%')
        .css('margin-right', '1em')
        .text('Effective Tax Rate Type: ')
      chartSelect.append(span)

      let select = $('<select>')
        .attr('id', 'effectiveTaxRateType')
        .css('font-size', '140%')
      select
        .append(
          $('<option>')
            .attr('value', 'primary')
            .attr('selected', true)
            .text('Primary Income')
        )
        .append(
          $('<option>')
            .attr('value', 'primaryMinusTCJA')
            .text('Primary Income, w/o one-time TCJA payments')
        )
      chartSelect.append(select)
    } else {
      let select = $('<select>')
        .attr('id', 'effectiveTaxRateType')
        .css('font-size', '140%')
        .css('display', 'none')
      select.append(
        $('<option>')
          .attr('value', 'primary')
          .attr('selected', true)
          .text('Primary Income')
      )
      chartSelect.append(select)
    }

    let chartContainer = $('<div>')
      .addClass('d3-chart-container')
      .attr('id', 'effectiveTaxRateChartContainer')
    chartContainer.append(
      $('<div>')
        .attr('id', 'effectiveTaxRateChart')
        .css('display', 'inline-block')
    )
    $(`#${parentID}`).append(chartContainer)

    const amountTypes = {
      total: {
        data: effectiveTaxRate,
        title: `Effective Tax Rate for ${chartSubject}`,
        sliceFromEnd: true,
      },
      totalMinusTCJA: {
        data: effectiveTaxRateMinusTCJA,
        title: `Effective Tax Rate w/o one-time TCJA for ${chartSubject}`,
        sliceFromEnd: true,
      },
      primary: {
        data: primaryEffectiveTaxRate,
        title: `(Primary) Effective Tax Rate for ${chartSubject}`,
        sliceFromEnd: true,
      },
      primaryMinusTCJA: {
        data: primaryEffectiveTaxRateMinusTCJA,
        title:
          '(Primary) Effective Tax Rate w/o one-time TCJA for ' + chartSubject,
        sliceFromEnd: true,
      },
    }

    const chartConfig = {
      // The ID of the select for the award type.
      selectID: '#effectiveTaxRateType',

      // The ID of the chart to draw the SVG in.
      chartID: '#effectiveTaxRateChart',

      // The ID of the relative parent div we will compute mouse positions
      // relative to.
      relativeParentID: relativeParentID,

      // The font size of the title.
      titleFontSize: '28px',

      // The font size of the title on small screens.
      titleFontSizeSmallScreen: '22px',

      // The font weight of the title.
      titleFontWeight: '700',

      // The title of the y axis.
      yAxisTitle: 'Tax Rate (Percent)',

      // The font size of the y-axis title.
      yAxisTitleFontSize: '25px',

      // The font size of the y-axis title on small screens.
      yAxisTitleFontSizeSmallScreen: '20px',

      // The font weight of the y-axis title.
      yAxisTitleFontWeight: '700',

      // The font size of the x-axis labels.
      xLabelFontSize: '15px',

      // The font weight of the x-axis labels.
      xLabelFontWeight: '700',

      // The font size of the y-axis labels.
      yLabelFontSize: '16px',

      // The font weight of the y-axis labels.
      yLabelFontWeight: '700',

      // How many pixels left of the mouse to place the top-left corner of the
      // tooltip.
      tooltipHorizontalOffset: 50,

      // How many pixels above the mouse to place the top-left corner of the
      // tooltip.
      tooltipVerticalOffset: 55,

      // The maximum width (in pixels) of any bar in the bar chart.
      maxBarWidth: 120,

      // The minimum height (in pixels) of any bar in the bar chart.
      minBarHeight: 4,

      // The height (in pixels) of the inner portion of the chart.
      innerChartHeight: 400,

      // The chart width (in pixels) for large screens.
      chartWidth: 1250,

      // The chart width (in pixels) for small screens.
      smallChartWidth: 800,

      // The largest number of width pixels a screen can have before it's not
      // considered 'small'.
      smallScreenWidth: 1500,

      // The maximum number of agencies to keep in the bar chart.
      maxSmallScreenLabels: 15,

      // The number of pixels in the top margin.
      marginTop: 100,

      // The number of pixels in the right margin.
      marginRight: 60,

      // The minimum number of pixels in the left margin.
      minMarginLeft: 120,

      // The maximum number of pixels in the left margin.
      maxMarginLeft: 200,

      // The number of pixels the left-most text should begin to the right of
      // the chart.
      marginLeftOffset: -20,

      // The minimum number of pixels in the bottom margin of the chart.
      minMarginBottom: 70,

      // The number of margin pixels per character of the labels.
      marginPerChar: 7.5,

      // The interior color of the bars in the chart.
      barColor: '#007FFF',

      // The interior color of the bars in the chart when moused over.
      barColorMouseover: 'blue',

      // The color of the borders of the bars in the chart.
      barBorderColor: '#000000',

      // The number of pixels of the borders of the bars in the chart.
      barBorderWidth: 2,

      xLabelFormat: function (d) {
        return d3.format('.2f')(d).replace('G', 'B')
      },

      tooltipHTML: function (d) {
        let htmlStr = d3.format('.2f')(d.amount).replace('G', 'B') + '%'
        if (d.dateReported) {
          htmlStr +=
            '<br/>' +
            `<span style="font-size: 80%;">(Reported: ${d.dateReported})</span>`
        }
        return htmlStr
      },
    }

    BarChart(amountTypes, chartConfig)
  }

  if (marketCaps[0].length) {
    let chartSelect = $('<div>').addClass('d3-chart-select')
    $(`#${parentID}`).append(chartSelect)

    chartSelect
      .append(
        $('<span>')
          .addClass('bold')
          .css('font-size', '140%')
          .css('margin-right', '1em')
          .text('Market Cap Type: ')
      )
      .append(
        $('<select>')
          .attr('id', 'marketCapType')
          .css('font-size', '140%')
          .append(
            $('<option>')
              .attr('value', 'basic')
              .attr('selected', true)
              .text('Basic')
          )
          .append($('<option>').attr('value', 'diluted').text('Diluted'))
      )

    let chartContainer = $('<div>')
      .addClass('d3-chart-container')
      .attr('id', 'marketCapChartContainer')
    chartContainer.append(
      $('<div>').attr('id', 'marketCapChart').css('display', 'inline-block')
    )
    $(`#${parentID}`).append(chartContainer)

    const amountTypes = {
      basic: {
        data: marketCaps[0],
        title: '(Basic) Market Capitalization by Year',
        sliceFromEnd: true,
      },
      diluted: {
        data: marketCaps[1],
        title: '(Diluted) Market Capitalization by Year',
        sliceFromEnd: true,
      },
    }

    const chartConfig = {
      // The ID of the select for the award type.
      selectID: '#marketCapType',

      // The ID of the chart to draw the SVG in.
      chartID: '#marketCapChart',

      // The ID of the relative parent div we will compute mouse positions
      // relative to.
      relativeParentID: relativeParentID,

      // The font size of the title.
      titleFontSize: '28px',

      // The font size of the title on small screens.
      titleFontSizeSmallScreen: '22px',

      // The font weight of the title.
      titleFontWeight: '700',

      // The title of the y axis.
      yAxisTitle: 'Amount (USD)',

      // The font size of the y-axis title.
      yAxisTitleFontSize: '25px',

      // The font size of the y-axis title on small screens.
      yAxisTitleFontSizeSmallScreen: '20px',

      // The font weight of the y-axis title.
      yAxisTitleFontWeight: '700',

      // The font size of the x-axis labels.
      xLabelFontSize: '15px',

      // The font weight of the x-axis labels.
      xLabelFontWeight: '700',

      // The font size of the y-axis labels.
      yLabelFontSize: '16px',

      // The font weight of the y-axis labels.
      yLabelFontWeight: '700',

      // How many pixels left of the mouse to place the top-left corner of the
      // tooltip.
      tooltipHorizontalOffset: 50,

      // How many pixels above the mouse to place the top-left corner of the
      // tooltip.
      tooltipVerticalOffset: 50,

      // The maximum width (in pixels) of any bar in the bar chart.
      maxBarWidth: 120,

      // The minimum height (in pixels) of any bar in the bar chart.
      minBarHeight: 4,

      // The height (in pixels) of the inner portion of the chart.
      innerChartHeight: 400,

      // The chart width (in pixels) for large screens.
      chartWidth: 1250,

      // The chart width (in pixels) for small screens.
      smallChartWidth: 800,

      // The largest number of width pixels a screen can have before it's not
      // considered 'small'.
      smallScreenWidth: 1500,

      // The maximum number of agencies to keep in the bar chart.
      maxSmallScreenLabels: 15,

      // The number of pixels in the top margin.
      marginTop: 100,

      // The number of pixels in the right margin.
      marginRight: 60,

      // The minimum number of pixels in the left margin.
      minMarginLeft: 120,

      // The maximum number of pixels in the left margin.
      maxMarginLeft: 200,

      // The number of pixels the left-most text should begin to the right of
      // the chart.
      marginLeftOffset: -20,

      // The minimum number of pixels in the bottom margin of the chart.
      minMarginBottom: 70,

      // The number of margin pixels per character of the labels.
      marginPerChar: 6.9,

      // The interior color of the bars in the chart.
      barColor: '#007FFF',

      // The interior color of the bars in the chart when moused over.
      barColorMouseover: 'blue',

      // The color of the borders of the bars in the chart.
      barBorderColor: '#000000',

      // The number of pixels of the borders of the bars in the chart.
      barBorderWidth: 2,

      xLabelFormat: function (d) {
        return d3.format('.3s')(d).replace('G', 'B')
      },

      tooltipHTML: function (d) {
        var htmlStr = '$' + d3.format('.3s')(d.amount).replace('G', 'B')
        return htmlStr
      },
    }

    BarChart(amountTypes, chartConfig)
  }

  const havePrimary =
    (primaryProvisionForIncomeTaxes !== undefined &&
      primaryProvisionForIncomeTaxes.length) ||
    (primaryIncomeBeforeIncomeTaxes !== undefined &&
      primaryIncomeBeforeIncomeTaxes.length)
  const haveMarketCaps = marketCaps[0].length > 0
  return {
    havePrimary: havePrimary,
    haveMarketCaps: haveMarketCaps,
  }
}
