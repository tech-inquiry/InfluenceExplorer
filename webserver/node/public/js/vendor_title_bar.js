function VendorLogoLink(profile) {
  if (
    profile == undefined ||
    !('urls' in profile) ||
    !('logo' in profile.urls)
  ) {
    return undefined
  }
  return profile.urls.logo
}

function VendorInstitutionalLink(profile) {
  if (
    profile == undefined ||
    !('urls' in profile) ||
    !('institutional' in profile.urls)
  ) {
    return undefined
  }
  return profile.urls.institutional
}

function VendorTitleBar(parentID, profile, vendorStylized, explorerRoute) {
  let title = $('<div>').addClass('title')
  $(`#${parentID}`).append(title)
  title
    .append(
      $('<span>')
        .addClass('title_logo_text')
        .append($('<a>').attr('href', explorerRoute).text('Tech Inquiry'))
    )
    .append(
      $('<span>')
        .addClass('stylized_title_text')
        .text(unescapeHTML(vendorStylized))
    )

  const logoLink = VendorLogoLink(profile)
  const institutionalLink = VendorInstitutionalLink(profile)

  if (logoLink) {
    let logoSpan = $('<span>')
      .addClass('title_logo_span')
      .css('background-color', '#FFFFFF')
    title.append(logoSpan)

    let logo = $('<img>')
      .addClass('title_logo')
      .attr('src', logoLink)
      .attr('alt', vendorStylized)
      .attr('title', vendorStylized)
    if (institutionalLink) {
      logoSpan.append($('<a>').attr('href', institutionalLink).append(logo))
    } else {
      logoSpan.append(logo)
    }
  }
}
