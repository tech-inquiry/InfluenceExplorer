function FinancialsTab(
  tabsState,
  financialProfile,
  chartSubject,
  peers,
  secondaryPeers,
  peeredBy,
  secondaryPeeredBy,
  competitors,
  competitorOf
) {
  const label = 'financials'
  const tabLogo = undefined
  let tabTitle = ''
  if (
    financialProfile.entities &&
    (financialProfile.peers ||
      financialProfile.peeredBy ||
      financialProfile.secondaryPeeredBy ||
      financialProfile.competitors ||
      financialProfile.competitorOf) &&
    financialProfile.firstFiscalMonthLabel
  ) {
    tabTitle = 'Income, Taxes, Directors, and Peers'
  } else if (
    financialProfile.entities &&
    financialProfile.firstFiscalMonthLabel
  ) {
    tabTitle = 'Income, Taxes, and Directors'
  } else if (financialProfile.entities) {
    tabTitle = 'Directors'
  } else if (
    !financialProfile.firstFiscalMonthLabel &&
    (financialProfile.peeredBy ||
      financialProfile.secondaryPeeredBy ||
      financialProfile.competitorOf)
  ) {
    tabTitle = 'Peers'
  } else {
    tabTitle = 'Income and Taxes'
  }
  NavTab(tabsState.navID(), label, tabLogo, tabTitle)

  let mainTab = $('<div>')
    .attr('id', `filing-tab-${label}`)
    .css('display', 'none')
    .css('padding-top', '0')
    .css('padding-left', '0')
  $(`#${tabsState.id()}`).append(mainTab)

  let tabsID = `${label}-tabs`
  let tabs = $('<div>').addClass('filing_tabs').attr('id', tabsID)
  mainTab.append(tabs)

  let nav = $('<ul>').addClass('nav nav-tabs').css('font-size', '175%')
  tabs.append(nav)
  let navLinks = {}
  if (financialProfile.firstFiscalMonthLabel) {
    navLinks[`#${label}-tab-summary`] = 'Summary'
  }
  if (financialProfile.entities) {
    navLinks[`#${label}-tab-entities`] = 'Directors'
  }
  if (financialProfile.peers || financialProfile.competitors) {
    const key = `#${label}-tab-peers`
    if (financialProfile.peers && financialProfile.competitors) {
      navLinks[key] = '(Self-Selected) Peers and Competitors'
    } else if (financialProfile.peers) {
      navLinks[key] = '(Self-Selected) Peers'
    } else {
      navLinks[key] = '(Self-Selected) Competitors'
    }
  }
  if (financialProfile.peeredBy || financialProfile.competitorOf) {
    const key = `#${label}-tab-peeredBy`
    if (financialProfile.peeredBy && financialProfile.competitorOf) {
      navLinks[key] = '(Externally-Selected) Peers and Competitors'
    } else if (financialProfile.peeredBy) {
      navLinks[key] = '(Externally-Selected) Peers'
    } else {
      navLinks[key] = '(Externally-Selected) Competitors'
    }
  }
  if (financialProfile.sourcedDocuments) {
    navLinks[`#${label}-tab-records`] = 'Source Documents'
  }
  for (const [key, value] of Object.entries(navLinks)) {
    nav.append(
      $('<li>')
        .addClass('nav-item')
        .append($('<a>').addClass('nav-link').attr('href', key).text(value))
    )
  }

  const summaryTabID = `${label}-tab-summary`
  let summaryTab = $('<div>')
    .addClass('content')
    .attr('id', summaryTabID)
    .css('display', 'none')
  tabs.append(summaryTab)
  const info = FinancialsChart(
    summaryTabID,
    financialProfile,
    chartSubject,
    tabsID
  )
  let summaryDiv = $('<div>').css('text-align', 'left').css('font-size', '125%')
  summaryTab.append(summaryDiv)
  if (financialProfile.note) {
    summaryDiv.append(
      $('<p>').css('margin-top', '1em').html(financialProfile.note)
    )
  }
  if (info.havePrimary) {
    summaryDiv.append(
      $('<p>')
        .css('margin-top', '1em')
        .text(
          '*** Many companies (only) report the taxes they paid over a ' +
            'subset of their income: often excluding the income from spun-off ' +
            'companies, minority interests in subsidiaries, and/or ' +
            'extraordinary events. We refer to the excluded income as ' +
            '"secondary" and the remaining, reported income as "primary".'
        )
    )
  }
  if (financialProfile.divestitures_and_acquisitions !== undefined) {
    summaryDiv.append(
      $('<p>')
        .css('margin-top', '1em')
        .text(
          '**** The following divestitures and/or acquisitions result in ' +
            'discontinuities in reported amounts at the end of the three-year ' +
            'retrospective window for Consolidated Statements of ' +
            'Earnings/Income/Operations or five-year retrospective window for ' +
            'Selected Five-Year Data (almost always revenue, (continuing) income ' +
            'before income taxes, (continuing) provision for income taxes, and ' +
            'next income).'
        )
    )

    let divAndAcq = $('<ul>')
      .css('column-count', '1')
      .css('margin-left', '1.5em')
    summaryDiv.append(divAndAcq)
    financialProfile.divestitures_and_acquisitions.forEach(function (item) {
      let text = `${item.date}: `
      if (item.type == 'divestiture') {
        text += `${chartSubject} divested ${item.name}`
        if (item.sold_to) {
          text += ` ${item.sold_to}`
        }
      } else if (item.type == 'acquisition') {
        text += `${chartSubject} acquired ${item.name}`
        if (item.bought_from) {
          text += ` ${item.bought_from}`
        }
      }
      let li = $('<li>').css('margin-top', '0.5em').text(text)
      if (item.note) {
        li.append(item.note)
      }
      divAndAcq.append(li)
    })
  }
  if (info.haveMarketCaps) {
    summaryDiv.append(
      $('<p>')
        .css('margin-top', '1em')
        .text(
          '***** Rough yearly market capitalizations were computed using ' +
            'the product of the last share price for the fiscal year (retrieved ' +
            'through the yfinance API with stock splits reversed back to the ' +
            'quote date) multiplied by the weighted average of the number of ' +
            'outstanding (basic or diluted) shares for the fiscal year reported ' +
            'in the 10-K. One could question whether it would make more sense to ' +
            'compute the weighted average of the product of the number of ' +
            'outstanding shares with the share price throughout the year -- ' +
            'which would require detailed knowledge of the price history, but ' +
            'Earnings Per Share are computed in a manner similar to our simple ' +
            'approach. For companies with separately traded classes of stock ' +
            "(e.g., Alphabet's GOOGL and GOOG) we sum the market cap " +
            'calculations for each class.'
        )
    )
  }

  if (financialProfile.entities) {
    const tabID = `${label}-tab-entities`
    let tab = $('<div>')
      .addClass('content')
      .attr('id', tabID)
      .css('display', 'none')
    tabs.append(tab)
    let card = $('<div>').addClass('card').css('font-size', '175%')
    tab.append(card)
    let cardBody = $('<div>').addClass('cardBody')
    card.append(cardBody)
    let list = $('<ul>').addClass('list-group list-group-flush')
    cardBody.append(list)
    financialProfile.entities.forEach(function (entity) {
      let li = $('<li>').addClass('list-group-item')
      list.append(li)
      if (entity.headshot) {
        li.append(
          $('<img/>')
            .attr('src', entity.headshot)
            .css('max-height', '200px')
            .css('margin-bottom', '1em')
        )
      }
      let html = entity.name
      if (entity.age || (entity.positions && entity.positions.length)) {
        html += ' ('
        if (entity.age) {
          html += `Age: ${entity.age}; `
        }
        entity.positions.forEach(function (position, posIndex) {
          if (typeof position == 'string') {
            html += position
          } else {
            html += position.title
            if ('period' in position) {
              html += ` [${position.period}]`
            }
          }
          if (posIndex != entity.positions.length - 1) {
            html += ', '
          }
        })
        html += ')'
      }

      let span = $('<span>')
        .css('font-size', '125%')
        .css('font-weight', '700')
        .html(html)
      li.append(span)
      li.append('<br/>')
      if (entity.other_boards) {
        let boardDiv = $('<div>').css('margin-top', '0.75em')
        li.append(boardDiv)
        let boardSpan = $('<span>')
          .css('font-size', '110%')
          .css('font-weight', '700')
          .text('Memberships on Other Boards:')
        boardDiv.append(boardSpan)
        let boardList = $('<ul>')
          .css('margin-top', '1em')
          .css('margin-left', '0.5em')
        boardDiv.append(boardList)
        entity.other_boards.forEach(function (item) {
          const otherBoardStr =
            typeof item == 'string'
              ? item
              : item.company + (item.period ? ` (${item.period})` : '')
          let boardLI = $('<li>')
            .css('margin-left', '0.5em')
            .text(otherBoardStr)
          boardList.append(boardLI)
        })
      }
      if (entity.blurb) {
        let blurbDiv = $('<div>').css('margin-top', '0.75em')
        li.append(blurbDiv)
        let blurbSpan = $('<span>')
          .css('font-size', '110%')
          .css('font-weight', '700')
          .text('Description to SEC:')
        blurbDiv.append(blurbSpan)
        blurbDiv.append(
          $('<div>')
            .css('margin-top', '1.5em')
            .css('margin-left', '1.5em')
            .css('margin-bottom', '0.5em')
            .html(entity.blurb)
        )
      }
      if (entity.stock) {
        let stockDiv = $('<div>').css('margin-top', '0.75em')
        li.append(stockDiv)
        let stockSpan = $('<span>')
          .css('font-size', '110%')
          .css('font-weight', '700')
          .text('Stock Ownership:')
        stockDiv.append(stockSpan)
        let stockList = $('<ul>')
          .css('margin-left', '0.5em')
          .css('list-style', 'none')
        stockDiv.append(stockList)
        for (const [key, value] of Object.entries(entity.stock)) {
          let stockLI = $('<li>').css('margin-left', '0.5em')
          stockList.append(stockLI)
          stockLI.append($('<span>').css('font-weight', '700').text(key))
          stockLI.append(` ${value.shares} shares`)
          if (value.percentage == '*') {
            stockLI.append('&nbsp;(Percent: &lt;1)')
          } else {
            stockLI.append(`&nbsp;(Percent: ${value.percentage}`)
          }
        }
      }
    })
  }

  function PeerTable(annotations) {
    let table = $('<table>').addClass('table')

    let thead = $('<thead>')
    table.append(thead)
    thead.append(
      $('<tr>')
        .append($('<th>').attr('scope', 'col').text('Image'))
        .append($('<th>').attr('scope', 'col').text('Name'))
    )

    let tbody = $('<tbody>')
    table.append(tbody)
    annotations.forEach(function (peer) {
      let tr = $('<tr>')
      tbody.append(tr)
      tr.append($('<td>').html(peer.logoHTML))
      if (peer.url) {
        tr.append(
          $('<td>').html(
            $('<a>').attr('href', peer.url).html(peer.stylizedTextWithTags)
          )
        )
      } else {
        tr.append($('<td>').html(peer.stylizedTextWithTags))
      }
    })

    return table
  }

  if (peers.length || competitors.length) {
    const tabID = `${label}-tab-peers`
    let tab = $('<div>')
      .addClass('content')
      .attr('id', tabID)
      .css('display', 'none')
    tabs.append(tab)
    let card = $('<div>').addClass('card').css('font-size', '175%')
    tab.append(card)
    let cardBody = $('<div>').addClass('cardBody')
    card.append(cardBody)

    if (competitors.length) {
      let cardText = $('<p>')
        .addClass('card-text')
        .text(
          'The following competitor group is defined specifically ' +
            'with respect to sector competition rather than employee/board ' +
            'or regulatory similarities.'
        )
      cardBody.append(cardText)
      let table = PeerTable(competitors)
      cardBody.append(table)
    }

    if (peers.length) {
      let cardText = $('<p>')
        .addClass('card-text')
        .text(
          'The following peer group is defined for executive compensation ' +
            'purposes and includes companies which are either competing in ' +
            'the same industry or for the same employees/board members, have ' +
            'similar regulatory dynamics, or a combination thereof.'
        )
      cardBody.append(cardText)
      if (secondaryPeers.length) {
        cardBody.append($('<h3>').addClass('card-text').text('Primary Peers'))
      }
      let table = PeerTable(peers)
      cardBody.append(table)

      if (secondaryPeers.length) {
        cardBody.append($('<h3>').addClass('card-text').text('Secondary Peers'))
        let secondaryTable = PeerTable(secondaryPeers)
        cardBody.append(secondaryTable)
      }
    }
  }
  if (peeredBy.length || secondaryPeeredBy.length || competitorOf.length) {
    const tabID = `${label}-tab-peeredBy`
    let tab = $('<div>')
      .addClass('content')
      .attr('id', tabID)
      .css('display', 'none')
    tabs.append(tab)
    let card = $('<div>').addClass('card').css('font-size', '175%')
    tab.append(card)
    let cardBody = $('<div>').addClass('cardBody')
    card.append(cardBody)

    if (competitorOf.length) {
      let cardText = $('<p>')
        .addClass('card-text')
        .text(
          'The following competitor group is defined by members of the ' +
            'listed group of companies choosing (in an SEC filing) ' +
            chartSubject +
            ' as a sectoral competitor rather than due to ' +
            'employee/board or regulatory similarities.'
        )
      cardBody.append(cardText)
      let table = PeerTable(competitorOf)
      cardBody.append(table)
    }

    if (peeredBy.length || secondaryPeeredBy.length) {
      let cardText = $('<p>')
        .addClass('card-text')
        .text(
          'The following peer group is defined by members of the listed ' +
            'group choosing (in an SEC filing) ' +
            chartSubject +
            ' for ' +
            'executive compensation purposes, competing in the same industry, ' +
            'for the same employees/board members, having similar regulatory ' +
            'dynamics, or a combination thereof.'
        )
      cardBody.append(cardText)
      if (peeredBy.length) {
        if (secondaryPeeredBy.length) {
          cardBody.append($('<h3>').addClass('card-text').text('Primary Peers'))
        }
        let table = PeerTable(peeredBy)
        cardBody.append(table)
      }
      if (secondaryPeeredBy.length) {
        cardBody.append($('<h3>').addClass('card-text').text('Secondary Peers'))
        let secondaryTable = PeerTable(secondaryPeeredBy)
        cardBody.append(secondaryTable)
      }
    }
  }
  if (financialProfile.sourcedDocuments) {
    const tabID = `${label}-tab-records`
    let tab = $('<div>')
      .addClass('content')
      .attr('id', tabID)
      .css('display', 'none')
    tabs.append(tab)
    let card = $('<div>').addClass('card').css('font-size', '175%')
    tab.append(card)
    let cardBody = $('<div>').addClass('cardBody')
    card.append(cardBody)
    let list = $('<ul>').addClass('list-group list-group-flush')
    cardBody.append(list)
    financialProfile.sourcedDocuments.forEach(function (item) {
      let li = $('<li>').addClass('list-group-item')
      list.append(li)
      if (item.date) {
        li.append(`${item.date}, `)
      }
      li.append(
        $('<a>').addClass('sourceLink').attr('href', item.url).text(item.title)
      )
    })
  }

  tabsState.addTab({ id: tabsID })
}
