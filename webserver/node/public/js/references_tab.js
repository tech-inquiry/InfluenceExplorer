function JournalisticReferences(profile) {
  if (
    profile == undefined ||
    !('urls' in profile) ||
    !('journalism' in profile.urls)
  ) {
    return []
  }
  return profile.urls.journalism
}

function MiscellaneousReferences(profile) {
  if (
    profile == undefined ||
    !('urls' in profile) ||
    !('misc' in profile.urls)
  ) {
    return []
  }
  return profile.urls.misc
}

function HaveReferences(profile) {
  return (
    JournalisticReferences(profile).length > 0 ||
    MiscellaneousReferences(profile).length > 0
  )
}

function ReferenceCategoryTab(
  parentID,
  references,
  label,
  categoryLabel,
  categoryTitle,
  referenceSourceConfig
) {
  function AuthorString(authorList) {
    if (authorList.length == 0) {
      return ''
    } else if (authorList.length == 1) {
      return authorList[0]
    } else if (authorList.length == 2) {
      return authorList[0] + ' and ' + authorList[1]
    } else {
      let authorString = ''
      authorList.forEach(function (author, index) {
        if (index > 0) {
          authorString += ', '
        }
        if (index == authorList.length - 1) {
          authorString += 'and '
        }
        authorString += author
      })
      return authorString
    }
  }

  let div = $('<div>')
    .addClass('content')
    .attr('id', `${label}-tab-${categoryLabel}`)
    .css('display', 'none')
  $(`#${parentID}`).append(div)

  let card = $('<div>').addClass('card').css('font-size', '175%')
  div.append(card)
  let cardBody = $('<div>').addClass('card-body')
  card.append(cardBody)

  cardBody.append($('<h2>').addClass('card-title').text(categoryTitle))

  let list = $('<ul>').addClass('list-group list-group-flush')
  cardBody.append(list)

  references.forEach(function (item) {
    let li = $('<li>').addClass('list-group-item')
    list.append(li)

    if ('org' in item) {
      const lowercaseOrg = item.org.toLowerCase()
      if (lowercaseOrg in referenceSourceConfig) {
        const sourceConfig = referenceSourceConfig[lowercaseOrg]
        let link = $('<a>')
          .addClass('sourceLink')
          .attr('href', sourceConfig.url)
        li.append(link)

        if ('icon_class' in sourceConfig) {
          let icon = $('<i/>')
            .addClass(sourceConfig['icon_class'])
            .attr('alt', sourceConfig['icon_alt'])
            .attr('title', sourceConfig['icon_alt'])
          for (const [k, v] of Object.entries(sourceConfig['icon_style'])) {
            icon.css(k, v)
          }
          link.html(icon)
        } else {
          let image = $('<img>')
            .attr('src', sourceConfig['image_url'])
            .attr('alt', sourceConfig['image_alt'])
            .attr('title', sourceConfig['image_alt'])
          for (const [k, v] of Object.entries(sourceConfig['image_style'])) {
            image.css(k, v)
          }
          link.html(image)
        }
      } else {
        li.append(`${item.org}: `)
      }
    }

    if ('authors' in item) {
      li.append(`${AuthorString(item.authors)}, `)
    }

    const linkText = 'title' in item ? item.title : item.url
    li.append(
      $('<a>').addClass('sourceLink').attr('href', item.url).html(linkText)
    )

    if ('date' in item) {
      li.append(`, ${item.date}`)
    }
  })
}

function ReferencesTab(tabsState, profile, referenceSourceConfig) {
  const label = 'references'
  const tabLogo = '/logos/notepad.png'
  const tabTitle = 'References'
  NavTab(tabsState.navID(), label, tabLogo, tabTitle)

  let tab = $('<div>')
    .addClass('content')
    .attr('id', `filing-tab-${label}`)
    .css('display', 'none')
    .css('padding-top', '0')
    .css('padding-left', '0')
  $(`#${tabsState.id()}`).append(tab)

  const tabsID = `${label}-tabs`
  let tabs = $('<div>').addClass('filing_tabs').attr('id', tabsID)
  tab.append(tabs)

  let nav = $('<ul>').addClass('nav nav-tabs').css('font-size', '175%')
  tabs.append(nav)

  const journRef = JournalisticReferences(profile)
  const miscRef = MiscellaneousReferences(profile)

  if (journRef.length) {
    nav.append(
      $('<li>')
        .addClass('nav-item')
        .append(
          $('<a>')
            .addClass('nav-link')
            .attr('href', `#${label}-tab-journalism`)
            .text('Journalism')
        )
    )
    ReferenceCategoryTab(
      tabsID,
      journRef,
      label,
      'journalism',
      'Articles',
      referenceSourceConfig
    )
  }
  if (miscRef.length) {
    nav.append(
      $('<li>')
        .addClass('nav-item')
        .append(
          $('<a>')
            .addClass('nav-link')
            .attr('href', `#${label}-tab-misc`)
            .text('Miscellaneous')
        )
    )
    ReferenceCategoryTab(
      tabsID,
      miscRef,
      label,
      'misc',
      'Misc. Citations',
      referenceSourceConfig
    )
  }

  tabsState.addTab({ id: tabsID })
}
