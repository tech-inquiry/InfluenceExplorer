function FeedTab(
  parentID,
  tabsListID,
  label,
  tabLogo,
  tabTitle,
  recordsNote = undefined,
  alternateNames = undefined,
  alternateNamesTitle = 'Included Names',
  haveSummary = false
) {
  NavTab(tabsListID, label, tabLogo, tabTitle)

  // Append the actual tab.
  let mainTab = $('<div>')
    .attr('id', `filing-tab-${label}`)
    .css('display', 'none')
    .css('padding-top', '0')
    .css('padding-left', '0')
  $(`#${parentID}`).append(mainTab)

  let tabs = $('<div>').addClass('filing_tabs').attr('id', `${label}-tabs`)
  mainTab.append(tabs)

  let nav = $('<ul>')
    .addClass('nav nav-tabs')
    .attr('id', `filing-tab-${label}-nav`)
    .css('font-size', '175%')
  tabs.append(nav)

  nav.append(
    $('<li>')
      .addClass('nav-item')
      .append(
        $('<a>')
          .addClass('nav-link')
          .attr('id', `${label}-tab-records-link`)
          .attr('href', `#${label}-tab-records`)
          .text('Annotated Records')
      )
  )
  if (haveSummary) {
    nav.append(
      $('<li>')
        .addClass('nav-item')
        .append(
          $('<a>')
            .addClass('nav-link')
            .attr('href', `#${label}-tab-summary`)
            .text('Summary')
        )
    )
  }
  if (alternateNames !== undefined && alternateNames.length > 1) {
    nav.append(
      $('<li>')
        .addClass('nav-item')
        .append(
          $('<a>')
            .addClass('nav-link')
            .attr('href', `#${label}-tab-alternate`)
            .text(alternateNamesTitle)
        )
    )
  }

  let recordsTab = $('<div>')
    .addClass('content')
    .attr('id', `${label}-tab-records`)
    .css('display', 'none')
  tabs.append(recordsTab)
  if (recordsNote !== undefined) {
    recordsTab.append(
      $('<div>')
        .css('font-size', '175%')
        .css('margin-bottom', '1em')
        .html(recordsNote)
    )
  }
  recordsTab.append(
    $('<div>')
      .attr('id', `${label}-tab-records-inner`)
      .append(
        $('<div>')
          .css('font-size', '150%')
          .css('font-weight', '700')
          .css('margin-top', '1em')
          .text('Performing PostgreSQL query...')
      )
  )

  if (haveSummary) {
    let summaryTab = $('<div>')
      .addClass('content')
      .attr('id', `${label}-tab-summary`)
      .css('display', 'none')
    tabs.append(summaryTab)
    summaryTab.append(
      $('<div>')
        .css('font-size', '150%')
        .css('margin-top', '1em')
        .css('font-weight', '700')
        .text('Performing PostgreSQL query...')
    )
  }

  if (alternateNames !== undefined && alternateNames.length > 1) {
    let alternateTab = $('<div>')
      .addClass('content')
      .attr('id', `${label}-tab-alternate`)
      .css('display', 'none')
    tabs.append(alternateTab)
    let card = $('<div>').addClass('card').css('font-size', '175%')
    alternateTab.append(card)
    let cardBody = $('<div>').addClass('card-body')
    card.append(cardBody)
    cardBody.append($('<h2>').addClass('card-title').text(alternateNamesTitle))
    let list = $('<ul>').addClass('list-group list-group-flush')
    cardBody.append(list)
    alternateNames.forEach(function (alternate, alternateIndex) {
      if (alternateIndex == 0) {
        return
      }
      list.append($('<li>').addClass('list-group-item').text(alternate))
    })
  }

  return tabs
}
