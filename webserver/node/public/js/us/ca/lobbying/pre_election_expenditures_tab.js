function USCALobbyingPreElectionExpendituresTab(
  tabsState,
  data,
  alternateNames = undefined
) {
  const label = 'calaccessPreElectExpend'
  const tabLogo = '/logos/state_of_california.svg'
  const tabTitle = 'California Pre-Election Expenditures'
  const queryURL =
    appendURLPath(
      EXPLORER_ROUTE,
      'api/us/ca/lobbying/pre_election_expenditures?'
    ) +
    (data['vendor']
      ? `vendor=${encodeURIComponentIncludingSlashes(data.vendor)}`
      : `text=${encodeURIComponentIncludingSlashes(data.text)}`)
  const recordsNote = $('<div>')
    .append(
      $('<p>')
        .text('All links and images are annotations added to the original ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr(
              'href',
              'https://www.sos.ca.gov/campaign-lobbying/cal-access-resources/raw-data-campaign-finance-and-lobbying-activity'
            )
            .text('CALACCESS zipfile')
        )
        .append(
          ' by Tech Inquiry. Any normalized names are clearly labeled with the ' +
            "original ('as written') text."
        )
    )
    .append(
      $('<p>')
        .append('You can alternatively download the ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', queryURL)
            .text('JSON data')
        )
        .append(' for this tab.')
    )

  FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote,
    alternateNames
  )

  const tabsID = `${label}-tabs`

  function renderRecordsTab(event, ui) {
    const containerID = `${label}-tab-records-inner`
    USCALobbyingPreElectionExpendituresTable(data, containerID)
  }

  function renderTab(event, ui) {
    const recordsTab = 0
    const activeTab = $(`#${tabsID}`).tabs('option', 'active')
    if (activeTab == recordsTab) {
      renderRecordsTab(event, ui)
    }
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
