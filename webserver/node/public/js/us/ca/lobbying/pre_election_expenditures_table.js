function USCALobbyingPreElectionExpendituresTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(
      EXPLORER_ROUTE,
      'api/us/ca/lobbying/preElectionExpenditures'
    ),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    function buildName(entityObj) {
      const nameKeys = ['prefix', 'first_name', 'last_name', 'suffix']
      let namePieces = []
      nameKeys.forEach(function (nameKey) {
        if (nameKey in entityObj && entityObj[nameKey]) {
          namePieces.push(entityObj[nameKey])
        }
      })
      if (namePieces.length) {
        return namePieces.join(' ')
      } else {
        return null
      }
    }

    let tableData = []
    filings.forEach(function (filing, filingIndex) {
      const filedDate = filing.cover
        ? dateWithoutTimeLex(filing.cover['filed_date'])
        : ''

      let filersStr = ''
      if (filing.cover && filing.cover.filer) {
        filersStr = buildName(filing.cover.filer)
      }

      let ballotMeasure = ''
      if (
        filing.cover &&
        filing.cover.ballot_measure &&
        filing.cover.ballot_measure.name
      ) {
        ballotMeasure = filing.cover.ballot_measure.name
      }

      let candidatesStr = ''
      if (filing.cover && filing.cover.candidate) {
        candidatesStr = buildName(filing.cover.candidate)
      }

      let description = ''
      if (
        filing.pre_election_expenditure &&
        filing.pre_election_expenditure.description
      ) {
        description = filing.pre_election_expenditure.description
      }

      let amount = 0
      if (
        filing.pre_election_expenditure &&
        'amount' in filing.pre_election_expenditure
      ) {
        amount = filing.pre_election_expenditure.amount
      }

      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        filedDate,
        filersStr,
        ballotMeasure,
        candidatesStr,
        description,
        amount,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Date Filed' },
        { targets: 2, title: 'Filer' },
        { targets: 3, title: 'Ballot Measure' },
        { targets: 4, title: 'Candidate' },
        { targets: 5, title: 'Description' },
        {
          targets: 6,
          title: 'Amount',
          render: $.fn.dataTable.render.number(',', '.', 2, '$'),
        },
        { targets: 7, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      USCALobbyingPreElectionExpenditures(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
