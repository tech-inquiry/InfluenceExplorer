function USCALobbyingActivity(holder, filing, titleID) {
  function buildName(entityObj) {
    if (!entityObj) {
      return null
    }
    const nameKeys = ['prefix', 'first_name', 'last_name', 'suffix']
    let namePieces = []
    nameKeys.forEach(function (nameKey) {
      if (nameKey in entityObj && entityObj[nameKey]) {
        namePieces.push(entityObj[nameKey])
      }
    })
    if (namePieces.length) {
      return namePieces.join(' ')
    } else {
      return null
    }
  }

  function appendCover(parentHTML, parentID, expanded) {
    if (!filing.cover) {
      return
    }

    const buttonHTML = 'Cover Page'
    const label = 'cover'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    const cover = filing.cover
    const coverAnnotation = filing.annotation.cover
    const entityAnnotations = filing.annotation.entities

    // Form the filer feature.
    let filerLines = []
    if (cover.filer) {
      const filerName = buildName(cover.filer)
      if (filerName) {
        const entity = entityAnnotations[filerName.toLowerCase()]
        appendEntityAnnotationLines(filerLines, entity, filerName)
      }
    }
    if (cover.firm) {
      const addressStr = calaccessLobbyingAddressFromObject(cover.firm)
      if (addressStr) {
        filerLines.push(addressStr)
      }
    }
    if (cover.filer_id) {
      filerLines.push(`ID: ${cover.filer_id}`)
    } else if (cover.filer && cover.filer.id) {
      // The filer ID is listed two separate ways -- avoid redundancy.
      filerLines.push(`ID: ${cover.filer.id}`)
    }
    accordion.appendFeature('Filer', filerLines.join('<br/>'))

    // Build the signer string.
    let signerLines = []
    if (cover.signer) {
      if (cover.signer.date) {
        signerLines.push(dateWithoutTime(new Date(cover.signer.date)))
      }
      const signerName = buildName(cover.signer)
      if (signerName) {
        const entity = entityAnnotations[signerName.toLowerCase()]
        appendEntityAnnotationLines(signerLines, entity, signerName)
      }
      if (cover.signer_prn) {
        const signerNamePRN = buildName(cover.signer_prn)
        if (signerNamePRN && signerNamePRN != signerName) {
          const entity = entityAnnotations[signerNamePRN.toLowerCase()]
          appendEntityAnnotationLines(signerLines, entity, signerNamePRN, 'PRN')
        }
      }
      if (cover.signer.title) {
        signerLines.push(cover.signer.title)
      }
      if (cover.signer.location) {
        signerLines.push(cover.signer.location)
      }
    }
    accordion.appendFeature('Signed', signerLines.join('<br/>'))

    accordion.appendDate('Date Filed', cover.date_filed)
    if (cover.period_begin && cover.period_end) {
      accordion.appendPeriod('Period', cover.period_begin, cover.period_end)
    } else {
      accordion.appendDate('Period Begin', cover.period_begin)
      accordion.appendDate('Period End', cover.period_end)
    }
    accordion.appendDate('Cumulative Begin', cover.cumulative_begin)
    if (cover.activity_description) {
      accordion.appendFeature(
        'Activity Description',
        coverAnnotation.activityDescription.richText
      )
    }

    if (cover.memo) {
      if (cover.memo.text) {
        accordion.appendFeature('Memo Text', coverAnnotation.memo.text.richText)
      }
      accordion.appendFeature('Memo Form Type', cover.memo.form_type)
      accordion.appendFeature('Memo Reference', cover.memo.reference)
      accordion.appendFeature('Memo Line Item', cover.memo.line_item)
      accordion.appendFeature('Memo Amendment ID', cover.memo.amendment_id)
    }

    accordion.appendFeature('Form Type', cover.form_type)
    accordion.appendFeature('Sender ID', cover.sender_id)
    accordion.appendFeature('Entity Code', cover.entity_code)
    accordion.appendFeature('Amendment ID', cover.amendment_id)
  }

  function appendPayment(parentHTML, parentID, expanded) {
    if (!filing.payment) {
      return
    }

    const buttonHTML = 'Payment'
    const label = 'payment'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    const payment = filing.payment
    const paymentAnnotation = filing.annotation.payment
    const entityAnnotations = filing.annotation.entities

    const employerName = buildName(payment.employer)

    if (payment.employer) {
      let lines = []
      const nameStr = buildName(payment.employer)
      if (nameStr) {
        const entity = entityAnnotations[nameStr.toLowerCase()]
        appendEntityAnnotationLines(lines, entity, nameStr)
      }
      const addressStr = calaccessLobbyingAddressFromObject(payment.employer)
      if (addressStr) {
        lines.push(addressStr)
      }
      accordion.appendFeature('Employer', lines.join('<br/>'))
    }

    if (payment.activity_description) {
      accordion.appendFeature(
        'Activity Description',
        paymentAnnotation.activityDescription.richText
      )
    }

    if (payment.amount) {
      accordion.appendFeature(
        'Fees',
        '$' + numberWithCommas(payment.amount.fees),
        true,
        payment.amount.fees
      )
      accordion.appendFeature(
        'Period Total',
        '$' + numberWithCommas(payment.amount.period_total),
        true,
        payment.amount.period_total
      )
      accordion.appendFeature(
        'Cumulative Total',
        '$' + numberWithCommas(payment.amount.cumulative_total),
        true,
        payment.amount.cumulative_total
      )
    }

    if (payment.memo) {
      if (payment.memo.text) {
        accordion.appendFeature(
          'Memo Text',
          paymentAnnotation.memo.text.richText
        )
      }
      accordion.appendFeature('Memo Form Type', payment.memo.form_type)
      accordion.appendFeature('Memo Reference', payment.memo.reference)
      accordion.appendFeature('Memo Line Item', payment.memo.line_item)
      accordion.appendFeature('Memo Amendment ID', payment.memo.amendment_id)
    }

    accordion.appendFeature('Form Type', payment.form_type)
    accordion.appendFeature('Line Item', payment.line_item)
    if (payment.transaction) {
      accordion.appendFeature('Transaction ID', payment.transaction.id)
    }
    accordion.appendFeature('Amendment ID', payment.amendment_id)
  }

  function appendUnmatchedMemo(
    memo,
    memoAnnotation,
    memoIndex,
    parentHTML,
    parentID
  ) {
    const buttonHTML = `Memo ${memoIndex}`
    const expanded = false
    const label = 'memo'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label,
      memoIndex
    )

    if (memo.text) {
      accordion.appendFeature('Text', memoAnnotation.text.richText)
    }
    accordion.appendFeature('Form Type', memo.form_type)
    accordion.appendFeature('Reference', memo.reference)
    accordion.appendFeature('Line Item', memo.line_item)
    accordion.appendFeature('Amendment ID', memo.amendment_id)
  }

  function appendUnmatchedMemos(parentHTML, parentID, expanded) {
    if (!filing.unmatched_memos.length) {
      return
    }

    const buttonHTML = 'Unmatched Memos'
    const label = 'memos'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()
    let collapseID = accordion.collapseID()

    filing.unmatched_memos.forEach(function (memo, memoIndex) {
      appendUnmatchedMemo(
        memo,
        filing.annotation.unmatchedMemos[memoIndex],
        memoIndex,
        collapseBody,
        collapseID
      )
    })
  }

  function appendBills(parentHTML, parentID, expanded) {
    if (
      !filing.annotation.bills.assembly.length &&
      !filing.annotation.bills.assemblyConcurrent.length &&
      !filing.annotation.bills.senate.length &&
      !filing.annotation.bills.senateConcurrent.length
    ) {
      return
    }

    const buttonHTML = 'Referenced Bills'
    const label = 'bills'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    function addBillCategory(key, title) {
      if (!filing.annotation.bills[key].length) {
        return
      }
      // Generate a dictionary from bill URLs to labels.
      let billMap = {}
      filing.annotation.bills[key].forEach(function (bill) {
        billMap[bill.url] = bill.label
      })

      let listHTML = $('<ul>').css('padding-left', '1em')
      for (const [url, label] of Object.entries(billMap)) {
        listHTML.append(
          $('<li>').append($('<a>').attr('href', url).text(label))
        )
      }
      accordion.appendFeature(title, listHTML)
    }

    addBillCategory('assembly', 'Assembly Bills')
    addBillCategory('assemblyConcurrent', 'Assembly Concurrent Resolutions')
    addBillCategory('senate', 'Senate Bills')
    addBillCategory('senateConcurrent', 'Senate Concurrent Resolutions')
  }

  let content = holder.find('.modal-content')
  content.empty()

  const maxTitleLength = 250

  let employersStr = ''
  if (filing.payment && filing.payment.employer) {
    employersStr = buildName(filing.payment.employer)
  }
  const title = trimLabel(
    filing.filing_id + ': ' + employersStr,
    maxTitleLength
  )
  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'calaccessLobbyDiscAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendModalSimpleFeature(accordionDiv, 'Filing ID', filing.filing_id)

  appendCover(accordionDiv, accordionID, false)
  appendPayment(accordionDiv, accordionID, false)
  appendUnmatchedMemos(accordionDiv, accordionID, false)
  appendBills(accordionDiv, accordionID, false)
}
