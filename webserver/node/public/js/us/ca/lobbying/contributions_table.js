function USCALobbyingContributionsTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/us/ca/lobbying/contributions'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    function buildName(entityObj) {
      const nameKeys = ['prefix', 'first_name', 'last_name', 'suffix']
      let namePieces = []
      nameKeys.forEach(function (nameKey) {
        if (nameKey in entityObj && entityObj[nameKey]) {
          namePieces.push(entityObj[nameKey])
        }
      })
      if (namePieces.length) {
        return namePieces.join(' ')
      } else {
        return null
      }
    }

    let tableData = []
    filings.forEach(function (filing, filingIndex) {
      const filedDate = filing.cover
        ? dateWithoutTimeLex(filing.cover['filed_date'])
        : ''

      let ballotMeasure = ''
      if (
        filing.cover &&
        filing.cover.ballot_measure &&
        filing.cover.ballot_measure.name
      ) {
        ballotMeasure = filing.cover.ballot_measure.name
      }

      let filersStr = ''
      if (filing.cover && filing.cover.filer) {
        filersStr = buildName(filing.cover.filer)
      }

      let candidatesStr = ''
      if (filing.cover && filing.cover.candidate) {
        candidatesStr = buildName(filing.cover.candidate)
      }

      let contributorsStr = ''
      if (
        filing.campaign_contribution &&
        filing.campaign_contribution.contributor
      ) {
        const contributor = filing.campaign_contribution.contributor
        if (contributor.employer) {
          contributorsStr = contributor.employer
        } else {
          contributorsStr = buildName(contributor)
        }
      }

      let received = 0
      if (
        filing.campaign_contribution &&
        filing.campaign_contribution.amount &&
        'received' in filing.campaign_contribution.amount
      ) {
        received = filing.campaign_contribution.amount.received
      }

      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        filedDate,
        ballotMeasure,
        filersStr,
        candidatesStr,
        contributorsStr,
        received,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Date Filed' },
        { targets: 2, title: 'Ballot Measure' },
        { targets: 3, title: 'Filer' },
        { targets: 4, title: 'Candidate' },
        { targets: 5, title: 'Contributor' },
        {
          targets: 6,
          title: 'Received',
          render: $.fn.dataTable.render.number(',', '.', 2, '$'),
        },
        { targets: 7, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      USCALobbyingContributions(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
