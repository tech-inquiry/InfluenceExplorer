function USCALobbyingActivityTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/us/ca/lobbying/activity'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    function buildName(entityObj) {
      const nameKeys = ['prefix', 'first_name', 'last_name', 'suffix']
      let namePieces = []
      nameKeys.forEach(function (nameKey) {
        if (nameKey in entityObj && entityObj[nameKey]) {
          namePieces.push(entityObj[nameKey])
        }
      })
      if (namePieces.length) {
        return namePieces.join(' ')
      } else {
        return null
      }
    }

    let tableData = []
    filings.forEach(function (filing, filingIndex) {
      const filedDate = filing.cover
        ? dateWithoutTimeLex(filing.cover['date_filed'])
        : ''

      let filersStr = ''
      if (filing.cover && filing.cover.filer) {
        filersStr = buildName(filing.cover.filer)
      }

      let employersStr = ''
      if (filing.payment && filing.payment.employer) {
        employersStr = buildName(filing.payment.employer)
      }

      let activities = new Set()
      if (filing.cover && filing.cover.activity_description) {
        activities.add(filing.cover.activity_description)
      }
      if (filing.payment && filing.payment.activity_description) {
        activities.add(filing.payment.activity_description)
      }
      const activitiesStr = Array.from(activities).join('\n\n')

      let fees = 0
      if (
        filing.payment &&
        filing.payment.amount &&
        filing.payment.amount.fees
      ) {
        fees = Number(filing.payment.amount.fees)
      }

      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        filedDate,
        filersStr,
        employersStr,
        activitiesStr,
        fees,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Date Filed' },
        { targets: 2, title: 'Filers' },
        { targets: 3, title: 'Employers' },
        { targets: 4, title: 'Activities' },
        {
          targets: 5,
          title: 'Fees',
          render: $.fn.dataTable.render.number(',', '.', 2, '$'),
        },
        { targets: 6, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      USCALobbyingActivity(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
