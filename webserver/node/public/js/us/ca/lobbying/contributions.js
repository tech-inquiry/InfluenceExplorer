function USCALobbyingContributions(holder, filing, titleID) {
  function buildName(entityObj) {
    if (!entityObj) {
      return null
    }
    const nameKeys = ['prefix', 'first_name', 'last_name', 'suffix']
    let namePieces = []
    nameKeys.forEach(function (nameKey) {
      if (nameKey in entityObj && entityObj[nameKey]) {
        namePieces.push(entityObj[nameKey])
      }
    })
    if (namePieces.length) {
      return namePieces.join(' ')
    } else {
      return null
    }
  }

  function appendCover(parentHTML, parentID, expanded) {
    if (!filing.cover) {
      return
    }

    const buttonHTML = 'Cover Page'
    const label = 'cover'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    const cover = filing.cover
    const entityAnnotations = filing.annotation.entities

    if (cover.ballot_measure) {
      accordion.appendFeature('Ballot Measure', cover.ballot_measure.name)
      accordion.appendFeature(
        'Ballot Measure Number',
        cover.ballot_measure.number_or_letter
      )
      accordion.appendFeature(
        'Ballot Measure Jurisdiction',
        cover.ballot_measure.jurisdiction
      )
    }

    // Form the filer feature.
    let filerLines = []
    if (cover.filer) {
      const filerName = buildName(cover.filer)
      if (filerName) {
        const entity = entityAnnotations[filerName.toLowerCase()]
        appendEntityAnnotationLines(filerLines, entity, filerName)
      }
    }
    if (cover.firm) {
      const addressStr = calaccessLobbyingAddressFromObject(cover.firm)
      if (addressStr) {
        filerLines.push(addressStr)
      }
    }
    if (cover.filer_id) {
      filerLines.push(`ID: ${cover.filer_id}`)
    } else if (cover.filer && cover.filer.id) {
      // The filer ID is listed two separate ways -- avoid redundancy.
      filerLines.push(`ID: ${cover.filer.id}`)
    }
    accordion.appendFeature('Filer', filerLines.join('<br/>'))

    // Form the treasurer feature.
    let treasurerLines = []
    if (cover.treasurer) {
      const treasurerName = buildName(cover.treasurer)
      if (treasurerName) {
        const entity = entityAnnotations[treasurerName.toLowerCase()]
        appendEntityAnnotationLines(treasurerLines, entity, treasurerName)
      }
      const addressStr = calaccessLobbyingAddressFromObject(cover.treasurer)
      if (addressStr) {
        treasurerLines.push(addressStr)
      }
    }
    accordion.appendFeature('Treasurer', treasurerLines.join('<br/>'))

    // Form the candidate feature.
    let candidateLines = []
    if (cover.candidate) {
      const candidateName = buildName(cover.candidate)
      if (candidateName) {
        const entity = entityAnnotations[candidateName.toLowerCase()]
        appendEntityAnnotationLines(candidateLines, entity, candidateName)
      }
      const addressStr = calaccessLobbyingAddressFromObject(cover.candidate)
      if (addressStr) {
        candidateLines.push(addressStr)
      }
    }
    accordion.appendFeature('Candidate', candidateLines.join('<br/>'))

    if (cover.office) {
      accordion.appendFeature('Office Code', cover.office.code)
      accordion.appendFeature('Office Description', cover.office.description)
      accordion.appendFeature(
        'Office District Number',
        cover.office.district_number
      )
      accordion.appendFeature(
        'Office Jurisdiction Code',
        cover.office.jurisdiction_code
      )
      accordion.appendFeature(
        'Office Jurisdiction Description',
        cover.office.jurisdiction_description
      )
      accordion.appendFeature(
        'Office Sought or Held',
        cover.office.sought_or_held
      )
    }
    if (cover.yes_nos) {
      accordion.appendFeature(
        'Controlled Committee',
        cover.yes_nos.controlled_committee
      )
      accordion.appendFeature(
        'Sponsored Committee',
        cover.yes_nos.sponsored_committee
      )
    }
    if (cover.committee) {
      accordion.appendFeature('Committee Type', cover.committee.type)
    }

    // Build the signer string.
    let signerLines = []
    if (cover.signer) {
      if (cover.signer.date) {
        signerLines.push(dateWithoutTime(new Date(cover.signer.date)))
      }
      const signerName = buildName(cover.signer)
      if (signerName) {
        const entity = entityAnnotations[signerName.toLowerCase()]
        appendEntityAnnotationLines(signerLines, entity, signerName)
      }
      if (cover.signer_prn) {
        const signerNamePRN = buildName(cover.signer_prn)
        if (signerNamePRN && signerNamePRN != signerName) {
          const entity = entityAnnotations[signerNamePRN.toLowerCase()]
          appendEntityAnnotationLines(signerLines, entity, signerNamePRN, 'PRN')
        }
      }
      if (cover.signer.title) {
        signerLines.push(cover.signer.title)
      }
      if (cover.signer.location) {
        signerLines.push(cover.signer.location)
      }
    }
    accordion.appendFeature('Signed', signerLines.join('<br/>'))

    accordion.appendDate('Date Filed', cover.filed_date)
    accordion.appendDate('Election Date', cover.election_date)
    if (cover.period_begin && cover.period_end) {
      accordion.appendPeriod('Period', cover.period_begin, cover.period_end)
    } else {
      accordion.appendDate('Period Begin', cover.period_begin)
      accordion.appendDate('Period End', cover.period_end)
    }
    accordion.appendDate('Cumulative Begin', cover.cumulative_begin)

    if (cover.memo) {
      if (cover.memo.text) {
        accordion.appendFeature('Memo Text', cover.memo.text)
      }
      accordion.appendFeature('Memo Form Type', cover.memo.form_type)
      accordion.appendFeature('Memo Reference', cover.memo.reference)
      accordion.appendFeature('Memo Line Item', cover.memo.line_item)
      accordion.appendFeature('Memo Amendment ID', cover.memo.amendment_id)
    }

    accordion.appendFeature('Form Type', cover.form_type)
    accordion.appendFeature('Sender ID', cover.sender_id)
    accordion.appendFeature('Entity Code', cover.entity_code)
    accordion.appendFeature('Amendment ID', cover.amendment_id)
    accordion.appendFeature(
      'Amendment Explanation',
      cover.amendment_explanation
    )
  }

  function appendCampaignContribution(parentHTML, parentID, expanded) {
    if (!filing.campaign_contribution) {
      return
    }

    const payment = filing.campaign_contribution
    const entityAnnotations = filing.annotation.entities

    const buttonHTML = 'Campaign Contribution'
    const label = 'campaignContribution'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Description', payment.description)

    if (payment.support_or_oppose_code) {
      const suppOppCode = payment.support_or_oppose_code.toUpperCase()
      let suppOppDesc = ''
      if (suppOppCode == 'S') {
        suppOppDesc = 'Support'
      } else if (suppOppCode == 'O') {
        suppOppDesc = 'Oppose'
      } else {
        suppOppDesc = `Invalid Code (${suppOppCode})`
      }
      accordion.appendFeature('Support or Oppose', suppOppDesc)
    }

    if (payment.contributor) {
      let lines = []
      const nameStr = buildName(payment.contributor)
      if (nameStr) {
        const entity = entityAnnotations[nameStr.toLowerCase()]
        appendEntityAnnotationLines(lines, entity, nameStr)
      }
      const addressStr = calaccessLobbyingAddressFromObject(payment.contributor)
      if (addressStr) {
        lines.push(addressStr)
      }
      if (payment.contributor.occupation) {
        lines.push(`Occupation: ${payment.contributor.occupation}`)
      }
      if (payment.contributor.employer) {
        const entity =
          entityAnnotations[payment.contributor.employer.toLowerCase()]
        appendEntityAnnotationLines(
          lines,
          entity,
          payment.contributor.employer,
          'Employer'
        )
      }
      accordion.appendFeature('Contributor', lines.join('<br/>'))
    }

    if (payment.intermediary) {
      let lines = []
      const nameStr = buildName(payment.intermediary)
      if (nameStr) {
        const entity = entityAnnotations[nameStr.toLowerCase()]
        appendEntityAnnotationLines(lines, entity, nameStr)
      }
      const addressStr = calaccessLobbyingAddressFromObject(
        payment.intermediary
      )
      if (addressStr) {
        lines.push(addressStr)
      }
      if (payment.intermediary.employer) {
        const entity =
          entityAnnotations[payment.intermediary.employer.toLowerCase()]
        appendEntityAnnotationLines(
          lines,
          entity,
          payment.intermediary.employer,
          'Employer'
        )
      }
      accordion.appendFeature('Intermediary', lines.join('<br/>'))
    }

    if (payment.amount) {
      accordion.appendFeature(
        'Received',
        '$' + numberWithCommas(payment.amount.received),
        true,
        payment.amount.received
      )
      accordion.appendFeature(
        'Period Total',
        '$' + numberWithCommas(payment.amount.period_total),
        true,
        payment.amount.period_total
      )
      accordion.appendFeature(
        'Cumulative Total',
        '$' + numberWithCommas(payment.amount.cumulative_total),
        true,
        payment.amount.cumulative_total
      )
      accordion.appendFeature(
        'Cumulative Year-to-Date',
        '$' + numberWithCommas(payment.amount.cumulative_year_to_date),
        true,
        payment.amount.cumulative_year_to_date
      )
      accordion.appendFeature(
        'Cumulative Other',
        '$' + numberWithCommas(payment.amount.cumulative_other),
        true,
        payment.amount.cumulative_other
      )
    }

    accordion.appendDate('Date Received', payment.date_received)

    if (payment.memo) {
      if (payment.memo.text) {
        accordion.appendFeature('Memo Text', payment.memo.text)
      }
      accordion.appendFeature('Memo Form Type', payment.memo.form_type)
      accordion.appendFeature('Memo Reference', payment.memo.reference)
      accordion.appendFeature('Memo Line Item', payment.memo.line_item)
      accordion.appendFeature('Memo Amendment ID', payment.memo.amendment_id)
    }

    accordion.appendFeature('Form Type', payment.form_type)
    accordion.appendFeature('Line Item', payment.line_item)
    if (payment.transaction) {
      accordion.appendFeature('Transaction ID', payment.transaction.id)
      accordion.appendFeature('Transaction Type', payment.transaction.type)
    }
    accordion.appendFeature('Amendment ID', payment.amendment_id)
  }

  function appendUnmatchedMemo(memo, memoIndex, parentHTML, parentID) {
    const buttonHTML = `Memo ${memoIndex}`
    const expanded = false
    const label = 'memo'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label,
      memoIndex
    )

    if (memo.text) {
      accordion.appendFeature('Text', memo.text)
    }
    accordion.appendFeature('Form Type', memo.form_type)
    accordion.appendFeature('Reference', memo.reference)
    accordion.appendFeature('Line Item', memo.line_item)
    accordion.appendFeature('Amendment ID', memo.amendment_id)
  }

  function appendUnmatchedMemos(parentHTML, parentID, expanded) {
    if (!filing.unmatched_memos.length) {
      return
    }

    const buttonHTML = 'Unmatched Memos'
    const label = 'unmatchedMemos'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()
    let collapseID = accordion.collapseID()

    filing.unmatched_memos.forEach(function (memo, memoIndex) {
      appendUnmatchedMemo(memo, memoIndex, collapseBody, collapseID)
    })
  }

  let content = holder.find('.modal-content')
  content.empty()

  const maxTitleLength = 250

  let employersStr = ''
  if (filing.campaign_contribution && filing.campaign_contribution.employer) {
    employersStr = filing.campaign_contribution.employer
  }
  const title = trimLabel(
    `${filing.filing_id}: ${employersStr}`,
    maxTitleLength
  )
  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'calaccessCampaignContribAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendModalSimpleFeature(accordionDiv, 'Filing ID', filing.filing_id)

  appendCover(accordionDiv, accordionID, false)
  appendCampaignContribution(accordionDiv, accordionID, false)
  appendUnmatchedMemos(accordionDiv, accordionID, false)
}
