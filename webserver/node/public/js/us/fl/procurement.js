function USFLProcurement(holder, filing, titleID) {
  function appendActionDetails(parentHTML, parentID, expanded) {
    if (
      !(
        filing.type ||
        filing.annotation.awardURL ||
        filing.po_number ||
        filing.grant_award_id ||
        filing.flair_contract_id ||
        filing.state_term_contract_id ||
        filing.agency_contract_id ||
        filing.commodity_type_code ||
        filing.commodity_type_description ||
        filing.authorized_advanced_payment ||
        filing.method_of_procurement ||
        filing.contract_exemption_explanation ||
        filing.statutory_authority ||
        filing.recipient_type ||
        filing.business_case_study ||
        filing.involves_gov_aid ||
        filing.previously_done_by_state ||
        filing.considered_for_insourcing ||
        filing.improvement_on_state_property ||
        filing.improvement_description ||
        filing.improvement_value ||
        filing.improvement_unamortized_value ||
        filing.provide_admin_cost ||
        filing.admin_cost_percent ||
        filing.provide_periodic_increase ||
        filing.periodic_increase_percent ||
        filing.cfda_code ||
        filing.cfda_description ||
        filing.csfa_code ||
        filing.csfa_description
      )
    ) {
      return
    }

    const buttonHTML = 'Contract Action Details'
    const label = 'action'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Contract Action Type', filing.type)
    accordion.appendFeature('PO Number', filing.po_number)
    accordion.appendFeature('Grant Award Number', filing.grant_award_id)
    accordion.appendFeature('FLAIR Contract ID', filing.flair_contract_id)
    accordion.appendFeature(
      'State Term Contract ID',
      filing.state_term_contract_id
    )
    accordion.appendFeature('Commodity Type', filing.commodity_type)
    accordion.appendFeature(
      'Commodity Type Description',
      filing.commodity_type_description
    )
    accordion.appendFeature(
      'Authorized Advanced Payment',
      filing.authorized_advanced_payment
    )
    accordion.appendFeature(
      'Method of Procurement',
      filing.method_of_procurement
    )
    accordion.appendFeature(
      'Contract Exemption Explanation',
      filing.contract_exemption_explanation
    )
    accordion.appendFeature('Statutory Authority', filing.statutory_authority)
    accordion.appendFeature('Recipient Type', filing.recipient_type)
    accordion.appendFeature('Business Case Study', filing.business_case_study)
    accordion.appendFeature('Involves Government Aid', filing.involves_gov_aid)
    accordion.appendFeature(
      'Previously Done by State',
      filing.previously_done_by_state
    )
    accordion.appendFeature(
      'Considered for Insourcing',
      filing.considered_for_insourcing
    )
    accordion.appendFeature(
      'Improvement on State Property',
      filing.improvement_on_state_property
    )
    accordion.appendFeature(
      'Improvement Description',
      filing.improvement_description
    )
    accordion.appendFeature(
      'Improvement Value',
      numberWithCommas(filing.improvement_value) + ' USD',
      true,
      filing.improvement_value
    )
    accordion.appendFeature(
      'Improvement Unamortized Value',
      filing.improvement_unamortized_value
    )

    accordion.appendFeature(
      'Provide Administrative Cost',
      filing.provide_admin_cost
    )
    accordion.appendFeature(
      'Administrative Cost Percent',
      filing.admin_cost_percent
    )
    accordion.appendFeature(
      'Provide for Periodic Increase',
      filing.provide_periodic_increase
    )
    accordion.appendFeature(
      'Periodic Increase Percent',
      filing.periodic_increase_percent
    )
    accordion.appendFeature('CFDA Code', filing.cfda_code)
    accordion.appendFeature('CFDA Description', filing.cfda_description)
    accordion.appendFeature('CSFA Code', filing.csfa_code)
    accordion.appendFeature('CSFA Description', filing.csfa_description)
  }

  function appendAmounts(parentHTML, parentID, expanded) {
    if (
      !(
        filing.orig_contract_amount ||
        filing.total_amount ||
        filing.recurring_budgetary_amount ||
        filing.non_recurring_budgetary_amount ||
        filing.po_budget_amount
      )
    ) {
      return
    }

    const buttonHTML = 'Contract Amounts'
    const label = 'amounts'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature(
      'Original Contract Amount',
      numberWithCommas(filing.orig_contract_amount) + ' USD',
      true,
      filing.orig_contract_amount
    )
    accordion.appendFeature(
      'Total Amount',
      numberWithCommas(filing.total_amount) + ' USD',
      true,
      filing.total_amount
    )
    accordion.appendFeature(
      'Recurring Budgetary Amount',
      numberWithCommas(filing.recurring_budgetary_amount) + ' USD',
      true,
      filing.recurring_budgetary_amount
    )
    accordion.appendFeature(
      'Non-Recurring Budgetary Amount',
      numberWithCommas(filing.non_recurring_budgetary_amount) + ' USD',
      true,
      filing.non_recurring_budgetary_amount
    )
    accordion.appendFeature(
      'PO Budget Amount',
      numberWithCommas(filing.po_budget_amount) + ' USD',
      true,
      filing.po_budget_amount
    )
  }

  function appendDates(parentHTML, parentID, expanded) {
    if (
      !(
        filing.grant_award_date ||
        filing.po_award_date ||
        filing.begin_date ||
        filing.original_end_date ||
        filing.new_end_date ||
        filing.contract_execution_date
      )
    ) {
      return
    }

    const buttonHTML = 'Contract Dates'
    const label = 'dates'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature(
      'Grant Award Date',
      dateWithoutTime(new Date(filing.grant_award_date)),
      true,
      filing.grant_award_date
    )
    accordion.appendFeature(
      'PO Award Date',
      dateWithoutTime(new Date(filing.po_award_date)),
      true,
      filing.po_award_date
    )
    accordion.appendFeature(
      'Begin Date',
      dateWithoutTime(new Date(filing.begin_date)),
      true,
      filing.begin_date
    )
    accordion.appendFeature(
      'Original End Date',
      dateWithoutTime(new Date(filing.original_end_date)),
      true,
      filing.original_end_date
    )
    accordion.appendFeature(
      'New End Date',
      dateWithoutTime(new Date(filing.new_end_date)),
      true,
      filing.new_end_date
    )
    accordion.appendFeature(
      'Contract Execution Date',
      dateWithoutTime(new Date(filing.contract_execution_date)),
      true,
      filing.contract_execution_date
    )
    accordion.appendFeature(
      'Business Case Date',
      dateWithoutTime(new Date(filing.business_case_date)),
      true,
      filing.business_case_date
    )
  }

  function appendAgency(parentHTML, parentID, expanded) {
    if (
      !(
        filing.agency ||
        filing.agency_service_area ||
        filing.agency_reference_number ||
        filing.agency_contract_id
      )
    ) {
      return
    }

    let buttonHTML = 'Contracting Agency'
    if (filing.agency) {
      buttonHTML += ': ' + filing.agency
    }
    const label = 'agency'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Contracting Agency', filing.agency)
    accordion.appendFeature('Agency Service Area', filing.agency_service_area)
    accordion.appendFeature(
      'Agency Reference Number',
      filing.agency_reference_number
    )
    accordion.appendFeature('Agency Contract ID', filing.agency_contract_id)
  }

  function appendVendor(parentHTML, parentID, expanded) {
    if (!filing.vendor) {
      return
    }

    const buttonHTML = `Vendor: ${filing.vendor}`
    const label = 'vendor'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    let subtext = $('<span>')
      .css('font-style', 'italic')
      .css('font-size', '100%')
    if (
      filing.annotation.vendor.text.toLowerCase() != filing.vendor.toLowerCase()
    ) {
      subtext.append(`Vendor as written: "${filing.vendor}", `)
    }

    let vendorValue = $('<p>')
      .addClass('card-text modal-card-text')
      .append(filing.annotation.vendor.logoHTML)
      .append(
        $('<a>')
          .attr(
            'href',
            encodeEntity(filing.annotation.vendor.text, EXPLORER_ROUTE)
          )
          .text(filing.annotation.vendor.stylizedText)
      )
      .append('<br/><br/>')
      .append(subtext)

    collapseBody.append(vendorValue)
  }

  function appendDescription(parentHTML, parentID, expanded) {
    if (
      !(
        filing.long_title ||
        filing.short_title ||
        filing.comment ||
        filing.legal_challenges ||
        filing.legal_challenge_description
      )
    ) {
      return
    }

    const maxLabelLength = 50
    let buttonHTML = 'Description'
    if (filing.comment) {
      buttonHTML += ': ' + trimLabel(filing.comment, maxLabelLength)
    }

    const label = 'description'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Long Title', filing.long_title)
    accordion.appendFeature('Short Title', filing.short_title)
    accordion.appendFeature('Comment', filing.comment)

    accordion.appendFeature('Legal Challenges', filing.legal_challenges)
    accordion.appendFeature(
      'Legal Challenge Description',
      filing.legal_challenge_description
    )
  }

  let content = holder.find('.modal-content')
  content.empty()

  let title = ''
  if (filing.long_title) {
    title = filing.long_title
    if (filing.short_title) {
      title +=
        '<br/>' + `<span style="font-size: 100%;">${filing.short_title}</span>`
    }
  } else if (filing.short_title) {
    title = filing.short_title
  }
  if (filing.annotation.awardURL) {
    title +=
      `<a href="${filing.annotation.awardURL}">` +
      '<img src="/logos/document.jpg" ' +
      'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" />' +
      '</a>'
  }

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'flAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendActionDetails(accordionDiv, accordionID, false)
  appendAmounts(accordionDiv, accordionID, false)
  appendDates(accordionDiv, accordionID, false)
  appendVendor(accordionDiv, accordionID, false)
  appendAgency(accordionDiv, accordionID, false)
  appendDescription(accordionDiv, accordionID, true)
}
