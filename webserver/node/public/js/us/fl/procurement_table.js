function USFLProcurementTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/us/fl/procurement'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    let tableData = []
    filings.forEach(function (filing, filingIndex) {
      const beginDate = filing.begin_date
        ? dateWithoutTimeLex(filing.begin_date)
        : ''
      const vendor = filing.vendor ? filing.vendor : ''
      const agency = filing.agency ? filing.agency : ''
      const totalAmount =
        filing.total_amount !== undefined ? filing.total_amount : ''
      const longTitle = filing.long_title ? filing.long_title : ''
      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        beginDate,
        vendor,
        agency,
        totalAmount,
        longTitle,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Begin Date' },
        { targets: 2, title: 'Vendor' },
        { targets: 3, title: 'Agency' },
        {
          targets: 4,
          title: 'Total Amount',
          render: $.fn.dataTable.render.number(',', '.', 2, '$'),
        },
        { targets: 5, title: 'Long Title' },
        { targets: 6, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      USFLProcurement(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
