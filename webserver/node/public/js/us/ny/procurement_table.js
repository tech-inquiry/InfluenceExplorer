function USNYProcurementTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/us/ny/procurement'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    let tableData = []
    filings.forEach(function (filing, filingIndex) {
      const startDate = filing.start_date
        ? dateWithoutTimeLex(filing.start_date)
        : ''
      const contractor = filing.contractor ? filing.contractor : ''
      const groupNumber = filing.group_number ? filing.group_number : ''
      const awardNumber = filing.award_number ? filing.award_number : ''
      const contractNumber = filing.contract_number
        ? filing.contract_number
        : ''
      const description = filing.description ? filing.description : ''
      tableData.push([
        filingIndex,
        startDate,
        contractor,
        groupNumber,
        awardNumber,
        contractNumber,
        description,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Start Date' },
        { targets: 2, title: 'Contractor' },
        { targets: 3, title: 'Group Number' },
        { targets: 4, title: 'Award Number' },
        { targets: 5, title: 'Contract Number' },
        { targets: 6, title: 'Description' },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      USNYProcurement(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
