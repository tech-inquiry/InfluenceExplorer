function USNYProcurement(holder, filing, titleID) {
  function appendActionDetails(parentHTML, parentID, expanded) {
    if (!(filing.start_date || filing.end_date)) {
      return
    }

    const label = 'actionDetails'
    const buttonHTML = 'Contract Action Details'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (filing.group_number > 0) {
      let groupDiv = $('<div>')
        .append(filing.group_number)
        .append(
          $('<span>')
            .css('font-style', 'italic')
            .css('font-size', '90%')
            .append('&nbsp;&nbsp;Copy and paste into Group Number field on ')
            .append(
              $('<a>')
                .attr(
                  'href',
                  'https://online.ogs.ny.gov/Purchase/Search/default.asp'
                )
                .text('NY OGS')
            )
            .append(' for more info.')
        )

      accordion.appendFeature('Group Number', groupDiv)
    }

    if (filing.award_number > 0) {
      let awardDiv = $('<div>')
        .append(filing.award_number)
        .append(
          $('<span>')
            .css('font-style', 'italic')
            .css('font-size', '90%')
            .append('&nbsp;&nbsp;Copy and paste into Award Number field on ')
            .append(
              $('<a>')
                .attr(
                  'href',
                  'https://online.ogs.ny.gov/Purchase/Search/default.asp'
                )
                .text('NY OGS')
            )
            .append(' for more info.')
        )

      accordion.appendFeature('Award Number', awardDiv)
    }

    accordion.appendFeature('Contract Number', filing.contract_number)
  }

  function appendDates(parentHTML, parentID, expanded) {
    if (!(filing.start_date || filing.end_date)) {
      return
    }

    const label = 'dates'
    const buttonHTML = 'Contract Dates'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature(
      'Contract Period',
      dateWithoutTime(new Date(filing.start_date)) +
        ' - ' +
        dateWithoutTime(new Date(filing.end_date))
    )
  }

  function appendVendor(parentHTML, parentID, expanded) {
    if (!filing.contractor) {
      return
    }

    const maxTitleLength = 50

    const label = 'contractor'
    const buttonHTML = trimLabel('Vendor: ' + filing.contractor, maxTitleLength)
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    let nameValue = $('<div>')
      .append(filing.annotation.contractor.logoHTML)
      .append(
        $('<a>')
          .attr('href', filing.annotation.contractor.url)
          .text(filing.annotation.contractor.stylizedText)
      )
    if (
      filing.annotation.contractor.text !=
      filing.annotation.contractor.origText.toLowerCase()
    ) {
      nameValue
        .append('<br/><br/>')
        .append(
          $('<span>')
            .css('font-style', 'italic')
            .css('font-size', '90%')
            .text(`As written: "${filing.contractor}"`)
        )
    }

    accordion.appendFeature('Vendor', nameValue)
  }

  function appendDescription(parentHTML, parentID, expanded) {
    if (!filing.description) {
      return
    }

    const maxDescriptionLength = 50

    const label = 'description'
    const buttonHTML = trimLabel(
      'Description: ' + filing.description,
      maxDescriptionLength
    )
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Description', filing.description)
  }

  let content = holder.find('.modal-content')
  content.empty()

  const maxTitleLength = 200
  const title = trimLabel(
    filing.contractor + ': ' + filing.description,
    maxTitleLength
  )

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'flAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendActionDetails(accordionDiv, accordionID, false)
  appendDates(accordionDiv, accordionID, false)
  appendVendor(accordionDiv, accordionID, false)
  appendDescription(accordionDiv, accordionID, true)
}
