function USSecuritiesTable(data, containerID, tableID, dialogID, dialogWidth) {
  $.when(
    $.ajax({
      url: appendURLPath(EXPLORER_ROUTE, 'api/us/central/securities/meta'),
      data: data,
    }),
    $.ajax({
      url: appendURLPath(EXPLORER_ROUTE, 'api/us/central/securities/filings'),
      data: data,
    })
  ).then(function (metaResult, filingsResult) {
    const metaItems = metaResult[0].filings
    const filingItems = filingsResult[0].filings

    fillWithResponsiveTable(containerID, tableID)

    let tableData = []
    filingItems.forEach(function (filing, filingIndex) {
      // Extract the company name out of one of the matching meta results.
      let name = ''
      for (const meta of metaItems) {
        if (meta.cik == filing.cik) {
          name = meta.name
        }
      }

      tableData.push([
        filingIndex,
        filing.filing_date,
        filing.report_date,
        filing.cik,
        name,
        filing.form,
        filing.primary_doc_description,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Filing Date' },
        { targets: 2, title: 'Report Date' },
        { targets: 3, title: 'CIK' },
        { targets: 4, title: 'Name' },
        { targets: 5, title: 'Type' },
        { targets: 6, title: 'Description' },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filingItems[filingIndex]
      const urlEnd = `/Archives/edgar/data/${
        filing.cik
      }/${filing.accession.replaceAll('-', '')}/${filing.primary_document}`
      const url = filing.is_inline_xbrl
        ? `https://www.sec.gov/ix?doc=${urlEnd}`
        : `https://www.sec.gov/${urlEnd}`
      let win = window.open(url)
      if (win) {
        win.focus()
      } else {
        alert('Please allow popups to allow links to SEC filings.')
      }
    })
  })
}
