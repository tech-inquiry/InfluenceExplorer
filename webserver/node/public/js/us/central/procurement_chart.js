function USFederalAwardsChart(
  holder,
  aggregates,
  chartTitlePrefix,
  chartSubject,
  relativeParentID,
  maxLabels = 25,
  summarySelectID = 'awardSummaryType',
  amountSelectID = 'awardAmountType',
  chartID = 'awardsChart',
  chartContainerID = 'awardsChartContainer'
) {
  holder.empty()

  // Two separate selectors:
  //   Summary type: {year, agency, vendorInclusive, vendorExclusive},
  //   Amount type: {obligated, exercised, potential}

  const summaryTitlePiece = {
    year: 'Year',
    agency: 'Agency',
    vendorExclusive: "Vendor (excluding subsid's)",
    vendorInclusive: "Vendor (including subsid's)",
  }

  const amountTitlePiece = {
    obligated: 'Obligated',
    exercised: 'Exercised',
    potential: 'Potential',
  }

  function extractAmount(x, amountType) {
    if (amountType == 'obligated') {
      return x.obligatedAmount
    } else if (amountType == 'exercised') {
      return x.exercisedAmount
    } else {
      return x.potentialAmount
    }
  }

  function prepAggregates(summaryType, amountType) {
    function prepAwardsByYear() {
      let rolledAwards = []
      aggregates['byYear'].forEach(function (item) {
        rolledAwards.push({
          label: item.label,
          amount: extractAmount(item, amountType),
        })
      })
      rolledAwards = rolledAwards.sort(function (x, y) {
        return d3.ascending(x.label, y.label)
      })
      sliceFromEnd(rolledAwards, maxLabels)
      return rolledAwards
    }

    function prepAwardsByAgency() {
      let rolledAwards = []
      aggregates['byAgency'].forEach(function (item) {
        rolledAwards.push({
          label: item.label,
          amount: extractAmount(item, amountType),
        })
      })
      rolledAwards = rolledAwards.sort(function (x, y) {
        return d3.descending(x.amount, y.amount)
      })
      return rolledAwards.slice(0, maxLabels)
    }

    function prepAwardsByVendorExclusive() {
      let rolledAwards = []
      aggregates['byVendorExclusive'].forEach(function (item) {
        rolledAwards.push({
          label: item.label,
          amount: extractAmount(item, amountType),
        })
      })
      rolledAwards = rolledAwards.sort(function (x, y) {
        return d3.descending(x.amount, y.amount)
      })
      return rolledAwards.slice(0, maxLabels)
    }

    function prepAwardsByVendorInclusive() {
      let rolledAwards = []
      aggregates['byVendorInclusive'].forEach(function (item) {
        rolledAwards.push({
          label: item.label,
          amount: extractAmount(item, amountType),
        })
      })
      rolledAwards = rolledAwards.sort(function (x, y) {
        return d3.descending(x.amount, y.amount)
      })
      return rolledAwards.slice(0, maxLabels)
    }

    if (summaryType == 'year') {
      return prepAwardsByYear()
    } else if (summaryType == 'agency') {
      return prepAwardsByAgency()
    } else if (summaryType == 'vendorExclusive') {
      return prepAwardsByVendorExclusive()
    } else {
      return prepAwardsByVendorInclusive()
    }
  }

  const amountLabels = ['obligated', 'exercised', 'potential']
  const proposedSummaryLabels = [
    'year',
    'agency',
    'vendorExclusive',
    'vendorInclusive',
  ]

  const summaryLabels = []
  const amountTypes = {}
  proposedSummaryLabels.forEach(function (summaryLabel) {
    if (prepAggregates(summaryLabel, 'obligated').length) {
      summaryLabels.push(summaryLabel)
    }
    amountLabels.forEach(function (amountLabel) {
      const key = JSON.stringify([summaryLabel, amountLabel])
      amountTypes[key] = {
        data: prepAggregates(summaryLabel, amountLabel),

        title:
          chartTitlePrefix +
          amountTitlePiece[amountLabel] +
          ' Amount by ' +
          summaryTitlePiece[summaryLabel] +
          ' for ' +
          chartSubject,

        // Whether data arrays should be truncated from the back.
        sliceFromEnd: summaryLabel == 'year',
      }
    })
  })

  let summarySelect = $('<select>')
    .attr('id', summarySelectID)
    .css('font-size', '140%')
  if (summaryLabels.indexOf('year') > -1) {
    summarySelect.append($('<option>', { value: 'year', text: 'Year' }))
  }
  if (summaryLabels.indexOf('agency') > -1) {
    summarySelect.append(
      $('<option>', {
        value: 'agency',
        text: 'Agency',
        selected: 'selected',
      })
    )
  }
  if (summaryLabels.indexOf('vendorExclusive') > -1) {
    summarySelect.append(
      $('<option>', {
        value: 'vendorExclusive',
        text: "Vendor (excluding subsid's)",
      })
    )
  }
  if (summaryLabels.indexOf('vendorInclusive') > -1) {
    summarySelect.append(
      $('<option>', {
        value: 'vendorInclusive',
        text: "Vendor (including subsid's)",
      })
    )
  }
  holder
    .append(
      $('<div>')
        .addClass('d3-chart-select')
        .append(
          $('<span>')
            .addClass('bold')
            .css('font-size', '140%')
            .css('margin-right', '1em')
            .text('Summary Type')
        )
        .append(summarySelect)
        .append(
          $('<span>')
            .addClass('bold')
            .css('font-size', '140%')
            .css('margin-left', '2em')
            .css('margin-right', '1em')
            .text('Amount Type')
        )
        .append(
          $('<select>')
            .attr('id', amountSelectID)
            .css('font-size', '140%')
            .append($('<option>', { value: 'obligated', text: 'Obligated' }))
            .append($('<option>', { value: 'exercised', text: 'Exercised' }))
            .append(
              $('<option>', {
                value: 'potential',
                text: 'Potential',
                selected: 'selected',
              })
            )
        )
    )
    .append(
      $('<div>')
        .addClass('d3-chart-container')
        .attr('id', chartContainerID)
        .append($('<div>').attr('id', chartID).css('display', 'inline-block'))
    )

  holder
    .append(
      $('<div>')
        .addClass('filing_description')
        .append(
          '* Due to space constraints, Tech Inquiry only mirrors contracts ' +
            'modified since 2016, and so aggregate amounts before 2016 are likely ' +
            'to be incomplete.'
        )
    )
    .append(
      $('<div>')
        .addClass('filing_description')
        .append('* The total ')
        .append(
          $('<a>')
            .attr(
              'href',
              'https://fedspendingtransparency.github.io/whitepapers/amount/'
            )
            .text("'potential'")
        )
        .append(
          ' contract value figure requires the most care -- and, ideally, manual ' +
            'analysis -- as there can be double-counting between the potential ' +
            'value of a parent award (e.g., an Indefinite Delivery Contract) and ' +
            'one of its subawards (e.g., a Delivery Order). But, due to disk space ' +
            'constraints, we cannot store all contract information and have only ' +
            'uploaded subaward linkages back to mid-2019. For this reason and ' +
            'others, for the sake of consistency we compute the sum of the ' +
            'potential contract values in the naive manner with the understanding ' +
            'that it is usually only a slight overestimate of a number which is, ' +
            'even when carefully computed, often a very loose upper bound on ' +
            'eventually exercised options values.'
        )
    )
    .append(
      $('<div>')
        .addClass('filing_description')
        .append('Source: ')
        .append(
          $('<a>')
            .attr('href', 'https://www.fpds.gov/wiki/index.php/ATOM_Feed_FAQ')
            .text('FPDS ATOM Feed')
        )
        .append(' (updated daily around 2AM ET)<br/>See the ')
        .append(
          $('<a>')
            .attr(
              'href',
              'https://en.wikipedia.org/wiki/Government_Accountability_Office'
            )
            .text("U.S. Government Accountability Office's")
        )
        .append(
          $('<a>')
            .attr('href', 'https://www.gao.gov/products/GAO-20-75')
            .text('report on "known data limitations"')
        )
        .append(' for critical analysis of the accuracy of ')
        .append($('<a>').attr('href', 'https://www.fpds.gov/').text('FPDS-NG'))
        .append(' data, which is also upstream of ')
        .append(
          $('<a>')
            .attr(
              'href',
              'https://en.wikipedia.org/wiki/Federal_Funding_Accountability_and_Transparency_Act_of_2006'
            )
            .text('USASpending.gov')
        )
        .append('.')
    )

  const chartConfig = {
    // The ID of the select for the award type.
    selectIDs: ['#' + summarySelectID, '#' + amountSelectID],

    // The ID of the chart to draw the SVG in.
    chartID: '#' + chartID,

    // The ID of the relative parent div we will compute mouse positions
    // relative to.
    relativeParentID: relativeParentID,

    // The font size of the title.
    titleFontSize: '28px',

    // The font size of the title on small screens.
    titleFontSizeSmallScreen: '22px',

    // The font weight of the title.
    titleFontWeight: '700',

    // The title of the y axis.
    yAxisTitle: 'Amount (USD)',

    // The font size of the y-axis title.
    yAxisTitleFontSize: '25px',

    // The font size of the title on small screens.
    yAxisTitleFontSizeSmallScreen: '20px',

    // The font weight of the y-axis title.
    yAxisTitleFontWeight: '700',

    // The font size of the x-axis labels.
    xLabelFontSize: '12px',

    // The font weight of the x-axis labels.
    xLabelFontWeight: '700',

    // The font size of the y-axis labels.
    yLabelFontSize: '16px',

    // The font weight of the y-axis labels.
    yLabelFontWeight: '700',

    // How many pixels left of the mouse to place the top-left corner of the
    // tooltip.
    tooltipHorizontalOffset: 50,

    // How many pixels above the mouse to place the top-left corner of the
    // tooltip.
    tooltipVerticalOffset: 45,

    // The maximum width (in pixels) of any bar in the bar chart.
    maxBarWidth: 120,

    // The minimum height (in pixels) of any bar in the bar chart.
    minBarHeight: 4,

    // The height (in pixels) of the inner portion of the chart.
    innerChartHeight: 400,

    // The chart width (in pixels) for large screens.
    chartWidth: 1250,

    // The chart width (in pixels) for small screens.
    smallChartWidth: 800,

    // The largest number of width pixels a screen can have before it's not
    // considered 'small'.
    smallScreenWidth: 1500,

    // The maximum number of labels to keep in the bar chart.
    maxSmallScreenLabels: 15,

    // The number of pixels in the top margin.
    marginTop: 100,

    // The number of pixels in the right margin.
    marginRight: 60,

    // The minimum number of pixels in the left margin.
    minMarginLeft: 120,

    // The maximum number of pixels in the left margin.
    maxMarginLeft: 200,

    // The number of pixels the left-most text should begin to the right of
    // the chart.
    marginLeftOffset: -20,

    // The minimum number of pixels in the bottom margin of the chart.
    minMarginBottom: 70,

    // The number of margin pixels per character of the labels.
    marginPerChar: 7,

    // The interior color of the bars in the chart.
    barColor: '#007FFF',

    // The interior color of the bars in the chart when moused over.
    barColorMouseover: 'blue',

    // The color of the borders of the bars in the chart.
    barBorderColor: '#000000',

    // The number of pixels of the borders of the bars in the chart.
    barBorderWidth: 2,

    xLabelFormat: function (d) {
      return d3.format('.3s')(d).replace('G', 'B')
    },

    tooltipHTML: function (d) {
      return '$' + d3.format('.3s')(d.amount).replace('G', 'B')
    },
  }

  BarChart(amountTypes, chartConfig)
}
