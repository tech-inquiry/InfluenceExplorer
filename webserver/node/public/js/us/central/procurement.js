const USASPENDING_QUERY_BASE = 'https://www.usaspending.gov/keyword_search/'
const FPDS_QUERY_BASE =
  'https://www.fpds.gov/ezsearch/search.do?indexName=awardfull&' +
  'templateName=1.5.2&s=FPDS.GOV&q='

function USFederalAward(holder, filing, titleID) {
  function appendActionType(accordion) {
    if (!filing.contract_action_type) {
      return
    }
    let actionTypeValue = undefined
    if (filing.annotation.actionType.url) {
      actionTypeValue = $('<a>')
        .attr('href', filing.annotation.actionType.url)
        .text(filing.annotation.actionType.stylizedText)
    } else {
      actionTypeValue = filing.annotation.actionType.stylizedText
    }
    accordion.appendFeature('Contract Action Type', actionTypeValue)
  }

  function appendDates(parentHTML, parentID) {
    const buttonHTML = 'Contract Dates'
    const expanded = false
    const label = 'dates'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendDate('Signed Date', filing.signed_date)
    accordion.appendDate('Modified Date', filing.modified_date)
    accordion.appendDate('Effective Date', filing.effective_date)
    accordion.appendDate(
      'Current Completion Date',
      filing.current_completion_date
    )
    accordion.appendDate(
      'Ultimate Completion Date',
      filing.ultimate_completion_date
    )
    accordion.appendDate('Last Date to Order', filing.last_date_to_order)
  }

  function appendValues(parentHTML, parentID) {
    const AMOUNT_URL =
      'https://fedspendingtransparency.github.io/whitepapers/amount/'

    const buttonHTML = 'Contract Amounts (USD)'
    const expanded = false
    const label = 'values'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    let table = $('<table>').addClass(
      'table table-striped table-fit modal-table'
    )
    collapseBody.append(table)

    let thead = $('<thead>')
    table.append(thead)
    thead.append(
      $('<tr>')
        .append($('<th>'))
        .append(
          $('<th>')
            .attr('scope', 'col')
            .append($('<a>').attr('href', AMOUNT_URL).text('Obligated'))
        )
        .append(
          $('<th>')
            .attr('scope', 'col')
            .append($('<a>').attr('href', AMOUNT_URL).text('Exercised'))
        )
        .append(
          $('<th>')
            .attr('scope', 'col')
            .append($('<a>').attr('href', AMOUNT_URL).text('Potential'))
        )
    )

    let tbody = $('<tbody>')
    table.append(tbody)
    tbody
      .append(
        $('<tr>')
          .append($('<th>').attr('scope', 'row').text('Modification'))
          .append($('<td>').text(numberWithCommas(filing.obligated_amount)))
          .append(
            $('<td>').text(
              numberWithCommas(filing.base_and_exercised_options_value)
            )
          )
          .append(
            $('<td>').text(numberWithCommas(filing.base_and_all_options_value))
          )
      )
      .append(
        $('<tr>')
          .append($('<th>').attr('scope', 'row').text('Total'))
          .append(
            $('<td>').text(numberWithCommas(filing.total_obligated_amount))
          )
          .append(
            $('<td>').text(
              numberWithCommas(filing.total_base_and_exercised_options_value)
            )
          )
          .append(
            $('<td>').text(
              numberWithCommas(filing.total_base_and_all_options_value)
            )
          )
      )
  }

  function appendActionDetails(parentHTML, parentID) {
    if (!(filing.piid || filing.parent_piid)) {
      return
    }

    const buttonHTML = 'Contract Action Details'
    const expanded = false
    const label = 'action'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    appendActionType(accordion)

    let identifierTable = $('<table>').addClass(
      'table table-striped table-fit modal-table'
    )
    let identifierTHead = $('<thead>')
    identifierTable.append(identifierTHead)
    let row = $('<tr>')
    identifierTHead.append(row)
    if (filing.parent_piid) {
      row.append('<th>')
    }
    row.append($('<th>').attr('scope', 'col').text('Procurement Instrument'))
    row.append($('<th>').attr('scope', 'col').text('Modification'))
    row.append(
      $('<th>')
        .attr('scope', 'col')
        .append(
          $('<a>')
            .attr('href', 'https://usaspending.gov/')
            .html(
              $('<img>')
                .attr('src', '/logos/usaspending_gov.png')
                .css('margin-left', '0.2em')
                .css('vertical-align', 'sub')
                .css('height', '1.2em')
            )
        )
    )
    row.append(
      $('<th>')
        .attr('scope', 'col')
        .append($('<a>').attr('href', 'https://www.fpds.gov/').text('fpds.gov'))
    )

    let identifierTBody = $('<tbody>')
    identifierTable.append(identifierTBody)
    if (filing.piid) {
      let row = $('<tr>')
      identifierTBody.append(row)
      if (filing.parent_piid) {
        row.append($('<th>').attr('scope', 'row'))
      }
      row.append(
        $('<td>').html(
          $('<a>')
            .attr('href', encodeSearch(filing.piid, EXPLORER_ROUTE))
            .text(filing.piid)
        )
      )
      row.append($('<td>').text(filing.mod_number))

      if (
        filing.contract_action_type == 'other transaction agreement' ||
        filing.contract_action_type == 'other transaction idv'
      ) {
        row.append($('<td>').text('N/A for OTAs'))
      } else {
        row.append(
          $('<td>').html(
            $('<a>')
              .attr('href', USASPENDING_QUERY_BASE + filing.piid)
              .text('Link')
          )
        )
      }

      row.append(
        $('<td>').html(
          $('<a>')
            .attr('href', FPDS_QUERY_BASE + filing.piid)
            .text('Link')
        )
      )
    }
    if (filing.parent_piid) {
      let row = $('<tr>')
      identifierTBody.append(row)
      row.append($('<th>').attr('scope', 'row').text('Parent'))
      row.append(
        $('<td>').html(
          $('<a>')
            .attr('href', encodeSearch(filing.parent_piid, EXPLORER_ROUTE))
            .text(filing.parent_piid)
        )
      )
      row.append($('<td>').text(filing.parent_mod_number))
      row.append(
        $('<td>').html(
          $('<a>')
            .attr(
              'href',
              `https://www.usaspending.gov/keyword_search/${filing.parent_piid}`
            )
            .text('Link')
        )
      )
      row.append(
        $('<td>').html(
          $('<a>')
            .attr('href', FPDS_QUERY_BASE + filing.parent_piid)
            .text('Link')
        )
      )
    }
    accordion.appendFeature('Identifiers', identifierTable)
  }

  function appendVendor(parentHTML, parentID) {
    const buttonHTML = `Vendor: ${filing.vendor.toUpperCase()}`
    const expanded = false
    const label = 'vendor'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    let subtext = $('<span>')
      .css('font-style', 'italic')
      .css('font-size', '100%')
    if (filing.annotation.vendor.text != filing.vendor) {
      subtext.append(`Vendor as written: "${filing.vendor.toUpperCase()}", `)
    }
    subtext.append(`UEI: ${filing.uei.toUpperCase()}`)

    let vendorValue = $('<p>')
      .addClass('card-text modal-card-text')
      .append(filing.annotation.vendor.logoHTML)
      .append(
        $('<a>')
          .attr(
            'href',
            encodeEntity(filing.annotation.vendor.text, EXPLORER_ROUTE)
          )
          .text(filing.annotation.vendor.stylizedText)
      )
      .append('<br/><br/>')
      .append(subtext)

    collapseBody.append(vendorValue)

    accordion.appendFeature(
      'Contractor Name',
      filing.contractor ? filing.contractor.toUpperCase() : undefined
    )
  }

  function appendProductOrServiceInfo(parentHTML, parentID) {
    if (!(filing.product_or_service_code || filing.principal_naics_code)) {
      return
    }

    // TODO(Jack Poulson): After incorporating a lookup table, extend with
    // trimmed expansion of codes.
    const buttonHTML = 'Product/Service'
    const expanded = false
    const label = 'productOrService'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature(
      'Product or Service Code',
      filing.product_or_service_code +
        ': ' +
        filing.annotation.product_or_service,
      true,
      filing.product_or_service_code
    )

    accordion.appendFeature(
      'Principal NAICS Code',
      filing.principal_naics_code + ': ' + filing.annotation.naics,
      true,
      filing.principal_naics_code
    )
  }

  function appendOffice(parentHTML, parentID, contracting = true) {
    const department = contracting
      ? filing.contracting_department
      : filing.funding_department
    if (!department) {
      return
    }

    const buttonPrefix = contracting ? 'Contracting' : 'Funding'
    const labelPrefix = contracting ? 'contracting' : 'funding'
    const annotation = contracting
      ? filing.annotation.contracting
      : filing.annotation.funding

    const buttonHTML = `${buttonPrefix} Office: ${annotation.office.toUpperCase()}`

    const expanded = false
    const label = `${labelPrefix}Office`
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    let table = $('<table>')
      .addClass('table table-striped table-fit modal-table')
      .append($('<thead>'))
    collapseBody.append(table)
    let tbody = $('<tbody>')
    table.append(tbody)

    let agencyValue = $('<td>')
      .append(annotation.agencyLogoHTML)
      .append(
        $('<a>')
          .attr('href', annotation.agencySearchURL)
          .text(annotation.agencyNorm)
      )
    if (annotation.department || annotation.agency) {
      let subtext = $('<span>')
        .css('font-style', 'italic')
        .css('font-size', '90%')
        .append(
          'Department as written: "' + annotation.department.toUpperCase() + '"'
        )
      if (annotation.agency) {
        subtext.append(
          '<br/>Agency as written: "' + annotation.agency.toUpperCase() + '"'
        )
      }
      agencyValue.append('<br/><br/>').append(subtext)
    }
    tbody.append(
      $('<tr>')
        .append($('<th>').attr('scope', 'row').text('Agency:'))
        .append(agencyValue)
    )

    tbody.append(
      $('<tr>')
        .append($('<th>').attr('scope', 'row').text('Office:'))
        .append(
          $('<td>').append(
            $('<a>')
              .attr('href', annotation.officeSearchURL)
              .text(annotation.office.toUpperCase())
          )
        )
    )
  }

  function appendDescription(parentHTML, parentID) {
    const maxLabelSize = 50

    let fullLabel = filing.description.toUpperCase()
    if (filing.annotation.placeOfPerformance) {
      fullLabel += `, ${filing.annotation.placeOfPerformance}`
    }
    const trimmedDescription = trimLabel(fullLabel, maxLabelSize)

    const buttonHTML = `Description: ${trimmedDescription}`
    const expanded = false
    const label = 'description'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature(
      'Contract Requirement Description',
      filing.description.toUpperCase()
    )

    accordion.appendFeature(
      'Place of Performance',
      filing.annotation.placeOfPerformance
    )
  }

  function appendNationalInterestActionCode(parentHTML) {
    const NIAC_URL =
      'https://www.fai.gov/sites/default/files/Memorandum_for_Chief_Acquisition_Officer_Council_NIA_Code_V4.0.pdf'

    let niacLines = []
    if (filing.niac_text && filing.niac_text != 'none') {
      niacLines.push(filing.niac_text)
    }
    if (filing.niac_description && filing.niac_description != 'none') {
      niacLines.push(filing.niac_description)
    }
    const niacLabel = niacLines.join(': ').toUpperCase()
    if (!niacLabel) {
      return
    }

    const niacTitle = $('<a>')
      .attr('href', NIAC_URL)
      .text('National Interest Action Code')

    appendModalSimpleFeature(parentHTML, niacTitle, niacLabel)
  }

  let content = holder.find('.modal-content')
  content.empty()

  let title = filing.title
  if (filing.piid) {
    if (
      filing.contract_action_type != 'other transaction agreement' &&
      filing.contract_action_type != 'other transaction idv'
    ) {
      const usaSpendingGuess = USASPENDING_QUERY_BASE + filing.piid
      title +=
        `<a href="${usaSpendingGuess}">` +
        '<img src="/logos/usaspending_gov_icon.png" class="img-thumbnail" ' +
        'style="width: 8em; margin: 0 0.25em 0 0.5em;" /></a>'
    }
    const fpdsGuess = FPDS_QUERY_BASE + filing.piid
    title += `<a class="img-thumbnail" href="${fpdsGuess}">fpds.gov</a>`
  }

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'fpdsAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendActionDetails(accordionDiv, accordionID)
  appendValues(accordionDiv, accordionID)
  appendDates(accordionDiv, accordionID)
  appendVendor(accordionDiv, accordionID)
  appendOffice(accordionDiv, accordionID, (contracting = true))
  appendOffice(accordionDiv, accordionID, (contracting = false))
  appendProductOrServiceInfo(accordionDiv, accordionID)
  appendDescription(accordionDiv, accordionID)
  appendModalSimpleFeature(
    body,
    'Major Program Code',
    filing.major_program_code
  )
  appendNationalInterestActionCode(body)
}
