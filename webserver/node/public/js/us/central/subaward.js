function USCentralProcurementSubaward(holder, filing, titleID) {
  const usaSpendingURL = `https://www.usaspending.gov/award/${filing.prime_key}`

  let placeOfPerformanceLines = []
  if (filing.sub_pop_address_line_1) {
    placeOfPerformanceLines.push(filing.sub_pop_address_line_1)
  }
  if (
    filing.sub_pop_city ||
    filing.sub_pop_state ||
    filing.sub_pop_zipcode ||
    filing.sub_pop_country
  ) {
    let cityLine = ''
    if (filing.sub_pop_city) {
      cityLine = filing.sub_pop_city
    }
    if (filing.sub_pop_state) {
      if (cityLine) {
        cityLine += ', '
      }
      cityLine += filing.sub_pop_state
    }
    if (filing.sub_pop_zipcode) {
      if (cityLine) {
        cityLine += ', '
      }
      cityLine += filing.sub_pop_zipcode
    }
    if (filing.sub_pop_country) {
      if (cityLine) {
        cityLine += ', '
      }
      cityLine += filing.sub_pop_country
    }
    placeOfPerformanceLines.push(cityLine)
  }
  const placeOfPerformance = placeOfPerformanceLines.join('<br/>')
  const placeOfPerformanceSingleLine = placeOfPerformanceLines.join(', ')

  function appendDates(parentHTML, parentID) {
    const buttonHTML = 'Contract Dates'
    const expanded = false
    const label = 'dates'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendDate('Subaward Action Date', filing.sub_action_date)
    accordion.appendDate(
      'Prime Award Base Action Date',
      filing.prime_base_action_date
    )
    accordion.appendDate(
      'Subaward Last Modified Date',
      filing.sub_fsrs_last_modified
    )
    accordion.appendFeature('Subaward Report Year', filing.sub_fsrs_year)
    accordion.appendFeature('Subaward Report Month', filing.sub_fsrs_month)
  }

  function appendValues(parentHTML, parentID) {
    const buttonHTML = 'Subaward Amount (USD)'
    const expanded = false
    const label = 'values'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature(
      'Subaward Amount',
      numberWithCommas(filing.sub_amount)
    )
  }

  function appendActionDetails(parentHTML, parentID) {
    if (!(filing.prime_piid || filing.sub_number)) {
      return
    }

    const buttonHTML = 'Contract Action Details'
    const expanded = false
    const label = 'action'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Subaward Number', filing.sub_number)
    accordion.appendFeature(
      'Prime Award PIID',
      $('<a>')
        .attr('href', encodeSearch(filing.prime_piid, EXPLORER_ROUTE))
        .text(filing.prime_piid),
      true,
      filing.prime_piid
    )
    accordion.appendFeature(
      'Prime Award Parent PIID',
      $('<a>')
        .attr('href', encodeSearch(filing.prime_parent_piid, EXPLORER_ROUTE))
        .text(filing.prime_parent_piid),
      true,
      filing.prime_parent_piid
    )

    if (filing.prime_key) {
      accordion.appendFeature(
        'USASpending URL',
        $('<a>').attr('href', usaSpendingURL).text(usaSpendingURL)
      )
    }
  }

  function appendVendors(parentHTML, parentID) {
    const buttonHTML =
      `Subawardee: ${filing.sub_name.toUpperCase()}, ` +
      `Prime Awardee: ${filing.prime_name.toUpperCase()}`
    const expanded = false
    const label = 'vendors'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    {
      let subtext = $('<span>')
        .css('font-style', 'italic')
        .css('font-size', '100%')
      if (filing.annotation.sub.text != filing.sub_name) {
        subtext.append(
          `Subawardee as written: "${filing.sub_name.toUpperCase()}", `
        )
      }
      subtext.append(`UEI: ${filing.sub_uei.toUpperCase()}`)
      if (filing.sub_dba) {
        subtext.append('<br/>').append(`DBA: ${filing.sub_dba.toUpperCase()}`)
      }
      if (filing.sub_parent_name) {
        subtext
          .append('<br/>')
          .append(`Parent: ${filing.sub_parent_name.toUpperCase()}`)
      }
      if (filing.sub_parent_uei) {
        subtext
          .append('<br/>')
          .append(`Parent UEI: ${filing.sub_parent_uei.toUpperCase()}`)
      }

      let vendorValue = $('<p>')
        .addClass('card-text modal-card-text')
        .css('font-size', '90%')
        .append(filing.annotation.sub.logoHTML)
        .append(
          $('<a>')
            .attr(
              'href',
              encodeEntity(filing.annotation.sub.text, EXPLORER_ROUTE)
            )
            .text(filing.annotation.sub.stylizedText)
        )
        .append('<br/>')
        .append(subtext)
      accordion.appendFeature('Subawardee', vendorValue)
    }

    {
      let subtext = $('<span>')
        .css('font-style', 'italic')
        .css('font-size', '100%')
      if (filing.annotation.prime.text != filing.prime_name) {
        subtext.append(
          `Prime Awardee as written: "${filing.prime_name.toUpperCase()}", `
        )
      }
      subtext.append(`UEI: ${filing.prime_uei.toUpperCase()}`)
      if (filing.prime_dba) {
        subtext.append('<br/>').append(`DBA: ${filing.prime_dba.toUpperCase()}`)
      }
      let vendorValue = $('<p>')
        .addClass('card-text modal-card-text')
        .css('font-size', '100%')
        .append(filing.annotation.prime.logoHTML)
        .append(
          $('<a>')
            .attr(
              'href',
              encodeEntity(filing.annotation.prime.text, EXPLORER_ROUTE)
            )
            .text(filing.annotation.prime.stylizedText)
        )
        .append('<br/><br/>')
        .append(subtext)
      accordion.appendFeature('Prime Awardee', vendorValue)
    }
  }

  function appendContractingOffice(parentHTML, parentID) {
    if (!filing.annotation.contracting.origAgency) {
      return
    }

    const buttonHTML = `Contracting Office: ${filing.annotation.contracting.office.toUpperCase()}`
    const expanded = false
    const label = 'contractingOffice'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    let table = $('<table>')
      .addClass('table table-striped table-fit modal-table')
      .append($('<thead>'))
    collapseBody.append(table)
    let tbody = $('<tbody>')
    table.append(tbody)

    let agencyValue = $('<td>')
      .append(filing.annotation.contracting.agencyLogoHTML)
      .append(
        $('<a>')
          .attr('href', filing.annotation.contracting.agencySearchURL)
          .text(filing.annotation.contracting.agency)
      )
    if (
      filing.annotation.contracting.origDept ||
      filing.annotation.contracting.origAgency
    ) {
      let subtext = $('<span>')
        .css('font-style', 'italic')
        .css('font-size', '100%')
        .append(
          'Department as written: "' +
            filing.annotation.contracting.origDept.toUpperCase() +
            '"'
        )
      if (filing.annotation.contracting.origAgency) {
        subtext.append(
          '<br/>Agency as written: "' +
            filing.annotation.contracting.origAgency.toUpperCase() +
            '"'
        )
      }
      agencyValue.append('<br/><br/>').append(subtext)
    }
    tbody.append(
      $('<tr>')
        .append($('<th>').attr('scope', 'row').text('Agency:'))
        .append(agencyValue)
    )

    tbody.append(
      $('<tr>')
        .append($('<th>').attr('scope', 'row').text('Office:'))
        .append(
          $('<td>').append(
            $('<a>')
              .attr('href', filing.annotation.contracting.officeSearchURL)
              .text(filing.annotation.contracting.office.toUpperCase())
          )
        )
    )
  }

  function appendFundingOffice(parentHTML, parentID) {
    if (!filing.annotation.funding.origAgency) {
      return
    }

    const buttonHTML = `Funding Office: ${filing.annotation.funding.office.toUpperCase()}`
    const expanded = false
    const label = 'fundingOffice'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    let table = $('<table>')
      .addClass('table table-striped table-fit modal-table')
      .append($('<thead>'))
    collapseBody.append(table)
    let tbody = $('<tbody>')
    table.append(tbody)

    let agencyValue = $('<td>')
      .append(filing.annotation.funding.agencyLogoHTML)
      .append(
        $('<a>')
          .attr('href', filing.annotation.funding.agencySearchURL)
          .text(filing.annotation.funding.agency)
      )
    if (
      filing.annotation.funding.origDept ||
      filing.annotation.funding.origAgency
    ) {
      let subtext = $('<span>')
        .css('font-style', 'italic')
        .css('font-size', '90%')
        .append(
          'Department as written: "' +
            filing.annotation.funding.origDept.toUpperCase() +
            '"'
        )
      if (filing.annotation.funding.origAgency) {
        subtext.append(
          '<br/>Agency as written: "' +
            filing.annotation.funding.origAgency.toUpperCase() +
            '"'
        )
      }
      agencyValue.append('<br/><br/>').append(subtext)
    }
    tbody.append(
      $('<tr>')
        .append($('<th>').attr('scope', 'row').text('Agency:'))
        .append(agencyValue)
    )

    tbody.append(
      $('<tr>')
        .append($('<th>').attr('scope', 'row').text('Office:'))
        .append(
          $('<td>').append(
            $('<a>')
              .attr('href', filing.annotation.funding.officeSearchURL)
              .text(filing.annotation.funding.office.toUpperCase())
          )
        )
    )
  }

  function appendDescription(parentHTML, parentID) {
    const maxLabelSize = 50

    let fullLabel = filing.sub_description
    if (placeOfPerformance) {
      fullLabel += ', ' + placeOfPerformanceSingleLine
    }
    const trimmedDescription = trimLabel(fullLabel, maxLabelSize)

    const buttonHTML = `Description: ${trimmedDescription}`
    const expanded = false
    const label = 'description'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Subaward Description', filing.sub_description)
    accordion.appendFeature('Place of Performance', placeOfPerformance)
  }

  let content = holder.find('.modal-content')
  content.empty()

  const maxTitleSize = 250
  let title = trimLabel(filing.sub_description, maxTitleSize)
  if (filing.prime_key) {
    title += `
      <a href="${usaSpendingURL}">
      <img src="/logos/usaspending_gov_icon.png" class="img-thumbnail"
      style="width: 8em; margin: 0 0.25em 0 0.5em;" /></a>`
  }

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'fpdsAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendActionDetails(accordionDiv, accordionID)
  appendValues(accordionDiv, accordionID)
  appendDates(accordionDiv, accordionID)
  appendVendors(accordionDiv, accordionID)
  appendContractingOffice(accordionDiv, accordionID)
  appendFundingOffice(accordionDiv, accordionID)
  appendDescription(accordionDiv, accordionID)
}
