function USSecuritiesTab(tabsState, data) {
  const label = 'us-securities'
  const tabLogo = '/logos/government_of_the_united_states.svg'
  const tabTitle = 'US SEC Filings'
  FeedTab(tabsState.id(), tabsState.navID(), label, tabLogo, tabTitle)

  const tabsID = `${label}-tabs`
  const dialogID = `${label}-dialog`
  createDialogDiv(dialogID)

  function renderRecordsTab(event, ui) {
    const containerID = `${label}-tab-records-inner`
    const tableID = `${label}-table`
    const dialogWidth = 'auto'
    USSecuritiesTable(data, containerID, tableID, dialogID, dialogWidth)
  }

  function renderTab(event, ui) {
    renderRecordsTab(event, ui)
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
