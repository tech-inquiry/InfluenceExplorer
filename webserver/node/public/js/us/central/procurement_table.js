function USFederalAwardsTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  function itemDate(item) {
    return item.type == 'prime'
      ? item.filing.signed_date
      : item.filing.sub_action_date
  }

  function itemPrimeAwardee(item) {
    const awardee =
      item.type == 'prime' ? item.filing.vendor : item.filing.prime_name
    return awardee ? awardee.toUpperCase() : ''
  }

  function itemSubawardee(item) {
    const subawardee = item.type == 'prime' ? '' : item.filing.sub_name
    return subawardee ? subawardee.toUpperCase() : ''
  }

  function itemPrimeAwardID(item) {
    if (item.type == 'prime') {
      if (item.filing.mod_number) {
        return `${item.filing.piid}<br/>Mod. ${item.filing.mod_number}`
      } else {
        return item.filing.piid
      }
    } else {
      return item.filing.prime_piid
    }
  }

  function itemSubawardNumber(item) {
    return item.type == 'prime' ? '' : item.filing.sub_number
  }

  function itemActionObligation(item) {
    return item.type == 'prime'
      ? item.filing.obligated_amount
      : item.filing.sub_amount
  }

  function itemTotalPotential(item) {
    return item.type == 'prime'
      ? item.filing.total_base_and_all_options_value
      : ''
  }

  function itemDescription(item) {
    return item.type == 'prime'
      ? item.filing.description
        ? item.filing.description.toUpperCase()
        : ''
      : item.filing.sub_description
  }

  function itemContractingInfo(item) {
    if (item.type == 'prime') {
      return {
        department: item.filing.contracting_department.toUpperCase(),
        agency: item.filing.contracting_agency.toUpperCase(),
      }
    } else {
      return {
        department: item.filing.prime_awarding_agency
          ? item.filing.prime_awarding_agency
          : '',
        agency: item.filing.prime_awarding_sub_agency
          ? item.filing.prime_awarding_sub_agency
          : '',
      }
    }
  }

  function itemFundingInfo(item) {
    if (item.type == 'prime') {
      return {
        department: item.filing.funding_department.toUpperCase(),
        agency: item.filing.funding_agency.toUpperCase(),
      }
    } else {
      return {
        department: item.filing.prime_funding_agency
          ? item.filing.prime_funding_agency
          : '',
        agency: item.filing.prime_funding_sub_agency
          ? item.filing.prime_funding_sub_agency
          : '',
      }
    }
  }

  $.when(
    $.ajax({
      url: appendURLPath(EXPLORER_ROUTE, 'api/us/central/procurement'),
      data: data,
    }),
    $.ajax({
      url: appendURLPath(EXPLORER_ROUTE, 'api/us/central/procurement/subaward'),
      data: data,
    })
  ).then(function (primeResult, subResult) {
    let items = []
    for (const filing of primeResult[0].filings) {
      items.push({ type: 'prime', filing: filing })
    }
    for (const filing of subResult[0].filings) {
      items.push({ type: 'sub', filing: filing })
    }

    items.sort(function (a, b) {
      const aDate = itemDate(a)
      const bDate = itemDate(b)
      return aDate == bDate ? 0 : aDate > bDate ? -1 : 1
    })

    fillWithResponsiveTable(containerID, tableID)

    // Test if there are any subawards.
    let haveSubawards = false
    for (const [itemIndex, item] of items.entries()) {
      const subawardee = itemSubawardee(item)
      const subawardNumber = itemSubawardNumber(item)
      if (subawardee || subawardNumber) {
        haveSubawards = true
      }
    }

    let tableData = []
    const maxDescriptionLength = 256

    for (const [itemIndex, item] of items.entries()) {
      const signedDate = dateWithoutTimeLex(itemDate(item))
      const primeAwardee = itemPrimeAwardee(item)
      const subawardee = itemSubawardee(item)
      const primeAwardID = itemPrimeAwardID(item)
      const subawardNumber = itemSubawardNumber(item)
      const actionObligation = itemActionObligation(item)
      const totalPotential = itemTotalPotential(item)
      const description = trimLabel(itemDescription(item), maxDescriptionLength)

      const fundingInfo = itemFundingInfo(item)
      const contractingInfo = itemContractingInfo(item)
      let agenciesSet = new Set()
      let agenciesList = []
      if (fundingInfo.agency || fundingInfo.department) {
        const candidate = `${fundingInfo.department}: ${fundingInfo.agency}`
        if (!agenciesSet.has(candidate.toUpperCase())) {
          agenciesSet.add(candidate.toUpperCase())
          agenciesList.push(candidate)
        }
      }
      if (contractingInfo.agency || contractingInfo.department) {
        const candidate = `${contractingInfo.department}: ${contractingInfo.agency}`
        if (!agenciesSet.has(candidate.toUpperCase())) {
          agenciesSet.add(candidate.toUpperCase())
          agenciesList.push(candidate)
        }
      }
      const agencies = agenciesList.join('; ')

      const entireFiling = JSON.stringify(item.filing)

      if (haveSubawards) {
        tableData.push([
          itemIndex,
          signedDate,
          primeAwardee,
          subawardee,
          agencies,
          primeAwardID,
          subawardNumber,
          actionObligation,
          totalPotential,
          description,
          entireFiling,
        ])
      } else {
        tableData.push([
          itemIndex,
          signedDate,
          primeAwardee,
          agencies,
          primeAwardID,
          actionObligation,
          totalPotential,
          description,
          entireFiling,
        ])
      }
    }

    let columDefs = []
    if (haveSubawards) {
      columnDefs = [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Signed Date' },
        { targets: 2, title: 'Prime Awardee' },
        { targets: 3, title: 'Subawardee' },
        { targets: 4, title: 'Agencies' },
        { targets: 5, title: 'Prime Identifier' },
        { targets: 6, title: 'Subaward Number' },
        {
          targets: 7,
          title: 'Action Obligation',
          render: $.fn.dataTable.render.number(',', '.', 2, '$'),
        },
        {
          targets: 8,
          title: 'Overall Ceiling',
          render: $.fn.dataTable.render.number(',', '.', 2, '$'),
        },
        { targets: 9, title: 'Description' },
        { targets: 10, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ]
    } else {
      columnDefs = [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Signed Date' },
        { targets: 2, title: 'Prime Awardee' },
        { targets: 3, title: 'Agencies' },
        { targets: 4, title: 'Prime Identifier' },
        {
          targets: 5,
          title: 'Action Obligation',
          render: $.fn.dataTable.render.number(',', '.', 2, '$'),
        },
        {
          targets: 6,
          title: 'Overall Ceiling',
          render: $.fn.dataTable.render.number(',', '.', 2, '$'),
        },
        { targets: 7, title: 'Description' },
        { targets: 8, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ]
    }

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: columnDefs,
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const itemIndex = table.row(this).index()
      const item = items[itemIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`

      if (item.type == 'prime') {
        USFederalAward(holder, item.filing, titleID)
      } else {
        USCentralProcurementSubaward(holder, item.filing, titleID)
      }
      holder.modal('show')
    })
  })
}
