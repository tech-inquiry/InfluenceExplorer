function USProcurementTab(
  tabsState,
  data,
  chartTitlePrefix = '',
  chartSubject = 'N/A',
  nameList = undefined,
  nameUEIList = undefined
) {
  const label = 'awards'
  const tabLogo = '/logos/government_of_the_united_states.svg'
  const tabTitle = 'US Federal Contracts'
  const queryURL =
    appendURLPath(EXPLORER_ROUTE, 'api/us/central/procurement?') +
    (data['vendor']
      ? `vendor=${encodeURIComponentIncludingSlashes(data.vendor)}`
      : `text=${encodeURIComponentIncludingSlashes(data.text)}`)
  const recordsNote = $('<div>')
    .append(
      $('<p>')
        .text('All links and images are annotations added to the original ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', 'https://www.fpds.gov/wiki/index.php/ATOM_Feed_FAQ')
            .text('FPDS ATOM feed')
        )
        .append(' (for case of prime awards) or ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr(
              'href',
              'https://www.usaspending.gov/download_center/custom_award_data'
            )
            .text('USASpending.gov export')
        )
        .append(
          ' (for subawards) by Tech Inquiry. ' +
            'Any normalized names are clearly labeled with the ' +
            "original ('as written') text."
        )
    )
    .append(
      $('<p>')
        .append('You can alternatively download the ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', queryURL)
            .text('JSON data')
        )
        .append(' for this tab.')
    )

  const haveSummary = true
  let tabs = FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote,
    undefined,
    undefined,
    haveSummary
  )

  if (
    (nameList !== undefined && nameList.length > 1) ||
    (nameUEIList !== undefined && nameUEIList.length > 1)
  ) {
    $(`#filing-tab-${label}-nav`).append(
      $('<li>')
        .addClass('nav-item')
        .append(
          $('<a>')
            .addClass('nav-link')
            .attr('id', `${label}-tab-alternate-link`)
            .attr('href', `#${label}-tab-alternate`)
            .text('Included Names')
        )
    )

    let alternateTab = $('<div>')
      .addClass('content')
      .attr('id', `${label}-tab-alternate`)
      .css('display', 'none')
    tabs.append(alternateTab)

    let card = $('<div>').addClass('card').css('font-size', '175%')
    alternateTab.append(card)
    let cardBody = $('<div>').addClass('card-body')
    card.append(cardBody)
    cardBody.append(
      $('<h2>').addClass('card-title').text('Included Name/UEI pairings')
    )
    let table = $('<table>').addClass('table')
    cardBody.append(table)
    let thead = $('<thead>')
    table.append(thead)
    thead.append(
      $('<tr>')
        .append($('<th>').attr('scope', 'col').text('Name'))
        .append($('<th>').attr('scope', 'col').text('UEI'))
    )
    let tbody = $('<tbody>')
    table.append(tbody)
    nameList.forEach(function (alternate, alternateIndex) {
      if (alternateIndex == 0) {
        // This is the original name and not an alternate.
        return
      }
      tbody.append(
        $('<tr>').append($('<td>').text(alternate)).append($('<td>'))
      )
    })
    nameUEIList.forEach(function (alternate, alternateIndex) {
      if (alternateIndex == 0) {
        // This is the original name and not an alternate.
        return
      }
      tbody.append(
        $('<tr>')
          .append($('<td>').text(alternate[0]))
          .append($('<td>').text(alternate[1]))
      )
    })
  }

  const tabsID = `${label}-tabs`

  function renderSummaryTab(event, ui) {
    $.ajax({
      url: appendURLPath(
        EXPLORER_ROUTE,
        'api/us/central/procurement/aggregates'
      ),
      data: data,
    }).done(function (result) {
      const summaryLabel = `${label}-tab-summary`
      const relativeParentID = tabsID
      const holder = $(`#${summaryLabel}`)
      const maxLabels = 25
      const summarySelectID = `${summaryLabel}-select`
      const amountSelectID = `${summaryLabel}-amount-select`
      const chartID = `${summaryLabel}-chart`
      const chartContainerID = `${summaryLabel}-chart-container`
      USFederalAwardsChart(
        holder,
        result,
        chartTitlePrefix,
        chartSubject,
        relativeParentID,
        maxLabels,
        summarySelectID,
        amountSelectID,
        chartID,
        chartContainerID
      )
    })
  }

  function renderRecordsTab(event, ui) {
    const containerID = `${label}-tab-records-inner`
    USFederalAwardsTable(data, containerID)
  }

  function renderTab(event, ui) {
    const recordsTab = 0
    const summaryTab = 1
    const activeTab = $(`#${tabsID}`).tabs('option', 'active')
    if (activeTab == summaryTab) {
      renderSummaryTab(event, ui)
    } else if (activeTab == recordsTab) {
      renderRecordsTab(event, ui)
    }
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
