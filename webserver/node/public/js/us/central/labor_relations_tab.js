function USLaborRelationsTab(tabsState, data, alternateNames = undefined) {
  const label = 'nlrb'
  const tabLogo = '/logos/government_of_the_united_states.svg'
  const tabTitle = 'US NLRB Filings'
  const recordsNote = $('<div>')
    .append(
      $('<p>')
        .text('All links and images are annotations added to the original ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr(
              'href',
              'https://www.nlrb.gov/reports/graphs-data/recent-filings'
            )
            .text('NLRB data')
        )
        .append(
          ' by Tech Inquiry. Any normalized names are clearly labeled with the ' +
            "original ('as written') text."
        )
    )
    .append(
      $('<p>')
        .text(
          'For focused analysis of US union elections, we recommend the US '
        )
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', 'https://unionelections.org/')
            .text('Union Election Data')
        )
        .append(' tracker run by ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', 'https://www.kevinreuning.com/')
            .text('Prof. Kevin Reuning')
        )
        .append('.')
    )
  FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote,
    alternateNames
  )

  const tabsID = `${label}-tabs`

  function renderRecordsTab(event, ui) {
    const containerID = `${label}-tab-records-inner`
    USLaborRelationsTable(data, containerID)
  }

  function renderTab(event, ui) {
    const recordsTab = 0
    const activeTab = $(`#${tabsID}`).tabs('option', 'active')
    if (activeTab == recordsTab) {
      renderRecordsTab(event, ui)
    }
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
