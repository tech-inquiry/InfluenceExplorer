function USLobbyingContributionsChart(
  holder,
  aggregates,
  chartTitlePrefix,
  chartSubject,
  relativeParentID,
  selectID = 'contributionsSummaryType',
  chartID = 'lobbyingContributionsChart',
  chartContainerID = 'lobbyingContributionsChartContainer'
) {
  holder.empty()

  const maxLabelLength = 55
  const formatLabel = function (label) {
    return trimLabel(label, maxLabelLength)
  }

  let contributionsByYear = aggregates.byYear
  let contributionsByRegistrant = aggregates.byRegistrant
  let contributionsByContributor = aggregates.byContributor
  let contributionsByHonoree = aggregates.byHonoree
  let contributionsByPayee = aggregates.byPayee
  contributionsByYear = contributionsByYear.sort(function (x, y) {
    return d3.ascending(x.label, y.label)
  })
  contributionsByRegistrant.forEach(function (item) {
    item.label = formatLabel(item.label)
  })
  contributionsByContributor.forEach(function (item) {
    item.label = formatLabel(item.label)
  })
  contributionsByHonoree.forEach(function (item) {
    item.label = formatLabel(item.label)
  })
  contributionsByPayee.forEach(function (item) {
    item.label = formatLabel(item.label)
  })

  let summaryType = $('<select>').attr('id', selectID).css('font-size', '140%')
  if (
    contributionsByYear.length >
    Math.max(
      contributionsByRegistrant.length,
      contributionsByContributor.length,
      contributionsByHonoree.length,
      contributionsByPayee.length
    )
  ) {
    summaryType
      .append(
        $('<option>', { value: 'year', selected: 'selected', text: 'Year' })
      )
      .append($('<option>', { value: 'registrant', text: 'Registrant' }))
      .append($('<option>', { value: 'contributor', text: 'Contributor' }))
      .append($('<option>', { value: 'honoree', text: 'Honoree' }))
      .append($('<option>', { value: 'payee', text: 'Payee' }))
  } else if (
    contributionsByRegistrant.length >
    Math.max(
      contributionsByYear.length,
      contributionsByContributor.length,
      contributionsByHonoree.length,
      contributionsByPayee.length
    )
  ) {
    summaryType
      .append($('<option>', { value: 'year', text: 'Year' }))
      .append(
        $('<option>', {
          value: 'registrant',
          selected: 'selected',
          text: 'Registrant',
        })
      )
      .append($('<option>', { value: 'contributor', text: 'Contributor' }))
      .append($('<option>', { value: 'honoree', text: 'Honoree' }))
      .append($('<option>', { value: 'payee', text: 'Payee' }))
  } else if (
    contributionsByContributor.length >
    Math.max(
      contributionsByYear.length,
      contributionsByRegistrant.length,
      contributionsByHonoree.length,
      contributionsByPayee.length
    )
  ) {
    summaryType
      .append($('<option>', { value: 'year', text: 'Year' }))
      .append($('<option>', { value: 'registrant', text: 'Registrant' }))
      .append(
        $('<option>', {
          value: 'contributor',
          selected: 'selected',
          text: 'Contributor',
        })
      )
      .append($('<option>', { value: 'honoree', text: 'Honoree' }))
      .append($('<option>', { value: 'payee', text: 'Payee' }))
  } else if (
    contributionsByHonoree.length >
    Math.max(
      contributionsByYear.length,
      contributionsByRegistrant.length,
      contributionsByContributor.length,
      contributionsByPayee.length
    )
  ) {
    summaryType
      .append($('<option>', { value: 'year', text: 'Year' }))
      .append($('<option>', { value: 'registrant', text: 'Registrant' }))
      .append($('<option>', { value: 'contributor', text: 'Contributor' }))
      .append(
        $('<option>', {
          value: 'honoree',
          selected: 'Selected',
          text: 'Honoree',
        })
      )
      .append($('<option>', { value: 'payee', text: 'Payee' }))
  } else {
    summaryType
      .append($('<option>', { value: 'year', text: 'Year' }))
      .append($('<option>', { value: 'registrant', text: 'Registrant' }))
      .append($('<option>', { value: 'contributor', text: 'Contributor' }))
      .append($('<option>', { value: 'honoree', text: 'Honoree' }))
      .append(
        $('<option>', { value: 'payee', selected: 'selected', text: 'Payee' })
      )
  }
  holder.append(
    $('<div>')
      .addClass('d3-chart-select')
      .append(
        $('<span>')
          .addClass('bold')
          .css('font-size', '140%')
          .css('margin-right', '1em')
          .text('Summarize')
      )
      .append(summaryType)
  )

  holder.append(
    $('<div>')
      .addClass('d3-chart-container')
      .attr('id', chartContainerID)
      .append($('<div>').attr('id', chartID).css('display', 'inline-block'))
  )

  holder.append(
    $('<div>')
      .addClass('filing_description')
      .append('Source: ')
      .append(
        $('<a>')
          .attr(
            'href',
            'https://www.senate.gov/legislative/Public_Disclosure/contributions_download.htm'
          )
          .text('Senate OPR LD-203 downloads')
      )
      .append(' (updated daily)<br/>Cf. ')
      .append(
        $('<a>')
          .attr('href', 'https://www.opensecrets.org/')
          .text('OpenSecrets')
      )
  )

  const amountTypes = {
    year: {
      data: contributionsByYear,
      title:
        chartTitlePrefix + 'Contributions by Year Involving ' + chartSubject,
      sliceFromEnd: true,
    },
    registrant: {
      data: contributionsByRegistrant,
      title:
        chartTitlePrefix +
        'Contributions by Registrant Involving ' +
        chartSubject,
      sliceFromEnd: false,
    },
    contributor: {
      data: contributionsByContributor,
      title:
        chartTitlePrefix +
        'Contributions by Contributor Involving ' +
        chartSubject,
      sliceFromEnd: false,
    },
    honoree: {
      data: contributionsByHonoree,
      title:
        chartTitlePrefix + 'Contributions by Honoree Involving ' + chartSubject,
      sliceFromEnd: false,
    },
    payee: {
      data: contributionsByPayee,
      title:
        chartTitlePrefix + 'Contributions by Payee Involving ' + chartSubject,
      sliceFromEnd: false,
    },
  }

  const chartConfig = {
    // The ID of the select for the award type.
    selectID: '#' + selectID,

    // The ID of the chart to draw the SVG in.
    chartID: '#' + chartID,

    // The ID of the relative parent div we will compute mouse positions
    // relative to.
    relativeParentID: relativeParentID,

    // The font size of the title.
    titleFontSize: '28px',

    // The font size of the title on small screens.
    titleFontSizeSmallScreen: '22px',

    // The font weight of the title.
    titleFontWeight: '700',

    // The title of the y axis.
    yAxisTitle: 'Amount (USD)',

    // The font size of the y-axis title.
    yAxisTitleFontSize: '25px',

    // The font size of the y-axis title on small screens.
    yAxisTitleFontSizeSmallScreen: '20px',

    // The font weight of the y-axis title.
    yAxisTitleFontWeight: '700',

    // The font size of the x-axis labels.
    xLabelFontSize: '12px',

    // The font weight of the x-axis labels.
    xLabelFontWeight: '700',

    // The font size of the y-axis labels.
    yLabelFontSize: '16px',

    // The font weight of the y-axis labels.
    yLabelFontWeight: '700',

    // How many pixels left of the mouse to place the top-left corner of the
    // tooltip.
    tooltipHorizontalOffset: 50,

    // How many pixels above the mouse to place the top-left corner of the
    // tooltip.
    tooltipVerticalOffset: 45,

    // The maximum width (in pixels) of any bar in the bar chart.
    maxBarWidth: 120,

    // The minimum height (in pixels) of any bar in the bar chart.
    minBarHeight: 4,

    // The height (in pixels) of the inner portion of the chart.
    innerChartHeight: 400,

    // The chart width (in pixels) for large screens.
    chartWidth: 1250,

    // The chart width (in pixels) for small screens.
    smallChartWidth: 800,

    // The largest number of width pixels a screen can have before it's not
    // considered 'small'.
    smallScreenWidth: 1500,

    // The maximum number of agencies to keep in the bar chart.
    maxSmallScreenLabels: 15,

    // The number of pixels in the top margin.
    marginTop: 100,

    // The number of pixels in the right margin.
    marginRight: 60,

    // The minimum number of pixels in the left margin.
    minMarginLeft: 120,

    // The maximum number of pixels in the left margin.
    maxMarginLeft: 500,

    // The number of pixels the left-most text should begin to the right of
    // the chart.
    marginLeftOffset: -20,

    // The minimum number of pixels in the bottom margin of the chart.
    minMarginBottom: 70,

    // The number of margin pixels per character of the labels.
    marginPerChar: 6.5,

    // The interior color of the bars in the chart.
    barColor: '#007FFF',

    // The interior color of the bars in the chart when moused over.
    barColorMouseover: 'blue',

    // The color of the borders of the bars in the chart.
    barBorderColor: '#000000',

    // The number of pixels of the borders of the bars in the chart.
    barBorderWidth: 2,

    xLabelFormat: function (d) {
      return d3.format('.3s')(d).replace('G', 'B')
    },

    tooltipHTML: function (d) {
      return '$' + d3.format('.3s')(d.amount).replace('G', 'B')
    },
  }

  BarChart(amountTypes, chartConfig)
}
