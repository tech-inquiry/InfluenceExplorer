function USLobbyingContributionsTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/us/central/lobbying/contributions'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    let holder = $(`#${containerID}`)
    holder.empty()
    holder.append(
      $('<div>')
        .addClass('table-responsive')
        .append(
          $('<table>')
            .attr('id', tableID)
            .addClass('table table-striped table-bordered')
            .css('font-size', '140%')
        )
    )

    let tableData = []
    const kMaxPayeeListLength = 100
    filings.forEach(function (filing, filingIndex) {
      const received = dateWithoutTimeLex(filing.dt_posted)
      const period = `${filing.filing_year}, ${filing.filing_period}`
      const type = filing.filing_type

      let payee = ''
      let amount = 0
      filing.contribution_items.forEach(function (contribution) {
        amount += Number(contribution.amount)
        if (payee) {
          payee += ', '
        }
        payee += contribution.payee_name
      })
      payee = trimLabel(payee, kMaxPayeeListLength)

      const registrant = filing.registrant ? filing.registrant.name : ''
      const lobbyist = filing.lobbyist
        ? lobbyistNameFromObject(filing.lobbyist)
        : ''
      const pacs = filing.pacs.join(', ')

      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        received,
        period,
        type,
        registrant,
        lobbyist,
        pacs,
        payee,
        amount,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Received', visible: true },
        { targets: 2, title: 'Period', visible: true },
        { targets: 3, title: 'Type', visible: true },
        { targets: 4, title: 'Registrant Name', visible: true },
        { targets: 5, title: 'Lobbyist', visible: true },
        { targets: 6, title: 'PACs', visible: true },
        { targets: 7, title: 'Payee', visible: true },
        {
          targets: 8,
          title: 'Amount',
          visible: true,
          render: $.fn.dataTable.render.number(',', '.', 2, '$'),
        },
        { targets: 9, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      USLobbyingContributions(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
