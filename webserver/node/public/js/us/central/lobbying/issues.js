function USLobbyingIssues(holder, filing, titleID) {
  function jsonURLFromUUID(uuid) {
    return `https://lda.senate.gov/api/v1/filings/${uuid}`
  }

  function documentURLFromUUID(uuid) {
    return `https://lda.senate.gov/filings/public/filing/${uuid}/print/`
  }

  function getDescriptionAnnotation(description) {
    if (!description) {
      description = ''
    }

    function sessionFromYear(year) {
      // Each session of Congress has lasted two years since the 77th session
      // began in January of 1941. Bill numbers are reused between sessions,
      // and so we index on the pairing of the session and bill name (e.g.,
      // H.R. 6720 of the 116th session of Congress).
      return 77 + Math.floor((year - 1941) / 2)
    }

    // We default to assuming the session corresponds to the year of the
    // filing. But if a phrase of the form 'of 2017' appears, we compute the
    // session from the mentioned year. This handles cases such as those of
    //
    //   "H.R. 4477/S. 2135: Fix NICS Act of 2017" [filed in 2020].
    //
    let session = sessionFromYear(Number(filing['filing_year']))
    const yearMatch = description.match(/of (\d{4})/)
    if (yearMatch) {
      session = sessionFromYear(Number(yearMatch[1]))
    }

    const billPrefix = `https://www.congress.gov/bill/${session}th-congress/`

    function regexReplace(
      urlLabel,
      billLabel,
      billList,
      match,
      prefix,
      billNumber
    ) {
      const url = `${billPrefix}${urlLabel}/${billNumber}`
      const label = `${billLabel} ${billNumber}`
      billList.push({ url: url, label: label })
      return `<a href=${url}>${label}</a>`
    }

    let senateConcurrentResolutions = []
    const senateConcurrentResRegex = /\b(s\.? ?con\.? ?res\.?) *(\d+)/gi
    function senateConcurrentResRegexReplace(match, prefix, billNumber) {
      const urlLabel = 'senate-concurrent-resolution'
      const billLabel = 'S.Con.Res.'
      return regexReplace(
        urlLabel,
        billLabel,
        senateConcurrentResolutions,
        match,
        prefix,
        billNumber
      )
    }

    let senateJointResolutions = []
    const senateJointResRegex = /\b(s\.? ?j\.? ?res\.?) *(\d+)/gi
    function senateJointResRegexReplace(match, prefix, billNumber) {
      const urlLabel = 'senate-joint-resolution'
      const billLabel = 'S.J.Res.'
      return regexReplace(
        urlLabel,
        billLabel,
        senateJointResolutions,
        match,
        prefix,
        billNumber
      )
    }

    let senateResolutions = []
    const senateResRegex = /\b(s\.? ?res\.?) *(\d+)/gi
    function senateResRegexReplace(match, prefix, billNumber) {
      const urlLabel = 'senate-resolution'
      const billLabel = 'S.Res.'
      return regexReplace(
        urlLabel,
        billLabel,
        senateResolutions,
        match,
        prefix,
        billNumber
      )
    }

    let senateBills = []
    // We would ideally use the following, but the negative look-behind
    // does not function in iOS.
    //const senateRegex = /(?<!u\.)\b(s\.?) *(\d+)/ig;
    const senateRegex = /(u\.)?\b(s\.?) *(\d+)/gi
    function senateRegexReplace(match, preprefix, prefix, billNumber) {
      // We cannot assume functioning negative look-behind regexes in iOS,
      // so we instead check if the positive match (of "u.") was nontrivial.
      if (preprefix) {
        return match
      }
      const urlLabel = 'senate-bill'
      const billLabel = 'S.'
      return regexReplace(
        urlLabel,
        billLabel,
        senateBills,
        match,
        prefix,
        billNumber
      )
    }

    let houseConcurrentResolutions = []
    const houseConcurrentResRegex = /\b(h\.? ?con\.? ?res\.?) *(\d+)/gi
    function houseConcurrentResRegexReplace(match, prefix, billNumber) {
      const urlLabel = 'house-concurrent-resolution'
      const billLabel = 'H.Con.Res.'
      return regexReplace(
        urlLabel,
        billLabel,
        houseConcurrentResolutions,
        match,
        prefix,
        billNumber
      )
    }

    let houseJointResolutions = []
    const houseJointResRegex = /\b(h\.? ?j\.? ?res\.?) *(\d+)/gi
    function houseJointResRegexReplace(match, prefix, billNumber) {
      const urlLabel = 'house-joint-resolution'
      const billLabel = 'H.J.Res.'
      return regexReplace(
        urlLabel,
        billLabel,
        houseJointResolutions,
        match,
        prefix,
        billNumber
      )
    }

    let houseResolutions = []
    const houseResRegex = /\b(h\.? ?res\.?) *(\d+)/gi
    function houseResRegexReplace(match, prefix, billNumber) {
      const urlLabel = 'house-resolution'
      const billLabel = 'H.Res.'
      return regexReplace(
        urlLabel,
        billLabel,
        houseResolutions,
        match,
        prefix,
        billNumber
      )
    }

    let houseBills = []
    const houseRegex = /\b(h\.? ?r\.?) *(\d+)/gi
    function houseRegexReplace(match, prefix, billNumber) {
      const urlLabel = 'house-bill'
      const billLabel = 'H.R.'
      return regexReplace(
        urlLabel,
        billLabel,
        houseBills,
        match,
        prefix,
        billNumber
      )
    }

    const descriptionHTML = description
      .replace(senateConcurrentResRegex, senateConcurrentResRegexReplace)
      .replace(senateJointResRegex, senateJointResRegexReplace)
      .replace(senateResRegex, senateResRegexReplace)
      .replace(senateRegex, senateRegexReplace)
      .replace(houseConcurrentResRegex, houseConcurrentResRegexReplace)
      .replace(houseJointResRegex, houseJointResRegexReplace)
      .replace(houseResRegex, houseResRegexReplace)
      .replace(houseRegex, houseRegexReplace)

    return {
      description: descriptionHTML,
      session: session,
      house: {
        bills: houseBills,
        resolutions: houseResolutions,
        jointResolutions: houseJointResolutions,
        concurrentResolutions: houseConcurrentResolutions,
      },
      senate: {
        bills: senateBills,
        resolutions: senateResolutions,
        jointResolutions: senateJointResolutions,
        concurrentResolutions: senateConcurrentResolutions,
      },
    }
  }

  function appendRegistrant(
    registrant,
    registrantAnnotation,
    contactAnnotation,
    parentHTML,
    parentID
  ) {
    const buttonHTML = `Registrant: ${registrant.name}`
    const expanded = false
    const label = 'registrant'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    let nameText = $('<div>')
    const changedName =
      registrant.name.toLowerCase() != registrantAnnotation.text
    if (registrantAnnotation.logoHTML || changedName) {
      nameText.append(
        $('<p>')
          .html(registrantAnnotation.logoHTML)
          .append(
            $('<a>')
              .attr(
                'href',
                encodeEntity(registrantAnnotation.text, EXPLORER_ROUTE)
              )
              .text(registrantAnnotation.stylizedText)
          )
      )
      if (changedName) {
        nameText.append(
          $('<p>')
            .css('font-size', '90%')
            .css('font-style', 'italic')
            .text(`As written: "${registrant.name}"`)
        )
      }
    } else {
      nameText.append(
        $('<a>')
          .attr('href', encodeEntity(registrantAnnotation.text, EXPLORER_ROUTE))
          .text(registrantAnnotation.stylizedText)
      )
    }
    accordion.appendFeature('Name', nameText)

    accordion.appendFeature('Description', registrant.description)

    const addressStr = usLobbyingAddressFromObject(registrant)
    accordion.appendFeature('Address', addressStr)

    if (registrant.contact_name) {
      let contactText = $('<div>')
      let contactPara = $('<p>')
      contactText.append(contactPara)
      contactPara
        .append(contactAnnotation.logoHTML)
        .append(
          $('<a>')
            .attr('href', encodeEntity(contactAnnotation.text, EXPLORER_ROUTE))
            .text(contactAnnotation.stylizedText)
        )
      if (registrant.contact_telephone) {
        contactPara
          .append('<br/>')
          .append(formatPhoneNumber(registrant.contact_telephone))
      }
      if (registrant.contact_name.toLowerCase() != contactAnnotation.text) {
        contactText.append(
          $('<p>')
            .css('font-size', '90%')
            .css('font-style', 'italic')
            .text(`As written: "${registrant.contact_name}"`)
        )
      }

      accordion.appendFeature('Point of Contact w/ Client', contactText)
    }

    accordion.appendFeature('Posted By', filing.posted_by_name)

    accordion.appendFeature(
      'ID',
      $('<a>').attr('href', registrant.url).text(registrant.id)
    )
  }

  function appendClient(client, clientAnnotation, parentHTML, parentID) {
    const buttonHTML = `Client: ${client.name}`
    const expanded = false
    const label = 'client'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    let nameText = $('<div>')
    const changedName = client.name.toLowerCase() != clientAnnotation.text
    if (clientAnnotation.logoHTML || changedName) {
      nameText.append(
        $('<p>')
          .html(clientAnnotation.logoHTML)
          .append(
            $('<a>')
              .attr('href', encodeEntity(clientAnnotation.text, EXPLORER_ROUTE))
              .text(clientAnnotation.stylizedText)
          )
      )
      if (changedName) {
        nameText.append(
          $('<p>')
            .css('font-size', '90%')
            .css('font-style', 'italic')
            .text(`As written: "${client.name}"`)
        )
      }
    } else {
      nameText.append(
        $('<a>')
          .attr('href', encodeEntity(clientAnnotation.text, EXPLORER_ROUTE))
          .text(clientAnnotation.stylizedText)
      )
    }
    accordion.appendFeature('Name', nameText)

    accordion.appendFeature('Description', client.description)
    accordion.appendFeature('General Description', client.general_description)

    const addressStr = usLobbyingAddressFromObject(client)
    accordion.appendFeature('Address', addressStr)

    const govEntityText =
      client.client_government_entity +
      '<br/>' +
      '<span style="font-style: italic;">' +
      'Note: Senate OPR seems to inconsistently fill this field relative ' +
      'to whether box is checked in document URL given above' +
      '</span>'
    const govEntityColor = '#FFFF80'
    const govEntityGuard = client.client_government_entity
    accordion.appendFeature(
      'Government Entity',
      govEntityText,
      true,
      govEntityGuard,
      govEntityColor
    )

    accordion.appendFeature(
      'ID',
      $('<a>').attr('href', client.url).text(client.id)
    )
    accordion.appendFeature('ID Relative to Registrant', client.client_id)
  }

  function appendAffiliatedOrganizations(
    orgs,
    orgAnnotations,
    parentHTML,
    parentID
  ) {
    if (!orgs.length) {
      return
    }

    const buttonHTML = 'Affiliated Organizations'
    const expanded = false
    const label = 'affiliatedOrgs'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    orgs.forEach(function (org, orgIndex) {
      const annotation = orgAnnotations[orgIndex]

      let table = $('<table>')
        .addClass('table table-striped table-fit modal-table')
        .append($('<thead>'))
      collapseBody.append(table)
      let tbody = $('<tbody>')
      table.append(tbody)

      let name = $('<div>')
        .addClass('card-body')
        .append(
          $('<h3>')
            .addClass('card-title')
            .html(annotation.logoHTML)
            .append(
              $('<a>')
                .attr('href', encodeEntity(annotation.text, EXPLORER_ROUTE))
                .text(annotation.stylizedText)
            )
        )
      if (org.name.toLowerCase() != annotation.text) {
        name.append(
          $('<p>')
            .addClass('card-text modal-card-text')
            .text(`As written: "${org.name}"`)
        )
      }
      appendKeyValueToTableBody(tbody, 'Name', name)

      if (org.url) {
        appendKeyValueToTableBody(
          tbody,
          'URL',
          $('<a>').attr('href', org.url).text(org.url)
        )
      }

      const address = usLobbyingAddressFromObject(org)
      if (address) {
        appendKeyValueToTableBody(tbody, 'Address', address)
      }
    })
  }

  function appendConvictionDisclosures(disclosures, parentHTML, parentID) {
    if (!disclosures.length) {
      return
    }

    const buttonHTML = 'Conviction Disclosures'
    const expanded = false
    const label = 'convictions'
    const labelIndex = undefined
    const buttonColor = '#FF8080'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label,
      labelIndex,
      buttonColor
    )
    let collapseBody = accordion.collapseBody()

    disclosures.forEach(function (disclosure) {
      let table = $('<table>')
        .addClass('table table-striped table-fit modal-table')
        .append($('<thead>'))
      collapseBody.append(table)
      let tbody = $('<tbody>')
      table.append(tbody)

      appendKeyValueToTableBody(
        tbody,
        'Lobbyist Name',
        lobbyistNameFromObject(disclosure.lobbyist)
      )
      appendKeyValueToTableBody(tbody, 'Lobbyist ID', disclosure.lobbyist.id)
      appendKeyValueToTableBody(tbody, 'Description', disclosure.description)
      appendKeyValueToTableBody(tbody, 'Date', disclosure.date)
    })
  }

  function appendForeignEntities(foreignEntities, parentHTML, parentID) {
    if (!foreignEntities.length) {
      return
    }

    const buttonHTML = 'Foreign Entities'
    const expanded = false
    const label = 'foreignEntities'
    const labelIndex = undefined
    const buttonColor = '#FF8080'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label,
      labelIndex,
      buttonColor
    )
    let collapseBody = accordion.collapseBody()

    foreignEntities.forEach(function (foreignEntity) {
      let table = $('<table>')
        .addClass('table table-striped table-fit modal-table')
        .append($('<thead>'))
      collapseBody.append(table)
      let tbody = $('<tbody>')
      table.append(tbody)

      appendKeyValueToTableBody(tbody, 'Name', foreignEntity.name)
      if (foreignEntity.contribution) {
        appendKeyValueToTableBody(
          tbody,
          'Contribution',
          foreignEntity.contribution
        )
      }
      if (foreignEntity.ownership_percentage) {
        appendKeyValueToTableBody(
          tbody,
          'Ownership Percentage',
          foreignEntity.ownership_percentage
        )
      }
      const addressStr = usLobbyingAddressFromObject(foreignEntity)
      if (addressStr) {
        appendKeyValueToTableBody(tbody, 'Address', addressStr)
      }
    })
  }

  function appendActivityBills(
    annotation,
    activityIndex,
    parentHTML,
    parentID,
    billLinks
  ) {
    if (
      !(
        annotation.house.bills.length ||
        annotation.house.resolutions.length ||
        annotation.house.jointResolutions.length ||
        annotation.house.concurrentResolutions.length ||
        annotation.senate.bills.length ||
        annotation.senate.resolutions.length ||
        annotation.senate.jointResolutions.length ||
        annotation.senate.concurrentResolutions.length
      )
    ) {
      return
    }

    const buttonHTML = 'Referenced Bills'
    const expanded = false
    const label = 'bills'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label,
      activityIndex
    )

    function formBillList(items) {
      let itemList = $('<ul>').css('padding-left', '1em')
      items.forEach(function (item) {
        itemList.append(
          $('<li>').html($('<a>').attr('href', item.url).text(item.label))
        )
      })
      return itemList
    }

    if (annotation.house.bills.length) {
      annotation.house.bills.forEach(function (item) {
        billLinks['house']['bills'].add(JSON.stringify(item))
      })
      const billList = formBillList(annotation.house.bills)
      accordion.appendFeature('House Bills', billList)
    }
    if (annotation.house.resolutions.length) {
      annotation.house.resolutions.forEach(function (item) {
        billLinks['house']['resolutions'].add(JSON.stringify(item))
      })
      const billList = formBillList(annotation.house.resolutions)
      accordion.appendFeature('House Resolutions', billList)
    }
    if (annotation.house.jointResolutions.length) {
      annotation.house.jointResolutions.forEach(function (item) {
        billLinks['house']['jointResolutions'].add(JSON.stringify(item))
      })
      const billList = formBillList(annotation.house.jointResolutions)
      accordion.appendFeature('House Joint Resolutions', billList)
    }
    if (annotation.house.concurrentResolutions.length) {
      annotation.house.concurrentResolutions.forEach(function (item) {
        billLinks['house']['concurrentResolutions'].add(JSON.stringify(item))
      })
      const billList = formBillList(annotation.house.concurrentResolutions)
      accordion.appendFeature('House Concurrent Resolutions', billList)
    }

    if (annotation.senate.bills.length) {
      annotation.senate.bills.forEach(function (item) {
        billLinks['senate']['bills'].add(JSON.stringify(item))
      })
      const billList = formBillList(annotation.senate.bills)
      accordion.appendFeature('Senate Bills', billList)
    }
    if (annotation.senate.resolutions.length) {
      annotation.senate.resolutions.forEach(function (item) {
        billLinks['senate']['resolutions'].add(JSON.stringify(item))
      })
      const billList = formBillList(annotation.senate.resolutions)
      accordion.appendFeature('Senate Resolutions', billList)
    }
    if (annotation.senate.jointResolutions.length) {
      annotation.senate.jointResolutions.forEach(function (item) {
        billLinks['senate']['jointResolutions'].add(JSON.stringify(item))
      })
      const billList = formBillList(annotation.senate.jointResolutions)
      accordion.appendFeature('Senate Joint Resolutions', billList)
    }
    if (annotation.senate.concurrentResolutions.length) {
      annotation.senate.concurrentResolutions.forEach(function (item) {
        billLinks['senate']['concurrentResolutions'].add(JSON.stringify(item))
      })
      const billList = formBillList(annotation.senate.concurrentResolutions)
      accordion.appendFeature('Senate Concurrent Resolutions', billList)
    }
  }

  function appendActivityLobbyists(
    activity,
    activityAnnotation,
    activityIndex,
    parentHTML,
    parentID
  ) {
    if (!activity.lobbyists.length) {
      return
    }

    const buttonHTML = 'Lobbyists'
    const expanded = false
    const label = 'lobbyists'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label,
      activityIndex
    )
    let collapseBody = accordion.collapseBody()

    activity.lobbyists.forEach(function (lobbyist, lobbyistIndex) {
      const annotation = activityAnnotation['lobbyists'][lobbyistIndex]

      let table = $('<table>')
        .addClass('table table-striped table-fit modal-table')
        .append($('<thead>'))
      collapseBody.append(table)
      let tbody = $('<tbody>')
      table.append(tbody)

      // Insert the lobbyist name section.
      const name = lobbyistNameFromObject(lobbyist.lobbyist)
      if (annotation.stylizedText) {
        let nameCardBody = $('<div>').addClass('card-body')
        nameCardBody.append(
          $('<h3>')
            .addClass('card-title')
            .html(annotation.logoHTML)
            .append(
              $('<a>')
                .attr('href', encodeEntity(annotation.text, EXPLORER_ROUTE))
                .text(annotation.stylizedText)
            )
        )
        nameCardBody.append(
          $('<p>')
            .addClass('card-text modal-card-text')
            .text(`As written: "${name}"`)
        )
        appendKeyValueToTableBody(tbody, 'Name', nameCardBody)
      } else {
        appendKeyValueToTableBody(tbody, 'Name', name)
      }

      appendKeyValueToTableBody(tbody, 'ID', lobbyist.lobbyist.id)
      if (lobbyist.covered_position) {
        appendKeyValueToTableBody(
          tbody,
          'Covered Position',
          lobbyist.covered_position
        )
      }
      if (lobbyist.new) {
        tbody.append(
          $('<tr>')
            .css('background-color', '#FFFF80')
            .append($('<th>').attr('scope', 'row').text('New'))
            .append($('<td>').text(lobbyist.new))
        )
      }
    })
  }

  function appendActivityGovernmentEntities(
    activity,
    activityIndex,
    parentHTML,
    parentID
  ) {
    if (!activity.government_entities.length) {
      return
    }

    let govIDs = undefined
    $.getJSON('/js/us/central/lobbying/government_ids.json', function (data) {
      govIDs = data
    })

    const buttonHTML = 'Lobbied Government Entities'
    const expanded = false
    const label = 'govEntities'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label,
      activityIndex
    )
    let collapseBody = accordion.collapseBody()

    activity.government_entities.forEach(function (govEntity, govEntityIndex) {
      let card = $('<div>').addClass('card')
      collapseBody.append(card)
      let cardBody = $('<div>').addClass('card-body')
      card.append(cardBody)
      let cardText = $('<p>').addClass('card-text modal-card-text')
      cardBody.append(cardText)

      if (typeof govEntity == 'object' && govEntity !== null) {
        cardText.text(govEntity.name)
      } else if (String(govEntity) in govIDs) {
        const entityData = govIDs[String(govEntity)]

        cardText
          .append(
            $('<a>')
              .attr('href', entityData.url)
              .html(
                $('<img>')
                  .attr('src', entityData.logo)
                  .css('height', '43m')
                  .css('width', '14em')
                  .css('objectFit', 'contain')
                  .css('verticalAlign', 'sub')
                  .css('padding', '0')
                  .css('marginRight', '0.5em')
              )
          )
          .append(entityData.text)
      } else {
        console.log(`Did not have value stored for government ID ${govEntity}`)
        cardText.append(`ID: ${govEntity}`)
      }
    })
  }

  function appendBills(billLinks, parentHTML, parentID) {
    if (
      !(
        billLinks.house.bills.length ||
        billLinks.house.resolutions.length ||
        billLinks.house.jointResolutions.length ||
        billLinks.house.concurrentResolutions.length ||
        billLinks.senate.bills.length ||
        billLinks.senate.resolutions.length ||
        billLinks.senate.jointResolutions.length ||
        billLinks.senate.concurrentResolutions.length
      )
    ) {
      return
    }

    const buttonHTML = 'Referenced Bills'
    const expanded = false
    const label = 'bills'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    function formBillList(items) {
      let itemList = $('<ul>').css('padding-left', '1em')
      items.forEach(function (item) {
        itemList.append(
          $('<li>').html($('<a>').attr('href', item.url).text(item.label))
        )
      })
      return itemList
    }

    if (billLinks.house.bills.length) {
      const billList = formBillList(billLinks.house.bills)
      accordion.appendFeature('House Bills', billList)
    }
    if (billLinks.house.resolutions.length) {
      const billList = formBillList(billLinks.house.resolutions)
      accordion.appendFeature('House Resolutions', billList)
    }
    if (billLinks.house.jointResolutions.length) {
      const billList = formBillList(billLinks.house.jointResolutions)
      accordion.appendFeature('House Joint Resolutions', billList)
    }
    if (billLinks.house.concurrentResolutions.length) {
      const billList = formBillList(billLinks.house.concurrentResolutions)
      accordion.appendFeature('House Concurrent Resolutions', billList)
    }

    if (billLinks.senate.bills.length) {
      const billList = formBillList(billLinks.senate.bills)
      accordion.appendFeature('Senate Bills', billList)
    }
    if (billLinks.senate.resolutions.length) {
      const billList = formBillList(billLinks.senate.resolutions)
      accordion.appendFeature('Senate Resolutions', billList)
    }
    if (billLinks.senate.jointResolutions.length) {
      const billList = formBillList(billLinks.senate.jointResolutions)
      accordion.appendFeature('Senate Joint Resolutions', billList)
    }
    if (billLinks.senate.concurrentResolutions.length) {
      const billList = formBillList(billLinks.senate.concurrentResolutions)
      accordion.appendFeature('Senate Concurrent Resolutions', billList)
    }
  }

  function appendLobbyingActivities(
    lobbyingActivities,
    lobbyingActivityAnnotations,
    parentHTML,
    parentID
  ) {
    if (!lobbyingActivities.length) {
      return
    }

    let billLinks = {
      house: {
        bills: new Set(),
        resolutions: new Set(),
        jointResolutions: new Set(),
        concurrentResolutions: new Set(),
      },
      senate: {
        bills: new Set(),
        resolutions: new Set(),
        jointResolutions: new Set(),
        concurrentResolutions: new Set(),
      },
    }

    lobbyingActivities.forEach(function (activity, activityIndex) {
      const buttonHTML = `Issue: ${activity.general_issue_code_display}`
      const expanded = false
      const label = 'activity'
      const accordion = new Accordion(
        parentHTML,
        parentID,
        buttonHTML,
        expanded,
        label,
        activityIndex
      )
      let collapseBody = accordion.collapseBody()

      const descriptionAnnotation = getDescriptionAnnotation(
        activity.description
      )

      accordion.appendFeature(
        'Description of Focus for Lobbying on this Issue',
        descriptionAnnotation.description
      )

      if (activity.foreign_entity_issues) {
        let foreignCard = $('<div>').addClass('card')
        collapseBody.append(foreignCard)
        let foreignCardBody = $('<div>').addClass('card-body')
        foreignCard.append(foreignCardBody)

        foreignCardBody
          .append(
            $('<h3>')
              .addClass('card-title')
              .css('background-color', '#FFFF80')
              .text('Foreign Entity Issues')
          )
          .append(
            $('<p>')
              .addClass('card-text modal-card-text')
              .html(activity.foreign_entity_issues)
          )
      }

      const accordionID = `${label}Accordion`
      let panelGroup = $('<div>').addClass('accordion').attr('id', accordionID)
      collapseBody.append(panelGroup)

      appendActivityBills(
        descriptionAnnotation,
        activityIndex,
        panelGroup,
        accordionID,
        billLinks
      )
      const annotation = lobbyingActivityAnnotations[activityIndex]
      appendActivityLobbyists(
        activity,
        annotation,
        activityIndex,
        panelGroup,
        accordionID
      )
      appendActivityGovernmentEntities(
        activity,
        activityIndex,
        panelGroup,
        accordionID
      )
    })

    function billSetToList(bills) {
      let billList = []
      bills.forEach(function (item) {
        billList.push(JSON.parse(item))
      })
      return billList
    }

    billLinks['house']['bills'] = billSetToList(billLinks['house']['bills'])
    billLinks['house']['resolutions'] = billSetToList(
      billLinks['house']['resolutions']
    )
    billLinks['house']['jointResolutions'] = billSetToList(
      billLinks['house']['jointResolutions']
    )
    billLinks['house']['concurrentResolutions'] = billSetToList(
      billLinks['house']['concurrentResolutions']
    )
    billLinks['senate']['bills'] = billSetToList(billLinks['senate']['bills'])
    billLinks['senate']['resolutions'] = billSetToList(
      billLinks['senate']['resolutions']
    )
    billLinks['senate']['jointResolutions'] = billSetToList(
      billLinks['senate']['jointResolutions']
    )
    billLinks['senate']['concurrentResolutions'] = billSetToList(
      billLinks['senate']['concurrentResolutions']
    )
    appendBills(billLinks, parentHTML, parentID)
  }

  let content = holder.find('.modal-content')
  content.empty()

  let title = filing.filing_year + ' '
  if (filing.filing_period == 'first_quarter') {
    title += 'First Quarter'
  } else if (filing.filing_period == 'second_quarter') {
    title += 'Second Quarter'
  } else if (filing.filing_period == 'third_quarter') {
    title += 'Third Quarter'
  } else if (filing.filing_period == 'fourth_quarter') {
    title += 'Fourth Quarter'
  } else {
    title += filing.filing_period
  }
  title += ' Lobbying '
  if (filing.filing_type == 'RR') {
    title += 'Registration'
  } else if (filing.filing_type == 'RA') {
    title += 'Registration Amendment'
  } else {
    title += 'Report'
  }
  title += ' (Posted: ' + dateWithoutTime(new Date(filing.dt_posted)) + ')'
  if (filing.filing_uuid) {
    const documentURL = documentURLFromUUID(filing.filing_uuid)
    const jsonURL = jsonURLFromUUID(filing.filing_uuid)
    title +=
      `<a href="${documentURL}">` +
      '<img src="/logos/document.jpg" ' +
      'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" />' +
      '</a>'
    title +=
      `<a href="${jsonURL}">` +
      '<img src="/logos/json-file.svg" ' +
      'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" />' +
      '</a>'
  }

  if (filing.income || filing.expenses) {
    let amountStr = ''
    if (filing.income) {
      amountStr = 'Income: $' + numberWithCommas(filing.income) + ' USD'
    } else if (filing.expenses) {
      amountStr = 'Expenses: $' + numberWithCommas(filing.expenses) + ' USD'
    }
    title += `<br/><span style="font-size: 100%;">${amountStr}</span>`
  }

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'issuesAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendRegistrant(
    filing.registrant,
    filing.annotation.registrant,
    filing.annotation.contact,
    accordionDiv,
    accordionID
  )

  appendClient(
    filing.client,
    filing.annotation.client,
    accordionDiv,
    accordionID
  )

  appendAffiliatedOrganizations(
    filing.affiliated_organizations,
    filing.annotation['affiliatedOrgs'],
    accordionDiv,
    accordionID
  )

  appendConvictionDisclosures(
    filing.conviction_disclosures,
    accordionDiv,
    accordionID
  )

  appendForeignEntities(filing.foreign_entities, accordionDiv, accordionID)

  appendLobbyingActivities(
    filing.lobbying_activities,
    filing.annotation.lobbyingActivities,
    accordionDiv,
    accordionID
  )
}
