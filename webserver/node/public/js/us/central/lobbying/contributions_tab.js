function USLobbyingContributionsTab(
  tabsState,
  data,
  chartTitlePrefix,
  chartSubject,
  alternateNames = undefined
) {
  const label = 'usLobbyingContributions'
  const tabLogo = '/logos/government_of_the_united_states.svg'
  const tabTitle = 'US Politial Contributions'
  const queryURL =
    appendURLPath(EXPLORER_ROUTE, 'api/us/central/lobbying/contributions?') +
    (data['vendor']
      ? `vendor=${encodeURIComponentIncludingSlashes(data.vendor)}`
      : `text=${encodeURIComponentIncludingSlashes(data.text)}`)

  const recordsNote = $('<div>')
    .append(
      $('<p>')
        .text('All links and images are annotations added to the original ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', 'https://lda.senate.gov/system/public/')
            .text('Senate OPR REST API data')
        )
        .append(
          ' by Tech Inquiry. Any normalized names are clearly labeled with the ' +
            "original ('as written') text."
        )
    )
    .append(
      $('<p>')
        .append('You can alternatively download the ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', queryURL)
            .text('JSON data')
        )
        .append(' for this tab.')
    )

  const alternateNamesTitle = 'Included Names'
  const haveSummary = true
  FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote,
    alternateNames,
    alternateNamesTitle,
    haveSummary
  )

  const tabsID = `${label}-tabs`

  function renderSummaryTab(event, ui) {
    $.ajax({
      url: appendURLPath(
        EXPLORER_ROUTE,
        'api/us/central/lobbying/contributions/aggregates'
      ),
      data: data,
    }).done(function (result) {
      const summaryLabel = `${label}-tab-summary`
      const relativeParentID = tabsID
      const holder = $(`#${summaryLabel}`)
      const selectID = `${summaryLabel}-select`
      const chartID = `${summaryLabel}-chart`
      const chartContainerID = `${summaryLabel}-chart-container`
      USLobbyingContributionsChart(
        holder,
        result,
        chartTitlePrefix,
        chartSubject,
        relativeParentID,
        selectID,
        chartID,
        chartContainerID
      )
    })
  }

  function renderRecordsTab(event, ui) {
    const containerID = `${label}-tab-records-inner`
    USLobbyingContributionsTable(data, containerID)
  }

  function renderTab(event, ui) {
    const recordsTab = 0
    const summaryTab = 1
    const activeTab = $(`#${tabsID}`).tabs('option', 'active')
    if (activeTab == summaryTab) {
      renderSummaryTab(event, ui)
    } else if (activeTab == recordsTab) {
      renderRecordsTab(event, ui)
    }
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
