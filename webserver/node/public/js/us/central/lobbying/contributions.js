function USLobbyingContributions(holder, filing, titleID) {
  function jsonURLFromUUID(uuid) {
    return `https://lda.senate.gov/api/v1/contributions/${uuid}`
  }

  function documentURLFromUUID(uuid) {
    return `https://lda.senate.gov/filings/public/contribution/${uuid}/print/`
  }

  function appendFiler(parentHTML, parentID) {
    const addressStr = usLobbyingAddressFromObject(filing)
    if (!(addressStr || filing.filer_type_display || filing.contact_name)) {
      return
    }

    const buttonHTML = 'Filer'
    const expanded = false
    const label = 'filer'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Filer Type', filing.filer_type_display)
    accordion.appendFeature('Filer Address', addressStr)
    accordion.appendFeature('Contact Name', filing.contact_name)
  }

  function appendRegistrant(
    registrant,
    registrantAnnotation,
    contactAnnotation,
    parentHTML,
    parentID
  ) {
    const buttonHTML = `Registrant: ${registrant.name}`
    const expanded = false
    const label = 'registrant'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    let nameText = $('<div>')
    const changedName =
      registrant.name.toLowerCase() != registrantAnnotation.text
    if (registrantAnnotation.logoHTML || changedName) {
      nameText.append(
        $('<p>')
          .html(registrantAnnotation.logoHTML)
          .append(
            $('<a>')
              .attr(
                'href',
                encodeEntity(registrantAnnotation.text, EXPLORER_ROUTE)
              )
              .text(registrantAnnotation.stylizedText)
          )
      )
      if (changedName) {
        nameText.append(
          $('<p>')
            .css('font-size', '90%')
            .css('font-style', 'italic')
            .text(`As written: "${registrant.name}"`)
        )
      }
    } else {
      nameText.append(
        $('<a>')
          .attr('href', encodeEntity(registrantAnnotation.text, EXPLORER_ROUTE))
          .text(registrantAnnotation.stylizedText)
      )
    }
    accordion.appendFeature('Registrant Name', nameText)
    accordion.appendFeature('Registrant Description', registrant.description)

    const addressStr = usLobbyingAddressFromObject(registrant)
    accordion.appendFeature('Registrant Address', addressStr)

    if (registrant.contact_name) {
      let contactText = $('<div>')
      let contactPara = $('<p>')
      contactText.append(contactPara)
      contactPara
        .append(contactAnnotation.logoHTML)
        .append(
          $('<a>')
            .attr('href', encodeEntity(contactAnnotation.text, EXPLORER_ROUTE))
            .text(contactAnnotation.stylizedText)
        )
      if (registrant.contact_telephone) {
        contactPara
          .append('<br/>')
          .append(formatPhoneNumber(registrant.contact_telephone))
      }
      if (registrant.contact_name.toLowerCase() != contactAnnotation.text) {
        contactText.append(
          $('<p>')
            .css('font-size', '90%')
            .css('font-style', 'italic')
            .text(`As written: "${registrant.contact_name}"`)
        )
      }

      accordion.appendFeature('Point of Contact', contactText)
    }

    accordion.appendFeature(
      'Registrant ID',
      $('<a>').attr('href', registrant.url).text(registrant.id)
    )
  }

  function appendLobbyist(filing, annotation, parentHTML, parentID) {
    lobbyist = filing.lobbyist
    const addressStr =
      filing.filer_type == 'lobbyist' ? usLobbyingAddressFromObject(filing) : ''
    if (!(lobbyist || addressStr)) {
      return
    }

    const name = lobbyistNameFromObject(lobbyist)

    const buttonHTML = `Lobbyist: ${name}`
    const expanded = false
    const label = 'lobbyist'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    let table = $('<table>')
      .addClass('table table-striped table-fit modal-table')
      .append($('<thead>'))
    collapseBody.append(table)
    let tbody = $('<tbody>')
    table.append(tbody)

    // Insert the lobbyist name section.
    if (annotation.stylizedText) {
      let nameCardBody = $('<div>').addClass('card-body')
      nameCardBody.append(
        $('<h3>')
          .addClass('card-title')
          .html(annotation.logoHTML)
          .append(
            $('<a>')
              .attr('href', encodeEntity(annotation.text, EXPLORER_ROUTE))
              .text(annotation.stylizedText)
          )
      )
      nameCardBody.append(
        $('<p>')
          .addClass('card-text modal-card-text')
          .text(`As written: "${name}"`)
      )
      appendKeyValueToTableBody(tbody, 'Name', nameCardBody)
    } else {
      appendKeyValueToTableBody(tbody, 'Name', name)
    }

    appendKeyValueToTableBody(tbody, 'ID', lobbyist.id)
    appendKeyValueToTableBody(tbody, 'Address', addressStr)
  }

  function appendComments(comments, parentHTML, parentID) {
    if (!comments) {
      return
    }

    const maxTitleLength = 75

    const buttonHTML = trimLabel(`Comments: ${comments}`, maxTitleLength)
    const expanded = false
    const label = 'comments'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Comments', comments)
  }

  function appendPACs(pacs, parentHTML, parentID) {
    if (!pacs.length) {
      return
    }

    const maxTitleLength = 75

    let buttonHTML = 'PACS: '
    pacs.forEach(function (pac, index) {
      if (index > 0) {
        buttonHTML += ', '
      }
      buttonHTML += pac
    })
    buttonHTML = trimLabel(buttonHTML, maxTitleLength)
    const expanded = false
    const label = 'pacs'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    let pacDiv = $('<ul>').css('padding-left', '1em')
    pacs.forEach(function (pac) {
      pacDiv.append($('<li>').text(pac))
    })

    accordion.appendFeature('PACs', pacDiv)
  }

  function appendContributionItems(
    items,
    itemAnnotations,
    parentHTML,
    parentID
  ) {
    if (!items.length) {
      return
    }

    const buttonHTML = 'Contribution Items'
    const expanded = false
    const label = 'contribution'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    const accordionID = `${label}ItemsAccordion`
    let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
    collapseBody.append(accordionDiv)

    items.forEach(function (item, index) {
      const annotation = itemAnnotations[index]

      let itemButtonHTML = ''
      if (item.payee_name && item.honoree_name) {
        itemButtonHTML = `${item.payee_name}/${item.honoree_name}`
      } else if (item.payee_name) {
        itemButtonHTML = item.payee_name
      } else if (item.honoree_name) {
        itemButtonHTML = item.honoree_name
      }
      if (item.amount && item.date) {
        if (itemButtonHTML) {
          itemButtonHTML += ': '
        }
        itemButtonHTML += `$${numberWithCommas(item.amount)}, ${dateWithoutTime(
          new Date(item.date)
        )}`
      } else if (item.amount) {
        if (itemButtonHTML) {
          itemButtonHTML += ': '
        }
        itemButtonHTML += `$${numberWithCommas(item.amount)}`
      } else if (item.date) {
        if (itemButtonHTML) {
          itemButtonHTML += ': '
        }
        itemButtonHTML += dateWithoutTime(new Date(item.date))
      }
      const itemExpanded = false
      const itemLabel = `${label}Item`
      const itemAccordion = new Accordion(
        accordionDiv,
        accordionID,
        itemButtonHTML,
        itemExpanded,
        itemLabel,
        index
      )

      itemAccordion.appendFeature('Payee Name', item.payee_name)
      itemAccordion.appendFeature('Honoree Name', item.honoree_name)
      itemAccordion.appendFeature(
        'Contribution Amount',
        '$' + numberWithCommas(item.amount),
        true,
        item.amount
      )
      itemAccordion.appendDate('Contribution Date', item.date)
      itemAccordion.appendFeature('Contributor Name', item.contributor_name)
      itemAccordion.appendFeature(
        'Contribution Type',
        item.contribution_type_display
      )
    })
  }

  let content = holder.find('.modal-content')
  content.empty()

  let title = filing.filing_year + ' '
  if (filing.filing_period == 'mid_year') {
    title += 'Mid-Year (Jan 1 - Jun 30)'
  } else if (filing.filing_period == 'year_end') {
    title += 'Year-End (July 1 - Dec 31)'
  } else {
    title += filing.filing_period
  }
  title += ' Lobbying '
  if (filing.filing_type == 'YY') {
    title += 'Year-End Report'
  } else if (filing.filing_type == 'YA') {
    title += 'Year-End Amendment'
  } else if (filing.filing_type == 'MM') {
    title += 'Mid-Year Report'
  } else if (filing.filing_type == 'MA') {
    title += 'Mid-Year Amendment'
  } else {
    title += 'Report'
  }
  title += ` (Posted: ${dateWithoutTime(new Date(filing.dt_posted))})`
  if (filing.filing_uuid) {
    const documentURL = documentURLFromUUID(filing.filing_uuid)
    const jsonURL = jsonURLFromUUID(filing.filing_uuid)
    title +=
      `<a href="${documentURL}">` +
      '<img src="/logos/document.jpg" ' +
      'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" />' +
      '</a>'
    title +=
      `<a href="${jsonURL}">` +
      '<img src="/logos/json-file.svg" ' +
      'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" />' +
      '</a>'
  }

  if (filing.contribution_items.length) {
    let amount = 0
    filing.contribution_items.forEach(function (contribution) {
      amount += Number(contribution.amount)
    })
    let amountStr = `Amount: $${numberWithCommas(amount)} USD`
    title += `<br/><span style="font-size: 100%;">${amountStr}</span>`
  }

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'contributionsAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendFiler(accordionDiv, accordionID)

  appendRegistrant(
    filing.registrant,
    filing.annotation.registrant,
    filing.annotation.contact,
    accordionDiv,
    accordionID
  )

  appendLobbyist(filing, filing.annotation.lobbyist, accordionDiv, accordionID)

  appendComments(filing.comments, accordionDiv, accordionID)

  appendPACs(filing.pacs, accordionDiv, accordionID)

  appendContributionItems(
    filing.contribution_items,
    filing.annotation.contributions,
    accordionDiv,
    accordionID
  )
}
