function USLobbyingIssuesTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/us/central/lobbying/issues'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    let holder = $(`#${containerID}`)
    holder.empty()
    holder.append(
      $('<div>')
        .addClass('table-responsive')
        .append(
          $('<table>')
            .attr('id', tableID)
            .addClass('table table-striped table-bordered')
            .css('font-size', '140%')
        )
    )

    let tableData = []
    filings.forEach(function (filing, filingIndex) {
      const received = dateWithoutTimeLex(filing.dt_posted)
      const year = filing.filing_year
      const type = filing.filing_type
      const registrantName = filing.registrant.name
      const clientName = filing.client.name
      const amount = filing.income
        ? filing.income
        : filing.expenses
        ? filing.expenses
        : ''
      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        received,
        year,
        type,
        registrantName,
        clientName,
        amount,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Received', visible: true },
        { targets: 2, title: 'Year', visible: true },
        { targets: 3, title: 'Type', visible: true },
        { targets: 4, title: 'Registrant', visible: true },
        { targets: 5, title: 'Client', visible: true },
        {
          targets: 6,
          title: 'Amount',
          visible: true,
          render: $.fn.dataTable.render.number(',', '.', 2, '$'),
        },
        { targets: 7, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      USLobbyingIssues(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
