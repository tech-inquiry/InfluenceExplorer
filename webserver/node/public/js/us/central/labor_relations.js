function USLaborRelations(holder, filing, titleID) {
  function appendOverview(parentHTML, parentID, expanded) {
    if (!(filing.title || filing.location || filing.region_assigned)) {
      return
    }

    const buttonHTML = 'Overview'
    const label = 'description'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Title', filing.title)
    accordion.appendFeature('Location', filing.location)
    accordion.appendFeature('Region Assigned', filing.region_assigned)

    accordion.appendFeature(
      'Case Number',
      $('<a>')
        .attr('href', `https://www.nlrb.gov/case/${filing.case_number}`)
        .text(filing.case_number),
      true,
      filing.case_number
    )

    {
      let statusLabel = filing.status
      if (filing.reason_closed) {
        statusLabel += ` (${filing.reason_closed})`
      }
      accordion.appendFeature('Status', statusLabel)
    }

    accordion.appendFeature(
      'Last Update',
      dateWithoutTime(new Date(filing.lastUpdateDate)) +
        ': ' +
        filing.lastUpdateType,
      true,
      filing.lastUpdateDate
    )

    accordion.appendDate('Date Filed', filing.date_filed)

    if (filing.allegations.length) {
      let allegationsList = $('<ul>').css('padding-left', '1em')
      filing.annotation.allegations.forEach(function (allegation) {
        let item = $('<li>')
        if ('url' in allegation) {
          item.html(
            $('<a>').attr('href', allegation.url).text(allegation.stylizedText)
          )
        } else {
          item.text(allegation.stylizedText)
        }
        allegationsList.append(item)
      })
      accordion.appendFeature('Allegations', allegationsList)
    }
  }

  function appendDocketActivity(parentHTML, parentID, expanded) {
    if (!filing.docket_activity) {
      return
    }

    const buttonHTML = 'Docket Activity'
    const label = 'docket'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    let docketTable = $('<table>').addClass(
      'table table-striped table-fit modal-table'
    )
    collapseBody.append(docketTable)
    docketTable.append(
      $('<thead>').append(
        $('<tr>')
          .append($('<th>').attr('scope', 'col').text('Date'))
          .append($('<th>').attr('scope', 'col').text('Document Type'))
          .append($('<th>').attr('scope', 'col').text('Filed By'))
      )
    )

    let docketTBody = $('<tbody>')
    docketTable.append(docketTBody)
    filing.docket_activity.forEach(function (activity) {
      let row = $('<tr>')
      docketTBody.append(row)
      row.append($('<td>').text(activity.date))
      if (activity.document_url) {
        row.append(
          $('<td>').html(
            $('<a>')
              .attr('href', activity.document_url)
              .text(activity.document_type)
          )
        )
      } else {
        row.append($('<td>').text(activity.document_type))
      }
      row.append($('<td>').text(activity.filed_by))
    })
  }

  function appendParticipants(parentHTML, parentID, expanded) {
    if (!filing.participants.length) {
      return
    }

    const buttonHTML = 'Participants'
    const label = 'participants'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    const partyTypes = [
      {
        label: 'Charged Parties',
        data: filing.annotation.chargedParties,
      },
      {
        label: 'Charging Parties',
        data: filing.annotation.chargingParties,
      },
      {
        label: 'Involved Parties',
        data: filing.annotation.undecidedChargingParties,
      },
    ]
    partyTypes.forEach(function (partyType) {
      if (!partyType.data.length) {
        return
      }

      let participantsValue = $('<div>')
      partyType.data.forEach(function (party) {
        let partyTable = $('<table>')
          .addClass('table table-fit table-striped')
          .append($('<thead>'))
        participantsValue.append(partyTable)
        let partyTBody = $('<tbody>')
        partyTable.append(partyTBody)

        if (party.employer) {
          let employerCardBody = $('<div>').addClass('card-body')
          employerCardBody.append(
            $('<h3>')
              .addClass('card-title')
              .html(party.employer.logoHTML)
              .append(
                $('<a>')
                  .attr('href', party.employer.url)
                  .text(party.employer.stylizedText)
              )
          )
          if (party.employer.text != party.employer.origText) {
            employerCardBody.append(
              $('<p>')
                .addClass('card-text')
                .text(`As written: "${party.employer.origText}"`)
            )
          }
          appendKeyValueToTableBody(partyTBody, 'Employer', employerCardBody)
        }

        if (party.name) {
          let nameCardBody = $('<div>').addClass('card-body')
          nameCardBody.append(
            $('<h3>')
              .addClass('card-title')
              .html(party.name.logoHTML)
              .append(
                $('<a>')
                  .attr('href', party.name.url)
                  .text(party.name.stylizedText)
              )
          )
          if (party.name.text != party.name.origText) {
            nameCardBody.append(
              $('<p>')
                .addClass('card-text')
                .text(`As written: "${party.name.origText}"`)
            )
          }
          appendKeyValueToTableBody(partyTBody, 'Name', nameCardBody)
        }

        if (party.employment_type) {
          appendKeyValueToTableBody(
            partyTBody,
            'Employment Type',
            party.employment_type
          )
        }

        {
          // Insert the address.
          let addressStr = ''
          if (party.street_address) {
            addressStr = party.street_address.toUpperCase()
          }
          if (party.city_and_state) {
            if (addressStr) {
              addressStr += '<br/>'
            }
            addressStr += party.city_and_state.toUpperCase()
          }
          if (party.zipcode) {
            if (addressStr) {
              addressStr += ' '
            }
            addressStr += party.zipcode
          }

          if (addressStr) {
            appendKeyValueToTableBody(partyTBody, 'Address', addressStr)
          }
        }

        if (party.phone_number) {
          appendKeyValueToTableBody(
            partyTBody,
            'Phone Number',
            party.phone_number
          )
        }
      })
      accordion.appendFeature(partyType.label, participantsValue)
    })
  }

  function appendRelatedDocuments(parentHTML, parentID, expanded) {
    if (!filing.related_documents.length) {
      return
    }

    const buttonHTML = 'Related Documents'
    const label = 'relatedDocs'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    let docList = $('<ul>')
    filing.related_documents.forEach(function (doc) {
      let item = $('<li>')
      if (doc.url) {
        item.append($('<a>').attr('href', doc.url).text(doc.title))
      } else {
        item.text(doc.title)
      }
      docList.append(item)
    })
    accordion.appendFeature('Related Documents', docList)
  }

  function appendRelatedCases(parentHTML, parentID, expanded) {
    if (!filing.related_cases.length) {
      return
    }

    const buttonHTML = 'Related Cases'
    const label = 'relatedCases'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    let caseTable = $('<table>')
      .addClass('table table-fit table-striped')
      .css('font-size', '175%')
    collapseBody.append(caseTable)
    caseTable.append(
      $('<thead>').append(
        $('<tr>')
          .append($('<th>').attr('scope', 'col').text('Case Number'))
          .append($('<th>').attr('scope', 'col').text('Case Name'))
          .append($('<th>').attr('scope', 'col').text('Status'))
      )
    )

    let caseTBody = $('<tbody>')
    caseTable.append(caseTBody)
    filing.related_cases.forEach(function (info) {
      caseTBody.append(
        $('<tr>')
          .append(
            $('<td>').append(
              $('<a>')
                .attr('href', `https://www.nlrb.gov/case/${info.case_number}`)
                .text(info.case_number)
            )
          )
          .append($('<td>').text(info.case_name))
          .append($('<td>').text(info.status))
      )
    })
  }

  let content = holder.find('.modal-content')
  content.empty()

  const maxTitleLength = 200

  let title = filing.case_number + ' (' + filing.status + '): '
  if (filing.title) {
    title += filing.title
  }
  title = trimLabel(title, maxTitleLength)
  const documentURL = 'https://www.nlrb.gov/case/' + filing.case_number
  title +=
    `<a href="${documentURL}">` +
    '<img src="/logos/document.jpg" ' +
    'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" />' +
    '</a>'

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'nlrbAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendOverview(accordionDiv, accordionID, false)
  appendDocketActivity(accordionDiv, accordionID, false)
  appendParticipants(accordionDiv, accordionID, false)
  appendRelatedDocuments(accordionDiv, accordionID, false)
  appendRelatedCases(accordionDiv, accordionID, false)
}
