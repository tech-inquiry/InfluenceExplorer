const DOD_BASE = 'https://www.defense.gov/Newsroom/Contracts/Contract/Article/'

function USDODAnnouncements(holder, filing, titleID) {
  function appendText(parentHTML, parentID) {
    if (!filing.text) {
      return
    }

    const buttonHTML = 'Announcement Text'
    const expanded = false
    const label = 'text'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    const formattedText = filing.text.replace(/(?:\r\n|\r|\n)/g, '<br/>')

    accordion.appendFeature('', formattedText)
  }

  let content = holder.find('.modal-content')
  content.empty()

  let title =
    dateWithoutTime(new Date(filing.date)) + ': ' + trimLabel(filing.text, 100)
  const docURL = DOD_BASE + filing.article_id
  title +=
    `<a href="${docURL}">` +
    '<img src="/logos/document.jpg" ' +
    'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" /></a>'

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)
  content.append(modalFooter())

  const accordionID = 'USDODAnnouncementsAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendText(accordionDiv, accordionID)
}
