function USDODAnnouncementsTab(tabsState, dataBase) {
  const label = 'us-dod-announcements'
  const tabLogo = '/logos/government_of_the_united_states.svg'
  const tabTitle = 'US DoD Announcements'
  const queryURL =
    appendURLPath(EXPLORER_ROUTE, 'api/us/central/procurement/dod?') +
    (dataBase['vendor']
      ? `vendor=${encodeURIComponentIncludingSlashes(dataBase.vendor)}`
      : `text=${encodeURIComponentIncludingSlashes(dataBase.text)}`)
  const recordsNote = $('<div>')
    .append(
      $('<p>')
        .text('All links and images are annotations added to the original ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', 'https://www.defense.gov/Newsroom/Contracts/')
            .text('US DoD Contract Announcements')
        )
        .append(
          ' by Tech Inquiry. Any normalized names are clearly labeled with the ' +
            "original ('as written') text."
        )
    )
    .append(
      $('<p>')
        .append('You can alternatively download the ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', queryURL)
            .text('JSON data')
        )
        .append(' for this tab.')
    )

  FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote
  )

  const tabsID = `${label}-tabs`

  function renderRecordsTab(event, ui) {
    let data = dataBase
    const containerID = `${label}-tab-records-inner`
    USDODAnnouncementsTable(data, containerID)
  }

  function renderTab(event, ui) {
    const recordsTab = 0
    const activeTab = $(`#${tabsID}`).tabs('option', 'active')
    if (activeTab == recordsTab) {
      renderRecordsTab(event, ui)
    }
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
