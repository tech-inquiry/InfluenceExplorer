function USLaborRelationsTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/us/central/laborRelations'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    let tableData = []
    filings.forEach(function (filing, filingIndex) {
      const lastUpdate = dateWithoutTime(new Date(filing.lastUpdateDate))
      const caseNumber = filing.case_number
      const status = filing.status
      const title = filing.title ? filing.title : ''
      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        lastUpdate,
        title,
        caseNumber,
        status,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Last Update' },
        { targets: 2, title: 'Title' },
        { targets: 3, title: 'Case Number' },
        { targets: 4, title: 'Status' },
        { targets: 5, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      USLaborRelations(holder, filing, filingIndex, filings.length)
      holder.modal('show')
    })
  })
}
