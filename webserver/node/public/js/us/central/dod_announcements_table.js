function USDODAnnouncementsTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/us/central/procurement/dod'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    let tableData = []
    const maxTextLength = 256
    filings.forEach(function (filing, filingIndex) {
      const date = dateWithoutTimeLex(filing.date)
      const articleID = filing.article_id
      const text = trimLabel(filing.text, maxTextLength)
      const entireFiling = JSON.stringify(filing)

      tableData.push([filingIndex, date, articleID, text, entireFiling])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Date' },
        { targets: 2, title: 'ID' },
        { targets: 3, title: 'Text' },
        { targets: 4, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      USDODAnnouncements(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
