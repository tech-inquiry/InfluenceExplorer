function AUProcurementTab(tabsState, dataBase, alternateNames = undefined) {
  const label = 'au'
  const tabLogo = '/logos/government_of_australia.svg'
  const tabTitle = 'Australian Government Contracts'
  const queryURL =
    appendURLPath(EXPLORER_ROUTE, 'api/au/procurement?') +
    (dataBase['vendor']
      ? `vendor=${encodeURIComponentIncludingSlashes(dataBase.vendor)}`
      : `text=${encodeURIComponentIncludingSlashes(dataBase.text)}`)
  const recordsNote = $('<div>')
    .append(
      $('<p>')
        .text('All links and images are annotations added to the original ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', 'https://www.tenders.gov.au/')
            .text('federal contracts list')
        )
        .append(
          ' by Tech Inquiry. Any normalized names are clearly labeled with ' +
            "the original ('as written') text."
        )
    )
    .append(
      $('<p>')
        .text('See ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', 'https://www.open-contracting.org/')
            .text("Open Contracting Partnership's")
        )
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr(
              'href',
              'https://www.open-contracting.org/2020/02/11/what-does-australias-open-contracting-data-look-like/'
            )
            .text("What does Australia's open contracting data look like?")
        )
        .append(' for an overview.')
    )
    .append(
      $('<p>')
        .append('You can alternatively download the ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', queryURL)
            .text('JSON data')
        )
        .append(' for this tab.')
    )

  FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote,
    alternateNames
  )

  const tabsID = `${label}-tabs`

  function renderRecordsTab(event, ui) {
    const data = dataBase
    const containerID = `${label}-tab-records-inner`
    AUProcurementTable(data, containerID)
  }

  function renderTab(event, ui) {
    const recordsTab = 0
    const activeTab = $(`#${tabsID}`).tabs('option', 'active')
    if (activeTab == recordsTab) {
      renderRecordsTab(event, ui)
    }
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
