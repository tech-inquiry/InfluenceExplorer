function AUProcurement(holder, filing, titleID) {
  function appendActionDetails(parentHTML, parentID, expanded) {
    if (!filing.filing_id) {
      return
    }

    const buttonHTML = 'Contract Action Details'
    const label = 'actionDetails'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    //accordion.appendFeature('ID', filing.filing_id)

    if (filing.award) {
      accordion.appendFeature('Award ID', filing.award.id)
      accordion.appendFeature('Award Status', filing.award.status)
    }
    if (filing.contract) {
      accordion.appendFeature('Contract ID', filing.contract.id)
      accordion.appendFeature('Contract Status', filing.contract.status)
      accordion.appendFeature(
        'Contract Procurement Method',
        filing.contract.procurementMethod
      )
      accordion.appendFeature(
        'Contract Procurement Method Details',
        filing.contract.procurementMethodDetails
      )
    }
    if (filing.tender) {
      accordion.appendFeature('Tender ID', filing.tender.id)
      accordion.appendFeature('Tender Status', filing.tender.status)
      accordion.appendFeature(
        'Tender Procurement Method',
        filing.tender.procurementMethod
      )
      accordion.appendFeature(
        'Tender Procurement Method Details',
        filing.tender.procurementMethodDetails
      )
    }
  }

  function appendDates(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Dates'
    const label = 'dates'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendDate('Date Published', filing.published_date)
    accordion.appendDate('Date', filing.date)
    if (filing.award) {
      accordion.appendDate('Award Date', filing.award.date)

      if (filing.award.contractPeriod) {
        let period = ''
        if (filing.award.contractPeriod.startDate) {
          period = dateWithoutTime(
            new Date(filing.award.contractPeriod.startDate)
          )
        }
        period += ' -- '
        if (filing.award.contractPeriod.endDate) {
          period += dateWithoutTime(
            new Date(filing.award.contractPeriod.endDate)
          )
        }
        accordion.appendFeature('Award Period', period)
      }
    }
    if (filing.tender) {
      if (filing.tender.tenderPeriod) {
        let period = ''
        if (filing.tender.tenderPeriod.startDate) {
          period = dateWithoutTime(
            new Date(filing.tender.tenderPeriod.startDate)
          )
        }
        period += ' -- '
        if (filing.tender.tenderPeriod.endDate) {
          period += dateWithoutTime(
            new Date(filing.tender.tenderPeriod.endDate)
          )
        }
        accordion.appendFeature('Tender Period', period)
      }
      if (filing.tender.milestones) {
        filing.tender.milestones.forEach(function (item, index) {
          let milestoneHTML = $('<div>')
          if (item.description) {
            milestoneHTML.append(item.description)
          }
          if (item.dueDate) {
            if (item.description) {
              milestoneHTML.append('<br/>')
            }
            milestoneHTML.append(dateWithoutTime(new Date(item.dueDate)))
          }
          accordion.appendFeature(`Milestone ${index + 1}`, milestoneHTML)
        })
      }
    }
    if (filing.contract) {
      if (filing.contract.contractPeriod) {
        let period = ''
        if (filing.contract.contractPeriod.startDate) {
          period = dateWithoutTime(
            new Date(filing.contract.contractPeriod.startDate)
          )
        }
        period += ' -- '
        if (filing.contract.contractPeriod.endDate) {
          period += dateWithoutTime(
            new Date(filing.contract.contractPeriod.endDate)
          )
        }
        accordion.appendFeature('Contract Period', period)
      }
      if (filing.contract.milestones) {
        filing.contract.milestones.forEach(function (item, index) {
          let milestoneHTML = $('<div>')
          if (item.description) {
            milestoneHTML.append(item.description)
          }
          if (item.dueDate) {
            if (item.description) {
              milestoneHTML.append('<br/>')
            }
            milestoneHTML.append(dateWithoutTime(new Date(item.dueDate)))
          }
          accordion.appendFeature(`Milestone ${index + 1}`, milestoneHTML)
        })
      }
      if (filing.contract.amendments) {
        filing.contract.amendments.forEach(function (item, index) {
          accordion.appendDate(`Amendment ${index} Date`, item.date)
        })
      }
    }
  }

  function appendAmounts(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Amounts'
    const label = 'amounts'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (filing.award && filing.award.value) {
      accordion.appendFeature(
        'Award Value',
        numberWithCommas(filing.award.value.amount) +
          ' ' +
          filing.award.value.currency,
        true,
        filing.award.value.amount
      )
    }
    if (filing.contract) {
      if (filing.contract.minValue) {
        accordion.appendFeature(
          'Contract Minimum Value',
          numberWithCommas(filing.contract.minValue.amount) +
            ' ' +
            filing.contract.minValue.currency,
          true,
          filing.contract.minValue.amount
        )
      }
      if (filing.contract.value) {
        accordion.appendFeature(
          'Contract Value',
          numberWithCommas(filing.contract.value.amount) +
            ' ' +
            filing.contract.value.currency,
          true,
          filing.contract.value.amount
        )
      }
      if (filing.contract.amendments) {
        filing.contract.amendments.forEach(function (item, index) {
          if (item.additionalAmendmentDetails) {
            item.additionalAmendmentDetails.forEach(function (detail) {
              accordion.appendFeature(detail.name, detail.value)
            })
          }
        })
      }
    }
    if (filing.tender) {
      if (filing.tender.minValue) {
        accordion.appendFeature(
          'Tender Minimum Value',
          numberWithCommas(filing.tender.minValue.amount) +
            ' ' +
            filing.tender.minValue.currency,
          true,
          filing.tender.minValue.amount
        )
      }
      if (filing.tender.value) {
        accordion.appendFeature(
          'Tender Value',
          numberWithCommas(filing.tender.value.amount) +
            ' ' +
            filing.tender.value.currency,
          true,
          filing.tender.value.currency
        )
      }
    }
  }

  function appendSuppliers(parentHTML, parentID, expanded) {
    if (!filing.award.suppliers) {
      return
    }

    const buttonHTML = 'Suppliers'
    const label = 'suppliers'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    if (filing.award.suppliers.length == 1) {
      const supplier = filing.award.suppliers[0]
      let vendorIndex = -1
      filing.annotation.vendors.forEach(function (vendor, vendorIndCand) {
        if (supplier.name.toLowerCase() == vendor.origText.toLowerCase()) {
          vendorIndex = vendorIndCand
        }
      })
      if (vendorIndex >= 0) {
        appendParty(
          collapseBody,
          supplier,
          filing.annotation.vendors[vendorIndex],
          'Supplier'
        )
      } else {
        accordion.appendFeature('Supplier', supplier.name)
      }
    } else {
      filing.award.suppliers.forEach(function (supplier, index) {
        let vendorIndex = -1
        filing.annotation.vendors.forEach(function (vendor, vendorIndCand) {
          if (supplier.name.toLowerCase() == vendor.origText.toLowerCase()) {
            vendorIndex = vendorIndCand
          }
        })
        if (vendorIndex >= 0) {
          appendParty(
            collapseBody,
            supplier,
            filing.annotation.vendors[vendorIndex],
            `Supplier ${index + 1}`
          )
        } else {
          accordion.appendFeature(`Supplier ${index + 1}`, supplier.name)
        }
      })
    }
  }

  function appendParties(parentHTML, parentID, expanded) {
    if (!filing.parties) {
      return
    }

    const buttonHTML = 'Parties'
    const label = 'parties'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    if (filing.parties.length == 1) {
      appendParty(
        collapseBody,
        filing.parties[0],
        filing.annotation.vendors[0],
        'Party'
      )
    } else {
      filing.parties.forEach(function (party, index) {
        appendParty(
          collapseBody,
          party,
          filing.annotation.vendors[index],
          `Party ${index + 1}`
        )
      })
    }
  }

  function appendDescription(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Description'
    const label = 'description'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (filing.tender) {
      accordion.appendFeature('Tender Title', filing.tender.title)
      accordion.appendFeature('Tender Description', filing.tender.description)
      if (filing.tender.items) {
        filing.tender.items.forEach(function (item, index) {
          if ('classification' in item) {
            let itemHTML = $('<div>')
            if ('scheme' in item.classification) {
              itemHTML.append(`Scheme: ${item.classification.scheme}`)
            }
            if ('id' in item.classification) {
              if (item.classification.scheme) {
                itemHTML.append('<br/>')
              }
              itemHTML.append(`ID: ${item.classification.id}`)
            }
            if ('description' in item.classification) {
              if (item.classification.scheme || item.classification.id) {
                itemHTML.append('<br/>')
              }
              itemHTML.append('Description: ')
              if ('uri' in item.classification) {
                itemHTML.append(
                  $('<a>')
                    .attr('href', item.classification.uri)
                    .text(item.classification.description)
                )
              } else {
                itemHTML.append(item.classification.description)
              }
            }
            accordion.appendFeature(`Item ${index + 1}`, itemHTML)
          }
        })
      }
    }

    if (filing.contract) {
      accordion.appendFeature('Contract Title', filing.contract.title)
      accordion.appendFeature(
        'Contract Description',
        filing.contract.description
      )

      if (filing.contract.items) {
        filing.contract.items.forEach(function (item, index) {
          if ('classification' in item) {
            let itemHTML = $('<div>')
            if ('scheme' in item.classification) {
              itemHTML.append(`Scheme: ${item.classification.scheme}`)
            }
            if ('id' in item.classification) {
              if (item.classification.scheme) {
                itemHTML.append('<br/>')
              }
              itemHTML.append(`ID: ${item.classification.id}`)
            }
            if ('description' in item.classification) {
              if (item.classification.scheme || item.classification.id) {
                itemHTML.append('<br/>')
              }
              itemHTML.append('Description: ')
              if ('uri' in item.classification) {
                itemHTML.append(
                  $('<a>')
                    .attr('href', item.classification.uri)
                    .text(item.classification.description)
                )
              } else {
                itemHTML.append(item.classification.description)
              }
            }
            accordion.appendFeature(`Item ${index + 1}`, itemHTML)
          }
        })
      }
      if (filing.contract.amendments) {
        filing.contract.amendments.forEach(function (item, index) {
          accordion.appendFeature(
            `Amendment ${index} Rationale`,
            item.rationale
          )
        })
      }
    }
  }

  function appendParty(parentHTML, party, annotation, title) {
    let partyHTML = $('<div>')
    partyHTML
      .append(annotation.logoHTML)
      .append(
        $('<a>').attr('href', annotation.url).text(annotation.stylizedText)
      )
    if (annotation.text != annotation.origText.toLowerCase()) {
      partyHTML
        .append('<br/><br/>')
        .append(
          $('<span>')
            .css('font-style', 'italic')
            .css('font-size', '90%')
            .text(`As written: "${annotation.origText}"`)
        )
    }
    if (
      party.additionalIdentifiers ||
      party.address ||
      party.contactPoint ||
      party.roles
    ) {
      partyHTML.append('<br/>')
    }
    if (party.additionalIdentifiers) {
      partyHTML.append('<br/>')
      const scheme = party.additionalIdentifiers[0].scheme
      const id = party.additionalIdentifiers[0].id
      if (scheme == 'AU-ABN') {
        partyHTML.append(
          $('<a>')
            .attr('href', `https://abr.business.gov.au/ABN/View?id=${id}`)
            .text(`${scheme}: ${id}`)
        )
      } else {
        partyHTML.append(`${scheme}: ${id}`)
      }
    }
    if (party.address) {
      partyHTML.append('<br/>')
      const addressObj = {
        streetLines: [party.address.streetAddress],
        city: party.address.locality,
        postalCode: party.address.postalCode,
        country: party.address.countryName,
      }
      partyHTML.append(formatAddress(addressObj))
    }
    if (party.contactPoint) {
      partyHTML.append('<br/>')
      let contactHTML = $('<div>')
      if (party.contactPoint.name) {
        contactHTML.append(party.contactPoint.name)
      }
      if (party.contactPoint.email) {
        contactHTML.append('<br/>Email: ')
        contactHTML.append(
          $('<a>')
            .attr('href', `mailto:${party.contactPoint.email}`)
            .text(party.contactPoint.email)
        )
      }
      if (party.contactPoint.telephone) {
        const telephoneStr = formatPhoneNumber(party.contactPoint.telephone)
        contactHTML.append(`<br/>Telephone: ${telephoneStr}`)
      }
      if (party.contactPoint.branch) {
        contactHTML.append(`<br/>Branch: ${party.contactPoint.branch}`)
      }
      if (party.contactPoint.division) {
        contactHTML.append(`<br/>Division: ${party.contactPoint.division}`)
      }
      if (party.contactPoint.uri) {
        contactHTML.append('<br/>')
        contactHTML.append(
          $('<a>')
            .attr('href', party.contactPoint.uri)
            .text(party.contactPoint.uri)
        )
      }
      partyHTML.append(contactHTML)
    }
    if (party.roles) {
      partyHTML.append(`<br/>Role: ${party.roles[0]}`)
    }
    appendModalSimpleFeature(parentHTML, title, partyHTML)
  }

  const maxTitleLength = 200

  let content = holder.find('.modal-content')
  content.empty()

  function documentURLFromID(id) {
    const idKept = id.split('-')[1]
    console.log(id)
    console.log(idKept)
    const splitID =
      `${idKept.substr(0, 8)}-${idKept.substr(8, 4)}-` +
      `${idKept.substr(12, 4)}-${idKept.substr(16, 4)}-${idKept.substr(20, 12)}`
    return `https://www.tenders.gov.au/Cn/Show/${splitID}`
  }

  function jsonURLFromID(id) {
    const idKept = id.split('-')[0]
    return `https://api.tenders.gov.au/ocds/findById/${idKept}`
  }

  let title = ''
  if (filing.contract) {
    title = trimLabel(
      `${filing.contract.title}: ${filing.contract.description}`,
      maxTitleLength
    )
  }
  if (filing.filing_id) {
    const id = filing.award
      ? filing.award.id
      : filing.contract
      ? filing.contract.awardID
      : ''
    const documentURL = documentURLFromID(id)
    const jsonURL = jsonURLFromID(id)
    title +=
      `<a href="${documentURL}">` +
      '<img src="/logos/document.jpg" ' +
      'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" />' +
      '</a>'
    title +=
      `<a href="${jsonURL}">` +
      '<img src="/logos/json-file.svg" ' +
      'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" />' +
      '</a>'
  }

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'auAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendActionDetails(accordionDiv, accordionID, false)
  appendDates(accordionDiv, accordionID, false)
  appendAmounts(accordionDiv, accordionID, false)
  appendParties(accordionDiv, accordionID, false)
  appendSuppliers(accordionDiv, accordionID, false)
  appendDescription(accordionDiv, accordionID, true)
}
