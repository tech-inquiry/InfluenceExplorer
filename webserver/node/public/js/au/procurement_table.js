function AUProcurementTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/au/procurement'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    let tableData = []
    const maxDescriptionLength = 256
    filings.forEach(function (filing, filingIndex) {
      const date = filing.date ? dateWithoutTimeLex(filing.date) : ''

      const publishedDate = filing.published_date
        ? dateWithoutTimeLex(filing.published_date)
        : ''

      let vendor = ''
      filing.annotation.vendors.forEach(function (vendorAnnotation, index) {
        if (!vendor && 'origText' in vendorAnnotation) {
          if (
            'roles' in filing.parties[index] &&
            filing.parties[index].roles[0] == 'supplier'
          ) {
            vendor = vendorAnnotation.origText
          }
        }
      })

      let buyer = ''
      filing.annotation.vendors.forEach(function (vendorAnnotation, index) {
        if (!buyer && 'origText' in vendorAnnotation) {
          if (
            'roles' in filing.parties[index] &&
            filing.parties[index].roles[0] == 'procuringEntity'
          ) {
            buyer = vendorAnnotation.origText
          }
        }
      })

      const description =
        filing.contract && filing.contract.description
          ? trimLabel(filing.contract.description, maxDescriptionLength)
          : ''

      let value = ''
      if (filing.contract) {
        if (filing.contract.minValue) {
          value = filing.contract.minValue.amount
        }
        if (filing.contract.value) {
          if (
            !value ||
            parseFloat(filing.contract.value.amount) > parseFloat(value)
          ) {
            value = filing.contract.value.amount
          }
        }
      }

      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        date,
        publishedDate,
        vendor,
        buyer,
        description,
        value,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Date' },
        { targets: 2, title: 'Publication Date' },
        { targets: 3, title: 'Vendor' },
        { targets: 4, title: 'Buyer' },
        { targets: 5, title: 'Description' },
        {
          targets: 6,
          title: 'Value',
          render: $.fn.dataTable.render.number(',', '.', 2, 'AU$'),
        },
        { targets: 7, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      AUProcurement(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
