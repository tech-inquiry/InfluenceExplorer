function CALobbyingRegistrationTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/ca/lobbying/registration'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    function buildName(obj) {
      if (!obj) {
        return ''
      }
      const nameKeys = ['name', 'firstName', 'lastName']
      let namePieces = []
      nameKeys.forEach(function (nameKey) {
        if (nameKey in obj) {
          namePieces.push(obj[nameKey])
        }
      })
      return namePieces.join(' ')
    }

    function beneficiaryNames(filing) {
      let nameList = []
      filing.beneficiaries.forEach(function (beneficiary) {
        const name = buildName(beneficiary)
        if (name) {
          nameList.push(name)
        }
      })
      return nameList.join(', ')
    }

    let tableData = []
    filings.forEach(function (filing, filingIndex) {
      const date = filing.registration.postedDate
        ? dateWithoutTimeLex(filing.registration.postedDate)
        : dateWithoutTimeLex(filing.registration.startDate)
      const clientName = buildName(filing.client)
      const beneficiaries = beneficiaryNames(filing)
      const firm = filing.firm ? buildName(filing.firm) : ''
      const registrantName = buildName(filing.registrant)
      const registrantPosition = filing.registrant.position
        ? filing.registrant.position
        : ''
      const activity = filing.activity
        ? `${filing.activity.topics} (${filing.activity.category})`
        : ''
      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        date,
        clientName,
        beneficiaries,
        firm,
        registrantName,
        registrantPosition,
        activity,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Date' },
        { targets: 2, title: 'Client Name' },
        { targets: 3, title: 'Beneficiaries' },
        { targets: 4, title: 'Firm' },
        { targets: 5, title: 'Registrant Name' },
        { targets: 6, title: 'Registrant Position' },
        { targets: 7, title: 'Activity' },
        { targets: 8, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      CALobbyingRegistration(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
