function CALobbyingCommunicationTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/ca/lobbying/communication'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    function buildName(obj) {
      if (!obj) {
        return ''
      }
      const nameKeys = ['name', 'firstName', 'lastName']
      let namePieces = []
      nameKeys.forEach(function (nameKey) {
        if (nameKey in obj) {
          namePieces.push(obj[nameKey])
        }
      })
      return namePieces.join(' ')
    }

    let tableData = []
    filings.forEach(function (filing, filingIndex) {
      const date = filing.date ? dateWithoutTimeLex(filing.date) : ''
      const clientName = filing.name ? filing.name : ''
      const registrantName = buildName(filing.registrant)
      const officeHolderName = buildName(filing.dpoh)
      const officeHolderTitle = filing.dpoh.title ? filing.dpoh.title : ''
      const officeHolderInst = filing.dpoh.institution
        ? filing.dpoh.institution
        : ''
      let activity = filing.activity ? filing.activity.category : ''
      if (filing.activity && filing.activity.category != filing.activity.text) {
        if (activity) {
          activity += ` (${filing.activity.text})`
        } else {
          activity = filing.activity.text
        }
      }
      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        date,
        clientName,
        registrantName,
        officeHolderName,
        officeHolderTitle,
        officeHolderInst,
        activity,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Date' },
        { targets: 2, title: 'Client Name' },
        { targets: 3, title: 'Registrant Name' },
        { targets: 4, title: 'Office Holder Name' },
        { targets: 5, title: 'Office Holder Title' },
        { targets: 6, title: 'Office Holder Inst.' },
        { targets: 7, title: 'Activity' },
        { targets: 8, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      CALobbyingCommunication(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
