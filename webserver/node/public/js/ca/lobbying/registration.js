function CALobbyingRegistration(holder, filing, titleID) {
  function nameToHTML(name) {
    if (!name) {
      return ''
    }

    const entity = filing.annotation.entities[name.toLowerCase()]
    let lines = []
    appendEntityAnnotationLines(lines, entity, name)
    return lines.join('<br/>')
  }

  function buildName(object) {
    if (!object) {
      return ''
    }
    const nameKeys = ['name', 'firstName', 'lastName']
    let namePieces = []
    nameKeys.forEach(function (nameKey) {
      if (nameKey in object) {
        namePieces.push(object[nameKey])
      }
    })
    return namePieces.join(' ')
  }

  function buildAddress(object) {
    let addressObj = {
      streetLines: [],
      city: object.city,
      state: object.state,
      postalCode: object.postal,
      country: object.country,
    }
    return formatAddress(addressObj)
  }

  function appendHeader(parentHTML, parentID, expanded) {
    const registration = filing.registration

    const buttonHTML = 'Registration Header'
    const label = 'registrationHeader'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Registration ID', filing.registration_id)
    accordion.appendFeature('Registration Parent ID', registration.parentID)
    accordion.appendFeature('Registration Number', registration.number)
    accordion.appendFeature('Registration Type', registration.type)
    accordion.appendFeature('Registration Version', registration.version)
  }

  function appendRegistrant(parentHTML, parentID, expanded) {
    const registrant = filing.registrant
    const firm = filing.firm
    if (!(registrant || firm)) {
      return
    }

    const buttonHTML = 'Registrant'
    const label = 'registrant'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (firm) {
      accordion.appendFeature('Consulting Firm', nameToHTML(firm.name))
      accordion.appendFeature(
        'Registrant Position at Consulting Firm',
        firm.registrantPosition
      )
      if (!registrant || registrant.address != firm.address) {
        accordion.appendFeature('Consulting Firm Address', firm.address)
      }
    }
    if (registrant) {
      accordion.appendFeature(
        'Registrant Name',
        nameToHTML(buildName(registrant))
      )
      accordion.appendFeature('Registrant Address', registrant.address)
      accordion.appendFeature('Registrant Position', registrant.position)
      accordion.appendPhone('Registrant Phone', registrant.phone)
      accordion.appendPhone('Registrant Fax', registrant.fax)
      accordion.appendFeature(
        'Registrant Payment Contingent?',
        registrant.contingentPayment
      )
      accordion.appendFeature(
        'Registrant Account Number',
        registrant.accountNumber
      )
    }
  }

  function appendClient(parentHTML, parentID, expanded) {
    const client = filing.client
    const principalRep = filing.principal_representative
    if (!(client || principalRep)) {
      return
    }

    const buttonHTML = 'Client'
    const label = 'client'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (client) {
      accordion.appendFeature('Name', nameToHTML(buildName(client)))
      accordion.appendFeature('Primary ID', client.primaryNumber)
      accordion.appendFeature('Secondary ID', client.secondaryNumber)
      accordion.appendPhone('Phone', client.phone)
      accordion.appendPhone('Fax', client.fax)
      accordion.appendFeature('Has Parents?', client.hasParents)
      accordion.appendFeature('Has Subsidiaries?', client.hasSubsidiaries)
      accordion.appendFeature('Is Coalition?', client.isCoalition)
      accordion.appendFeature('Is Controlled?', client.isControlled)
      accordion.appendFeature('Is Foreign Funded?', client.isForeignFunded)
      accordion.appendDate('Year End Date', client.yearEndDate)
    }
    if (principalRep) {
      accordion.appendFeature(
        "Principal Representative's Name",
        nameToHTML(buildName(principalRep))
      )
      accordion.appendFeature(
        "Principal Representative's Title",
        principalRep.position
      )
    }
  }

  function appendBeneficiary(parentHTML, parentID, beneficiary, index) {
    const name = buildName(beneficiary)
    const buttonHTML = name
    const label = 'beneficiary'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      false,
      label,
      index
    )

    const address = buildAddress(beneficiary)

    accordion.appendFeature('Name', nameToHTML(name))
    accordion.appendFeature('Type', beneficiary.type)
    accordion.appendFeature('Address', address)
  }

  function appendBeneficiaries(parentHTML, parentID, expanded) {
    const beneficiaries = filing.beneficiaries
    if (!beneficiaries.length) {
      return
    }

    const buttonHTML = 'Beneficiaries'
    const label = 'beneficiary'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    beneficiaries.forEach(function (beneficiary, index) {
      appendBeneficiary(
        accordion.collapseBody(),
        accordion.collapseID(),
        beneficiary,
        index
      )
    })
  }

  function appendDates(parentHTML, parentID, expanded) {
    const registration = filing.registration
    if (!registration) {
      return
    }

    const buttonHTML = 'Registration Dates'
    const label = 'dates'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendDate('Start Date', registration.startDate)
    accordion.appendDate('End Date', registration.endDate)
    accordion.appendFeature('Posted Date', registration.postedDate)
  }

  function appendActivity(parentHTML, parentID, expanded) {
    const activity = filing.activity
    if (!activity) {
      return
    }

    const buttonHTML = 'Activity'
    const label = 'activity'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Category', activity.category)
    accordion.appendFeature('Topics', activity.topics)
    accordion.appendFeature('Description', activity.description)
  }

  function appendInstitutions(parentHTML, parentID, expanded) {
    const commInst = filing.comm_institutions
    const fundInst = filing.funder_institutions
    if (!(commInst.length || fundInst.length)) {
      return
    }

    const buttonHTML = 'Government Institutions'
    const label = 'institutions'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (commInst.length) {
      let list = $('<ul>').css('padding-left', '1em')
      commInst.forEach(function (institution) {
        list.append($('<li>').text(institution))
      })
      accordion.appendFeature('Institutions Communicated With', list)
    }

    if (fundInst.length) {
      let list = $('<ul>').css('padding-left', '1em')
      fundInst.forEach(function (institution) {
        list.append(
          $('<li>').text(
            `${institution.name} (CA$${numberWithCommas(institution.amount)})`
          )
        )
      })
      accordion.appendFeature('Institutions Funded By', list)
    }
  }

  function appendCommunicationTechniques(parentHTML, parentID, expanded) {
    const commTech = filing.comm_techniques
    if (!commTech.length) {
      return
    }

    const buttonHTML = 'Communication Techniques'
    const label = 'commTechniques'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    let techniques = $('<ul>').css('padding-left', '1em')
    commTech.forEach(function (technique) {
      let desc = technique.type
      if (technique.text) {
        desc += ` (${technique.text})`
      }
      techniques.append($('<li>').text(desc))
    })
    accordion.appendFeature('Techniques', techniques)
  }

  let content = holder.find('.modal-content')
  content.empty()

  const maxTitleLength = 100
  let title = `${buildName(filing.registrant)}: ${buildName(filing.client)}`
  title = trimLabel(title, maxTitleLength)

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'caLobbyingRegistrationAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendHeader(accordionDiv, accordionID, false)
  appendDates(accordionDiv, accordionID, false)
  appendRegistrant(accordionDiv, accordionID, false)
  appendClient(accordionDiv, accordionID, false)
  appendBeneficiaries(accordionDiv, accordionID, false)
  appendInstitutions(accordionDiv, accordionID, false)
  appendCommunicationTechniques(accordionDiv, accordionID, false)
  appendActivity(accordionDiv, accordionID, false)
}
