function CALobbyingCommunication(holder, filing, titleID) {
  function nameToHTML(name) {
    if (!name) {
      return ''
    }

    const entity = filing.annotation.entities[name.toLowerCase()]
    let lines = []
    appendEntityAnnotationLines(lines, entity, name)
    return lines.join('<br/>')
  }

  function buildName(object) {
    if (!object) {
      return ''
    }
    const nameKeys = ['name', 'firstName', 'lastName']
    let namePieces = []
    nameKeys.forEach(function (nameKey) {
      if (nameKey in object) {
        namePieces.push(object[nameKey])
      }
    })
    return namePieces.join(' ')
  }

  function appendHeader(parentHTML, parentID, expanded) {
    const buttonHTML = 'Communication Header'
    const label = 'communicationHeader'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Communication ID', filing.comm_id)
    accordion.appendFeature('Communication Type', filing.type)
  }

  function appendRegistrant(parentHTML, parentID, expanded) {
    const registrant = filing.registrant
    if (!registrant) {
      return
    }
    const buttonHTML = 'Registrant'
    const label = 'registrant'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (registrant) {
      accordion.appendFeature('Name', nameToHTML(buildName(registrant)))
      accordion.appendFeature('ID', registrant.id)
    }
  }

  function appendClient(parentHTML, parentID, expanded) {
    const buttonHTML = 'Client'
    const label = 'client'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Name', nameToHTML(filing.name))
    accordion.appendFeature('ID', filing.client_id)
  }

  function appendDates(parentHTML, parentID, expanded) {
    const buttonHTML = 'Dates'
    const label = 'dates'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendDate('Date', filing.date)
    accordion.appendDate('Submission Date', filing.submission_date)
    accordion.appendFeature('Posted Date', filing.posted_date)
  }

  function appendOfficeHolder(parentHTML, parentID, expanded) {
    const officeHolder = filing.dpoh
    if (!officeHolder) {
      return
    }

    const buttonHTML = 'Designated Public Office Holder'
    const label = 'officeHolder'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Name', nameToHTML(buildName(officeHolder)))
    accordion.appendFeature('Title', officeHolder.title)
    accordion.appendFeature('Branch', officeHolder.branch)
    accordion.appendFeature('Institution', officeHolder.institution)
  }

  function appendActivity(parentHTML, parentID, expanded) {
    const activity = filing.activity
    if (!activity) {
      return
    }

    const buttonHTML = 'Activity'
    const label = 'activity'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    let desc = activity.category
    if (activity.text && activity.category != activity.text) {
      desc += ` (${activity.text})`
    }
    accordion.appendFeature('Activity Description', desc)
  }

  let content = holder.find('.modal-content')
  content.empty()

  const title = filing.name
  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'caLobbyingCommunicationAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendHeader(accordionDiv, accordionID, false)
  appendDates(accordionDiv, accordionID, false)
  appendRegistrant(accordionDiv, accordionID, false)
  appendClient(accordionDiv, accordionID, false)
  appendOfficeHolder(accordionDiv, accordionID, false)
  appendActivity(accordionDiv, accordionID, false)
}
