function CAProcurementProactive(holder, filing, titleID) {
  function appendActionDetails(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Action Details'
    const label = 'actionDetails'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Reference Number', filing.reference_number)
    accordion.appendFeature('Procurement ID', filing.procurement_id)
    accordion.appendFeature(
      'Standing Offer Number',
      filing.standing_offer_number
    )

    accordion.appendFeature('Number of Bids', filing.number_of_bids)
    accordion.appendFeature('Trade Agreement', filing.trade_agreement)

    if (filing.instrument_type) {
      const instrumentTypeMap = {
        A: 'Amendment',
        C: 'Contract',
        SOSA: 'Standing Offer or Supply Arrangement',
      }

      let typeString = undefined
      let instrumentTypeTokens = filing.instrument_type.split(':')
      const primaryToken = instrumentTypeTokens[0].trim()
      if (primaryToken in instrumentTypeMap) {
        typeString = instrumentTypeMap[primaryToken]
      } else {
        typeString = filing.instrument_type
      }
      accordion.appendFeature('Instrument Type', typeString)
    }

    if (
      filing.annotation.tradeAgreementExceptions &&
      filing.annotation.tradeAgreementExceptions != 'None'
    ) {
      accordion.appendFeature(
        'Trade Agreement Exceptions',
        filing.annotation.tradeAgreementExceptions
      )
    }
  }

  function appendValues(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Values'
    const label = 'values'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature(
      'Contract Value',
      numberWithCommas(filing.contract_value) + ' CAD',
      true,
      filing.contract_value
    )
    accordion.appendFeature(
      'Original Value',
      numberWithCommas(filing.original_value) + ' CAD',
      true,
      filing.original_value
    )
    accordion.appendFeature(
      'Amendment Value',
      numberWithCommas(filing.amendment_value) + ' CAD',
      true,
      filing.amendment_value
    )
  }

  function appendDates(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Dates'
    const label = 'dates'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendDate('Contract Date', filing.contract_date)
    accordion.appendDate('Contract Start', filing.contract_period_start)
    accordion.appendDate('Delivery Date', filing.delivery_date)
    accordion.appendFeature('Reporting Period', filing.reporting_period)
  }

  function appendVendor(parentHTML, parentID, expanded) {
    const buttonHTML = 'Vendor'
    const label = 'vendor'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    let vendorNameValue = $('<div>')
    vendorNameValue
      .append(filing.annotation.vendor.logoHTML)
      .append(
        $('<a>')
          .attr('href', filing.annotation.vendor.url)
          .text(filing.annotation.vendor.stylizedText)
      )
    if (
      filing.annotation.vendor.text !=
      filing.annotation.vendor.origText.toLowerCase()
    ) {
      vendorNameValue
        .append('<br/><br/>')
        .append(
          $('<span>')
            .css('font-style', 'italic')
            .css('font-size', '90%')
            .text(`As written: "${filing.vendor_name}"`)
        )
    }
    accordion.appendFeature('Vendor', vendorNameValue)

    accordion.appendFeature('Vendor Postal Code', filing.vendor_postal_code)
    accordion.appendFeature('Vendor Country', filing.country_of_vendor)
    if (filing.owner_org_title) {
      accordion.appendFeature('Owner Title', filing.owner_org_title)
    } else {
      accordion.appendFeature('Owner Org', filing.owner_org)
    }
  }

  function appendBuyer(parentHTML, parentID, expanded) {
    if (!filing.buyer_name) {
      return
    }

    const buttonHTML = 'Buyer'
    const label = 'buyer'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Buyer', filing.buyer_name)
  }

  function appendDescription(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Description'
    const label = 'description'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Description', filing.description)
    accordion.appendFeature('Comments', filing.comments)
    accordion.appendFeature('Additional Comments', filing.additional_comments)
  }

  let content = holder.find('.modal-content')
  content.empty()

  const maxTitleLength = 100

  let title = ''
  if (filing.vendor_name) {
    title = `${filing.vendor_name}: ${filing.description}`
  } else {
    title = filing.description
  }
  title = trimLabel(title, maxTitleLength)

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'caProcurementProactiveAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendActionDetails(accordionDiv, accordionID, false)
  appendDates(accordionDiv, accordionID, false)
  appendValues(accordionDiv, accordionID, false)
  appendVendor(accordionDiv, accordionID, false)
  appendBuyer(accordionDiv, accordionID, false)
  appendDescription(accordionDiv, accordionID, true)
}
