function CAProcurementPWSGCTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/ca/procurement/pwsgc'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    let tableData = []
    filings.forEach(function (filing, filingIndex) {
      const awardDate = filing.award_date
        ? dateWithoutTimeLex(filing.award_date)
        : ''
      const vendor = filing.supplier_legal_name
        ? filing.supplier_legal_name
        : ''
      const totalContractValue =
        filing.total_contract_value !== undefined
          ? filing.total_contract_value
          : ''
      const endUser =
        filing.end_user_entity !== undefined ? filing.end_user_entity : ''
      const description = filing.gsin_description ? filing.gsin_description : ''
      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        awardDate,
        vendor,
        totalContractValue,
        endUser,
        description,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Award Date' },
        { targets: 2, title: 'Vendor' },
        {
          targets: 3,
          title: 'Total Contract Value',
          render: $.fn.dataTable.render.number(',', '.', 2, 'CA$'),
        },
        { targets: 4, title: 'End User' },
        { targets: 5, title: 'Description' },
        { targets: 6, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      CAProcurementPWSGC(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
