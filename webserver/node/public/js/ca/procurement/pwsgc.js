function CAProcurementPWSGC(holder, filing, titleID) {
  function appendActionDetails(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Action Details'
    const label = 'actionDetails'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Contract Number', filing.contract_number)
    accordion.appendFeature('Amendment Number', filing.amendment_number)

    accordion.appendFeature('Competitive Tender', filing.competitive_tender)
    accordion.appendFeature(
      'Limited Tender Reason',
      filing.limited_tender_reason_description
    )
    accordion.appendFeature(
      'Solicitation Procedure',
      filing.solicitation_procedure_description
    )
    accordion.appendFeature(
      'Trade Agreement',
      filing.trade_agreement_description
    )
  }

  function appendDates(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Dates'
    const label = 'dates'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendDate('Award Date', filing.award_date)
    accordion.appendDate('Expiry Date', filing.expiry_date)
    accordion.appendDate('Date Published', filing.date_file_published)
  }

  function appendValues(parentHTML, parentID, expanded) {
    if (!(filing.contract_value || filing.total_contract_value)) {
      return
    }

    const buttonHTML = 'Contract Values'
    const label = 'values'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature(
      'Contract Value',
      numberWithCommas(filing.contract_value) + ' CAD',
      true,
      filing.contract_value
    )
    accordion.appendFeature(
      'Total Contract Value',
      numberWithCommas(filing.total_contract_value) + ' CAD',
      true,
      filing.total_contract_value
    )
  }

  function appendVendor(parentHTML, parentID, expanded) {
    const buttonHTML = 'Vendor'
    const label = 'vendor'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (filing.supplier_legal_name) {
      let vendorNameValue = $('<div>')
      vendorNameValue
        .append(filing.annotation.vendor.logoHTML)
        .append(
          $('<a>')
            .attr('href', filing.annotation.vendor.url)
            .text(filing.annotation.vendor.stylizedText)
        )
      if (
        filing.annotation.vendor.text !=
        filing.annotation.vendor.origText.toLowerCase()
      ) {
        vendorNameValue
          .append('<br/><br/>')
          .append(
            $('<span>')
              .css('font-style', 'italic')
              .css('font-size', '90%')
              .text(`As written: "${filing.supplier_legal_name}"`)
          )
      }
      accordion.appendFeature('Vendor Name', vendorNameValue)
    }

    if (
      filing.supplier_standardized_name &&
      filing.supplier_standardized_name != filing.supplier_legal_name
    ) {
      accordion.appendFeature(
        'Vendor Standardized Name',
        filing.supplier_standardized_name
      )
    }

    if (
      filing.supplier_operating_name &&
      filing.supplier_operating_name != filing.supplier_legal_name &&
      filing.supplier_operating_name != filing.supplier_standardized_name
    ) {
      accordion.appendFeature(
        'Vendor Operating Name',
        filing.supplier_operating_name
      )
    }

    const addressObj = {
      city: filing.supplier_address_city,
      state: filing.supplier_address_prov_state,
      postalCode: filing.supplier_address_postal_code,
      country: filing.supplier_address_country,
    }
    accordion.appendFeature('Vendor Address', formatAddress(addressObj))

    if (
      filing.organization_employee_count &&
      filing.organization_employee_count != 'UNKNOWN'
    ) {
      accordion.appendFeature(
        'Vendor Employee Count',
        filing.organization_employee_count
      )
    }
  }

  function appendBuyer(parentHTML, parentID, expanded) {
    const buttonHTML = 'Government Buyer'
    const label = 'buyer'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('End User', filing.end_user_identity)
    accordion.appendFeature(
      'Contracting Office',
      filing.contracting_entity_office_name
    )

    let addressObj = {
      streetLines: [],
      city: filing.contracting_address_city,
      state: filing.contracting_address_prov_state,
      postalCode: filing.contracting_address_postal_code,
      country: filing.contracting_address_country,
    }
    if (
      filing.contracting_address_street_1 ||
      filing.contracting_address_street_2
    ) {
      if (filing.contracting_address_street_1) {
        addressObj.streetLines.push(filing.contracting_address_street_1)
      }
      if (filing.contracting_address_street_2) {
        addressObj.streetLines.push(filing.contracting_address_street_2)
      }
    }
    accordion.appendFeature('Contracting Address', formatAddress(addressObj))
  }

  function appendDescription(parentHTML, parentID, expanded) {
    if (!filing.gsin_description) {
      return
    }

    const buttonHTML = 'Contract Description'
    const label = 'description'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Contract Description', filing.gsin_description)
  }

  let content = holder.find('.modal-content')
  content.empty()

  const maxTitleLength = 100

  let title = ''
  if (filing.supplier_legal_name) {
    title = filing.supplier_legal_name + ': ' + filing.gsin_description
  } else {
    title = filing.gsin_description
  }
  title = trimLabel(title, maxTitleLength)

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'CAProcurementPWSGCAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendActionDetails(accordionDiv, accordionID, false)
  appendDates(accordionDiv, accordionID, false)
  appendValues(accordionDiv, accordionID, false)
  appendVendor(accordionDiv, accordionID, false)
  appendBuyer(accordionDiv, accordionID, false)
  appendDescription(accordionDiv, accordionID, true)
}
