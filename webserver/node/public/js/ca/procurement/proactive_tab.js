function CAProcurementProactiveTab(
  tabsState,
  data,
  alternateNames = undefined
) {
  const label = 'ca-proactive-disclosure'
  const tabLogo = '/logos/government_of_canada.svg'
  const tabTitle = 'Canadian Proactively Disclosed Contracts'
  const queryURL =
    appendURLPath(EXPLORER_ROUTE, 'api/ca/procurement/proactive?') +
    (data['vendor']
      ? `vendor=${encodeURIComponentIncludingSlashes(data.vendor)}`
      : `text=${encodeURIComponentIncludingSlashes(data.text)}`)
  const recordsNote = $('<div>')
    .append(
      $('<p>')
        .text('All links and images are annotations added to the original ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr(
              'href',
              'https://open.canada.ca/data/en/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b'
            )
            .text('proactive disclosure data')
        )
        .append(
          ' by Tech Inquiry. Any normalized names are clearly labeled with the ' +
            "original ('as written') text."
        )
    )
    .append(
      $('<p>')
        .append('You can alternatively download the ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', queryURL)
            .text('JSON data')
        )
        .append(' for this tab.')
    )

  FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote,
    alternateNames
  )

  const tabsID = `${label}-tabs`

  function renderRecordsTab(event, ui) {
    const containerID = `${label}-tab-records-inner`
    CAProcurementProactiveTable(data, containerID)
  }

  function renderTab(event, ui) {
    renderRecordsTab(event, ui)
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
