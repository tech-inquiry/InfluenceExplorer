function CAProcurementProactiveTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/ca/procurement/proactive'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    let tableData = []
    filings.forEach(function (filing, filingIndex) {
      const contractDate = filing.contract_date
        ? dateWithoutTimeLex(filing.contract_date)
        : ''
      const vendor = filing.vendor_name ? filing.vendor_name : ''
      const ownerTitle = filing.owner_org_title ? filing.owner_org_title : ''
      const contractValue =
        filing.contract_value !== undefined ? filing.contract_value : ''

      let descriptionLines = []
      if (filing.description) {
        descriptionLines.push(filing.description)
      }
      if (filing.comments) {
        descriptionLines.push(filing.comments)
      }
      if (filing.additional_comments) {
        descriptionLines.push(filing.additional_comments)
      }
      const description = descriptionLines.join('; ')

      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        contractDate,
        vendor,
        ownerTitle,
        contractValue,
        description,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Contract Date' },
        { targets: 2, title: 'Vendor' },
        { targets: 3, title: 'Owner Title' },
        {
          targets: 4,
          title: 'Contract Value',
          render: $.fn.dataTable.render.number(',', '.', 2, 'CA$'),
        },
        { targets: 5, title: 'Description' },
        { targets: 6, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      CAProcurementProactive(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
