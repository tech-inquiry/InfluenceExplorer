function CAProcurementPWSGCTab(tabsState, data, alternateNames = undefined) {
  const label = 'ca-pwsgc-contract'
  const tabLogo = '/logos/government_of_canada.svg'
  const tabTitle = 'Canadian PWSGC Contracts'
  const queryURL =
    appendURLPath(EXPLORER_ROUTE, 'api/ca/procurement/pwsgc?') +
    (data['vendor']
      ? `vendor=${encodeURIComponentIncludingSlashes(data.vendor)}`
      : `text=${encodeURIComponentIncludingSlashes(data.text)}`)
  const recordsNote = $('<div>')
    .append(
      $('<p>')
        .text('All links and images are annotations added to the original ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr(
              'href',
              'https://open.canada.ca/data/en/dataset/53753f06-8b28-42d7-89f7-04cd014323b0#wb-auto-6'
            )
            .text('PWSGC contracts')
        )
        .append(
          ' by Tech Inquiry. Any normalized names are clearly labeled with the ' +
            "original ('as written') text."
        )
    )
    .append(
      $('<p>')
        .append('You can alternatively download the ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', queryURL)
            .text('JSON data')
        )
        .append(' for this tab.')
    )

  FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote,
    alternateNames
  )

  const tabsID = `${label}-tabs`

  function renderRecordsTab(event, ui) {
    const containerID = `${label}-tab-records-inner`
    CAProcurementPWSGCTable(data, containerID)
  }

  function renderTab(event, ui) {
    renderRecordsTab(event, ui)
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
