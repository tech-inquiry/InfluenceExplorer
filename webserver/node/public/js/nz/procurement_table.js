function NZProcurementTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/nz/procurement'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    let tableData = []
    const maxDescriptionLength = 256
    filings.forEach(function (filing, filingIndex) {
      const awardedDate = filing.awarded_date
        ? dateWithoutTimeLex(filing.awarded_date)
        : ''

      const title = filing.title ? filing.title : ''
      const noticeID = filing.notice_id ? filing.notice_id : ''
      const postingAgency = filing.posting_agency ? filing.posting_agency : ''

      const overview = filing.overview
        ? trimLabel(filing.overview, maxDescriptionLength)
        : ''
      const comments = filing.comments
        ? trimLabel(filing.comments, maxDescriptionLength)
        : ''

      const awardedAmount = filing.awarded_amount ? filing.awarded_amount : ''

      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        awardedDate,
        title,
        noticeID,
        postingAgency,
        overview,
        comments,
        awardedAmount,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      pageLength: 25,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Awarded Date' },
        { targets: 2, title: 'Title' },
        { targets: 3, title: 'ID' },
        { targets: 4, title: 'Posting Agency' },
        { targets: 5, title: 'Overview' },
        { targets: 6, title: 'Comments' },
        {
          targets: 7,
          title: 'Awarded Amount',
          render: $.fn.dataTable.render.number(',', '.', 2, 'NZ$'),
        },
        { targets: 8, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      NZProcurement(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
