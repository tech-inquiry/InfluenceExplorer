function NZProcurement(holder, filing, titleID) {
  function appendActionDetails(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Action Details'
    const label = 'actionDetails'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Notice ID', filing.notice_id)
    accordion.appendFeature('Reference Number', filing.reference_number)
    accordion.appendFeature('Notice Type', filing.notice_type)
    accordion.appendFeature('Competition Type', filing.competition_type)
    accordion.appendFeature('Award Type', filing.award_type)
    accordion.appendFeature('Tender Coverage', filing.tender_coverage)
    accordion.appendFeature(
      'Prequalification Required?',
      filing.prequalification
    )
  }

  function appendDates(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Dates'
    const label = 'dates'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendDate('Awarded Date', filing.awarded_date)
    accordion.appendDate('Open Date', filing.open_date)
    accordion.appendDate('Close Date', filing.close_date)
  }

  function appendValues(parentHTML, parentID, expanded) {
    if (!filing.awarded_amount) {
      return
    }

    const buttonHTML = 'Contract Values'
    const label = 'values'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature(
      'Awarded Amount',
      numberWithCommas(filing.awarded_amount) + ' NZD',
      true,
      filing.awarded_amount
    )
  }

  function appendVendors(parentHTML, parentID, expanded) {
    if (!filing.annotation.vendors) {
      return
    }

    const buttonHTML = 'Vendors'
    const label = 'vendor'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (filing.annotation.vendors.length == 1) {
      let annotation = filing.annotation.vendors[0]
      let vendorHTML = $('<div>')
      vendorHTML
        .append(annotation.logoHTML)
        .append(
          $('<a>').attr('href', annotation.url).text(annotation.stylizedText)
        )
      accordion.appendFeature('Vendor', vendorHTML)
    } else if (filing.annotation.vendors.length > 1) {
      filing.annotation.vendors.forEach(function (annotation, index) {
        let vendorHTML = $('<div>')
        vendorHTML
          .append(annotation.logoHTML)
          .append(
            $('<a>').attr('href', annotation.url).text(annotation.stylizedText)
          )
        accordion.appendFeature('Vendor ' + (index + 1), vendorHTML)
      })
    }
    if (filing.alternate_address) {
      accordion.appendFeature('Alternate Address', filing.alternate_address)
    }
  }

  function appendPostingAgency(parentHTML, parentID, expanded) {
    if (!(filing.posting_agency || filing.department)) {
      return
    }

    const buttonHTML = 'Posting Agency'
    const label = 'agency'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Posting Agency', filing.posting_agency)
    accordion.appendFeature('Department', filing.department)
  }

  function appendDescription(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Description'
    const label = 'description'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Contract Title', filing.title)
    accordion.appendFeature('Contract Overview', filing.overview)
    accordion.appendFeature('Comments', filing.comments)
  }

  let content = holder.find('.modal-content')
  content.empty()

  const maxTitleLength = 200

  let title = ''
  if (filing.title) {
    title = trimLabel(filing.title, maxTitleLength)
  }

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'nzAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendActionDetails(accordionDiv, accordionID, false)
  appendDates(accordionDiv, accordionID, false)
  appendValues(accordionDiv, accordionID, false)
  appendVendors(accordionDiv, accordionID, false)
  appendPostingAgency(accordionDiv, accordionID, false)
  appendDescription(accordionDiv, accordionID, true)
}
