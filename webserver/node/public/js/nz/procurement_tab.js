function NZProcurementTab(tabsState, dataBase, alternateNames = undefined) {
  const label = 'nz'
  const tabLogo = '/logos/government_of_new_zealand.svg'
  const tabTitle = 'New Zealand Government Contracts'
  const queryURL =
    appendURLPath(EXPLORER_ROUTE, 'api/nz/procurement?') +
    (dataBase['vendor']
      ? `vendor=${encodeURIComponentIncludingSlashes(dataBase.vendor)}`
      : `text=${encodeURIComponentIncludingSlashes(dataBase.text)}`)
  const recordsNote = $('<div>')
    .append(
      $('<p>')
        .text('All links and images are annotations added to the original ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr(
              'href',
              'https://www.mbie.govt.nz/cross-government-functions/new-zealand-government-procurement-and-property/open-data/'
            )
            .text('government contracts list')
        )
        .append(
          ' by Tech Inquiry. New Zealand procurement records do not have ' +
            'a dedicated field for the awarded vendor -- at best, there is a ' +
            'mention in the comments. When no vendor is clearly referenced, ' +
            'we deviate from our usual practice by annotating the vendors ' +
            'whose products are being procured (even if indirectly).'
        )
    )
    .append(
      $('<p>')
        .append('You can alternatively download the ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', queryURL)
            .text('JSON data')
        )
        .append(' for this tab.')
    )

  const alternateNamesTitle = 'Included Notice IDs'
  FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote,
    alternateNames,
    alternateNamesTitle
  )

  const tabsID = `${label}-tabs`

  function renderRecordsTab(event, ui) {
    const data = dataBase
    const containerID = `${label}-tab-records-inner`
    NZProcurementTable(data, containerID)
  }

  function renderTab(event, ui) {
    const recordsTab = 0
    const activeTab = $(`#${tabsID}`).tabs('option', 'active')
    if (activeTab == recordsTab) {
      renderRecordsTab(event, ui)
    }
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
