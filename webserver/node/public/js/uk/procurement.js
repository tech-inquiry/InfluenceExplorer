function getUKProcurementMaxValue(filing) {
  let value = undefined
  if (filing.award && filing.award.value) {
    value = parseFloat(filing.award.value.amount)
  }
  if (filing.tender && filing.tender.minValue) {
    if (!value || filing.tender.minValue.amount > value) {
      value = parseFloat(filing.tender.minValue.amount)
    }
  }
  if (filing.tender && filing.tender.value) {
    if (!value || filing.tender.value.amount > value) {
      value = parseFloat(filing.tender.value.amount)
    }
  }
  return value
}

function UKProcurement(holder, filing, titleID) {
  function documentURLFromUUID(uuid) {
    return `https://www.contractsfinder.service.gov.uk/notice/${uuid}`
  }

  function uuidFromJSONURL(url) {
    const urlChunks = url.split('/')
    const filename = urlChunks[urlChunks.length - 1]

    // Remove the '.json' from the end
    return filename.substr(0, filename.length - 5)
  }

  function documentURLFromJSONURL(jsonURL) {
    const uuid = uuidFromJSONURL(jsonURL)
    return documentURLFromUUID(uuid)
  }

  function appendSupplier(parentHTML, supplier, annotation, title = '') {
    let supplierHTML = $('<div>')
    supplierHTML
      .append(annotation.logoHTML)
      .append(
        $('<a>').attr('href', annotation.url).text(annotation.stylizedText)
      )
    if (annotation.text != annotation.origText.toLowerCase()) {
      supplierHTML
        .append('<br/><br/>')
        .append(
          $('<span>')
            .css('font-style', 'italic')
            .css('font-size', '90%')
            .text(`As written: "${annotation.origText}"`)
        )
    }
    if (supplier.identifier) {
      supplierHTML.append('<br/>')
      const idLabel = `${supplier.identifier.scheme} ID: ${supplier.identifier.id}`
      if (annotation.companyHouseURL) {
        supplierHTML.append(
          $('<a>').attr('href', annotation.companyHouseURL).text(idLabel)
        )
      } else if (supplier.identifier.scheme) {
        supplierHTML.append(idLabel)
      }
    }
    if (supplier.address) {
      supplierHTML.append('<br/>')
      supplierHTML.append(supplier.address.streetAddress)
    }
    if (supplier.contactPoint) {
      supplierHTML.append('<br/>')
      supplierHTML.append(`Contact: ${supplier.contactPoint.name}`)
    }

    if (title) {
      appendModalSimpleFeature(parentHTML, title, supplierHTML)
    } else {
      parentHTML.append(supplierHTML)
    }
  }

  function appendActionDetails(parentHTML, parentID, expanded) {
    if (!(filing.tender || filing.award)) {
      return
    }

    const buttonHTML = 'Contract Action Details'
    const label = 'actionDetails'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (filing.award) {
      accordion.appendFeature('Award ID', filing.award.id)
      accordion.appendFeature('Award Status', filing.award.status)
    }
    if (filing.tender) {
      accordion.appendFeature('Tender ID', filing.tender.id)
      accordion.appendFeature('Tender Status', filing.tender.status)
      accordion.appendFeature(
        'Tender Procurement Method',
        filing.tender.procurementMethod
      )
      accordion.appendFeature(
        'Tender Procurement Method Details',
        filing.tender.procurementMethodDetails
      )
    }
  }

  function appendDates(parentHTML, parentID, expanded) {
    const buttonHTML = 'Contract Dates'
    const label = 'dates'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendDate('Publication Date', filing.published_date)
    accordion.appendDate('Deadline Date', filing.deadline_date)

    if (filing.award) {
      accordion.appendDate('Award Date', filing.award.date)

      if (filing.award.contractPeriod) {
        let period = ''
        if (filing.award.contractPeriod.startDate) {
          period = dateWithoutTime(
            new Date(filing.award.contractPeriod.startDate)
          )
        }
        period += ' -- '
        if (filing.award.contractPeriod.endDate) {
          period += dateWithoutTime(
            new Date(filing.award.contractPeriod.endDate)
          )
        }
        accordion.appendFeature('Award Period', period)
      }
    }

    if (filing.tender) {
      if (filing.tender.tenderPeriod) {
        let period = ''
        if (filing.tender.tenderPeriod.startDate) {
          period = dateWithoutTime(
            new Date(filing.tender.tenderPeriod.startDate)
          )
        }
        period += ' -- '
        if (filing.tender.tenderPeriod.endDate) {
          period += dateWithoutTime(
            new Date(filing.tender.tenderPeriod.endDate)
          )
        }
        accordion.appendFeature('Tender Period', period)
      }

      if (filing.tender.milestones) {
        filing.tender.milestones.forEach(function (item, index) {
          let milestoneHTML = $('<div>')
          if (item.description) {
            milestoneHTML.append(item.description)
          }
          if (item.dueDate) {
            if (item.description) {
              milestoneHTML.append('<br/>')
            }
            milestoneHTML.append(dateWithoutTime(new Date(item.dueDate)))
          }
          accordion.appendFeature('Milestone ' + (index + 1), milestoneHTML)
        })
      }
    }
  }

  function appendSuppliers(parentHTML, parentID, expanded) {
    if (
      !(filing.award && filing.award.suppliers && filing.award.suppliers.length)
    ) {
      return
    }

    const maxTitleLength = 200
    let title = filing.award.suppliers.length == 1 ? 'Vendor: ' : 'Vendors: '
    filing.award.suppliers.forEach(function (supplier, index) {
      if (index) {
        title += ', '
      }
      title += supplier.name
    })
    title = trimLabel(title, maxTitleLength)

    const buttonHTML = title
    const label = 'vendors'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )
    let collapseBody = accordion.collapseBody()

    if (filing.award.suppliers.length == 1) {
      appendSupplier(
        collapseBody,
        filing.award.suppliers[0],
        filing.annotation.vendors[0],
        'Vendor'
      )
    } else {
      filing.award.suppliers.forEach(function (supplier, index) {
        appendSupplier(
          collapseBody,
          supplier,
          filing.annotation.vendors[index],
          'Vendor ' + (index + 1)
        )
      })
    }
  }

  function appendAmounts(parentHTML, parentID, expanded) {
    if (
      !(filing.tender.minValue || filing.tender.value || filing.award.value)
    ) {
      return
    }

    const buttonHTML = 'Contract Amounts'
    const label = 'amounts'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (filing.tender) {
      if (filing.tender.minValue) {
        accordion.appendFeature(
          'Tender Minimum Value',
          numberWithCommas(filing.tender.minValue.amount) +
            ' ' +
            filing.tender.minValue.currency,
          true,
          filing.tender.minValue.amount
        )
      }
      if (filing.tender.value) {
        accordion.appendFeature(
          'Tender Value',
          numberWithCommas(filing.tender.value.amount) +
            ' ' +
            filing.tender.value.currency,
          true,
          filing.tender.value.amount
        )
      }
    }

    if (filing.award && filing.award.value) {
      accordion.appendFeature(
        'Award Value',
        numberWithCommas(filing.award.value.amount) +
          ' ' +
          filing.award.value.currency,
        true,
        filing.award.value.amount
      )
    }
  }

  function appendBuyer(parentHTML, parentID, expanded) {
    if (!filing.buyer) {
      return
    }

    const maxTitleLength = 200

    let buttonHTML = 'Government Buyer'
    if (filing.buyer.name) {
      buttonHTML += `: ${filing.buyer.name}`
    }
    buttonHTML = trimLabel(buttonHTML, maxTitleLength)
    const label = 'buyer'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Name', filing.buyer.name)

    const addressObj = {
      streetLines: [filing.buyer.address.streetAddress],
      city: filing.buyer.address.locality,
      postalCode: filing.buyer.address.postalCode,
      country: filing.buyer.address.countryName,
    }
    accordion.appendFeature('Address', formatAddress(addressObj))

    if (filing.buyer.contactPoint) {
      let contactHTML = $('<div>')
      if (filing.buyer.contactPoint.name) {
        contactHTML.append(filing.buyer.contactPoint.name)
      }
      if (filing.buyer.contactPoint.email) {
        if (filing.buyer.contactPoint.name) {
          contactHTML.append('<br/>')
        }
        contactHTML.append(
          $('<a>')
            .attr('href', `mailto:${filing.buyer.contactPoint.email}`)
            .text(filing.buyer.contactPoint.email)
        )
      }
      if (filing.buyer.contactPoint.telephone) {
        if (filing.buyer.contactPoint.name || filing.buyer.contactPoint.email) {
          contactHTML.append('<br/>')
        }
        contactHTML.append(
          formatPhoneNumber(filing.buyer.contactPoint.telephone)
        )
      }
      if (filing.buyer.contactPoint.uri) {
        if (
          filing.buyer.contactPoint.name ||
          filing.buyer.contactPoint.email ||
          filing.buyer.contactPoint.telephone
        ) {
          contactHTML.append('<br/>')
        }
        contactHTML.append(
          $('<a>')
            .attr('href', filing.buyer.contactPoint.uri)
            .text(filing.buyer.contactPoint.uri)
        )
      }
      accordion.appendFeature('Contact', contactHTML)
    }
  }

  function appendDescription(parentHTML, parentID, expanded) {
    if (!(filing.tender || filing.award)) {
      return
    }

    const maxLabelSize = 50
    let buttonHTML = 'Description'
    if (filing.comment) {
      buttonHTML += `: ${trimLabel(filing.comment, maxLabelLength)}`
    }

    const label = 'comment'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Tender Title', filing.tender.title)
    accordion.appendFeature('Tender Description', filing.tender.description)

    if (filing.tender.items) {
      filing.tender.items.forEach(function (item, index) {
        if ('classification' in item) {
          let itemHTML = $('<div>')
          if ('scheme' in item.classification) {
            itemHTML.append('Scheme: ' + item.classification.scheme)
          }
          if ('id' in item.classification) {
            if (item.classification.scheme) {
              itemHTML.append('<br/>')
            }
            itemHTML.append(`ID: ${item.classification.id}`)
          }
          if ('description' in item.classification) {
            if (item.classification.scheme || item.classification.id) {
              itemHTML.append('<br/>')
            }
            itemHTML.append('Description: ')
            if ('uri' in item.classification) {
              itemHTML.append(
                $('<a>')
                  .attr('href', item.classification.uri)
                  .text(item.classification.description)
              )
            } else {
              itemHTML.append(item.classification.description)
            }
          }
          accordion.appendFeature(`Item ${index + 1}`, itemHTML)
        }
      })
    }
  }

  const maxTitleLength = 200
  const minTitleLengthBeforeIconNewline = 50

  let content = holder.find('.modal-content')
  content.empty()

  let title = ''
  if (filing.tender) {
    title = `<span style="font-weight: 700;">${filing.tender.title}</span>`
    if (filing.tender.description) {
      title += ': ' + trimLabel(filing.tender.description, maxTitleLength)
    }
  }
  if (filing.uri) {
    const jsonURL = filing.uri
    const documentURL = documentURLFromJSONURL(jsonURL)

    if (title.length > minTitleLengthBeforeIconNewline) {
      title += '<br/>'
    }
    title +=
      `<a href="${documentURL}">` +
      '<img src="/logos/document.jpg" ' +
      'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" />' +
      '</a>'
    title +=
      `<a href="${jsonURL}">` +
      '<img src="/logos/json-file.svg" ' +
      'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" />' +
      '</a>'
  }

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  const accordionID = 'ukAccordion'
  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendActionDetails(accordionDiv, accordionID, false)
  appendAmounts(accordionDiv, accordionID, false)
  appendDates(accordionDiv, accordionID, false)
  appendSuppliers(accordionDiv, accordionID, false)
  appendBuyer(accordionDiv, accordionID, false)
  appendDescription(accordionDiv, accordionID, true)
}
