function UKProcurementTab(tabsState, dataBase, alternateNames = undefined) {
  const label = 'uk'
  const tabLogo = '/logos/government_of_the_united_kingdom.svg'
  const tabTitle = 'UK Government Contracts'
  const queryURL =
    appendURLPath(EXPLORER_ROUTE, 'api/uk/procurement?') +
    (dataBase['vendor']
      ? `vendor=${encodeURIComponentIncludingSlashes(dataBase.vendor)}`
      : `text=${encodeURIComponentIncludingSlashes(dataBase.text)}`)
  const recordsNote = $('<div>')
    .append(
      $('<p>')
        .text('All links and images are annotations added to the original ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', 'https://www.gov.uk/contracts-finder')
            .text('government contracts list')
        )
        .append(
          ' by Tech Inquiry. Any normalized names are clearly labeled with the ' +
            "original ('as written') text."
        )
    )
    .append(
      $('<p>')
        .append('You can alternatively download the ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', queryURL)
            .text('JSON data')
        )
        .append(' for this tab.')
    )

  FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote,
    alternateNames
  )

  const tabsID = `${label}-tabs`

  function renderRecordsTab(event, ui) {
    const data = dataBase
    const containerID = `${label}-tab-records-inner`
    UKProcurementTable(data, containerID)
  }

  function renderTab(event, ui) {
    const recordsTab = 0
    const activeTab = $(`#${tabsID}`).tabs('option', 'active')
    if (activeTab == recordsTab) {
      renderRecordsTab(event, ui)
    }
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
