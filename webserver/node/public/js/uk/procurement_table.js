function UKProcurementTable(
  data,
  containerID,
  tableID = undefined,
  modalID = undefined
) {
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/uk/procurement'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    let tableData = []
    const maxDescriptionLength = 256
    filings.forEach(function (filing, filingIndex) {
      const publishedDate = filing.published_date
        ? dateWithoutTimeLex(filing.published_date)
        : ''

      let vendor = ''
      filing.annotation.vendors.forEach(function (vendorAnnotation) {
        if ('origText' in vendorAnnotation) {
          if (vendor) {
            vendor += ', '
          }
          vendor += vendorAnnotation.origText
        }
      })
      vendor = trimLabel(vendor, maxDescriptionLength)

      const buyer = filing.buyer && filing.buyer.name ? filing.buyer.name : ''

      const description =
        filing.tender && filing.tender.description
          ? trimLabel(filing.tender.description, maxDescriptionLength)
          : ''

      let maxValue = getUKProcurementMaxValue(filing)
      if (!maxValue) {
        maxValue = 0
      }

      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        publishedDate,
        vendor,
        buyer,
        description,
        maxValue,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Publication Date' },
        { targets: 2, title: 'Vendor' },
        { targets: 3, title: 'Buyer' },
        { targets: 4, title: 'Description' },
        {
          targets: 5,
          title: 'Max Value',
          render: $.fn.dataTable.render.number(',', '.', 2, '£'),
        },
        { targets: 6, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      UKProcurement(holder, filing, titleID)
      holder.modal('show')
    })
  })
}
