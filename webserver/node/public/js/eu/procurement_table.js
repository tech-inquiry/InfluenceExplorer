function EUProcurementTable(
  api_endpoint,
  data,
  containerID,
  accordionID = undefined,
  tableID = undefined,
  modalID = undefined
) {
  if (!accordionID) {
    accordionID = `${containerID}-accordion`
  }
  if (!tableID) {
    tableID = `${containerID}-table`
  }
  if (!modalID) {
    modalID = `${containerID}-modal`
    createModalDiv(modalID)
  }

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, api_endpoint),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    fillWithResponsiveTable(containerID, tableID)

    let tableData = []
    const maxDescriptionLength = 256
    filings.forEach(function (filing, filingIndex) {
      const actionDate = getEUProcurementActionDate(filing)
      const actionDateStr = actionDate ? actionDate : ''

      const translationTown = getEUProcurementTranslationTown(filing)
      const translationTownStr = translationTown ? translationTown : ''

      const noticeOriginalCPV = getEUProcurementNoticeOriginalCPV(filing)
      const noticeOriginalCPVStr = noticeOriginalCPV
        ? trimLabel(noticeOriginalCPV, maxDescriptionLength)
        : ''

      const maxValue = getEUProcurementMaxValue(filing)

      const numContractAwards = getEUProcurementNumContractAwards(filing)

      const contractTitleStr = serializeLanguageDict(
        getEUProcurementContractTitle(filing)
      )

      const shortDescrDict = getEUProcurementContractShortDescription(filing)
      for (const key in shortDescrDict) {
        shortDescrDict[key] = trimLabel(
          shortDescrDict[key],
          maxDescriptionLength
        )
      }
      const shortDescrStr = serializeLanguageDict(shortDescrDict)

      const contractingAuthoritiesStr =
        getEUProcurementContractingAuthorities(filing).join('; ')

      const contractorsStr =
        getEUProcurementContractorsAndOperators(filing).join('; ')

      const entireFiling = JSON.stringify(filing)
      tableData.push([
        filingIndex,
        actionDateStr,
        translationTownStr,
        noticeOriginalCPVStr,
        maxValue.amount,
        numContractAwards,
        contractTitleStr,
        shortDescrStr,
        contractingAuthoritiesStr,
        contractorsStr,
        entireFiling,
      ])
    })

    const table = $(`#${tableID}`).DataTable({
      width: '100%',
      data: tableData,
      columnDefs: [
        { targets: 0, title: 'Index', visible: false },
        { targets: 1, title: 'Action Date' },
        { targets: 2, title: 'Town' },
        { targets: 3, title: 'Original CPV' },
        {
          targets: 4,
          title: 'Max Value',
          render: $.fn.dataTable.render.number(',', '.', 2, ''),
        },
        { targets: 5, title: 'Num Awards' },
        { targets: 6, title: 'Contract Title' },
        { targets: 7, title: 'Short Description' },
        { targets: 8, title: 'Contracting Authorities' },
        { targets: 9, title: 'Contractors' },
        { targets: 10, title: 'Entire Filing', visible: false },
        { targets: '_all', searchable: true },
      ],
    })

    $(`#${tableID} tbody`).on('click', 'tr', function () {
      const filingIndex = table.row(this).index()
      const filing = filings[filingIndex]
      const holder = $(`#${modalID}`)
      const titleID = `${modalID}-title`
      EUProcurement(holder, filing, titleID, accordionID)
      holder.modal('show')
    })
  })
}
