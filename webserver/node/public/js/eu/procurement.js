// Metadata for the procurement tabs for each supported EU country.
const EUCountryProcurementTabs = {
  '1a': {
    logo: '/logos/government_of_the_republic_of_kosovo.svg',
    title: 'Kosovan Contracts (through EU)',
  },
  al: {
    logo: '/logos/government_of_the_republic_of_albania.svg',
    title: 'Albanian Contracts (through EU)',
  },
  am: {
    logo: '/logos/government_of_the_republic_of_armenia.svg',
    title: 'Armenian Contracts (through EU)',
  },
  ar: {
    logo: '/logos/government_of_the_argentine_republic.svg',
    title: 'Argentine Contracts (through EU)',
  },
  at: {
    logo: '/logos/government_of_austria.svg',
    title: 'Austrian Contracts',
  },
  ba: {
    logo: '/logos/government_of_bosnia_and_herzegovina.svg',
    title: 'Bosnian Contracts (through EU)',
  },
  bd: {
    logo: '/logos/government_of_the_peoples_republic_of_bangladesh.svg',
    title: 'Bangladeshi Contracts (through EU)',
  },
  be: {
    logo: '/logos/government_of_the_kingdom_of_belgium.svg',
    title: 'Belgian Contracts',
  },
  bg: {
    logo: '/logos/government_of_the_republic_of_bulgaria.svg',
    title: 'Bulgarian Contracts',
  },
  bj: {
    logo: '/logos/government_of_the_republic_of_benin.svg',
    title: 'Benin Contracts (through EU)',
  },
  bo: {
    logo: '/logos/government_of_the_plurinational_state_of_bolivia.svg',
    title: 'Bolivian Contracts (through EU)',
  },
  by: {
    logo: '/logos/government_of_the_republic_of_belarus.svg',
    title: 'Belarusian Contracts (through EU)',
  },
  cg: {
    logo: '/logos/government_of_the_republic_of_congo.svg',
    title: 'Congo Contracts (through EU)',
  },
  ch: {
    logo: '/logos/government_of_switzerland.svg',
    title: 'Swiss Contracts',
  },
  ci: {
    logo: '/logos/government_of_the_republic_of_cote_divoire.svg',
    title: "Cote d'Ivoire Contracts",
  },
  cm: {
    logo: '/logos/government_of_the_republic_of_cameroon.svg',
    title: 'Cameroon Contracts (through EU)',
  },
  cn: {
    logo: '/logos/government_of_the_peoples_republic_of_china.svg',
    title: 'Chinese Contracts (through EU)',
  },
  co: {
    logo: '/logos/government_of_the_republic_of_colombia.svg',
    title: 'Colombian Contracts (through EU)',
  },
  cy: {
    logo: '/logos/government_of_the_republic_of_cyprus.svg',
    title: 'Cypriot Contracts (through EU)',
  },
  cz: {
    logo: '/logos/government_of_the_czech_republic.svg',
    title: 'Czech Contracts',
  },
  de: {
    logo: '/logos/government_of_the_federal_republic_of_germany.svg',
    title: 'German Contracts',
  },
  dk: {
    logo: '/logos/government_of_denmark.svg',
    title: 'Danish Contracts',
  },
  dz: {
    logo: '/logos/government_of_the_peoples_republic_of_algeria.svg',
    title: 'Algerian Contracts (through EU)',
  },
  ec: {
    logo: '/logos/government_of_the_republic_of_ecuador.svg',
    title: 'Ecuadorian Contracts (through EU)',
  },
  ee: {
    logo: '/logos/government_of_the_republic_of_estonia.svg',
    title: 'Estonian Contracts',
  },
  eg: {
    logo: '/logos/government_of_the_arab_republic_of_egypt.svg',
    title: 'Egyptian Contracts (through EU)',
  },
  es: {
    logo: '/logos/government_of_the_kingdom_of_spain.svg',
    title: 'Spanish Contracts',
  },
  et: {
    logo: '/logos/government_of_the_federal_democratic_republic_of_ethiopia.svg',
    title: 'Ethiopian Contracts (through EU)',
  },
  fi: {
    logo: '/logos/government_of_finland.svg',
    title: 'Finnish Contracts',
  },
  fr: {
    logo: '/logos/government_of_france.svg',
    title: 'French Contracts',
  },
  ge: {
    logo: '/logos/government_of_georgia.svg',
    title: 'Georgian Contracts (through EU)',
  },
  gn: {
    logo: '/logos/government_of_the_republic_of_guinea.svg',
    title: 'Guinea Contracts (through EU)',
  },
  gp: {
    logo: '/logos/department_of_guadeloupe.svg',
    title: 'Guadeloupe Contracts (through EU)',
  },
  gr: {
    logo: '/logos/government_of_the_hellenic_republic.svg',
    title: 'Greek Contracts',
  },
  hr: {
    logo: '/logos/government_of_the_republic_of_croatia.svg',
    title: 'Croatian Contracts',
  },
  ht: {
    logo: '/logos/government_of_the_republic_of_haiti.svg',
    title: 'Haitian Contracts (through EU)',
  },
  hu: {
    logo: '/logos/government_of_hungary.svg',
    title: 'Hungarian Contracts',
  },
  ie: {
    logo: '/logos/government_of_ireland.svg',
    title: 'Irish Contracts',
  },
  il: {
    logo: '/logos/government_of_israel.svg',
    title: 'Israeli Contracts (through EU)',
  },
  in: {
    logo: '/logos/government_of_the_republic_of_india.svg',
    title: 'Indian Contracts (through EU)',
  },
  is: {
    logo: '/logos/government_of_iceland.svg',
    title: 'Icelandic Contracts',
  },
  it: {
    logo: '/logos/government_of_italy.svg',
    title: 'Italian Contracts',
  },
  ke: {
    logo: '/logos/government_of_the_republic_of_kenya.svg',
    title: 'Kenyan Contracts (through EU)',
  },
  kg: {
    logo: '/logos/government_of_the_kyrgyz_republic.svg',
    title: 'Kyrgyz Republic Contracts (through EU)',
  },
  kn: {
    logo: '/logos/government_of_the_federation_of_saint_christopher_and_nevis.svg',
    title: 'Saint Kitts and Nevis Contracts (through EU)',
  },
  kz: {
    logo: '/logos/government_of_the_republic_of_kazakhstan.svg',
    title: 'Kazakh Contracts (through EU)',
  },
  la: {
    logo: '/logos/government_of_the_lao_peoples_democratic_republic.svg',
    title: 'Lao Contracts (through EU)',
  },
  lb: {
    logo: '/logos/government_of_the_republic_of_lebanon.svg',
    title: 'Lebanese Contracts (through EU)',
  },
  li: {
    logo: '/logos/government_of_the_principality_of_liechtenstein.svg',
    title: 'Liechtenstein Contracts (through EU)',
  },
  lt: {
    logo: '/logos/government_of_the_republic_of_lithuania.svg',
    title: 'Lithuanian Contracts',
  },
  lu: {
    logo: '/logos/government_of_the_grand_duchy_of_luxembourg.svg',
    title: 'Luxembourgish Contracts',
  },
  lv: {
    logo: '/logos/government_of_the_republic_of_latvia.svg',
    title: 'Latvian Contracts',
  },
  ma: {
    logo: '/logos/government_of_the_kingdom_of_morocco.svg',
    title: 'Moroccan Contracts (through EU)',
  },
  md: {
    logo: '/logos/government_of_the_republic_of_moldova.svg',
    title: 'Moldovan Contracts',
  },
  me: {
    logo: '/logos/government_of_montenegro.svg',
    title: 'Montenegrin Contracts (through EU)',
  },
  mg: {
    logo: '/logos/government_of_the_republic_of_madagascar.svg',
    title: 'Madagascar Contracts (through EU)',
  },
  mk: {
    logo: '/logos/government_of_the_republic_of_north_macedonia.svg',
    title: 'Macedonian Contracts',
  },
  ml: {
    logo: '/logos/government_of_the_republic_of_mali.svg',
    title: 'Malian Contracts (through EU)',
  },
  mq: {
    logo: '/logos/department_of_martinique.svg',
    title: 'Martinique Contracts (through EU)',
  },
  mt: {
    logo: '/logos/government_of_the_republic_of_malta.svg',
    title: 'Maltese Contracts',
  },
  mw: {
    logo: '/logos/government_of_the_republic_of_malawi.svg',
    title: 'Malawi Contracts (through EU)',
  },
  ni: {
    logo: '/logos/government_of_the_republic_of_nicaragua.svg',
    title: 'Nicaraguan Contracts (through EU)',
  },
  nl: {
    logo: '/logos/government_of_the_netherlands.svg',
    title: 'Netherlands Contracts',
  },
  no: {
    logo: '/logos/government_of_norway.svg',
    title: 'Norwegian Contracts',
  },
  np: {
    logo: '/logos/government_of_the_federal_democratic_republic_of_nepal.svg',
    title: 'Nepali Contracts (through EU)',
  },
  pl: {
    logo: '/logos/government_of_poland.svg',
    title: 'Polish Contracts',
  },
  ps: {
    logo: '/logos/government_of_the_state_of_palestine.svg',
    title: 'Palestinian Contracts (through EU)',
  },
  pt: {
    logo: '/logos/government_of_the_portuguese_republic.svg',
    title: 'Portuguese Contracts',
  },
  py: {
    logo: '/logos/government_of_the_republic_of_paraguay.svg',
    title: 'Paraguay Contracts (through EU)',
  },
  re: {
    logo: '/logos/department_of_réunion.svg',
    title: 'Réunion Contracts (through EU)',
  },
  ro: {
    logo: '/logos/government_of_romania.svg',
    title: 'Romanian Contracts',
  },
  rs: {
    logo: '/logos/government_of_the_republic_of_serbia.svg',
    title: 'Serbian Contracts',
  },
  ru: {
    logo: '/logos/government_of_the_russian_federation.svg',
    title: 'Russian Contracts (through EU)',
  },
  se: {
    logo: '/logos/government_of_sweden.svg',
    title: 'Swedish Contracts',
  },
  si: {
    logo: '/logos/government_of_the_republic_of_slovenia.svg',
    title: 'Slovenian Contracts',
  },
  sk: {
    logo: '/logos/government_of_the_slovak_republic.svg',
    title: 'Slovak Contracts',
  },
  sl: {
    logo: '/logos/government_of_the_republic_of_sierra_leone.svg',
    title: 'Sierra Leone Contracts (through EU)',
  },
  sn: {
    logo: '/logos/government_of_the_republic_of_senegal.svg',
    title: 'Senegalese Contracts (through EU)',
  },
  td: {
    logo: '/logos/government_of_the_republic_of_chad.svg',
    title: 'Chad Contracts (through EU)',
  },
  th: {
    logo: '/logos/government_of_the_kingdom_of_thailand.svg',
    title: 'Thai Contracts (through EU)',
  },
  tj: {
    logo: '/logos/government_of_the_republic_of_tajikistan.svg',
    title: 'Tajik Contracts (through EU)',
  },
  tn: {
    logo: '/logos/government_of_the_republic_of_tunisia.svg',
    title: 'Tunisian Contracts (through EU)',
  },
  tr: {
    logo: '/logos/government_of_the_republic_of_turkey.svg',
    title: 'Turkish Contracts (through EU)',
  },
  tz: {
    logo: '/logos/government_of_the_united_republic_of_tanzania.svg',
    title: 'Tanzanian Contracts (through EU)',
  },
  ua: {
    logo: '/logos/government_of_ukraine.svg',
    title: 'Ukrainian Contracts (through EU)',
  },
  ug: {
    logo: '/logos/government_of_the_republic_of_uganda.svg',
    title: 'Ugandan Contracts (through EU)',
  },
  uk: {
    logo: '/logos/government_of_the_united_kingdom.svg',
    title: 'UK Contracts (Pre-Brexit, through EU)',
  },
  us: {
    logo: '/logos/government_of_the_united_states.svg',
    title: 'US Contracts (through EU)',
  },
  yt: {
    logo: '/logos/department_of_mayotte.svg',
    title: 'Mayotte Contracts (through EU)',
  },
  zm: {
    logo: '/logos/government_of_the_republic_of_zambia.svg',
    title: 'Zambian Contracts (through EU)',
  },
}

function EUProcurementNameToHTML(filing, name) {
  if (!name) {
    return ''
  }

  const entity = filing.annotation.entities[name.toLowerCase()]
  let lines = []
  appendEntityAnnotationLines(lines, entity, name)
  return lines.join('<br/>')
}

function serializeLanguageDict(dict) {
  let lines = []
  for (const [key, value] of Object.entries(dict)) {
    lines.push(`${key.toUpperCase()}: "${value}"`)
  }
  return lines.join(', ')
}

function getEUProcurementActionDate(filing) {
  if (filing['last_action_date']) {
    return filing['last_action_date']
  } else {
    return undefined
  }
}

function getEUProcurementTranslationCountry(filing) {
  for (const [key, value] of Object.entries(filing.data)) {
    const translation = value.translation
    if (!translation) {
      continue
    }
    if ('title' in translation) {
      const title = translation.title
      return title.country
    }
  }
  return undefined
}

function getEUProcurementTranslationTown(filing) {
  for (const [key, value] of Object.entries(filing.data)) {
    const translation = value.translation
    if (!translation) {
      continue
    }
    if ('title' in translation) {
      const title = translation.title
      return title.town
    }
  }
  return undefined
}

function getEUProcurementContractingAuthorities(filing) {
  let namesList = []
  let namesSet = new Set()
  for (const [key, value] of Object.entries(filing.data)) {
    const translation = value.translation
    if (!translation) {
      continue
    }
    if ('names' in translation) {
      for (const name of translation['names']) {
        if (name && !(name.toLowerCase() in namesSet)) {
          namesList.push(name)
          namesSet.add(name.toLowerCase())
        }
      }
    }
  }
  return namesList
}

function getEUProcurementContractors(filing) {
  let namesList = []
  let namesSet = new Set()
  for (const [key, value] of Object.entries(filing.data)) {
    const form = value.form
    if (!form || !('award_contracts' in form)) {
      continue
    }
    for (const contract of form['award_contracts']) {
      if (!contract) {
        continue
      }
      if (!('awarded_contract' in contract)) {
        continue
      }
      const awardedContract = contract['awarded_contract']
      if (!('contractors' in awardedContract)) {
        continue
      }
      const contractors = awardedContract['contractors']
      if (!('contractor' in contractors)) {
        continue
      }
      for (const contractor of contractors['contractor']) {
        if (!('address' in contractor)) {
          continue
        }
        const address = contractor['address']
        if (!('official_name' in address)) {
          continue
        }
        const name = address['official_name']
        if (name && !(name.toLowerCase() in namesSet)) {
          namesList.push(name)
          namesSet.add(name.toLowerCase())
        }
      }
    }
  }
  return namesList
}

function getEUProcurementDefenceContractAwards(filing) {
  let contractAwards = {}
  for (const [languageCode, value] of Object.entries(filing.data)) {
    const form = value.form
    if (!form || !('fd_contract_award_defence' in form)) {
      continue
    }
    const defence = form['fd_contract_award_defence']
    if (!('contract_award' in defence)) {
      continue
    }
    contractAwards[languageCode] = defence['contract_award']
  }
  return contractAwards
}

function getEUProcurementDefenceOperators(filing) {
  let namesList = []
  let namesSet = new Set()
  for (const [key, value] of Object.entries(filing.data)) {
    const form = value.form
    if (!form || !('fd_contract_award_defence' in form)) {
      continue
    }
    const defence = form['fd_contract_award_defence']
    if (!('contract_award' in defence)) {
      continue
    }
    for (const contract of defence['contract_award']) {
      if (!('operator' in contract)) {
        continue
      }
      const operator = contract['operator']
      if ('name' in operator && operator['name']) {
        const name = operator['name']
        if (!(name.toLowerCase() in namesSet)) {
          namesList.push(name)
          namesSet.add(name.toLowerCase())
        }
      }
    }
  }
  return namesList
}

function getEUProcurementContractorsAndOperators(filing) {
  const contractors = getEUProcurementContractors(filing)
  const operators = getEUProcurementDefenceOperators(filing)
  return [...new Set([].concat(contractors, operators))]
}

function getEUProcurementTranslationTitle(filing) {
  for (const [key, value] of Object.entries(filing.data)) {
    const translation = value.translation
    if (!translation) {
      continue
    }
    if ('title' in translation && 'text' in translation.title) {
      const title = translation.title
      return title.text.join('; ')
    }
  }
  return undefined
}

function getEUProcurementContractAwards(filing) {
  let awards = {}
  for (const [key, value] of Object.entries(filing.data)) {
    const form = value.form
    if (!form) {
      continue
    }
    if ('award_contracts' in form) {
      awards[key] = form.award_contracts
    }
  }
  return awards
}

function getEUProcurementNumContractAwards(filing) {
  const awards = getEUProcurementContractAwards(filing)
  let maxAwards = 0
  for (const [language, language_data] of Object.entries(awards)) {
    let numAwards = 0
    for (const item of language_data) {
      if (item && item.awarded_contract) {
        numAwards += 1
      }
    }
    maxAwards = Math.max(maxAwards, numAwards)
  }
  return maxAwards
}

function getEUProcurementContractObjects(filing) {
  let objects = {}
  for (const [key, value] of Object.entries(filing.data)) {
    const form = value.form
    if (!form) {
      continue
    }
    if ('object_contract' in form) {
      objects[key] = form.object_contract
    }
  }
  return objects
}

function getEUProcurementContractTitle(filing) {
  let titles = {}
  const objects = getEUProcurementContractObjects(filing)
  for (const [key, value] of Object.entries(objects)) {
    if ('title' in value) {
      if (value.title.length) {
        titles[key] = value.title.join('; ')
      }
    }
  }
  return titles
}

function getEUProcurementContractShortDescription(filing) {
  let shortDescr = {}
  for (const [key, value] of Object.entries(filing.data)) {
    const form = value.form
    if (!form) {
      continue
    }
    if ('object_contract' in form) {
      const objectContract = form.object_contract
      if ('short_descr' in objectContract) {
        if (objectContract.short_descr.length) {
          shortDescr[key] = objectContract.short_descr.join('; ')
        }
      }
    }
  }
  return shortDescr
}

function getEUProcurementURI(filing) {
  for (const [key, value] of Object.entries(filing.data)) {
    const codedData = value.coded_data
    if (!codedData) {
      continue
    }
    if ('notice_data' in codedData) {
      const noticeData = codedData.notice_data
      if ('uri' in noticeData) {
        return noticeData.uri
      }
    }
  }
  return undefined
}

function getEUProcurementComplementaryInfo(filing) {
  let info = undefined
  for (const [key, value] of Object.entries(filing.data)) {
    if (!('form' in value)) {
      continue
    }
    const form = value.form
    if (!('complementary_info' in form)) {
      continue
    }
    return form.complementary_info
  }
  return undefined
}

function getEUProcurementContractingAuthority(filing) {
  let authority = undefined
  for (const [key, value] of Object.entries(filing.data)) {
    if (!('form' in value)) {
      continue
    }
    const form = value.form
    if (!('contracting_body' in form)) {
      continue
    }
    return form.contracting_body
  }
  return undefined
}

function getEUProcurementNoticeData(filing) {
  // At the moment, this should always be in English.
  const codedData = filing.data.en.coded_data
  if (codedData && codedData.notice_data) {
    return codedData.notice_data
  }
  return undefined
}

function getEUProcurementNoticeValues(filing) {
  const noticeData = getEUProcurementNoticeData(filing)
  let valueList = []
  // TODO(Jack Poulson): Normalize these lists upstream.
  if (
    noticeData &&
    'VALUES_LIST' in noticeData &&
    noticeData['VALUES_LIST'] &&
    'VALUES' in noticeData['VALUES_LIST'] &&
    noticeData['VALUES_LIST']['VALUES']
  ) {
    let values = noticeData['VALUES_LIST']['VALUES']
    if (!Array.isArray(values)) {
      values = [values]
    }
    for (const value of values) {
      if ('SINGLE_VALUE' in value) {
        const singleValue = value['SINGLE_VALUE']['VALUE']
        valueList.push({
          amount: singleValue['#text'],
          currency: singleValue['@CURRENCY'],
          type: value['@TYPE'],
        })
      } else if ('RANGE_VALUE' in value) {
        for (const [rangeIndex, rangeValue] of value['RANGE_VALUE'][
          'VALUE'
        ].entries()) {
          valueList.push({
            amount: rangeValue['#text'],
            currency: rangeValue['@CURRENCY'],
            type: `${value['@TYPE']} (Range value ${rangeIndex})`,
          })
        }
      } else {
        console.log(`Improper noticeData value:`)
        console.log(value)
      }
    }
  }
  if (noticeData && 'values' in noticeData && noticeData['values']) {
    const values = noticeData['values']
    if ('value' in values && values['value']) {
      valueList.push(values['value'])
    }
  }
  return valueList
}

function getEUProcurementNoticeOriginalCPV(filing) {
  const noticeData = getEUProcurementNoticeData(filing)
  if (noticeData && 'original_cpv' in noticeData) {
    return noticeData['original_cpv']
  }
  return undefined
}

function getEUProcurementTitle(filing) {
  const originalCPV = getEUProcurementNoticeOriginalCPV(filing)
  const contractTitle = serializeLanguageDict(
    getEUProcurementContractTitle(filing)
  )
  if (originalCPV && contractTitle) {
    return `[CPV] ${originalCPV}<br/>[Contract Title] ${contractTitle}`
  } else if (originalCPV) {
    return `[CPV] ${originalCPV}`
  } else if (contractTitle) {
    return `[Contract Title] ${contractTitle}`
  } else {
    return undefined
  }
}

function getEUProcurementContractAwardValues(filing) {
  let values = []
  const awards = getEUProcurementContractAwards(filing)
  if (!Object.keys(awards).length) {
    return values
  }

  for (const [language, language_data] of Object.entries(awards)) {
    for (const item of language_data) {
      if (!item) {
        continue
      }
      const contract = item.awarded_contract
      if (contract) {
        if (
          contract.values &&
          (contract.values.total || contract.values.estimated_total)
        ) {
          values.push(contract.values)
        }
      }
    }
  }
  return values
}

function getEUProcurementContractObjectsValues(filing) {
  let values = []
  const objects = getEUProcurementContractObjects(filing)
  if (!Object.keys(objects).length) {
    return values
  }
  for (const [language, language_data] of Object.entries(objects)) {
    if ('estimated_total' in language_data) {
      values.push(language_data.estimated_total)
    }
  }
  return values
}

function parseEUProcurementValue(value) {
  if (!value) {
    return undefined
  }
  value = value.replaceAll(' ', '')
  return parseFloat(value)
}

function getEUProcurementMaxValue(filing) {
  const awardValues = getEUProcurementContractAwardValues(filing)
  const objectValues = getEUProcurementContractObjectsValues(filing)
  const noticeValues = getEUProcurementNoticeValues(filing)

  // For now, we assume a fixed currency is used across the values.
  let maxAmount = 0
  let currency = ''
  for (const value of awardValues) {
    if (value.total) {
      const amount = parseEUProcurementValue(value.total.amount)
      if (amount > maxAmount) {
        maxAmount = amount
        currency = value.currency
      }
    }
    if (value.estimated_total) {
      const amount = parseEUProcurementValue(value.estimated_total.amount)
      if (amount > maxAmount) {
        maxAmount = amount
        currency = value.currency
      }
    }
  }
  for (const value of objectValues) {
    const amount = parseEUProcurementValue(value.amount)
    if (amount > maxAmount) {
      maxAmount = amount
      currency = value.currency
    }
  }
  for (const value of noticeValues) {
    const amount = parseEUProcurementValue(value.amount)
    if (amount > maxAmount) {
      maxAmount = amount
      currency = value.currency
    }
  }
  return { amount: maxAmount, currency: currency }
}

function EUProcurement(holder, filing, titleID, accordionID) {
  function appendContractingAuthority(parentHTML, parentID, expanded) {
    const authority = getEUProcurementContractingAuthority(filing)
    if (!authority) {
      return
    }

    const buttonHTML = 'Contracting Authority'
    const label = 'contractingAuthority'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    const address = authority.address
    if (address) {
      accordion.appendFeature(
        'Official Name',
        EUProcurementNameToHTML(filing, address.official_name)
      )
      accordion.appendFeature('Contact Point', address.contact_point)

      const addressObj = {
        streetLines: [address.address],
        city: address.city,
        postalCode: address.postal_code,
        country: address.country,
        email: address.email,
      }
      accordion.appendFeature('Address', formatAddress(addressObj))
      accordion.appendURL('General URL', prefixURL(address.general_url))
      accordion.appendFeature('National ID', address.national_id)
      // TODO(Jack Poulson): address.nuts
    }
    accordion.appendFeature('CA Type', authority.ca_type)
    accordion.appendFeature('CA Activity', authority.ca_activity)
    accordion.appendFeature('CE Type', authority.ce_type)
    accordion.appendFeature('CE Activity', authority.ce_activity)
  }

  function appendObjectOfContract(parentHTML, parentID, expanded) {
    const objects = getEUProcurementContractObjects(filing)
    if (!Object.keys(objects).length) {
      return
    }

    const buttonHTML = 'Object of Contract'
    const label = 'contractObject'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    // Display the top-level titles.
    for (const [key, value] of Object.entries(objects)) {
      const language = key.toUpperCase()
      const titleStr = value.title ? value.title.join('; ') : ''
      accordion.appendFeature(`Title [${language}]`, titleStr)
    }

    // Display the top-level short descriptions.
    for (const [key, value] of Object.entries(objects)) {
      const language = key.toUpperCase()
      const titleStr = value.title ? value.title.join('; ') : ''
      const shortDescrStr = value.short_descr
        ? value.short_descr.join('; ')
        : ''
      if (titleStr != shortDescrStr) {
        accordion.appendFeature(
          `Short Description [${language}]`,
          shortDescrStr
        )
      }
    }

    // Display the remaining top-level features
    for (const [key, value] of Object.entries(objects)) {
      const language = key.toUpperCase()
      if (value.estimated_total) {
        accordion.appendFeature(
          'Estimated Total',
          `${value.estimated_total.amount} ${value.estimated_total.currency}`
        )
      }
      accordion.appendFeature('Reference Number', value.reference_number)
    }

    // Create the list of object description accordions.
    let objDescrAccordions = []
    for (const [key, value] of Object.entries(objects)) {
      const language = key.toUpperCase()
      if (value.object_descr) {
        for (const [objDescrIndex, objDescr] of value.object_descr.entries()) {
          if (objDescrIndex == objDescrAccordions.length) {
            const objDescrButtonHTML = `Object Description ${objDescrIndex}`
            const objDescrLabel = `contractObjectDescr-${objDescrIndex}`
            let objDescrAccordion = new Accordion(
              accordion.collapseBody(),
              accordion.collapseID(),
              objDescrButtonHTML,
              false,
              objDescrLabel
            )
            objDescrAccordions.push(objDescrAccordion)
          }
        }
      }
    }

    // Display the object description titles.
    for (const [key, value] of Object.entries(objects)) {
      const language = key.toUpperCase()
      if (!value.object_descr) {
        continue
      }
      for (const [objDescrIndex, objDescr] of value.object_descr.entries()) {
        let objDescrAccordion = objDescrAccordions[objDescrIndex]
        const objDescrTitle = objDescr.title ? objDescr.title.join('; ') : ''
        objDescrAccordion.appendFeature(`Title [${language}]`, objDescrTitle)
      }
    }

    // Display the object description short descriptions.
    for (const [key, value] of Object.entries(objects)) {
      const language = key.toUpperCase()
      if (!value.object_descr) {
        continue
      }
      for (const [objDescrIndex, objDescr] of value.object_descr.entries()) {
        let objDescrAccordion = objDescrAccordions[objDescrIndex]
        const objDescrTitle = objDescr.title ? objDescr.title.join('; ') : ''
        const objDescrShortDescr = objDescr.short_descr
          ? objDescr.short_descr.join('; ')
          : ''
        if (objDescrShortDescr != objDescrTitle) {
          objDescrAccordion.appendFeature(
            `Short Description [${language}]`,
            objDescrShortDescr
          )
        }
      }
    }

    // Display the remaining object description features
    for (const [key, value] of Object.entries(objects)) {
      const language = key.toUpperCase()
      if (!value.object_descr) {
        continue
      }
      for (const [objDescrIndex, objDescr] of value.object_descr.entries()) {
        let objDescrAccordion = objDescrAccordions[objDescrIndex]
        objDescrAccordion.appendFeature('Lot Number', objDescr.lot_number)
        objDescrAccordion.appendFeature('Item Number', objDescr.item_number)
        if (objDescr.main_site) {
          objDescrAccordion.appendFeature(
            'Main Site',
            objDescr.main_site.join('; ')
          )
        }
        if (objDescr.eu_progr_related) {
          objDescrAccordion.appendFeature(
            'Related EU Programs',
            objDescr.eu_progr_related.join('; ')
          )
        }
        // TODO(Jack Poulson): cpv_additional[i].cpv_code
        // TODO(Jack Poulson): nuts
      }
    }
  }

  function appendContractAwards(parentHTML, parentID, expanded) {
    const awards = getEUProcurementContractAwards(filing)
    if (!Object.keys(awards).length) {
      return
    }

    const buttonHTML = 'Contract Awards'
    const label = 'contractAwards'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    // Get the list of award titles.
    let awardTitles = []
    for (const [key, value] of Object.entries(awards)) {
      const language = key.toUpperCase()
      for (const [itemIndex, item] of value.entries()) {
        if (itemIndex == awardTitles.length) {
          awardTitles.push({})
        }
        if (item.title) {
          awardTitles[itemIndex][language] = item.title.join('; ')
        }
      }
    }

    // Create the list of award accordions.
    let awardAccordions = []
    for (const [key, value] of Object.entries(awards)) {
      const language = key.toUpperCase()
      for (const [itemIndex, item] of value.entries()) {
        const contract = item.awarded_contract
        if (itemIndex == awardAccordions.length) {
          // Get the contract title (preferring English, if one exists)
          const awardTitle = awardTitles[itemIndex]
          let awardTitleStr = ''
          if ('EN' in awardTitle) {
            awardTitleStr = awardTitle['EN']
          } else {
            for (const [titleKey, titleValue] of Object.entries(awardTitle)) {
              if (titleValue) {
                awardTitleStr = titleValue
              }
            }
          }

          // Get the list of contractor names for this award
          let contractorSet = new Set()
          if (contract && contract.contractors) {
            const contractors = contract.contractors
            if (contractors.contractor) {
              for (const [cIndex, c] of contractors.contractor.entries()) {
                const cAddress = c.address
                if (cAddress) {
                  contractorSet.add(cAddress.official_name)
                }
              }
            }
          }
          const contractorNames = Array.from(contractorSet)
          const contractorsStr = contractorNames.join('; ')

          let awardButtonHTML = awardTitleStr
            ? awardTitleStr
            : `Contract ${itemIndex}`
          if (contractorsStr) {
            awardButtonHTML += ` [${contractorsStr}]`
          }
          const awardLabel = `contractAward-${itemIndex}`
          const awardAccordion = new Accordion(
            accordion.collapseBody(),
            accordion.collapseID(),
            awardButtonHTML,
            false,
            awardLabel
          )
          awardAccordions.push(awardAccordion)
        }
      }
    }

    // Add the titles first.
    for (const [key, value] of Object.entries(awards)) {
      const language = key.toUpperCase()
      for (const [itemIndex, item] of value.entries()) {
        let awardAccordion = awardAccordions[itemIndex]
        if (item.title) {
          awardAccordion.appendFeature(
            `Title [${language}]`,
            item.title.join('; ')
          )
        }
      }
    }

    // Add the remaining features.
    for (const [key, value] of Object.entries(awards)) {
      const language = key.toUpperCase()
      for (const [itemIndex, item] of value.entries()) {
        const contract = item.awarded_contract
        let awardAccordion = awardAccordions[itemIndex]

        awardAccordion.appendFeature('Lot Number', item.lot_number)
        awardAccordion.appendFeature('Item Number', item.item_number)
        awardAccordion.appendFeature('Contract Number', item.contract_number)

        if (contract) {
          const values = contract.values
          if (values && values.total) {
            awardAccordion.appendFeature(
              'Total Value',
              `${values.total.amount} ${values.total.currency}`
            )
          }
          if (values && values.estimated_total) {
            awardAccordion.appendFeature(
              'Estimated Total Value',
              `${values.estimated_total.amount} ${values.estimated_total.currency}`
            )
          }
          const tenders = contract.tenders
          if (contract.tenders) {
            awardAccordion.appendFeature('Num Tenders', tenders.num_tenders)
            awardAccordion.appendFeature(
              'Num Tenders SME',
              tenders.num_tenders_sme
            )
          }
          if (contract.contractors) {
            const contractors = contract.contractors
            if (contractors.contractor) {
              for (const [cIndex, c] of contractors.contractor.entries()) {
                // Get the contractor name -- if it exists.
                const cAddress = c.address
                const cName =
                  cAddress && cAddress.official_name
                    ? cAddress.official_name
                    : ''

                let cButtonHTML = `Contractor ${cIndex}`
                if (cName) {
                  cButtonHTML += ` [${cName}]`
                }
                const cLabel = `contractor-${itemIndex}-${cIndex}`
                const cAccordion = new Accordion(
                  awardAccordion.collapseBody(),
                  awardAccordion.collapseID(),
                  cButtonHTML,
                  false,
                  cLabel
                )
                if (cAddress) {
                  cAccordion.appendFeature(
                    'Name',
                    EUProcurementNameToHTML(filing, cName)
                  )
                  const addressObj = {
                    streetLines: [cAddress.address],
                    city: cAddress.city,
                    postalCode: cAddress.postal_code,
                    country: cAddress.country,
                  }
                  cAccordion.appendFeature('Address', formatAddress(addressObj))
                  cAccordion.appendFeature('National ID', cAddress.national_id)
                  // TODO(Jack Poulson): nuts
                }
              }
            }
            awardAccordion.appendFeature(
              'Conclusion Date',
              contractors.conclusion_date
            )
          }
        }
      }
    }
  }

  function appendDefenceContractAwards(parentHTML, parentID, expanded) {
    const awards = getEUProcurementDefenceContractAwards(filing)
    if (!Object.keys(awards).length) {
      return
    }

    const buttonHTML = 'Defence Contract Awards'
    const label = 'defenceContractAwards'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    // Get the list of award titles.
    let awardTitles = []
    for (const [key, value] of Object.entries(awards)) {
      const language = key.toUpperCase()
      for (const [itemIndex, item] of value.entries()) {
        if (itemIndex == awardTitles.length) {
          awardTitles.push({})
        }
        if (item.title) {
          awardTitles[itemIndex][language] = item.title.join('; ')
        }
      }
    }

    let awardAccordions = []
    for (const [key, value] of Object.entries(awards)) {
      const language = key.toUpperCase()
      for (const [itemIndex, item] of value.entries()) {
        if (itemIndex == awardAccordions.length) {
          // Get the contract title (preferring English, if one exists)
          const awardTitle = awardTitles[itemIndex]
          let awardTitleStr = ''
          if ('EN' in awardTitle) {
            awardTitleStr = awardTitle['EN']
          } else {
            for (const [titleKey, titleValue] of Object.entries(awardTitle)) {
              if (titleValue) {
                awardTitleStr = titleValue
              }
            }
          }

          const awardButtonHTML = awardTitleStr
            ? awardTitleStr
            : `Defence Award ${itemIndex}`
          const awardLabel = `defenceContractAward-${itemIndex}`
          const awardAccordion = new Accordion(
            accordion.collapseBody(),
            accordion.collapseID(),
            awardButtonHTML,
            false,
            awardLabel
          )
          awardAccordions.push(awardAccordion)
        }
      }
    }

    // Add the titles first.
    for (const [key, value] of Object.entries(awards)) {
      const language = key.toUpperCase()
      for (const [itemIndex, item] of value.entries()) {
        let awardAccordion = awardAccordions[itemIndex]
        if (item.title) {
          awardAccordion.appendFeature(
            `Title [${language}]`,
            item.title.join('; ')
          )
        }
      }
    }

    // Add the remaining features.
    for (const [key, value] of Object.entries(awards)) {
      const language = key.toUpperCase()
      for (const [itemIndex, item] of value.entries()) {
        const awardAccordion = awardAccordions[itemIndex]

        awardAccordion.appendFeature('Contract Number', item.contract_number)
        awardAccordion.appendDate('Date', item.date)
        if ('value' in item && 'cost_range' in item.value) {
          const costRange = item.value.cost_range
          awardAccordion.appendFeature(
            'Value',
            `${costRange.amount} ${costRange.currency}`
          )
        }

        if ('operator' in item) {
          const operator = item.operator
          awardAccordion.appendFeature(
            'Operator Name',
            EUProcurementNameToHTML(filing, operator.name)
          )
          const addressObj = {
            streetLines: [operator.address],
            city: operator.town,
            postalCode: operator.postal_code,
            country: operator.country,
          }
          awardAccordion.appendFeature(
            'Operator Address',
            formatAddress(addressObj)
          )
        }
      }
    }
  }

  function appendTranslation(parentHTML, parentID, expanded) {
    const town = getEUProcurementTranslationTown(filing)
    const country = getEUProcurementTranslationCountry(filing)
    const title = getEUProcurementTranslationTitle(filing)
    const names = getEUProcurementContractingAuthorities(filing)

    if (!(town || country || title || names.length)) {
      return
    }

    const buttonHTML = 'Short Summary'
    const label = 'shortSummary'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    accordion.appendFeature('Country', country)
    accordion.appendFeature('Town', town)
    accordion.appendFeature('Title', title)
    for (const [index, name] of names.entries()) {
      accordion.appendFeature(`Contracting Authority ${index + 1}`, name)
    }
  }

  function appendNoticeData(parentHTML, parentID, expanded) {
    const noticeData = getEUProcurementNoticeData(filing)
    const values = getEUProcurementNoticeValues(filing)
    const originalCPV = getEUProcurementNoticeOriginalCPV(filing)
    if (!noticeData) {
      return
    }

    const buttonHTML = 'Notice Data'
    const label = 'noticeData'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    for (const value of values) {
      accordion.appendFeature(
        `Value (${value.type})`,
        `${value.amount} ${value.currency}`
      )
    }
    accordion.appendFeature('Original CPV', originalCPV)
    accordion.appendFeature('Language', noticeData.language)
    // TODO(Jack Poulson):
    //   - ca_ce_nuts
    //   - performance_nuts
    //   - ref_notice->no_doc_ojs
    accordion.appendURL('General URL', prefixURL(noticeData.ia_url_general))
    accordion.appendFeature('NO DOC OJS', noticeData.no_doc_ojs)
    accordion.appendURL('URI', prefixURL(noticeData.uri))
  }

  function appendComplementaryInfo(parentHTML, parentID, expanded) {
    const info = getEUProcurementComplementaryInfo(filing)
    if (!info) {
      return
    }

    const buttonHTML = 'Complementary Information'
    const label = 'complementaryInfo'
    const accordion = new Accordion(
      parentHTML,
      parentID,
      buttonHTML,
      expanded,
      label
    )

    if (info.address_review_body) {
      const address = info.address_review_body
      const compReviewBodyButtonHTML = 'Review Body'
      const compReviewBodyLabel = 'complementaryReviewBody'
      const subAccordion = new Accordion(
        accordion.collapseBody(),
        accordion.collapseID(),
        compReviewBodyButtonHTML,
        false,
        compReviewBodyLabel
      )

      subAccordion.appendFeature(
        'Official Name',
        EUProcurementNameToHTML(address.official_name)
      )

      const addressObj = {
        streetLines: [address.address],
        city: address.city,
        postalCode: address.postal_code,
        country: address.country,
        email: address.email,
      }
      subAccordion.appendFeature('Address', formatAddress(addressObj))
      subAccordion.appendURL('URL', prefixURL(address.url))
    }
    if (info.address_review_info) {
      const address = info.address_review_info
      const compReviewInfoButtonHTML = 'Review Info'
      const compReviewInfoLabel = 'complementaryReviewInfo'
      const subAccordion = new Accordion(
        accordion.collapseBody(),
        accordion.collapseID(),
        compReviewInfoButtonHTML,
        false,
        compReviewInfoLabel
      )

      subAccordion.appendFeature(
        'Official Name',
        EUProcurementNameToHTML(address.official_name)
      )
      const addressObj = {
        streetLines: [address.address],
        city: address.city,
        postalCode: address.postal_code,
        country: address.country,
        email: address.email,
      }
      subAccordion.appendFeature('Address', formatAddress(addressObj))
      subAccordion.appendURL('URL', prefixURL(address.url))
    }
    if (info.address_mediation_body) {
      const address = info.address_mediation_body
      const mediationBodyButtonHTML = 'Review Info'
      const mediationBodyLabel = 'mediationBody'
      const subAccordion = new Accordion(
        accordion.collapseBody(),
        accordion.collapseID(),
        mediationBodyButtonHTML,
        false,
        mediationBodyLabel
      )

      subAccordion.appendFeature(
        'Official Name',
        EUProcurementNameToHTML(address.official_name)
      )
      const addressObj = {
        streetLines: [address.address],
        city: address.city,
        postalCode: address.postal_code,
        country: address.country,
        email: address.email,
      }
      subAccordion.appendFeature('Address', formatAddress(addressObj))
      subAccordion.appendURL('URL', prefixURL(address.url))
    }

    accordion.appendFeature('Dispatch Notice Date', info.dispatch_notice_date)
  }

  const maxTitleLength = 200
  const minTitleLengthBeforeIconNewline = 50

  let content = holder.find('.modal-content')
  content.empty()

  let title = getEUProcurementTitle(filing)
  if (filing.tender) {
    title = getTitle(filing)
    title = trimLabel(title, maxTitleLength)
    title = `<span style="font-weight: 700;">${title}</span>`
  }
  const uri = getEUProcurementURI(filing)
  if (uri) {
    if (title.length > minTitleLengthBeforeIconNewline) {
      title += '<br/>'
    }
    title +=
      `<a href="${uri}">` +
      '<img src="/logos/document.jpg" ' +
      'style="height: 1.5em; margin: 0 0.25em 0 0.5em;" />' +
      '</a>'
  }

  content.append(modalHeader(title, titleID))

  let body = modalBody()
  content.append(body)

  content.append(modalFooter())

  let accordionDiv = $('<div>').addClass('accordion').attr('id', accordionID)
  body.append(accordionDiv)

  appendTranslation(accordionDiv, accordionID, false)
  appendContractingAuthority(accordionDiv, accordionID, false)
  appendObjectOfContract(accordionDiv, accordionID, false)
  appendContractAwards(accordionDiv, accordionID, false)
  appendDefenceContractAwards(accordionDiv, accordionID, false)
  appendNoticeData(accordionDiv, accordionID, false)
  appendComplementaryInfo(accordionDiv, accordionID, false)
  // TODO(Jack Poulson)
}
