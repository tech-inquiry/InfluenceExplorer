function EUProcurementTab(
  countryCode,
  tabLogo,
  tabTitle,
  tabsState,
  dataBase,
  alternateNames = undefined
) {
  const label = `eu-${countryCode}`
  const queryURL =
    appendURLPath(EXPLORER_ROUTE, `api/eu/${countryCode}/procurement?`) +
    (dataBase['vendor']
      ? `vendor=${encodeURIComponentIncludingSlashes(dataBase.vendor)}`
      : `text=${encodeURIComponentIncludingSlashes(dataBase.text)}`)
  const recordsNote = $('<div>')
    .append(
      $('<p>')
        .text('All links and images are annotations added to the original ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', 'https://ted.europa.eu/TED/')
            .text('Tenders Electronic Daily')
        )
        .append(
          ' data by Tech Inquiry. Any normalized names are clearly labeled with' +
            " the original ('as written') text. " +
            ' And, '
        )
        .append(
          $('<span>')
            .css('font-weight', '700')
            .text(
              'for multi-lingual fields, the English text is the result of ' +
                ' machine translation, which is useful but error-prone.'
            )
        )
    )
    .append(
      $('<p>')
        .append('Please browse the ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', 'https://corporateeurope.org/en/reports')
            .text('reports')
        )
        .append(' from ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', 'https://corporateeurope.org/en')
            .text('Corporate Europe Observatory')
        )
        .append(' for focused studies on corporate influence in Europe.')
    )
    .append(
      $('<p>')
        .append('You can alternatively download the ')
        .append(
          $('<a>')
            .addClass('sourceLink')
            .attr('href', queryURL)
            .text('JSON data')
        )
        .append(' for this tab.')
    )

  FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote,
    alternateNames
  )

  const tabsID = `${label}-tabs`

  function renderRecordsTab(event, ui) {
    const api_endpoint = `api/eu/${countryCode}/procurement`
    const data = dataBase
    const containerID = `${label}-tab-records-inner`
    EUProcurementTable(api_endpoint, data, containerID)
  }

  function renderTab(event, ui) {
    const recordsTab = 0
    const activeTab = $(`#${tabsID}`).tabs('option', 'active')
    if (activeTab == recordsTab) {
      renderRecordsTab(event, ui)
    }
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
