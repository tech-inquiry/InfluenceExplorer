function AssociationsTab(tabsState, stylizedName, associations) {
  const label = 'associations'
  const tabLogo = '/logos/link.png'
  const tabTitle = 'Associations'
  NavTab(tabsState.navID(), label, tabLogo, tabTitle)

  let tab = $('<div>')
    .addClass('content')
    .attr('id', `filing-tab-${label}`)
    .css('display', 'none')
    .css('padding-top', '0')
    .css('padding-left', '0')
  $(`#${tabsState.id()}`).append(tab)

  // Prioritize high 'fromWeight's.
  associations.sort(function (assn1, assn2) {
    const fromWeight1 = 'weight' in assn1['from'] ? assn1['from']['weight'] : 1
    const fromWeight2 = 'weight' in assn2['from'] ? assn2['from']['weight'] : 1

    if (fromWeight1 > fromWeight2) {
      return -1
    }
    if (fromWeight1 == fromWeight2) {
      const from1 = assn1['from']['text']
      const from2 = assn2['from']['text']
      if (from1 < from2) {
        return -1
      } else if (from1 == from2) {
        const name1 = assn1['from']['name']
        const name2 = assn2['from']['name']
        if (name1 < name2) {
          return -1
        } else if (name1 == name2) {
          return 0
        } else {
          return 1
        }
      } else {
        return 1
      }
    }
    if (fromWeight1 < fromWeight2) {
      return 1
    }
  })

  let card = $('<div>').css('font-size', '175%')
  tab.append(card)
  let cardBody = $('<div>').addClass('card-body')
  card.append(cardBody)

  let table = $('<table>').addClass('table')
  cardBody.append(table)

  let thead = $('<thead>')
  table.append(thead)
  thead.append(
    $('<tr>')
      .append($('<th>').attr('scope', 'col').text('Relationship / Citation'))
      .append($('<th>').attr('scope', 'col').text('Name / Profile Link'))
      .append($('<th>').attr('scope', 'col').text('Image / Website'))
      .append($('<th>').attr('scope', 'col').text('Interfaces'))
  )

  let tbody = $('<tbody>')
  table.append(tbody)
  associations.forEach(function (association) {
    let tr = $('<tr>')
    tbody.append(tr)

    let fromTD = $('<td>')
    tr.append(fromTD)
    if ('url' in association) {
      fromTD.append(
        $('<a>')
          .addClass('sourceLink')
          .attr('href', association.url)
          .text(association.from.text)
      )
    } else {
      fromTD.text(association.from.text)
    }

    let nameTD = $('<td>')
    tr.append(nameTD)
    if ('nameURL' in association.from) {
      nameTD.append(
        $('<a>')
          .addClass('sourceLink')
          .attr('href', association.from.nameURL)
          .html(association.from.stylizedName)
      )
    } else {
      nameTD.html(association.from.stylizedName)
    }

    let logoTD = $('<td>')
    tr.append(logoTD)
    if ('logoHTML' in association.from) {
      logoTD.html(association.from.logoHTML)
    }

    let interfaceTD = $('<td>')
    tr.append(interfaceTD)
    let interfaceComponents = []
    if ('interface' in association.to) {
      interfaceComponents.push(
        `${association.to.interface} (for ${stylizedName})`
      )
    }
    if ('interface' in association.from) {
      interfaceComponents.push(
        `${association.from.interface} (for ${association.from.stylizedName})`
      )
    }
    interfaceTD.text(interfaceComponents.join(', '))
  })

  tabsState.addTab({})
}
