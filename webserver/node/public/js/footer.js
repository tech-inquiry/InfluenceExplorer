function FillFooter(holderID, explorerRoute) {
  let holder = $(`#${holderID}`)
  let content = holder.find('.modal-content')
  content.empty()

  let card = $('<div>')
    .addClass('card')
    .css('margin', '1em')
    .css('font-size', '200%')
  content.append(card)

  let cardBody = $('<div>').addClass('card-body')
  card.append(cardBody)

  let cardTitle = $('<h2>').addClass('card-title').css('font-weight', '700')
  cardTitle.append('Data Access, Your Privacy, and Where to Donate')
  cardBody.append(cardTitle)

  let torText = $('<p>').addClass('card-text').css('max-width', '1400px')
  cardBody.append(torText)
  torText
    .append(
      'Tech Inquiry does not use trackers or employ analytics on users. ' +
        'However, webservers are aware of your IP address. If this is an issue, ' +
        'please consider browsing through a quasi-anonymous method such as '
    )
    .append(
      $('<a>')
        .addClass('card-link')
        .attr('href', encodeEntity('the tor project, inc.', explorerRoute))
        .text('Tor')
    )
    .append(
      '. You can also download all of the source code and data running this ' +
        'website at '
    )
    .append(
      $('<a>')
        .addClass('card-link')
        .attr('href', 'https://gitlab.com/tech-inquiry/InfluenceExplorer/')
        .text('gitlab.com/tech-inquiry/InfluenceExplorer/')
    )
    .append(".  And you can browse Gitlab's government contracts ")
    .append(
      $('<a>')
        .addClass('card-link')
        .attr('href', encodeEntity('gitlab inc.', explorerRoute))
        .text('here')
    )
    .append('.')

  let commentText = $('<p>').addClass('card-text').css('max-width', '1400px')
  cardBody.append(commentText)
  commentText
    .append(
      $('<span>').css('font-weight', '700').text('Questions or Comments? ')
    )
    .append(
      'Researcher or reporter interested in API access, ' +
        'or looking for further refinement/verification of some of our data? ' +
        'Email us at '
    )
    .append(
      $('<a>')
        .addClass('card-link"')
        .attr('href', 'mailto:info@techinquiry.org')
        .text('info@techinquiry.org')
    )
    .append('.')

  let supportText = $('<p>').addClass('card-text').css('max-width', '1400px')
  cardBody.append(supportText)
  supportText
    .append(
      $('<span>').css('font-weight', '700').text('Want to support our work? ')
    )
    .append(
      'Because Tech Inquiry does not accept funding from government agencies, ' +
        'corporations, or billionaire-led foundations, we operate on a tight ' +
        'budget. Even small '
    )
    .append(
      $('<a>')
        .addClass('btn btn-primary')
        .css('padding', '0')
        .css('font-size', '100%')
        .attr('href', 'https://techinquiry.org/donating.pdf')
        .text('donations')
    )
    .append(' make a big difference to our org.')

  holder.modal('show')
}

function Footer(parentID, explorerRoute) {
  // We will display the footer in a modal instead.
  let footerPointerID = 'footer-pointer'
  let footerModalID = 'footer-modal'
  createModalDiv(footerModalID)

  let card = $('<div>')
    .addClass('card')
    .css('font-size', '150%')
    .css('width', '95%')
    .css('margin', '0 auto')
    .css('bottom', '0')
    .css('padding', '1em')
    .css('background-color', 'rgba(245, 245, 245)')
    .css('z-index', '5')
  $(`#${parentID}`).append(card)

  let cardBody = $('<div>')
    .addClass('card-body')
    .css('align', 'left')
    .css('padding', '1em')
  card.append(cardBody)

  let cardText = $('<p>')
    .addClass('card-text')
    .attr('id', footerPointerID)
    .css('max-width', '1400px')
  cardText.append(
    'Click here for more information about this website and your privacy'
  )
  cardBody.append(cardText)

  $(`#${footerPointerID}`).on('click', function () {
    // DO_NOT_SUBMIT
    console.log('Clicked footer pointer')
    FillFooter(footerModalID, explorerRoute)
  })
}
