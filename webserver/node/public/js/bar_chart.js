function BarChart(amountTypes, config) {
  function wrap(text, width, fontSize, fontWeight, maxLineChars) {
    if (text.length <= maxLineChars) {
      return
    }
    text.each(function () {
      let text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr('y'),
        dy = 1.0,
        tspan = text
          .text(null)
          .append('tspan')
          .attr('x', width / 2)
          .attr('y', y)
          .attr('font-size', fontSize)
          .attr('font-weight', fontWeight)
          .attr('dy', dy + 'em')
      while ((word = words.pop())) {
        line.push(word)
        tspan.text(line.join(' '))
        if (tspan.text().length > maxLineChars) {
          line.pop()
          tspan.text(line.join(' '))
          line = [word]
          tspan = text
            .append('tspan')
            .attr('x', width / 2)
            .attr('y', y)
            .attr('font-size', fontSize)
            .attr('font-weight', fontWeight)
            .attr('dy', ++lineNumber * lineHeight + dy + 'em')
            .text(word)
        }
      }
    })
  }

  // Whether we determined that the screen was small.
  const smallScreen = screen.width <= config.smallScreenWidth

  // If necessary, truncate for small screens.
  const titleFontSize = smallScreen
    ? config.titleFontSizeSmallScreen
    : config.titleFontSize
  const yAxisTitleFontSize = smallScreen
    ? config.yAxisTitleFontSizeSmallScreen
    : config.yAxisTitleFontSize

  const maxTitleLineCharsBigScreen = 65
  const maxTitleLineCharsSmallScreen = 48
  const maxTitleLineChars = smallScreen
    ? maxTitleLineCharsSmallScreen
    : maxTitleLineCharsBigScreen
  if (smallScreen) {
    for (var key in amountTypes) {
      let data = amountTypes[key]['data']
      if (amountTypes[key].sliceFromEnd) {
        sliceFromEnd(data, config.maxSmallScreenLabels)
      } else {
        data = data.slice(0, config.maxSmallScreenLabels)
      }
    }
  }

  // Get the maximum label length in characters to determine the left and
  // bottom margins.
  let maxLabelLength = 1
  for (var key in amountTypes) {
    const data = amountTypes[key]['data']
    if (data.length) {
      maxLabelLength = Math.max(
        d3.max(data, (item) => item.label.length),
        maxLabelLength
      )
    }
  }

  // Determine the bottom margin.
  const marginBottomCandidate = config.marginPerChar * maxLabelLength
  const marginBottom = Math.max(marginBottomCandidate, config.minMarginBottom)

  // Determine the left margin.
  const marginLeftCandidate =
    config.marginLeftOffset + config.marginPerChar * maxLabelLength
  const marginLeft = Math.min(
    Math.max(marginLeftCandidate, config.minMarginLeft),
    config.maxMarginLeft
  )

  const chartWidth = smallScreen ? config.smallChartWidth : config.chartWidth
  const chartHeight = marginBottom + config.innerChartHeight

  margin = {
    top: config.marginTop,
    right: config.marginRight,
    bottom: marginBottom,
    left: marginLeft,
  }

  // Set the dimensions and margins of the graph.
  const width = chartWidth - (margin.left + margin.right)
  const height = chartHeight - (margin.top + margin.bottom)

  let lastTypes = []
  let chart = d3
    .select(config.chartID)
    .append('svg')
    .attr('width', chartWidth)
    .attr('height', chartHeight)
    .append('g')
    .attr('transform', 'translate(' + margin.left + ', ' + margin.top + ')')

  // Draw a thin black border.
  // In some cases, the bottom border does not show up if we draw a rectangle
  // whose dimensions are exactly that of the chart, so we decrease by one
  // pixel to avoid clipping.
  d3.select(config.chartID)
    .select('svg')
    .append('rect')
    .attr('x', 0)
    .attr('y', 0)
    .attr('height', chartHeight - 1)
    .attr('width', chartWidth - 1)
    .style('stroke', 'black')
    .style('fill', 'none')
    .style('stroke-width', 1)

  let titleObj = chart.append('text')

  let xScale = d3.scaleBand().range([0, width]).padding(0.2)
  let xAxis = chart
    .append('g')
    .attr('transform', 'translate(0, ' + height + ')')

  let yScale = d3.scaleLinear().range([height, 0])
  let yAxis = chart.append('g')
  let yAxisTitleObj = chart.append('text')

  let tooltip = d3.select(config.chartID).append('div').attr('class', 'toolTip')
  let formatValue = d3.format('.3s')

  function updateChart(types) {
    const typesKey = types.length == 1 ? types[0] : JSON.stringify(types)
    if (types === lastTypes || !(typesKey in amountTypes)) {
      return
    }

    const data = amountTypes[typesKey]['data']
    const chartTitle = amountTypes[typesKey]['title']

    titleObj
      .text(chartTitle)
      .attr('x', width / 2)
      .attr('y', -0.9 * margin.top)
      .attr('text-anchor', 'middle')
      .attr('font-size', titleFontSize)
      .attr('font-weight', config.titleFontWeight)
      .call(
        wrap,
        width,
        titleFontSize,
        config.titleFontWeight,
        maxTitleLineChars
      )

    yAxisTitleObj
      .text(config.yAxisTitle)
      .attr(
        'transform',
        'translate(' + -0.7 * margin.left + ',' + height / 2 + ')rotate(-90)'
      )
      .attr('text-anchor', 'middle')
      .attr('font-size', yAxisTitleFontSize)
      .attr('font-weight', config.yAxisTitleFontWeight)

    const yMin = Math.min(
      d3.min(data, (item) => item.amount),
      0
    )
    const yMax = Math.max(
      d3.max(data, (item) => item.amount),
      0
    )

    xScale.domain(
      data.map(function (d) {
        return d.label
      })
    )
    xAxis
      .call(d3.axisBottom(xScale))
      .selectAll('text')
      .attr('transform', 'translate(-10,0)rotate(-45)')
      .style('text-anchor', 'end')
      .style('font-size', config.xLabelFontSize)
      .style('font-weight', config.xLabelFontWeight)

    yScale.domain([yMin, yMax])
    yAxis
      .transition()
      .duration(500)
      .call(d3.axisLeft(yScale).tickFormat(config.xLabelFormat))
      .selectAll('text')
      .style('font-size', config.yLabelFontSize)
      .style('font-weight', config.yLabelFontWeight)

    if (lastTypes === []) {
      chart
        .selectAll('rect')
        .data(data)
        .enter()
        .append('rect')
        .attr('x', function (d) {
          return (
            xScale(d.label) +
            (xScale.bandwidth() -
              Math.min(xScale.bandwidth(), config.maxBarWidth)) /
              2
          )
        })
        .attr('y', function (d) {
          return yScale(Math.max(d.amount, 0))
        })
        .attr('width', Math.min(xScale.bandwidth(), config.maxBarWidth))
        .attr('height', function (d) {
          const propHeight = Math.abs(yScale(d.amount) - yScale(0))
          return Math.max(propHeight, config.minBarHeight)
        })
        .attr('stroke', config.barBorderColor)
        .attr('stroke-width', config.barBorderWidth)
        .attr('fill', config.barColor)
    }

    let rects = chart.selectAll('rect').data(data)

    // Detach the listeners and then reattach after the transition completes.
    rects.on('mouseleave, mousemove, mouseover, mouseout', null)

    rects.exit().remove()

    rects
      .enter()
      .append('rect')
      .merge(rects)
      .transition()
      .duration(500)
      .attr('x', function (d) {
        return (
          xScale(d.label) +
          (xScale.bandwidth() -
            Math.min(xScale.bandwidth(), config.maxBarWidth)) /
            2
        )
      })
      .attr('y', function (d) {
        return yScale(Math.max(d.amount, 0))
      })
      .attr('width', Math.min(xScale.bandwidth(), config.maxBarWidth))
      .attr('height', function (d) {
        const propHeight = Math.abs(yScale(d.amount) - yScale(0))
        return Math.max(propHeight, config.minBarHeight)
      })
      .attr('stroke', config.barBorderColor)
      .attr('stroke-width', config.barBorderWidth)
      .attr('fill', config.barColor)
      .on('end', function () {
        chart
          .selectAll('rect')
          .on('mouseleave', function (d) {
            tooltip.style('display', 'none').html()
          })
          .on('mousemove', function (d) {
            const bounds = document
              .getElementById(config.relativeParentID)
              .getBoundingClientRect()
            tooltip
              .style(
                'left',
                d3.event.clientX -
                  bounds.left -
                  config.tooltipHorizontalOffset +
                  'px'
              )
              .style(
                'top',
                d3.event.clientY -
                  bounds.top -
                  config.tooltipVerticalOffset +
                  'px'
              )
              .style('display', 'inline-block')
              .html(config.tooltipHTML(d))
          })
          .on('mouseover', function (d) {
            d3.select(this).attr('fill', config.barColorMouseover)
          })
          .on('mouseout', function (d) {
            d3.select(this).attr('fill', config.barColor)
          })
      })

    lastTypes = types
  }

  // Collect all of the select values from selectIDs
  function gatherSelectIDs() {
    if (config.selectID !== undefined) {
      return [config.selectID]
    } else {
      return config.selectIDs
    }
  }

  // Return the values from the selects.
  function gatherSelectValues() {
    const selectIDs = gatherSelectIDs()
    selectValues = []
    selectIDs.forEach(function (selectID) {
      selectValues.push($(selectID).val())
    })
    return selectValues
  }

  // We are purposefully calling D3 outside of the 'document.ready' function
  // since it can take some time for the document to finish loading.
  updateChart(gatherSelectValues())
  $(document).ready(function () {
    const selectIDs = gatherSelectIDs()
    selectIDs.forEach(function (selectID) {
      $(selectID).on('change', function () {
        updateChart(gatherSelectValues())
      })
    })
  })
}
