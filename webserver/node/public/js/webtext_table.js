function WebtextTable(data, containerID) {
  function urlPreview(url, targetLength) {
    if (url.startsWith('https://')) {
      url = url.substring(8, url.length)
    }
    if (url.startsWith('http://')) {
      url = url.substring(7, url.length)
    }
    if (url.startsWith('www.')) {
      url = url.substring(4, url.length)
    }
    return trimLabel(url, targetLength)
  }

  const URL_PREVIEW_LENGTH = 50

  $.ajax({
    url: appendURLPath(EXPLORER_ROUTE, 'api/webtext'),
    data: data,
  }).done(function (result) {
    let filings = result.filings
    let holder = $(`#${containerID}`)
    holder.empty()

    let card = $('<div>').css('font-size', '175%')
    holder.append(card)
    let cardBody = $('<div>').addClass('card-body')
    card.append(cardBody)

    let table = $('<table>').addClass('table')
    cardBody.append(table)

    let thead = $('<thead>')
    table.append(thead)
    thead.append(
      $('<tr>')
        .append($('<th>').attr('scope', 'col').text('Source URL'))
        .append($('<th>').attr('scope', 'col').text('Entities'))
    )

    let tbody = $('<tbody>')
    table.append(tbody)
    filings.forEach(function (filing) {
      let tr = $('<tr>')
      tbody.append(tr)

      let urlTD = $('<td>').append(
        $('<a>')
          .addClass('sourceLink')
          .attr('href', filing.url)
          .attr('alt', filing.url)
          .attr('title', filing.url)
          .text(urlPreview(filing.url, URL_PREVIEW_LENGTH))
      )
      tr.append(urlTD)

      let entitiesTD = $('<td>')
      tr.append(entitiesTD)
      let entityLines = []
      filing.entityAnnotations.forEach(function (annotation) {
        entityLines.push(
          `${annotation.logoHTML}<a class="sourceLink" href="${encodeEntity(
            annotation.text,
            EXPLORER_ROUTE
          )}">${annotation.stylizedText}</a>`
        )
      })
      entitiesTD.append(entityLines.join('<br/>'))
    })
  })
}
