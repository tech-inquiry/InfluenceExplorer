function WebtextTab(tabsState, dataBase) {
  const label = 'webtext'
  const tabLogo = undefined
  const tabTitle = 'Entities / URLs'
  const recordsNote = ''

  FeedTab(
    tabsState.id(),
    tabsState.navID(),
    label,
    tabLogo,
    tabTitle,
    recordsNote
  )

  const tabsID = `${label}-tabs`

  function renderRecordsTab(event, ui) {
    let data = dataBase
    const containerID = `${label}-tab-records-inner`
    WebtextTable(data, containerID)
  }

  function renderTab(event, ui) {
    const recordsTab = 0
    const activeTab = $(`#${tabsID}`).tabs('option', 'active')
    if (activeTab == recordsTab) {
      renderRecordsTab(event, ui)
    }
  }

  tabsState.addTab({ id: tabsID, render: renderTab })
}
