function getSearchURL(hint) {
  const SEARCH_PREFIX = appendURLPath(EXPLORER_ROUTE, 'search')

  const startDate = $('#startDate').val()
  const endDate = $('#endDate').val()

  const encodedText = encodeURIComponent(hint).replace(/\//g, '%2F')
  const encodedStartDate = encodeURIComponent(startDate)
  const encodedEndDate = encodeURIComponent(endDate)

  const awardDateSortType = $('#award_date_sort_type').val()
  const useModifiedDate = awardDateSortType === 'Modified Date'

  let urlArgs = [`text=${encodedText}`]
  if (startDate) {
    urlArgs.push(`startDate=${encodedStartDate}`)
  }
  if (endDate) {
    urlArgs.push(`endDate=${encodedEndDate}`)
  }
  if (useModifiedDate) {
    urlArgs.push('useModifiedDate=true')
  }

  const url = `${SEARCH_PREFIX}?${urlArgs.join('&')}`
  return url
}

function getSuggestions(hint) {
  const MAX_RETRIEVED_SUGGESTIONS = 50
  const MAX_SHOWN_SUGGESTIONS = 25

  // Build up the parameters for the autocomplete suggestion query.
  const postBody = {
    hint: hint,
    maxRetrievedSuggestions: MAX_RETRIEVED_SUGGESTIONS,
    maxShownSuggestions: MAX_SHOWN_SUGGESTIONS,
  }

  const startDate = $('#startDate').val()
  const endDate = $('#endDate').val()
  const encodedStartDate = encodeURIComponent(startDate)
  const encodedEndDate = encodeURIComponent(endDate)

  const awardDateSortType = $('#award_date_sort_type').val()
  const useModifiedDate = awardDateSortType === 'Modified Date'

  const SUGGEST_PREFIX = appendURLPath(EXPLORER_ROUTE, 'api/entitySuggestions')
  // Perform the post to the /suggest REST API and replace the contents of the
  // autocomplete box with the results.
  $.get(
    SUGGEST_PREFIX,
    postBody,
    function (items, textStatus) {
      $('.connected_autocomplete a').each(function () {
        $(this).remove()
      })

      // Switch the autocomplete box's styling from 'off' to 'on'.
      $('.connected_autocomplete')
        .removeClass('connected_autocomplete_off')
        .addClass('connected_autocomplete_on')

      const hintURL = getSearchURL(hint)
      $('.connected_autocomplete').append(
        $('<a>')
          .attr('href', hintURL)
          .append(
            $('<span>')
              .addClass('connected_autocomplete_span')
              .append(
                $('<span>').css('font-style', 'italic').text('Search for ')
              )
              .append(escapeHTML(hint))
          )
      )

      const getURL = function (vendor) {
        const VENDOR_PREFIX = appendURLPath(EXPLORER_ROUTE, 'vendor')
        const encodedVendor = encodeURIComponent(vendor).replace(/\//g, '%2F')
        var url = `${VENDOR_PREFIX}/${encodedVendor}/`
        let urlArgs = []
        if (startDate) {
          urlArgs.push(`startDate=${encodedStartDate}`)
        }
        if (endDate) {
          urlArgs.push(`endDate=${encodedEndDate}`)
        }
        if (useModifiedDate) {
          urlArgs.push('useModifiedDate=true')
        }
        if (urlArgs.length) {
          url += `?${urlArgs.join('&')}`
        }
        return url
      }

      const indentationRate = 0.85
      $.each(items, function (index, item) {
        const numAncestors = item.simpleAncestors.length
        for (var i = 0; i < numAncestors; ++i) {
          const ancestor = item.simpleAncestors[numAncestors - 1 - i]
          const ancestorURL = getURL(ancestor.vendor)
          const indentation = i * indentationRate
          let linkObj = $('<a>')
            .attr('href', ancestorURL)
            .css('padding-top', '0.25rem')
            .css('padding-bottom', '0.25rem')
          if (i == 0) {
            linkObj
              .css('border-top', '1px solid grey')
              .css('border-radius', '15px')
          }
          $('.connected_autocomplete').append(linkObj)
          let span = $('<span>')
            .addClass('connected_autocomplete_span')
            .css('font-size', '85%')
            .css('color', 'grey')
            .css('margin-left', indentation.toString() + 'em')
          linkObj.append(span)
          if (ancestor.logoLink) {
            span
              .append(
                $('<img>')
                  .attr('src', ancestor.logoLink)
                  .css('vertical-align', 'sub')
                  .css('margin-right', '0.75em')
                  .css('height', '1.25em')
              )
              .append(escapeHTML(ancestor.stylizedVendor))
          } else {
            span.text(ancestor.stylizedVendor)
          }
        }
        const url = getURL(item.vendor)
        const indentation = numAncestors * indentationRate
        let linkObj = $('<a>').attr('href', url)
        if (numAncestors) {
          linkObj.css('padding-top', '0.25rem')
        } else {
          linkObj
            .css('border-top', '1px solid grey')
            .css('border-radius', '15px')
        }
        $('.connected_autocomplete').append(linkObj)
        let spanObj = $('<span>')
          .addClass('connected_autocomplete_span')
          .css('margin-left', indentation.toString() + 'em')
        linkObj.append(spanObj)
        if (item.logoLink) {
          spanObj
            .append(
              $('<img>')
                .css('vertical-align', 'sub')
                .css('margin-right', '1em')
                .css('height', '1.75em')
                .attr('src', item.logoLink)
            )
            .append(escapeHTML(item.stylizedVendor))
        } else {
          spanObj.text(item.stylizedVendor)
        }
      })
    },
    'json'
  )
}

$(document).ready(function () {
  var previous = ''

  function clearSuggestions() {
    previous = ''
    $('.connected_autocomplete').empty()

    // Switch the autocomplete box's styling from 'on' to 'off'.
    $('.connected_autocomplete')
      .removeClass('connected_autocomplete_on')
      .addClass('connected_autocomplete_off')
  }

  // Change the placeholder text if a new entity type is selected.
  $('#entity').attr('placeholder', ' Choose a company or search with terms')

  // On submission of the searchbar input text.
  $('.connected_searchbar#entity').keypress(function (event) {
    const keyCode = event.keyCode ? event.keyCode : event.which
    const pressedEnter = keyCode == '13'
    if (pressedEnter) {
      const text = $('#entity').val()
      const url = getSearchURL(text)
      location.href = url
    }
  })

  // Clear autocomplete if we click on the parent div (outside of search box).
  $('.connected_container').click(clearSuggestions)

  // Clear autocomplete if we click on the title bar.
  $('.title').click(clearSuggestions)

  // Run the start and end date pickers.
  var startDate = $('#startDate')
  startDate.datepicker({
    dateFormat: 'yy/mm/dd',
    startDate: undefined,
  })

  var endDate = $('#endDate')
  endDate.datepicker({
    dateFormat: 'yy/mm/dd',
    startDate: undefined,
  })

  // Retrieve suggestions if a new input is found.
  $('.connected_searchbar#entity').on('click change paste keyup', function () {
    const hint = $(this).val()
    if (previous !== '' && hint == '') {
      clearSuggestions()
    } else if (hint !== '' && hint !== previous) {
      previous = hint
      getSuggestions(hint)
    }
  })
})
