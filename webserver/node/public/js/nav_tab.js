function NavTab(parentID, label, logo, title) {
  let link = $('<a>')
    .addClass('nav-link')
    .attr('id', `${label}-link`)
    .attr('href', `#filing-tab-${label}`)
  if (logo !== undefined) {
    link.append(
      $('<img>')
        .attr('src', logo)
        .css('height', '1.25em')
        .css('vertical-align', 'sub')
        .css('margin-right', '0.5em')
    )
  }
  link.append(title)

  let item = $('<li>').addClass('nav-item').append(link)
  $(`#${parentID}`).append(item)
}
