// Turn the address object into an HTML label.
function mapAddressLabel(address) {
  let addressLabel = ''
  if (address.streetAddress) {
    addressLabel += address.streetAddress
  }
  if (address.city && address.state && address.zipcode) {
    if (addressLabel) {
      addressLabel += '<br/>'
    }
    addressLabel += `${address.city}, ${address.state} ${address.zipcode}`
  } else if (address.city && address.state) {
    if (addressLabel) {
      addressLabel += '<br/>'
    }
    addressLabel += `${address.city}, ${address.state}`
  } else if (address.city && address.zipcode) {
    if (addressLabel) {
      addressLabel += '<br/>'
    }
    addressLabel += `${address.city}, ${address.zipcode}`
  } else if (address.state && address.zipcode) {
    if (addressLabel) {
      addressLabel += '<br/>'
    }
    addressLabel += `${address.state} ${address.zipcode}`
  }
  if (address.country) {
    if (addressLabel) {
      addressLabel += '<br/>'
    }
    addressLabel += address.country
  }
  return addressLabel
}

function mapPopupLabel(address, defaultNameLabel) {
  let popupLabel = '<span style="font-size: 170%;"><b>'
  if ('url' in address) {
    popupLabel += `<a href="${address.url}">`
  }
  if ('name' in address) {
    popupLabel += address.name
  } else {
    popupLabel += defaultNameLabel
  }
  if ('url' in address) {
    popupLabel += '</a>'
  }
  popupLabel +=
    '</b></span><br/><span style="font-size: 130%;">' + mapAddressLabel(address)
  if ('phoneNumber' in address && address['phoneNumber']) {
    popupLabel += `<br/>Phone: ${formatPhoneNumber(address['phoneNumber'])}`
  }
  if ('faxNumber' in address && address['faxNumber']) {
    popupLabel += `<br/>Fax: ${formatPhoneNumber(address['faxNumber'])}`
  }
  popupLabel += '</span>'

  return popupLabel
}

// Build and return a leaflet map.
function leafletMapFromAddresses(
  mapID,
  addresses,
  rsfCountryRGBs,
  defaultNameLabel,
  singleMarkerZoomLevel = 7
) {
  // Set up the map bounds.
  markers = []
  addresses.forEach(function (address) {
    if (!address.latitude || !address.longitude) {
      return
    }
    const marker = L.marker([address.latitude, address.longitude])
    markers.push(marker)
  })

  let leafletMap = undefined
  if (markers.length <= 1) {
    const mapCenterLatitude = addresses[0].latitude
    const mapCenterLongitude = addresses[0].longitude
    leafletMap = L.map(mapID).setView(
      [mapCenterLatitude, mapCenterLongitude],
      singleMarkerZoomLevel
    )
  } else {
    const markerGroup = new L.featureGroup(markers)
    leafletMap = L.map(mapID).fitBounds(markerGroup.getBounds())

    let legend = L.control({ position: 'topright' })
    legend.onAdd = function (map) {
      const legendLabel =
        '<a href="https://rsf.org/en/ranking">Press Freedom Score</a>'
      const legendItems = [
        { background: '#FF0000', label: '&ge;50 (Least Free)' },
        { background: '#CC0033', label: '40' },
        { background: '#990066', label: '30' },
        { background: '#660099', label: '20' },
        { background: '#3300CC', label: '10' },
        { background: '#0000FF', label: '0 (Most Free)' },
      ]

      let div = L.DomUtil.create('div', 'legend')
      div.innerHTML += `<h4>${legendLabel}</h4>`
      legendItems.forEach(function (legendItem) {
        div.innerHTML +=
          `<i style="background: ${legendItem.background}"></i>` +
          `<span>${legendItem.label}</span><br/>`
      })

      return div
    }
    legend.addTo(leafletMap)
  }
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; OpenStreetMap contributors',
  }).addTo(leafletMap)

  addresses.forEach(function (address) {
    if (!address.latitude || !address.longitude) {
      return
    }

    const markerPoint = [address.latitude, address.longitude]
    const rsfRGB = address.country
      ? rsfCountryRGBs[address.country.toLowerCase()]
      : undefined
    const rgbStr =
      rsfRGB !== undefined
        ? `rgb(${rsfRGB[0]},${rsfRGB[1]},${rsfRGB[2]})`
        : '#333355'
    const marker =
      markers.length == 1
        ? L.marker(markerPoint)
        : L.circleMarker(markerPoint)
            .setRadius(10)
            .setStyle({ color: rgbStr, fillOpacity: 0.4 })

    const popupLabel = mapPopupLabel(address, defaultNameLabel)
    marker.addTo(leafletMap).bindPopup(popupLabel)
    if (markers.length == 1 || address.is_headquarters) {
      marker.openPopup()
    }
  })
  return leafletMap
}
