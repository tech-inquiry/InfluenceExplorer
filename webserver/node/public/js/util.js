function prefixURL(url) {
  if (!url) {
    return url
  }

  if (
    url.toLowerCase().startsWith('http://') ||
    url.toLowerCase().startsWith('https://')
  ) {
    return url
  } else {
    return `http://${url}`
  }
}

function escapeHTML(string) {
  const htmlEscapeMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;',
  }

  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return htmlEscapeMap[s]
  })
}

function unescapeHTML(str) {
  return String(str)
    .replace(/&amp;/g, '&')
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>')
    .replace(/&quot;/g, '"')
    .replace(/&#39;/g, "'")
    .replace(/&#x2F;/g, '/')
    .replace(/&#x60;/g, '`')
    .replace(/&#x3D;/g, '=')
}

function appendURLPath(urlBase, urlAddition, absolute = true) {
  urlBase = urlBase.replace(/\/$/, '')
  urlAddition = urlAddition.replace(/^\//, '')
  if (absolute || urlBase) {
    return urlBase + '/' + urlAddition
  } else {
    return urlAddition
  }
}

function trimLabel(label, maxLength) {
  if (label.length > maxLength) {
    label = label.slice(0, maxLength - 3) + '...'
  }
  return label
}

function sliceFromEnd(data, maxLength) {
  if (data.length > maxLength) {
    data = data.slice(data.length - maxLength, maxLength)
  }
}

// Formats a Date as string of form '[month] [date], [year]'.
function dateWithoutTime(date) {
  if (date == null || isNaN(date.getTime())) {
    return 'N/A'
  }
  const month = new Intl.DateTimeFormat('en-US', { month: 'long' }).format(date)
  return month + ' ' + date.getDate() + ', ' + date.getFullYear()
}

function dateWithoutTimeLex(date) {
  if (!date) {
    return ''
  }
  const tokens = date.split('T')
  if (tokens.length) {
    return tokens[0]
  } else {
    return ''
  }
}

// Formats a number into a string with two digits after the period and a
// comma between each three signifant digits to the left of it.
function numberWithCommas(x) {
  if (isNaN(x) || x == undefined) {
    return ''
  } else {
    // Even if the input is loaded via TypeORM as a 'number', if the
    // column is nullable, we must still do a conversion to ensure the
    // member function 'toFixed' exists.
    return Number(x)
      .toFixed(2)
      .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }
}

function formatPhoneNumber(phoneNumberString) {
  var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
  var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
  if (match) {
    return '(' + match[1] + ') ' + match[2] + '-' + match[3]
  }
  return phoneNumberString
}

// Formats a GB-COH Company House ID to 8 digits, prepending with zeros.
function prependZerosToGBCOH(id) {
  return ('00000000' + id).slice(-8)
}

function encodeURIComponentIncludingSlashes(component) {
  return encodeURIComponent(component.replace(/\//g, '%2F'))
}

function encodeEntity(name, explorerRoute) {
  return appendURLPath(
    explorerRoute,
    `vendor/${encodeURIComponentIncludingSlashes(name)}/`
  )
}

function encodeSearch(term, explorerRoute) {
  return appendURLPath(
    explorerRoute,
    `search/?text=${encodeURIComponentIncludingSlashes(term)}`
  )
}

function appendEntityAnnotationLines(entityLines, entity, name, prefix = null) {
  const colonPrefix = prefix ? `${prefix}: ` : ''
  if (entity == null) {
    entityLines.push(`${colonPrefix}${name}`)
    return
  }

  entityLines.push(
    `${colonPrefix}${entity.logoHTML} ` +
      `<a href="${encodeEntity(entity.text, EXPLORER_ROUTE)}">` +
      `${entity.stylizedText}</a>`
  )
  if (entity.text != name.toLowerCase()) {
    entityLines.push(`<span style="font-size: 90%;">As written: ${name}</span>`)
  }
}

function cityStateZipLine(city, state, zipcode) {
  let line = ''
  if (city) {
    line = city
  }
  if (state) {
    if (line) {
      line += ', '
    }
    line += state
  }
  if (zipcode) {
    if (line) {
      line += ' '
    }
    line += zipcode
  }
  return line
}

function formatAddress(address) {
  let lines = []
  if ('streetLines' in address) {
    for (const line of address['streetLines']) {
      if (line && line.trim()) {
        lines.push(line.trim())
      }
    }
  }
  if (address.city || address.state || address.postalCode) {
    lines.push(
      cityStateZipLine(address.city, address.state, address.postalCode)
    )
  }
  if (address.country) {
    lines.push(address.country)
  }
  if (address.email) {
    lines.push(`Email: ${address.email}`)
  }
  if (address.phone) {
    lines.push(`Phone: ${formatPhoneNumber(address.phone)}`)
  }
  if (address.fax) {
    lines.push(`Fax: ${formatPhoneNumber(address.fax)}`)
  }
  return lines.join('<br/>')
}

function lobbyistNameFromObject(lobbyist) {
  let pieces = []

  if (lobbyist.prefix_display) {
    pieces.push(lobbyist.prefix_display)
  }
  if (lobbyist.first_name) {
    pieces.push(lobbyist.first_name)
  }
  if (lobbyist.nickname) {
    pieces.push(lobbyist.nickname)
  }
  if (lobbyist.last_name) {
    pieces.push(lobbyist.last_name)
  }
  if (lobbyist.suffix_display) {
    pieces.push(lobbyist.suffix_display)
  }

  return pieces.join(' ')
}

function usLobbyingAddressFromObject(entity) {
  let address = {
    streetLines: [],
    city: entity.city,
    state: entity.state_display,
    postalCode: entity.zip,
    country: entity.country_display,
  }
  if (entity.address) {
    address.streetLines.push(entity.address)
  } else if (entity.address_1) {
    address.streetLines.push(entity.address_1)
    if (entity.address_2) {
      address.streetLines.push(entity.address_2)
    }
    if (entity.address_3) {
      address.streetLines.push(entity.address_3)
    }
    if (entity.address_4) {
      address.streetLines.push(entity.address_4)
    }
  }
  return formatAddress(address)
}

function calaccessLobbyingAddressFromObject(entity) {
  let address = {
    city: entity.city,
    state: entity.state,
    postalCode: entity.zipcode,
    phone: entity.phone,
    fax: entity.fax,
  }
  return formatAddress(address)
}

function appendKeyValueToTableBody(tbody, keyHTML, valueHTML) {
  tbody.append(
    $('<tr>')
      .append($('<th>').attr('scope', 'row').html(keyHTML))
      .append($('<td>').html(valueHTML))
  )
}

// Iterate over the tokens and create a new line at each first break crossing
// the splitLength.
function splitLabelIntoLines(label, splitLength) {
  if (label.length <= splitLength) {
    return [label]
  }
  const numLinesEst = Math.ceil(label.length / splitLength)
  const targetLength = label.length / numLinesEst

  const tokens = label.split(/[\s\-]/)
  let lines = []
  let lineStart = 0
  let lineLength = 0
  tokens.forEach(function (token, index) {
    if (lineLength >= targetLength) {
      lines.push(tokens.slice(lineStart, index).join(' '))
      lineStart = index
      lineLength = 0
    }
    lineLength += token.length
  })
  lines.push(tokens.slice(lineStart, tokens.length).join(' '))
  return lines
}

// Creates a hidden modal div with the given ID as an append to 'body'.
function createModalDiv(modalID) {
  const modalTitleID = `${modalID}-title`
  let modal = $('<div>')
    .addClass('modal fade')
    .attr('id', modalID)
    .attr('tabindex', '-1')
    .attr('aria-labelledby', modalTitleID)
  $('body').append(modal)

  let modalDialog = $('<div>').addClass('modal-dialog modal-ti')
  modal.append(modalDialog)

  let modalContent = $('<div>').addClass('modal-content')
  modalDialog.append(modalContent)

  modal.modal({ show: false })
}

// Creates a hidden dialog div with the given ID as an append to 'body'.
function createDialogDiv(dialogID) {
  $('body').append($('<div>').attr('id', dialogID).css('display', 'none'))
}

function fillWithResponsiveTable(containerID, tableID, fontSize = '140%') {
  let holder = $(`#${containerID}`)
  holder.empty()
  holder.append(
    $('<div>')
      .addClass('table-responsive')
      .append(
        $('<table>')
          .attr('id', tableID)
          .addClass('table table-striped table-bordered')
          .css('font-size', fontSize)
      )
  )
}
