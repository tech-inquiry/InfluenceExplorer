#!/usr/bin/env bash

# TODO: if ever backgrounded, kill prior background instances of onchange that calls this
# kill -9 $(pgrep -f node_modules/.bin/onchange | grep -v $$)

echo 'Change detected. Rolling out new version.'
echo 'Scaling up temporary instance.'
./runpm2.sh scale explorer 2
./runpm2.sh reload all
echo 'Scaling back down.'
./runpm2.sh scale explorer 1
echo 'deploy complete.'
