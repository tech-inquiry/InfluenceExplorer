The client-side webserver/node/dist/main.js bundle can be generated the following command
It assumes your working directory is `webserver/node`.

for local dev:

```
npm run build-fe:dev
```

for production:

```
npm run build-fe:prod
```
