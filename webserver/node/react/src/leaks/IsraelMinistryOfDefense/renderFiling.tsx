import * as React from 'react'

import { FilingWithTags } from '../../FilingWithTags'
import * as util from '../../utilities/util'

const kArchivePrefix = '/israel-leaks/defense'

export class Email extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/il/leak/defense')
    super.setUniqueKeys(['email_id'])
  }

  render() {
    const filing = this.state.filing

    const header = (
      <p className="card-text">
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    const emailID = filing.email_id
    const token = util.retrieveToken()

    let url = `${kArchivePrefix}/${emailID}.html`
    if (token) {
      url += `?token=${token}`
    }

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2">
            <code className="text-dark">Email {filing.email_id}</code>
          </h3>
          {header}
          <iframe
            src={url}
            style={{ height: '1024px', width: '100%' }}
          ></iframe>
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Email filing={filing} />
}
