import * as React from 'react'

import { FilingFeed } from '../FilingFeed'

import { kLeakPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import * as renderFiling from './Teixeira/renderFiling'
import * as util from '../utilities/util'

const kArchivePrefix = '/teixeira'

export class Teixeira extends React.Component<any, any> {
  urlBase: string
  maxTextLength: number
  tableID: string
  label: string
  title: any
  columns: any[]

  constructor(props) {
    super(props)
    this.urlBase = appConfig.apiURL + '/' + kLeakPaths['Teixeira'].get

    this.maxTextLength = 256
    this.tableID = 'ti-table-teixeira'
    this.title = (
      <div>
        <p className="ti-compact">
          <span>
            <img src="/logos/discord_inc.png" className="dropdown-flag" />
            Airman Teixeira leaks
          </span>
        </p>
        <p className="ti-compact text-muted lh-1" style={{ fontSize: '80%' }}>
          <small>
            Tech Inquiry has begun to index a manual transcription of the text
            from the Airman Teixeira leaks from a Discord forum, as
            intermediated by Distributed Denial of Secrets at{' '}
            <a href="https://ddosecrets.com/article/airman-teixeira-leaks">
              ddosecrets.com/article/airman-teixeira-leaks
            </a>
            .
          </small>
        </p>
      </div>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)
    this.renderItem = this.renderItem.bind(this)

    this.label = this.props.label ? this.props.label : 'imoj'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Reference ID' },
      { title: 'Text' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex, haveSubawards) {
    const referenceID = filing.reference_id
    const text = filing.text
    const entireFiling = JSON.stringify(filing)

    return [filingIndex, referenceID, text, entireFiling]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  renderItem(filing) {
    return renderFiling.renderFiling(filing)
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={this.renderItem}
      />
    )
  }
}
