import * as React from 'react'

import { Cablegate } from './Cablegate'
import { IDF } from './IDF'
import { MinistryOfDefense } from './IsraelMinistryOfDefense'
import { MinistryOfJustice } from './IsraelMinistryOfJustice'
import { Teixeira } from './Teixeira'
import { kLeakPaths } from '../utilities/api'
import * as ui from '../utilities/ui'
import { coerce } from '../utilities/util'

class LeaksRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = coerce(this.props.label, 'leaks')
    this.sourceLabel = this.sourceLabel.bind(this)
    this.sourceModalId = this.sourceModalId.bind(this)
  }

  sourceLabel(source) {
    return `${this.label}-${source}`
  }

  sourceModalId(source) {
    return ui.modalId(this.sourceLabel(source))
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    const isEntity = profile != undefined
    const sources = this.props.sources

    const that = this

    const capImage = (
      <div
        className="mx-auto mb-0 ti-cap-img"
        style={{
          opacity: 1.0,
          height: '100px',
          maxWidth: '250px',
          backgroundImage:
            'url("https://techinquiry.org/logos/diplomatic_cables.png")',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: '50% 50%',
          backgroundSize: 'contain',
        }}
      ></div>
    )

    const footerText = isEntity ? (
      <>
        Tech Inquiry has emails for{' '}
        <span className="fst-italic">
          {profile.entityAnnotations[info.query].stylizedText}
        </span>{' '}
        from{' '}
        <span className="fw-bold">
          {sources.length + ' '}
          {sources.length > 1 ? 'sources' : 'source'}
        </span>
        .
      </>
    ) : (
      <>
        Tech Inquiry has emails for{' '}
        <span className="fst-italic">{info.query}</span> from{' '}
        <span className="fw-bold">
          {sources.length + ' '}
          {sources.length > 1 ? 'sources' : 'source'}
        </span>
        .
      </>
    )

    // We will place all of the modal objects up top.
    let enabledSources = [
      'Cablegate',
      'Israel Defense Forces',
      'Israel Ministry of Defense',
      'Israel Ministry of Justice',
      'Teixeira',
    ]

    function sourceToItem(source, index) {
      return ui.dropdownSource(
        source,
        index,
        enabledSources,
        kLeakPaths,
        that.sourceModalId
      )
    }

    return (
      <ui.RowHelper
        capImage={capImage}
        button={ui.getCardDropdownButton(
          'Public Interest Leaks',
          `${this.label}-dropdownMenuButton`,
          sources.map(sourceToItem)
        )}
        footerText={footerText}
      />
    )
  }
}

class LeaksModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = coerce(this.props.label, 'leaks')
    this.sourceLabel = this.sourceLabel.bind(this)
  }

  sourceLabel(source) {
    return `${this.label}-${source}`
  }

  render() {
    const info = this.props.info
    const isEntity = info.profile != undefined
    return (
      <>
        <Cablegate
          label={this.sourceLabel('Cablegate')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <IDF
          label={this.sourceLabel('Israel Defense Forces')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <MinistryOfDefense
          label={this.sourceLabel('Israel Ministry of Defense')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <MinistryOfJustice
          label={this.sourceLabel('Israel Ministry of Justice')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <Teixeira
          label={this.sourceLabel('Teixeira')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
      </>
    )
  }
}

export function insertCardAndModal(
  info,
  sources,
  cards: any[],
  modals: any[]
): void {
  if (!sources.length) {
    return
  }
  cards.push(<LeaksRow info={info} sources={sources} />)
  modals.push(<LeaksModal info={info} />)
}
