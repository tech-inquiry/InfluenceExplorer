import * as React from 'react'

import { Accordion, AccordionItem, SimpleAccordionItem } from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

const signatureMap = require('../../../../data/cablegate_signatures.json')

const editorials = {
  '10ASTANA267': {
    date: '2023-02-07',
    note: (
      <span>
        Despite the version of cable 10ASTANA267 released in{' '}
        <a href="https://file.wikileaks.org/file/cablegate/cablegate-201108300212.7z">
          cablegate-201108300212.7z
        </a>{' '}
        having been continually disseminated as official for more than a decade
        (including by WikiLeaks itself, DDoSecrets, and OCCRP), a cursory
        inspection reveals that it is artificial and was overwritten with
        commentary on news coverage of the leak of the original cable. Many
        other cables include clear transcription errors -- presumably from
        Optical Character Recognition (OCR) -- and others include transposed and
        overlapping, as well as missing, sections. But this is the only instance
        the author is aware of where the entire post-header body of the cable is
        spurious.
      </span>
    ),
  },
}

export function getSubjectFromLines(lines: string[]): string {
  // Our assumption is that the subject spans consecutive non-empty lines and
  // is always followed by an empty line or a line beginning with 'REF:'.
  for (let index = 0; index < lines.length; index += 1) {
    const line = lines[index]
    if (line.startsWith('SUBJECT: ')) {
      const firstLine = line.substring(9).trim()

      if (firstLine.includes(' REF: ')) {
        const tokens = firstLine.split(' REF: ')
        return tokens[0].trim()
      }
      if (firstLine.includes(' Classified By:')) {
        const tokens = firstLine.split(' Classified By:')
        return tokens[0].trim()
      }

      let subjectLines = [firstLine]
      for (let j = index + 1; j < lines.length; j += 1) {
        const nextLine = lines[j].trim()
        if (!nextLine) {
          break
        }

        if (nextLine.startsWith('REF: ')) {
          break
        } else if (nextLine.includes(' REF: ')) {
          const tokens = nextLine.split(' REF: ')
          subjectLines.push(tokens[0].trim())
          break
        }

        if (nextLine.startsWith('Classified By:')) {
          break
        } else if (nextLine.includes(' Classified By:')) {
          const tokens = nextLine.split(' Classified By:')
          subjectLines.push(tokens[0].trim())
          break
        }

        subjectLines.push(nextLine)
      }

      return subjectLines.join(' ')
    }
  }
  return ''
}

export function getSignatureFromLines(
  lines: string[],
  referenceID: string
): string {
  // Certain lines occasionally appear beneath cable signatures. For example,
  // Ralph Frank's 04ZAGREB1483 cable has the line 'NNNN' two lines beneath
  // 'FRANK'. And cable 09BRUSSELS1647 contains a '.' line beneath the
  // signature 'Kennard'.
  const skipLines = [
    '"',
    '.',
    '...',
    ': NOT PASSED TO ABOVE ADDRESSEE(S)',
    '_____________________________________________ ____________________',
    '- 1 -',
    '=======================CABLE ENDS============================',
    '#',
    '/',
    '^',
    '>',
    '1',
    '2',
    '3',
    '4',
    '4.',
    '5',
    '6',
    '10',
    'A',
    'AND',
    'BRUSSELS 00000659 004 OF 004',
    'comment.',
    'Comment.',
    'CONFIDENTIAL',
    'D',
    'EC-LXI/APP_WP 3.2, APPENDIX A',
    'End Cable Text',
    'ND',
    'NNNN',
    'PAGE 04 HANOI 01882 070254Z',
    'RLAND',
    'SIPDIS',
    'UNCLASSIFIED',
    'UNCLASSIFIED 2',
    'UNCLASSIFIED 3',
  ]
  const skipBegins = [
    "“Visit Ankara's Classified Web Site at",
    '"Visit Ankara\'s Classified Web Site at',
    'gov.gov/wiki/Portal',
    'http://www.intelink.s gov.gov/wiki/',
    'http://www.state.sgov.gov/',
    'To view the entire SMART message, go to URL',
    "Visit Embassy Singapore's",
  ]

  if (signatureMap.hasOwnProperty(referenceID)) {
    return signatureMap[referenceID]
  }

  // In cables such as 10MANAMA15, the signature (i.e., ERELI) follows
  // quotes.
  //
  // In cables such as 10MADRID154 and 10KYIV190, the signature
  // (i.e., SOLOMONT and TEFFT) is at the end of the line after a period
  // rather than on its own line.
  //
  // The last line of cable 09MANAMA713 ends with '(BDF notetaker) ERELI',
  // and so we add support for splitting on a closing parenthesis.
  //
  // Cable 08MANAMA592 ends with numerous asterisks followed by ERELI.
  const splitStrings = ['XXX ', '." ', '.) ', '" ', '. ', ') ', '* ']

  const assumedMaxSignatureTokens = 3
  for (let i = lines.length - 1; i >= 0; i -= 1) {
    let line = lines[i].trim()
    if (line) {
      if (skipLines.includes(line)) {
        continue
      }

      let skipLine = false
      for (const skipBegin of skipBegins) {
        if (line.startsWith(skipBegin)) {
          skipLine = true
          break
        }
      }
      if (skipLine) {
        continue
      }

      // Cable 10KYIV241 ends with 'Comment.' and contains no signature.
      if (line == 'Comment.') {
        return ''
      }

      // In cables such as 10MANAMA52, the signature (i.e., ERELI) is at the
      // end of a line directly after 'End comment.'.
      const endCommentVariants = [
        'End comment.',
        'End Comment.',
        'END COMMENT.',
        'End note.)',
        'End text.',
        'End comment',
        'End Comment',
      ]
      for (const endComment of endCommentVariants) {
        if (line.includes(endComment)) {
          const tokens = line.split(endComment)
          const remainder = tokens[tokens.length - 1].trim()
          if (remainder.split(' ').length <= assumedMaxSignatureTokens) {
            return remainder
          } else {
            line = remainder
          }
        }
      }

      for (const splitString of splitStrings) {
        if (line.includes(splitString)) {
          const tokens = line.split(splitString)
          const remainder = tokens[tokens.length - 1].trim()
          if (remainder.split(' ').length <= assumedMaxSignatureTokens) {
            return remainder
          }
        }
      }

      // Cable 10KYIV275 is truncated just after its subject line and ends
      // with "Classified By: Ambassador Jo..."
      if (line.includes('Classified By: ')) {
        return ''
      }

      // Cable 10VIENNA176 has no signature -- though it does name a point of
      // contact for the TIP report -- and ends with
      // "Political Specialist FSN-11: 25 hours".
      if (line.includes('Political Specialist ')) {
        return ''
      }

      if (line.split(' ').length > assumedMaxSignatureTokens) {
        return ''
      }

      // Cable 09MANAMA711 ends with the line '#ERELI', and 08ISLAMABAD3837
      // with '"Patterson', so we strip out all pound signs and quotations and
      // then trim.
      return line
        .replaceAll('#', '')
        .replaceAll('"', '')
        .replaceAll('\\', '')
        .trim()
    }
  }
  return ''
}

export class Cable extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/us/central/cablegate')
    super.setUniqueKeys(['reference_id'])
  }

  render() {
    const filing = this.state.filing

    function entityAnnotationToItem(annotation: any): any {
      const logoEntry = annotation.logo ? (
        <a href={util.getEntityURL(annotation.text)}>
          <img
            className="ti-association-list-entity-logo"
            src={annotation.logo}
          />
        </a>
      ) : (
        <span style={{ fontSize: '1.5rem' }}>
          {annotation.stylizedText.toUpperCase()}
        </span>
      )

      return (
        <div className="row flex-md-row-reverse align-items-center mx-0 px-0 py-3">
          <div className="col-12 col-md-4 align-items-center px-3 bg-light">
            <div className="text-center">{logoEntry}</div>
          </div>
          <div className="col-12 col-md-8">
            <p className="mb-1" style={{ fontSize: '1.5rem' }}>
              <a
                className="text-decoration-none"
                href={util.getEntityURL(annotation.text)}
              >
                {annotation.stylizedText}
              </a>
            </p>
            {annotation.note ? (
              <p className="text-muted mt-1" style={{ fontSize: '1.25rem' }}>
                {annotation.note}
              </p>
            ) : undefined}
          </div>
        </div>
      )
    }

    function renderEditorial() {
      if (!editorials.hasOwnProperty(filing.reference_id)) {
        return undefined
      }
      const editorial = editorials[filing.reference_id]

      const label = 'editorial'
      const title = 'Editorial Note'

      const body = (
        <div>
          <p className="ti-article">
            <strong>[{editorial.date}]</strong> {editorial.note}
          </p>
        </div>
      )

      return <AccordionItem label={label} title={title} body={body} />
    }

    function renderSource() {
      const label = 'source'

      const annotation = filing.annotation.embassy
      const title = `Source: ${annotation.origText}`

      const logoEntry = annotation.logo ? (
        <a href={util.getEntityURL(annotation.text)}>
          <img
            className="ti-association-list-entity-logo"
            src={annotation.logo}
          />
        </a>
      ) : (
        <span style={{ fontSize: '1.5rem' }}>
          {annotation.stylizedText.toUpperCase()}
        </span>
      )

      const sourceFeatures = [
        <ui.TableKeyValue k="Source" v={filing.embassy} />,
      ]

      let attributionFeatures = []
      // We render the attributed authors first.
      for (const annotation of filing.annotation.staffEntities) {
        if (annotation.attributed) {
          attributionFeatures.push(entityAnnotationToItem(annotation))
        }
      }
      for (const annotation of filing.annotation.staffEntities) {
        if (!annotation.attributed) {
          attributionFeatures.push(entityAnnotationToItem(annotation))
        }
      }

      const body = (
        <div>
          <div className="container px-0">
            <div className="row flex-md-row-reverse align-items-center mx-0 px-0 py-3">
              <div className="col-12 col-md-4 align-items-center px-3 bg-light">
                <div className="text-center">{logoEntry}</div>
              </div>
              <div className="col-12 col-md-8">
                <table className="table" style={{ fontSize: '1.5rem' }}>
                  <tbody>{sourceFeatures}</tbody>
                </table>
              </div>
            </div>
          </div>
          {attributionFeatures.length ? (
            <div className="container px-0">{attributionFeatures}</div>
          ) : undefined}
        </div>
      )

      return (
        <AccordionItem label={label} title={title} body={body} closed={true} />
      )
    }

    function renderRecipientEntities() {
      const label = 'recipient-entities'

      let features = []
      for (const annotation of filing.annotation.recipientEntities) {
        features.push(entityAnnotationToItem(annotation))
      }
      if (!features.length) {
        return undefined
      }

      const title = 'Recipients'

      const body = <div className="container px-0">{features}</div>

      return (
        <AccordionItem label={label} title={title} body={body} closed={true} />
      )
    }

    function renderManualEntities() {
      const label = 'manual-entities'

      let features = []
      for (const annotation of filing.annotation.manualEntities) {
        features.push(entityAnnotationToItem(annotation))
      }
      if (!features.length) {
        return undefined
      }

      const title = 'Manually Tagged Entities'

      const body = <div className="container px-0">{features}</div>

      return (
        <AccordionItem label={label} title={title} body={body} closed={true} />
      )
    }

    function renderPreamble() {
      const body = (
        <div className="card">
          <div className="card-body">
            <code
              className="text-start text-dark"
              style={{ fontSize: '1.25rem' }}
            >
              <pre>{filing.preamble}</pre>
            </code>
          </div>
        </div>
      )
      return (
        <AccordionItem
          label="cablePreamble"
          title="Preamble"
          body={body}
          closed={true}
        />
      )
    }

    function renderText() {
      const body = (
        <div className="card">
          <div className="card-body">
            <code
              className="text-start text-dark"
              style={{ fontSize: '1.25rem', width: '70ch' }}
            >
              <pre>{filing.text}</pre>
            </code>
          </div>
        </div>
      )

      return <AccordionItem label="cableText" title="Text" body={body} />
    }

    function wikileaksURL(referenceID: string): string {
      return `https://wikileaks.org/plusd/cables/${referenceID}_a.html`
    }

    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light"
            href={wikileaksURL(filing.reference_id)}
            role="button"
          >
            View on WikiLeaks
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderEditorial())
    accordionItems.push(renderSource())
    accordionItems.push(renderRecipientEntities())
    accordionItems.push(renderManualEntities())
    accordionItems.push(renderPreamble())
    accordionItems.push(renderText())
    let accordion = (
      <Accordion id="cablegate-accordion" items={accordionItems} />
    )

    const lines = filing.text.split('\n')
    const subject = getSubjectFromLines(lines)

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          {subject ? (
            <h3 className="fw-bold lh-1 mb-2">
              <code className="text-dark">{subject}</code>
            </h3>
          ) : undefined}
          <h3 className="fw-bold lh-1 mb-2">
            <code className="text-dark">
              {`${filing.reference_id}, ${filing.date}, Classification: ${filing.classification}`}
            </code>
          </h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Cable filing={filing} />
}
