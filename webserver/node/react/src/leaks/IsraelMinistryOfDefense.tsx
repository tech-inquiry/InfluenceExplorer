import * as React from 'react'

import { FilingFeed } from '../FilingFeed'

import { kLeakPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import * as renderFiling from './IsraelMinistryOfDefense/renderFiling'
import * as util from '../utilities/util'

export class MinistryOfDefense extends React.Component<any, any> {
  urlBase: string
  maxTextLength: number
  tableID: string
  label: string
  title: any
  columns: any[]

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL + '/' + kLeakPaths['Israel Ministry of Defense'].get

    this.maxTextLength = 256
    this.tableID = 'ti-table-imoj'
    this.title = (
      <div>
        <p className="ti-compact">
          <span>
            <img
              src="/logos/israel_ministry_of_defense.svg"
              className="dropdown-flag"
            />
            Israel Ministry of Defense leaks (from 'Anonymous for Justice')
          </span>
        </p>
        <p className="ti-compact text-muted lh-1" style={{ fontSize: '80%' }}>
          <small>
            Tech Inquiry has indexed the files leaked by the hacktivist group
            'Anonymous for Justice', as intermediated by Distributed Denial of
            Secrets at{' '}
            <a href="https://ddosecrets.com/article/israel-ministry-of-defense">
              ddosecrets.com/article/israel-ministry-of-defense
            </a>
            . As noted by DDoSecrets, care should be taken given that the files
            were leaked during an ongoing war.
          </small>
        </p>
      </div>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)
    this.renderItem = this.renderItem.bind(this)

    this.label = this.props.label ? this.props.label : 'imoj'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Email ID' },
      { title: 'Text' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex, haveSubawards) {
    const emailID = filing.email_id
    const text = filing.text_start
    const entireFiling = JSON.stringify(filing)

    return [filingIndex, emailID, text, entireFiling]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  renderItem(filing) {
    return renderFiling.renderFiling(filing)
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={this.renderItem}
      />
    )
  }
}
