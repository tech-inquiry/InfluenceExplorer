import * as React from 'react'

import { FilingFeed } from '../FilingFeed'

import { kLeakPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import * as renderFiling from './cablegate/renderFiling'
import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

export class Cablegate extends React.Component<any, any> {
  urlBase: string
  maxTitleLength: number
  maxDescriptionLength: number
  tableID: string
  label: string
  title: string
  columns: any[]

  constructor(props) {
    super(props)
    this.urlBase = appConfig.apiURL + '/' + kLeakPaths['Cablegate'].get

    this.maxTitleLength = 200
    this.maxDescriptionLength = 256
    this.tableID = 'ti-table-cablegate'
    this.title = 'U.S. State Department cables (Cablegate)'

    this.closeModal = this.closeModal.bind(this)
    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)
    this.renderItem = this.renderItem.bind(this)

    this.label = this.props.label ? this.props.label : 'cablegate'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Time' },
      { title: 'Source' },
      { title: 'Summary' },
      { title: 'Signature' },
      { title: 'Reference ID' },
      { title: 'Classification' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex, haveSubawards) {
    const maxSummaryLength = 200
    const referenceID = filing.reference_id
    const date = filing.date
    const source = filing.embassy
    const classification = filing.classification
    const entireFiling = JSON.stringify(filing)

    // Try to detect the SUBJECT line and fall back to the trimmed preamble
    // if not.
    const lines = filing.text.split('\n')
    const subject = renderFiling.getSubjectFromLines(lines)
    const summary = util.trimLabel(
      subject ? subject : filing.text,
      maxSummaryLength
    )

    const signature = renderFiling.getSignatureFromLines(lines, referenceID)

    return [
      filingIndex,
      date,
      source,
      summary,
      signature,
      referenceID,
      classification,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  closeModal() {
    // Grab the modal created by the FilingFeed and then click.
    document.getElementById(ui.modalCloseButtonId(this.label)).click()
  }

  renderItem(filing) {
    return renderFiling.renderFiling(filing)
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={this.renderItem}
      />
    )
  }
}
