import * as React from 'react'

import { FilingWithTags } from '../../FilingWithTags'
import * as util from '../../utilities/util'

function ddosecretsURL(referenceID: string): string {
  return `https://data.ddosecrets.com/Airman%20Teixeira%20Leaks/${referenceID}.jpg`
}

export class Teixeira extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/us/central/teixeira')
    super.setUniqueKeys(['reference_id'])
  }

  render() {
    const filing = this.state.filing

    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light"
            href={ddosecretsURL(filing.reference_id)}
            role="button"
          >
            View on DDoSecrets
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    const url = `/teixeira/${filing.reference_id}.jpg`

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2">
            <code className="text-dark">
              Image number {filing.reference_id}
            </code>
          </h3>
          {header}
          <iframe
            src={url}
            style={{ height: '1024px', width: '100%' }}
          ></iframe>
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Teixeira filing={filing} />
}
