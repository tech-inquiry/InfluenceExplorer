import * as React from 'react'

import { FilingWithTags } from '../../FilingWithTags'
import * as util from '../../utilities/util'

const kArchivePrefix = '/israel-leaks/idf'

export class Email extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = ['reference_id', 'part']
    super(props)
    super.setEndpoint('/api/il/leak/idf')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing

    const header = (
      <p className="card-text">
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    const referenceID = filing.reference_id
    const leakPart = filing.part
    const token = util.retrieveToken()

    let url = `${kArchivePrefix}/IDF-Part${leakPart + 1}/${referenceID}`
    if (token) {
      url += `?token=${token}`
    }

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2">
            <code className="text-dark">
              Document {filing.reference_id} of leak part {filing.part + 1}
            </code>
          </h3>
          {header}
          <iframe
            src={url}
            style={{ height: '1024px', width: '100%' }}
          ></iframe>
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Email filing={filing} />
}
