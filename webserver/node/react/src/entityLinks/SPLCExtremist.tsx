import * as React from 'react'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

class SPLCExtremistRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'splc-extremist')
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    if (!profile || !profile['splc extremist']) {
      return undefined
    }

    const capImage = ui.getCapImage(
      'https://techinquiry.org/logos/southern_poverty_law_center.svg',
      util.getEntityURL('southern poverty law center')
    )

    const url =
      'https://www.splcenter.org/fighting-hate/extremist-files/' +
      profile['splc extremist']['id']
    const recommendation = profile['splc extremist']['classification']

    // TODO(Jack Poulson): Incorporate the button class.
    const buttonType =
      recommendation == 'hate group'
        ? 'btn-outline-danger'
        : 'btn-outline-warning'

    const buttonTitle = "View SPLC's profile"
    const buttonURL = url

    const footerText = (
      <span>
        Southern Povery Law Center's{' '}
        <a href="https://www.splcenter.org/fighting-hate/extremist-files">
          Extremist Files
        </a>{' '}
        profile prominent people and organizations which the center classifies
        as extremist or, for many organizations, with the stronger label of a
        'hate group'.
      </span>
    )

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        buttonURL={url}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

export function insertCard(info, cards) {
  const profile = info.profile
  if (profile && profile['splc extremist']) {
    cards.push(<SPLCExtremistRow info={info} />)
  }
}
