import * as React from 'react'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

class ProPublicaNonprofitExplorerRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'propublica-nonprofit')
  }

  render() {
    const info = this.props.info
    const profile = info.profile

    const capImage = ui.getCapImage(
      'https://techinquiry.org/logos/pro_publica_inc.svg',
      util.getEntityURL('pro publica, inc.')
    )

    const buttonTitle = 'View tax filings'
    const buttonURL = `https://projects.propublica.org/nonprofits/organizations/${profile['ein']}`

    const footerText = (
      <span>
        ProPublica's Nonprofit Explorer contains tax returns from tax-exempt
        organizations, including financial details such as their executive
        compensation and revenue and expenses.
      </span>
    )

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        buttonURL={buttonURL}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

export function insertCard(info, cards) {
  const profile = info.profile
  if (profile && profile['ein']) {
    cards.push(<ProPublicaNonprofitExplorerRow info={info} />)
  }
}
