import * as React from 'react'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

class OpenCorporatesRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'opencorporates')
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    if (!profile.urls || !profile.urls.opencorporates) {
      return undefined
    }

    const capImage = ui.getCapImage(
      'https://techinquiry.org/logos/opencorporates_ltd.svg',
      util.getEntityURL('opencorporates ltd')
    )

    const buttonTitle = 'View records'
    const buttonURL = profile.urls.opencorporates

    const footerText = (
      <span>
        OpenCorporates is the largest open database of corporate filings in the
        world.
      </span>
    )

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        buttonURL={buttonURL}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

export function insertCard(info, cards) {
  const profile = info.profile
  if (profile && profile.urls && profile.urls.opencorporates) {
    cards.push(<OpenCorporatesRow info={info} />)
  }
}
