import * as React from 'react'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

class CorpWatchRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'corpwatch')
  }

  render() {
    const info = this.props.info
    const profile = info.profile

    const capImage = ui.getCapImage(
      'https://techinquiry.org/logos/corpwatch.png',
      util.getEntityURL('corpwatch')
    )

    const buttonTitle = "View CorpWatch's profile"
    const buttonURL = `http://www.corpwatch.org/company/${profile['corpwatch']}`

    const footerText = (
      <span>
        CorpWatch tracks corporate wrongdoing, including through the corporate
        profiles of its Gulliver project.
      </span>
    )

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        buttonURL={buttonURL}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

export function insertCard(info, cards) {
  const profile = info.profile
  if (profile && profile['corpwatch']) {
    cards.push(<CorpWatchRow info={info} />)
  }
}
