import * as React from 'react'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

class ViolationTrackerRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'violation-tracker')
  }

  render() {
    const info = this.props.info
    const profile = info.profile

    const capImage = ui.getCapImage(
      'https://techinquiry.org/logos/good_jobs_first.png',
      util.getEntityURL('good jobs first')
    )

    const buttonTitle = 'View profile'
    const buttonURL = `https://violationtracker.goodjobsfirst.org/prog.php?parent=${profile['violation tracker']}`

    const footerText = (
      <span>
        Good Jobs First's Violation Tracker is a wide-ranging database on
        corporate misconduct. It covers banking, consumer protection, false
        claims, environmental, wage & hour, safety, discrimination, and
        price-fixing since 2000.
      </span>
    )

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        buttonURL={buttonURL}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

export function insertCard(info, cards) {
  const profile = info.profile
  if (profile && profile['violation tracker']) {
    cards.push(<ViolationTrackerRow info={info} />)
  }
}
