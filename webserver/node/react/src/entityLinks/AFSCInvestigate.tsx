import * as React from 'react'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

class AFSCInvestigateRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'afsc-investigate')
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    if (!profile || !profile['afsc investigate']) {
      return undefined
    }

    const capImage = ui.getCapImage(
      'https://techinquiry.org/logos/american_friends_service_committee.svg',
      util.getEntityURL('american friends service committee')
    )

    const url =
      'https://investigate.afsc.org/company/' +
      profile['afsc investigate']['name']
    const recommendation = profile['afsc investigate']['recommendation']
    const buttonType =
      recommendation == 'Divestment'
        ? 'btn-outline-danger'
        : 'btn-outline-warning'

    const buttonTitle = 'View profile'
    const footerText = (
      <span>
        Investigate's research focuses on companies involved in state violence
        and human rights violations around the world, through incarceration and
        immigrant detention, military occupation, and the border militarization
        industries.
      </span>
    )

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        buttonURL={url}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

export function insertCard(info, cards) {
  const profile = info.profile
  if (profile && profile['afsc investigate']) {
    cards.push(<AFSCInvestigateRow info={info} />)
  }
}
