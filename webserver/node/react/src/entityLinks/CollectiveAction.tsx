import * as React from 'react'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

class CollectiveActionRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'collective-action')
  }

  render() {
    const info = this.props.info
    const profile = info.profile

    const capImage = ui.getCapImage(
      'https://techinquiry.org/logos/collective_action_in_tech.svg',
      util.getEntityURL('collective action in tech')
    )

    const buttonTitle = 'View profile'
    const buttonURL = `https://data.collectiveaction.tech/?query=${profile['collective action']}`

    const footerText = (
      <>
        Collective Action in Tech attempts to document all collective action
        from workers in the tech industry.
      </>
    )

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        buttonURL={buttonURL}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

export function insertCard(info, cards) {
  const profile = info.profile
  if (profile && profile['collective action']) {
    cards.push(<CollectiveActionRow info={info} />)
  }
}
