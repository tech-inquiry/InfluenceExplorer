import * as React from 'react'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

class PaywatchRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'paywatch')
  }

  render() {
    const info = this.props.info
    const profile = info.profile

    const capImage = ui.getCapImage(
      'https://techinquiry.org/logos/afl-cio.png',
      util.getEntityURL(
        'american federation of labor and congress of industrial organizations'
      )
    )

    const buttonTitle = 'View profile'
    const buttonURL = `https://aflcio.org/paywatch/${profile['afl-cio paywatch']}`

    const footerText = (
      <span>
        AFL-CIO's Paywatch tracks the ratio between executive and median worker
        pay at the largest companies in the United States.
      </span>
    )

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        buttonURL={buttonURL}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

export function insertCard(info, cards) {
  const profile = info.profile
  if (profile && profile['afl-cio paywatch']) {
    cards.push(<PaywatchRow info={info} />)
  }
}
