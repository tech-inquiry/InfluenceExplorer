import * as React from 'react'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

class OpenSecretsRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'opensecrets')
  }

  render() {
    const info = this.props.info
    const profile = info.profile

    const capImage = ui.getCapImage(
      'https://techinquiry.org/logos/center_for_responsive_politics.png',
      util.getEntityURL('center for responsive politics')
    )

    const buttonTitle = 'View profile'
    const buttonURL = profile.urls.opensecrets

    const footerText = (
      <span>
        OpenSecrets is the nation's premier research group tracking money in
        U.S. politics and its effect on elections and public policy.
      </span>
    )

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        buttonURL={buttonURL}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

export function insertCard(info, cards) {
  const profile = info.profile
  if (profile && profile.urls && profile.urls.opensecrets) {
    cards.push(<OpenSecretsRow info={info} />)
  }
}
