import * as React from 'react'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

class WhoProfitsRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'whoprofits')
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    if (!profile || !profile.whoprofits) {
      return undefined
    }

    const capImage = ui.getCapImage(
      'https://techinquiry.org/logos/who_profits_from_the_occupation.png',
      util.getEntityURL('who profits from the occupation')
    )

    const url = 'https://www.whoprofits.org/company/' + profile.whoprofits

    const buttonTitle = "View Who Profits' profile"
    const buttonURL = url

    const footerText = (
      <span>
        Who Profits is an independent research center dedicated to exposing the
        role of the private sector in the Israeli occupation economy.
      </span>
    )

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        buttonURL={url}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

export function insertCard(info, cards) {
  const profile = info.profile
  if (profile && profile.whoprofits) {
    cards.push(<WhoProfitsRow info={info} />)
  }
}
