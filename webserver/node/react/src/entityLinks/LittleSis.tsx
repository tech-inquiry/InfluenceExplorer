import * as React from 'react'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

class LittleSisRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'littlesis')
  }

  render() {
    const info = this.props.info
    const profile = info.profile

    const capImage = ui.getCapImage(
      'https://techinquiry.org/logos/public_accountability_initiative_inc.png',
      util.getEntityURL('public accountability initiative, inc.')
    )

    const buttonTitle = 'View profile'
    const buttonURL = profile.urls.littlesis

    const footerText = <span>Click to view the LittleSis profile.</span>

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        buttonURL={buttonURL}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

export function insertCard(info, cards) {
  const profile = info.profile
  if (profile && profile.urls && profile.urls.littlesis) {
    cards.push(<LittleSisRow info={info} />)
  }
}
