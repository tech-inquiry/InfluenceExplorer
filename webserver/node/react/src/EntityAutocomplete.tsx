import * as React from 'react'

import * as ui from './utilities/ui'
import * as util from './utilities/util'

function suggestionToItem(suggestion, index: number) {
  const url = util.getEntityURL(suggestion.vendor)
  const logoEntry = (
    <a href={url}>
      <div className="text-center">
        <img
          className="ti-modal-autocomplete-entity-logo ti-expand-on-hover"
          src={util.coerce(suggestion.logoLink, util.defaultLogo)}
        />
      </div>
    </a>
  )

  return (
    <div key={url} className="row align-items-center my-1 py-1">
      <div className="col-6 align-items-center px-5 bg-light">
        <div className="text-center">{logoEntry}</div>
      </div>
      <div className="col-6 align-items-center">
        <h3 className="fw-bold mb-2">
          <a className="text-decoration-none" href={url}>
            {suggestion.stylizedVendor}
          </a>
        </h3>
      </div>
    </div>
  )
}

export class EntityAutocomplete extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'autocomplete')
  }

  render() {
    return (
      <button
        type="button"
        className="btn btn-light"
        data-bs-toggle="modal"
        data-bs-target={'#' + ui.modalId(this.label)}
      >
        <i className="bi bi-search me-2"></i> Search our records
      </button>
    )
  }
}

export class EntityAutocompleteDisplayMobile extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)

    this.label = util.coerce(this.props.label, 'autocomplete')

    this.state = {
      hint: util.coerce(this.props.hint, ''),
      lastHint: undefined,
      suggestions: [],
    }

    this.handleInputPress = this.handleInputPress.bind(this)
    this.handleInputReset = this.handleInputReset.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
  }

  async handleInputReset(evt) {
    const hint = ''
    const changed = hint != this.state.lastHint
    this.setState({ hint: hint, lastHint: this.state.hint })
    if (changed) {
      this.setState({ suggestions: [] })
    }
  }

  async handleInputChange(evt) {
    const hint = evt.target.value.substr(0, this.props.maxInputLength)
    const changed = hint != this.state.lastHint
    this.setState({ hint: hint, lastHint: this.state.hint })
    if (changed) {
      if (hint) {
        const that = this
        util.getSuggestions(hint).then(function (suggestions) {
          that.setState({ suggestions: suggestions })
        })
      } else {
        this.setState({ suggestions: [] })
      }
    }
  }

  handleInputPress(evt) {
    if (evt.key === 'Enter') {
      if (this.state.hint) {
        ;(window as Window).location = util.getSearchURL(this.state.hint)
      }
    }
  }

  render() {
    return (
      <div className="card mt-5">
        <div className="card-body justify-content-center align-items-center">
          <div className="input-group input-group-lg my-2">
            <span className="input-group-text">
              <i className="bi bi-search"></i>
            </span>
            <input
              type="text"
              className="form-control"
              style={{ fontSize: '2rem' }}
              placeholder="Entity name or search terms"
              value={this.state.hint}
              onChange={this.handleInputChange}
              onKeyDown={this.handleInputPress}
              autoFocus
            />
            <button
              className="btn btn-outline-secondary"
              onClick={this.handleInputReset}
            >
              &times;
            </button>
          </div>
          <div className="container justify-content-center align-items-center my-1">
            {this.state.suggestions.map(suggestionToItem)}
          </div>
        </div>
      </div>
    )
  }
}

export class EntityAutocompleteDisplay extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)

    this.label = util.coerce(this.props.label, 'autocomplete')

    this.state = {
      hint: util.coerce(this.props.hint, ''),
      lastHint: undefined,
      suggestions: [],
    }

    this.handleInputPress = this.handleInputPress.bind(this)
    this.handleInputReset = this.handleInputReset.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
  }

  async handleInputReset(evt) {
    const hint = ''
    const changed = hint != this.state.lastHint
    this.setState({ hint: hint, lastHint: this.state.hint })
    if (changed) {
      this.setState({ suggestions: [] })
    }
  }

  async handleInputChange(evt) {
    const hint = evt.target.value.substr(0, this.props.maxInputLength)
    const changed = hint != this.state.lastHint
    this.setState({ hint: hint, lastHint: this.state.hint })
    if (changed) {
      if (hint) {
        const that = this
        util.getSuggestions(hint).then(function (suggestions) {
          that.setState({ suggestions: suggestions })
        })
      } else {
        this.setState({ suggestions: [] })
      }
    }
  }

  handleInputPress(evt) {
    if (evt.key === 'Enter') {
      if (this.state.hint) {
        ;(window as Window).location = util.getSearchURL(this.state.hint)
      }
    }
  }

  render() {
    if (this.props.isModal) {
      return (
        <div className="modal" id={ui.modalId(this.label)} tabIndex={-1}>
          <div className="modal-dialog modal-lg modal-fullscreen-lg-down">
            <div className="modal-content">
              <div className="modal-header">
                <h3 className="modal-title mb-0">
                  Search for terms or choose an entity
                </h3>
                <button
                  type="button"
                  id={ui.modalCloseButtonId(this.label)}
                  className="btn-close btn-lg"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body justify-content-center align-items-center border border-secondary my-0">
                <div className="input-group input-group-lg my-2">
                  <span className="input-group-text">
                    <i className="bi bi-search"></i>
                  </span>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Entity name or search terms"
                    value={this.state.hint}
                    onChange={this.handleInputChange}
                    onKeyDown={this.handleInputPress}
                    autoFocus
                  />
                  <button
                    className="btn btn-outline-secondary"
                    onClick={this.handleInputReset}
                  >
                    &times;
                  </button>
                </div>
                <div className="container justify-content-center align-items-center my-1">
                  {this.state.suggestions.map(suggestionToItem)}
                </div>
              </div>
              <div className="modal-footer">
                <p className="text-muted lh-sm mt-1">
                  After typing a phrase, either click an entity to view their
                  profile or hit submit for a full text search.
                </p>
                <ui.SecondaryCloseButton />
              </div>
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <div className="card">
          <div className="card-body justify-content-center align-items-center">
            <div className="input-group input-group-lg my-2">
              <span className="input-group-text">
                <i className="bi bi-search"></i>
              </span>
              <input
                type="text"
                className="form-control"
                placeholder="Entity name or search terms"
                value={this.state.hint}
                onChange={this.handleInputChange}
                onKeyDown={this.handleInputPress}
                autoFocus
              />
              <button
                className="btn btn-outline-secondary"
                onClick={this.handleInputReset}
              >
                &times;
              </button>
            </div>
            <div className="container justify-content-center align-items-center my-1">
              {this.state.suggestions.map(suggestionToItem)}
            </div>
          </div>
          <div className="card-footer text-muted lh-sm">
            After typing a phrase, either click an entity to view their profile
            or hit submit for a full text search.
          </div>
        </div>
      )
    }
  }
}
