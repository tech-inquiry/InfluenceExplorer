import * as React from 'react'
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry'

import * as util from './utilities/util'

export class EntityMasonry extends React.Component<any, any> {
  entities: any[]
  logoPrefix: string
  items: any[]

  minHeight: string
  maxHeight: string
  padding: string
  borderRadius: string
  gutter: string
  columnsCountBreakPoints: any

  constructor(props) {
    super(props)

    this.entities = props.entities
    this.logoPrefix = props.logoPrefix

    this.minHeight = props.minHeight ? props.minHeight : '80px'
    this.maxHeight = props.maxHeight ? props.maxHeight : '120px'
    this.padding = props.padding ? props.padding : '7px'
    this.borderRadius = props.borderRadius ? props.borderRadius : '7px'
    this.gutter = props.gutter ? props.gutter : '5px'

    this.columnsCountBreakPoints = props.columnsCountBreakPoints
      ? props.columnsCountBreakPoints
      : { 300: 2, 450: 3, 600: 4, 750: 5, 900: 6 }

    const that = this
    this.items = this.entities.map(function (item, index) {
      const logo = item.logo
        ? item.logo.startsWith('/logos/')
          ? that.logoPrefix + item.logo
          : item.logo
        : util.defaultLogo
      return (
        <div style={{ minHeight: that.minHeight, maxHeight: that.maxHeight }}>
          <a href={util.getEntityURL(item.name)} key={index}>
            <img
              src={logo}
              className="ti-expand-on-hover"
              style={{
                height: '100%',
                width: '100%',
                objectFit: 'contain',
                padding: that.padding,
                borderRadius: that.borderRadius,
              }}
            />
          </a>
        </div>
      )
    })
  }

  render() {
    return (
      <ResponsiveMasonry columnsCountBreakPoints={this.columnsCountBreakPoints}>
        <Masonry gutter={this.gutter}>{this.items}</Masonry>
      </ResponsiveMasonry>
    )
  }
}

export class BookMasonry extends React.Component<any, any> {
  books: any[]
  logoPrefix: string
  items: any[]

  minHeight: string
  maxHeight: string
  padding: string
  borderRadius: string
  gutter: string
  columnsCountBreakPoints: any

  constructor(props) {
    super(props)

    this.books = props.books
    this.logoPrefix = props.logoPrefix

    this.minHeight = props.minHeight ? props.minHeight : '80px'
    this.maxHeight = props.maxHeight ? props.maxHeight : '120px'
    this.padding = props.padding ? props.padding : '7px'
    this.borderRadius = props.borderRadius ? props.borderRadius : '7px'
    this.gutter = props.gutter ? props.gutter : '5px'

    this.columnsCountBreakPoints = props.columnsCountBreakPoints
      ? props.columnsCountBreakPoints
      : { 300: 2, 450: 3, 600: 4, 750: 5, 900: 6 }

    const that = this
    this.items = this.books.map(function (item, index) {
      const logo = item.logo ? that.logoPrefix + item.logo : util.defaultLogo
      return (
        <div style={{ minHeight: that.minHeight, maxHeight: that.maxHeight }}>
          <a href={util.getBookURL(item.name)} key={index}>
            <img
              src={logo}
              className="ti-expand-on-hover"
              style={{
                height: '100%',
                width: '100%',
                objectFit: 'contain',
                padding: that.padding,
                borderRadius: that.borderRadius,
              }}
            />
          </a>
        </div>
      )
    })
  }

  render() {
    return (
      <ResponsiveMasonry columnsCountBreakPoints={this.columnsCountBreakPoints}>
        <Masonry gutter={this.gutter}>{this.items}</Masonry>
      </ResponsiveMasonry>
    )
  }
}

export function createMasonry(
  entities,
  logoPrefix,
  minHeight = 64,
  maxHeight = 96,
  columnsCountBreakPoints = undefined
) {
  return Object.keys(entities).map(function (key, index) {
    const values = entities[key]
    return (
      <div key={index} className="card border-primary my-3">
        <div className="card-body">
          <h3 className="card-title fw-bold">{key}</h3>
          {Array.isArray(values) ? (
            <EntityMasonry
              entities={values}
              logoPrefix={logoPrefix}
              minHeight={minHeight}
              maxHeight={maxHeight}
              columnsCountBreakPoints={columnsCountBreakPoints}
            />
          ) : (
            Object.keys(values).map(function (subkey, subindex) {
              const subvalues = values[subkey]
              return (
                <div className="my-2 pb-3">
                  <h4 className="card-title">{subkey}</h4>
                  <div className="bg-light">
                    <EntityMasonry
                      entities={subvalues}
                      logoPrefix={logoPrefix}
                      minHeight={minHeight}
                      maxHeight={maxHeight}
                      columnsCountBreakPoints={columnsCountBreakPoints}
                    />
                  </div>
                </div>
              )
            })
          )}
        </div>
      </div>
    )
  })
}

export function createMasonryBooks(
  books,
  logoPrefix,
  minHeight = 64,
  maxHeight = 96,
  columnsCountBreakPoints = undefined
) {
  return Object.keys(books).map(function (key, index) {
    const values = books[key]
    return (
      <div key={index} className="card border-primary my-3">
        <div className="card-body">
          <h3 className="card-title fw-bold">{key}</h3>
          {Array.isArray(values) ? (
            <BookMasonry
              books={values}
              logoPrefix={logoPrefix}
              minHeight={minHeight}
              maxHeight={maxHeight}
              columnsCountBreakPoints={columnsCountBreakPoints}
            />
          ) : (
            Object.keys(values).map(function (subkey, subindex) {
              const subvalues = values[subkey]
              return (
                <div className="my-2 pb-3">
                  <h4 className="card-title">{subkey}</h4>
                  <div className="bg-light">
                    <BookMasonry
                      books={subvalues}
                      logoPrefix={logoPrefix}
                      minHeight={minHeight}
                      maxHeight={maxHeight}
                      columnsCountBreakPoints={columnsCountBreakPoints}
                    />
                  </div>
                </div>
              )
            })
          )}
        </div>
      </div>
    )
  })
}
