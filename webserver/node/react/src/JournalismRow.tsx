import * as React from 'react'

import * as ui from './utilities/ui'
import * as util from './utilities/util'

const citationLogos = {
  'ABC News': '/logos/american_broadcasting_company.svg',
  'Al Jazeera': '/logos/al_jazeera.png',
  'Al Jazeera America': '/logos/al_jazeera.png',
  'Al Jazeera Investigation Unit': '/logos/al_jazeera.png',
  'All-Source Intelligence': '/logos/all-source_intelligence_fusion.jpeg',
  'Amnesty International': '/logos/amnesty_international.svg',
  BBC: '/logos/british_broadcasting_corporation.svg',
  'Bloomberg Law': '/logos/bloomberg_lp.svg',
  'Bloomberg News': '/logos/bloomberg_lp.svg',
  'Breaking Defense': '/logos/breaking_defense.png',
  'Business Insider': '/logos/insider_inc.svg',
  'Business Wire': '/logos/business_wire_inc.svg',
  'BuzzFeed News': '/logos/buzzfeed_inc.svg',
  'CBS News': '/logos/cbs_news_inc.svg',
  'Cision PR Newswire': '/logos/cision_ltd.png',
  CNBC: '/logos/nbc_news.svg',
  CNN: '/logos/cnn_global.svg',
  'Defense Security Cooperation Agency':
    '/logos/defense_security_cooperation_agency.jpg',
  'Drop Site': '/logos/drop_site_news.png',
  'Drop Site News': '/logos/drop_site_news.png',
  'Electronic Privacy Information Center':
    '/logos/electronic_privacy_information_center.png',
  'Fast Company': '/logos/fast_company_inc.svg',
  'Federal Bureau of Investigation':
    '/logos/federal_bureau_of_investigation.svg',
  'Financial Times': '/logos/the_financial_times_ltd.svg',
  Forbes: '/logos/forbes_global_media_holdings_inc.svg',
  Haaretz: '/logos/haaretz_daily_newspaper_limited.svg',
  'Huffington Post': '/logos/huffpost.svg',
  'Human Rights Watch': '/logos/human_rights_watch.svg',
  'Intelligence Online': '/logos/intelligence_online.webp',
  IrpiMedia: '/logos/irpi_media.png',
  'Just Futures Law': '/logos/just_futures_law.png',
  LinkedIn: '/logos/linkedin_corporation.svg',
  'Los Angeles Times': '/logos/los_angeles_times_communications_llc.svg',
  Mijente: '/logos/mijente.svg',
  'Mother Jones': '/logos/foundation_for_national_progress.png',
  Motherboard: '/logos/vice_motherboard.png',
  'National Public Radio': '/logos/national_public_radio.svg',
  'NBC News': '/logos/nbc_news.svg',
  Newsweek: '/logos/newsweek_digital_llc.svg',
  POLITICO: '/logos/politico.svg',
  'POLITICO Europe': '/logos/politico.svg',
  ProPublica: '/logos/pro_publica_inc.svg',
  Recode: '/logos/vox_media_inc.svg',
  Reuters: '/logos/reuters_ltd.svg',
  'Rolling Stone': '/logos/rolling_stone.svg',
  'South China Morning Post': '/logos/south_china_morning_post.svg',
  Substack: '/logos/substack_inc.png',
  'The American Prospect': '/logos/the_american_prospect.png',
  'The Associated Press': '/logos/the_associated_press.svg',
  'The Forward': '/logos/the_forward.svg',
  'The General and the Ambassador':
    '/logos/the_general_and_the_ambassador_podcast.png',
  'The Grayzone': '/logos/the_grayzone.png',
  'The Guardian': '/logos/guardian_media_group_plc.png',
  'The High Side': '/logos/the_high_side.jpeg',
  'The Hill': '/logos/the_hill.svg',
  'The Intercept': '/logos/the_intercept.svg',
  'The Jerusalem Post': '/logos/the_jerusalem_post.png',
  'The Markup': '/logos/the_markup_news_inc.png',
  'The Nation': '/logos/the_nation_company_llc.svg',
  'The New York Times': '/logos/the_new_york_times_company.svg',
  'The New York Times Magazine': '/logos/the_new_york_times_company.svg',
  'The New Yorker': '/logos/the_new_yorker_magazine.svg',
  'The Team House': '/logos/the_team_house.png',
  'The Times of Israel': '/logos/the_times_of_israel.svg',
  'The Verge': '/logos/the_verge.svg',
  'The Wall Street Journal': '/logos/the_wall_street_journal.svg',
  'The Washington Post': '/logos/wp_company_llc.svg',
  TIME: '/logos/time_usa_llc.svg',
  'TIME Magazine': '/logos/time_usa_llc.svg',
  'Toronto Star': '/logos/toronto_star_newspapers_limited.svg',
  'Twitter, Inc.': '/logos/twitter_inc.svg',
  'U.S. Department of Justice':
    '/logos/united_states_department_of_justice.svg',
  Vice: '/logos/vice_media_group_llc.svg',
  Vox: '/logos/vox_media_inc.svg',
  WIRED: '/logos/wired_magazine.svg',
  'WIRED UK': '/logos/wired_magazine.svg',
  YouTube: '/logos/youtube.png',
  Zeteo: '/logos/zeteo.png',
}

function articleToItem(article, index) {
  const orgEntry = (
    <a className="text-decoration-none" href={article.url}>
      {citationLogos.hasOwnProperty(article.org) ? (
        <img
          className="ti-journalism-org-list-item"
          src={citationLogos[article.org]}
        />
      ) : (
        <h4 className="fw-bold">{article.org}</h4>
      )}
    </a>
  )

  const authorsEntry = util.getChicagoStyleAuthorsString(article.authors)
  const dateEntry = util.getDateString(article.date)
  const authorDateLine = authorsEntry
    ? `${authorsEntry}, ${dateEntry}`
    : dateEntry
  if (article.type == 'journal') {
    const volumeEntry = article.volume ? `Vol. ${article.volume},` : undefined
    const numberEntry = article.number ? `no. ${article.number}` : undefined
    return (
      <div
        key={index}
        className="row flex-md-row-reverse align-items-center py-3"
      >
        <div className="col-12 col-md-4 align-items-center bg-light">
          <div className="text-center">
            <span className="fst-italic">{orgEntry}</span> {volumeEntry}{' '}
            {numberEntry}
          </div>
        </div>
        <div className="col-12 col-md-8">
          <h4 className="fw-bold mb-2">
            <a className="text-decoration-none" href={article.url}>
              {article.title}
            </a>
          </h4>
          <h5 className="mb-3">{authorDateLine}</h5>
          <p style={{ fontSize: '125%' }}>{article.note ? article.note : ''}</p>
        </div>
      </div>
    )
  } else {
    return (
      <div
        key={index}
        className="row flex-md-row-reverse align-items-center py-3"
      >
        <div className="col-12 col-md-4 align-items-center bg-light">
          <div className="text-center">{orgEntry}</div>
        </div>
        <div className="col-12 col-md-8">
          <h4 className="fw-bold mb-2">
            <a className="text-decoration-none" href={article.url}>
              {article.title}
            </a>
          </h4>
          <h5 className="mb-3">{authorDateLine}</h5>
          <p style={{ fontSize: '125%' }}>{article.note ? article.note : ''}</p>
        </div>
      </div>
    )
  }
}

class JournalismRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(
      this.props.label,
      this.props.isJournalism ? 'journalism' : 'misc-citations'
    )
  }

  render() {
    const entity = this.props.info.query
    const profile = this.props.info.profile
    if (!profile) {
      return undefined
    }
    if (this.props.isJournalism) {
      if (!profile.journalism) {
        return undefined
      }
    } else {
      if (!profile.misc) {
        return undefined
      }
    }

    const selfAnnotation = profile.entityAnnotations[entity]

    const logo = this.props.isJournalism ? 'journalism.png' : 'references.png'
    const capImage = ui.getCapImage(
      `https://techinquiry.org/logos/${logo}`,
      undefined,
      '100px',
      '250px',
      1.0
    )

    const citationList = this.props.isJournalism
      ? profile.journalism
      : profile.misc

    const buttonTitle = this.props.isJournalism
      ? 'Investigations'
      : 'Misc. citations'

    const footerText = (
      <>
        Click to view{' '}
        <span className="fw-bold">
          {citationList.length + ' manually tagged '}
          {citationList.length > 1 ? 'articles' : 'article'}
        </span>{' '}
        relating to{' '}
        <span className="fst-italic">{selfAnnotation.stylizedText}</span>
        {selfAnnotation.stylizedText.endsWith('.') ? '' : '.'}
      </>
    )

    return (
      <ui.EntityRowCard
        label={this.label}
        buttonTitle={buttonTitle}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

class JournalismModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(
      this.props.label,
      this.props.isJournalism ? 'journalism' : 'misc-citations'
    )
  }

  render() {
    const profile = this.props.info.profile
    if (!profile) {
      return undefined
    }
    if (this.props.isJournalism) {
      if (!profile.journalism) {
        return undefined
      }
    } else {
      if (!profile.misc) {
        return undefined
      }
    }

    const modalTitle =
      (this.props.isJournalism ? 'Select Investigations' : 'Misc. Citations') +
      ' (reverse-chronological)'

    const citationList = this.props.isJournalism
      ? profile.journalism
      : profile.misc
    const modalBody = (
      <div className="container px-3">{citationList.map(articleToItem)}</div>
    )

    return (
      <ui.EntityRowModal
        label={this.label}
        title={modalTitle}
        body={modalBody}
      />
    )
  }
}

export function insertCardAndModal(
  info,
  isJournalism,
  cards: any[],
  modals: any[]
): void {
  const profile = info.profile
  if (!profile) {
    return
  }
  if (isJournalism && !profile.journalism) {
    return
  }
  if (!isJournalism && !profile.misc) {
    return
  }
  cards.push(<JournalismRow info={info} isJournalism={isJournalism} />)
  modals.push(<JournalismModal info={info} isJournalism={isJournalism} />)
}
