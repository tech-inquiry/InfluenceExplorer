import * as React from 'react'

import { Fade } from 'react-slideshow-image'
import 'react-slideshow-image/dist/styles.css'

import * as util from './utilities/util'

export class RotatingImage extends React.Component<any, any> {
  opacity: number
  height: string
  width: string
  maxWidth: string
  intervalMS: number

  constructor(props) {
    super(props)

    this.opacity = util.coerce(this.props.opacity, 1.0)
    this.height = util.coerce(this.props.height, '10em')
    this.maxWidth = util.coerce(this.props.maxWidth, '100%')
    this.intervalMS = util.coerce(this.props.intervalMS, 2500)
  }

  render() {
    const that = this

    let imgClasses = 'ti-rotating-image-list-item mx-auto'
    if (that.props.shadow) {
      imgClasses += ' shadow'
    }

    function imageToItem(item, index) {
      const image = util.isString(item) ? item : item['image']
      const url = util.isString(item) ? undefined : item['url']
      let imgStyle = {
        opacity: that.opacity,
        height: that.height,
      }
      if (!that.props.isTall) {
        imgStyle['maxWidth'] = that.maxWidth
      }
      const imageItem = (
        <div className="py-3 px-5 text-center" style={{ width: '100%' }}>
          <img className={imgClasses} style={imgStyle} src={image} />
        </div>
      )
      const fullItem = url ? <a href={url}>{imageItem}</a> : imageItem
      return (
        <div key={index} className="each-slide-effect">
          {fullItem}
        </div>
      )
    }

    if (!this.props.items.length) {
      return undefined
    } else if (this.props.items.length == 1) {
      return <div>{imageToItem(this.props.items[0], 0)}</div>
    } else {
      return (
        <div>
          <Fade arrows={false} duration={this.intervalMS}>
            {this.props.items.map(imageToItem)}
          </Fade>
        </div>
      )
    }
  }
}
