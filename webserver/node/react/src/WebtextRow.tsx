import * as React from 'react'

import { appConfig, DEBUG } from './utilities/constants'
import { RotatingImage } from './RotatingImage'
import * as ui from './utilities/ui'
import * as util from './utilities/util'

async function getEntityList(info: any, maxEntities: number) {
  const urlBase = `${appConfig.apiURL}/webtext?text=${info.query}`
  const result = await fetch(urlBase as string).then((res) => res.json())

  let entities = {}
  for (const filing of result.filings) {
    for (const annotation of filing.entityAnnotations) {
      const entity = annotation.text
      if (!entities.hasOwnProperty(entity)) {
        entities[entity] = {
          stylizedName: annotation.stylizedText,
          image: annotation.logo,
          urls: [],
        }
      }
      entities[entity].urls.push(filing.url)
    }
  }

  // TODO(Jack Poulson): Cap at maxEntities?
  let entityList = []
  for (const entity in entities) {
    entityList.push({
      name: entity,
      stylizedName: entities[entity].stylizedName,
      image: entities[entity].image,
      url: util.getEntityURL(entity),
      urls: entities[entity].urls,
    })
  }

  entityList.sort(function (entity1, entity2) {
    if (entity1.urls.length > entity2.urls.length) {
      return -1
    }
    if (entity1.urls.length == entity2.urls.length) {
      return 0
    }
    return 1
  })

  return entityList
}

class WebtextRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'webtext')
    this.state = { items: [], loaded: false }
  }

  async componentDidMount() {
    this.setState({ items: this.props.entityList, loaded: true })
  }

  render() {
    const info = this.props.info

    const buttonTitle = 'Webtext'

    const capImage = (
      <RotatingImage
        items={this.state.items}
        opacity={1.0}
        development={info.development}
      />
    )

    const footerText = (
      <span>
        Tech Inquiry has tagged {this.state.items.length} entities in websites
        matching the query "{info.query}".
      </span>
    )

    if (this.state.items.length) {
      return (
        <ui.EntityRow
          label={this.label}
          buttonTitle={buttonTitle}
          capImage={capImage}
          footerText={footerText}
        />
      )
    } else {
      if (this.state.loaded) {
        return undefined
      } else {
        return (
          <div className="container text-center font-monospace mt-3">
            <p className="text-start">
              <span style={{ fontSize: '150%' }}>
                Performing webtext search...
              </span>
              <div className="spinner-grow text-primary" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            </p>
          </div>
        )
      }
    }
  }
}

class WebtextModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'webtext')
    this.state = { items: [], loaded: false }

    this.entityToItem = this.entityToItem.bind(this)
  }

  async componentDidMount() {
    this.setState({ items: this.props.entityList, loaded: true })
  }

  entityToItem(entity, index) {
    const stylizedName = entity.stylizedName
    const logo = entity.image
    const entityURL = entity.url

    const logoEntry = (
      <a href={entityURL}>
        <img
          className="ti-association-list-entity-logo"
          src={util.coerce(logo, util.defaultLogo)}
        />
      </a>
    )
    const nameEntry = (
      <a className="text-decoration-none" href={entityURL}>
        {stylizedName}
      </a>
    )

    const urls = entity.urls

    const note = entity.urls.map((url, urlIndex) => (
      <div
        className="ti-compact small"
        style={{ fontSize: '0.75rem' }}
        key={urlIndex}
      >
        <a href={url}>{util.simplifyURL(url)}</a>
      </div>
    ))
    return (
      <div
        key={index}
        className="row flex-md-row-reverse align-items-center mx-0 px-0 py-3"
      >
        <div className="col-12 col-md-4 align-items-center px-3 bg-light">
          <div className="text-center">{logoEntry}</div>
        </div>
        <div className="col-12 col-md-8">
          <h3 className="fw-bold mb-2">{nameEntry}</h3>
          <p className="lh-sm" style={{ fontSize: '1.5rem' }}>
            {note}
          </p>
        </div>
      </div>
    )
  }

  render() {
    const modalTitle = 'Webtext'
    const modalBody = (
      <div>
        <div key="association-container" className="container px-0">
          {this.state.items.map(this.entityToItem)}
        </div>
      </div>
    )

    if (this.state.items.length) {
      return (
        <ui.EntityRowModal
          label={this.label}
          title={modalTitle}
          body={modalBody}
          dialogExtraClasses="modal-fullscreen"
        />
      )
    } else {
      return undefined
    }
  }
}

export async function insertCardAndModal(info, cards: any[], modals: any[]) {
  if (info.profile || info.isTag) {
    return
  }
  const maxEntities = 10
  const entityList = await getEntityList(info, maxEntities)
  cards.push(<WebtextRow info={info} entityList={entityList} />)
  modals.push(<WebtextModal info={info} entityList={entityList} />)
}
