import * as React from 'react'

import { USCentralSecurities } from './USCentral'
import * as ui from '../utilities/ui'
import { coerce } from '../utilities/util'
import { appConfig } from '../utilities/constants'

class SecuritiesRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = coerce(this.props.label, 'securities')
    this.sourceLabel = this.sourceLabel.bind(this)
    this.sourceModalId = this.sourceModalId.bind(this)
  }

  sourceLabel(source) {
    return `${this.label}-${source}`
  }

  sourceModalId(source) {
    return ui.modalId(this.sourceLabel(source))
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    const isEntity = profile != undefined
    const sources = this.props.sources

    const that = this

    const capImage = (
      <div
        className="mx-auto mb-0 ti-cap-img"
        style={{
          opacity: 1.0,
          height: '100px',
          maxWidth: '250px',
          backgroundImage:
            'url("https://techinquiry.org/logos/securities.png")',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: '50% 50%',
          backgroundSize: 'contain',
        }}
      ></div>
    )

    let footerText = isEntity ? (
      <>
        Tech Inquiry has securities records for{' '}
        <span className="fst-italic">
          {profile.entityAnnotations[info.query].stylizedText}
        </span>{' '}
        from{' '}
        <span className="fw-bold">
          {sources.length + ' '}
          {sources.length > 1 ? 'sources' : 'source'}
        </span>
        .
      </>
    ) : (
      <>
        Tech Inquiry has securities records for{' '}
        <span className="fst-italic">{info.query}</span> from{' '}
        <span className="fw-bold">
          {sources.length + ' '}
          {sources.length > 1 ? 'sources' : 'source'}
        </span>
        .
      </>
    )

    // We will place all of the modal objects up top.
    let enabledSources = ['US federal securities']

    function sourceToItem(source, index) {
      return ui.dropdownSource(
        source,
        index,
        enabledSources,
        undefined,
        that.sourceModalId
      )
    }

    return (
      <ui.RowHelper
        capImage={capImage}
        button={ui.getCardDropdownButton(
          'Securities',
          `${this.label}-dropdownMenuButton`,
          sources.map(sourceToItem)
        )}
        footerText={footerText}
      />
    )
  }
}

class SecuritiesModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = coerce(this.props.label, 'securities')
    this.sourceLabel = this.sourceLabel.bind(this)
  }

  sourceLabel(source) {
    return `${this.label}-${source}`
  }

  render() {
    const info = this.props.info
    const isEntity = info.profile != undefined
    return (
      <USCentralSecurities
        label={this.sourceLabel('US federal securities')}
        query={info.query}
        isEntity={isEntity}
      />
    )
  }
}

export function insertCardAndModal(
  info,
  sources,
  cards: any[],
  modals: any[]
): void {
  if (!sources.length) {
    return
  }
  cards.push(<SecuritiesRow info={info} sources={sources} />)
  modals.push(<SecuritiesModal info={info} />)
}
