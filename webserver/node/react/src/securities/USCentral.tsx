import * as React from 'react'

import { FilingFeed } from '../FilingFeed'

import { kSecuritiesPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { encodeURLComponent, getRetrieveURL } from '../utilities/util'

// TODO(Jack Poulson): Add an iframe wrapper as for Teixeira.

export class USCentralSecurities extends React.Component<any, any> {
  metaURLBase: string
  filingsURLBase: string
  tableID: string
  label: string
  columns: any[]
  title: string

  constructor(props) {
    super(props)
    this.metaURLBase =
      appConfig.apiURL + '/' + kSecuritiesPaths['US federal securities'].getMeta
    this.filingsURLBase =
      appConfig.apiURL +
      '/' +
      kSecuritiesPaths['US federal securities'].getFilings

    this.tableID = 'ti-table-us-central-securities'
    this.title = 'US Federal Securities'

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURLs = this.retrieveURLs.bind(this)
    this.actOnItem = this.actOnItem.bind(this)

    this.label = this.props.label ? this.props.label : 'us-central-securities'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Filing Date' },
      { title: 'Report Date' },
      { title: 'CIK' },
      { title: 'Name' },
      { title: 'Type' },
      { title: 'Description' },
    ]
  }

  itemToRow(metaItems, filing, filingIndex) {
    // Extract the company name out of one of the matching meta results.
    let name = ''
    for (const meta of metaItems) {
      if (meta.cik == filing.cik) {
        name = meta.name
      }
    }

    return [
      metaItems.length + filingIndex,
      filing.filing_date,
      filing.report_date,
      filing.cik,
      name,
      filing.form,
      filing.primary_doc_description,
    ]
  }

  itemsToTableData(items) {
    let metaItems = []
    let filingItems = []
    for (const [i, item] of items.entries()) {
      if (item.source == 'meta') {
        metaItems.push(item.filing)
      } else {
        filingItems.push(item.filing)
      }
    }

    const that = this
    return filingItems.map(function (filing, filingIndex) {
      return that.itemToRow(metaItems, filing, filingIndex)
    })
  }

  actOnItem(item) {
    const filing = item.filing
    const urlEnd =
      `/Archives/edgar/data/${filing.cik}/` +
      `${filing.accession.replaceAll('-', '')}/${filing.primary_document}`
    const url = filing.is_inline_xbrl
      ? `https://www.sec.gov/ix?doc=${urlEnd}`
      : `https://www.sec.gov/${urlEnd}`
    window.open(url)
  }

  retrieveURLs() {
    return {
      meta: getRetrieveURL(
        this.metaURLBase,
        this.props.query,
        this.props.isAgency,
        this.props.isEntity,
        this.props.isTag
      ),
      filings: getRetrieveURL(
        this.filingsURLBase,
        this.props.query,
        this.props.isAgency,
        this.props.isEntity,
        this.props.isTag
      ),
    }
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURLs={this.retrieveURLs()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        actOnItem={this.actOnItem}
      />
    )
  }
}
