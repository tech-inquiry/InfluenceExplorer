import * as React from 'react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import RequestEntityUpdate from './apps/RequestEntityUpdate'

import '../css/requestEntityUpdate.css'

const root = createRoot(document.getElementById('request-entity-update-app'))
root.render(
  <BrowserRouter>
    <RequestEntityUpdate />
  </BrowserRouter>
)
