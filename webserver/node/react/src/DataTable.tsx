import * as React from 'react'
import * as $ from 'jquery'
import 'datatables.net'

import { DEBUG } from './utilities/constants'

export class DataTable extends React.Component<any, any> {
  table: any
  counter: number

  constructor(props) {
    super(props)

    this.table = undefined
    this.state = { tableInitialized: false }
    this.initializeTable = this.initializeTable.bind(this)

    if (DEBUG) {
      this.counter = 0
      console.log(`${this.counter}: Constructing DataTable`)
      this.counter += 1
    }
  }

  initializeTable() {
    if (DEBUG) {
      console.log(
        `${this.counter}: Initializing DataTable with ` +
          `${this.props.tableData.length} rows`
      )
      this.counter += 1
    }

    const that = this
    const conditionalRowStyles = [
      {
        when: (row) =>
          that.props.highlightFilter
            ? that.props.highlightFilter(row.index())
            : undefined,
        style: {
          backgroundColor: 'green',
          color: 'white',
        },
      },
    ]

    const table = $(`#${this.props.tableID}`)
    this.table = table.DataTable({
      data: this.props.tableData,
      columns: this.props.columns,
      responsive: true,
      // Unfortunately, conditionalRowStyles does not seem to be supported.
      // conditionalRowStyles: conditionalRowStyles,
    })
    if (this.props.registerClickedRow) {
      $(`#${this.props.tableID} tbody`).on('click', 'tr', function () {
        const rowIndex = that.table.row(this).index()
        that.props.registerClickedRow(rowIndex)
      })
    }

    this.setState({ tableInitialized: true })
  }

  componentDidMount() {
    // DEBUG
    //this.initializeTable()
  }

  componentWillUnmount() {
    if (this.table) {
      this.table.destroy()
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    if (!this.state.tableInitialized && this.props.tableData !== undefined) {
      this.initializeTable()
    } else if (prevProps.tableData != this.props.tableData) {
      if (DEBUG) {
        console.log(
          `${this.counter}: Replacing table with ` +
            `${this.props.tableData.length} rows`
        )
        this.counter += 1
      }

      this.table.clear()
      if (prevProps.columns == this.props.columns) {
        // Replace the rows then redraw.
        this.table.rows.add(this.props.tableData)
        this.table.draw()
      } else {
        // Rather than simply replacing the rows, we completely destroy and
        // rebuild.
        this.table.destroy()
        this.initializeTable()
      }
    }
  }

  render() {
    const initialContent = (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">
            <div className="spinner-grow text-primary me-2" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            Retrieving table data from database...
          </h5>
        </div>
      </div>
    )
    const errorContent = (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">
            Error retrieving table data. If the problem persists, please email{' '}
            <a href="mailto:info@techinquiry.org">info@techinquiry.org</a>.
          </h5>
        </div>
      </div>
    )

    return (
      <div>
        {this.state.tableInitialized
          ? undefined
          : this.props.tableError
          ? errorContent
          : initialContent}
        <div className="tableResponsive" style={this.props.style}>
          <table
            id={this.props.tableID}
            className="table table-striped table-bordered"
          ></table>
        </div>
      </div>
    )
  }
}
