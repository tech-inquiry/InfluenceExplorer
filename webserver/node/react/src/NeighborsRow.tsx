import * as React from 'react'

import { RotatingImage } from './RotatingImage'
import * as ui from './utilities/ui'
import * as util from './utilities/util'

// Filter the neighbors down to those who are neither descendants
// nor ancestors.
function filterProfileNeighbors(entity, profile) {
  if (!profile.hasOwnProperty('neighbors')) {
    return
  }

  let exclusionSet = new Set()
  exclusionSet.add(entity)
  for (const listName of ['ancestors', 'descendants']) {
    if (profile.hasOwnProperty(listName)) {
      for (const entityName of profile[listName]) {
        exclusionSet.add(entityName)
      }
    }
  }

  let neighbors = []
  for (const neighbor of profile.neighbors) {
    if (!exclusionSet.has(neighbor)) {
      neighbors.push(neighbor)
    }
  }

  profile.neighbors = neighbors
}

class NeighborsRow extends React.Component<any, any> {
  label: string
  items: any[]

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'neighbors')

    const info = this.props.info
    const profile = info.profile
    filterProfileNeighbors(info.query, profile)

    this.items = []
    if (profile.hasOwnProperty('neighbors')) {
      // Truncate at a fixed number of neighbors.
      const maxDisplayedNeighbors = 25
      profile.neighbors = profile.neighbors.slice(0, maxDisplayedNeighbors)

      for (const neighbor of profile.neighbors) {
        const annotation = profile.entityAnnotations[neighbor]
        if (annotation.logoSmall) {
          this.items.push({
            image: annotation.logoSmall,
            url: util.getEntityURL(neighbor),
          })
        }
      }
    }
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    if (!profile.hasOwnProperty('neighbors') || !profile.neighbors.length) {
      return undefined
    }

    const buttonTitle = 'Related entities'

    const capImage = (
      <RotatingImage
        items={this.items}
        opacity={1.0}
        development={info.development}
      />
    )

    const footerText = (
      <span>
        Click to view related entities, which are generated from a custom
        recommender system built on top of manually tagged associations and
        international procurement and lobbying records.
      </span>
    )

    return (
      <ui.EntityRowCard
        label={this.label}
        buttonTitle={buttonTitle}
        capImage={capImage}
        footerText={footerText}
      />
    )
  }
}

class NeighborsModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'neighbors')

    const info = this.props.info
    const profile = info.profile
    filterProfileNeighbors(info.query, profile)

    if (profile.hasOwnProperty('neighbors')) {
      // Truncate at a fixed number of neighbors.
      const maxDisplayedNeighbors = 25
      profile.neighbors = profile.neighbors.slice(0, maxDisplayedNeighbors)
    }
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    if (!profile.hasOwnProperty('neighbors') || !profile.neighbors.length) {
      return undefined
    }

    const modalTitle = 'Related entities list'
    const modalBody = (
      <div className="container align-items-center my-1">
        {profile.neighbors.map(info.entityToItem)}
      </div>
    )

    return (
      <ui.EntityRowModal
        label={this.label}
        title={modalTitle}
        body={modalBody}
        dialogExtraClasses="modal-lg modal-fullscreen-lg-down"
      />
    )
  }
}

export function insertCardAndModal(info, cards: any[], modals: any[]): void {
  const profile = info.profile
  if (!profile || !profile.neighbors || !profile.neighbors.length) {
    return
  }
  cards.push(<NeighborsRow info={info} />)
  modals.push(<NeighborsModal info={info} />)
}
