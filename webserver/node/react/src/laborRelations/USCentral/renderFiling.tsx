import * as React from 'react'

import { Accordion, AccordionItem, SimpleAccordionItem } from '../../Accordion'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

export function renderFiling(filing) {
  const maxTitleLength = 200

  function renderOverview() {
    const label = 'overview'
    const title = 'Overview'

    let features = []

    ui.includeTableKeyValue(features, 'Title', filing.title)
    ui.includeTableKeyValue(features, 'Location', filing.location)
    ui.includeTableKeyValue(features, 'Region Assigned', filing.region_assigned)

    ui.includeTableKeyValue(
      features,
      'Case Number',
      <a href={`https://www.nlrb.gov/case/${filing.case_number}`}>
        {filing.case_number}
      </a>,
      true,
      filing.case_number
    )

    {
      let statusLabel = filing.status
      if (filing.reason_closed) {
        statusLabel += ` (${filing.reason_closed})`
      }
      ui.includeTableKeyValue(features, 'Status', statusLabel)
    }

    ui.includeTableKeyValue(
      features,
      'Last Update',
      util.dateWithoutTime(new Date(filing.lastUpdateDate)) +
        ': ' +
        filing.lastUpdateType,
      true,
      filing.lastUpdateDate
    )

    ui.includeTableKeyDate(features, 'Date Filed', filing.date_filed)

    if (filing.allegations.length) {
      let allegations = []
      for (const allegation of filing.annotation.allegations) {
        if ('url' in allegation) {
          allegations.push(
            <li>
              <a href={allegation.url}>{allegation.stylizedText}</a>
            </li>
          )
        } else {
          allegations.push(<li>{allegation.stylizedText}</li>)
        }
      }
      ui.includeTableKeyValue(
        features,
        'Allegations',
        <ul style={{ paddingLeft: '1em' }}>{allegations}</ul>
      )
    }

    return (
      <SimpleAccordionItem
        tableClassName="table ti-key-value-table"
        key={title}
        label={label}
        title={title}
        features={features}
      />
    )
  }

  function renderDocketActivity() {
    const label = 'docketActivity'
    const title = 'Docket Activity'

    if (!filing.docket_activity.length) {
      return undefined
    }

    const table = (
      <table className="table table-striped table-fit">
        <thead>
          <tr>
            <th scope="col">Date</th>
            <th scope="col">Document Type</th>
            <th scope="col">Filed By</th>
          </tr>
        </thead>
        <tbody>
          {filing.docket_activity.map(function (activity) {
            return (
              <tr>
                <td>{activity.date}</td>
                <td>
                  {activity.document_url ? (
                    <a href={activity.document_url}>{activity.document_type}</a>
                  ) : (
                    activity.document_type
                  )}
                </td>
                <td>{activity.filed_by}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    )

    return (
      <AccordionItem
        tableClassName="table ti-key-value-table"
        key={title}
        label={label}
        title={title}
        body={table}
      />
    )
  }

  function renderParticipants() {
    const label = 'participants'
    const title = 'Participants'

    const partyTypes = [
      {
        title: 'Charged Parties',
        label: 'chargedParties',
        data: filing.annotation.chargedParties,
      },
      {
        title: 'Charging Parties',
        label: 'chargingParties',
        data: filing.annotation.chargingParties,
      },
      {
        title: 'Involved Parties',
        label: 'involvedParties',
        data: filing.annotation.undecidedChargingParties,
      },
    ]

    let accordionItems = []

    for (const partyType of partyTypes) {
      if (!partyType.data.length) {
        continue
      }

      let partyAccordionItems = []

      for (const [i, party] of partyType.data.entries()) {
        let partyFeatures = []
        const partyTitle = party.employer
          ? party.employer.stylizedText
          : party.name
          ? party.name.stylizedText
          : 'N/A'
        const partyLabel = `${partyType.label}-${i + 1}`

        if (party.employer) {
          partyFeatures.push(
            ui.renderEntityAnnotation(party.employer, 'Employer')
          )
        }
        if (party.name) {
          partyFeatures.push(ui.renderEntityAnnotation(party.name, 'Name'))
        }

        ui.includeTableKeyValue(
          partyFeatures,
          'Employment Type',
          party.employment_type
        )

        let addressLines = []
        if (party.street_address) {
          addressLines.push(
            <p className="ti-compact">{party.street_address.toUpperCase()}</p>
          )
        }
        if (party.city_and_state) {
          let cityLine = party.city_and_state.toUpperCase()
          if (party.zipcode) {
            cityLine += ' ' + party.zipcode
          }
          addressLines.push(<p className="ti-compact">{cityLine}</p>)
        }
        ui.includeTableKeyValue(partyFeatures, 'Address', addressLines)

        ui.includeTableKeyValue(
          partyFeatures,
          'Phone Number',
          util.formatPhoneNumber(party.phone_number)
        )

        partyAccordionItems.push(
          <SimpleAccordionItem
            tableClassName="table ti-key-value-table"
            key={partyTitle}
            label={partyLabel}
            title={partyTitle}
            features={partyFeatures}
          />
        )
      }

      const partyAccordion = (
        <Accordion
          id={`us-central-labor-relations-${partyType.label}-accordion`}
          items={partyAccordionItems}
        />
      )

      accordionItems.push(
        <AccordionItem
          tableClassName="table ti-key-value-table"
          key={partyType.title}
          label={partyType.label}
          title={partyType.title}
          body={partyAccordion}
        />
      )
    }

    const accordion = (
      <Accordion
        id="us-central-labor-relations-parties-accordion"
        items={accordionItems}
      />
    )

    return (
      <AccordionItem
        tableClassName="table ti-key-value-table"
        key={title}
        label={label}
        title={title}
        body={accordion}
      />
    )
  }

  function renderRelatedDocuments() {
    const label = 'relatedDocuments'
    const title = 'Related Documents'

    if (!filing.related_documents.length) {
      return undefined
    }

    const body = (
      <ul style={{ paddingLeft: '1em' }}>
        {filing.related_documents.map(function (doc) {
          return (
            <li>{doc.url ? <a href={doc.url}>{doc.title}</a> : doc.title}</li>
          )
        })}
      </ul>
    )

    return (
      <AccordionItem
        tableClassName="table ti-key-value-table"
        key={title}
        label={label}
        title={title}
        body={body}
      />
    )
  }

  function renderRelatedCases() {
    const label = 'relatedCases'
    const title = 'Related Cases'

    if (!filing.related_cases.length) {
      return undefined
    }

    const table = (
      <table className="table table-fit table-striped">
        <thead>
          <tr>
            <th scope="col">Case Number</th>
            <th scope="col">Case Name</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          {filing.related_cases.map(function (info) {
            return (
              <tr>
                <td>
                  <a href={`https://www.nlrb.gov/case/${info.case_number}`}>
                    {info.case_number}
                  </a>
                </td>
                <td>{info.case_name}</td>
                <td>{info.status}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    )

    return (
      <AccordionItem
        tableClassName="table ti-key-value-table"
        key={title}
        label={label}
        title={title}
        body={table}
      />
    )
  }

  let title = `${filing.case_number} (${filing.status}): `
  if (filing.title) {
    title += filing.title
  }
  title = util.trimLabel(title, maxTitleLength)

  const documentURL = 'https://www.nlrb.gov/case/' + filing.case_number
  let header = (
    <p className="card-text">
      <a href={documentURL}>
        <button
          type="button"
          className="btn btn-secondary"
          style={{
            fontSize: '95%',
            padding: '4px 7px',
            margin: '0 1em 0.5em 1em',
          }}
        >
          View Original
        </button>
      </a>
    </p>
  )

  let accordionItems = []
  accordionItems.push(renderOverview())
  accordionItems.push(renderDocketActivity())
  accordionItems.push(renderParticipants())
  accordionItems.push(renderRelatedDocuments())
  accordionItems.push(renderRelatedCases())
  let accordion = (
    <Accordion
      id="us-central-labor-relations-accordion"
      items={accordionItems}
    />
  )

  return (
    <div className="card text-center mb-2 border-0">
      <div className="card-body">
        <h3 className="fw-bold lh-1 mb-2">{title}</h3>
        {header}
        {accordion}
      </div>
    </div>
  )
}
