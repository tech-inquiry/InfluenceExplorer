import * as React from 'react'

import { FilingFeed } from '../FilingFeed'

import { kLaborRelationsPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USCentral/renderFiling'

import * as util from '../utilities/util'

export class USCentralLaborRelations extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  columns: any[]
  title: string

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL +
      '/' +
      kLaborRelationsPaths['US federal labor relations'].get

    this.tableID = 'ti-table-us-central-labor-relations'
    this.title = 'US National Labor Relations Board Cases'

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : 'us-federal-labor-relations'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Last Update' },
      { title: 'Title' },
      { title: 'Case Number' },
      { title: 'Status' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    const lastUpdate = util.dateWithoutTime(new Date(filing.lastUpdateDate))
    const caseNumber = filing.case_number
    const status = filing.status
    const title = filing.title ? filing.title : ''
    const entireFiling = JSON.stringify(filing)
    return [filingIndex, lastUpdate, title, caseNumber, status, entireFiling]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
