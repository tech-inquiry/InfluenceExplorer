import * as React from 'react'

import { appConfig } from './utilities/constants'
import * as util from './utilities/util'

import {
  EntityAutocomplete,
  EntityAutocompleteDisplay,
} from './EntityAutocomplete'

export const TIPageContainer = ({ maxInputLength, isMobile, children }) => {
  const urlComponents = window.location.href.split('?')
  let loginURL = `/login?redirectPath=${urlComponents[0]}`
  if (urlComponents[1]) {
    loginURL += `&redirectParams=${urlComponents[1]}`
  }
  return (
    <div className="ti-app-container">
      <nav className="navbar navbar-expand navbar-dark bg-primary shadow">
        <div className="container-fluid mx-3">
          <a className="navbar-brand" href={appConfig.root}>
            Tech Inquiry
          </a>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a
                  className="nav-link active"
                  aria-current="page"
                  href="?about=board"
                >
                  Board
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link active"
                  aria-current="page"
                  href="?about=overview"
                >
                  About
                </a>
              </li>
            </ul>
          </div>
          {isMobile ? undefined : (
            <div className="justify-content-end">
              <EntityAutocomplete />
            </div>
          )}
          {util.retrieveToken() ? undefined : (
            <span className="ml-2">
              <a
                className="nav-link active text-white"
                aria-current="page"
                href={loginURL}
              >
                Log in
              </a>
            </span>
          )}
        </div>
      </nav>
      {isMobile ? undefined : (
        <EntityAutocompleteDisplay
          isModal={true}
          maxInputLength={maxInputLength}
        />
      )}
      <div className="ti-content">{children}</div>
    </div>
  )
}
