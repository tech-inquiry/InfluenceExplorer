import * as React from 'react'

import * as util from './utilities/util'

export class FooterCard extends React.Component<any, any> {
  render() {
    return (
      <div id="footer" className="mt-5">
        <div className="container text-center font-monospace small border-top">
          <p className="text-start mt-5 mb-0">
            Please forward any comments or tips to our executive director, Jack
            Poulson, via{' '}
            <a href={util.getEntityURL('signal technology foundation')}>
              Signal
            </a>{' '}
            at <code className="text-dark">@poulson.01</code>. For anything
            sensitive, please use a personal device that has never had your
            employer's software installed on it.
          </p>
          <p className="text-start mt-2 mb-0">
            You can donate to Tech Inquiry through its fiscal sponsor, Signals
            Network, at{' '}
            <a href="https://give-usa.keela.co/tsn-keelapay-donation">
              this link
            </a>{' '}
            by selecting 'Tech Inquiry Organization' from the drop-down menu.
          </p>
          <p className="text-start mt-2 mb-0">
            Tech Inquiry's data and software is{' '}
            <a href="https://gitlab.com/tech-inquiry/InfluenceExplorer">
              open sourced
            </a>{' '}
            -- though the documentation is outdated -- and we also make our most
            recently trained embeddings available upon request.
          </p>
          <p className="text-start mt-2 mb-0">
            Some of our icons are sourced from{' '}
            <a href="https://www.flaticon.com/">Flaticon</a>.
          </p>
        </div>
      </div>
    )
  }
}
