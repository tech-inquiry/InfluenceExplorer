import * as React from 'react'

import { FilingFeed } from '../FilingFeed'

import { Accordion, AccordionItem, SimpleAccordionItem } from '../Accordion'
import { kForeignAgentPaths } from '../utilities/api'
import * as util from '../utilities/util'
import { appConfig } from '../utilities/constants'

// TODO(Jack Poulson): Add an iframe wrapper like for Teixeira.

export class USCentralForeignAgents extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  columns: any[]
  title: string

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL + '/' + kForeignAgentPaths['US foreign agents'].get

    this.tableID = 'ti-table-us-central-foreign-agents'
    this.title = 'US Foreign Agents'

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)
    this.actOnItem = this.actOnItem.bind(this)

    this.label = this.props.label
      ? this.props.label
      : 'us-central-foreign-agents'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Date Stamped' },
      { title: 'ID' },
      { title: 'Name' },
      { title: 'Type' },
      { title: 'Form Name' },
      { title: 'Foreign Principal' },
      { title: 'Country' },
      { title: 'URL', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    return [
      filingIndex,
      util.dateWithoutTimeLex(filing.date_stamped),
      filing.registrant_number,
      filing.registrant_name,
      filing.document_type,
      filing.short_form_name,
      filing.foreign_principal_name,
      filing.foreign_principal_country,
      filing.url,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  actOnItem(filing) {
    window.open(filing.url)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        actOnItem={this.actOnItem}
      />
    )
  }
}
