import * as React from 'react'

import { USCentralForeignAgents } from './USCentral'
import * as ui from '../utilities/ui'
import { coerce } from '../utilities/util'
import { appConfig } from '../utilities/constants'

class ForeignAgentsRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = coerce(this.props.label, 'foreign-agents')
    this.sourceLabel = this.sourceLabel.bind(this)
    this.sourceModalId = this.sourceModalId.bind(this)
  }

  sourceLabel(source) {
    return `${this.label}-${source}`
  }

  sourceModalId(source) {
    return ui.modalId(this.sourceLabel(source))
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    const isEntity = profile != undefined
    const sources = this.props.sources

    const that = this

    const capImage = (
      <div
        className="mx-auto mb-0 ti-cap-img"
        style={{
          opacity: 1.0,
          height: '100px',
          maxWidth: '250px',
          backgroundImage: 'url("https://techinquiry.org/logos/foreign.png")',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: '50% 50%',
          backgroundSize: 'contain',
        }}
      ></div>
    )

    const footerText = isEntity ? (
      <>
        Tech Inquiry has foreign agent registrations for{' '}
        <span className="fst-italic">
          {profile.entityAnnotations[info.query].stylizedText}
        </span>{' '}
        from{' '}
        <span className="fw-bold">
          {sources.length + ' '}
          {sources.length > 1 ? 'sources' : 'source'}
        </span>
        .
      </>
    ) : (
      <>
        Tech Inquiry has foreign agent registrations for{' '}
        <span className="fst-italic">{info.query}</span> from{' '}
        <span className="fw-bold">
          {sources.length + ' '}
          {sources.length > 1 ? 'sources' : 'source'}
        </span>
        .
      </>
    )

    // We will place all of the modal objects up top.
    let enabledSources = ['US foreign agents']

    function sourceToItem(source, index) {
      return ui.dropdownSource(
        source,
        index,
        enabledSources,
        undefined,
        that.sourceModalId
      )
    }

    return (
      <ui.RowHelper
        capImage={capImage}
        button={ui.getCardDropdownButton(
          'Foreign agents',
          `${this.label}-dropdownMenuButton`,
          sources.map(sourceToItem)
        )}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

class ForeignAgentsModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = coerce(this.props.label, 'foreign-agents')
    this.sourceLabel = this.sourceLabel.bind(this)
  }

  sourceLabel(source) {
    return `${this.label}-${source}`
  }

  render() {
    const info = this.props.info
    const isEntity = info.profile != undefined
    return (
      <USCentralForeignAgents
        label={this.sourceLabel('US foreign agents')}
        query={info.query}
        isAgency={false}
        isEntity={isEntity}
        isTag={info.isTag}
      />
    )
  }
}

export function insertCardAndModal(
  info,
  sources,
  cards: any[],
  modals: any[]
): void {
  if (!sources.length) {
    return
  }
  cards.push(<ForeignAgentsRow info={info} sources={sources} />)
  modals.push(<ForeignAgentsModal info={info} />)
}
