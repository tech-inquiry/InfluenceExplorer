import * as React from 'react'

import * as afscInvestigate from './entityLinks/AFSCInvestigate'
import * as associations from './AssociationsRow'
import * as book from './BookRow'
import * as children from './ChildrenRow'
import * as entities from './EntitiesRow'
import * as foreignAgents from './foreignAgents/ForeignAgents'
import * as journalism from './JournalismRow'
import * as laborRelations from './laborRelations/LaborRelations'
import * as leaks from './leaks/Leaks'
import * as lobbying from './lobbying/Lobbying'
import * as neighbors from './NeighborsRow'
import * as parents from './ParentsRow'
import * as procurement from './procurement/Procurement'
import * as securities from './securities/Securities'
import * as webtext from './WebtextRow'

// Rather than importing in a single alphabetical list, we separate out this
// group because its usage is simpler due to the lack of modals and feeds.
import * as collectiveAction from './entityLinks/CollectiveAction'
import * as corpWatch from './entityLinks/CorpWatch'
import * as littleSis from './entityLinks/LittleSis'
import * as openCorporates from './entityLinks/OpenCorporates'
import * as openSecrets from './entityLinks/OpenSecrets'
import * as paywatch from './entityLinks/Paywatch'
import * as proPublicaNonprofitExplorer from './entityLinks/ProPublicaNonprofitExplorer'
import * as splcExtremist from './entityLinks/SPLCExtremist'
import * as violationTracker from './entityLinks/ViolationTracker'
import * as whoProfits from './entityLinks/WhoProfits'

function noResultsCard(query: string): any {
  return (
    <div className="card">
      <div className="card-body">
        <p className="card-text">
          {'No results were found for the query "'}
          <span className="fst-italic">{query}</span>
          {'".'}
        </p>
      </div>
    </div>
  )
}

export function getInitialCardsAndModals(
  query,
  entityToItem,
  profile = undefined,
  isTag = false,
  development = false
): any[] {
  const info = {
    query: query,
    entityToItem: entityToItem,
    profile: profile,
    isTag: isTag,
    development: development,
  }

  let cards = []
  let modals = []

  // TODO(Jack Poulson): Consider iterating over the list of modules.
  book.insertCardAndModal(info, cards, modals)
  journalism.insertCardAndModal(info, true, cards, modals)
  journalism.insertCardAndModal(info, false, cards, modals)
  parents.insertCardAndModal(info, cards, modals)
  children.insertCardAndModal(info, cards, modals)
  associations.insertCardAndModal(info, cards, modals)
  neighbors.insertCardAndModal(info, cards, modals)

  // TODO(Jack Poulson): Consider iterating over the list of modules.
  openCorporates.insertCard(info, cards)
  splcExtremist.insertCard(info, cards)
  afscInvestigate.insertCard(info, cards)
  whoProfits.insertCard(info, cards)
  openSecrets.insertCard(info, cards)
  collectiveAction.insertCard(info, cards)
  violationTracker.insertCard(info, cards)
  paywatch.insertCard(info, cards)
  corpWatch.insertCard(info, cards)
  proPublicaNonprofitExplorer.insertCard(info, cards)
  littleSis.insertCard(info, cards)

  cards = cards.filter((card) => card)
  return [cards, modals]
}

export async function getCardsAndModals(
  query,
  sources,
  entityToItem,
  profile = undefined,
  isTag = false,
  development = false
) {
  const info = {
    query: query,
    entityToItem: entityToItem,
    profile: profile,
    isTag: isTag,
    development: development,
  }

  let cards = []
  let modals = []

  await entities.insertCardAndModal(info, cards, modals)
  book.insertCardAndModal(info, cards, modals)
  journalism.insertCardAndModal(info, true, cards, modals)
  journalism.insertCardAndModal(info, false, cards, modals)
  parents.insertCardAndModal(info, cards, modals)
  children.insertCardAndModal(info, cards, modals)
  associations.insertCardAndModal(info, cards, modals)
  neighbors.insertCardAndModal(info, cards, modals)
  openCorporates.insertCard(info, cards)
  procurement.insertCardAndModal(info, sources.procurement, cards, modals)
  splcExtremist.insertCard(info, cards)
  afscInvestigate.insertCard(info, cards)
  whoProfits.insertCard(info, cards)
  lobbying.insertCardAndModal(info, sources.lobbying, cards, modals)
  leaks.insertCardAndModal(info, sources.leaks, cards, modals)
  openSecrets.insertCard(info, cards)
  securities.insertCardAndModal(info, sources.securities, cards, modals)
  foreignAgents.insertCardAndModal(info, sources.foreignAgents, cards, modals)
  laborRelations.insertCardAndModal(info, sources.laborRelations, cards, modals)
  collectiveAction.insertCard(info, cards)
  violationTracker.insertCard(info, cards)
  paywatch.insertCard(info, cards)
  corpWatch.insertCard(info, cards)
  proPublicaNonprofitExplorer.insertCard(info, cards)
  littleSis.insertCard(info, cards)
  await webtext.insertCardAndModal(info, cards, modals)

  cards = cards.filter((card) => card)
  if (!cards.length) {
    cards = [noResultsCard(query)]
  }
  return [cards, modals]
}

export function getLeakCardsAndModals(
  query,
  sources,
  entityToItem,
  profile = undefined,
  isTag = false,
  development = false
): any[] {
  const info = {
    query: query,
    entityToItem: entityToItem,
    profile: profile,
    isTag: isTag,
    development: development,
  }

  let cards = []
  let modals = []

  leaks.insertCardAndModal(info, sources.leaks, cards, modals)

  cards = cards.filter((card) => card)
  if (!cards.length) {
    cards = [noResultsCard(query)]
  }
  return [cards, modals]
}
