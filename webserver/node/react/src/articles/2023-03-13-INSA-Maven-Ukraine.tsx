import * as React from 'react'

export const title = `Where Drone Warfare Intersects with Ukraine and U.S. Information Operations`

export const content = (
  <div className="card">
    <img
      className="card-img-top"
      src="https://techinquiry.org/logos/insa_maven_ukraine.png"
    />
    <div className="card-body ti-article mt-0">
      <h2>
        Where Drone Warfare Intersects with Ukraine and U.S. Information
        Operations
      </h2>
      <h4>
        The National Geospatial-Intelligence Agency disclosed in newly public
        recordings how Project Maven was deployed in Ukraine with the goal of
        decreasing 'Find, Fix, Finish' cycles down to less than ten minutes.
        After an off-the-record panel on disinformation involving prominent
        researchers from Graphika and Stanford, the agency also discussed how it
        is funded by Congress to publish and promote research critical of China
        in partnership with universities.
      </h4>
      <h4 className="mt-3">
        Jack Poulson{' '}
        <span style={{ fontSize: '1.2rem' }}>
          [Email: <code className="text-dark">jack@techinquiry.org</code>,{' '}
          Signal: <code className="text-dark">+1.646.733.6810</code>]
        </span>
        <br />
        2023-03-13, 11:37pm ET
      </h4>

      <div>
        <p className="card-text ti-article mt-5">
          The Intelligence and National Security Alliance -- or{' '}
          <a href="https://www.insaonline.org/">INSA</a> -- is arguably the
          central networking nonprofit for the U.S. Intelligence Community. And
          its summits are sometimes even{' '}
          <a href="https://events.jspargo.com/inss18/public/Content.aspx?ID=74472">
            formally tied
          </a>{' '}
          to classified briefings.
        </p>
        <p className="card-text ti-article mt-3">
          In the opening keynote of a{' '}
          <a href="https://www.youtube.com/watch?v=JHLUdsDzTvQ">
            video recording
          </a>{' '}
          of INSA's{' '}
          <a href="https://web.archive.org/web/20230213112146/https://www.insaonline.org/detail-pages/event/2023/03/08/default-calendar/spring-symposium-emerging-technologies">
            March 8th Summit
          </a>{' '}
          which was published this morning, an Associate Director of the
          National Geospatial-Intelligence Agency{' '}
          <a href="https://youtu.be/JHLUdsDzTvQ?t=2831">disclosed</a> that the
          Pentagon's{' '}
          <a href="https://www.nytimes.com/2018/06/01/technology/google-pentagon-project-maven.html">
            controversial
          </a>{' '}
          artificial intelligence drone warfare effort -- Project Maven, which
          was{' '}
          <a href="https://federalnewsnetwork.com/intelligence-community/2022/04/pentagon-shifting-project-maven-marquee-artificial-intelligence-initiative-to-nga/">
            taken over
          </a>{' '}
          by the NGA's{' '}
          <a href="https://usgif.org/biography/jim-mccool/">
            Data and Digital Innovation
          </a>{' '}
          Directorate in{' '}
          <a href="https://www.youtube.com/watch?v=JHLUdsDzTvQ&t=1885s">
            January 2023
          </a>{' '}
          -- was deployed by{' '}
          <em>
            "a military partner who, in fact, took some of these technologies to
            Europe under the banner of our monitoring of the Ukraine crisis"
          </em>
          . (Beyond a lunch sponsored by Oracle and a snack break sponsored by
          Google, Google Director of Strategic Initiatives{' '}
          <a href="https://www.defenseone.com/technology/2020/05/defense-innovation-board-director-moves-google/165086/">
            Joshua Marcuse
          </a>{' '}
          was also a{' '}
          <a href="https://www.youtube.com/watch?v=JHLUdsDzTvQ&t=4245s">
            speaker
          </a>
          . As was a member of the Pentagon's{' '}
          <a href="https://www.cto.mil/osc/">Office of Strategic Capital</a>,
          which Tech Inquiry yesterday{' '}
          <a href="https://techinquiry.org/?article=osc-svb">reported</a> as
          advocating for its "close friends" impacted by the failure of Silicon
          Valley Bank.)
        </p>
        <p className="card-text ti-article mt-3">
          <a href="https://www.youtube.com/watch?v=JHLUdsDzTvQ&t=1885s">
            According to
          </a>{' '}
          NGA Associate Director for Capabilities{' '}
          <a href="https://usgif.org/biography/phil-chudoba/">
            Phillip C. Chudoba
          </a>
          , Maven is{' '}
          <em>
            "presently, the only performant computer vision A.I. and M.L.
            program in the DoD"
          </em>{' '}
          and <a href="https://youtu.be/JHLUdsDzTvQ?t=2778">its goal</a> is to
          decrease the time required to find and attack a target from hours down
          to less than ten minutes,{' '}
          <em>
            "so that we can execute those find, fix, and finish cycles before
            our competitors can leave the target area"
          </em>
          . (Reuters{' '}
          <a href="https://www.reuters.com/technology/ukraine-is-using-palantirs-software-targeting-ceo-says-2023-02-02/">
            reported
          </a>{' '}
          last month the data fusion intelligence contractor Palantir -- a{' '}
          <a href="https://www.cnet.com/tech/palantir-extends-us-defense-contract-that-prompted-protest-at-google/">
            prominent
          </a>{' '}
          Project Maven contractor -- was{' '}
          <em>"responsible for most of the targeting in Ukraine"</em>.)
        </p>
        <p className="card-text ti-article mt-3">
          Chudoba traced back the deployment of Maven in Ukraine to the XVIII
          Airbone Corp's{' '}
          <a href="https://executivegov.com/2021/10/army-conducts-ai-enabled-target-identification-exercise-under-scarlet-dragon/">
            Scarlet Dragon
          </a>{' '}
          exercises run by Erik Kurilla, who{' '}
          <a href="https://www.defense.gov/News/News-Stories/Article/Article/2987059/former-centcom-chief-of-staff-assumes-role-of-commander/">
            became
          </a>{' '}
          the leader of U.S. Central Command in April 2022. According to a{' '}
          <a href="https://youtu.be/JHLUdsDzTvQ?t=8761s">later keynote</a> by
          the former director of the Pentagon's Joint Artificial Intelligence
          Center, Michael Groen, Kurilla is an <em>"AI enthusiast"</em> and the
          experimental A.I. programs underneath his CENTCOM umbrella within the
          Navy and Air Force (Task Forces{' '}
          <a href="https://www.cusnc.navy.mil/Task-Forces/">59</a> and{' '}
          <a href="https://www.af.mil/News/Article-Display/Article/3235960/afcents-innovation-task-force-99-establishes-ops-hq/">
            99
          </a>
          ) are direct results of Project Maven.
        </p>
        <p className="card-text ti-article mt-3">
          Chudoba's opening keynote was followed by the only off-the-record
          session of the summit: a panel on "Technology and
          disinformation/misinformation" whose participants included the Acting
          Director of the Office of the Director of National Intelligence's{' '}
          <a href="https://www.dni.gov/index.php/nctc-who-we-are/organization/340-about/organization/foreign-malign-influence-center">
            Foreign Malign Influence Center
          </a>
          , Jeffrey Wichman, as well as prominent disinformation researcher{' '}
          <a href="https://www.nytimes.com/2018/12/19/us/alabama-senate-roy-jones-russia.html">
            Renee DiResta
          </a>{' '}
          and Graphika CTO{' '}
          <a href="https://graphika.com/team/jennifer-mathieu">
            Jennifer Mathieu
          </a>
          . But{' '}
          <a href="https://www.usaspending.gov/award/CONT_AWD_FA864922P1137_9700_-NONE-_-NONE-">
            public records
          </a>{' '}
          show that Graphika's subsidiary, Octant Data, was paid $748,352 by the
          U.S. Air Force for an ongoing contract on{' '}
          <em>
            "detecting, forecasting, and countering Chinese Information
            Operations (IO) to undermine the national security of the United
            States"
          </em>
          .
        </p>
        <p className="card-text ti-article mt-3">
          The only public session with a focus on information operations was the
          closing panel, moderated by the former Deputy Director of the CIA's
          Open Source Center, Kristen Wood. One of the panelists, the founder of
          the NGA's open source initiative{' '}
          <a href="https://www.tearline.mil/">"Tearline.mil"</a>, Chris
          Rasmussen, explained that one of the Congressionally{' '}
          <a href="https://youtu.be/JHLUdsDzTvQ?t=14391">mandated</a> priorities
          of Tearline is{' '}
          <em>
            "to crank out more Tearline and public content on China's malign
            activities and get it into the public space"
          </em>
          . Tearline's website{' '}
          <a href="https://www.tearline.mil/about-tearline/">asserts</a> that
          the project{' '}
          <em>
            "builds on the precedent of the{' '}
            <a href="https://www.cia.gov/the-world-factbook/">
              CIA World Factbook
            </a>
            "
          </em>
          , and Rasmussen described in the panel how the scope includes{' '}
          <em>"shame and blame"</em> pressure campaigns on Uyghur abuses and
          providing information to <em>"officers at Nike"</em>.
        </p>
        <p className="card-text ti-article mt-3">
          Another member of the closing panel,{' '}
          <a href="https://www.linkedin.com/in/devon-blake/">Devon Blake</a>,
          explained how her employer,{' '}
          <a href="https://www.wsj.com/articles/app-taps-unwitting-users-abroad-to-gather-open-source-intelligence-11624544026">
            gig-work surveillance
          </a>{' '}
          contractor Premise Data, is a substitute for{' '}
          <a href="https://youtu.be/JHLUdsDzTvQ?t=14474">
            "boots on the ground in remote areas"
          </a>{' '}
          to help avoid "mis and disinformation" as well as to monitor
          "potential protests". Blake also{' '}
          <a href="https://youtu.be/JHLUdsDzTvQ?t=14003">recounted</a> her time
          as the commander of Army Europe's{' '}
          <a href="https://www.inscom.army.mil/MSC/66MIB/index.html?csrt=5097727540633546431">
            66th Military Intelligence Brigade
          </a>{' '}
          during the 2016 to 2018 pilot of a "multi-national" force "looking
          east" which{' '}
          <em>
            "debrief[ed] Ukrainian soldiers...that were coming off of the
            eastern front. And that was not in the news."
          </em>
          .{' '}
          <em>
            "That program eventually turned into{' '}
            <a href="https://mipb.army.mil/documents/12618257/13743252/skilling_mapping+info_online.pdf/2c548b70-8bb5-4822-8821-51b51dc76d02">
              Northern Raven
            </a>
            ...they are tremendously effective right now for the efforts in
            Ukraine."
          </em>
        </p>
        <p className="card-text ti-article mt-3">
          Tearline founder Rasmussen also{' '}
          <a href="https://youtu.be/JHLUdsDzTvQ?t=16518">promoted</a> the usage
          of satellite imagery collected from frequencies outside of the visual
          spectrum by commercial firm Hawkeye 360, which is{' '}
          <a href="https://www.he360.com/hawkeye-360-expands-advisory-board-with-addition-of-robert-cardillo/">
            advised
          </a>{' '}
          by former NGA Director Robert Cardillo. Rasmussen's reference to
          Hawkeye 360 appears to complement Chudoba's{' '}
          <a href="https://youtu.be/JHLUdsDzTvQ?t=2973">claim</a> that the U.S.
          has{' '}
          <em>
            "a heavy reliance on non-[electro-optical] imaging systems to see
            what's happening [in Ukraine]...Our reliance on some of the radar
            capabilities kind of has been driven home."
          </em>
        </p>
        <p className="card-text ti-article mt-3">
          Tech Inquiry was pointedly rejected from attending the INSA Summit on
          the grounds that we are not "confirmed press". But, to{' '}
          <a href="https://youtu.be/JHLUdsDzTvQ?t=2486">paraphrase</a> the
          opening keynote of NGA's Chudoba, "[professionals] watch the tapes".
        </p>
      </div>
    </div>
  </div>
)
