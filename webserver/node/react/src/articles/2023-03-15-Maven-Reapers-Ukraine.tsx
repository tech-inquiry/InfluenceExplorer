import * as React from 'react'

export const title = `What We Know About Project Maven, Reapers, and Ukraine`

export const content = (
  <div className="card">
    <img
      className="card-img-top"
      src="https://techinquiry.org/logos/maven_dod_ig_2022-01-06.png"
    />
    <div className="card-body ti-article mt-0">
      <h2>What We Know About Project Maven, Reapers, and Ukraine</h2>
      <h4>
        Given Tuesday's collision between a U.S. MQ-9 Reaper and a Russian
        fighter jet, as well as last week's confirmation by the NGA of Project
        Maven's usage in Ukraine, Tech Inquiry reviews what we know about
        Project Maven's usage in Reapers and in Ukraine.
      </h4>
      <h4 className="mt-3">
        Jack Poulson{' '}
        <span style={{ fontSize: '1.2rem' }}>
          [Email: <code className="text-dark">jack@techinquiry.org</code>,{' '}
          Signal: <code className="text-dark">+1.646.733.6810</code>]
        </span>
        <br />
        2023-03-15, 1:15pm ET
      </h4>
      <h4 className="mt-3">
        [2023-03-15, 1:57pm ET]{' '}
        <em>
          An aside was added on the founding of Rebellion Defense in the context
          of Google downgrading its work on Project Maven.
        </em>
      </h4>

      <div>
        <p className="card-text ti-article mt-5">
          Late Monday night Tech Inquiry{' '}
          <a href="https://techinquiry.org/?article=insa-maven-ukraine">
            reported
          </a>{' '}
          on an Assistant Director of the U.S. National Geospatial-Intelligence
          Agency <a href="https://youtu.be/JHLUdsDzTvQ?t=2831">disclosing</a>{' '}
          the deployment of the Pentagon's A.I. drone targeting program, Project
          Maven, in Ukraine. The next day, a U.S.{' '}
          <a href="https://www.cnn.com/2023/03/15/europe/reaper-drone-russia-us-ukraine-explainer-intl/index.html">
            MQ-9 Reaper
          </a>{' '}
          drone{' '}
          <a href="https://apnews.com/article/ukraine-russia-war-kramatorsk-peace-plan-fde09a8ea4d9b692637ad8f76cd05f3a">
            crashed
          </a>{' '}
          into the Black Sea after coming in close contact with a Russian
          fighter jet.
        </p>
        <p className="card-text ti-article mt-3">
          Tech Inquiry{' '}
          <a href="https://techinquiry.org/EasyAsPAI/resources/EasyAsPAI.pdf">
            reported
          </a>{' '}
          on Project Maven's integration with the{' '}
          <a href="https://www.ga-asi.com/radars/lynx-multi-mode-radar">Lynx</a>{' '}
          Synthetic Aperture Radar (SAR) of MQ-9 Reapers in September 2021
          through a detailed analysis of public procurement records. As a result
          of 90 day disclosure delays, it became public today that one of the
          primary Project Maven contracts -- codenamed "Pavement + Cadillac" --
          was increased by more than $9 million in December. (Tech Inquiry was
          the first to{' '}
          <a href="https://techinquiry.org/EasyAsPAI/resources/EasyAsPAI.pdf">
            report
          </a>{' '}
          the Project Maven contract codenames. And while a January 2022{' '}
          <a href="https://media.defense.gov/2022/Jan/10/2002919460/-1/-1/1/DODIG-2022-049.REDACTED.PDF">
            report
          </a>{' '}
          from the Inspector General (IG) of the Department of Defense confirmed
          two of them, the IG denied Tech Inquiry's Freedom of Information
          request for the more detailed classified report based upon a December
          18, 2018 determination exempting all "Project Maven infrastructure".)
        </p>
        <p className="card-text ti-article mt-3">
          Despite public narratives centering on Project Maven's usage of
          artificial intelligence to automate the analysis of drone surveillance
          footage for targeting, in 2021 Tech Inquiry reported that public
          disclosures of Project Maven's subcontracts revealed a broader focus
          including the analysis of satellite imagery, Synthetic Aperture Radar,
          social media, and 'Captured Enemy Materials' (CEM). The typical
          term-of-art for the fusion of such a broad range of surveillance
          capabilities is{' '}
          <a href="https://csrc.nist.gov/glossary/term/all_source_intelligence">
            all-source intelligence
          </a>
          . (Out of the three prime awards analyzed by Tech Inquiry, Microsoft
          was the largest recipient and was paid $31.6 million for work
          including the "automat[ing] and augmentat[ing] the analysis of [Wide
          Area Motion Imagery]" and "[Electro-optical] and [infrared] [Full
          Motion Video] [Machine Learning]".)
        </p>
        <p className="card-text ti-article mt-3">
          But the potential military escalation caused by Tuesday's{' '}
          <a href="https://apnews.com/article/russia-us-reaper-drone-collision-501614b07b504fa5c609e64146e6ab4f">
            crash
          </a>{' '}
          of a U.S. MQ-9 Reaper, which a U.S. defense official said was over the
          Black Sea west of Crimea, brings fresh relevance to $1.95 million in
          Project Maven subcontracts paid to the embattled A.I. defense and
          intelligence contractor Rebellion Defense. Notably, a $600,000{' '}
          <a href="https://www.usaspending.gov/award/CONT_AWD_W911QX20C0023_9700_-NONE-_-NONE-">
            subaward
          </a>{' '}
          signed on January 4, 2021 was to{' '}
          <em>
            "Improve MQ-9 Lynx Synthetic Aperture Radar (SAR) imagery labeling
            quality through Rebellion Defense's SAR colorization methodology"
          </em>
          .
        </p>
        <p className="card-text ti-article mt-3">
          (Rebellion Defense was{' '}
          <a href="https://rebelliondefense.com/chris-lynch">founded</a> by
          former Director of the Defense Digital Service Chris Lynch with the{' '}
          <a href="https://www.ncsc.gov.uk/blog-post/how-rebellion-defence-accelerate-product-development">
            backing
          </a>{' '}
          of former Google CEO Eric Schmidt, largely in the{' '}
          <a href="https://www.wired.com/story/inside-the-pentagons-plan-to-win-over-silicon-valleys-ai-experts/">
            context
          </a>{' '}
          of Google's{' '}
          <a href="https://theintercept.com/2019/03/01/google-project-maven-contract/">
            backpedalling
          </a>{' '}
          on Project Maven in 2018. But, as was{' '}
          <a href="https://techinquiry.org/?article=google-aerial">reported</a>{' '}
          by Tech Inquiry on Friday, new contract{' '}
          <a href="https://www.fpds.gov/ezsearch/search.do?indexName=awardfull&templateName=1.5.3&s=FPDS.GOV&q=HQ08452290039+9700+">
            disclosures
          </a>{' '}
          reveal that Google resumed selling A.I. for processing aerial imagery
          to the Pentagon.)
        </p>
        <p className="card-text ti-article mt-3">
          Rebellion's work on Project Maven, as well as its{' '}
          <a href="https://www.usaspending.gov/award/CONT_AWD_47QFCA18F0067_4732_GS00Q14OADU401_4732">
            $633,600 subaward
          </a>{' '}
          on a Special Operations Command{' '}
          <a href="https://techinquiry.org/FOIA/Alion-JCETII.pdf">contract</a>{' '}
          also revealed by Tech Inquiry, were the subject of a Vox{' '}
          <a href="https://www.vox.com/recode/23507236/inside-disruption-rebellion-defense-washington-connected-military-tech-startup">
            exposé
          </a>{' '}
          in December. According to Vox,{' '}
          <em>
            "[Rebellion's] experiment failed, and the Pentagon discontinued the
            contract, according to a former senior Pentagon official familiar
            with the contract."
          </em>
        </p>
        <p className="card-text ti-article mt-3">
          But the usage of electromagnetic surveillance systems operating
          outside of the visual range was emphasized by the NGA as a key
          component of U.S. situational awareness in Ukraine last week.
          According to NGA Assistant Secretary for Capabilities{' '}
          <a href="https://youtu.be/JHLUdsDzTvQ?t=2973">Phillip C. Chudoba</a>,
          the U.S. has{' '}
          <em>
            "a heavy reliance on non-[electro-optical] imaging systems to see
            what's happening [in Ukraine]...Our reliance on some of the radar
            capabilities kind of has been driven home."
          </em>{' '}
          And the founder of the NGA's open source intelligence project, Chris
          Rasmussen, went so far as to{' '}
          <a href="https://youtu.be/JHLUdsDzTvQ?t=16518">name</a>{' '}
          <a href="https://techinquiry.org/?entity=hawkeye%20360%2C%20inc%2E&guard=">
            Hawkeye 360
          </a>{' '}
          as an important provider of such technology.
        </p>
        <p className="card-text ti-article mt-3">
          Given the NGA's{' '}
          <a href="https://techinquiry.org/?article=insa-maven-ukraine">
            disclosure
          </a>{' '}
          last week that Project Maven was deployed in Ukraine by a "military
          partner", and the existence of public contract notices detailing
          Project Maven's integration into the Synthetic Aperture Radar of MQ-9
          Reapers, it is worth determining whether Project Maven inadvertently
          contributed to one of the most significant military escalations of our
          time.
        </p>
      </div>
    </div>
  </div>
)
