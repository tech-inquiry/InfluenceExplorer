import * as React from 'react'

export const title = `Oracle Overtakes Microsoft in Pentagon Joint Warfighting Cloud Capability Income`

export const content = (
  <div className="card">
    <img
      className="card-img-top"
      src="https://techinquiry.org/logos/pentagon_oracle.webp"
    />
    <div className="card-body ti-article mt-0">
      <h2>
        Oracle Overtakes Microsoft in Pentagon Joint Warfighting Cloud
        Capability Income
      </h2>
      <h4>
        A newly public contract modification increased Oracle's obligations
        under the Pentagon's $9 billion ceiling Joint Warfighting Cloud
        Capability (JWCC) contract up to roughly $4.45 million, exceeding
        Microsoft's $3.598 million.
      </h4>
      <h4 className="mt-3">
        Jack Poulson{' '}
        <span style={{ fontSize: '1.2rem' }}>
          [Email: <code className="text-dark">jack@techinquiry.org</code>,{' '}
          Signal: <code className="text-dark">+1.646.733.6810</code>]
        </span>
        <br />
        2023-03-23, 10:19am ET
      </h4>

      <div>
        <p className="card-text ti-article mt-5">
          Continually evaluating companies' expected income from multi-billion
          dollar Pentagon contracts is part{' '}
          <a href="https://techinquiry.org/docs/InternationalCloud.pdf">
            art form
          </a>{' '}
          and part horse race. And, in terms of hard cash, a{' '}
          <a href="https://www.fpds.gov/ezsearch/search.do?indexName=awardfull&templateName=1.5.3&s=FPDS.GOV&q=HC105023F0003+9700+">
            contract modification
          </a>{' '}
          made public this morning shows that Oracle pulled ahead of Microsoft
          in the Pentagon's long-anticipated $9 billion successor to its{' '}
          <a href="https://www.nytimes.com/2021/07/06/technology/JEDI-contract-cancelled.html">
            cancelled
          </a>{' '}
          JEDI cloud computing award, the Joint Warfighting Cloud Capability
          contract (or, JWCC).
        </p>
        <p className="card-text ti-article mt-3">
          Before contract modifications became public, it would only be fair to
          assume an equal split of the potential $9 billion JWCC pie between the
          four receipients: Amazon, Microsoft, Oracle, and Google (indeed, this
          was Tech Inquiry's approach in a{' '}
          <a href="https://techinquiry.org/docs/InternationalCloud.pdf">
            report
          </a>{' '}
          last year). But, as a result of 90-day delays in proactive Department
          of Defense procurement disclosures, delivery orders revealing more
          information on the financial split started becoming public this month.
          (Tech Inquiry was, to the best of our knowledge, the first to{' '}
          <a href="https://techinquiry.org/?article=google-aerial">report</a>{' '}
          the initial split, with Microsoft in the lead.)
        </p>
        <p className="card-text ti-article mt-3">
          As of today, public records for JWCC show that Oracle has been
          obligated $4,445,952.98, Microsoft $3,598,000, Google $914,800, and
          Amazon a mere $100,000. However, each company's delivery orders
          underneath the shared $9 billion ceiling contract have their own
          maximum payout: roughly $70.77 million for Microsoft, $40.95 million
          for Oracle, and $16.84 million for Google. (Amazon still has no public
          delivery order under JWCC.)
        </p>
        <p className="card-text ti-article mt-3">
          Each of these three delivery orders has a completion date of December
          7, 2023, and so their ceilings serve as a reasonable estimate of the
          expected amounts each company should receive on JWCC by the end of the
          year. Thus, while Oracle is currently ahead on obligations,
          Microsoft's larger delivery order ceiling suggests that Microsoft is
          likely to overtake Oracle again in the coming months.
        </p>
        <p className="card-text ti-article mt-3">
          You can view Tech Inquiry's feed on the four JWCC awards to Microsoft,
          Oracle, Google, and Amazon{' '}
          <a href="https://techinquiry.org/?text=HQ003423D0020%20OR%20HQ003423D0018%20OR%20HQ003423D0017%20OR%20HQ003423D0019&guard=">
            here
          </a>
          . As it stands, perhaps the biggest surprise is that Amazon has yet to
          receive a public delivery order on JWCC.
        </p>
      </div>
    </div>
  </div>
)
