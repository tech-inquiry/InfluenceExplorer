import * as React from 'react'

export const title = `Indian Affairs Claims Cobwebs Tangles is the Only Platform for its Cellphone Location-Tracking and Dark Web Surveillance`

export const content = (
  <div className="card">
    <img
      className="card-img-top"
      src="https://techinquiry.org/logos/cobwebs_technologies_ltd.png"
    />
    <div className="card-body ti-article mt-0">
      <h2>
        Indian Affairs Claims Cobwebs Tangles is the Only Platform for its
        Cellphone Location-Tracking and Dark Web Surveillance
      </h2>
      <h4>
        The Bureau of Indian Affairs' Office of Justice Services (OJS) announced
        this month its intent to buy the cellphone location-tracking and dark
        web surveillance capabilities of Cobwebs Technologies. OJS claims that
        Cobwebs{' '}
        <em>
          "is the only platform that will meet the Government’s needs as it can
          combine internet (open, deep, and dark web) intelligence"
        </em>
        .
      </h4>
      <h4 className="mt-3">
        Jack Poulson{' '}
        <span style={{ fontSize: '1.2rem' }}>
          [Email: <code className="text-dark">jack@techinquiry.org</code>,{' '}
          Signal: <code className="text-dark">+1.646.733.6810</code>]
        </span>
        <br />
        2023-03-21, 12:20pm ET
      </h4>

      <div>
        <p className="card-text ti-article mt-5">
          Two weeks ago, the Office of Justice Services -- or{' '}
          <a href="https://www.bia.gov/bia/ojs">OJS</a> -- component of the
          Bureau of Indian Affairs{' '}
          <a href="https://sam.gov/opp/4c8243eef3c440df95391e8083624531/view">
            announced
          </a>{' '}
          that the Tangles platform of Israel-based{' '}
          <a href="https://techinquiry.org/?entity=cobwebs%20technologies%20ltd%2E&guard=">
            Cobwebs Technologies
          </a>{' '}
          was the only platform capable of meeting the office's cellphone
          location-tracking and internet surveillance needs. (Any competing
          firms which seek to bid on the contract -- such as{' '}
          <a href="https://theintercept.com/2021/11/04/treasury-surveillance-location-data-babel-street/">
            Babel Street
          </a>{' '}
          -- are required to email their capability statement to the contracting
          specialist, Stephanie Scalise, by tomorrow at 3pm ET.)
        </p>
        <p className="card-text ti-article mt-3">
          <a href="https://sam.gov/opp/4c8243eef3c440df95391e8083624531/view">
            <img
              className="card-img-top"
              src="https://techinquiry.org/logos/cobwebs_office_of_justice_services.png"
            />
          </a>
        </p>
        <p className="card-text ti-article mt-3">
          Last month Motherboard{' '}
          <a href="https://www.vice.com/en/article/xgynn4/company-helping-irs-go-undercover-cobwebs-technologies">
            reported
          </a>{' '}
          on the Criminal Investigations unit of the U.S. Internal Revenue
          Service buying Tangles to aid in its undercover operations. OJS plays
          an analogous role within the Bureau of Indian Affairs, with a mission
          including{' '}
          <em>
            "enforcing laws, maintaining justice and order, and by ensuring that
            sentenced American Indian offenders are confined in safe, secure,
            and humane environments."
          </em>{' '}
          (The Federal Bureau of Investigation and Bureau of Indian Affairs{' '}
          <a href="https://www.justice.gov/opa/pr/fbi-and-bureau-indian-affairs-sign-agreement-improve-law-enforcement-indian-country">
            announced
          </a>{' '}
          an agreement in December for "the BIA Office of Justice Services and
          the FBI [to] cooperate on investigations and share information and
          investigative reports".)
        </p>
        <p className="card-text ti-article mt-3">
          Tech Inquiry is also{' '}
          <a href="https://techinquiry.org/FOIA/Cobwebs-ICE-2022-ICFO-30651.pdf">
            releasing
          </a>{' '}
          its (redacted) copy of the emails and contract associated with U.S.
          Immigration and Customs Enforcement's{' '}
          <a href="https://www.usaspending.gov/award/CONT_AWD_70CMSD22P00000108_7012_-NONE-_-NONE-">
            ongoing
          </a>{' '}
          purchase order for Cobwebs' "Gold Tangles & WebLoc". While Cobwebs'
          contracts with ICE and the IRS have respectively paid out{' '}
          <a href="https://www.usaspending.gov/award/CONT_AWD_70CMSD22P00000108_7012_-NONE-_-NONE-">
            $225,060
          </a>{' '}
          and{' '}
          <a href="https://www.usaspending.gov/award/CONT_AWD_205AE922P00204_2050_-NONE-_-NONE-">
            $229,000
          </a>
          , the company sold a "Platinum Tangles and WebLoc Annual Subscription"
          to defense contractor{' '}
          <a href="https://techinquiry.org/?entity=science%20applications%20international%20corporation&guard=">
            SAIC
          </a>{' '}
          for $478,225 one year ago as part of a{' '}
          <a href="https://www.usaspending.gov/award/CONT_AWD_W9126020FD504_9700_W9113M17D0007_9700">
            subcontract
          </a>{' '}
          with the U.S. Army's{' '}
          <a href="https://techinquiry.org/?entity=united%20states%20army%20space%20and%20missile%20defense%20command&guard=">
            Space and Missile Defense Command
          </a>
          .
        </p>
        <p className="card-text ti-article mt-3">
          Cobwebs' largest known{' '}
          <a href="https://www.usaspending.gov/award/CONT_AWD_70RDAD20C00000016_7001_-NONE-_-NONE-">
            contract
          </a>{' '}
          is with the Office of Intelligence Analysis (OIA) of the Department of
          Homeland Security;{' '}
          <a href="https://techinquiry.org/?entity=dhs%20office%20of%20intelligence%20and%20analysis&guard=">
            OIA
          </a>{' '}
          is one of the{' '}
          <a href="https://www.dni.gov/index.php/what-we-do">
            eighteen members
          </a>{' '}
          of the U.S. Intelligence Community. The five-year, $2.9 million
          ceiling contract between Cobwebs and OIA began on August 25, 2020 and
          has so far paid out $1.76 million.
        </p>
        <p className="card-text ti-article mt-3">
          One of the partners Cobwebs advertises on its{' '}
          <a href="https://cobwebs.com/">homepage</a> is The Center for Advanced
          Defense Studies (
          <a href="https://techinquiry.org/?entity=the%20center%20for%20advanced%20defense%20studies%2C%20inc%2E&guard=">
            C4ADS
          </a>
          ), a digital think tank which serves as a{' '}
          <a href="https://www.youtube.com/watch?t=584&v=D-UDrFNUPSM&feature=youtu.be">
            "beta test-bed"
          </a>{' '}
          for data-fusion company{' '}
          <a href="https://techinquiry.org/?entity=palantir%20technologies%2C%20inc%2E&guard=">
            Palantir
          </a>
          . Beyond C4ADS's{' '}
          <a href="https://techinquiry.org/docs/HowWatchdogsAreSilenced.pdf">
            close ties
          </a>{' '}
          to U.S. Intelligence and Special Operations, it is a{' '}
          <a href="https://www.nytimes.com/2021/03/22/insider/north-korea-oil-supply.html">
            prominent source
          </a>{' '}
          for reporting on official U.S. adversaries' avoidance of U.S.
          sanctions. (And, according to his{' '}
          <a href="https://www.linkedin.com/in/loganpauley/">LinkedIn</a>,
          former C4ADS Analyst Logan T. Pauley currently works full-time at
          Cobwebs.)
        </p>
      </div>
    </div>
  </div>
)
