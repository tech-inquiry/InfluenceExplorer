import * as React from 'react'

export const title = `NORAD's $100 million data fusion contract for object tracking`

export const content = (
  <div className="card">
    <img
      className="card-img-top"
      src="https://techinquiry.org/logos/norad_kinetica_db.png"
    />
    <div className="card-body ti-article mt-0">
      <h2>NORAD's $100 million data fusion contract for object tracking</h2>
      <h4>
        Tech Inquiry is publishing its copy of NORAD's $100 million contract
        with data fusion company Kinetica, which specializes in real-time
        monitoring of high volumes of location-tracking data.
      </h4>
      <h4 className="mt-3">
        Jack Poulson{' '}
        <span style={{ fontSize: '1.2rem' }}>
          [Email: <code className="text-dark">jack@techinquiry.org</code>,{' '}
          Signal: <code className="text-dark">+1.646.733.6810</code>]
        </span>
        <br />
        2023-02-23, 11:05am ET
      </h4>

      <div>
        <p className="card-text ti-article mt-5">
          In February of 2021, data fusion company{' '}
          <a href="https://techinquiry.org/?entity=kinetica%20db%2C%20inc%2E&guard=">
            Kinetica
          </a>{' '}
          <a href="https://www.businesswire.com/news/home/20210202005324/en/Air-Forces-Digital-Directorate-Awards-Kinetica-a-Contract-With-100M-Ceiling-to-Provide-a-Streaming-Data-Warehouse-to-Fuse-and-Enrich-Sensor-Data-in-Real-Time">
            announced
          </a>{' '}
          its $100 million ceiling contract through the Air Force's{' '}
          <a href="https://techinquiry.org/?entity=aflcmc%20digital%20directorate&guard=">
            Digital Directorate
          </a>{' '}
          to provide{' '}
          <a href="https://techinquiry.org/?entity=north%20american%20aerospace%20defense%20command&guard=">
            NORAD
          </a>{' '}
          "real-time intelligence from multiple, high-velocity streaming data
          feeds".
        </p>
        <p className="card-text ti-article mt-3">
          Given renewed attention on NORAD as a result of a Chinese balloon
          flying over U.S. airspace, Tech Inquiry has{' '}
          <a href="https://techinquiry.org/FOIA/NORAD-KineticaDB.pdf">
            published
          </a>{' '}
          its copy of Kinetica's contract, which has a completion date currently
          set to September 22, 2025. Because the contract was arranged through
          an Other Transaction Agreement (OTA), it is not published on
          USASpending.gov as a result of a technicality of the Federal Funding
          Accountability and Transparency Act (
          <a href="https://www.epa.gov/grants/federal-funding-accountability-and-transparency-act">
            FFATA
          </a>
          ).
        </p>
        <p className="card-text ti-article mt-3">
          Numerous software companies focused on national security, including
          the{' '}
          <a href="https://www.vox.com/recode/23507236/inside-disruption-rebellion-defense-washington-connected-military-tech-startup">
            embattled
          </a>{' '}
          <a href="https://techinquiry.org/?entity=rebellion%20defense%2C%20inc%2E&guard=">
            Rebellion Defense
          </a>
          , have been{' '}
          <a href="https://www.dropbox.com/s/6spegxy07e3s5uz/Software%20in%20Defense%20Coalition%20Defense%20Modernization_IC%20Letter%2012.26.22_AM.pdf?dl=0">
            lobbying
          </a>{' '}
          the U.S. Congress to expand its usage of OTAs "to include innovative
          software prototypes" through the "Software in Defense Coalition".
          Kinetica's $100 million NORAD contract followed its prototype contract
          with the{' '}
          <a href="https://techinquiry.org/?entity=defense%20innovation%20unit&guard=">
            Defense Innovation Unit
          </a>
          , a contracting arm of the Pentagon which has worked with several of
          the members of the Software in Defense Coalition.
        </p>
      </div>
    </div>
  </div>
)
