import * as React from 'react'

export const title = `Anduril Opened a Suicide Drone Development Facility Twelve Minutes Away From Georgia Tech`

export const content = (
  <div className="card">
    <img
      className="card-img-top"
      src="https://techinquiry.org/logos/anduril_altius.webp"
    />
    <div className="card-body ti-article mt-0">
      <h2>
        Anduril Opened a Suicide Drone Development Facility Twelve Minutes Away
        From Georgia Tech
      </h2>
      <h4>
        On Monday Anduril announced the opening of its 180,000 square foot
        Atlanta office, which will handle development of the company's loitering
        munition, ALTIUS. The U.S. announced the sale of Anduril's ALTIUS drones
        to Ukraine last month.
      </h4>
      <h4 className="mt-3">
        Jack Poulson{' '}
        <span style={{ fontSize: '1.2rem' }}>
          [Email: <code className="text-dark">jack@techinquiry.org</code>,{' '}
          Signal: <code className="text-dark">+1.646.733.6810</code>]
        </span>
        <br />
        2023-03-16, 3:45pm ET
      </h4>
      <h4 className="mt-3">
        [2023-03-16, 6:45pm ET]{' '}
        <em>
          Images from Google Streetview ostensibly showing the construction of
          Anduril's new office were added.
        </em>
      </h4>

      <div>
        <p className="card-text ti-article mt-5">
          On Monday Anduril Industries{' '}
          <a href="https://www.anduril.com/article/anduril-expands-into-new-atlanta-office-research-development-and-production-facility/">
            announced
          </a>{' '}
          the completion of its new 180,000 square foot "research, development
          and production facility", the new home for the company's ALTIUS
          tube-launched drone line{' '}
          <a href="https://www.defensenews.com/pentagon/2021/04/01/anduril-buys-tube-launched-drone-developer-area-i/">
            acquired
          </a>{' '}
          from Georgia-based Area-I in April 2021.
        </p>
        <p className="card-text ti-article mt-3">
          Anduril's modification of its ALTIUS drones to allow them to behave as
          flying bombs, or "loitering munitions", was first{' '}
          <a href="https://breakingdefense.com/2022/10/meet-andurils-new-loitering-munitions-the-firms-first-but-not-last-weapons-program/">
            reported
          </a>{' '}
          in October 2022. Breaking Defense{' '}
          <a href="https://breakingdefense.com/2023/02/marking-one-year-of-ukraine-war-us-pledges-2b-in-weapons-including-new-drone-types/">
            reported
          </a>{' '}
          last month on the U.S.'s first disclosure of the sale of Anduril's
          Altius-600 drones to Ukraine.
        </p>
        <p className="card-text ti-article mt-3">
          According to a July 2022{' '}
          <a href="https://www.georgia.org/press-release/anduril-industries-invest-60-million-create-180-new-jobs-fulton-county">
            announcement
          </a>{' '}
          from Georgia Governor Brian Kemp, the company's new facility cost at
          least $60 million and is "located at 1435 Hills Place Northwest".
          Thus, Anduril's new suicide drone development facility is less than a
          15 minute drive from both Georgia Tech and its primary defense
          contracting arm,{' '}
          <a href="https://www.gtri.gatech.edu/">
            Georgia Tech Research Institute
          </a>
          .
        </p>
        <p className="card-text ti-article mt-3">
          Images of the the publicly disclosed address from Google Street View
          dated January 2023 and February 2022 -- ostensibly showing the
          construction of the new office -- follow:
          <img
            className="card-img-top"
            src="https://techinquiry.org/logos/anduril_atlanta-2023-01.png"
          />
          <br />
          <img
            className="card-img-top mt-3"
            src="https://techinquiry.org/logos/anduril_atlanta-2022-02.png"
          />
        </p>
      </div>
    </div>
  </div>
)
