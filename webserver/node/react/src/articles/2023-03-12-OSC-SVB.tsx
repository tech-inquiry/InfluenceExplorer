import * as React from 'react'

export const title = `The Pentagon's Office of Strategic Capital has been "advocating" for its "close friends" impacted by Silicon Valley Bank`

export const content = (
  <div className="card">
    <img
      className="card-img-top"
      src="https://techinquiry.org/logos/jason_rathje_bens.png"
    />
    <div className="card-body ti-article mt-0">
      <h2>
        The Pentagon's Office of Strategic Capital has been "advocating" for its
        "close friends" impacted by Silicon Valley Bank
      </h2>
      <h4>
        The Director of the Office of Strategic Capital, Jason Rathje, stated in
        an email to partners at 6:05pm ET on Sunday that businesses affected by
        <em>"the SVB failure...employ close friends"</em> of the Office, which
        has been
        <em>
          "actively collaborating with our DoD and other government colleagues
          to advocate for our national security community"
        </em>{' '}
        over the last 48 hours.
      </h4>
      <h4 className="mt-3">
        Jack Poulson{' '}
        <span style={{ fontSize: '1.2rem' }}>
          [Email: <code className="text-dark">jack@techinquiry.org</code>,{' '}
          Signal: <code className="text-dark">+1.646.733.6810</code>]
        </span>
        <br />
        2023-03-12, 6:45pm ET
      </h4>

      <div>
        <p className="card-text ti-article mt-5">
          In an email sent at 6:05pm ET on Sunday, the Director of the
          Pentagon's{' '}
          <a href="https://www.cto.mil/osc/">Office of Strategic Capital</a>,{' '}
          <a href="https://www.linkedin.com/in/jason-rathje-b89b7427/">
            Jason Rathje
          </a>
          , stated that the Office had spent the last 48 hours "actively
          collaborating with our DoD and other government colleagues to advocate
          for our national security community". Rathje partially attributed the
          Pentagon office's advocacy to the fact that "the SVB failure" impacted
          businesses which "employ close friends" of the Office.
        </p>
        <p className="card-text ti-article mt-3">
          The Office of Strategic Capital was{' '}
          <a href="https://www.wsj.com/articles/pentagon-office-to-support-tech-investment-critical-for-national-security-11669918443">
            announced
          </a>{' '}
          at the beginning of December 2022 and{' '}
          <a href="https://www.defense.gov/News/Releases/Release/Article/3233377/secretary-of-defense-establishes-office-of-strategic-capital/">
            aims
          </a>{' '}
          to "scale investments" with the Defense Advanced Research Projects
          Agency and the Defense Innovation Unit{' '}
          <em>
            "by increasing the capital available to critical technology
            companies to help them reach scaled production"
          </em>
          .
        </p>
        <p className="card-text ti-article mt-3">
          As{' '}
          <a href="https://techinquiry.org/?article=10x-overnight">reported</a>{' '}
          by Tech Inquiry, the national security focused venture capital firm
          America's Frontier Fund -- whose backers include former Google CEO
          Eric Schmidt and Palantir co-founder Peter Thiel -- stated at an event
          at Silicon Valley Bank on February 1st that many of its "choke point"
          investments would <em>"10x overneight, like no question about it"</em>{' '}
          if <em>"the China/Taiwan situation happens"</em>. America's Frontier
          Fund CEO Gilman Louie was the founding CEO of the U.S. Intelligence
          Community's primary venture capital arm, In-Q-Tel, which has long been{' '}
          <a href="https://www.wsj.com/articles/the-cias-venture-capital-firm-like-its-sponsor-operates-in-the-shadows-1472587352">
            criticized
          </a>{' '}
          over its trustees' conflicts of interest.
        </p>
        <p className="card-text ti-article mt-3">
          It should perhaps not come as a shock that an arm of the Pentagon
          would use its national security influence to "advocate" for its "close
          friends" in the venture capital community.
        </p>
        <p className="card-text ti-article mt-3">
          The full text of the email follows:
          <br />
          <code className="text-dark">
            Good Afternoon,
            <br />
            <br />
            At OSC, our mission relies on strong public-private partnerships. As
            a result, since late last week we have been tracking the Silicon
            Valley Bank situation closely.
            <br />
            <br />
            While we have been heads-down in assessing impacts to national
            security, I wanted to let you know that we understand that this is
            an incredibly difficult time. Over the past 8 years, our OSC
            teammates have had the fortune of witnessing the exponential growth
            in national security-focused entrepreneurs, investors, and
            innovators who both directly and indirectly support the DoD mission.
            As such, many of the small businesses affected by the SVB failure
            are not only organizations that have worked alongside us, but also
            employ close friends, including reservists and veterans we’ve had
            the opportunity to serve with.
            <br />
            <br />
            Over the past 48 hours, we have been actively collaborating with our
            DoD and other government colleagues to advocate for our national
            security community and provide insight into ongoing mitigation
            efforts. We are constantly monitoring national security-related
            impacts to the crisis, and we are looking forward to providing more
            information as it becomes available.
            <br />
            <br />
            Thank you for being a partner,
            <br />
            <br />
            Jason
            <br />
            Director, Office of Strategic Capital
          </code>
        </p>
      </div>
    </div>
  </div>
)
