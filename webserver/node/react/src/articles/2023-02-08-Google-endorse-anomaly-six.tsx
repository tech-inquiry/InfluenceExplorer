import * as React from 'react'

export const title = `Google Cloud's CTO is helping endorse controversial cellphone location-tracking and chat room infiltration intelligence contractors`

export const content = (
  <div className="card-body ti-article mt-0">
    <h2>
      Google Cloud's CTO is helping endorse controversial cellphone
      location-tracking and chat room infiltration intelligence contractors
    </h2>
    <h3>
      Cellphone location-tracking firms Babel Street and Anomaly Six were
      announced as members of{' '}
      <a href="https://techinquiry.org/?entity=missionlink%2C%20inc%2E&guard=">
        MissionLink.next
      </a>
      's 2023 cohort, as was{' '}
      <a href="https://flashpoint.io/solutions/public-sector-and-national-security/">
        Flashpoint
      </a>
      , which infiltrates private chat rooms on platforms such as Telegram as a
      service.
    </h3>
    <h4 className="mt-3">
      Jack Poulson{' '}
      <span style={{ fontSize: '1.2rem' }}>
        [Email: <code className="text-dark">jack@techinquiry.org</code>, Signal:{' '}
        <code className="text-dark">+1.646.733.6810</code>]
      </span>
      <br />
      2023-02-08, 6:00pm ET
    </h4>

    <div>
      <p className="card-text ti-article mt-5">
        Yesterday the non-profit national security accelerator MissionLink.next
        -- of which Google Cloud's CTO{' '}
        <a href="https://www.linkedin.com/in/wgrannis/">Will Grannis</a> is
        perhaps the most prominent advisory board member --{' '}
        <a href="https://www.prnewswire.com/news-releases/missionlinknext-unveils-2023-cohort-and-new-advisory-board-members-301741111.html">
          announced
        </a>{' '}
        the list of companies it would be supporting in 2023. Included in the
        list are several controversial intelligence contractors, such as
        cellphone location-tracking firms{' '}
        <a href="https://techinquiry.org/?entity=babel%20street%2C%20inc%2E&guard=">
          Babel Street
        </a>{' '}
        and{' '}
        <a href="https://techinquiry.org/?entity=anomaly%206%20llc&guard=">
          Anomaly Six
        </a>
        , and chat room infiltration as a service company{' '}
        <a href="https://techinquiry.org/?entity=ej2%20communications%2C%20inc%2E&guard=">
          Flashpoint
        </a>
        . All three companies sell their services to U.S. Special Operations
        Command (SOCOM).
      </p>
      <p className="card-text ti-article mt-2">
        Babel Street's cellphone location-tracking product was{' '}
        <a href="https://www.protocol.com/government-buying-location-data">
          revealed
        </a>{' '}
        by Protocol in 2020 to be called Locate X, and a{' '}
        <a href="https://techinquiry.org/docs/A6-Babel-MoFo.pdf">lawsuit</a>{' '}
        first{' '}
        <a href="https://www.wsj.com/articles/u-s-government-contractor-embedded-software-in-apps-to-track-phones-11596808801">
          covered
        </a>{' '}
        by The Wall Street Journal revealed that Babel Street sued its former
        employees Brendan Huff and Jeffrey Heinz for their new company, Anomaly
        Six, allegedly copying the trade secrets of Locate X. (The author{' '}
        <a href="https://theintercept.com/2022/04/22/anomaly-six-phone-tracking-zignal-surveillance-cia-nsa/">
          reported
        </a>{' '}
        in April of last year on a sales pitch from Anomaly Six to social-media
        surveillance firm -- and fellow SOCOM contractor --{' '}
        <a href="https://techinquiry.org/?entity=zignal%20labs%2C%20inc%2E&guard=">
          Zignal Labs
        </a>{' '}
        on how the two platforms could be combined to, for example, deanonymize
        CIA officers.)
      </p>
      <p className="card-text ti-article mt-2">
        Google owns perhaps the largest commercial trove of cellphone
        location-tracking data in the United States, yet its usage by
        governments typically requires a warrant (such as in the case of the FBI
        investigations into the January 6th riot -- the author previously{' '}
        <a href="https://www.forbes.com/sites/thomasbrewster/2022/02/23/meet-the-secretive-surveillance-wizards-helping-the-fbi-and-ice-wiretap-facebook-and-google-users/">
          revealed
        </a>{' '}
        how the software company{' '}
        <a href="https://techinquiry.org/?entity=pen-link%2C%20ltd%2E&guard=">
          Pen-Link
        </a>{' '}
        promotes and enables police access to both Google Search histories and
        cellphone location data).
      </p>
      <p className="card-text ti-article mt-2">
        Google{' '}
        <a href="https://www.vice.com/en/article/5db4ad/google-bans-safegraph-former-saudi-intelligence">
          banned
        </a>{' '}
        the competing cellphone location-tracking data broker{' '}
        <a href="https://techinquiry.org/?entity=safegraph%20inc%2E&guard=">
          SafeGraph
        </a>{' '}
        from its Play Store in 2021, and it is widely understood that
        location-tracking data brokers such as Babel Street and Anomaly Six
        serve as a 'back-door' around warrants for intelligence agencies and
        special operations forces. Grannis's endorsement is thus at odds with
        Google's previous public positioning as a careful steward of cellphone
        location data. (Grannis was also the{' '}
        <a href="https://cloud.google.com/blog/topics/public-sector/google-cloud-public-sector-has-a-new-ceo">
          head
        </a>{' '}
        of Google's new public sector division until being replaced by Karen
        Dahut in September.)
      </p>
      <p className="card-text ti-article mt-3">
        Google did not immediately return a request for comment.
      </p>
    </div>
  </div>
)
