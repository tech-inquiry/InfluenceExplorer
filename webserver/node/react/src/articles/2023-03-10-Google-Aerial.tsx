import * as React from 'react'

export const title = `Google restarted selling A.I. to Pentagon for aerial imagery`

export const content = (
  <div className="card">
    <img
      className="card-img-top"
      src="https://techinquiry.org/logos/google_aerial_imagery.png"
    />
    <div className="card-body ti-article mt-0">
      <h2>Google restarted selling A.I. to Pentagon for aerial imagery</h2>
      <h3>
        Google was paid $400,000 through an Other Transaction Agreement by the
        Pentagon in September for
        <em>
          "Artificial Intelligence/Machine Learning for processing aerial
          imagery"
        </em>{' '}
        and nearly three million for <em>"Air Logistics Optimization"</em>.
        Google has also received $914,800 from the potentially $9 billion Joint
        Warfighting Cloud Capability award shared with Microsoft, Amazon, and
        Oracle.
      </h3>
      <h4 className="mt-3">
        Jack Poulson{' '}
        <span style={{ fontSize: '1.2rem' }}>
          [Email: <code className="text-dark">jack@techinquiry.org</code>,{' '}
          Signal: <code className="text-dark">+1.646.733.6810</code>]
        </span>
        <br />
        2023-03-10, 11:10am ET
      </h4>
      <h3 className="mt-3">
        <em>
          [2023-03-10, 2:00pm ET] An additional sentence was added to provide
          the equivalent public obligations to Microsoft, Oracle, and Amazon on
          JWCC.
        </em>
      </h3>

      <div>
        <p className="card-text ti-article mt-5">
          A{' '}
          <a href="https://www.fpds.gov/ezsearch/search.do?indexName=awardfull&templateName=1.5.3&s=FPDS.GOV&q=HQ08452290039+9700+">
            contract notice
          </a>{' '}
          withheld from <a href="https://usaspending.gov">USASpending.gov</a> on
          a technicality and from daily Department of Defense notices for being
          too small reveals that Google has sold{' '}
          <code className="text-dark">
            "Artificial Intelligence/Machine Learning for processing aerial
            imagery"
          </code>{' '}
          to the Office of the Secretary of Defense and Army National Guard.
          Google's explicit sale of artificial intelligence to the Pentagon for
          use in processing aerial imagery contrasts with the company earlier{' '}
          <a href="https://theintercept.com/2019/03/01/google-project-maven-contract/">
            downgrading
          </a>{' '}
          its work on the "Project Maven" Pentagon drone surveillance contract
          from designing{' '}
          <a href="https://www.nytimes.com/2018/06/01/technology/google-pentagon-project-maven.html">
            custom A.I.
          </a>{' '}
          to selling commodity cloud computing.
        </p>
        <p className="card-text ti-article mt-3">
          While it is highly likely that Google's "aerial imagery" contract
          involves military drones, Tech Inquiry's{' '}
          <a href="https://techinquiry.org/EasyAsPAI/">previous analysis</a> of
          Project Maven subcontracting records revealed a focus extending beyond
          drones to satellite imagery (and even social media surveillance).
        </p>
        <p className="card-text ti-article mt-3">
          Google's September 9th, 2022 $400,000 contract with the Pentagon was
          followed later the same month with a million dollar "air logistics
          optimization" contract which was then nearly tripled in size on
          December, 6th. The next day, Google was{' '}
          <a href="https://www.usaspending.gov/award/CONT_IDV_HQ003423D0017_9700">
            awarded
          </a>{' '}
          one of four spots on the $9 billion ceiling Joint Warfighting Cloud
          Capability contract and then{' '}
          <a href="https://www.usaspending.gov/award/CONT_AWD_HQ003423F0023_9700_HQ003423D0017_9700">
            provided
          </a>{' '}
          a minimum guaranted payout of $100,000. (The next day the was{' '}
          <a href="https://www.usaspending.gov/award/CONT_AWD_HC105023F0005_9700_HQ003423D0017_9700">
            awarded
          </a>{' '}
          an additional $814,800 payout.)
        </p>
        <p className="card-text ti-article mt-3">
          (For context, relative to Google's $914,800 in JWCC obligations,
          public records so far demonstrate $100,000 in obligations to{' '}
          <a href="https://techinquiry.org/?text=HQ003423D0019&guard=">
            Amazon
          </a>
          , $2.1 million to{' '}
          <a href="https://techinquiry.org/?text=HQ003423D0018&guard=">
            Oracle
          </a>
          {', '}
          and $3.6 million to{' '}
          <a href="https://techinquiry.org/?text=HQ003423D0020&guard=">
            Microsoft
          </a>
          .)
        </p>
        <p className="card-text ti-article mt-3">
          Unlike Google's JWCC awards, Google's more than $3 million in "aerial
          imagery" and "Air Logistics Optimization" contracts are through a more
          flexible -- and{' '}
          <a href="https://fcw.com/acquisition/2023/03/nsf-watchdog-warns-other-transaction-pitfalls-funding-decisions-loom/383661/">
            controversial
          </a>{' '}
          -- procurement mechanism known as an Other Transaction Agreement and
          are therefore withheld from USASpending.gov. (Despite the transparency
          and accountability shortcomings of OTAs, a consortium of Pentagon
          software contractors has been{' '}
          <a href="https://www.dropbox.com/s/6spegxy07e3s5uz/Software%20in%20Defense%20Coalition%20Defense%20Modernization_IC%20Letter%2012.26.22_AM.pdf?dl=0">
            lobbying
          </a>{' '}
          Congress for their further adoption).
        </p>
        <p className="card-text ti-article mt-3">
          Google's recent uptick in defense contracting is driven by the
          creation of{' '}
          <a href="https://cloud.google.com/blog/topics/public-sector/announcing-google-public-sector">
            Google Public Sector
          </a>
          , which in October{' '}
          <a href="https://www.virginiabusiness.com/article/booze-allen-exec-named-google-public-sector-ceo/">
            appointed
          </a>{' '}
          former Booz Allen executive Karen Dahut as its CEO. Beyond JWCC,
          Google is{' '}
          <a href="https://techinquiry.org/docs/InternationalCloud.pdf">
            one of five
          </a>{' '}
          reported recipients of the Central Intelligence Agency's Commercial
          Cloud Enterprise contract -- which was estimated by the CIA to be
          worth "tens of billions" -- and is a co-recipient with Amazon of the
          Israeli government's $1.2 billion "Nimbus" cloud modernization
          contract. Tech Inquiry earlier{' '}
          <a href="https://theintercept.com/2022/07/24/google-israel-artificial-intelligence-project-nimbus/">
            revealed
          </a>{' '}
          details of Nimbus's relationship with the Israeli Ministry of Defense
          based upon Google's own training materials (which were linked to by
          the Israeli government).
        </p>
        <p className="card-text ti-article mt-3">
          Tech Inquiry has submitted Freedom of Information requests to the
          Office of the Secretary of Defense for Google Inc.'s aerial imagery
          contract with Procurement Instrument Identifier (PIID){' '}
          <code className="text-dark">HQ08452290039</code>, Google Support
          Service LLC's Air Logistics Optimization contract with PIID{' '}
          <code className="text-dark">HQ08452290056</code>, Google Support
          Service LLC's $9 billion ceilling Joint Warfighting Cloud Capability
          contract <code className="text-dark">HQ003423D0017</code>, and the
          latter's two child awards. Tech Inquiry also FOIA'd Google's JWCC
          contract through DISA, which served as the funding office.
        </p>
      </div>
    </div>
  </div>
)
