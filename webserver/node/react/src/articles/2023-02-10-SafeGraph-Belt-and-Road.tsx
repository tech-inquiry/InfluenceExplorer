import * as React from 'react'

export const title = `Cellphone Location-Tracking Data Broker SafeGraph has its first Public Contract with the U.S. Department of Defense: Monitoring China's Belt and Road Initiative for the Air Force`

export const content = (
  <div className="card-body ti-article mt-0">
    <h2>
      Cellphone Location-Tracking Data Broker SafeGraph has its first Public
      Contract with the U.S. Department of Defense: Monitoring China's Belt and
      Road Initiative for the Air Force
    </h2>
    <h4>
      SafeGraph's newly public $74,888 contract with the U.S. Air Force is for
      "Wide Area Monitoring" of "One Belt, One Road" Facilities as part of the
      Joint All Domain Command and Control (JADC2) Initiative
    </h4>
    <h4 className="mt-3">
      Jack Poulson{' '}
      <span style={{ fontSize: '1.2rem' }}>
        [Email: <code className="text-dark">jack@techinquiry.org</code>, Signal:{' '}
        <code className="text-dark">+1.646.733.6810</code>]
      </span>
      <br />
      2023-02-10, 10:00am ET
    </h4>

    <div>
      <p className="card-text ti-article mt-5">
        The cellphone location-tracking data broker{' '}
        <a href="https://techinquiry.org/?entity=safegraph%20inc%2E&guard=">
          SafeGraph
        </a>{' '}
        has a{' '}
        <a href="https://www.usaspending.gov/award/CONT_AWD_FA864923P0176_9700_-NONE-_-NONE-">
          newly public
        </a>{' '}
        contract with the U.S. Air Force which is clearly labeled as for "Wide
        Area Monitoring" of China's Belt and Road Initiative. While SafeGraph's
        cellphone location-tracking competitors -- such as Babel Street and
        Anomaly Six -- have multiple public contracts with the U.S. military,
        this is a first for SafeGraph.
      </p>
      <p className="card-text ti-article mt-2">
        SafeGraph's contract -- which is newly public due to the Department of
        Defense's 90-day contract publication delay -- is labeled as part of the
        Pentagon's high-level software engineering and artificial intelligence
        effort for unifying the data from its myriad weapons and surveillance
        systems: the Joint All-Domain Command and Control, or{' '}
        <a href="https://breakingdefense.com/tag/joint-all-domain-command-and-control/">
          JADC2
        </a>
        , initiative. The full award description is "
        <code className="text-dark">
          WIDE AREA MONITORING OF ADVERSARY ONE BELT, ONE ROAD (OBOR) FACILITIES
          TO SUPPORT JOINT ALL DOMAIN COMMAND AND CONTROL (JADC2)
        </code>
        " and is clearly focused on surveillance of China's{' '}
        <a href="https://crsreports.congress.gov/product/pdf/IF/IF11735">
          Belt and Road Initiative
        </a>
        , which makes infrastructure investments within Africa and Europe . The
        Congressional Research Service has described the Belt and Road
        Initiative as focused on the promotion of China's technology supply
        chain -- with the implication as a competitor to U.S. alternatives --
        and as a rival to World Bank lending.
      </p>
      <p className="card-text ti-article mt-2">
        As reported by Motherboard journalist Joseph Cox in 2021 and 2022,
        SafeGraph was{' '}
        <a href="https://www.vice.com/en/article/5db4ad/google-bans-safegraph-former-saudi-intelligence">
          banned
        </a>{' '}
        from Google's Play Store in 2021 and has{' '}
        <a href="https://www.vice.com/en/article/m7vzjb/location-data-abortion-clinics-safegraph-planned-parenthood">
          sold
        </a>{' '}
        its location data for use tracking visitors to abortion clinics.
      </p>
      <p className="card-text ti-article mt-3">
        Tech Inquiry has submitted a Freedom of Information Request to the U.S.
        Air Force for a copy of SafeGraph's Belt and Road Initiative monitoring
        contract but is unlikely to receive responsive records for at least six
        months.
      </p>
    </div>
  </div>
)
