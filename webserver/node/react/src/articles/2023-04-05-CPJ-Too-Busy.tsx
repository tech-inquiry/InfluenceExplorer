import * as React from 'react'

export const title = `Committee to Protect Journalists is Too Busy to Comment on Bombing of a Russian Journalist`

export const content = (
  <div className="card">
    <img
      className="card-img-top"
      src="https://techinquiry.org/logos/committee_to_protect_western_journalists.png"
    />
    <div className="card-body ti-article mt-0">
      <h2>
        Committee to Protect Journalists Too Busy to Comment on Bombing of
        Pro-Russian Military Correspondent
      </h2>
      <h4>
        In a statement emailed to Tech Inquiry, Committee to Protect Journalists
        asserted that it was too busy to comment on Sunday's bombing of the
        pro-Russian military correspondent known as Vladlen Tatarsky. Yet its
        campaign to support the detained American journalist Evan Gershkovich is
        in full swing.
      </h4>
      <h4 className="mt-3">
        Jack Poulson{' '}
        <span style={{ fontSize: '1.2rem' }}>
          [Email: <code className="text-dark">jack@techinquiry.org</code>,{' '}
          Signal: <code className="text-dark">+1.646.733.6810</code>]
        </span>
        <br />
        2023-04-05, 12:00pm ET
      </h4>
      <h4 className="mt-3">
        [2023-04-05, 1:23pm ET] A statement from the Freedom of the Press
        Foundation was added. And the spelling of Christo Grozev's last name was
        corrected.
      </h4>
      <h4 className="mt-3">
        [2023-04-05, 3:55pm ET] A sentence was added on Russia's retaliation to
        the silence on Tatarsky.
      </h4>

      <div>
        <p className="card-text ti-article mt-5">
          The pro-Russian military correspondent Maksim Fomin -- better known as
          Vladlen Tatarsky -- was{' '}
          <a href="https://www.nytimes.com/live/2023/04/03/world/russia-ukraine-war">
            killed
          </a>{' '}
          on Sunday by a bomb detonated at a cafe in St. Petersburg where he was
          giving a talk. Tatarsky was one of Russia's{' '}
          <a href="https://www.theguardian.com/world/2023/apr/02/russian-pro-war-military-blogger-killed-blast-st-petersburg-vladlen-tatarsky">
            most influential
          </a>{' '}
          military bloggers and the bomb was reportedly hidden within a statue
          presented to Tatarsky as a gift.
        </p>
        <p className="card-text ti-article mt-3">
          Christo Grozev of the investigative nonprofit Bellingcat defended the
          act of terrorism -- which reportedly injured at least 30 people -- as
          a "legitimate target" in a{' '}
          <a href="https://news.sky.com/video/ukraine-war-bellingcats-christo-grozev-looks-at-who-couldve-killed-russian-blogger-12848676">
            video interview
          </a>{' '}
          conducted with Sky News, pointing to Tatarsky's assignment to a
          Russian military regiment in 2022. Canada's Ambassador to the United
          Nations, Robert Rae, similarly defended the bombing in a{' '}
          <a href="https://twitter.com/BobRae48/status/1643235809888272384">
            tweet
          </a>{' '}
          on Tuesday on the grounds that "Russian journalists are either in
          exile or in jail."
        </p>
        <p className="card-text ti-article mt-3">
          The Committee to Protect Journalists -- which is headquartered in New
          York City and{' '}
          <a href="https://projects.propublica.org/nonprofits/organizations/133081500/202133029349301628/full">
            reported
          </a>{' '}
          roughly $18 million in 2020 revenue -- maintains a{' '}
          <a href="https://cpj.org/data/killed/?status=Killed&motiveConfirmed%5B%5D=Confirmed&motiveUnconfirmed%5B%5D=Unconfirmed&type%5B%5D=Journalist&type%5B%5D=Media%20Worker&start_year=1992&end_year=2023&group_by=year">
            database
          </a>{' '}
          of killed journalists and media workers but has not yet added
          Tatarsky. (The most recent{' '}
          <a href="https://cpj.org/data/people/pal-kola/">entry</a> is the March
          27th shooting of a security guard for the Albanian TV station Top
          Channel.)
        </p>
        <p className="card-text ti-article mt-3">
          At the moment, the Committee to Protect Journalists appears to be
          focused on its{' '}
          <a href="https://cpj.org/2023/03/cpj-media-organizations-and-partners-call-for-release-of-us-journalist-evan-gershkovich/">
            campaign
          </a>{' '}
          for the release of Wall Street Journal reporter Evan Gershkovich, who
          was recently detained in Russia on espionage charges. (Reuters{' '}
          <a href="https://www.reuters.com/world/russia-says-it-will-ignore-media-lobbying-release-arrested-us-reporter-2023-04-05/">
            reported
          </a>{' '}
          on Wednesday that Russia will ignore media lobbying for the release of
          Gershkovich as a response to the same organizations remaining silent
          on the assassination of Tatarsky.)
        </p>
        <p className="card-text ti-article mt-3">
          In a statement shared with Tech Inquiry by the Director of Advocacy of
          Freedom of the Press Foundation -- which{' '}
          <a href="https://media.freedom.press/media/documents/2020_Impact_Report.pdf">
            listed
          </a>{' '}
          Committee to Protect Journalists as a major supporter in 2020 -- the
          organization stated:{' '}
          <em>
            "We condemn any act of violence against any publisher, anywhere, in
            retaliation for content they publish, no matter how controversial.
            The cure for bad speech is more speech, not bombs."
          </em>
        </p>
      </div>
    </div>
  </div>
)
