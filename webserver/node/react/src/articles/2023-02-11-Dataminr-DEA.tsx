import * as React from 'react'

export const title = `Social Media Surveillance Giant Dataminr Signed its First Public Contract with the DEA`

export const content = (
  <div className="card-body ti-article mt-0">
    <h2>
      Social Media Surveillance Giant Dataminr Signed its First Public Contract
      with the DEA
    </h2>
    <h3>
      While Dataminr openly sells its software to police -- and a leaked
      brochure from 2016 showed Dataminr's relationship with U.S. intelligence
      agencies -- this is Dataminr's first public contract with the DEA.
    </h3>
    <h4 className="mt-3">
      Jack Poulson{' '}
      <span style={{ fontSize: '1.2rem' }}>
        [Email: <code className="text-dark">jack@techinquiry.org</code>, Signal:{' '}
        <code className="text-dark">+1.646.733.6810</code>]
      </span>
      <br />
      2023-02-11, 10:15am ET
    </h4>

    <div>
      <p className="card-text ti-article mt-5">
        Internet surveillance firm Dataminr has been waging a public relations{' '}
        <a href="https://gizmodo.com/twitter-says-its-partner-dataminr-wasnt-surveilling-pro-1844328988">
          battle
        </a>{' '}
        for the better part of a decade to portray its products as "alerts"
        rather than "surveillance": the language became the company's means of
        balancing the competing concerns of advertising its surveillance
        capabilities to clients such as U.S. police while providing plausible
        deniability to data providers such as Twitter, which{' '}
        <a href="https://www.theverge.com/2017/1/27/14412014/dataminr-twitter-firehose-foreign-pitch-canada-azerbaijan-surveillance-pdf">
          previously owned
        </a>{' '}
        5% of Dataminr. (Google more recently{' '}
        <a href="https://web.archive.org/web/20221207033401/https://www.dataminr.com/about">
          invested
        </a>{' '}
        in Dataminr and{' '}
        <a href="https://www.dataminr.com/press/dataminr-appoints-cristina-bita-to-board-of-directors">
          landed
        </a>{' '}
        a seat on its board of directors).
      </p>
      <p className="card-text ti-article mt-2">
        Partly as a result of{' '}
        <a href="https://theintercept.com/2020/07/09/twitter-dataminr-police-spy-surveillance-black-lives-matter-protests/">
          reporting
        </a>{' '}
        on Dataminr's alleged support for police surveillance of Black Lives
        Matter protesters through Twitter, one of the primary constraints the
        company has imposed on its police clients is a blacklisting of the term
        "protest" in search; such a constraint gives Twitter plausible
        deniability for opposing police disruption of civil rights movements
        without significantly altering the data brokerage pipeline from Twitter
        to Dataminr to police. But as was explained to the author at{' '}
        <a href="https://www.discoverisc.com/east/en-us.html">ISC East</a> 2022
        by Dataminr's Canadian competitor{' '}
        <a href="https://techinquiry.org/?entity=social%20asset%20management%20inc%2E&guard=">
          Samdesk
        </a>{' '}
        (which has{' '}
        <a href="https://www.samdesk.io/blog/samdesk-taps-into-nextdoor-as-their-first-ever-public-data-partner/">
          recently partnered
        </a>{' '}
        with{' '}
        <a href="https://techinquiry.org/?entity=nextdoor%20inc%2E&guard=">
          Nextdoor
        </a>
        ), Samdesk simply builds a list of alternative search terms for
        "protest" into its platform. (Samdesk also demoed its pre-built
        functionality for monitoring global coup-related activity.)
      </p>
      <p className="card-text ti-article mt-2">
        The bulk of Dataminr's U.S. federal income has resulted from its
        (ongoing){' '}
        <a href="https://www.usaspending.gov/award/CONT_AWD_FA701421C0024_9700_-NONE-_-NONE-">
          $267 million
        </a>{' '}
        contract with the U.S. Air Force (there the legal term of art for social
        media surveillance is "publicly available information" monitoring).
        Dataminr's{' '}
        <a href="https://www.fpds.gov/ezsearch/search.do?indexName=awardfull&templateName=1.5.3&s=FPDS.GOV&q=15DDHQ23P00000174+1524+">
          newly signed
        </a>{' '}
        contract with the Drug Enforcement Administration -- whose{' '}
        <a href="https://www.dni.gov/files/documents/ppd-28/DEA.pdf">
          Office of National Security Intelligence
        </a>{' '}
        is one of{' '}
        <a href="https://www.dni.gov/index.php/what-we-do">eighteen members</a>{' '}
        of the U.S. Intelligence Community -- is at odds with the widespread{' '}
        <a href="https://www.wsj.com/articles/twitter-bars-intelligence-agencies-from-using-analytics-service-1462751682">
          reporting
        </a>{' '}
        in 2016 that Twitter "cut off" intelligence agencies from its platform.
        (Telegram has since become an increasingly central target of government
        surveillance, with firms such as{' '}
        <a href="https://techinquiry.org/?entity=ej2%20communications%2C%20inc%2E&guard=">
          Flashpoint
        </a>{' '}
        and{' '}
        <a href="https://techinquiry.org/?entity=nisos%20holdings%20inc%2E&guard=">
          Nisos
        </a>{' '}
        openly selling access to the information they gain from infiltrating
        private Telegram groups.)
      </p>
      <p className="card-text ti-article mt-3">
        Tech Inquiry has submitted a Freedom of Information Request to the Drug
        Enforcement Administration for a copy of their new contract with
        Dataminr; Dataminr did not immediately respond to a request for comment.
      </p>
    </div>
  </div>
)
