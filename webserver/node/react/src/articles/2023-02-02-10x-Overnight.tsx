import * as React from 'react'

export const title = `How an Eric Schmidt-backed Venture Capital Firm Claims its Investments Will Increase "10x Overnight" if China Invades Taiwan`

export const content = (
  <div className="card-body ti-article mt-0">
    <h2>
      How an Eric Schmidt-backed Venture Capital Firm Claims its Investments
      Will Increase "10x Overnight" if China Invades Taiwan
    </h2>
    <h3>
      And how paying out profits through bonuses allows America's Frontier Fund
      to classify itself as a nonprofit and openly "shape" billions of dollars
      of government subsidies into its investments into supply chain "choke
      points".
    </h3>
    <h4 className="mt-3">
      Jack Poulson{' '}
      <span style={{ fontSize: '1.2rem' }}>
        [Email: <code className="text-dark">jack@techinquiry.org</code>, Signal:{' '}
        <code className="text-dark">+1.646.733.6810</code>]
      </span>
      <br />
      2023-02-02, 1:45pm ET
    </h4>

    <div>
      <p className="card-text ti-article mt-5">
        Last night in a second floor office of{' '}
        <a href="https://www.svb.com/">Silicon Valley Bank</a> blocks away from
        Madison Square Park, a representative of the national security focused
        venture capital firm{' '}
        <a href="https://www.scmp.com/news/china/science/article/3188579/us-aids-semiconductor-industry-billionaire-backed-tech-fund">
          America's Frontier Fund
        </a>{' '}
        (AFF) explained to a small crowd supplied with complementary wine and
        finger food how AFF planned to profit from several{' '}
        <em>"choke points"</em> in semiconductor and rare earth mineral supply
        chains. AFF's representative -- who was wearing a nametag simply labeled
        'Tom' and filling in last minute for president{' '}
        <a href="https://americasfrontier.org/team/jordan-blashek">
          Jordan Blashek
        </a>{' '}
        -- boasted that AFF's choke point investments would{' '}
        <em>"10x overnight, like no question about it"</em> if{' '}
        <em>"the China/Taiwan situation happens"</em> or, more generally,{' '}
        <em>"if there is a kinetic event in the Pacific"</em>.
      </p>
    </div>

    <div className="container d-flex justify-content-center my-4">
      <div id="mobile-box">
        <div className="card">
          <div className="card-body text-start">
            <h4 className="h4 font-weight-bold">
              "10x Overnight, Like No Question About It"
            </h4>
            <p className="card-text ti-article mb-3">
              In a huddle after the former panel, a representative of America's
              Frontier Fund explains how{' '}
              <em>
                "if the China/Taiwan situation happens, some of our investments
                could 10x, like overnight...I don't want to share the name, but
                the one example I gave was a critical component that...the total
                market value is $200 million, but it is a critical component to
                a $50 billion market cap. That's like a choke point, right. And
                if it's only produced in China, for example, and there's a
                kinetic event in the Pacific, that would 10x overnight, like no
                question about it. There's a couple of different things like
                that."
              </em>
            </p>

            <audio controls className="embed-responsive-item">
              <source src="https://techinquiry.org/docs/ten-x-overnight.mp3" />
            </audio>
          </div>
        </div>
      </div>
    </div>

    <div>
      <p className="card-text ti-article">
        Tom's candid assessment of the profit potential of war with China took
        place in a small huddle after the gathering's{' '}
        <a href="https://web.archive.org/web/20230202182550/https://events.svb.com/frontierevent">
          formal panel
        </a>
        , in which he had described how{' '}
        <a href="https://www.vox.com/recode/2022/6/9/23160588/eric-schmidt-americas-frontier-fund-google-alphabet-tech-government-revolving-door">
          AFF
        </a>{' '}
        used its non-profit designation to <em>"shape"</em> hundreds of billions
        of dollars of U.S. Government subsidies into semiconductors through
        2022's{' '}
        <a href="https://www.congress.gov/bill/117th-congress/house-bill/4346">
          CHIPS and Science Act
        </a>
        . AFF's dependence on U.S. Government subsidies for semiconductors and
        rare earth minerals was described as <em>"the fourth pillar"</em> of the
        business. (AFF backer and former Google CEO Eric Schmidt similarly{' '}
        <a href="https://www.youtube.com/watch?v=NijXsm4-3aw&t=94s">
          explained
        </a>{' '}
        in July 2021 how, through his position as Chairman of the U.S.
        government's{' '}
        <a href="https://prospect.org/power/silicon-valley-takes-battlespace-eric-schmidt-rebellion/">
          National Security Commission on AI
        </a>
        , his staffers had{' '}
        <a href="https://www.cnbc.com/2022/10/24/how-googles-former-ceo-eric-schmidt-helped-write-ai-laws-in-washington-without-publicly-disclosing-investments-in-ai-start-ups.html">
          written
        </a>{' '}
        <em>"one hundred pages of legislation"</em> which the U.S. Congress
        could <em>"just pass"</em> within the act which would eventually become
        the CHIPS and Science Act.)
      </p>
    </div>

    <div className="container d-flex justify-content-center my-5">
      <div id="mobile-box">
        <div className="card">
          <div className="card-body text-start">
            <h4 className="h4 font-weight-bold">"The Fourth Pillar"</h4>
            <p className="card-text ti-article mb-3">
              During the{' '}
              <a href="https://web.archive.org/web/20230202182550/https://events.svb.com/frontierevent">
                formal panel
              </a>{' '}
              of the event, AFF's representative explained how the organization{' '}
              <em>"shaped"</em> the{' '}
              <a href="https://www.congress.gov/bill/117th-congress/house-bill/4346">
                CHIPS Act
              </a>{' '}
              and treats U.S. government subsidies as "the fourth pillar" of the
              business's focus on supply chain <em>"choke points"</em> such as
              rare earth minerals.
            </p>

            <audio controls className="embed-responsive-item">
              <source src="https://techinquiry.org/docs/shaped_chips.mp3" />
            </audio>
          </div>
        </div>
      </div>
    </div>

    <div>
      <p className="card-text ti-article">
        AFF's stand-in for president Jordan Blashek was similarly candid about
        how, from the perspective of investors, there is{' '}
        <em>"no difference whatsoever"</em> between AFF and traditional venture
        capital despite AFF's non-profit designation. And that{' '}
        <em>
          "the only difference is the carry and the management fees, in theory,
          cycle back to the parent-level non-profit"
        </em>
        . This strategy was explained to have originated from lessons learned by
        the CEO of AFF,{' '}
        <a href="https://alsop-louie.com/team/gilman-louie/">Gilman Louie</a>,
        during his time as the first CEO of the primary venture capital arm of
        the U.S. Intelligence Community, In-Q-Tel. Tom explained that Louie{' '}
        <em>
          "set up In-Q-Tel in 1999 and made it not-for-profit and made all these
          insane structures to not make money because he didn't want to have to
          remit that money to Treasury...and he still made a ton of money."
        </em>{' '}
        (According to In-Q-Tel's{' '}
        <a href="https://projects.propublica.org/nonprofits/display_990/522149962/download990pdf_03_2022_prefixes_47-54%2F522149962_202103_990_2022030219680233">
          tax filings
        </a>{' '}
        for fiscal year 2020, CEO{' '}
        <a href="https://www.iqt.org/about-iqt/">Chris Darby</a> was given a
        bonus of $3.29 million that year by the government-subsidized non-profit
        on top of his base compensation of roughly $800,000.{' '}
        <a href="https://www.nscai.gov/commissioners/chris-darby-1055/">
          Darby
        </a>
        , like his predecessor{' '}
        <a href="https://www.nscai.gov/commissioners/gilman-louie/">Louie</a>,
        was a Commissioner of the Schmidt-chaired NSCAI.)
      </p>
    </div>

    <div className="container d-flex justify-content-center my-4">
      <div id="mobile-box">
        <div className="card">
          <div className="card-body text-start">
            <h4 className="h4 font-weight-bold">
              "The Only Difference is the Carry and Management Fees"
            </h4>
            <p className="card-text ti-article mb-3">
              <em>
                "If you think about it from an LP perspective, there's no
                difference whatsoever from a traditional venture capital fund.
                The only difference is the{' '}
                <a href="https://www.holloway.com/g/venture-capital/sections/carried-interest-and-management-fees">
                  carry
                </a>{' '}
                and the management fees, in theory, cycle back to the
                parent-level nonprofit."
              </em>
            </p>
            <audio controls className="embed-responsive-item">
              <source src="https://techinquiry.org/docs/no_difference_whatsoever.mp3" />
            </audio>
          </div>
        </div>
      </div>
    </div>

    <div className="container d-flex justify-content-center my-4">
      <div id="mobile-box">
        <div className="card">
          <div className="card-body text-start">
            <h4 className="h4 font-weight-bold">
              "We are a Non-Profit...Which Allows Us to Work Very Closely with
              Government"
            </h4>
            <p className="card-text ti-article mb-3">
              <em>
                "We are a non-profit at the parent level, which allows us to
                work very closely with government. There's incentive
                structures...it's not direct carry but it's similar structures.
                Right, because if we didn't have that, LPs would say you're not
                invested...Gilman Louie, who's one of our co-founders, set up
                In-Q-Tel in 1999 and made it not-for-profit. And made all of
                these insane structures to not make money, because he didn't
                want to remit that money to Treasury...He still made a ton of
                money."
              </em>
            </p>
            <audio controls className="embed-responsive-item">
              <source src="https://techinquiry.org/docs/work_closely_with_government.mp3" />
            </audio>
          </div>
        </div>
      </div>
    </div>

    <div>
      <p className="card-text ti-article mb-5">
        America's Frontier Fund has not responded to a request for comment
        despite twelve hours of advance notice.
      </p>
    </div>
  </div>
)
