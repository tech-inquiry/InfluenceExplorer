import * as React from 'react'

export const title = `Dataminr Lobbied the EU to Buy its Social Media Surveillance Products for Border Enforcement`

export const content = (
  <div className="card">
    <img
      className="card-img-top"
      src="https://techinquiry.org/logos/dataminr_frontex.png"
    />
    <div className="card-body ti-article mt-0">
      <h2>
        Dataminr Lobbied the EU to Buy its Social Media Surveillance Products
        for Border Enforcement
      </h2>
      <h3>
        A newly public transparency disclosure shows that Dataminr spent at
        least €50,000 in 2022 lobbying EU institutions such as Frontex to
        purchase its social media surveillance products for uses such as border
        security.
      </h3>
      <h4 className="mt-3">
        Jack Poulson{' '}
        <span style={{ fontSize: '1.2rem' }}>
          [Email: <code className="text-dark">jack@techinquiry.org</code>,{' '}
          Signal: <code className="text-dark">+1.646.733.6810</code>]
        </span>
        <br />
        2023-02-20, 3:10pm ET
      </h4>

      <div>
        <p className="card-text ti-article mt-5">
          In a newly public transparency{' '}
          <a href="https://data.europa.eu/data/datasets/transparency-register?locale=en">
            disclosure
          </a>
          , social media surveillance firm{' '}
          <a href="https://techinquiry.org/?entity=dataminr%2C%20inc%2E&guard=">
            Dataminr
          </a>{' '}
          revealed that it spent at least €50,000 lobbying European Union
          institutions such as the border enforcement agency{' '}
          <a href="https://frontex.europa.eu/">Frontex</a> on topics including
          "border security".
        </p>
        <p className="card-text ti-article mt-3">
          Beyond border security, Dataminr also disclosed lobbying relating to
          the{' '}
          <a href="https://commission.europa.eu/law/law-topic/data-protection/data-protection-eu_en">
            Data Protection Law Enforcement Directive
          </a>{' '}
          (LED), which is{' '}
          <a href="https://www.cnil.fr/en/law-enforcement-directive-what-are-we-talking-about">
            complementary
          </a>{' '}
          to the General Data Protection Regulation (GDPR) for matters involving
          state security and national defense activities. According to the
          French data protection regulator{' '}
          <a href="https://www.cnil.fr/en/law-enforcement-directive-what-are-we-talking-about">
            CNIL
          </a>
          , the LED also covers activities{' '}
          <em>
            "which relate to police activities carried out prior to the
            commission of a criminal offence. The purposes covered by the LED
            may thus include preventive police activities for the purpose of
            protecting against threats to public security that could lead to a
            criminal charge (police activities at demonstrations, sporting
            events, maintaining public order, etc.)"
          </em>
          .
        </p>
        <p className="card-text ti-article mt-3">
          While the New York based Dataminr has multiple contracts in the United
          States with the Department of Homeland Security, their contracts are
          primarily through the{' '}
          <a href="https://www.secretservice.gov/">Secret Service</a>,{' '}
          <a href="https://www.fema.gov/">FEMA</a>,{' '}
          <a href="https://www.cisa.gov/">CISA</a>, and{' '}
          <a href="https://www.dhs.gov/federal-protective-service">FPS</a>{' '}
          rather than <a href="https://www.ice.gov/">ICE</a> or{' '}
          <a href="https://www.cbp.gov/">CBP</a>. Though in 2015 and 2016
          Dataminr{' '}
          <a href="https://www.usaspending.gov/award/CONT_IDV_HSHQDC13D00025_7001">
            sold
          </a>{' '}
          its surveillance to at least 50 users within CBP's Targeting and
          Analysis Systems Program Directorate (TASPD) through the
          Virginia-based reseller{' '}
          <a href="https://techinquiry.org/?entity=snap%2C%20inc%2E&guard=">
            Snap
          </a>
          .
        </p>
        <p className="card-text ti-article mt-3">
          Outside of the United States, Dataminr has sold its products to the{' '}
          <a href="https://www.afp.gov.au/">Australian Federal Police</a> and
          Australian Department of{' '}
          <a href="https://www.homeaffairs.gov.au/">Home Affairs</a>, the UK{' '}
          <a href="https://www.gov.uk/government/organisations/ministry-of-defence">
            Ministry of Defence
          </a>{' '}
          and{' '}
          <a href="https://www.gov.uk/government/organisations/cabinet-office">
            Cabinet Office
          </a>
          , and lobbied the{' '}
          <a href="https://www.rcmp-grc.gc.ca/">
            Royal Canadian Mounted Police
          </a>{' '}
          (RCMP).
        </p>
        <p className="card-text ti-article mt-3">
          Dataminr's lobbying of the RCMP has been ongoing since at least
          October of 2021, yet no such contracts have publicly materialized. It
          is thus an open question as to whether or when Dataminr's lobbying of
          EU border enforcement agencies would impact potential immigrants.
        </p>
        <p className="card-text ti-article mt-3">
          Dataminr did not immediately respond to a request for comment.
        </p>
      </div>
    </div>
  </div>
)
