import * as React from 'react'

// TODO: Don't preload these.
import * as TenXOvernight from './articles/2023-02-02-10x-Overnight'
import * as GoogleEndorseAnomalySix from './articles/2023-02-08-Google-endorse-anomaly-six'
import * as SafeGraphBeltAndRoad from './articles/2023-02-10-SafeGraph-Belt-and-Road'
import * as DataminrDEA from './articles/2023-02-11-Dataminr-DEA'
import * as DataminrFrontex from './articles/2023-02-20-Dataminr-Frontex'
import * as NORADKineticaDB from './articles/2023-02-23-NORAD-Kinetica-DB'
import * as GoogleAerial from './articles/2023-03-10-Google-Aerial'
import * as OSCSVB from './articles/2023-03-12-OSC-SVB'
import * as INSAMavenUkraine from './articles/2023-03-13-INSA-Maven-Ukraine'
import * as MavenReapersUkraine from './articles/2023-03-15-Maven-Reapers-Ukraine'
import * as AndurilSuicideDronesAtlanta from './articles/2023-03-16-Anduril-Atlanta'
import * as CobwebsOJS from './articles/2023-03-21-Cobwebs-OJS'
import * as JWCCOracle from './articles/2023-03-23-JWCC-Oracle'
import * as CPJTooBusy from './articles/2023-04-05-CPJ-Too-Busy'

const articleMap = {
  '10x-overnight': TenXOvernight,
  'google-endorse-anomaly-six': GoogleEndorseAnomalySix,
  'safegraph-belt-and-road': SafeGraphBeltAndRoad,
  'dataminr-dea': DataminrDEA,
  'dataminr-frontex': DataminrFrontex,
  'norad-kinetica-db': NORADKineticaDB,
  'google-aerial': GoogleAerial,
  'osc-svb': OSCSVB,
  'insa-maven-ukraine': INSAMavenUkraine,
  'maven-reapers-ukraine': MavenReapersUkraine,
  'anduril-suicide-drones-atlanta': AndurilSuicideDronesAtlanta,
  'indian-affairs-cellphone-tracking': CobwebsOJS,
  'jwcc-oracle': JWCCOracle,
  'cpj-too-busy': CPJTooBusy,
}

export function getArticleTitle(articleKey: string): string {
  if (articleMap.hasOwnProperty(articleKey)) {
    return articleMap[articleKey].title
  } else {
    return undefined
  }
}

export class DisplayArticle extends React.Component<any, any> {
  constructor(props) {
    super(props)
  }

  render() {
    if (articleMap.hasOwnProperty(this.props.articleKey)) {
      return articleMap[this.props.articleKey].content
    } else {
      return undefined
    }
  }
}
