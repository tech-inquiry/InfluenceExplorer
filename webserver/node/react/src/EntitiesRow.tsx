import * as React from 'react'

import { appConfig, DEBUG } from './utilities/constants'
import { RotatingImage } from './RotatingImage'
import * as ui from './utilities/ui'
import * as util from './utilities/util'

async function getEntityList(info: any, maxEntities: number) {
  const urlBase = `${appConfig.apiURL}/entities`
  const isAgency = false
  const isEntity = false
  const retrieveURL = util.getRetrieveURL(
    urlBase,
    info.query,
    isAgency,
    isEntity,
    info.isTag
  )
  const result = await fetch(retrieveURL as string).then((res) => res.json())

  // TODO(Jack Poulson): Cap at maxEntities?
  let entityList = []
  for (const filing of result.filings) {
    entityList.push({
      name: filing.vendor,
      stylizedName: filing.annotation.stylizedText,
      image: filing.annotation.logo,
      url: filing.annotation.url,
    })
  }

  return entityList
}

class EntitiesRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'entities')
    this.state = { items: [], loaded: false }
  }

  async componentDidMount() {
    this.setState({ items: this.props.entityList, loaded: true })
  }

  render() {
    const info = this.props.info

    const buttonTitle = 'Entities'

    const capImage = (
      <RotatingImage
        items={this.state.items}
        opacity={1.0}
        development={info.development}
      />
    )

    const footerText = (
      <span>
        Tech Inquiry has {this.state.items.length}{' '}
        {this.state.items.length > 1 ? 'entities' : 'entity'} matching the query{' '}
        <span className="fst-italic">{info.query}</span>.
      </span>
    )

    if (this.state.items.length) {
      return (
        <ui.EntityRow
          label={this.label}
          buttonTitle={buttonTitle}
          capImage={capImage}
          footerText={footerText}
        />
      )
    } else {
      if (this.state.loaded) {
        return undefined
      } else {
        return (
          <div className="container text-center font-monospace mt-3">
            <p className="text-start">
              <span style={{ fontSize: '150%' }}>
                Performing entities search...
              </span>
              <div className="spinner-grow text-primary" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            </p>
          </div>
        )
      }
    }
  }
}

class EntitiesModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'entities')
    this.state = { items: [], loaded: false }
    this.entityToItem = this.entityToItem.bind(this)
  }

  async componentDidMount() {
    this.setState({ items: this.props.entityList, loaded: true })
  }

  entityToItem(entityObj, index) {
    const logo = entityObj.image
    const entityStylized = entityObj.stylizedName
    const url = entityObj.url

    const logoEntry = (
      <a href={url}>
        <img
          className="ti-modal-list-entity-logo"
          loading="lazy"
          src={util.coerce(logo, util.defaultLogo)}
        />
      </a>
    )

    return (
      <div key={entityObj.name} className="row align-items-center my-1">
        <div className="col-6 align-items-center py-2 px-5 bg-light">
          <div className="text-center">{logoEntry}</div>
        </div>
        <div className="col-6">
          <h3 className="fw-bold font-monospace mb-2">
            <a className="text-decoration-none" href={url}>
              {entityStylized}
            </a>
          </h3>
        </div>
      </div>
    )
  }

  render() {
    const modalTitle = 'Entities'
    const modalBody = (
      <div>
        <div key="association-container" className="container px-0">
          {this.state.items.map(this.entityToItem)}
        </div>
      </div>
    )

    if (this.state.items.length) {
      return (
        <ui.EntityRowModal
          label={this.label}
          title={modalTitle}
          body={modalBody}
          dialogExtraClasses="modal-lg modal-fullscreen-lg-down"
        />
      )
    } else {
      return undefined
    }
  }
}

export async function insertCardAndModal(info, cards: any[], modals: any[]) {
  if (info.profile) {
    return
  }
  const maxEntities = 10
  const entityList = await getEntityList(info, maxEntities)
  cards.push(<EntitiesRow info={info} entityList={entityList} />)
  modals.push(<EntitiesModal info={info} entityList={entityList} />)
}
