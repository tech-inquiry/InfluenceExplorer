import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  AccordionItemWithLogo,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import { appConfig } from '../../utilities/constants'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

const kContractTypesExcludedFromUSASpending = [
  'other transaction agreement',
  'other transaction idv',
  'other transaction order',
]

function fpdsSearchURL(query) {
  const searchBase =
    'https://www.fpds.gov/ezsearch/search.do?indexName=awardfull&' +
    'templateName=1.5.3&s=FPDS.GOV&q='
  return `${searchBase}${query}`
}

function wrapFPDSLink(query) {
  const pieces = query.split('&q=')
  if (pieces.length >= 2) {
    // We already have a full URL.
    return query
  } else {
    return fpdsSearchURL(query)
  }
}

function usaSpendingAwardURL(uniqueKey) {
  const awardBase = 'https://www.usaspending.gov/award/'
  return `${awardBase}${uniqueKey}`
}

function usaSpendingSearchURL(query) {
  const searchBase = 'https://www.usaspending.gov/keyword_search/'
  return `${searchBase}${query}`
}

// NOTE: This is not yet functional, as the GSA Library backend seems to cache
// info from the previous queries. And the coverage seems unpredictable: for
// example, Alion's GSA contract with PIID 47QFCA18F0067 appears to not be in
// the GSA Library.
function gsaSearchURL(piid) {
  const searchBase =
    'https://www.gsaelibrary.gsa.gov/ElibMain/contractorInfo.do'
  return `${searchBase}?contractNumber=${piid}`
}

export function highlightFilter(item): boolean {
  if (!item) {
    console.log('Undefined item input to highlightFilter')
    return false
  }
  if (item.source == 'prime') {
    const modNumber = item.filing.mod_number
    if (!modNumber) {
      return true
    }
    const numericModString = modNumber.replace(/\D/g, '')
    const numericMod = Number(numericModString)
    return numericMod ? false : true
  } else {
    // At the moment, we highlight all subawards the same way we highlight
    // new prime awards.
    return true
  }
}

function renderPrimeVendor(filing) {
  const label = 'vendor'
  const annotation = filing.annotation.vendor
  const title = `Vendor: ${annotation.stylizedText.toUpperCase()}`

  const features = [
    <ui.TableKeyValue k="Name" v={util.coerceUpperCase(filing.vendor)} />,
    <ui.TableKeyValue k="UEI" v={util.coerceUpperCase(filing.uei)} />,
    <ui.TableKeyValue
      k="Ultimate Parent"
      v={util.coerceUpperCase(filing.ultimate_parent)}
    />,
    <ui.TableKeyValue
      k="Ultimate Parent UEI"
      v={util.coerceUpperCase(filing.ultimate_parent_uei)}
    />,
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderPrimeContractor(filing) {
  const label = 'contractor'
  const title = 'Contractor'
  const annotation = filing.annotation.contractor
  if (!filing.contractor) {
    return undefined
  }

  const features = [
    <ui.TableKeyValue
      k="Contractor"
      v={util.coerceUpperCase(filing.contractor)}
    />,
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderPrimeActionDetails(filing) {
  if (!(filing.piid || filing.parent_piid)) {
    return undefined
  }

  const label = 'actionDetails'
  const title = 'Contract Action Details'

  let features = []

  if (filing.contract_action_type) {
    const value = filing.annotation.actionType.url ? (
      <a href={filing.annotation.actionType.url}>
        {filing.annotation.actionType.stylizedText}
      </a>
    ) : (
      filing.annotation.actionType.stylizedText
    )
    ui.includeTableKeyValue(features, 'Contract Action Type', value)
  }

  const identifiers = (
    <table className="table table-sm table-striped table-fit">
      <thead>
        <tr>
          {filing.parent_piid ? <th></th> : undefined}
          <th scope="col">Procurement Instrument</th>
          <th scope="col">Modification</th>
          <th scope="col">
            <a href="https://usaspending.gov">
              <img
                src={util.concatenatePath(
                  appConfig.root,
                  '/logos/usaspending_gov.png'
                )}
                style={{
                  marginLeft: '0.2em',
                  verticalAlign: 'sub',
                  height: '1.2em',
                }}
              />
            </a>
          </th>
          <th scope="col">
            <a href="https://www.fpds.gov/">fpds.gov</a>
          </th>
        </tr>
      </thead>
      <tbody>
        {filing.piid ? (
          <tr>
            {filing.parent_piid ? <th scope="row"></th> : undefined}
            <td>
              <a href={util.getSearchURL(filing.piid)}>{filing.piid}</a>
            </td>
            <td>{filing.mod_number}</td>
            {kContractTypesExcludedFromUSASpending.includes(
              filing.contract_action_type
            ) ? (
              <td>N/A for OTAs</td>
            ) : (
              <td>
                <a href={usaSpendingSearchURL(filing.piid)}>Link</a>
              </td>
            )}
            <td>
              <a href={wrapFPDSLink(filing.fpds_link)}>Link</a>
            </td>
          </tr>
        ) : undefined}
        {filing.parent_piid ? (
          <tr>
            <th scope="row">Parent</th>
            <td>
              <a href={util.getSearchURL(filing.parent_piid)}>
                {filing.parent_piid}
              </a>
            </td>
            <td>{filing.parent_mod_number}</td>
            <td>
              <a href={usaSpendingSearchURL(filing.parent_piid)}>Link</a>
            </td>
            <td>
              <a href={fpdsSearchURL(filing.parent_piid)}>Link</a>
            </td>
          </tr>
        ) : undefined}
      </tbody>
    </table>
  )
  ui.includeTableKeyValue(features, 'Contract Identifiers', identifiers)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderPrimeDates(filing) {
  const label = 'contractDates'
  const title = 'Contract Dates'

  let features = []

  ui.includeTableKeyDate(features, 'Signed Date', filing.signed_date)
  ui.includeTableKeyDate(features, 'Modified Date', filing.modified_date)
  ui.includeTableKeyDate(features, 'Effective Date', filing.effective_date)
  ui.includeTableKeyDate(
    features,
    'Current Completion Date',
    filing.current_completion_date
  )
  ui.includeTableKeyDate(
    features,
    'Ultimate Completion Date',
    filing.ultimate_completion_date
  )
  ui.includeTableKeyDate(
    features,
    'Last Date to Order',
    filing.last_date_to_order
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderPrimeValues(filing) {
  const label = 'contractValues'
  const title = 'Contract Values'
  const amountURL =
    'https://fedspendingtransparency.github.io/whitepapers/amount/'

  const body = (
    <table className="table table-sm table-striped table-fit">
      <thead>
        <tr>
          <th scope="col"></th>
          <th scope="col">
            <a href={amountURL}>Obligated</a>
          </th>
          <th scope="col">
            <a href={amountURL}>Exercised</a>
          </th>
          <th scope="col">
            <a href={amountURL}>Potential</a>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">Modification</th>
          <td>{util.numberWithCommas(filing.obligated_amount)}</td>
          <td>
            {util.numberWithCommas(filing.base_and_exercised_options_value)}
          </td>
          <td>{util.numberWithCommas(filing.base_and_all_options_value)}</td>
        </tr>
        <tr>
          <th scope="row">Total</th>
          <td>{util.numberWithCommas(filing.total_obligated_amount)}</td>
          <td>
            {util.numberWithCommas(
              filing.total_base_and_exercised_options_value
            )}
          </td>
          <td>
            {util.numberWithCommas(filing.total_base_and_all_options_value)}
          </td>
        </tr>
      </tbody>
    </table>
  )
  return <AccordionItem key={title} label={label} title={title} body={body} />
}

function renderPrimeProductOrServiceInfo(filing) {
  if (!(filing.product_or_service_code || filing.principal_naics_code)) {
    return undefined
  }

  const label = 'productOrService'
  const title = 'Product / Service Info'

  let features = []

  ui.includeTableKeyValue(
    features,
    'Major Program',
    filing.major_program_code.toUpperCase()
  )

  // Append the National Interest Action Code, if it exists.
  const niacURL =
    'https://www.fai.gov/sites/default/files/' +
    'Memorandum_for_Chief_Acquisition_Officer_Council_NIA_Code_V4.0.pdf'
  const niacTitle = <a href={niacURL}>National Interest Action Code</a>
  let niacLines = []
  if (filing.niac_text && filing.niac_text != 'none') {
    niacLines.push(filing.niac_text)
  }
  if (filing.niac_description && filing.niac_description != 'none') {
    niacLines.push(filing.niac_description)
  }
  const niacLabel = niacLines.join(': ').toUpperCase()
  ui.includeTableKeyValue(features, niacTitle, niacLabel)

  if (
    filing.product_or_service_code &&
    filing.product_or_service_code != 'None'
  ) {
    ui.includeTableKeyValue(
      features,
      'Product or Service Code',
      `${filing.product_or_service_code}: ` +
        `${filing.annotation.product_or_service}`
    )
  }
  if (filing.principle_naics_code && filing.principle_naics_code != 'None') {
    ui.includeTableKeyValue(
      features,
      'Principle NAICS Code',
      `${filing.principle_naics_code}: ${filing.annotation.naics}`
    )
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderPrimeDescription(filing) {
  const label = 'description'
  const title = 'Description'

  let features = []

  // Convert a PIID into a form such as W911QX-22-C-0031.
  function hyphenatePIID(piid: string): string {
    if (!piid) {
      return piid
    }
    if (piid.length == 13) {
      return `${piid.substring(0, 6)}-${piid.substring(6, 8)}-${piid.substring(
        8,
        9
      )}-${piid.substring(9, 13)}`
    } else {
      // TODO(Jack Poulson): Add further options.
      return piid
    }
  }

  function piidSearchString(piid: string): string {
    const hyphenatedPIID = hyphenatePIID(piid)
    if (piid == hyphenatedPIID) {
      return piid
    } else {
      return `${piid} OR ${hyphenatedPIID}`
    }
  }

  if (filing.solicitation_id) {
    ui.includeTableKeyValue(
      features,
      'Solicitation ID',
      <a href={util.getSearchURL(piidSearchString(filing.solicitation_id))}>
        {hyphenatePIID(filing.solicitation_id.toUpperCase())}
      </a>
    )
  }

  ui.includeTableKeyValue(
    features,
    'Contract Requirement',
    filing.description.toUpperCase()
  )
  ui.includeTableKeyValue(
    features,
    'Place of Performance',
    filing.annotation.placeOfPerformance
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderPrimeOffice(filing, contracting = true) {
  const annotation = contracting
    ? filing.annotation.contracting
    : filing.annotation.funding

  const titlePrefix = contracting ? 'Contracting' : 'Funding'
  const labelPrefix = contracting ? 'contracting' : 'funding'
  const title = `${titlePrefix} Office: ${annotation.office.toUpperCase()}`
  const label = `${labelPrefix}Office`

  const logoEntry = annotation.agencyLogo ? (
    <a href={util.getEntityURL(annotation.agencyNorm)}>
      <img
        className="ti-association-list-entity-logo"
        src={annotation.agencyLogo}
      />
    </a>
  ) : (
    <span style={{ fontSize: '1.5rem' }}>
      {annotation.agencyStylized.toUpperCase()}
    </span>
  )

  const features = [
    <ui.TableKeyValue
      k="Department"
      v={util.coerceUpperCase(annotation.department)}
    />,
    util.coerceUpperCase(annotation.agency) !=
    util.coerceUpperCase(annotation.department) ? (
      <ui.TableKeyValue
        k="Agency"
        v={util.coerceUpperCase(annotation.agency)}
      />
    ) : undefined,
    <ui.TableKeyValue k="Office" v={util.coerceUpperCase(annotation.office)} />,
  ]

  return (
    <AccordionItemWithLogo
      label={label}
      title={title}
      features={features}
      logo={logoEntry}
    />
  )
}

export class PrimeAward extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = [
      'uei',
      'piid',
      'mod_number',
      'parent_piid',
      'parent_mod_number',
    ]
    super(props)
    super.setEndpoint('/api/us/central/procurement')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing

    let title = <code className="text-dark">{filing.title}</code>

    const haveUSASpendingGuess =
      filing.piid &&
      !kContractTypesExcludedFromUSASpending.includes(
        filing.contract_action_type
      )
    const usaSpendingGuess = haveUSASpendingGuess
      ? usaSpendingSearchURL(filing.piid)
      : undefined

    const header = (
      <div className="card-text mb-3">
        <div className="mb-2">
          <a
            className="btn btn-light me-2"
            href={wrapFPDSLink(filing.fpds_link)}
            role="button"
          >
            View in FPDS
          </a>
          {haveUSASpendingGuess ? (
            <a className="btn btn-light" href={usaSpendingGuess} role="button">
              View in USASpending
            </a>
          ) : undefined}
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </div>
    )

    let accordionItems = []
    accordionItems.push(renderPrimeVendor(filing))
    accordionItems.push(renderPrimeContractor(filing))
    accordionItems.push(renderPrimeOffice(filing, true))
    accordionItems.push(renderPrimeOffice(filing, false))
    accordionItems.push(renderPrimeActionDetails(filing))
    accordionItems.push(renderPrimeValues(filing))
    accordionItems.push(renderPrimeDates(filing))
    accordionItems.push(renderPrimeProductOrServiceInfo(filing))
    accordionItems.push(renderPrimeDescription(filing))
    let accordion = (
      <Accordion id="us-central-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

function subawardPlaceOfPerformanceLines(filing) {
  function pushIfNonEmpty(array, item) {
    if (item) {
      array.push(item)
    }
  }

  let lines = []
  pushIfNonEmpty(lines, filing.sub_pop_address_line_1)

  let cityLines = []
  pushIfNonEmpty(cityLines, filing.sub_pop_city)
  pushIfNonEmpty(cityLines, filing.sub_pop_state)
  pushIfNonEmpty(cityLines, filing.sub_pop_zipcode)
  pushIfNonEmpty(cityLines, filing.sub_pop_country)
  pushIfNonEmpty(lines, cityLines.join(', '))

  return lines
}

function renderSubawardPrimeContractor(filing) {
  const label = 'prime'
  const annotation = filing.annotation
  const title = `Prime Contractor: ${annotation.prime.stylizedText.toUpperCase()}`

  const logoEntry = annotation.prime.logo ? (
    <a href={util.getEntityURL(annotation.prime.text)}>
      <img
        className="ti-association-list-entity-logo"
        src={annotation.prime.logo}
      />
    </a>
  ) : (
    <span style={{ fontSize: '1.5rem' }}>
      <a href={util.getEntityURL(annotation.prime.text)}>
        {annotation.prime.stylizedText.toUpperCase()}
      </a>
    </span>
  )

  const features = [
    <ui.TableKeyValue k="Name" v={util.coerceUpperCase(filing.prime_name)} />,
    filing.prime_dba &&
    util.coerceUpperCase(filing.prime_dba) !=
      util.coerceUpperCase(filing.prime_name) ? (
      <ui.TableKeyValue k="DBA" v={util.coerceUpperCase(filing.prime_dba)} />
    ) : undefined,
    <ui.TableKeyValue k="UEI" v={util.coerceUpperCase(filing.prime_uei)} />,
    filing.prime_parent_name ? (
      <ui.TableKeyValue
        k="Parent Name"
        v={util.coerceUpperCase(filing.prime_parent_name)}
      />
    ) : undefined,
    filing.prime_parent_uei ? (
      <ui.TableKeyValue
        k="Parent UEI"
        v={util.coerceUpperCase(filing.prime_parent_uei)}
      />
    ) : undefined,
  ]

  return (
    <AccordionItemWithLogo
      label={label}
      title={title}
      features={features}
      logo={logoEntry}
    />
  )
}

function renderSubawardSubcontractor(filing) {
  const label = 'subcontractor'
  const annotation = filing.annotation
  const title = `Subcontractor: ${annotation.sub.stylizedText.toUpperCase()}`

  const logoEntry = annotation.sub.logo ? (
    <a href={util.getEntityURL(annotation.sub.text)}>
      <img
        className="ti-association-list-entity-logo"
        src={annotation.sub.logo}
      />
    </a>
  ) : (
    <span style={{ fontSize: '1.5rem' }}>
      <a href={util.getEntityURL(annotation.sub.text)}>
        {annotation.sub.stylizedText.toUpperCase()}
      </a>
    </span>
  )

  const features = [
    <ui.TableKeyValue k="Name" v={util.coerceUpperCase(filing.sub_name)} />,
    filing.sub_dba ? (
      <ui.TableKeyValue k="DBA" v={util.coerceUpperCase(filing.sub_dba)} />
    ) : undefined,
    <ui.TableKeyValue k="UEI" v={util.coerceUpperCase(filing.sub_uei)} />,
    filing.sub_parent_name ? (
      <ui.TableKeyValue
        k="Parent Name"
        v={util.coerceUpperCase(filing.sub_parent_name)}
      />
    ) : undefined,
    filing.sub_parent_uei ? (
      <ui.TableKeyValue
        k="Parent UEI"
        v={util.coerceUpperCase(filing.sub_parent_uei)}
      />
    ) : undefined,
  ]

  return (
    <AccordionItemWithLogo
      label={label}
      title={title}
      features={features}
      logo={logoEntry}
    />
  )
}

function renderSubawardOffice(filing, contracting = true) {
  const annotation = contracting
    ? filing.annotation.contracting
    : filing.annotation.funding

  const titlePrefix = contracting ? 'Contracting' : 'Funding'
  const labelPrefix = contracting ? 'contracting' : 'funding'
  const title = `${titlePrefix} Office: ${annotation.office.toUpperCase()}`
  const label = `${labelPrefix}Office`

  const logoEntry = annotation.agencyLogo ? (
    <a href={util.getEntityURL(annotation.agencyNorm)}>
      <img
        className="ti-association-list-entity-logo"
        src={annotation.agencyLogo}
      />
    </a>
  ) : (
    <span style={{ fontSize: '1.5rem' }}>
      {annotation.agencyStylized.toUpperCase()}
    </span>
  )

  const features = [
    <ui.TableKeyValue
      k="Department"
      v={util.coerceUpperCase(annotation.origDept)}
    />,
    util.coerceUpperCase(annotation.origAgency) !=
    util.coerceUpperCase(annotation.origDept) ? (
      <ui.TableKeyValue
        k="Agency"
        v={util.coerceUpperCase(annotation.origAgency)}
      />
    ) : undefined,
    <ui.TableKeyValue k="Office" v={util.coerceUpperCase(annotation.office)} />,
  ]

  return (
    <AccordionItemWithLogo
      label={label}
      title={title}
      features={features}
      logo={logoEntry}
    />
  )
}

function renderSubawardActionDetails(filing) {
  const label = 'contractActionDetails'
  const title = 'Contract Action Details'

  let features = []

  ui.includeTableKeyValue(features, 'Subaward Number', filing.sub_number)
  ui.includeTableKeyValue(
    features,
    'Prime Award PIID',
    <a href={util.getSearchURL(filing.prime_piid)}>{filing.prime_piid}</a>,
    true,
    filing.prime_piid
  )
  ui.includeTableKeyValue(
    features,
    'Prime Award Parent PIID',
    <a href={util.getSearchURL(filing.prime_parent_piid)}>
      {filing.prime_parent_piid}
    </a>,
    true,
    filing.prime_parent_piid
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderSubawardDates(filing) {
  const label = 'contractDates'
  const title = 'Contract Dates'

  const month = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ]

  function monthFromOneIndexed(value) {
    return month[parseInt(value) - 1]
  }

  let features = []

  ui.includeTableKeyDate(
    features,
    'Subaward Action Date',
    filing.sub_action_date
  )
  ui.includeTableKeyDate(
    features,
    'Prime Award Base Action Date',
    filing.prime_base_action_date
  )
  ui.includeTableKeyDate(
    features,
    'Subaward Last Modified Date',
    filing.sub_fsrs_last_modified
  )
  if (filing.sub_fsrs_year && filing.sub_fsrs_month) {
    ui.includeTableKeyValue(
      features,
      'Subaward Report',
      `${monthFromOneIndexed(filing.sub_fsrs_month)} ` + filing.sub_fsrs_year
    )
  } else if (filing.sub_fsrs_year) {
    ui.includeTableKeyValue(
      features,
      'Subaward Report Year',
      filing.sub_fsrs_year
    )
  } else if (filing.sub_fsrs_month) {
    ui.includeTableKeyValue(
      features,
      'Subaward Report Month',
      monthFromOneIndexed(filing.sub_fsrs_month)
    )
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderSubawardValues(filing) {
  const label = 'subawardValues'
  const title = 'Subaward Amount'

  let features = []

  ui.includeTableKeyValue(
    features,
    'Subaward Amount',
    util.numberWithCommas(filing.sub_amount)
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderSubawardDescription(filing) {
  const label = 'subawardDescription'
  const title = 'Subaward Description'

  let features = []

  ui.includeTableKeyValue(
    features,
    'Subaward Description',
    filing.sub_description
  )

  const popLines = subawardPlaceOfPerformanceLines(filing)
  if (popLines.length) {
    ui.includeTableKeyValue(features, 'Place of Performance', popLines)
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

export class SubAward extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = ['prime_key', 'sub_number', 'sub_name']
    super(props)
    super.setEndpoint('/api/us/central/procurement/subaward')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing

    const maxTitleLength = 200

    const usaSpendingURL = filing.prime_key
      ? usaSpendingAwardURL(filing.prime_key)
      : undefined

    const title = (
      <code className="text-dark">
        {util.trimLabel(filing.sub_description, maxTitleLength)}
      </code>
    )

    const header = usaSpendingURL ? (
      <p className="card-text mb-3">
        <div className="mb-2">
          <a className="btn btn-light" href={usaSpendingURL} role="button">
            View in USASpending
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    ) : undefined

    let accordionItems = []
    accordionItems.push(renderSubawardPrimeContractor(filing))
    accordionItems.push(renderSubawardSubcontractor(filing))
    accordionItems.push(renderSubawardOffice(filing, true))
    accordionItems.push(renderSubawardOffice(filing, false))
    accordionItems.push(renderSubawardActionDetails(filing))
    accordionItems.push(renderSubawardValues(filing))
    accordionItems.push(renderSubawardDates(filing))
    accordionItems.push(renderSubawardDescription(filing))
    let accordion = (
      <Accordion id="us-central-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(item) {
  return item.source == 'prime' ? (
    <PrimeAward filing={item.filing} />
  ) : (
    <SubAward filing={item.filing} />
  )
}
