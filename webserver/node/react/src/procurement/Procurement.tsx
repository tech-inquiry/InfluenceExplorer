import * as React from 'react'

import { AUProcurement } from './AU'
import { CanadaBuys } from './CanadaBuys'
import { CAProactiveProcurement } from './CAProactive'
import { CAPWSGCProcurement } from './CAPWSGC'
import { EUProcurement } from './EU'
import { ILProcurementExempt, ILProcurementTender } from './IL'
import { NZProcurement } from './NZ'
import { UATender } from './UATender'
import { UKProcurement } from './UK'
import { USCentralGrants } from './USCentralGrants'
import { USCentralProcurement } from './USCentral'
import { USCentralOpportunities } from './USCentralOpportunities'
import { USDODAnnounce } from './USDODAnnounce'
import { USAZProcurement } from './USAZ'
import { USCAProcurement } from './USCA'
import { USFLProcurement } from './USFL'
import { USTXDPSProcurement } from './USTXDPS'
import { kEUCountryCodes, kProcurementPaths } from '../utilities/api'
import * as ui from '../utilities/ui'
import { coerce } from '../utilities/util'

function euSource(countryCode) {
  return `EU ${countryCode.toUpperCase()}`
}

function euAgencySource(countryCode) {
  return `EU ${countryCode.toUpperCase()} (as agency)`
}

class ProcurementRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = coerce(this.props.label, 'procurement')
    this.sourceLabel = this.sourceLabel.bind(this)
    this.sourceModalId = this.sourceModalId.bind(this)
  }

  sourceLabel(source) {
    return `${this.label}-${source}`
  }

  sourceModalId(source) {
    return ui.modalId(this.sourceLabel(source))
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    const isEntity = profile != undefined
    const sources = this.props.sources

    const that = this

    const capImage = (
      <div
        className="mx-auto mb-0 ti-cap-img"
        style={{
          opacity: 1.0,
          height: '100px',
          maxWidth: '250px',
          backgroundImage: 'url("https://techinquiry.org/logos/contract.png")',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: '50% 50%',
          backgroundSize: 'contain',
        }}
      ></div>
    )

    const target = isEntity
      ? profile.entityAnnotations[info.query].stylizedText
      : info.query
    const footerText = (
      <>
        Tech Inquiry has procurement records for{' '}
        <span className="fst-italic">{target}</span> from{' '}
        <span className="fw-bold">
          {sources.length + ' '}
          {sources.length > 1 ? 'sources' : 'source'}
        </span>
        .
      </>
    )

    // We will place all of the modal objects up top.
    let enabledSources = [
      'AU (as vendor)',
      'AU (as agency)',
      'CA proactive (as vendor)',
      'CA proactive (as agency)',
      'CA PWSGC (as vendor)',
      'CA PWSGC (as agency)',
      'Canada Buys (as vendor)',
      'Canada Buys (as agency)',
      'IL Exempt (as vendor)',
      'IL Exempt (as agency)',
      'IL Tender (as vendor)',
      'IL Tender (as agency)',
      'NZ',
      'UA tender (as vendor)',
      'UA tender (as agency)',
      'UK (as vendor)',
      'UK (as agency)',
      'US federal opportunities',
      'US DOD announcements',
      'US federal (as vendor)',
      'US federal (as agency)',
      'US federal grants (as vendor)',
      'US federal grants (as agency)',
      'US AZ',
      'US CA (as vendor)',
      'US CA (as agency)',
      'US FL (as vendor)',
      'US FL (as agency)',
      'US TX DPS (as agency)',
      'US TX DPS (as vendor)',
    ]
    for (const countryCode of kEUCountryCodes) {
      enabledSources.push(euSource(countryCode))
      enabledSources.push(euAgencySource(countryCode))
    }

    function sourceToItem(source, index) {
      return ui.dropdownSource(
        source,
        index,
        enabledSources,
        kProcurementPaths,
        that.sourceModalId
      )
    }

    return (
      <ui.RowHelper
        capImage={capImage}
        button={ui.getCardDropdownButton(
          'Procurement',
          `${this.label}-dropdownMenuButton`,
          sources.map(sourceToItem)
        )}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

class ProcurementModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = coerce(this.props.label, 'procurement')
    this.sourceLabel = this.sourceLabel.bind(this)
  }

  sourceLabel(source) {
    return `${this.label}-${source}`
  }

  render() {
    const info = this.props.info
    const isEntity = info.profile != undefined

    const sourceLabel = this.sourceLabel

    const sourceObjects = (
      <div>
        <AUProcurement
          label={sourceLabel('AU (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <AUProcurement
          label={sourceLabel('AU (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <CAProactiveProcurement
          label={sourceLabel('CA proactive (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <CAProactiveProcurement
          label={sourceLabel('CA proactive (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <CAPWSGCProcurement
          label={sourceLabel('CA PWSGC (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <CAPWSGCProcurement
          label={sourceLabel('CA PWSGC (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <CanadaBuys
          label={sourceLabel('Canada Buys (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <CanadaBuys
          label={sourceLabel('Canada Buys (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <ILProcurementExempt
          label={sourceLabel('IL Exempt (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <ILProcurementExempt
          label={sourceLabel('IL Exempt (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <ILProcurementTender
          label={sourceLabel('IL Tender (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <ILProcurementTender
          label={sourceLabel('IL Tender (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <NZProcurement
          label={sourceLabel('NZ')}
          query={info.query}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <UATender
          label={sourceLabel('UA tender (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <UATender
          label={sourceLabel('UA tender (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <UKProcurement
          label={sourceLabel('UK (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <UKProcurement
          label={sourceLabel('UK (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USCentralProcurement
          label={sourceLabel('US federal (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USCentralProcurement
          label={sourceLabel('US federal (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USCentralGrants
          label={sourceLabel('US federal grants (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USCentralGrants
          label={sourceLabel('US federal grants (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        {isEntity ? undefined : (
          <USCentralOpportunities
            label={sourceLabel('US federal opportunities')}
            query={info.query}
            isAgency={false}
            isEntity={isEntity}
            isTag={info.isTag}
          />
        )}
        {isEntity ? undefined : (
          <USDODAnnounce
            label={sourceLabel('US DOD announcements')}
            query={info.query}
            isAgency={false}
            isEntity={isEntity}
            isTag={info.isTag}
          />
        )}
        <USAZProcurement
          label={sourceLabel('US AZ')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USCAProcurement
          label={sourceLabel('US CA (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USCAProcurement
          label={sourceLabel('US CA (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USFLProcurement
          label={sourceLabel('US FL (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USFLProcurement
          label={sourceLabel('US FL (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USTXDPSProcurement
          label={sourceLabel('US TX DPS (as agency)')}
          query={info.query}
          isAgency={true}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USTXDPSProcurement
          label={sourceLabel('US TX DPS (as vendor)')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        {kEUCountryCodes.map(function (countryCode) {
          return (
            <>
              <EUProcurement
                key={countryCode}
                countryCode={countryCode}
                label={sourceLabel(euSource(countryCode))}
                query={info.query}
                isAgency={false}
                isEntity={isEntity}
                isTag={info.isTag}
              />
              <EUProcurement
                key={countryCode + ' (as agency)'}
                countryCode={countryCode}
                label={sourceLabel(euAgencySource(countryCode))}
                query={info.query}
                isAgency={true}
                isEntity={isEntity}
                isTag={info.isTag}
              />
            </>
          )
        })}
      </div>
    )

    return sourceObjects
  }
}

export function insertCardAndModal(
  info,
  sources,
  cards: any[],
  modals: any[]
): void {
  if (!sources.length) {
    return
  }
  cards.push(<ProcurementRow info={info} sources={sources} />)
  modals.push(<ProcurementModal info={info} />)
}
