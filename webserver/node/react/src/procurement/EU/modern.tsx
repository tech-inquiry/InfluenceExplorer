import * as React from 'react'
import { readFileSync } from 'fs'

import {
  Accordion,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

import * as euUtil from './util'

type Dict = { [key: string]: any }
type Filing = Dict

const cpvURL = 'https://techinquiry.org/data/cpv.json'
let cpvMap = undefined

export async function ensureCpvMap() {
  while (cpvMap == undefined) {
    try {
      const response = await fetch(cpvURL)
      if (!response.ok) {
        throw Error(response.statusText)
      }
      cpvMap = await response.json()
    } catch (error) {
      console.log(error)
    }
  }
}

function cpvStringFromCode(code: string): string {
  if (cpvMap.hasOwnProperty(code)) {
    return cpvMap[code]
  }
  for (let i = 9; i >= 0; i -= 1) {
    const fallbackCode = `${code}-${i}`
    if (cpvMap.hasOwnProperty(fallbackCode)) {
      return cpvMap[fallbackCode]
    }
  }
  // We could not find the description, so just return the code itself.
  return `CPV code: ${code}`
}

function codesToString(codeList: string[]): string {
  let texts = []
  for (const code of codeList) {
    texts.push(cpvStringFromCode(code))
  }
  return texts.join('; ')
}

export function getActionDate(filing: Filing): string {
  return util.dateWithoutTimeLex(filing['last_action_date'])
}

function getTown(filing: Filing): string {
  const buyers = getBuyerInfo(filing)
  for (const buyer of buyers) {
    if (!buyer.hasOwnProperty('address')) {
      continue
    }
    const address = buyer['address']
    if (address.hasOwnProperty('city')) {
      return address['city']
    }
  }
  return undefined
}

function getCPVCode(filing: Filing): string {
  for (const [language, langData] of Object.entries(filing.data)) {
    if (langData.hasOwnProperty('project')) {
      const project = langData['project']
      if (project.hasOwnProperty('main_commodity_classification')) {
        // TODO(Jack Poulson): Verify there is a list rather than a string.
        return project['main_commodity_classification'][0]
      }
    }
    if (langData.hasOwnProperty('project_lots')) {
      const lots = langData['project_lots']
      for (const lot of lots) {
        if (lot.hasOwnProperty('project')) {
          const project = lot['project']
          if (project.hasOwnProperty('main_commodity_classification')) {
            // TODO(Jack Poulson): Verify there is a list rather than a string.
            return project['main_commodity_classification'][0]
          }
        }
      }
    }
  }
  return undefined
}

export function getCPV(filing: Filing): string {
  const code = getCPVCode(filing)
  return cpvStringFromCode(code)
}

export function getTitles(filing: Filing): Dict {
  return {
    cpv: getCPV(filing),
    contractTitle: euUtil.serializeLanguageDict(getContractTitle(filing)),
  }
}

export function getMaxValue(filing: Filing): Dict {
  for (const [language, langData] of Object.entries(filing.data)) {
    if (!langData.hasOwnProperty('extension')) {
      continue
    }
    const extension = langData['extension']
    if (!extension.hasOwnProperty('notice_result')) {
      continue
    }
    const noticeResult = extension['notice_result']
    if (noticeResult.hasOwnProperty('total_amount')) {
      const totalAmount = noticeResult['total_amount']
      return {
        amount: euUtil.parseNumber(totalAmount['amount']),
        currency: totalAmount['currency'],
      }
    }
  }
  return { amount: 0, currency: 'EUR' }
}

export function getNumContractAwards(filing: Filing): number {
  for (const [language, langData] of Object.entries(filing.data)) {
    let numAwards = 0
    if (!langData.hasOwnProperty('extension')) {
      continue
    }
    const extension = langData['extension']
    if (!extension.hasOwnProperty('notice_result')) {
      continue
    }
    const noticeResult = extension['notice_result']
    if (!noticeResult.hasOwnProperty('lot_result')) {
      continue
    }
    if (!Array.isArray(noticeResult['lot_result'])) {
      console.log(`lot_result for doc_id ${filing.doc_id} was not an array.`)
      return 0
    }
    for (const lotResult of noticeResult['lot_result']) {
      if (
        lotResult.hasOwnProperty('tender_result') &&
        lotResult['tender_result'] == 'selec-w'
      ) {
        numAwards += 1
      }
    }
    if (numAwards) {
      return numAwards
    }
  }
  return 0
}

export function getContractTitle(filing: Filing): { [key: string]: string } {
  let titles = {}
  for (const [language, langData] of Object.entries(filing.data)) {
    if (langData.hasOwnProperty('project')) {
      const project = langData['project']
      titles[language] = project['name']
    }
  }
  return titles
}

export function getContractShortDescription(filing: Filing): {
  [key: string]: string
} {
  let shortDescr = {}
  for (const [language, langData] of Object.entries(filing.data)) {
    if (langData.hasOwnProperty('project')) {
      const project = langData['project']
      shortDescr[language] = project['description']
    }
  }
  return shortDescr
}

export function getBuyers(filing: Filing): string[] {
  const buyers = getBuyerInfo(filing)
  let namesList = []
  for (const buyer of buyers) {
    namesList.push(buyer['name'])
  }
  return namesList
}

function getBuyer(filing: Filing): Dict {
  // TODO(Jack Poulson)
  return {}
}

export function getContractorInfo(filing: Filing): any[] {
  for (const [language, langData] of Object.entries(filing.data)) {
    if (!langData.hasOwnProperty('extension')) {
      continue
    }
    let contractors = []
    const extension = langData['extension']
    if (!extension.hasOwnProperty('notice_result')) {
      continue
    }
    let idList = []
    const noticeResult = extension['notice_result']
    if (noticeResult.hasOwnProperty('tendering_parties')) {
      for (const party of noticeResult['tendering_parties']) {
        if (party.hasOwnProperty('tenderers')) {
          for (const tenderer of party['tenderers']) {
            idList.push(tenderer['id'])
          }
        }
      }
    }
    if (extension.hasOwnProperty('organizations')) {
      for (const organization of extension['organizations']) {
        if (idList.includes(organization['party_id'])) {
          contractors.push(organization)
        }
      }
    } else {
      contractors = idList
    }
    if (contractors.length) {
      return contractors
    }
  }
  return []
}

export function getBuyerInfo(filing: Filing): any[] {
  for (const [language, langData] of Object.entries(filing.data)) {
    let buyers = []
    if (!langData.hasOwnProperty('contracting_parties')) {
      continue
    }
    let idList = []
    for (const party of langData['contracting_parties']) {
      idList.push(party['id'])
    }
    if (langData.hasOwnProperty('extension')) {
      const extension = langData['extension']
      if (extension.hasOwnProperty('organizations')) {
        for (const organization of extension['organizations']) {
          if (idList.includes(organization['party_id'])) {
            buyers.push(organization)
          }
        }
      }
    } else {
      buyers = idList
    }
    if (buyers.length) {
      return buyers
    }
  }
  return []
}

export function getContractors(filing: Filing): string[] {
  const contractors = getContractorInfo(filing)
  let namesList = []
  for (const contractor of contractors) {
    namesList.push(contractor['name'])
  }
  return namesList
}

function renderOrganization(
  filing: Filing,
  org,
  isBuyer: boolean,
  tag = undefined
): any {
  const labelType = isBuyer ? 'buyer' : 'contractor'
  const titleType = isBuyer ? 'Buyer' : 'Contractor'
  const label = tag == undefined ? labelType : `${labelType}-${tag}`
  const title = tag == undefined ? titleType : `${titleType} ${tag}`

  let features = []

  let annotation = undefined

  const name = org['name']
  ui.includeTableKeyValue(features, 'Name', name)
  if (name) {
    annotation = filing.annotation.entities[name.toLowerCase()]
  }

  let addressObj = {}
  const address = org['address']
  if (address) {
    addressObj['streetLines'] = [address['department'], address['street']]
    addressObj['city'] = address['city']
    addressObj['postalCode'] = address['postal']
    addressObj['country'] = address['country']
  }
  const contact = org['contact']
  if (contact) {
    addressObj['email'] = contact['email']
    addressObj['phone'] = contact['phone']
  }
  ui.includeTableKeyValue(features, 'Address', ui.simpleAddress(addressObj))

  const generalURL = util.prefixURL(org['website'])
  ui.includeTableKeyValue(
    features,
    'General URL',
    <a href={generalURL}>{util.simplifyURL(generalURL)}</a>,
    true,
    generalURL
  )
  if (contact) {
    ui.includeTableKeyValue(features, 'Contact Name', contact['name'])
  }
  // TODO(Jack Poulson): Determine if entities list is the ID.
  //ui.includeTableKeyValue(
  //  features,
  //  'National ID',
  //  address['national_id']
  //)

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderBuyers(filing: Filing): any {
  const isBuyer = true
  const buyers = getBuyerInfo(filing)
  if (!buyers.length) {
    return undefined
  }
  if (buyers.length == 1) {
    // Directly render the single buyer.
    return renderOrganization(filing, buyers[0], isBuyer)
  } else {
    // Render a list of buyers.
    let items = []
    for (const [index, buyer] of buyers.entries()) {
      items.push(renderOrganization(filing, buyer, isBuyer, index + 1))
    }
    return <Accordion id="buyers-accordion" items={items} />
  }
}

function renderContractors(filing: Filing): any {
  const isBuyer = false
  const contractors = getContractorInfo(filing)
  if (!contractors.length) {
    return undefined
  }
  if (contractors.length == 1) {
    // Directly render the single contractor.
    return renderOrganization(filing, contractors[0], isBuyer)
  } else {
    // Render a list of contractors.
    let items = []
    for (const [index, contractor] of contractors.entries()) {
      items.push(renderOrganization(filing, contractor, isBuyer, index + 1))
    }
    return <Accordion id="contractors-accordion" items={items} />
  }
}

function renderDescriptions(filing: Filing): any {
  const label = 'descriptions'
  const title = 'Descriptions'

  let features = []

  for (const [language, langData] of Object.entries(filing.data)) {
    if (langData.hasOwnProperty('project')) {
      const project = langData['project']
      ui.includeTableKeyValue(
        features,
        `Description [${language.toUpperCase()}]`,
        project['description']
      )
      ui.includeTableKeyValue(
        features,
        'Procurement Type',
        project['procurement_type']
      )
    }
    if (langData.hasOwnProperty('extension')) {
      const extension = langData['extension']
      if (extension.hasOwnProperty('modification')) {
        const modification = extension['modification']
        if (modification.hasOwnProperty('change')) {
          ui.includeTableKeyValue(
            features,
            'Change Description',
            modification['change']['description']
          )
        }
      }
    }
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderAmounts(filing: Filing): any {
  const label = 'amounts'
  const title = 'Amounts'

  let features = []

  for (const [language, langData] of Object.entries(filing.data)) {
    if (langData.hasOwnProperty('project')) {
      const project = langData['project']
      if (project.hasOwnProperty('requested_tender_total')) {
        const requested = project['requested_tender_total']
        if (requested.hasOwnProperty('estimated')) {
          const estimated = requested['estimated']
          ui.includeTableKeyValue(
            features,
            'Estimated Tender Total',
            `${estimated['currency']} ${util.numberWithCommas(
              estimated['amount']
            )}`
          )
        }
      }
    }
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderCommodityClassifications(filing: Filing): any {
  const label = 'commodity-classifications'
  const title = 'Commodity Classifications'

  let features = []

  for (const [language, langData] of Object.entries(filing.data)) {
    if (langData.hasOwnProperty('project')) {
      const project = langData['project']
      if (project.hasOwnProperty('main_commodity_classification')) {
        ui.includeTableKeyValue(
          features,
          'Main Commodity Classification',
          codesToString(project['main_commodity_classification'])
        )
      }
      if (project.hasOwnProperty('additional_commodity_classification')) {
        ui.includeTableKeyValue(
          features,
          'Additional Commodity Classifications',
          codesToString(project['additional_commodity_classification'])
        )
      }
    }
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderIdentifiers(filing: Filing): any {
  const label = 'identifiers'
  const title = 'Identifiers'

  let features = []

  ui.includeTableKeyValue(features, 'Document ID', filing.doc_id)
  for (const [language, langData] of Object.entries(filing.data)) {
    if (langData.hasOwnProperty('extension')) {
      const extension = langData['extension']
      if (extension.hasOwnProperty('publication')) {
        const publication = extension['publication']
        ui.includeTableKeyValue(
          features,
          'Gazette ID',
          publication['gazette_id']
        )
        ui.includeTableKeyValue(
          features,
          'Publication Date',
          util.dateWithoutTimeLex(publication['date'])
        )
      }
    }
    ui.includeTableKeyValue(
      features,
      'Issue Date',
      util.dateWithoutTimeLex(langData['issue_date'])
    )
    if (langData.hasOwnProperty('notice_type')) {
      ui.includeTableKeyValue(
        features,
        'Notice Type',
        langData['notice_type'].toUpperCase()
      )
    }
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderProjectLot(lot, lotIndex): any {
  const label = `project-lot-${lotIndex + 1}`
  const title = `Project Lot ${lotIndex + 1}`

  let features = []

  if (lot.hasOwnProperty('project')) {
    const project = lot['project']
    ui.includeTableKeyValue(features, 'Title', project['name'])
    ui.includeTableKeyValue(features, 'Description', project['description'])
    ui.includeTableKeyValue(features, 'Note', project['note'])
    if (project.hasOwnProperty('planned_period')) {
      const period = project['planned_period']
      if (period.hasOwnProperty('amount')) {
        ui.includeTableKeyValue(
          features,
          'Planned Period',
          `${period['amount']} ${period['unit']}`
        )
      }
      // TODO(Jack Poulson): Consider changing upstream to 'start_date'.
      if (period.hasOwnProperty('startDate')) {
        ui.includeTableKeyValue(
          features,
          'Planned Start Date',
          util.dateWithoutTimeLex(period['startDate'])
        )
      }
      if (period.hasOwnProperty('endDate')) {
        ui.includeTableKeyValue(
          features,
          'Planned End Date',
          util.dateWithoutTimeLex(period['endDate'])
        )
      }
    }
    ui.includeTableKeyValue(
      features,
      'Main Commodity Classification',
      project['main_commodity_classification']
    )
    ui.includeTableKeyValue(
      features,
      'Additional Commodity Classifications',
      project['additional_commodity_classification']
    )
  }
  if (lot.hasOwnProperty('terms')) {
    const terms = lot['terms']
    if (terms.hasOwnProperty('call_for_tenders')) {
      for (const [callIndex, tenderCall] of terms[
        'call_for_tenders'
      ].entries()) {
        const id = util.isString(tenderCall['id'])
          ? tenderCall['id']
          : tenderCall['id']['#text']
        if (
          tenderCall.hasOwnProperty('attachment') &&
          tenderCall['attachment'].hasOwnProperty('uri')
        ) {
          const uri = tenderCall['attachment']['uri']
          ui.includeTableKeyValue(
            features,
            `Tender Call ${callIndex + 1}`,
            <a href={uri}>{id}</a>
          )
          console.log(`URI: ${uri}`)
        } else {
          ui.includeTableKeyValue(features, `Tender Call ${callIndex + 1}`, id)
        }
      }
    }
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderProjectLots(filing: Filing): any {
  const label = 'project-lots'
  const title = 'Project Lots'

  let items = []

  for (const [language, langData] of Object.entries(filing.data)) {
    if (langData.hasOwnProperty('project_lots')) {
      for (const [lotIndex, lot] of langData['project_lots'].entries()) {
        items.push(renderProjectLot(lot, lotIndex))
      }
    }
  }

  return <Accordion id="project-lots-accordion" items={items} />
}

export function renderAccordionItems(filing: Filing): any[] {
  let accordionItems = []
  accordionItems.push(renderIdentifiers(filing))
  accordionItems.push(renderBuyers(filing))
  accordionItems.push(renderContractors(filing))
  accordionItems.push(renderDescriptions(filing))
  accordionItems.push(renderAmounts(filing))
  accordionItems.push(renderCommodityClassifications(filing))
  accordionItems.push(renderProjectLots(filing))
  // TODO(Jack Poulson): Add more.
  return accordionItems
}
