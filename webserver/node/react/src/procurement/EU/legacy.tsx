import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

import * as euUtil from './util'

type Dict = { [key: string]: any }
type Filing = Dict

const kValueTypes = {
  cost_range: 'Cost Range',
  estimated_total: 'Estimated Total Value',
  total: 'Total Value',
}
const kRangeTypes = {
  // TODO(Jack Poulson): Update this to val_range_total after the change is
  // executed in workstation/gov/eu/procurement.py
  VAL_RANGE_TOTAL: 'Range of Total Value',
}

function renderBodyAndLogo(body, logo): any {
  return (
    <div className="container px-0">
      <div className="row flex-md-row-reverse align-items-center mx-0 px-0 py-3">
        <div className="col-12 col-md-4 align-items-center px-3 bg-light">
          <div className="text-center">{logo}</div>
        </div>
        <div className="col-12 col-md-8">{body}</div>
      </div>
    </div>
  )
}

export function getActionDate(filing: Filing): string {
  if (filing['last_action_date']) {
    return util.dateWithoutTimeLex(filing['last_action_date'])
  }

  const noticeData = getNoticeData(filing)
  if (noticeData['no_doc_ojs']) {
    return noticeData['no_doc_ojs'].substring(0, 4)
  }
  return undefined
}

function getTranslationCountry(filing: Filing): string {
  for (const [language, data] of Object.entries(filing.data)) {
    const translation = data['translation']
    if (!translation) {
      continue
    }
    if (translation.hasOwnProperty('title')) {
      const title = translation['title']
      return title['country']
    }
  }
  return undefined
}

function getTranslationTitle(filing: Filing): string {
  for (const [language, data] of Object.entries(filing.data)) {
    const translation = data['translation']
    if (!translation) {
      continue
    }
    if (
      translation.hasOwnProperty('title') &&
      translation['title'].hasOwnProperty('text')
    ) {
      return translation['title'].text.join('<br>')
    }
  }
  return undefined
}

function getTown(filing: Filing): string {
  for (const [language, data] of Object.entries(filing.data)) {
    const translation = data['translation']
    if (translation?.hasOwnProperty('title')) {
      return translation['title']['town']
    }
  }
  return undefined
}

function getNoticeData(filing: Filing): Dict {
  if (!filing.data.hasOwnProperty('en')) {
    return undefined
  }
  if (!filing.data.en.hasOwnProperty('coded_data')) {
    return undefined
  }
  // At the moment, this should always be in English.
  const codedData = filing.data.en.coded_data
  if (codedData?.notice_data) {
    return codedData.notice_data
  }
  return undefined
}

export function getCPV(filing: Filing): string {
  const noticeData = getNoticeData(filing)
  if (noticeData?.hasOwnProperty('original_cpv')) {
    return noticeData['original_cpv']
  }
  return undefined
}

export function getTitles(filing: Filing): Dict {
  return {
    cpv: getCPV(filing),
    contractTitle: euUtil.serializeLanguageDict(getContractTitle(filing)),
  }
}

function getComplementaryInfo(filing: Filing): Dict {
  let info = undefined
  for (const [language, data] of Object.entries(filing.data)) {
    const form = data['form']
    if (!form) {
      continue
    }
    if (form.hasOwnProperty('complementary_info')) {
      return form['complementary_info']
    }
  }
  return undefined
}

function getContractAwards(filing: Filing): Dict {
  let awards = {}
  for (const [language, data] of Object.entries(filing.data)) {
    const form = data['form']
    if (!form) {
      continue
    }
    if (form.hasOwnProperty('award_contracts')) {
      awards[language] = form['award_contracts']
    }
  }
  return awards
}

function getDefenceContractAwards(filing: Filing): Dict {
  let contractAwards = {}
  for (const [language, data] of Object.entries(filing.data)) {
    const form = data['form']
    if (!form || !form.hasOwnProperty('fd_contract_award_defence')) {
      continue
    }
    const defence = form['fd_contract_award_defence']
    if (defence.hasOwnProperty('contract_award')) {
      contractAwards[language] = defence['contract_award']
    }
  }
  return contractAwards
}

function getContractObjects(filing: Filing): Dict {
  let objects = {}
  for (const [language, data] of Object.entries(filing.data)) {
    const form = data['form']
    if (!form) {
      continue
    }
    if (form.hasOwnProperty('object_contract')) {
      objects[language] = form['object_contract']
    }
  }
  return objects
}

function getContractAwardValues(filing: Filing): Dict[] {
  let values = []
  const awards = getContractAwards(filing)
  if (!Object.keys(awards).length) {
    return values
  }

  for (const [language, data] of Object.entries(awards)) {
    for (const item of data) {
      if (!item) {
        continue
      }
      const contract = item.awarded_contract
      if (contract) {
        if (contract['values']) {
          values.push(contract['values'])
        }
      }
    }
  }
  return values
}

function getDefenceContractAwardValues(filing: Filing): Dict[] {
  let values = []
  const awards = getDefenceContractAwards(filing)
  if (!Object.keys(awards).length) {
    return values
  }

  for (const [language, data] of Object.entries(awards)) {
    for (const item of data) {
      if (!item) {
        continue
      }
      const contract = item.awarded_contract
      if (contract?.['values']) {
        values.push(contract['values'])
      }
    }
  }
  return values
}

function getContractObjectsValues(filing: Filing): Dict[] {
  let values = []
  const objects = getContractObjects(filing)
  if (!Object.keys(objects).length) {
    return values
  }
  for (const [language, data] of Object.entries(objects)) {
    if (data.hasOwnProperty('estimated_total')) {
      values.push(data['estimated_total'])
    }
  }
  return values
}

function getNoticeValues(filing: Filing): Dict[] {
  const noticeData = getNoticeData(filing)
  let valueList = []
  // TODO(Jack Poulson): Normalize these lists upstream.
  if (
    noticeData?.hasOwnProperty('VALUES_LIST') &&
    noticeData['VALUES_LIST']?.hasOwnProperty('VALUES') &&
    noticeData['VALUES_LIST']['VALUES']
  ) {
    let values = noticeData['VALUES_LIST']['VALUES']
    if (!Array.isArray(values)) {
      values = [values]
    }
    for (const value of values) {
      if (value.hasOwnProperty('SINGLE_VALUE')) {
        const singleValue = value['SINGLE_VALUE']['VALUE']
        valueList.push({
          amount: singleValue['#text'],
          currency: singleValue['@CURRENCY'],
          type: value['@TYPE'],
        })
      } else if (value.hasOwnProperty('RANGE_VALUE')) {
        for (const [rangeIndex, rangeValue] of value['RANGE_VALUE'][
          'VALUE'
        ].entries()) {
          valueList.push({
            amount: rangeValue['#text'],
            currency: rangeValue['@CURRENCY'],
            type: `${value['@TYPE']} (Range value ${rangeIndex})`,
          })
        }
      } else {
        console.log(`Improper noticeData value:`)
        console.log(value)
      }
    }
  }
  if (noticeData?.hasOwnProperty('values') && noticeData['values']) {
    const values = noticeData['values']
    if (values.hasOwnProperty('value') && values['value']) {
      valueList.push(values['value'])
    }
  }
  return valueList
}

export function getMaxValue(filing: Filing): Dict {
  // For now, we assume a fixed currency is used across the values.
  let maxAmount = 0
  let currency = ''
  function incorporateAmount(
    candidateAmount: string,
    candidateCurrency: string
  ) {
    const amount = euUtil.parseNumber(candidateAmount)
    if (amount > maxAmount) {
      maxAmount = amount
      currency = candidateCurrency
    }
  }

  for (const value of getContractAwardValues(filing)) {
    for (const valueType in kValueTypes) {
      const component = value[valueType]
      if (component) {
        incorporateAmount(component['amount'], component['currency'])
      }
    }
    for (const rangeType in kRangeTypes) {
      const component = value[rangeType]
      if (component) {
        // TODO(Jack Poulson): Update this to 'high' once the renaming is
        // executed in workstation/gov/eu/procurement.py
        incorporateAmount(component['HIGH'], component['@CURRENCY'])
      }
    }
  }
  for (const value of getDefenceContractAwardValues(filing)) {
    for (const valueType in kValueTypes) {
      const component = value[valueType]
      if (component) {
        incorporateAmount(component['amount'], component['currency'])
      }
    }
    for (const rangeType in kRangeTypes) {
      const component = value[rangeType]
      if (component) {
        // TODO(Jack Poulson): Update this to 'high' once the renaming is
        // executed in workstation/gov/eu/procurement.py
        incorporateAmount(component['HIGH'], component['@CURRENCY'])
      }
    }
  }
  for (const value of getContractObjectsValues(filing)) {
    incorporateAmount(value.amount, value.currency)
  }
  for (const value of getNoticeValues(filing)) {
    incorporateAmount(value.amount, value.currency)
  }
  return { amount: maxAmount, currency: currency }
}

export function getNumContractAwards(filing: Filing): number {
  let maxAwards = 0
  for (const [language, data] of Object.entries(getContractAwards(filing))) {
    let numAwards = 0
    for (const item of data) {
      if (!item) {
        continue
      }
      if (item.awarded_contract) {
        numAwards += 1
      }
    }
    maxAwards = Math.max(maxAwards, numAwards)
  }

  let maxDefenceAwards = 0
  for (const [language, data] of Object.entries(
    getDefenceContractAwards(filing)
  )) {
    let numAwards = 0
    for (const item of data) {
      if (!item) {
        continue
      }
      numAwards += 1
    }
    maxDefenceAwards = Math.max(maxDefenceAwards, numAwards)
  }

  return maxAwards + maxDefenceAwards
}

export function getContractTitle(filing: Filing): { [key: string]: string } {
  let titles = {}
  const objects = getContractObjects(filing)
  for (const [key, value] of Object.entries(objects)) {
    if (value.hasOwnProperty('title')) {
      if (value['title'].length) {
        titles[key] = value['title'].join('; ')
      }
    }
  }
  return titles
}

export function getContractShortDescription(filing: Filing): {
  [key: string]: string
} {
  let shortDescr = {}
  for (const [key, value] of Object.entries(filing.data)) {
    const form = value['form']
    if (!form) {
      continue
    }
    if (form.hasOwnProperty('object_contract')) {
      const objectContract = form.object_contract
      if (objectContract.hasOwnProperty('short_descr')) {
        if (objectContract.short_descr.length) {
          shortDescr[key] = objectContract.short_descr.join('; ')
        }
      }
    }
  }
  return shortDescr
}

export function getBuyers(filing: Filing): string[] {
  let namesList = []
  let namesSet = new Set()
  for (const [key, value] of Object.entries(filing.data)) {
    const translation = value['translation']
    if (!translation) {
      continue
    }
    if (translation.hasOwnProperty('names')) {
      for (const name of translation['names']) {
        if (name && !(name.toLowerCase() in namesSet)) {
          namesList.push(name)
          namesSet.add(name.toLowerCase())
        }
      }
    }
  }
  return namesList
}

export function getUniqueBuyers(filing: Filing): string[] {
  let names = []
  let nameSet = new Set<string>()
  for (const name of getBuyers(filing)) {
    const nameKey = name.toLowerCase()
    if (nameSet.has(nameKey)) {
      continue
    }
    names.push(name)
    nameSet.add(nameKey)
  }
  return names
}

export function getBuyer(filing: Filing): Dict {
  let authority = undefined
  for (const [key, value] of Object.entries(filing['data'])) {
    const form = value['form']
    if (!form) {
      continue
    }
    if (!form.hasOwnProperty('contracting_body')) {
      continue
    }
    return form['contracting_body']
  }
  return undefined
}

function getContractors(filing: Filing): string[] {
  let namesList = []
  let namesSet = new Set()
  for (const [key, value] of Object.entries(filing.data)) {
    const form = value['form']
    if (!form || !form.hasOwnProperty('award_contracts')) {
      continue
    }
    for (const contract of form['award_contracts']) {
      if (!contract) {
        continue
      }
      if (!contract.hasOwnProperty('awarded_contract')) {
        continue
      }

      // The 'contractors' wrapper around the 'contractor' list may not exist.
      const award = contract['awarded_contract']
      let contractorList = undefined
      if (award.hasOwnProperty('contractors')) {
        contractorList = award['contractors']['contractor']
      } else {
        contractorList = award['contractor']
      }
      if (contractorList != undefined) {
        // TODO(Jack Poulson): Remove this after finishing reprocessing of the
        // EU procurement data.
        if (!Array.isArray(contractorList)) {
          contractorList = [contractorList]
        }
        for (const contractor of contractorList) {
          if (!contractor.hasOwnProperty('address')) {
            continue
          }
          const address = contractor['address']
          if (!address.hasOwnProperty('official_name')) {
            continue
          }
          const name = address['official_name']
          if (name && !(name.toLowerCase() in namesSet)) {
            namesList.push(name)
            namesSet.add(name.toLowerCase())
          }
        }
      }
    }
  }
  return namesList
}

function getDefenceOperators(filing: Filing): string[] {
  let namesList = []
  let namesSet = new Set()
  for (const [key, value] of Object.entries(filing.data)) {
    const form = value['form']
    if (!form || !form.hasOwnProperty('fd_contract_award_defence')) {
      continue
    }
    const defence = form['fd_contract_award_defence']
    if (!defence.hasOwnProperty('contract_award')) {
      continue
    }
    for (const contract of defence['contract_award']) {
      if (!contract.hasOwnProperty('operator')) {
        continue
      }
      const operator = contract['operator']
      if (operator.hasOwnProperty('name') && operator['name']) {
        const name = operator['name']
        if (!(name.toLowerCase() in namesSet)) {
          namesList.push(name)
          namesSet.add(name.toLowerCase())
        }
      }
    }
  }
  return namesList
}

export function getContractorsAndOperators(filing: Filing): string[] {
  const contractors = getContractors(filing)
  const operators = getDefenceOperators(filing)
  return [...new Set([].concat(contractors, operators))]
}

function annotationFromName(filing: Filing, name: string): any {
  return name ? filing.annotation.entities[name.toLowerCase()] : undefined
}

function logoEntryFromAnnotation(annotation: any): any {
  let logoEntry = undefined
  if (annotation) {
    if (annotation.logo) {
      logoEntry = (
        <a href={util.getEntityURL(annotation.text)}>
          <img
            className="ti-association-list-entity-logo"
            src={annotation.logo}
          />
        </a>
      )
    } else {
      logoEntry = annotation.stylizedText.toUpperCase()
    }
  }
  return logoEntry
}

function renderShortSummary(filing: Filing): any {
  const label = 'shortSummary'
  const title = 'Short Summary'

  let features = []

  for (const value of getNoticeValues(filing)) {
    ui.includeTableKeyValue(
      features,
      `Value (${value['type']})`,
      `${util.numberWithCommas(value['amount'])} ${value['currency']}`
    )
  }

  ui.includeTableKeyValue(features, 'CPV', getCPV(filing))
  const noticeData = getNoticeData(filing)
  ui.includeTableKeyValue(features, 'Language', noticeData.language)
  // TODO(Jack Poulson):
  //   - ca_ce_nuts
  //   - performance_nuts
  //   - ref_notice->no_doc_ojs
  ui.includeTableKeyValue(
    features,
    'General URL',
    <a href={util.prefixURL(noticeData['ia_url_general'])}>
      {util.simplifyURL(noticeData['ia_url_general'])}
    </a>
  )
  ui.includeTableKeyValue(features, 'NO DOC OJS', noticeData.no_doc_ojs)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderBuyer(filing: Filing): any {
  const label = 'buyer'
  const title = 'Buyer'

  let features = []

  let annotation = undefined

  const authority = getBuyer(filing)
  if (!authority) {
    return undefined
  }

  const address = authority['address']
  if (address) {
    ui.includeTableKeyValue(features, 'Official Name', address['official_name'])
    if (address['official_name']) {
      const name = address['official_name']
      annotation = filing.annotation.entities[name.toLowerCase()]
    }

    const addressObj = {
      streetLines: [address['address']],
      city: address['city'],
      postalCode: address['postal_code'],
      country: address['country'],
      email: address['email'],
    }

    ui.includeTableKeyValue(features, 'Address', ui.simpleAddress(addressObj))

    const generalURL = util.prefixURL(address['general_url'])
    ui.includeTableKeyValue(
      features,
      'General URL',
      <a href={generalURL}>{util.simplifyURL(generalURL)}</a>,
      true,
      generalURL
    )
    ui.includeTableKeyValue(features, 'Contact Point', address['contact_point'])
    ui.includeTableKeyValue(features, 'National ID', address['national_id'])
    // TODO(Jack Poulson): address.nuts
  }
  ui.includeTableKeyValue(features, 'CA Type', authority['ca_type'])
  ui.includeTableKeyValue(features, 'CA Activity', authority['ca_activity'])
  ui.includeTableKeyValue(features, 'CE Type', authority['ce_type'])
  ui.includeTableKeyValue(features, 'CE Activity', authority['ce_activity'])

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderComplementaryInfo(filing: Filing): any {
  const label = 'complementaryInfo'
  const title = 'Complementary Info'

  let features = []

  const info = getComplementaryInfo(filing)
  if (!info) {
    return undefined
  }

  const componentTitles = {
    address_review_body: 'Review Body',
    address_review_info: 'Review Info',
    address_mediation_body: 'Mediation Body',
  }

  let annotation = undefined

  for (const [key, value] of Object.entries(componentTitles)) {
    if (!info[key]) {
      continue
    }
    const address = info[key]

    if (address['official_name']) {
      const name = address['official_name']
      const candidate = filing.annotation.entities[name.toLowerCase()]
      if (candidate != undefined) {
        if (annotation == undefined) {
          annotation = candidate
        } else if (
          annotation.logo == undefined &&
          candidate.logo != undefined
        ) {
          annotation = candidate
        }
      }
    }

    const addressObj = {
      streetLines: [address['official_name'], address.address],
      city: address.city,
      postalCode: address.postal_code,
      country: address.country,
      email: address.email,
      url: address.url,
    }

    ui.includeTableKeyValue(features, value, ui.simpleAddress(addressObj))
  }

  ui.includeTableKeyDate(
    features,
    'Dispatch Notice Date',
    info['dispatch_notice_date']
  )

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderContractAwards(filing: Filing, source: string): any {
  const awards = getContractAwards(filing)
  if (!Object.keys(awards).length) {
    return
  }

  const label = 'contractAwards'
  const title = 'Contract Awards'

  let features = []

  // Get the list of award titles.
  let awardTitles = []
  for (const [language, data] of Object.entries(awards)) {
    for (const [itemIndex, item] of data.entries()) {
      if (itemIndex == awardTitles.length) {
        awardTitles.push({})
      }
      if (item?.title) {
        awardTitles[itemIndex][language.toUpperCase()] =
          item['title'].join('; ')
      }
    }
  }

  // Create the lists of features of the award accordions.
  let awardFeaturesList = []
  let awardFeaturesTitles = []
  for (const [language, data] of Object.entries(awards)) {
    for (const [itemIndex, item] of data.entries()) {
      if (itemIndex != awardFeaturesList.length) {
        continue
      }
      if (!item) {
        continue
      }

      // Get the contract title, preferring English last, as it should be
      // a machine translation.
      const awardTitle = awardTitles[itemIndex]
      let awardTitleStr = ''
      for (const [titleKey, titleValue] of Object.entries(awardTitle)) {
        if (titleValue && titleKey != 'EN') {
          // The type of titleValue cannot be inferred.
          awardTitleStr = titleValue as string
        }
      }
      if (awardTitle.hasOwnProperty('EN') && !awardTitleStr) {
        awardTitleStr = awardTitle['EN']
      }

      // Get the list of contractor names for this award
      let contractorSet = new Set()
      const contract = item['awarded_contract']
      let contractorList = undefined
      if (contract) {
        if (contract['contractors']) {
          contractorList = contract['contractors']['contractor']
        } else {
          contractorList = contract['contractor']
        }
      }
      if (contractorList != undefined) {
        // TODO(Jack Poulson): Remove this after finishing reprocessing of the
        // EU procurement data.
        if (!Array.isArray(contractorList)) {
          contractorList = [contractorList]
        }
        for (const [cIndex, c] of contractorList.entries()) {
          const cAddress = c['address']
          if (cAddress?.['official_name']) {
            contractorSet.add(cAddress['official_name'])
          }
        }
      }

      const contractorNames = Array.from(contractorSet)
      const contractorsStr = contractorNames.join('; ')
      let awardFeaturesTitle = util.coerce(
        awardTitleStr,
        `Contract ${itemIndex}`
      )
      if (contractorsStr) {
        awardFeaturesTitle += ` [${contractorsStr}]`
      }
      awardFeaturesList.push([])
      awardFeaturesTitles.push(awardFeaturesTitle)
    }
  }

  // Add the titles first.
  for (const [language, data] of Object.entries(awards)) {
    for (const [itemIndex, item] of data.entries()) {
      let awardFeatures = awardFeaturesList[itemIndex]
      if (item?.title) {
        ui.includeTableKeyValue(
          awardFeatures,
          `Title [${language.toUpperCase()}]`,
          item.title.join('; ')
        )
      }
    }
  }

  // Add the remaining features.
  for (const [language, data] of Object.entries(awards)) {
    for (const [itemIndex, item] of data.entries()) {
      if (!item) {
        continue
      }
      let awardFeatures = awardFeaturesList[itemIndex]

      ui.includeTableKeyValue(awardFeatures, 'Lot Number', item.lot_number)
      ui.includeTableKeyValue(awardFeatures, 'Item Number', item.item_number)
      ui.includeTableKeyValue(
        awardFeatures,
        'Contract Number',
        item.contract_number
      )

      const contract = item.awarded_contract
      if (contract) {
        if (contract.hasOwnProperty('values')) {
          const values = contract['values']
          for (const [typeKey, typeTitle] of Object.entries(kValueTypes)) {
            const component = values[typeKey]
            if (!component) {
              continue
            }
            ui.includeTableKeyValue(
              awardFeatures,
              typeTitle,
              `${component['currency']} ${util.numberWithCommas(
                component['amount']
              )}`
            )
          }
          for (const [typeKey, typeTitle] of Object.entries(kRangeTypes)) {
            const component = values[typeKey]
            if (!component) {
              continue
            }
            ui.includeTableKeyValue(
              awardFeatures,
              `Lower ${typeTitle}`,
              `${component['LOW']} ${component['@CURRENCY']}`
            )
            ui.includeTableKeyValue(
              awardFeatures,
              `Higher ${typeTitle}`,
              `${component['HIGH']} ${component['@CURRENCY']}`
            )
          }
        }
        const tenders = contract['tenders']
        if (contract['tenders']) {
          ui.includeTableKeyValue(
            awardFeatures,
            'Num Tenders',
            tenders['num_tenders']
          )
          ui.includeTableKeyValue(
            awardFeatures,
            'Num Tenders SME',
            tenders['num_tenders_sme']
          )
        }

        let contractorList = undefined
        if (contract.contractors) {
          contractorList = contract['contractors']['contractor']
        } else {
          contractorList = contract['contractor']
        }
        if (contractorList) {
          // TODO(Jack Poulson): Remove this after finishing reprocessing of the
          // EU procurement data.
          if (!Array.isArray(contractorList)) {
            contractorList = [contractorList]
          }
          for (const [cIndex, c] of contractorList.entries()) {
            // Get the contractor name -- if it exists.
            const cAddress = c['address']
            const cName = util.coerce(cAddress?.['official_name'], '')

            let cTitle = `Contractor ${cIndex + 1}`
            const cLabel = `contractor-${itemIndex}-${cIndex + 1}`
            let cFeatures = []

            const annotation = annotationFromName(filing, cName)
            const logoEntry = logoEntryFromAnnotation(annotation)

            const addressObj = cAddress
              ? {
                  streetLines: [cName, cAddress['address']],
                  city: cAddress['city'],
                  postalCode: cAddress['postal_code'],
                  country: cAddress['country'],
                }
              : { streetLines: [cName] }

            const cBody = [
              ui.simpleAddress(addressObj),
              cAddress?.national_id
                ? 'National ID: ' + cAddress.national_id
                : undefined,
            ]
            const cContainer = renderBodyAndLogo(cBody, logoEntry)
            ui.includeTableKeyValue(awardFeatures, cTitle, cContainer)
          }
          if (contract.contractors) {
            ui.includeTableKeyValue(
              awardFeatures,
              'Conclusion Date',
              contract.contractors['conclusion_date']
            )
          }
        }
      }
    }
  }

  const item = (
    <div>
      <table className="table ti-key-value-table">
        <tbody>{features}</tbody>
      </table>
      {
        <Accordion
          id={`${source}-contract-award-accordion`}
          items={awardFeaturesList.map(function (awardFeatures, index) {
            const awardLabel = `contractAward-${index}`
            const awardTitle = awardFeaturesTitles[index]
            return (
              <SimpleAccordionItem
                tableClassName="table ti-key-value-table"
                key={awardTitle}
                label={awardLabel}
                title={awardTitle}
                features={awardFeatures}
              />
            )
          })}
        />
      }
    </div>
  )

  return <AccordionItem title={title} key={title} body={item} />
}

function renderDefenceContractAwards(filing: Filing, source: string): any {
  const awards = getDefenceContractAwards(filing)
  if (!Object.keys(awards).length) {
    return
  }

  const label = 'defenceContractAwards'
  const title = 'Defence Contract Awards'

  let features = []

  // Get the list of award titles.
  let awardTitles = []
  for (const [language, data] of Object.entries(awards)) {
    for (const [itemIndex, item] of data.entries()) {
      if (itemIndex == awardTitles.length) {
        awardTitles.push({})
      }
      if (item.title) {
        awardTitles[itemIndex][language.toUpperCase()] =
          item['title'].join('; ')
      }
    }
  }

  // Create the lists of features of the award accordions.
  let awardFeaturesList = []
  let awardFeaturesTitles = []
  for (const [language, data] of Object.entries(awards)) {
    for (const [itemIndex, item] of data.entries()) {
      if (itemIndex == awardFeaturesList.length) {
        // Get the contract title, preferring English last.
        const awardTitle = awardTitles[itemIndex]
        let awardTitleStr = ''
        for (const [titleKey, titleValue] of Object.entries(awardTitle)) {
          if (titleValue && titleKey != 'EN') {
            // The type of titleValue cannot be inferred.
            awardTitleStr = titleValue as string
          }
        }
        if (awardTitle.hasOwnProperty('EN') && !awardTitleStr) {
          awardTitleStr = awardTitle['EN']
        }

        let awardFeaturesTitle = util.coerce(
          awardTitleStr,
          `Defence Award ${itemIndex}`
        )
        awardFeaturesList.push([])
        awardFeaturesTitles.push(awardFeaturesTitle)
      }
    }
  }

  // Add the titles first.
  for (const [language, data] of Object.entries(awards)) {
    for (const [itemIndex, item] of data.entries()) {
      let awardFeatures = awardFeaturesList[itemIndex]
      if (item.title) {
        ui.includeTableKeyValue(
          awardFeatures,
          `Title [${language.toUpperCase()}]`,
          item.title.join('; ')
        )
      }
    }
  }

  // Add the remaining features.
  for (const [language, data] of Object.entries(awards)) {
    for (const [itemIndex, item] of data.entries()) {
      let awardFeatures = awardFeaturesList[itemIndex]

      ui.includeTableKeyValue(
        awardFeatures,
        'Contract Number',
        item.contract_number
      )
      ui.includeTableKeyDate(awardFeatures, 'Date', item.date)
      if (item.hasOwnProperty('value')) {
        const v = item['value']
        for (const [typeKey, typeTitle] of Object.entries(kValueTypes)) {
          if (v.hasOwnProperty(typeKey)) {
            const valueComponent = v[typeKey]
            ui.includeTableKeyValue(
              awardFeatures,
              typeTitle,
              `${valueComponent.amount} ${valueComponent.currency}`
            )
          }
        }
        for (const [typeKey, typeTitle] of Object.entries(kRangeTypes)) {
          if (v.hasOwnProperty(typeKey)) {
            const valueComponent = v[typeKey]
            ui.includeTableKeyValue(
              awardFeatures,
              `Lower ${typeTitle}`,
              `${valueComponent['LOW']} ${valueComponent['@CURRENCY']}`
            )
            ui.includeTableKeyValue(
              awardFeatures,
              `Higher ${typeTitle}`,
              `${valueComponent['HIGH']} ${valueComponent['@CURRENCY']}`
            )
          }
        }
      }

      if (item.hasOwnProperty('operator')) {
        const operator = item['operator']

        const annotation = annotationFromName(filing, operator['name'])
        const logoEntry = logoEntryFromAnnotation(annotation)

        const addressObj = {
          streetLines: [operator['address']],
          city: operator['town'],
          postalCode: operator['postal_code'],
          country: operator['country'],
        }

        const body = (
          <div className="container px-0">
            <div className="row flex-md-row-reverse align-items-center mx-0 px-0 py-3">
              <div className="col-12 col-md-4 align-items-center px-3 bg-light">
                <div className="text-center">{logoEntry}</div>
              </div>
              <div className="col-12 col-md-8">
                <table className="table">
                  <tbody>
                    <ui.Address address={addressObj} />
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        )

        ui.includeTableKeyValue(awardFeatures, 'Operator', body)
      }
    }
  }

  const item = (
    <div>
      <table className="table ti-key-value-table">
        <tbody>{features}</tbody>
      </table>
      {
        <Accordion
          id={`${source}-defence-contract-award-accordion`}
          items={awardFeaturesList.map(function (awardFeatures, index) {
            const awardLabel = `defenceContractAward-${index}`
            const awardTitle = awardFeaturesTitles[index]
            return (
              <SimpleAccordionItem
                tableClassName="table ti-key-value-table"
                key={awardTitle}
                label={awardLabel}
                title={awardTitle}
                features={awardFeatures}
              />
            )
          })}
        />
      }
    </div>
  )

  return <AccordionItem title={title} key={title} body={item} />
}

function renderNoticeData(filing: Filing): any {
  const label = 'noticeData'
  const title = 'Notice Data'

  let features = []

  ui.includeTableKeyValue(features, 'Title', getTranslationTitle(filing))
  ui.includeTableKeyValue(features, 'Town', getTown(filing))
  ui.includeTableKeyValue(features, 'Country', getTranslationCountry(filing))

  let annotation = undefined
  for (const [nameIndex, name] of getUniqueBuyers(filing).entries()) {
    const nameKey = name.toLowerCase()
    ui.includeTableKeyValue(
      features,
      `Contracting Authority ${nameIndex + 1}`,
      name
    )
    const candidate = filing.annotation.entities[nameKey]
    if (candidate != undefined) {
      if (annotation == undefined) {
        annotation = candidate
      } else if (annotation.logo == undefined && candidate.logo != undefined) {
        annotation = candidate
      }
    }
  }

  return (
    <AccordionItemWithAnnotation
      tableClassName="table ti-key-value-table"
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderObjectOfContract(filing: Filing, source: string): any {
  const label = 'objectOfContract'
  const title = 'Object of Contract'

  let features = []

  const objects = getContractObjects(filing)
  if (!Object.keys(objects).length) {
    return undefined
  }

  // Display the top-level titles.
  for (const [language, data] of Object.entries(objects)) {
    const titleStr = data.title ? data.title.join('; ') : ''
    ui.includeTableKeyValue(
      features,
      `Title [${language.toUpperCase()}]`,
      titleStr
    )
  }

  // Display the top-level short descriptions.
  for (const [language, data] of Object.entries(objects)) {
    const titleStr = data.title ? data.title.join('; ') : ''
    const shortDescrStr = data.short_descr ? data.short_descr.join('; ') : ''
    if (titleStr != shortDescrStr) {
      ui.includeTableKeyValue(
        features,
        `Short Description [${language.toUpperCase()}]`,
        shortDescrStr
      )
    }
  }

  // Display the remaining top-level features
  for (const [language, data] of Object.entries(objects)) {
    if (data.estimated_total) {
      ui.includeTableKeyValue(
        features,
        'Estimated Total',
        `${data.estimated_total.amount} ${data.estimated_total.currency}`
      )
    }
    ui.includeTableKeyValue(features, 'Reference Number', data.reference_number)
  }

  // Create the list of object description accordion items.
  let objectDescriptionFeatures = []
  for (const [language, data] of Object.entries(objects)) {
    if (data['object_descr']) {
      for (const [objDescIndex, objDesc] of data['object_descr'].entries()) {
        if (objDescIndex == objectDescriptionFeatures.length) {
          objectDescriptionFeatures.push([])
        }
      }
    }
  }

  // Display the object description titles.
  for (const [language, data] of Object.entries(objects)) {
    if (!data['object_descr']) {
      continue
    }
    for (const [objDescIndex, objDesc] of data['object_descr'].entries()) {
      let objDescFeatures = objectDescriptionFeatures[objDescIndex]
      const objDescTitle = objDesc.title ? objDesc.title.join('; ') : ''
      ui.includeTableKeyValue(
        objDescFeatures,
        `Title [${language.toUpperCase()}]`,
        objDescTitle
      )
    }
  }

  // Display the object description short descriptions.
  for (const [language, data] of Object.entries(objects)) {
    if (!data.object_descr) {
      continue
    }
    for (const [objDescIndex, objDesc] of data['object_descr'].entries()) {
      let objDescFeatures = objectDescriptionFeatures[objDescIndex]
      const objDescTitle = objDesc.title ? objDesc.title.join('; ') : ''
      const objDescShortDesc = objDesc['short_descr']
        ? objDesc['short_descr'].join('; ')
        : ''
      if (objDescShortDesc != objDescTitle) {
        ui.includeTableKeyValue(
          objDescFeatures,
          `Short Description [${language.toUpperCase()}]`,
          objDescShortDesc
        )
      }
    }
  }

  // Display the remaining object description features
  for (const [language, data] of Object.entries(objects)) {
    if (!data['object_descr']) {
      continue
    }
    for (const [objDescIndex, objDesc] of data['object_descr'].entries()) {
      let objDescFeatures = objectDescriptionFeatures[objDescIndex]
      ui.includeTableKeyValue(
        objDescFeatures,
        'Lot Number',
        objDesc['lot_number']
      )
      ui.includeTableKeyValue(
        objDescFeatures,
        'Item Number',
        objDesc['item_number']
      )
      if (objDesc['main_site']) {
        ui.includeTableKeyValue(
          objDescFeatures,
          'Main Site',
          objDesc.main_site.join('; ')
        )
      }
      if (objDesc['eu_progr_related'] && objDesc['eu_progr_related']) {
        const related = objDesc['eu_progr_related']
        ui.includeTableKeyValue(
          objDescFeatures,
          'Related EU Programs',
          Array.isArray(related) ? related.join('; ') : related
        )
      }
      // TODO(Jack Poulson): cpv_additional[i].cpv_code
      // TODO(Jack Poulson): nuts
    }
  }

  const item = (
    <div>
      <table className="table ti-key-value-table">
        <tbody>{features}</tbody>
      </table>
      {
        <Accordion
          id={`${source}-object-description-accordion`}
          items={objectDescriptionFeatures.map(function (
            objDescFeatures,
            index
          ) {
            const objDescLabel = `objectDescription-${index}`
            const objDescTitle = `Object Description ${index}`
            return (
              <SimpleAccordionItem
                tableClassName="table ti-key-value-table"
                key={objDescTitle}
                label={objDescLabel}
                title={objDescTitle}
                features={objDescFeatures}
              />
            )
          })}
        />
      }
    </div>
  )

  return <AccordionItem title={title} key={title} body={item} />
}

export function renderAccordionItems(filing, source: string): any[] {
  let accordionItems = []
  accordionItems.push(renderShortSummary(filing))
  accordionItems.push(renderNoticeData(filing))
  accordionItems.push(renderBuyer(filing))
  accordionItems.push(renderObjectOfContract(filing, source))
  accordionItems.push(renderContractAwards(filing, source))
  accordionItems.push(renderDefenceContractAwards(filing, source))
  accordionItems.push(renderComplementaryInfo(filing))
  return accordionItems
}
