import * as React from 'react'

type Dict = { [key: string]: any }
type Filing = Dict

export function parseNumber(value: string): number {
  if (!value) {
    return undefined
  }
  value = value.replaceAll(' ', '')
  return parseFloat(value)
}

export function getURI(filing: Filing): string {
  // TODO(Jack Poulson): Remove the double-quote replacement after it the
  // database is updated to no longer contain such spurious characters.
  const doc_id = filing.doc_id.replaceAll('"', '')
  return `https://ted.europa.eu/en/notice/-/detail/${doc_id}`
}

export function serializeLanguageDict(dict) {
  if (!dict) {
    return ''
  }
  if (Object.entries(dict).length == 1) {
    return Object.entries(dict)[0][1]
  }
  let lines = []
  for (const [language, data] of Object.entries(dict)) {
    lines.push(`${language.toUpperCase()}: "${data}"`)
  }
  return lines.join(', ')
}
