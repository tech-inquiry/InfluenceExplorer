import * as React from 'react'

import { Accordion } from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as util from '../../utilities/util'

import * as legacy from './legacy'
import * as modern from './modern'
import * as euUtil from './util'

type Dict = { [key: string]: any }

function getTitle(filing: Dict): string {
  const maxTitleLength = 200
  const docID = filing.doc_id
  const titles = filing.is_legacy
    ? legacy.getTitles(filing)
    : modern.getTitles(filing)
  const title = util.coerce(titles.contractTitle, titles.cpv)
  return `[${docID}] ${util.trimLabel(title, maxTitleLength)}`
}

export class Filing extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint(`/api/eu/${this.props.countryCode}/procurement`)
    super.setUniqueKeys(['doc_id'])
  }

  render() {
    const filing = this.props.filing
    const title = getTitle(filing)

    const uri = euUtil.getURI(filing)
    const header = uri ? (
      <p className="card-text">
        <a className="btn btn-light" href={uri} role="button">
          View Original
        </a>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    ) : undefined

    let accordionItems = []

    const source = `eu-${this.props.countryCode}`
    if (filing.is_legacy) {
      accordionItems = legacy.renderAccordionItems(filing, source)
    } else {
      accordionItems = modern.renderAccordionItems(filing)
    }

    let accordion = (
      <Accordion
        id={`${source}-procurement-accordion`}
        items={accordionItems}
      />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}
