import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

export class Award extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/us/ca/procurement')
    super.setUniqueKeys(['department', 'document_id'])
  }

  render() {
    const filing = this.state.filing

    function renderVendor() {
      const label = 'vendor'
      const title = 'Vendor'

      let features = []

      const annotation = filing.annotation.vendor

      ui.includeTableKeyValue(
        features,
        'Name',
        util.coerceUpperCase(annotation.origText)
      )
      ui.includeTableKeyValue(features, 'Vendor ID', filing.supplier_id)

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderAgency() {
      const label = 'agency'
      const title = 'Agency'

      let features = []

      const annotation = filing.annotation.agency

      ui.includeTableKeyValue(features, 'Agency', annotation.origText)

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderActionDetails() {
      const label = 'contract-action-details'
      const title = 'Contract Action Details'

      let features = []

      ui.includeTableKeyValue(features, 'Document ID', filing.document_id)
      ui.includeTableKeyValue(features, 'Status', filing.status)
      ui.includeTableKeyValue(
        features,
        'Acquisition Type',
        filing.acquisition_type
      )
      ui.includeTableKeyValue(
        features,
        'Acquisition Method',
        filing.acquisition_method
      )
      ui.includeTableKeyValue(features, 'Buyer Name', filing.buyer_name)
      ui.includeTableKeyValue(features, 'Buyer Email', filing.buyer_email)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderAmounts() {
      const label = 'amounts'
      const title = 'Amounts'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Total Amount',
        `${util.numberWithCommas(filing.grand_total)} USD`,
        true,
        filing.grand_total
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderDates() {
      const label = 'dates'
      const title = 'Dates'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Start Date',
        util.dateWithoutTime(new Date(filing.start_date)),
        true,
        filing.start_date
      )
      ui.includeTableKeyValue(
        features,
        'End Date',
        util.dateWithoutTime(new Date(filing.end_date)),
        true,
        filing.end_date
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderDescription() {
      const label = 'description'
      const title = 'Description'

      let features = []

      ui.includeTableKeyValue(
        features,
        'First Item Title',
        filing.first_item_title
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    let title = []
    if (filing.first_item_title) {
      title.push(<p className="ti-compact">{filing.first_item_title}</p>)
    }

    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light"
            href="https://suppliers.fiscal.ca.gov/psc/psfpd1/SUPPLIER/ERP/c/ZZ_PO.ZZ_SCPRS1_CMP.GBL"
            role="button"
          >
            Original Dataset
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderVendor())
    accordionItems.push(renderAgency())
    accordionItems.push(renderActionDetails())
    accordionItems.push(renderAmounts())
    accordionItems.push(renderDates())
    accordionItems.push(renderDescription())
    let accordion = (
      <Accordion id="au-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
