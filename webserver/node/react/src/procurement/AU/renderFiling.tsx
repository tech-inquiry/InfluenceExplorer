import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function abnURL(id) {
  return `https://abr.business.gov.au/ABN/View?id=${id}`
}

function renderExtraIDs(extraIDs) {
  if (!extraIDs) {
    return undefined
  }
  const scheme = extraIDs[0].scheme
  const id = extraIDs[0].id
  const label = `${scheme}: ${id}`
  return (
    <tr>
      <th scope="row" className="text-end">
        Extra ID
      </th>
      <td className="text-start">
        {extraIDs[0].scheme == 'AU-ABN' ? (
          <a href={abnURL(extraIDs[0].id)}>{label}</a>
        ) : (
          label
        )}
      </td>
    </tr>
  )
}

function renderContractActionDetails(filing) {
  const label = 'contract-action-details'
  const title = 'Contract Action Details'

  let features = []
  if (filing.award) {
    ui.includeTableKeyValue(
      features,
      'Award ID',
      <code className="text-dark">{filing.award.id}</code>
    )
    ui.includeTableKeyValue(features, 'Award Status', filing.award.status)
  }
  if (filing.contract) {
    ui.includeTableKeyValue(
      features,
      'Contract ID',
      <code className="text-dark">{filing.contract.id}</code>
    )
    ui.includeTableKeyValue(features, 'Contract Status', filing.contract.status)
    ui.includeTableKeyValue(
      features,
      'Contract Procurement Method',
      filing.contract.procurementMethod
    )
    ui.includeTableKeyValue(
      features,
      'Contract Procurement Method Details',
      filing.contract.procurementMethodDetails
    )
  }
  if (filing.tender) {
    ui.includeTableKeyValue(
      features,
      'Tender ID',
      <code className="text-dark">{filing.tender.id}</code>
    )
    ui.includeTableKeyValue(features, 'Tender Status', filing.tender.status)
    ui.includeTableKeyValue(
      features,
      'Tender Procurement Method',
      filing.tender.procurementMethod
    )
    ui.includeTableKeyValue(
      features,
      'Tender Procurement Method Details',
      filing.tender.procurementMethodDetails
    )
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderContractAmounts(filing) {
  const label = 'contract-amounts'
  const title = 'Contract Amounts'

  let features = []

  function includeAmountWithCurrency(k, v) {
    if (v) {
      features.push(
        <ui.TableKeyValue
          key={features.length}
          k={k}
          v={util.amountWithCurrency(v.amount, v.currency)}
        />
      )
    }
  }

  if (filing.award) {
    includeAmountWithCurrency('Award Value', filing.award.value)
  }
  if (filing.contract) {
    includeAmountWithCurrency(
      'Contract Minimum Value',
      filing.contract.minValue
    )
    includeAmountWithCurrency('Contract Value', filing.contract.value)
    if (filing.contract.amendments) {
      for (const amendment of filing.contract.amendments) {
        if (amendment.additionalAmendmentDetails) {
          for (const detail of amendment.additionalAmendmentDetails) {
            ui.includeTableKeyValue(features, detail.name, detail.value)
          }
        }
      }
    }
  }
  if (filing.tender) {
    includeAmountWithCurrency('Tender Minimum Value', filing.tender.minValue)
    includeAmountWithCurrency('Tender Value', filing.tender.value)
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderContractDates(filing) {
  const label = 'contract-dates'
  const title = 'Contract Dates'

  function periodString(period) {
    if (!period) {
      return ''
    }
    let str = ''
    if (period.startDate) {
      str = util.dateWithoutTime(new Date(period.startDate))
    }
    str += ' -- '
    if (period.endDate) {
      str += util.dateWithoutTime(new Date(period.endDate))
    }
    return str
  }

  function milestoneKey(index) {
    return `Milestone ${index + 1}`
  }

  function milestoneValue(item) {
    return (
      <div>
        {item.description ? (
          <p className="ti-compact">{item.description}</p>
        ) : undefined}
        {item.dueDate
          ? util.dateWithoutTime(new Date(item.dueDate))
          : undefined}
      </div>
    )
  }

  let features = []
  ui.includeTableKeyDate(features, 'Date Published', filing.published_date)
  ui.includeTableKeyDate(features, 'Date', filing.date)
  if (filing.award) {
    ui.includeTableKeyDate(features, 'Award Date', filing.award.date)
    ui.includeTableKeyValue(
      features,
      'Award Period',
      periodString(filing.award.period),
      true,
      filing.award.period
    )
  }
  if (filing.tender) {
    ui.includeTableKeyValue(
      features,
      'Tender Period',
      periodString(filing.tender.period),
      true,
      filing.tender.period
    )
    if (filing.tender.milestones) {
      for (const [i, milestone] of filing.tender.milestones.entries()) {
        ui.includeTableKeyValue(
          features,
          milestoneKey(i),
          milestoneValue(milestone)
        )
      }
    }
  }
  if (filing.contract) {
    ui.includeTableKeyValue(
      features,
      'Contract Period',
      periodString(filing.contract.period),
      true,
      filing.contract.period
    )
    if (filing.contract.milestones) {
      for (const [i, milestone] of filing.contract.milestones.entries()) {
        ui.includeTableKeyValue(
          features,
          milestoneKey(i),
          milestoneValue(milestone)
        )
      }
    }
    if (filing.contract.amendments) {
      for (const [i, amendment] of filing.contract.amendments.entries()) {
        ui.includeTableKeyDate(features, `Amendment ${i} Date`, amendment.date)
      }
    }
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderContractDescription(filing) {
  const label = 'contract-description'
  const title = 'Contract Description'

  let features = []

  function includeItemClassification(item, index) {
    if (!('classification' in item)) {
      return
    }
    const classification = item.classification
    ui.includeTableKeyValue(
      features,
      `Item ${index + 1}`,
      <div>
        {'scheme' in classification ? (
          <p className="ti-compact">Scheme: {classification.scheme}</p>
        ) : undefined}
        {'id' in classification ? (
          <p className="ti-compact">{classification.id}</p>
        ) : undefined}
        {'description' in classification ? (
          <p className="ti-compact">
            Description:{' '}
            {'uri' in classification ? (
              <a href={classification.uri}>{classification.description}</a>
            ) : (
              classification.description
            )}
          </p>
        ) : undefined}
      </div>
    )
  }

  if (filing.tender) {
    ui.includeTableKeyValue(features, 'Tender Title', filing.tender.title)
    ui.includeTableKeyValue(
      features,
      'Tender Description',
      filing.tender.description
    )
    if (filing.tender.items) {
      filing.tender.items.map(includeItemClassification)
    }
  }
  if (filing.contract) {
    ui.includeTableKeyValue(features, 'Contract Title', filing.contract.title)
    ui.includeTableKeyValue(
      features,
      'Contract Description',
      filing.contract.description
    )
    if (filing.contract.items) {
      filing.contract.items.map(includeItemClassification)
    }
    if (filing.contract.amendments) {
      for (const [i, item] of filing.contract.amendments.entries()) {
        ui.includeTableKeyValue(
          features,
          `Amendment ${i + 1} Rationale`,
          item.rationale
        )
      }
    }
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderSupplier(filing) {
  const label = 'supplier'

  if (!filing.parties) {
    return undefined
  }

  let supplierIndex = undefined
  for (const [i, party] of filing.parties.entries()) {
    if (party.role == 'supplier') {
      supplierIndex = i
      break
    }
  }
  if (supplierIndex == undefined) {
    return undefined
  }

  const supplier = filing.parties[supplierIndex]
  const annotation = filing.annotation.vendors[supplierIndex]
  const title = `Supplier: ${util.coerceUpperCase(annotation.origText)}`

  const features = [
    <ui.TableKeyValue
      k="Supplier Name"
      v={util.coerceUpperCase(annotation.origText)}
    />,
    renderExtraIDs(supplier.additionalIdentifiers),
    renderAddress(supplier.address),
    renderContactPoint(supplier.contactPoint),
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderAdditionalIdentifiers(identifiers) {
  if (!identifiers) {
    return undefined
  }
  const scheme = identifiers[0].scheme
  const id = identifiers[0].id
  if (scheme == 'AU-ABN') {
    return (
      <ui.TableKeyValue
        k="Extra ID"
        v={
          <a href={abnURL(id)}>
            {scheme}: {id}
          </a>
        }
      />
    )
  } else {
    return <ui.TableKeyValue k="Extra ID" v={`${scheme}: ${id}`} />
  }
}

function renderAddress(address) {
  if (!address) {
    return undefined
  }
  return (
    <ui.Address
      address={{
        streetLines: [address.streetAddress],
        city: address.locality,
        postalCode: address.postalCode,
        country: address.countryName,
      }}
    />
  )
}

function renderContactPoint(contact) {
  if (!contact) {
    return undefined
  }
  const item = (
    <div>
      {contact.name ? <p className="ti-compact">{contact.name}</p> : undefined}
      {contact.email ? (
        <p className="ti-compact">
          Email: <a href="mailto:{contact.email}">{contact.email}</a>
        </p>
      ) : undefined}
      {contact.telephone ? (
        <p className="ti-compact">
          Telephone: {util.formatPhoneNumber(contact.telephone)}
        </p>
      ) : undefined}
      {contact.branch ? (
        <p className="ti-compact">Branch: {contact.branch}</p>
      ) : undefined}
      {contact.division ? (
        <p className="ti-compact">Division: {contact.division}</p>
      ) : undefined}
      {contact.uri ? (
        <p className="ti-compact">
          <a href={contact.uri}>contact.uri</a>
        </p>
      ) : undefined}
    </div>
  )
  return <ui.TableKeyValue k="Contact Point" v={item} />
}

function renderRole(role) {
  if (!role) {
    return undefined
  }
  return <ui.TableKeyValue k="Role" v={role} />
}

function renderParty(party, annotation, title) {
  const item = (
    <table className="table">
      <tbody>
        {ui.renderEntityAnnotation(annotation)}
        {renderAdditionalIdentifiers(party.additionalIdentifiers)}
        {renderAddress(party.address)}
        {renderContactPoint(party.contactPoint)}
        {renderRole(party.role)}
      </tbody>
    </table>
  )
  return <ui.TableKeyValue k={title} v={item} key={title} />
}

function renderSuppliers(filing) {
  const label = 'suppliers'
  const title = 'Suppliers'

  let features = []
  if (!filing.award || !filing.award.suppliers) {
    return undefined
  }
  for (const [i, party] of filing.award.suppliers.entries()) {
    let vendorIndex = -1
    for (const [j, vendor] of filing.annotation.vendors.entries()) {
      if (party.name.toLowerCase() == vendor.origText.toLowerCase()) {
        vendorIndex = j
      }
    }
    if (vendorIndex >= 0) {
      features.push(
        renderParty(
          party,
          filing.annotation.vendors[vendorIndex],
          `Supplier ${i + 1}`
        )
      )
    } else {
      features.push(<ui.TableKeyValue k={`Supplier ${i + 1}`} v={party.name} />)
    }
  }
  if (features.length <= 1) {
    return undefined
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderProcuringEntity(filing) {
  const label = 'procuring-entity'

  if (!filing.parties) {
    return undefined
  }

  let agencyIndex = undefined
  for (const [i, party] of filing.parties.entries()) {
    if (party.role == 'procuringEntity') {
      agencyIndex = i
      break
    }
  }
  if (agencyIndex == undefined) {
    return undefined
  }

  const agency = filing.parties[agencyIndex]
  const annotation = filing.annotation.vendors[agencyIndex]
  const title = `Procuring Entity: ${util.coerceUpperCase(annotation.origText)}`

  const features = [
    <ui.TableKeyValue
      k="Procuring Entity"
      v={util.coerceUpperCase(annotation.origText)}
    />,
    renderExtraIDs(agency.additionalIdentifiers),
    renderAddress(agency.address),
    renderContactPoint(agency.contactPoint),
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

export class Award extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/au/procurement')
    super.setUniqueKeys(['filing_id'])
  }

  render() {
    const filing = this.state.filing
    const maxTitleLength = 200

    let title = ''
    if (filing.contract) {
      title = util.trimLabel(
        `${filing.contract.description} (${filing.contract.title})`,
        maxTitleLength
      )
    }
    let header = undefined
    if (filing.filing_id) {
      function documentURLFromID(id) {
        const idKept = id.split('-')[1]
        const splitID =
          `${idKept.substr(0, 8)}-${idKept.substr(8, 4)}-` +
          `${idKept.substr(12, 4)}-${idKept.substr(16, 4)}-` +
          idKept.substr(20, 12)
        return `https://www.tenders.gov.au/Cn/Show/${splitID}`
      }

      function jsonURLFromID(id) {
        const idKept = id.split('-')[0]
        return `https://api.tenders.gov.au/ocds/findById/${idKept}`
      }

      const id = filing.award
        ? filing.award.id
        : filing.contract
        ? filing.contract.awardID
        : ''

      const documentURL = documentURLFromID(id)
      const jsonURL = jsonURLFromID(id)
      header = (
        <p className="card-text">
          <div className="mb-2">
            <a className="btn btn-light me-2" href={documentURL} role="button">
              View Original
            </a>
            <a className="btn btn-light" href={jsonURL} role="button">
              View JSON
            </a>
          </div>
          {super.getTagDisplay()}
          {super.getTagForm()}
        </p>
      )
    }

    let accordionItems = []
    accordionItems.push(renderSupplier(filing))
    accordionItems.push(renderProcuringEntity(filing))
    accordionItems.push(renderSuppliers(filing))
    accordionItems.push(renderContractActionDetails(filing))
    accordionItems.push(renderContractDates(filing))
    accordionItems.push(renderContractAmounts(filing))
    accordionItems.push(renderContractDescription(filing))
    let accordion = (
      <Accordion id="au-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
