import * as React from 'react'

import { Accordion, AccordionItem, SimpleAccordionItem } from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

export class Award extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = ['product_code', 'supplier', 'contract']
    super(props)
    super.setEndpoint('/api/us/az/procurement')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing

    function renderVendor() {
      const label = 'vendor'
      const title = 'Vendor'

      let features = []

      const annotation = filing.annotation.vendor
      features.push(ui.renderEntityAnnotation(annotation))

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderActionDetails() {
      const label = 'contract-action-details'
      const title = 'Contract Action Details'

      let features = []

      ui.includeTableKeyValue(features, 'Contract ID', filing.contract)
      ui.includeTableKeyValue(features, 'Unit', filing.unit)
      ui.includeTableKeyValue(features, 'Product Code', filing.product_code)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderAmounts() {
      const label = 'amounts'
      const title = 'Amounts'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Total Amount',
        `${util.numberWithCommas(filing.negotiated_price)} ${filing.currency}`,
        true,
        filing.negotiated_price
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderDates() {
      const label = 'dates'
      const title = 'Dates'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Start Date',
        util.dateWithoutTime(new Date(filing.start_date)),
        true,
        filing.start_date
      )
      ui.includeTableKeyValue(
        features,
        'End Date',
        util.dateWithoutTime(new Date(filing.end_date)),
        true,
        filing.end_date
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderDescription() {
      const label = 'description'
      const title = 'Description'

      let features = []

      ui.includeTableKeyValue(features, 'Item Label', filing.item_label)
      ui.includeTableKeyValue(features, 'Commodity', filing.commodity)
      ui.includeTableKeyValue(features, 'Summary', filing.summary)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    let title = []
    if (filing.item_label) {
      title.push(<p className="ti-compact">{filing.item_label}</p>)
    } else if (filing.commodity) {
      title.push(<p className="ti-compact">{filing.commodity}</p>)
    }

    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light"
            href="https://app.az.gov/page.aspx/en/pdt/item_browse_public"
            role="button"
          >
            Original Dataset
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderVendor())
    accordionItems.push(renderActionDetails())
    accordionItems.push(renderAmounts())
    accordionItems.push(renderDates())
    accordionItems.push(renderDescription())
    let accordion = (
      <Accordion id="au-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
