import * as React from 'react'

import { FilingFeed } from '../FilingFeed'

import { appConfig } from '../utilities/constants'
import { kProcurementPaths } from '../utilities/api'
import { renderFiling } from './USDODAnnounce/renderFiling'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

export class USDODAnnounce extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  title: string
  columns: any[]

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL + '/' + kProcurementPaths['US DOD announcements'].get

    this.tableID = 'ti-table-us-dod-announce'
    this.title = 'US Defense Dept. Procurement Announcements'

    this.closeModal = this.closeModal.bind(this)
    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label ? this.props.label : 'us-dod-announce'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Date' },
      { title: 'ID' },
      { title: 'Text' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex, haveSubawards) {
    const maxTextLength = 200
    const date = util.dateWithoutTimeLex(filing.date)
    const articleID = filing.article_id
    const text = util.trimLabel(filing.text, maxTextLength)
    const entireFiling = JSON.stringify(filing)

    return [filingIndex, date, articleID, text, entireFiling]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  closeModal() {
    // Grab the modal created by the FilingFeed and then click.
    document.getElementById(ui.modalCloseButtonId(this.label)).click()
  }

  render() {
    return (
      <FilingFeed
        tableName="us_dod_announcements"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
