import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USAZ/renderFiling'

import * as util from '../utilities/util'

export class USAZProcurement extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths['US AZ'].get

    this.tableID = 'ti-table-us-az-procurement'
    this.title = (
      <span>
        <img src="/logos/state_of_arizona.svg" className="dropdown-flag" />
        Arizona (US) Procurement Records
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label ? this.props.label : 'us-az-procurement'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Start Date' },
      { title: 'Vendor' },
      {
        title: 'Total Amount',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
      },
      { title: 'Item Label' },
      { title: 'Commodity' },
      { title: 'Contract ID' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex: number) {
    const startDate = filing.start_date
      ? util.dateWithoutTimeLex(filing.start_date)
      : ''
    const vendor = filing.supplier ? filing.supplier.toUpperCase() : ''
    const totalAmount =
      filing.negotiated_price !== undefined ? filing.negotiated_price : ''
    const itemLabel = filing.item_label ? filing.item_label : ''
    const commodity = filing.commodity ? filing.commodity : ''
    const contractID = filing.contract ? filing.contract : ''
    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      startDate,
      vendor,
      totalAmount,
      itemLabel,
      commodity,
      contractID,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
