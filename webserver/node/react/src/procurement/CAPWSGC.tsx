import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import * as util from '../utilities/util'

import { renderFiling } from './CAPWSGC/renderFiling'

export class CAPWSGCProcurement extends React.Component<any, any> {
  urlBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    const source = this.props.isAgency
      ? 'CA PWSGC (as agency)'
      : 'CA PWSGC (as vendor)'
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[source].get

    this.maxDescriptionLength = 256
    this.tableID = this.props.isAgency
      ? 'ti-table-ca-pwsgc-agency-procurement'
      : 'ti-table-ca-pwsgc-procurement'
    this.title = (
      <span>
        <img src="/logos/government_of_canada.svg" className="dropdown-flag" />
        Canadian PWSGC procurement records{' '}
        {this.props.isAgency ? '(as agency)' : '(as vendor)'}
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? 'ca-pwsgc-agency-procurement'
      : 'ca-pwsgc-procurement'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Award Date' },
      { title: 'Vendor' },
      {
        title: 'Total Contract Value',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, 'CA$'),
      },
      { title: 'End User' },
      { title: 'Description' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex: number) {
    const awardDate = filing.award_date
      ? util.dateWithoutTimeLex(filing.award_date)
      : ''
    const vendor = util.coerce(filing.supplier_legal_name, '')
    const totalContractValue = util.coerce(filing.total_contract_value, '')
    const endUser = util.coerce(filing.end_user_entity, '')
    const description = util.coerce(filing.gsin_description, '')
    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      awardDate,
      vendor,
      totalContractValue,
      endUser,
      description,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="ca_pwsgc_contract_filing"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
