import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import * as util from '../utilities/util'

import { renderExemptFiling, renderTender } from './IL/renderFiling'

function formatDate(date): string {
  return date ? util.dateWithoutTimeLex(date) : ''
}

function hasMoreThanAscii(str: string): boolean {
  if (!str) {
    return false
  }
  return str.split('').some(function (char) {
    return char.charCodeAt(0) > 127
  })
}

function formatTranslation(hebrewText, englishText) {
  if (hasMoreThanAscii(hebrewText)) {
    return hebrewText
      ? hebrewText == englishText
        ? hebrewText
        : `${hebrewText}, [EN machine translation]: ${englishText}`
      : ''
  } else {
    return hebrewText
  }
}

export class ILProcurementExempt extends React.Component<any, any> {
  urlBase: string
  maxTitleLength: number
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    const direction = this.props.isAgency ? '(as agency)' : '(as vendor)'
    const source = `IL Exempt ${direction}`
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[source].get

    this.maxTitleLength = 200
    this.maxDescriptionLength = 256
    this.tableID = this.props.isAgency
      ? 'ti-table-il-procurement-exempt-agency'
      : 'ti-table-il-procurement-exempt'
    this.title = (
      <span>
        <img src="/logos/government_of_israel.svg" className="dropdown-flag" />
        Israeli Exempt Contract Records {direction}
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? 'il-procurement-exempt-agency'
      : 'il-procurement-exempt'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Publication Date' },
      { title: 'Update Date' },
      { title: 'Title' },
      { title: 'Entities' },
      { title: 'Subjects' },
      {
        title: 'Amount',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, ''),
      },
      { title: 'Currency' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    const publicationDate = formatDate(filing.bid_publication_date)
    const modificationDate = formatDate(filing.bid_update_date)
    const endDate = formatDate(filing.bid_last_date)

    const title = formatTranslation(filing.title, filing.title_en)
    const entities = formatTranslation(
      filing.entities.join('; '),
      filing.entities_en.join('; ')
    )

    const subjects = formatTranslation(
      filing.subjects.join('; '),
      filing.subjects_en.join('; ')
    )

    // Sum up the engagement amounts.
    let amount = 0
    let currency = ''
    for (const engagement of filing.engagements) {
      if (engagement.amount) {
        const preppedAmount = engagement.amount.replaceAll(',', '')
        amount += parseFloat(preppedAmount)
      }
      if (engagement.currency) {
        currency = engagement.currency
      }
    }
    const amountStr = String(amount)

    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      publicationDate,
      modificationDate,
      title,
      entities,
      subjects,
      amountStr,
      currency,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderExemptFiling}
      />
    )
  }
}

export class ILProcurementTender extends React.Component<any, any> {
  urlBase: string
  maxTitleLength: number
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    const direction = this.props.isAgency ? '(as agency)' : '(as vendor)'
    const source = `IL Tender ${direction}`
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[source].get

    this.maxTitleLength = 200
    this.maxDescriptionLength = 256
    this.tableID = this.props.isAgency
      ? 'ti-table-il-procurement-tender-agency'
      : 'ti-table-il-procurement-tender'
    this.title = (
      <span>
        <img src="/logos/government_of_israel.svg" className="dropdown-flag" />
        Israeli Tenders {direction}
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? 'il-procurement-tender-agency'
      : 'il-procurement-tender'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Publication Date' },
      { title: 'Modification Date' },
      { title: 'Start Date' },
      { title: 'End Date' },
      { title: 'Title' },
      { title: 'Buyer' },
      { title: 'Subjects' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    const publicationDate = formatDate(filing.bid_publication_date)
    const modificationDate = formatDate(filing.bid_update_date)
    const startDate = formatDate(filing.first_submission_date)
    const endDate = formatDate(filing.last_submission_date)

    const title = formatTranslation(filing.title, filing.title_en)
    const buyer = formatTranslation(filing.publisher, filing.publisher_en)

    const subjects = formatTranslation(
      filing.subjects.join('; '),
      filing.subjects_en.join('; ')
    )

    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      publicationDate,
      modificationDate,
      startDate,
      endDate,
      title,
      buyer,
      subjects,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderTender}
      />
    )
  }
}
