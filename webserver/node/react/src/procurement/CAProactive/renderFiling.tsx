import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function renderActionDetails(filing) {
  const label = 'contract-action-details'
  const title = 'Contract Action Details'

  let features = []

  ui.includeTableKeyValue(features, 'Reference Number', filing.reference_number)
  ui.includeTableKeyValue(features, 'Procurement ID', filing.procurement_id)
  ui.includeTableKeyValue(
    features,
    'Standing Offer Number',
    filing.standing_offer_number
  )
  ui.includeTableKeyValue(features, 'Number of Bids', filing.number_of_bids)
  ui.includeTableKeyValue(features, 'Trade Agreement', filing.trade_agreement)

  if (filing.instrument_type) {
    const instrumentTypeMap = {
      A: 'Amendment',
      C: 'Contract',
      SOSA: 'Standing Offer or Supply Arrangement',
    }
    let typeString = undefined
    let instrumentTypeTokens = filing.instrument_type.split(':')
    const primaryToken = instrumentTypeTokens[0].trim()
    if (primaryToken in instrumentTypeMap) {
      typeString = instrumentTypeMap[primaryToken]
    } else {
      typeString = filing.instrument_type
    }
    ui.includeTableKeyValue(features, 'Instrument Type', typeString)
  }

  if (
    filing.annotation.tradeAgreementExceptions &&
    filing.annotation.tradeAgreementExceptions != 'None'
  ) {
    ui.includeTableKeyValue(
      features,
      'Trade Agreement Exceptions',
      filing.annotation.tradeAgreementExceptions
    )
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderValues(filing) {
  const label = 'contract-values'
  const title = 'Contract Values'

  let features = []

  function includeCADValue(k, v) {
    if (v) {
      features.push(
        <ui.TableKeyValue k={k} v={`${util.numberWithCommas(v)} CAD`} />
      )
    }
  }

  includeCADValue('Contract Value', filing.contract_value)
  includeCADValue('Original Value', filing.original_value)
  includeCADValue('Amendment Value', filing.amendment_value)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderDates(filing) {
  const label = 'contract-dates'
  const title = 'Contract Dates'

  let features = []

  ui.includeTableKeyDate(features, 'Contract Date', filing.contract_date)
  ui.includeTableKeyDate(
    features,
    'Contract Start',
    filing.contract_period_start
  )
  ui.includeTableKeyDate(features, 'Delivery Date', filing.delivery_date)
  ui.includeTableKeyValue(features, 'Reporting Period', filing.reporting_period)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderVendor(filing) {
  const label = 'vendor'

  const annotation = filing.annotation.vendor
  const title = `Vendor: ${annotation.origText.toUpperCase()}`

  const features = [
    <ui.TableKeyValue
      k="Vendor Name"
      v={util.coerceUpperCase(annotation.origText)}
    />,
    filing.vendor_postal_code ? (
      <ui.TableKeyValue k="Postal Code" v={filing.vendor_postal_code} />
    ) : undefined,
    filing.country_of_vendor ? (
      <ui.TableKeyValue k="Country" v={filing.country_of_vendor} />
    ) : undefined,
    <ui.TableKeyValue
      k="Procuring Org"
      v={filing.owner_org_title ? filing.owner_org_title : filing.owner_org}
    />,
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderAgency(filing) {
  const label = 'procuring-org'

  const annotation = filing.annotation.agency
  const title = `Vendor: ${annotation.text.toUpperCase()}`

  const features = [
    <ui.TableKeyValue
      k="Procuring Org"
      v={util.coerceUpperCase(annotation.origText)}
    />,
    filing.buyer_name ? (
      <ui.TableKeyValue k="Buyer" v={filing.buyer_name} />
    ) : undefined,
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderDescription(filing) {
  const label = 'description'
  const title = 'Description'

  let features = []

  ui.includeTableKeyValue(features, 'Description', filing.description)
  ui.includeTableKeyValue(features, 'Comments', filing.comments)
  ui.includeTableKeyValue(
    features,
    'Additional Comments',
    filing.additional_comments
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

export class Award extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = ['reference_number', 'procurement_id']
    super(props)
    super.setEndpoint('/api/ca/procurement/proactive')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing
    const maxTitleLength = 200

    const title = util.trimLabel(filing.description, maxTitleLength)

    let accordionItems = []
    accordionItems.push(renderVendor(filing))
    accordionItems.push(renderAgency(filing))
    accordionItems.push(renderActionDetails(filing))
    accordionItems.push(renderValues(filing))
    accordionItems.push(renderDates(filing))
    accordionItems.push(renderDescription(filing))
    let accordion = (
      <Accordion id="au-procurement-accordion" items={accordionItems} />
    )

    const proactiveURL =
      'https://open.canada.ca/data/en/dataset/d8f85d91-7dec-4fd1-8055-483b77225d8b'
    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a className="btn btn-light me-2" href={proactiveURL} role="button">
            Full Dataset
          </a>
          <a
            className="btn btn-light"
            href={`https://search.open.canada.ca/contracts/?sort=contract_date+desc&search_text=${filing.reference_number}+${filing.procurement_id}`}
            role="button"
          >
            Contract Search
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
