import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function hasMoreThanAscii(str: string): boolean {
  if (!str) {
    return false
  }
  return str.split('').some(function (char) {
    return char.charCodeAt(0) > 127
  })
}

function formatTranslation(hebrewText, englishText) {
  if (hasMoreThanAscii(hebrewText)) {
    return hebrewText
      ? hebrewText == englishText
        ? hebrewText
        : `${hebrewText}, [EN machine translation]: ${englishText}`
      : ''
  } else {
    return hebrewText
  }
}

export class ExemptFiling extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/il/procurement/exempt')
    super.setUniqueKeys(['bid_number'])
  }

  render() {
    const filing = this.state.filing

    function renderPublisher() {
      const label = 'publisher'
      const title = 'Publisher'

      let features = []

      ui.includeTableKeyValue(features, 'Publisher', filing.publisher)
      if (
        hasMoreThanAscii(filing.publisher) &&
        filing.publisher_en != filing.publisher
      ) {
        ui.includeTableKeyValue(
          features,
          'Publisher [EN machine translation]',
          filing.publisher_en
        )
      }

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={filing.annotation.publisher}
        />
      )
    }

    function renderDates() {
      const label = 'dates'
      const title = 'Overall Dates'

      let features = []

      ui.includeTableKeyDate(
        features,
        'Publication Date',
        filing.bid_publication_date
      )
      ui.includeTableKeyDate(
        features,
        'Last Modification',
        filing.bid_update_date
      )
      ui.includeTableKeyDate(features, 'Last Date', filing.bid_last_date)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderMiscInfo() {
      const label = 'miscInfo'
      const title = 'Misc. Info'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Exemption reasons',
        filing.exemption_reasons
      )
      if (
        hasMoreThanAscii(filing.exemption_reasons) &&
        filing.exemption_reasons != filing.exemption_reasons_en
      ) {
        ui.includeTableKeyValue(
          features,
          'Exemption reasons [EN machine translation]',
          filing.exemption_reasons_en
        )
      }

      ui.includeTableKeyValue(features, 'Contract ID', filing.bid_number)
      ui.includeTableKeyValue(features, 'Status', filing.bid_status)
      if (
        hasMoreThanAscii(filing.bid_status) &&
        filing.bid_status != filing.bid_status_en
      ) {
        ui.includeTableKeyValue(
          features,
          'Status [EN machine translation]',
          filing.bid_status_en
        )
      }
      ui.includeTableKeyValue(features, 'Process ID', filing.bid_process)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderEngagement(engagements, index) {
      const label = `engagement-${index}`
      const title =
        engagements.length == 1 ? 'Engagement' : `Engagement ${index + 1}`

      const engagement = engagements[index]

      let features = []

      ui.includeTableKeyValue(features, 'Amount', engagement.amount)
      ui.includeTableKeyValue(features, 'Currency', engagement.currency)
      ui.includeTableKeyDate(features, 'Start date', engagement.start_date)
      ui.includeTableKeyDate(features, 'End date', engagement.end_date)

      ui.includeTableKeyValue(features, 'Supplier', engagement.supplier)
      if (
        hasMoreThanAscii(engagement.supplier) &&
        engagement.supplier != engagement.supplier_en
      ) {
        ui.includeTableKeyValue(
          features,
          'Supplier [EN machine translation]',
          engagement.supplier_en
        )
      }
      ui.includeTableKeyValue(features, 'Supplier Tax ID', engagement.tax_id)
      ui.includeTableKeyValue(
        features,
        'Exemption regulation',
        engagement.regulation
      )
      if (
        hasMoreThanAscii(engagement.regulation) &&
        engagement.regulation != engagement.regulation_en
      ) {
        ui.includeTableKeyValue(
          features,
          'Exemption regulation [EN machine translation]',
          engagement.regulation_en
        )
      }

      ui.includeTableKeyValue(features, 'Tax ID', engagement.tax_is)

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={filing.annotation.vendors[index]}
        />
      )
    }

    function renderRelatedDocument(documents, index) {
      const label = `related-doc-${index}`
      const title =
        documents.length == 1
          ? 'Related Document'
          : `Related document ${index + 1}`

      const document = documents[index]

      let features = []

      ui.includeTableKeyDate(features, 'Date', document.date)
      ui.includeTableKeyValue(features, 'Short description', document.head)
      if (
        hasMoreThanAscii(document.head) &&
        document.head != document.head_en
      ) {
        ui.includeTableKeyValue(
          features,
          'Short description [EN machine translation]',
          document.head_en
        )
      }
      ui.includeTableKeyValue(features, 'Description', document.description)
      if (
        hasMoreThanAscii(document.description) &&
        document.description != document.description_en
      ) {
        ui.includeTableKeyValue(
          features,
          'Description [EN machine translation]',
          document.description_en
        )
      }
      ui.includeTableKeyValue(
        features,
        'URL',
        <a href={document.url}>{util.simplifyURL(document.url)}</a>
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderSubject(subjects, subjects_en, index) {
      const label = `subject-${index}`
      const title = subjects.length == 1 ? 'Subject' : `Subject ${index + 1}`

      const subject = subjects[index]
      const subject_en = subjects_en[index]

      let features = []

      ui.includeTableKeyValue(features, 'Subject', subject)
      if (hasMoreThanAscii(subject) && subject != subject_en) {
        ui.includeTableKeyValue(
          features,
          'Subject [EN machine translation]',
          subject_en
        )
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    const title = formatTranslation(filing.title, filing.title_en)
    let header = (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light"
            href={`https://mr.gov.il/ilgstorefront/en/p/${filing.bid_number}`}
            role="button"
          >
            View Original
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderPublisher())
    accordionItems.push(renderMiscInfo())
    for (let index = 0; index < filing.engagements.length; index += 1) {
      accordionItems.push(renderEngagement(filing.engagements, index))
    }
    accordionItems.push(renderDates())
    for (let index = 0; index < filing.related_documents.length; index += 1) {
      accordionItems.push(
        renderRelatedDocument(filing.related_documents, index)
      )
    }
    for (let index = 0; index < filing.subjects.length; index += 1) {
      accordionItems.push(
        renderSubject(filing.subjects, filing.subjects_en, index)
      )
    }

    let accordion = (
      <Accordion id="il-procurement-exempt-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export class Tender extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/il/procurement/tender')
    super.setUniqueKeys(['bid_number'])
  }

  render() {
    const filing = this.state.filing

    function renderPublisher() {
      const label = 'publisher'
      const title = 'Publisher'

      let features = []

      ui.includeTableKeyValue(features, 'Publisher', filing.publisher)
      if (
        hasMoreThanAscii(filing.publisher) &&
        filing.publisher_en != filing.publisher
      ) {
        ui.includeTableKeyValue(
          features,
          'Publisher [EN machine translation]',
          filing.publisher_en
        )
      }

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={filing.annotation.publisher}
        />
      )
    }

    function renderDates() {
      const label = 'dates'
      const title = 'Dates'

      let features = []

      ui.includeTableKeyDate(
        features,
        'Publication Date',
        filing.bid_publication_date
      )
      ui.includeTableKeyDate(
        features,
        'Last Modification',
        filing.bid_update_date
      )
      ui.includeTableKeyDate(
        features,
        'First Submission Date',
        filing.first_submission_date
      )
      ui.includeTableKeyDate(
        features,
        'Last Submission Date',
        filing.last_submission_date
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderMiscInfo() {
      const label = 'miscInfo'
      const title = 'Misc. Info'

      let features = []

      ui.includeTableKeyValue(features, 'Tender ID', filing.bid_number)
      ui.includeTableKeyValue(features, 'Status', filing.bid_status)
      if (
        hasMoreThanAscii(filing.bid_status) &&
        filing.bid_status != filing.bid_status_en
      ) {
        ui.includeTableKeyValue(
          features,
          'Status [EN machine translation]',
          filing.bid_status_en
        )
      }
      ui.includeTableKeyValue(features, 'Process ID', filing.bid_process)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderRelatedDocument(documents, index) {
      const label = `related-doc-${index}`
      const title =
        documents.length == 1
          ? 'Related Document'
          : `Related document ${index + 1}`

      const document = documents[index]

      let features = []

      ui.includeTableKeyDate(features, 'Date', document.date)
      ui.includeTableKeyValue(features, 'Short description', document.head)
      if (
        hasMoreThanAscii(document.head) &&
        document.head != document.head_en
      ) {
        ui.includeTableKeyValue(
          features,
          'Short description [EN machine translation]',
          document.head_en
        )
      }
      ui.includeTableKeyValue(features, 'Description', document.description)
      if (
        hasMoreThanAscii(document.description) &&
        document.description != document.description_en
      ) {
        ui.includeTableKeyValue(
          features,
          'Description [EN machine translation]',
          document.description_en
        )
      }
      ui.includeTableKeyValue(
        features,
        'URL',
        <a href={document.url}>{util.simplifyURL(document.url)}</a>
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderSubject(subjects, subjects_en, index) {
      const label = `subject-${index}`
      const title = subjects.length == 1 ? 'Subject' : `Subject ${index + 1}`

      const subject = subjects[index]
      const subject_en = subjects_en[index]

      let features = []

      ui.includeTableKeyValue(features, 'Subject', subject)
      if (hasMoreThanAscii(subject) && subject != subject_en) {
        ui.includeTableKeyValue(
          features,
          'Subject [EN machine translation]',
          subject_en
        )
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    const title = formatTranslation(filing.title, filing.title_en)
    let header = (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light"
            href={`https://mr.gov.il/ilgstorefront/en/p/${filing.bid_number}`}
            role="button"
          >
            View Original
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderPublisher())
    accordionItems.push(renderDates())
    accordionItems.push(renderMiscInfo())
    for (let index = 0; index < filing.related_documents.length; index += 1) {
      accordionItems.push(
        renderRelatedDocument(filing.related_documents, index)
      )
    }
    for (let index = 0; index < filing.subjects.length; index += 1) {
      accordionItems.push(
        renderSubject(filing.subjects, filing.subjects_en, index)
      )
    }

    let accordion = (
      <Accordion id="il-procurement-tender-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderExemptFiling(filing) {
  return <ExemptFiling filing={filing} />
}

export function renderTender(filing) {
  return <Tender filing={filing} />
}
