import * as React from 'react'

import { FilingWithTags } from '../../FilingWithTags'
import * as util from '../../utilities/util'

const kDODBase = 'https://www.defense.gov/Newsroom/Contracts/Contract/Article'

export class Award extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/us/central/procurement/dod-announce')
    super.setUniqueKeys(['article_id'])

    this.state = { ...this.state, articleHTML: undefined }

    this.fetchPage = this.fetchPage.bind(this)
  }

  async fetchPage() {
    let that = this
    const url = `${kDODBase}/${this.props.filing.article_id}`
    fetch(url)
      .then((response) => {
        return response.text()
      })
      .then((html) => {
        const parser = new DOMParser()
        const doc = parser.parseFromString(html, 'text/html')
        const body = doc.querySelector('.body')
        that.setState({ articleHTML: body.innerHTML })
      })
  }

  async componentDidMount() {
    await this.fetchPage()
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.filing != this.props.filing) {
      await this.fetchPage()
    }
  }

  render() {
    const filing = this.state.filing

    const url = `${kDODBase}/${filing.article_id}`
    const title =
      util.dateWithoutTime(new Date(filing.date)) +
      ': ' +
      util.trimLabel(filing.text, 100)

    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a className="btn btn-light" href={url} role="button">
            View Original
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {this.state.articleHTML ? (
            <div
              dangerouslySetInnerHTML={{ __html: this.state.articleHTML }}
            ></div>
          ) : (
            <div>Fetching page...</div>
          )}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
