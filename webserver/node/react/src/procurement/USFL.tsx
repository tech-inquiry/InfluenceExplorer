import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USFL/renderFiling'

import * as util from '../utilities/util'

export class USFLProcurement extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    const direction = this.props.isAgency ? '(as agency)' : '(as vendor)'
    const source = `US FL ${direction}`
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[source].get

    this.tableID = this.props.isAgency
      ? 'ti-table-us-fl-procurement-agency'
      : 'ti-table-us-fl-procurement'
    this.title = (
      <span>
        <img src="/logos/state_of_florida.svg" className="dropdown-flag" />
        Florida (US) Procurement Records {direction}
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? 'us-fl-procurement-agency'
      : 'us-fl-procurement'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Begin Date' },
      { title: 'Vendor' },
      { title: 'Agency' },
      {
        title: 'Total Amount',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
      },
      { title: 'Long Title' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex: number) {
    const beginDate = filing.begin_date
      ? util.dateWithoutTimeLex(filing.begin_date)
      : ''
    const vendor = util.coerce(filing.vendor, '')
    const agency = util.coerce(filing.agency, '')
    const totalAmount = util.coerce(filing.total_amount, '')
    const longTitle = util.coerce(filing.long_title, '')
    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      beginDate,
      vendor,
      agency,
      totalAmount,
      longTitle,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
