import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

export class Award extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = [
      'agency',
      'vendor',
      'type',
      'agency_contract_id',
      'po_number',
      'grant_award_id',
      'commodity_type_code',
      'flair_contract_id',
      'state_term_contract_id',
      'status',
    ]
    super(props)
    super.setEndpoint('/api/us/fl/procurement')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing

    function renderVendor() {
      const label = 'vendor'
      const title = 'Vendor'

      let features = []

      const annotation = filing.annotation.vendor

      ui.includeTableKeyValue(
        features,
        'Name',
        util.coerceUpperCase(annotation.origText)
      )

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderAgency() {
      const label = 'agency'
      const title = 'Agency'

      let features = []

      const annotation = filing.annotation.agency

      ui.includeTableKeyValue(
        features,
        'Name',
        util.coerceUpperCase(annotation.origText)
      )
      ui.includeTableKeyValue(
        features,
        'Agency Service Area',
        filing.agency_service_area
      )
      ui.includeTableKeyValue(
        features,
        'Agency Reference Number',
        filing.agency_reference_number
      )
      ui.includeTableKeyValue(
        features,
        'Agency Contract ID',
        filing.agency_contract_id
      )

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderActionDetails() {
      const label = 'contract-action-details'
      const title = 'Contract Action Details'

      let features = []

      ui.includeTableKeyValue(features, 'Contract Action Type', filing.type)
      ui.includeTableKeyValue(features, 'PO Number', filing.po_number)
      ui.includeTableKeyValue(
        features,
        'Grant Award Number',
        filing.grant_award_id
      )
      ui.includeTableKeyValue(
        features,
        'FLAIR Contract ID',
        filing.flair_contract_id
      )
      ui.includeTableKeyValue(
        features,
        'State Term Contract ID',
        filing.state_term_contract_id
      )
      ui.includeTableKeyValue(features, 'Commodity Type', filing.commodity_type)
      ui.includeTableKeyValue(
        features,
        'Commodity Type Description',
        filing.commodity_type_description
      )
      ui.includeTableKeyValue(
        features,
        'Authorized Advanced Payment',
        filing.authorized_advanced_payment
      )
      ui.includeTableKeyValue(
        features,
        'Method of Procurement',
        filing.method_of_procurement
      )
      ui.includeTableKeyValue(
        features,
        'Contract Exemption Explanation',
        filing.contract_exemption_explanation
      )
      ui.includeTableKeyValue(
        features,
        'Statutory Authority',
        filing.statutory_authority
      )
      ui.includeTableKeyValue(features, 'Recipient Type', filing.recipient_type)
      ui.includeTableKeyValue(
        features,
        'Business Case Study',
        filing.business_case_study
      )
      ui.includeTableKeyValue(
        features,
        'Involves Government Aid',
        filing.involves_gov_aid
      )
      ui.includeTableKeyValue(
        features,
        'Previously Done by State',
        filing.previously_done_by_state
      )
      ui.includeTableKeyValue(
        features,
        'Considered for Insourcing',
        filing.considered_for_insourcing
      )
      ui.includeTableKeyValue(
        features,
        'Improvement on State Property',
        filing.improvement_on_state_property
      )
      ui.includeTableKeyValue(
        features,
        'Improvement Description',
        filing.improvement_description
      )
      ui.includeTableKeyValue(
        features,
        'Improvement Value',
        `${util.numberWithCommas(filing.improvement_value)} USD`,
        true,
        filing.improvement_value
      )
      ui.includeTableKeyValue(
        features,
        'Improvement Unamortized Value',
        filing.improvement_unamortized_value
      )

      ui.includeTableKeyValue(
        features,
        'Provide Administrative Cost',
        filing.provide_admin_cost
      )
      ui.includeTableKeyValue(
        features,
        'Administrative Cost Percent',
        filing.admin_cost_percent
      )
      ui.includeTableKeyValue(
        features,
        'Provide for Periodic Increase',
        filing.provide_periodic_increase
      )
      ui.includeTableKeyValue(
        features,
        'Periodic Increase Percent',
        filing.periodic_increase_percent
      )
      ui.includeTableKeyValue(features, 'CFDA Code', filing.cfda_code)
      ui.includeTableKeyValue(
        features,
        'CFDA Description',
        filing.cfda_description
      )
      ui.includeTableKeyValue(features, 'CSFA Code', filing.csfa_code)
      ui.includeTableKeyValue(
        features,
        'CSFA Description',
        filing.csfa_description
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderAmounts() {
      const label = 'amounts'
      const title = 'Amounts'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Original Contract Amount',
        `${util.numberWithCommas(filing.orig_contract_amount)} USD`,
        true,
        filing.orig_contract_amount
      )
      ui.includeTableKeyValue(
        features,
        'Total Amount',
        `${util.numberWithCommas(filing.total_amount)} USD`,
        true,
        filing.total_amount
      )
      ui.includeTableKeyValue(
        features,
        'Recurring Budgetary Amount',
        `${util.numberWithCommas(filing.recurring_budgetary_amount)} USD`,
        true,
        filing.recurring_budgetary_amount
      )
      ui.includeTableKeyValue(
        features,
        'Non-Recurring Budgetary Amount',
        `${util.numberWithCommas(filing.non_recurring_budgetary_amount)} USD`,
        true,
        filing.non_recurring_budgetary_amount
      )
      ui.includeTableKeyValue(
        features,
        'PO Budget Amount',
        `${util.numberWithCommas(filing.po_budget_amount)} USD`,
        true,
        filing.po_budget_amount
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderDates() {
      const label = 'dates'
      const title = 'Dates'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Grant Award Date',
        util.dateWithoutTime(new Date(filing.grant_award_date)),
        true,
        filing.grant_award_date
      )
      ui.includeTableKeyValue(
        features,
        'PO Award Date',
        util.dateWithoutTime(new Date(filing.po_award_date)),
        true,
        filing.po_award_date
      )
      ui.includeTableKeyValue(
        features,
        'Begin Date',
        util.dateWithoutTime(new Date(filing.begin_date)),
        true,
        filing.begin_date
      )
      ui.includeTableKeyValue(
        features,
        'Original End Date',
        util.dateWithoutTime(new Date(filing.original_end_date)),
        true,
        filing.original_end_date
      )
      ui.includeTableKeyValue(
        features,
        'New End Date',
        util.dateWithoutTime(new Date(filing.new_end_date)),
        true,
        filing.new_end_date
      )
      ui.includeTableKeyValue(
        features,
        'Contract Execution Date',
        util.dateWithoutTime(new Date(filing.contract_execution_date)),
        true,
        filing.contract_execution_date
      )
      ui.includeTableKeyValue(
        features,
        'Business Case Date',
        util.dateWithoutTime(new Date(filing.business_case_date)),
        true,
        filing.business_case_date
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderDescription() {
      const label = 'description'
      const title = 'Description'

      let features = []

      ui.includeTableKeyValue(features, 'Long Title', filing.long_title)
      ui.includeTableKeyValue(features, 'Short Title', filing.short_title)
      ui.includeTableKeyValue(features, 'Comment', filing.comment)
      ui.includeTableKeyValue(
        features,
        'Legal Challenges',
        filing.legal_challenges
      )
      ui.includeTableKeyValue(
        features,
        'Legal Challenge Description',
        filing.legal_challenge_description
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    let title = []
    if (filing.long_title) {
      title.push(<p className="ti-compact">{filing.long_title}</p>)
    }
    if (filing.short_title) {
      title.push(<p className="ti-compact">{filing.short_title}</p>)
    }

    const header = filing.annotation.awardURL ? (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light"
            href={filing.annotation.awardURL}
            role="button"
          >
            View Original
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    ) : undefined

    let accordionItems = []
    accordionItems.push(renderVendor())
    accordionItems.push(renderAgency())
    accordionItems.push(renderActionDetails())
    accordionItems.push(renderAmounts())
    accordionItems.push(renderDates())
    accordionItems.push(renderDescription())
    let accordion = (
      <Accordion id="au-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
