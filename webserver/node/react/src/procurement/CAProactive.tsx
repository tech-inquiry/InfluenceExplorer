import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import * as util from '../utilities/util'

import { renderFiling } from './CAProactive/renderFiling'

export class CAProactiveProcurement extends React.Component<any, any> {
  urlBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    const source = this.props.isAgency
      ? 'CA proactive (as agency)'
      : 'CA proactive (as vendor)'
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[source].get

    this.maxDescriptionLength = 256
    this.tableID = this.props.isAgency
      ? 'ti-table-ca-proactive-agency-procurement'
      : 'ti-table-ca-proactive-procurement'
    this.title = (
      <span>
        <img src="/logos/government_of_canada.svg" className="dropdown-flag" />
        Canadian proactively-disclosed procurement{' '}
        {this.props.isAgency ? '(as agency)' : '(as vendor)'}
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? 'ca-proactive-agency-procurement'
      : 'ca-proactive-procurement'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Contract Date' },
      { title: 'Vendor' },
      { title: 'Owner Title' },
      { title: 'Procurement ID' },
      {
        title: 'Contract Value',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, 'CA$'),
      },
      { title: 'Description' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    const contractDate = filing.contract_date
      ? util.dateWithoutTimeLex(filing.contract_date)
      : ''
    const vendor = util.coerce(filing.vendor_name, '')
    const ownerTitle = util.coerce(filing.owner_org_title, '')
    const procurementID = util.coerce(filing.procurement_id, '')
    const contractValue = util.coerce(filing.contract_value, '')

    let descriptionLines = []
    if (filing.description) {
      descriptionLines.push(filing.description)
    }
    if (filing.comments) {
      descriptionLines.push(filing.comments)
    }
    if (filing.additional_comments) {
      descriptionLines.push(filing.additional_comments)
    }
    const description = descriptionLines.join('; ')

    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      contractDate,
      vendor,
      ownerTitle,
      procurementID,
      contractValue,
      description,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="ca_proactive_disclosure_filing"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
