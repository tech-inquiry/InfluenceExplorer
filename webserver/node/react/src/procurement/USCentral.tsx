import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { highlightFilter, renderFiling } from './USCentral/renderFiling'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

const kColumnsWithSubawards = [
  { title: 'Index', visible: false },
  { title: 'Date' },
  { title: 'Prime Awardee' },
  { title: 'Subawardee' },
  { title: 'Agencies' },
  { title: 'Parent Identifier' },
  { title: 'Prime Identifier' },
  { title: 'Subaward Number' },
  {
    title: 'Action Obligation',
    render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
  },
  {
    title: 'Overall Ceiling',
    render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
  },
  { title: 'Description' },
  { title: 'Entire Filing', visible: false },
]
const kColumnsWithoutSubawards = [
  { title: 'Index', visible: false },
  { title: 'Date' },
  { title: 'Prime Awardee' },
  { title: 'Agencies' },
  { title: 'Parent Identifier' },
  { title: 'Prime Identifier' },
  {
    title: 'Action Obligation',
    render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
  },
  {
    title: 'Overall Ceiling',
    render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
  },
  { title: 'Description' },
  { title: 'Entire Filing', visible: false },
]

export class USCentralProcurement extends React.Component<any, any> {
  primeURLBase: string
  subURLBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  title: any

  constructor(props) {
    super(props)

    const direction = this.props.isAgency ? '(as agency)' : '(as vendor)'

    this.primeURLBase =
      appConfig.apiURL +
      '/' +
      kProcurementPaths[`US federal prime ${direction}`].get
    this.subURLBase =
      appConfig.apiURL +
      '/' +
      kProcurementPaths[`US federal sub ${direction}`].get

    this.maxDescriptionLength = 256
    this.tableID = this.props.isAgency
      ? 'ti-table-us-central-procurement-agency'
      : 'ti-table-us-central-procurement'
    this.title = (
      <div>
        <p className="ti-compact">
          <span>
            <img
              src="/logos/government_of_the_united_states.svg"
              className="dropdown-flag"
            />
            U.S. federal procurement records{' '}
            {this.props.isAgency ? '(as agency)' : undefined}
          </span>
        </p>
        <p className="ti-compact text-muted lh-1" style={{ fontSize: '80%' }}>
          <small>
            Only records with prime award modification numbers of zero are new
            contracts -- others are updates. It is a common mistake for
            reporters to include modifications in their counts of the total
            number of contracts.
          </small>
        </p>
      </div>
    )

    this.closeModal = this.closeModal.bind(this)
    this.primeFilingToRow = this.primeFilingToRow.bind(this)
    this.subfilingToRow = this.subfilingToRow.bind(this)
    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURLs = this.retrieveURLs.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? 'us-central-procurement-agency'
      : 'us-central-procurement-vendor'

    // We will decide whether there are subawards in the preprocessing phase.
    this.state = {
      haveSubawards: undefined,
    }
  }

  primeFilingToRow(filing, filingIndex: number, haveSubawards: boolean) {
    const signedDate = util.dateWithoutTimeLex(filing.signed_date)
    const primeAwardee =
      filing.vendor.toUpperCase() +
      (filing.contractor ? '; ' + filing.contractor.toUpperCase() : '')
    const subawardee = ''
    const parentAwardID = filing.parent_mod_number
      ? `${filing.parent_piid}<br/>Mod. ${filing.parent_mod_number}`
      : filing.parent_piid
    const primeAwardID = filing.mod_number
      ? `${filing.piid}<br/>Mod. ${filing.mod_number}`
      : filing.piid
    const subawardNumber = ''
    const actionObligation = filing.obligated_amount
    const totalPotential = filing.total_base_and_all_options_value
    const description = util.trimLabel(
      util.coerceUpperCase(filing.description),
      this.maxDescriptionLength
    )
    const fundingInfo = {
      department: filing.funding_department.toUpperCase(),
      agency: filing.funding_agency.toUpperCase(),
    }
    const contractingInfo = {
      department: filing.contracting_department.toUpperCase(),
      agency: filing.contracting_agency.toUpperCase(),
    }

    let agenciesSet = new Set<string>()
    let agenciesList = []
    for (const info of [fundingInfo, contractingInfo]) {
      if (info.agency || info.department) {
        const candidate =
          info.department == info.agency
            ? info.department
            : `${info.department}: ${info.agency}`
        if (!agenciesSet.has(candidate.toUpperCase())) {
          agenciesSet.add(candidate.toUpperCase())
          agenciesList.push(candidate)
        }
      }
    }
    const agencies = agenciesList.join('; ')

    const entireFiling = JSON.stringify(filing)

    if (haveSubawards) {
      return [
        filingIndex,
        signedDate,
        primeAwardee,
        subawardee,
        agencies,
        parentAwardID,
        primeAwardID,
        subawardNumber,
        actionObligation,
        totalPotential,
        description,
        entireFiling,
      ]
    } else {
      return [
        filingIndex,
        signedDate,
        primeAwardee,
        agencies,
        parentAwardID,
        primeAwardID,
        actionObligation,
        totalPotential,
        description,
        entireFiling,
      ]
    }
  }

  subfilingToRow(filing, filingIndex: number) {
    const signedDate = util.dateWithoutTimeLex(filing.sub_action_date)
    const primeAwardee = filing.prime_name
      ? filing.prime_name.toUpperCase()
      : filing.prime_parent_name.toUpperCase()
    const subawardee = filing.sub_name.toUpperCase()
    const parentAwardID = filing.prime_parent_piid
    const primeAwardID = filing.prime_piid
    const subawardNumber = filing.sub_number
    const actionObligation = filing.sub_amount
    const totalPotential = ''
    const description = util.trimLabel(
      filing.sub_description,
      this.maxDescriptionLength
    )
    const fundingInfo = {
      department: util.coerce(filing.prime_funding_agency, ''),
      agency: util.coerce(filing.prime_funding_sub_agency, ''),
    }
    const contractingInfo = {
      department: util.coerce(filing.prime_awarding_agency, ''),
      agency: util.coerce(filing.prime_awarding_sub_agency, ''),
    }

    let agenciesSet = new Set<string>()
    let agenciesList = []
    for (const info of [fundingInfo, contractingInfo]) {
      if (info.agency || info.department) {
        const candidate =
          info.department == info.agency
            ? info.department
            : `${info.department}: ${info.agency}`
        if (!agenciesSet.has(candidate.toUpperCase())) {
          agenciesSet.add(candidate.toUpperCase())
          agenciesList.push(candidate)
        }
      }
    }
    const agencies = agenciesList.join('; ')

    const entireFiling = JSON.stringify(filing)

    return [
      filingIndex,
      signedDate,
      primeAwardee,
      subawardee,
      agencies,
      parentAwardID,
      primeAwardID,
      subawardNumber,
      actionObligation,
      totalPotential,
      description,
      entireFiling,
    ]
  }

  itemToRow(item, itemIndex: number, haveSubawards: boolean) {
    if (item.source == 'prime') {
      return this.primeFilingToRow(item.filing, itemIndex, haveSubawards)
    } else {
      return this.subfilingToRow(item.filing, itemIndex)
    }
  }

  itemsToTableData(items) {
    let haveSubawards = false
    for (const [itemIndex, item] of items.entries()) {
      if (item.source == 'sub') {
        haveSubawards = true
        break
      }
    }
    this.setState({ haveSubawards: haveSubawards })

    function itemDate(item) {
      return item.source == 'prime'
        ? item.filing.signed_date
        : item.filing.sub_action_date
    }

    // We also sort the items by date for consistency.
    items.sort(function (a, b) {
      const aDate = itemDate(a)
      const bDate = itemDate(b)
      return aDate == bDate ? 0 : aDate > bDate ? -1 : 1
    })

    const that = this
    return items.map(function (item, index) {
      return that.itemToRow(item, index, haveSubawards)
    })
  }

  retrieveURLs(): { [key: string]: string } {
    return {
      prime: util.getRetrieveURL(
        this.primeURLBase,
        this.props.query,
        this.props.isAgency,
        this.props.isEntity,
        this.props.isTag
      ),
      sub: util.getRetrieveURL(
        this.subURLBase,
        this.props.query,
        this.props.isAgency,
        this.props.isEntity,
        this.props.isTag
      ),
    }
  }

  // TODO(Jack Poulson): See if this can be deleted without effect.
  closeModal() {
    // Grab the modal created by the FilingFeed and then click.
    jQuery('#' + ui.modalCloseButtonId(this.label)).click()
  }

  render() {
    return (
      <FilingFeed
        tableName="us_central_procurement"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURLs={this.retrieveURLs()}
        columns={
          this.state.haveSubawards
            ? kColumnsWithSubawards
            : kColumnsWithoutSubawards
        }
        itemsToTableData={this.itemsToTableData}
        highlightFilter={highlightFilter}
        renderItem={renderFiling}
      />
    )
  }
}
