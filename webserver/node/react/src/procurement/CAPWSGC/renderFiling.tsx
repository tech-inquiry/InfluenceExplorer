import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function renderVendor(filing) {
  const label = 'vendor'

  const annotation = filing.annotation.vendor
  const title = `Vendor: ${annotation.origText.toUpperCase()}`

  const addressObj = {
    city: filing.supplier_address_city,
    state: filing.supplier_address_prov_state,
    postalCode: filing.supplier_address_postal_code,
    country: filing.supplier_address_country,
  }

  const legalName = util.canonicalText(filing.supplier_legal_name)
  const standardName = util.canonicalText(filing.supplier_standardized_name)
  const operatingName = util.canonicalText(filing.supplier_operating_name)

  const features = [
    <ui.TableKeyValue k="Vendor Legal Name" v={filing.supplier_legal_name} />,
    standardName && standardName != legalName ? (
      <ui.TableKeyValue
        k="Vendor Standardized Name"
        v={util.coerceUpperCase(standardName)}
      />
    ) : undefined,
    operatingName && operatingName != legalName ? (
      <ui.TableKeyValue
        k="Vendor Operating Name"
        v={util.coerceUpperCase(operatingName)}
      />
    ) : undefined,
    <ui.Address address={addressObj} />,
    filing.organization_employee_count != 'UNKNOWN' ? (
      <ui.TableKeyValue
        k="Employee Count"
        v={filing.organization_employee_count}
      />
    ) : undefined,
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderBuyer(filing) {
  const label = 'buyer'
  const title = `Buyer: ${filing.contracting_entity_office_name.toUpperCase()}`

  const annotation = filing.annotation.agency

  let addressObj = {
    streetLines: [],
    city: filing.contracting_address_city,
    state: filing.contracting_address_prov_state,
    postalCode: filing.contracting_address_postal_code,
    country: filing.contracting_address_country,
  }
  if (
    filing.contracting_address_street_1 ||
    filing.contracting_address_street_2
  ) {
    if (filing.contracting_address_street_1) {
      addressObj.streetLines.push(filing.contracting_address_street_1)
    }
    if (filing.contracting_address_street_2) {
      addressObj.streetLines.push(filing.contracting_address_street_2)
    }
  }

  const features = [
    <ui.TableKeyValue
      k="Contracting Office"
      v={filing.contracting_entity_office_name.toUpperCase()}
    />,
    <ui.Address address={addressObj} />,
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderActionDetails(filing) {
  const label = 'contractActionDetails'
  const title = 'Contract Action Details'

  let features = []

  ui.includeTableKeyValue(features, 'Contract Number', filing.contract_number)
  ui.includeTableKeyValue(features, 'Amendment Number', filing.amendment_number)
  ui.includeTableKeyValue(
    features,
    'Competitive Tender',
    filing.competitive_tender
  )
  ui.includeTableKeyValue(
    features,
    'Limited Tender Reason',
    filing.limited_tender_reason_description
  )
  ui.includeTableKeyValue(
    features,
    'Solicitation Procedure',
    filing.solicitation_procedure_description
  )
  ui.includeTableKeyValue(
    features,
    'Trade Agreement',
    filing.trade_agreement_description
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderValues(filing) {
  const label = 'contractValues'
  const title = 'Contract Values'

  let features = []

  ui.includeTableKeyValue(
    features,
    'Contract Value',
    util.numberWithCommas(filing.contract_value) + ' CAD',
    true,
    filing.contract_value
  )
  ui.includeTableKeyValue(
    features,
    'Total Contract Value',
    util.numberWithCommas(filing.total_contract_value) + ' CAD',
    true,
    filing.total_contract_value
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderDates(filing) {
  const label = 'dates'
  const title = 'Dates'

  let features = []

  ui.includeTableKeyDate(features, 'Award Date', filing.award_date)
  ui.includeTableKeyDate(features, 'Expiry Date', filing.expiry_date)
  ui.includeTableKeyDate(features, 'Date Published', filing.date_file_published)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderDescription(filing) {
  const label = 'description'
  const title = 'Description'

  let features = []

  ui.includeTableKeyValue(
    features,
    'Contract Description',
    filing.gsin_description
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

export class Award extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = ['contract_number', 'amendment_number']
    super(props)
    super.setEndpoint('/api/ca/procurement/pwsgc')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing
    const maxTitleLength = 200

    const title = util.trimLabel(
      filing.supplier_legal_name
        ? `${filing.supplier_legal_name}: ${filing.gsin_description}`
        : filing.gsin_description,
      maxTitleLength
    )

    const pwsgcURL =
      'https://open.canada.ca/data/en/dataset/53753f06-8b28-42d7-89f7-04cd014323b0#wb-auto-6'
    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a className="btn btn-light me-2" href={pwsgcURL} role="button">
            Full Dataset
          </a>
          <a
            className="btn btn-light"
            href={`https://search.open.canada.ca/contracts/?sort=contract_date+desc&search_text=${filing.contract_number}`}
          >
            Contract Search
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderVendor(filing))
    accordionItems.push(renderBuyer(filing))
    accordionItems.push(renderActionDetails(filing))
    accordionItems.push(renderValues(filing))
    accordionItems.push(renderDates(filing))
    accordionItems.push(renderDescription(filing))
    let accordion = (
      <Accordion id="au-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
