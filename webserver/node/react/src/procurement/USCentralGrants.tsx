import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USCentralGrants/renderFiling'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

const kColumnsWithSubgrants = [
  { title: 'Index', visible: false },
  { title: 'Action Date' },
  { title: 'Recipient' },
  { title: 'Subawardee' },
  { title: 'Agencies' },
  { title: 'Award ID' },
  { title: 'Subaward Number' },
  {
    title: 'Total Obligated',
    render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
  },
  {
    title: 'Subaward Amount',
    render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
  },
  { title: 'Title' },
  { title: 'Description' },
  { title: 'Entire Filing', visible: false },
]
const kColumnsWithoutSubgrants = [
  { title: 'Index', visible: false },
  { title: 'Action Date' },
  { title: 'Recipient' },
  { title: 'Agencies' },
  { title: 'Award ID' },
  {
    title: 'Total Obligated',
    render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
  },
  { title: 'Title' },
  { title: 'Description' },
  { title: 'Entire Filing', visible: false },
]

export class USCentralGrants extends React.Component<any, any> {
  primeURLBase: string
  subURLBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  title: any

  constructor(props) {
    super(props)
    const direction = this.props.isAgency ? '(as agency)' : '(as vendor)'

    this.primeURLBase =
      appConfig.apiURL +
      '/' +
      kProcurementPaths[`US federal prime grants ${direction}`].get
    this.subURLBase =
      appConfig.apiURL +
      '/' +
      kProcurementPaths[`US federal subgrants ${direction}`].get

    this.maxDescriptionLength = 256
    this.tableID = this.props.isAgency
      ? 'ti-table-us-central-grants-agency'
      : 'ti-table-us-central-grants-procurement'
    this.title = (
      <span>
        <img
          src="/logos/government_of_the_united_states.svg"
          className="dropdown-flag"
        />
        US federal grants {this.props.isAgency ? '(as agency)' : undefined}
      </span>
    )

    this.closeModal = this.closeModal.bind(this)
    this.primeGrantToRow = this.primeGrantToRow.bind(this)
    this.subgrantToRow = this.subgrantToRow.bind(this)
    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURLs = this.retrieveURLs.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? 'us-central-grants-agency'
      : 'us-central-grants'

    // We will decide whether there are subgrants in the preprocessing phase.
    this.state = {
      haveSubgrants: undefined,
    }
  }

  primeGrantToRow(filing, filingIndex: number, haveSubgrants: boolean) {
    const startDate = filing.action_date
      ? util.dateWithoutTimeLex(filing.action_date)
      : ''
    const recipient = util.coerceUpperCase(filing.recipient_name_raw, '')
    const subName = ''
    const totalObligated = util.coerce(filing.total_obligated, '')

    const fundingInfo = {
      department: filing.funding_agency_name.toUpperCase(),
      agency: filing.funding_sub_agency_name.toUpperCase(),
    }
    const awardingInfo = {
      department: filing.awarding_agency_name.toUpperCase(),
      agency: filing.awarding_sub_agency_name.toUpperCase(),
    }

    let agenciesSet = new Set<string>()
    let agenciesList = []
    for (const info of [fundingInfo, awardingInfo]) {
      if (info.agency || info.department) {
        const candidate =
          info.department == info.agency
            ? info.department
            : `${info.department}: ${info.agency}`
        if (!agenciesSet.has(candidate.toUpperCase())) {
          agenciesSet.add(candidate.toUpperCase())
          agenciesList.push(candidate)
        }
      }
    }
    const agencies = agenciesList.join('; ')

    const awardID = filing.mod_number
      ? `${filing.id_fain}<br/>Mod. ${filing.mod_number}`
      : filing.id_fain
    const subNumber = ''
    const subawardAmount = ''

    const title = util.coerce(filing.cfda_title, '')
    const description = util.trimLabel(
      util.coerce(filing.transaction_description, ''),
      this.maxDescriptionLength
    )
    const entireFiling = JSON.stringify(filing)
    if (haveSubgrants) {
      return [
        filingIndex,
        startDate,
        recipient,
        subName,
        agencies,
        awardID,
        subNumber,
        totalObligated,
        subawardAmount,
        title,
        description,
        entireFiling,
      ]
    } else {
      return [
        filingIndex,
        startDate,
        recipient,
        agencies,
        awardID,
        totalObligated,
        title,
        description,
        entireFiling,
      ]
    }
  }

  subgrantToRow(filing, filingIndex: number) {
    const startDate = filing.sub_action_date
      ? util.dateWithoutTimeLex(filing.sub_action_date)
      : ''
    const recipient = util.coerceUpperCase(filing.prime_name, '')
    const subName = util.coerceUpperCase(filing.sub_name, '')
    const totalObligated = util.coerce(filing.prime_amount, '')

    const fundingInfo = {
      department: filing.prime_funding_agency_name.toUpperCase(),
      agency: filing.prime_funding_sub_agency_name.toUpperCase(),
    }
    const awardingInfo = {
      department: filing.prime_awarding_agency_name.toUpperCase(),
      agency: filing.prime_awarding_sub_agency_name.toUpperCase(),
    }

    let agenciesSet = new Set<string>()
    let agenciesList = []
    for (const info of [fundingInfo, awardingInfo]) {
      if (info.agency || info.department) {
        const candidate =
          info.department == info.agency
            ? info.department
            : `${info.department}: ${info.agency}`
        if (!agenciesSet.has(candidate.toUpperCase())) {
          agenciesSet.add(candidate.toUpperCase())
          agenciesList.push(candidate)
        }
      }
    }
    const agencies = agenciesList.join('; ')

    const awardID = filing.prime_fain
    const subNumber = filing.sub_number
    const subAmount = filing.sub_amount

    const title = util.coerce(filing.prime_cfda_numbers_and_titles, '')
    const description = util.trimLabel(
      util.coerce(filing.sub_description, ''),
      this.maxDescriptionLength
    )
    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      startDate,
      recipient,
      subName,
      agencies,
      awardID,
      subNumber,
      totalObligated,
      subAmount,
      title,
      description,
      entireFiling,
    ]
  }

  itemToRow(item, itemIndex: number, haveSubgrants: boolean) {
    if (item.source == 'prime') {
      return this.primeGrantToRow(item.filing, itemIndex, haveSubgrants)
    } else {
      return this.subgrantToRow(item.filing, itemIndex)
    }
  }

  itemsToTableData(items) {
    let haveSubgrants = false
    for (const [itemIndex, item] of items.entries()) {
      if (item.source == 'sub') {
        haveSubgrants = true
        break
      }
    }
    this.setState({ haveSubgrants: haveSubgrants })

    function itemDate(item) {
      return item.source == 'prime'
        ? item.filing.action_date
        : item.filing.sub_action_date
    }

    // We also sort the items by date for consistency.
    items.sort(function (a, b) {
      const aDate = itemDate(a)
      const bDate = itemDate(b)
      return aDate == bDate ? 0 : aDate > bDate ? -1 : 1
    })

    const that = this
    return items.map(function (item, index) {
      return that.itemToRow(item, index, haveSubgrants)
    })
  }

  retrieveURLs(): { [key: string]: string } {
    return {
      prime: util.getRetrieveURL(
        this.primeURLBase,
        this.props.query,
        this.props.isAgency,
        this.props.isEntity,
        this.props.isTag
      ),
      sub: util.getRetrieveURL(
        this.subURLBase,
        this.props.query,
        this.props.isAgency,
        this.props.isEntity,
        this.props.isTag
      ),
    }
  }

  // TODO(Jack Poulson): See if this can be deleted without effect.
  closeModal() {
    // Grab the modal created by the FilingFeed and then click.
    document.getElementById(ui.modalCloseButtonId(this.label)).click()
  }

  render() {
    return (
      <FilingFeed
        tableName="us_central_grants"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURLs={this.retrieveURLs()}
        columns={
          this.state.haveSubgrants
            ? kColumnsWithSubgrants
            : kColumnsWithoutSubgrants
        }
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
