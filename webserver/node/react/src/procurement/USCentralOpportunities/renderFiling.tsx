import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  AccordionItemWithLogo,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

export class Award extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = ['notice_id', 'title']
    super(props)
    super.setEndpoint('/api/us/central/procurement/opportunity')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing

    function renderOffice() {
      const annotation = filing.annotation.contracting

      const titlePrefix = 'Contracting'
      const labelPrefix = 'contracting'
      const title = `${titlePrefix} Office: ${annotation.office.toUpperCase()}`
      const label = `${labelPrefix}Office`

      const logoEntry = annotation.agencyLogo ? (
        <a href={util.getEntityURL(annotation.agencyNorm)}>
          <img
            className="ti-association-list-entity-logo"
            src={annotation.agencyLogo}
          />
        </a>
      ) : (
        <span style={{ fontSize: '1.5rem' }}>
          {annotation.agencyStylized.toUpperCase()}
        </span>
      )

      const features = [
        <ui.TableKeyValue
          k="Department"
          v={util.coerceUpperCase(annotation.department)}
        />,
        util.coerceUpperCase(annotation.agency) !=
        util.coerceUpperCase(annotation.department) ? (
          <ui.TableKeyValue
            k="Agency"
            v={util.coerceUpperCase(annotation.agency)}
          />
        ) : undefined,
        <ui.TableKeyValue
          k="Office"
          v={util.coerceUpperCase(annotation.office)}
        />,
      ]

      return (
        <AccordionItemWithLogo
          label={label}
          title={title}
          features={features}
          logo={logoEntry}
        />
      )
    }

    function renderDescription() {
      if (!filing.description) {
        return undefined
      }

      // Try to introduce newlines where appropriate.
      const description = filing.description
        .replace(/\x95\s+(\S+)\s+/g, '<br/> &bull; $1 ')
        .replace(/\x95/g, '<br/> &bull; ')
        .replace(/\.\s\s+(\d+)\.\s\s\s\s*/g, '.<br/>  $1. ')
        .replace(/\s\s+(\d+)\.\s\s\s\s*/g, '<br/>  $1. ')
        .replace(/\s\s\s(\s+)/g, '<br/><br/>')
        .replace(/\s\s\s/g, '<br/>')

      const body = (
        <div
          style={{ fontSize: '1.3rem' }}
          dangerouslySetInnerHTML={{ __html: description }}
        ></div>
      )

      return (
        <AccordionItem label="description" title="Description" body={body} />
      )
    }

    function renderPlaceOfPerformance() {
      const title = 'Place of Performance'
      const label = 'place-of-performance'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Street Address',
        filing.pop_street_address
      )
      ui.includeTableKeyValue(features, 'City', filing.pop_city)
      ui.includeTableKeyValue(features, 'State', filing.pop_state)
      ui.includeTableKeyValue(features, 'Zipcode', filing.pop_zip)
      ui.includeTableKeyValue(features, 'Country', filing.pop_country)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderAward() {
      const title = 'Award'
      const label = 'award'

      let features = []

      ui.includeTableKeyValue(features, 'Awardee', filing.awardee)
      ui.includeTableKeyValue(
        features,
        'Award Date',
        util.dateWithoutTime(new Date(filing.award_date)),
        true,
        filing.award_date
      )
      ui.includeTableKeyValue(features, 'Award Amount', filing.award_amount)
      ui.includeTableKeyValue(features, 'Award Number', filing.award_number)

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={filing.annotation.awardee}
        />
      )
    }

    function renderContractingOffice() {
      const title = 'Contracting Office Location'
      const label = 'contracting-office-location'

      let features = []

      ui.includeTableKeyValue(features, 'City', filing.city)
      ui.includeTableKeyValue(features, 'State', filing.state)
      ui.includeTableKeyValue(features, 'Zipcode', filing.zip)
      ui.includeTableKeyValue(features, 'Country', filing.country_code)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderPrimaryContact() {
      const title = 'Primary Contact'
      const label = 'primary-contact'

      let features = []

      ui.includeTableKeyValue(features, 'Name', filing.primary_contact_name)
      ui.includeTableKeyValue(features, 'Title', filing.primary_contact_title)
      if (filing.primary_contact_email) {
        ui.includeTableKeyValue(
          features,
          'Email',
          <a href={`mailto:${filing.primary_contact_email}`}>
            <code className="text-dark">{filing.primary_contact_email}</code>
          </a>
        )
      }
      if (filing.primary_contact_phone) {
        ui.includeTableKeyValue(
          features,
          'Phone',
          <code className="text-dark">
            {util.formatPhoneNumber(filing.primary_contact_phone)}
          </code>
        )
      }
      if (filing.primary_contact_fax) {
        ui.includeTableKeyValue(
          features,
          'Fax',
          <code className="text-dark">
            {util.formatPhoneNumber(filing.primary_contact_fax)}
          </code>
        )
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderSecondaryContact() {
      const title = 'Secondary Contact'
      const label = 'secondary-contact'

      let features = []

      ui.includeTableKeyValue(features, 'Name', filing.secondary_contact_name)
      ui.includeTableKeyValue(features, 'Title', filing.secondary_contact_title)
      if (filing.secondary_contact_email) {
        ui.includeTableKeyValue(
          features,
          'Email',
          <a href={`mailto:${filing.secondary_contact_email}`}>
            <code className="text-dark">{filing.secondary_contact_email}</code>
          </a>
        )
      }
      if (filing.secondary_contact_phone) {
        ui.includeTableKeyValue(
          features,
          'Phone',
          <code className="text-dark">
            {util.formatPhoneNumber(filing.secondary_contact_phone)}
          </code>
        )
      }
      if (filing.secondary_contact_fax) {
        ui.includeTableKeyValue(
          features,
          'Fax',
          <code className="text-dark">
            {util.formatPhoneNumber(filing.secondary_contact_fax)}
          </code>
        )
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    const title =
      util.dateWithoutTime(new Date(filing.posted_date)) + ': ' + filing.title

    const header = (
      <p className="card-text">
        <div className="mb-2">
          {filing.link ? (
            <a className="btn btn-light me-2" href={filing.link} role="button">
              View Original
            </a>
          ) : undefined}
          {filing.additional_info_link ? (
            <a
              className="btn btn-light"
              href={filing.additional_info_link}
              role="button"
            >
              View Additional Info
            </a>
          ) : undefined}
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderOffice())
    accordionItems.push(renderAward())
    accordionItems.push(renderDescription())
    accordionItems.push(renderPlaceOfPerformance())
    accordionItems.push(renderContractingOffice())
    accordionItems.push(renderPrimaryContact())
    accordionItems.push(renderSecondaryContact())
    let accordion = (
      <Accordion
        id="us-central-opportunities-accordion"
        items={accordionItems}
      />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
