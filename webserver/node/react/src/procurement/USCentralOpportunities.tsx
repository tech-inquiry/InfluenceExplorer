import * as React from 'react'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USCentralOpportunities/renderFiling'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

export class USCentralOpportunities extends React.Component<any, any> {
  urlBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  title: any
  columns: any[]

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL + '/' + kProcurementPaths['US federal opportunities'].get

    this.maxDescriptionLength = 256
    this.tableID = 'ti-table-us-central-opportunities'
    this.title = (
      <div>
        <p className="ti-compact">
          <span>
            <img
              src="/logos/government_of_the_united_states.svg"
              className="dropdown-flag"
            />
            US Federal Solicitations
          </span>
        </p>
        <p className="ti-compact text-muted lh-1" style={{ fontSize: '80%' }}>
          <small>
            Tech Inquiry's mirror is based upon the data made available by
            SAM.gov through{' '}
            <a href="https://sam.gov/data-services/Contract%20Opportunities/datagov?privacy=Public">
              ContractOpportunitiesFullCSV.csv
            </a>
            . We highly recommend manually browsing each linked original
            solicitation on SAM.gov because their exported information is often
            missing links to PDFs. Further, SAM.gov does not export outdated
            versions of solicitation notices, which often provide crucial
            context. (Tech Inquiry is also left to guess how to format the
            description text.)
          </small>
        </p>
      </div>
    )

    this.closeModal = this.closeModal.bind(this)
    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : 'us-central-opportunities'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Posted Date' },
      { title: 'Solicitation' },
      { title: 'Title' },
      { title: 'Department' },
      { title: 'Description' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex, haveSubawards) {
    const maxTextLength = 200

    const postedDate = util.dateWithoutTimeLex(filing.posted_date)
    const solicitation = filing.solicitation_number
    const title = filing.title
    const department = filing.department
    const description = util.trimLabel(filing.description, maxTextLength)
    const entireFiling = JSON.stringify(filing)

    return [
      filingIndex,
      postedDate,
      solicitation,
      title,
      department,
      description,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  closeModal() {
    // Grab the modal created by the FilingFeed and then click.
    document.getElementById(ui.modalCloseButtonId(this.label)).click()
  }

  render() {
    return (
      <FilingFeed
        tableName="us_central_opportunities"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
