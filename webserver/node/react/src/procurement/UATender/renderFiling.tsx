import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function renderContractDates(filing) {
  const label = 'contract-dates'
  const title = 'Contract Dates'

  const data = filing.tender.data

  let features = []
  ui.includeTableKeyDate(features, 'Date', data.date)
  ui.includeTableKeyDate(features, 'Date Created', filing.date_created)
  ui.includeTableKeyDate(features, 'Date Modified', data.dateModified)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderContractActionDetails(filing) {
  const label = 'contract-action-details'
  const title = 'Contract Action Details'

  const data = filing.tender.data

  let features = []

  ui.includeTableKeyValue(features, 'Owner', data.owner)

  ui.includeTableKeyValue(features, 'Status', data.status)

  ui.includeTableKeyValue(features, 'Award criteria', data.awardCriteria)

  ui.includeTableKeyValue(
    features,
    'Procurement method',
    data.procurementMethod
  )

  ui.includeTableKeyValue(
    features,
    'Procurement method type',
    data.procurementMethodType
  )

  ui.includeTableKeyValue(features, 'Tender ID', data.tenderID)

  if (data.auctionUrl) {
    ui.includeTableKeyValue(
      features,
      'Auction URL',
      <a href={data.auctionUrl}>{util.simplifyURL(data.auctionUrl)}</a>
    )
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderContractDescription(filing) {
  const label = 'contract-description'
  const title = 'Contract Description'

  let features = []

  ui.includeTableKeyValue(
    features,
    'Description',
    filing.tender.data.description
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderSuppliers(filing) {
  const label = 'suppliers'
  const title = 'Suppliers'

  if (!filing.suppliers.length) {
    return undefined
  }

  let features = []

  // TODO(Jack Poulson): Use annotations instead, mirroring AU approach.
  const suppliers = filing.suppliers.join('; ')
  ui.includeTableKeyValue(features, 'Suppliers', suppliers)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderBuyers(filing) {
  const label = 'buyers'
  const title = 'Buyers'

  if (!filing.buyers.length) {
    return undefined
  }

  let features = []

  // TODO(Jack Poulson): Use annotations instead, mirroring AU approach.
  const buyers = filing.buyers.join('; ')
  ui.includeTableKeyValue(features, 'Buyers', buyers)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

export class Award extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/ua/procurement/tender')
    super.setUniqueKeys(['uid'])
  }

  render() {
    const filing = this.state.filing
    const maxTitleLength = 200

    const data = filing.tender.data

    const title = util.trimLabel(data.title, maxTitleLength)
    const documentURL = `https://prozorro.gov.ua/tender/${data.tenderID}`
    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a className="btn btn-light me-2" href={documentURL} role="button">
            View original
          </a>
          {data.auctionUrl ? (
            <a className="btn btn-light" href={data.auctionUrl} role="button">
              View auction
            </a>
          ) : undefined}
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderSuppliers(filing))
    accordionItems.push(renderBuyers(filing))
    accordionItems.push(renderContractDates(filing))
    accordionItems.push(renderContractActionDetails(filing))
    accordionItems.push(renderContractDescription(filing))
    let accordion = (
      <Accordion id="ua-tender-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
