import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import * as util from '../utilities/util'

import { renderFiling } from './AU/renderFiling'

const kLabel = 'au-procurement'
const kTableID = `ti-table-${kLabel}`
const kGovLogo = '/logos/government_of_australia.svg'

const kColumns = [
  { title: 'Index', visible: false },
  { title: 'Last Update' },
  { title: 'Publication Date' },
  { title: 'ID' },
  { title: 'Vendor' },
  { title: 'Buyer' },
  { title: 'Description' },
  {
    title: 'Value',
    render: jQuery.fn.dataTable.render.number(',', '.', 2, 'AU$'),
  },
  { title: 'Entire Filing', visible: false },
]

export class AUProcurement extends React.Component<any, any> {
  urlBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)

    const direction = this.props.isAgency ? '(as agency)' : '(as vendor)'

    const titleString = `Australian procurement records ${direction}`
    this.title = (
      <span>
        <img src={kGovLogo} className="dropdown-flag" />
        {titleString}
      </span>
    )

    const source = `AU ${direction}`
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[source].get

    this.maxDescriptionLength = 256
    this.tableID = this.props.isAgency ? `${kTableID}-agency` : kTableID

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? `${kLabel}-agency`
      : kLabel

    this.columns = kColumns
  }

  itemToRow(item, itemIndex) {
    let date = ''
    if (item.date) {
      date = util.dateWithoutTimeLex(item.date)
    }
    if (item.award?.date) {
      const dateCand = util.dateWithoutTimeLex(item.award.date)
      if (dateCand > date) {
        date = dateCand
      }
    }

    const publishedDate = item.published_date
      ? util.dateWithoutTimeLex(item.published_date)
      : ''

    let id = ''
    if (item.contract) {
      if (item.contract.amendments) {
        id = item.contract.amendments[0].id
      } else {
        id = item.contract.id
      }
    } else if (item.award) {
      id = item.award.id.split('-')[0]
    }

    let vendor = ''
    for (const [i, annotation] of item.annotation.vendors.entries()) {
      if (!vendor && 'origText' in annotation) {
        if ('role' in item.parties[i] && item.parties[i].role == 'supplier') {
          vendor = annotation.origText
        }
      }
    }

    let buyer = ''
    for (const [i, annotation] of item.annotation.vendors.entries()) {
      if (!buyer && 'origText' in annotation) {
        if (
          'role' in item.parties[i] &&
          item.parties[i].role == 'procuringEntity'
        ) {
          buyer = annotation.origText
        }
      }
    }

    const description = util.trimLabel(
      util.coerce(item.contract?.description, ''),
      this.maxDescriptionLength
    )

    let value = ''
    if (item.contract) {
      if (item.contract.minValue) {
        value = item.contract.minValue.amount
      }
      if (item.contract.value) {
        if (
          !value ||
          parseFloat(item.contract.value.amount) > parseFloat(value)
        ) {
          value = item.contract.value.amount
        }
      }
    }

    const entireItem = JSON.stringify(item)
    return [
      itemIndex,
      date,
      publishedDate,
      id,
      vendor,
      buyer,
      description,
      value,
      entireItem,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="au_contract_filing"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
