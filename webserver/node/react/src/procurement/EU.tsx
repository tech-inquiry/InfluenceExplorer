import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { appConfig } from '../utilities/constants'
import { kProcurementPaths } from '../utilities/api'
import * as util from '../utilities/util'

import * as legacy from './EU/legacy'
import * as modern from './EU/modern'
import * as euUtil from './EU/util'

import { Filing } from './EU/renderFiling'

type Dict = { [key: string]: any }

export class EUProcurement extends React.Component<any, any> {
  source: string
  urlBase: string
  maxTitleLength: number
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    const direction = this.props.isAgency ? ' (as agency)' : ''
    this.source = `EU ${this.props.countryCode.toUpperCase()}${direction}`
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[this.source].get

    this.maxTitleLength = 200
    this.maxDescriptionLength = 256
    this.tableID = this.props.isAgency
      ? `ti-table-eu-${this.props.countryCode}-procurement-agency`
      : `ti-table-eu-${this.props.countryCode}-procurement`
    this.title = (
      <span>
        <img
          src={kProcurementPaths[this.source].logo}
          className="dropdown-flag"
        />
        {`${this.source} Procurement Records`}
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.legacyItemToRow = this.legacyItemToRow.bind(this)
    this.modernItemToRow = this.modernItemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? `${this.source}-procurement-agency`
      : `${this.source}-procurement`

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Action Date' },
      { title: 'ID' },
      { title: 'CPV' },
      {
        title: 'Max Value',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, ''),
      },
      { title: 'Num Awards' },
      { title: 'Contract Title' },
      { title: 'Short Description' },
      { title: 'Contracting Authorities' },
      { title: 'Contractors' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  async componentDidMount() {
    await modern.ensureCpvMap()
  }

  legacyItemToRow(filing, filingIndex: number) {
    const actionDate = legacy.getActionDate(filing)
    const actionDateStr = util.coerce(actionDate, '')

    const cpv = legacy.getCPV(filing)
    const cpvStr = cpv ? util.trimLabel(cpv, this.maxDescriptionLength) : ''

    const maxValue = legacy.getMaxValue(filing)

    const numContractAwards = legacy.getNumContractAwards(filing)

    const contractTitleStr = euUtil.serializeLanguageDict(
      legacy.getContractTitle(filing)
    )

    const shortDescrDict = legacy.getContractShortDescription(filing)
    for (const key in shortDescrDict) {
      shortDescrDict[key] = util.trimLabel(
        shortDescrDict[key],
        this.maxDescriptionLength
      )
    }
    const shortDescrStr = euUtil.serializeLanguageDict(shortDescrDict)

    const buyersStr = legacy.getUniqueBuyers(filing).join('; ')

    const contractorsStr = legacy.getContractorsAndOperators(filing).join('; ')

    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      actionDateStr,
      filing.doc_id,
      cpvStr,
      maxValue.amount,
      numContractAwards,
      contractTitleStr,
      shortDescrStr,
      buyersStr,
      contractorsStr,
      entireFiling,
    ]
  }

  modernItemToRow(filing, filingIndex: number) {
    const actionDate = modern.getActionDate(filing)
    const actionDateStr = util.coerce(actionDate, '')

    const cpv = modern.getCPV(filing)
    const cpvStr = cpv ? util.trimLabel(cpv, this.maxDescriptionLength) : ''

    const maxValue = modern.getMaxValue(filing)

    const numContractAwards = modern.getNumContractAwards(filing)

    const contractTitleStr = euUtil.serializeLanguageDict(
      modern.getContractTitle(filing)
    )

    const shortDescrDict = modern.getContractShortDescription(filing)
    for (const key in shortDescrDict) {
      shortDescrDict[key] = util.trimLabel(
        shortDescrDict[key],
        this.maxDescriptionLength
      )
    }
    const shortDescrStr = euUtil.serializeLanguageDict(shortDescrDict)

    const buyersStr = modern.getBuyers(filing).join('; ')

    const contractorsStr = modern.getContractors(filing).join('; ')

    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      actionDateStr,
      filing.doc_id,
      cpvStr,
      maxValue.amount,
      numContractAwards,
      contractTitleStr,
      shortDescrStr,
      buyersStr,
      contractorsStr,
      entireFiling,
    ]
  }

  itemToRow(filing, filingIndex: number) {
    return filing['is_legacy']
      ? this.legacyItemToRow(filing, filingIndex)
      : this.modernItemToRow(filing, filingIndex)
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    const countryCode = this.props.countryCode

    function renderFilingWrapper(filing: Dict) {
      return <Filing filing={filing} countryCode={countryCode} />
    }

    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFilingWrapper}
      />
    )
  }
}
