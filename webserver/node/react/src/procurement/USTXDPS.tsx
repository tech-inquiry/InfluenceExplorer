import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USTXDPS/renderFiling'

import * as util from '../utilities/util'

export class USTXDPSProcurement extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    const source = this.props.isAgency
      ? 'US TX DPS (as agency)'
      : 'US TX DPS (as vendor)'
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[source].get

    this.tableID = this.props.isAgency
      ? 'ti-table-us-tx-dps-agency-procurement'
      : 'ti-table-us-tx-dps-procurement'
    this.title = (
      <span>
        <img
          src="/logos/texas_department_of_public_safety.png"
          className="dropdown-flag"
        />
        Texas Public Safety Procurement{' '}
        {this.props.isAgency ? '(as agency)' : '(as vendor)'}
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? 'us-tx-dps-agency-procurement'
      : 'us-tx-dps-procurement'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Start Date' },
      { title: 'End Date' },
      { title: 'Vendor' },
      { title: 'Description' },
      { title: 'Contract ID' },
      {
        title: 'Contract Maximum',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
      },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    const startDate = filing.start_date
      ? util.dateWithoutTimeLex(filing.start_date)
      : ''
    const endDate = filing.end_date
      ? util.dateWithoutTimeLex(filing.end_date)
      : ''
    const vendor = util.coerce(filing.supplier, '')
    const contractID = util.coerce(filing.contract, '').replace(/^0+/, '')
    const contractMaximum = util.coerce(filing.contract_maximum, '')

    const description = util.coerce(filing.description, '')

    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      startDate,
      endDate,
      vendor,
      description,
      contractID,
      contractMaximum,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
