import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function renderVendor(filing) {
  const label = 'vendor'

  const annotation = filing.annotation.vendor
  const title = `Vendor: ${annotation.origText.toUpperCase()}`

  const addressObj = {
    addressLine: [filing.supplier_address_line],
    city: filing.supplier_address_city,
    state: filing.supplier_address_province,
    postalCode: filing.supplier_address_postal_code,
    country: filing.supplier_address_country,
  }

  const legalName = util.canonicalText(filing.supplier_legal_name)
  const standardName = util.canonicalText(filing.supplier_standardized_name)
  const operatingName = util.canonicalText(filing.supplier_operating_name)

  const features = [
    <ui.TableKeyValue k="Vendor Legal Name" v={filing.supplier_legal_name} />,
    standardName && standardName != legalName ? (
      <ui.TableKeyValue
        k="Vendor Standardized Name"
        v={util.coerceUpperCase(standardName)}
      />
    ) : undefined,
    operatingName && operatingName != legalName ? (
      <ui.TableKeyValue
        k="Vendor Operating Name"
        v={util.coerceUpperCase(operatingName)}
      />
    ) : undefined,
    <ui.Address address={addressObj} />,
    filing.supplier_employee_count != 'UNKNOWN' ? (
      <ui.TableKeyValue k="Employee Count" v={filing.supplier_employee_count} />
    ) : undefined,
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderContractingEntity(filing) {
  const label = 'contracting-entity'
  const title = `Contracting Entity: ${filing.contracting_entity_name.toUpperCase()}`

  const annotation = filing.annotation.contractingAgency

  let addressObj = {
    streetLines: [filing.contracting_address_line],
    city: filing.contracting_address_city,
    state: filing.contracting_address_province,
    postalCode: filing.contracting_address_postal_code,
    country: filing.contracting_address_country,
  }

  const features = [
    <ui.TableKeyValue
      k="Contracting Entity"
      v={filing.contracting_entity_name.toUpperCase()}
    />,
    <ui.Address address={addressObj} />,
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderEndUser(filing) {
  const label = 'end-user'
  const title = `End user: ${filing.end_user_entities_name.toUpperCase()}`

  const annotation = filing.annotation.endUserAgency

  let addressObj = {
    streetLines: [filing.end_user_entities_address],
  }

  const features = [
    <ui.TableKeyValue
      k="Name"
      v={filing.end_user_entities_name.toUpperCase()}
    />,
    <ui.TableKeyValue
      k="Office name"
      v={filing.end_user_entities_office_name.toUpperCase()}
    />,
    <ui.Address address={addressObj} />,
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderContactInfo(filing) {
  const label = 'contactInfo'
  const title = 'Contact Info'

  let features = []

  ui.includeTableKeyValue(features, 'Name', filing.contact_info_name)

  const addressObj = {
    addressLine: [filing.contact_info_address_line],
    city: filing.contact_info_city,
    state: filing.contact_info_province,
    postalCode: filing.contact_info_postal_code,
    country: filing.contact_info_country,
    phone: filing.contact_info_phone,
    fax: filing.contact_info_fax,
    email: filing.contact_info_email,
  }

  features.push(<ui.Address address={addressObj} />)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderMiscDetails(filing) {
  const label = 'contractMiscDetails'
  const title = 'Contract Misc. Details'

  let features = []

  ui.includeTableKeyValue(features, 'Reference Number', filing.reference_number)
  ui.includeTableKeyValue(features, 'Amendment Number', filing.amendment_number)
  ui.includeTableKeyValue(
    features,
    'Procurement Number',
    filing.procurement_number
  )
  ui.includeTableKeyValue(
    features,
    'Solicitation Number',
    filing.solicitation_number
  )
  ui.includeTableKeyValue(features, 'Contract Number', filing.contract_number)

  ui.includeTableKeyValue(features, 'Contract Status', filing.contract_status)

  ui.includeTableKeyValue(
    features,
    'Procurement Category',
    filing.procurement_category
  )
  ui.includeTableKeyValue(
    features,
    'Limited Tender Reason',
    filing.limited_tendering_reason
  )
  ui.includeTableKeyValue(
    features,
    'Selection Criteria',
    filing.selection_criteria
  )
  ui.includeTableKeyValue(features, 'Trade Agreements', filing.trade_agreements)

  ui.includeTableKeyValue(
    features,
    'Regions of Delivery',
    filing.regions_of_delivery
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderValues(filing) {
  const label = 'contractValues'
  const title = 'Contract Values'

  let features = []

  ui.includeTableKeyValue(
    features,
    'Contract Amount',
    util.numberWithCommas(filing.contract_amount) + ' CAD',
    true,
    filing.contract_amount
  )
  ui.includeTableKeyValue(
    features,
    'Total Contract Value',
    util.numberWithCommas(filing.total_contract_value) + ' CAD',
    true,
    filing.total_contract_value
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderDates(filing) {
  const label = 'dates'
  const title = 'Dates'

  let features = []

  ui.includeTableKeyDate(features, 'Award Date', filing.contract_award_date)
  ui.includeTableKeyDate(features, 'Amendment Date', filing.amendment_date)
  ui.includeTableKeyDate(features, 'Contract Start', filing.contract_start_date)
  ui.includeTableKeyDate(features, 'Contract End', filing.contract_end_date)
  ui.includeTableKeyDate(features, 'Published', filing.publication_date)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderDescription(filing) {
  const label = 'description'
  const title = 'Description'

  let features = []

  ui.includeTableKeyValue(
    features,
    'Contract Description',
    filing.gsin_description
  )

  ui.includeTableKeyValue(
    features,
    'UNSPSC Description',
    filing.unspsc_description
  )

  ui.includeTableKeyValue(
    features,
    'Tender Description',
    filing.tender_description
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

export class Award extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = [
      'reference_number',
      'amendment_number',
      'procurement_number',
      'solicitation_number',
      'contract_number',
    ]
    super(props)
    super.setEndpoint('/api/ca/procurement/canadaBuys')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing
    const maxTitleLength = 200

    const title = util.trimLabel(filing.title, maxTitleLength)

    const canadaBuysURL =
      'https://open.canada.ca/data/en/dataset/4fe645a1-ffcd-40c1-9385-2c771be956a4'
    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a className="btn btn-light me-2" href={canadaBuysURL} role="button">
            Full Dataset
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderVendor(filing))
    accordionItems.push(renderEndUser(filing))
    accordionItems.push(renderContractingEntity(filing))
    accordionItems.push(renderContactInfo(filing))
    accordionItems.push(renderMiscDetails(filing))
    accordionItems.push(renderValues(filing))
    accordionItems.push(renderDates(filing))
    accordionItems.push(renderDescription(filing))
    let accordion = (
      <Accordion id="au-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
