import * as React from 'react'

import {
  Accordion,
  ItemWithAnnotation,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function renderAddress(address) {
  if (!address) {
    return undefined
  }
  return (
    <ui.Address
      address={{
        streetLines: [address.streetAddress],
        city: address.locality,
        postalCode: address.postalCode,
        country: address.countryName,
      }}
    />
  )
}

function renderContactPoint(contact) {
  if (!contact) {
    return undefined
  }
  const item = (
    <div>
      {contact.name ? <p className="ti-compact">{contact.name}</p> : undefined}
      {contact.email ? (
        <p className="ti-compact">
          Email: <a href="mailto:{contact.email}">{contact.email}</a>
        </p>
      ) : undefined}
      {contact.telephone ? (
        <p className="ti-compact">
          Telephone: {util.formatPhoneNumber(contact.telephone)}
        </p>
      ) : undefined}
      {contact.uri ? (
        <p className="ti-compact">
          <a href={contact.uri}>{contact.uri}</a>
        </p>
      ) : undefined}
    </div>
  )
  return <ui.TableKeyValue k="Contact Point" v={item} />
}

function documentURLFromUUID(uuid: string): string {
  return `https://www.contractsfinder.service.gov.uk/notice/${uuid}`
}

function uuidFromJSONURL(url: string): string {
  const urlChunks = url.split('/')
  const filename = urlChunks[urlChunks.length - 1]

  // Remove the '.json' from the end
  return filename.substr(0, filename.length - 5)
}

function documentURLFromJSONURL(jsonURL: string): string {
  const uuid = uuidFromJSONURL(jsonURL)
  return documentURLFromUUID(uuid)
}

function renderIdentifier(party, annotation) {
  if (!party.identifier && !party.id) {
    return undefined
  }
  if (party.identifier) {
    const identifier = party.identifier
    let innerItem = undefined
    const label = `${identifier.scheme}: ${identifier.id}`
    if (annotation.companyHouseURL) {
      innerItem = <a href={annotation.companyHouseURL}>{label}</a>
    } else if (identifier.scheme) {
      innerItem = label
    } else {
      return undefined
    }
    const item = <div>{innerItem}</div>
    return <ui.TableKeyValue k="Extra ID" v={item} />
  } else {
    return <ui.TableKeyValue k="ID" v={party.id} />
  }
}

function renderDetails(party, annotation) {
  if (!party.details) {
    return undefined
  }
  const details = party.details
  let features = []
  if (details.url) {
    features.push(
      <ui.TableKeyValue
        k="URL"
        v={<a href={util.prefixURL(details.url)}>{details.url}</a>}
      />
    )
  }
  if (details.scale) {
    features.push(
      <ui.TableKeyValue k="Scale" v={util.coerceUpperCase(details.scale)} />
    )
  }
  return features
}

function renderParty(party, annotation) {
  const features = [
    <ui.TableKeyValue k="Name" v={util.coerceUpperCase(annotation.origText)} />,
    renderIdentifier(party, annotation),
    renderDetails(party, annotation),
    renderAddress(party.address),
    renderContactPoint(party.contactPoint),
  ]

  return <ItemWithAnnotation features={features} annotation={annotation} />
}

function renderSuppliers(filing) {
  const label = 'suppliers'
  const title = 'Suppliers'

  if (!(filing.award && filing.award.suppliers)) {
    return undefined
  }

  let features = []
  for (const supplier of filing.award.suppliers) {
    const canonicalName = util.canonicalText(supplier.name)
    if (filing.annotation.parties.hasOwnProperty(canonicalName)) {
      // See if we can find the full 'party' listing.
      let party = supplier
      if (filing.parties != undefined) {
        for (const candidate of filing.parties) {
          if (util.canonicalText(candidate.name) == canonicalName) {
            party = candidate
            break
          }
        }
      }
      features.push(
        renderParty(party, filing.annotation.parties[canonicalName])
      )
    }
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderOtherParties(filing) {
  const label = 'other-parties'
  const title = 'Other Parties'

  if (!filing.parties) {
    return undefined
  }

  let supplierNames = new Set<string>()
  if (filing.award && filing.award.suppliers) {
    for (const supplier of filing.award.suppliers) {
      const canonicalName = util.canonicalText(supplier.name)
      supplierNames.add(canonicalName)
    }
  }

  let features = []
  for (const party of filing.parties) {
    const canonicalName = util.canonicalText(party.name)
    if (
      supplierNames.has(canonicalName) ||
      canonicalName == util.canonicalText(filing.annotation.buyer.origText)
    ) {
      continue
    }
    if (filing.annotation.parties.hasOwnProperty(canonicalName)) {
      features.push(
        renderParty(party, filing.annotation.parties[canonicalName])
      )
    }
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderBuyer(filing) {
  if (!filing.buyer) {
    return undefined
  }
  const label = 'buyer'
  const title = 'Government Buyer'

  const annotation = filing.annotation.buyer

  let features = []
  const canonicalName = util.canonicalText(annotation.origText)
  if (filing.annotation.parties.hasOwnProperty(canonicalName)) {
    // See if we can find the full 'party' listing.
    let party = filing.buyer
    if (filing.parties != undefined) {
      for (const candidate of filing.parties) {
        if (util.canonicalText(candidate.name) == canonicalName) {
          party = candidate
          break
        }
      }
    }

    features.push(renderParty(party, annotation))
    return (
      <SimpleAccordionItem
        key={title}
        label={label}
        title={title}
        features={features}
      />
    )
  } else {
    features = [
      <ui.TableKeyValue
        k="Name"
        v={util.coerceUpperCase(annotation.origText)}
      />,
      renderAddress(filing.buyer.address),
      renderContactPoint(filing.buyer.contactPoint),
    ]
    return (
      <AccordionItemWithAnnotation
        key={title}
        label={label}
        title={title}
        features={features}
        annotation={annotation}
      />
    )
  }
}

function renderActionDetails(filing) {
  if (!(filing.tender || filing.award)) {
    return undefined
  }

  const label = 'actionDetails'
  const title = 'Contract Action Details'

  let features = []
  if (filing.award) {
    ui.includeTableKeyValue(features, 'Award ID', filing.award.id)
    ui.includeTableKeyValue(features, 'Award Status', filing.award.status)
  }
  if (filing.tender) {
    ui.includeTableKeyValue(features, 'Tender ID', filing.tender.id)
    ui.includeTableKeyValue(features, 'Tender Status', filing.tender.status)
    ui.includeTableKeyValue(
      features,
      'Procurement Method',
      filing.tender.procurementMethod
    )
    ui.includeTableKeyValue(
      features,
      'Method Details',
      filing.tender.procurementMethodDetails
    )
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderDates(filing) {
  const label = 'dates'
  const title = 'Contract Dates'

  function periodString(period) {
    if (!period) {
      return ''
    }
    let str = ''
    if (period.startDate) {
      str = util.dateWithoutTime(new Date(period.startDate))
    }
    str += ' -- '
    if (period.endDate) {
      str += util.dateWithoutTime(new Date(period.endDate))
    }
    return str
  }

  function milestoneKey(index) {
    return `Milestone ${index + 1}`
  }

  function milestoneValue(item) {
    return (
      <div>
        {item.description ? (
          <p className="ti-compact">{item.description}</p>
        ) : undefined}
        {item.dueDate
          ? util.dateWithoutTime(new Date(item.dueDate))
          : undefined}
      </div>
    )
  }

  let features = []
  if (filing.award) {
    ui.includeTableKeyDate(features, 'Award Date', filing.award.date)
    if (filing.award.contractPeriod) {
      ui.includeTableKeyValue(
        features,
        'Award Period',
        periodString(filing.award.contractPeriod),
        true,
        filing.award.contractPeriod
      )
    }
  }
  if (filing.tender) {
    if (filing.tender.tenderPeriod) {
      ui.includeTableKeyValue(
        features,
        'Tender Period',
        periodString(filing.tender.tenderPeriod),
        true,
        filing.tender.tenderPeriod
      )
    }
    if (filing.tender.milestones) {
      for (const [i, milestone] of filing.tender.milestones.entries()) {
        ui.includeTableKeyValue(
          features,
          milestoneKey(i),
          milestoneValue(milestone)
        )
      }
    }
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderContractAmounts(filing) {
  const label = 'amounts'
  const title = 'Contract Amounts'

  let features = []
  function includeAmountWithCurrency(k, v) {
    if (v) {
      features.push(
        <ui.TableKeyValue
          k={k}
          v={util.amountWithCurrency(v.amount, v.currency)}
        />
      )
    }
  }

  if (filing.tender) {
    includeAmountWithCurrency('Tender Minimum Value', filing.tender.minValue)
    includeAmountWithCurrency('Tender Value', filing.tender.value)
  }
  if (filing.award) {
    includeAmountWithCurrency('Award Value', filing.award.value)
  }

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderDescription(filing) {
  const label = 'description'
  const title = 'Description'

  let features = []

  function includeClassification(classification, index) {
    if (!classification) {
      return
    }
    const v = (
      <div>
        {classification.scheme ? (
          <p className="ti-compact">Scheme: {classification.scheme}</p>
        ) : undefined}
        {classification.id ? (
          <p className="ti-compact">ID: {classification.id}</p>
        ) : undefined}
        {classification.description ? (
          classification.uri ? (
            <a href={classification.uri}>{classification.description}</a>
          ) : (
            classification.description
          )
        ) : undefined}
      </div>
    )
    features.push(<ui.TableKeyValue k={`Item ${index + 1}`} v={v} />)
  }

  if (filing.tender) {
    ui.includeTableKeyValue(features, 'Tender Title', filing.tender.title)
    ui.includeTableKeyValue(
      features,
      'Tender Description',
      filing.tender.description
    )

    if (filing.tender.items) {
      for (const [i, item] of filing.tender.items.entries()) {
        includeClassification(item.classification, i)
      }
    }
  }

  ui.includeTableKeyValue(features, 'Comment', filing.comment)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

export class Award extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/uk/procurement')
    super.setUniqueKeys(['uri'])
  }

  render() {
    const filing = this.state.filing
    const maxTitleLength = 200

    let title = filing.tender.description
      ? util.trimLabel(
          `${filing.tender.title}: ${filing.tender.description}`,
          maxTitleLength
        )
      : filing.tender.title
    let header = undefined
    if (filing.uri) {
      const jsonURL = filing.uri
      const documentURL = documentURLFromJSONURL(jsonURL)
      header = (
        <p className="card-text">
          <a className="btn btn-light me-2" href={documentURL} role="button">
            View Original
          </a>{' '}
          <a className="btn btn-light" href={jsonURL} role="button">
            View JSON
          </a>
          {super.getTagDisplay()}
          {super.getTagForm()}
        </p>
      )
    }

    let accordionItems = []
    accordionItems.push(renderSuppliers(filing))
    accordionItems.push(renderBuyer(filing))
    accordionItems.push(renderOtherParties(filing))
    accordionItems.push(renderActionDetails(filing))
    accordionItems.push(renderContractAmounts(filing))
    accordionItems.push(renderDates(filing))
    accordionItems.push(renderDescription(filing))
    let accordion = (
      <Accordion id="uk-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
