import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import * as util from '../utilities/util'

import { renderFiling } from './NZ/renderFiling'

export class NZProcurement extends React.Component<any, any> {
  urlBase: string
  maxTitleLength: number
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    const source = `NZ`
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[source].get

    this.maxTitleLength = 200
    this.maxDescriptionLength = 256
    this.tableID = 'ti-table-nz-procurement'
    this.title = (
      <span>
        <img
          src="/logos/government_of_new_zealand.svg"
          className="dropdown-flag"
        />
        New Zealand Procurement Records
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label ? this.props.label : 'nz-procurement'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Award Date' },
      { title: 'Open Date' },
      { title: 'Close Date' },
      { title: 'Notice Type' },
      { title: 'ID' },
      { title: 'Posting Agency' },
      { title: 'Title' },
      { title: 'Description' },
      {
        title: 'Amount',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, 'NZ$'),
      },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(item, itemIndex) {
    const awardDate = item.awarded_date
      ? util.dateWithoutTimeLex(item.awarded_date)
      : ''
    const openDate = item.open_date
      ? util.dateWithoutTimeLex(item.open_date)
      : ''
    const closeDate = item.close_date
      ? util.dateWithoutTimeLex(item.close_date)
      : ''
    const noticeType = item.notice_type
    const id = item.notice_id
    const postingAgency = item.posting_agency
    const title = item.title
    const description = util.coerce(
      util.trimLabel(item.overview, this.maxDescriptionLength),
      ''
    )
    const amount = util.coerce(item.awarded_amount, '')

    const entireItem = JSON.stringify(item)
    return [
      itemIndex,
      awardDate,
      openDate,
      closeDate,
      noticeType,
      id,
      postingAgency,
      title,
      description,
      amount,
      entireItem,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
