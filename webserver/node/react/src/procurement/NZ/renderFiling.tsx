import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function renderProcuringAgency(filing) {
  const label = 'posting-agency'
  const title = 'Posting Agency'

  let features = []

  ui.includeTableKeyValue(features, 'Posting Agency', filing.posting_agency)
  ui.includeTableKeyValue(features, 'Department', filing.department)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderContractActionDetails(filing) {
  const label = 'contract-action-details'
  const title = 'Contract Action Details'

  let features = []

  ui.includeTableKeyValue(features, 'Notice ID', filing.notice_id)
  ui.includeTableKeyValue(features, 'Notice Type', filing.notice_type)
  ui.includeTableKeyValue(features, 'Tender Coverage', filing.tender_coverage)
  ui.includeTableKeyValue(
    features,
    'Tender Prequalification',
    filing.prequalification
  )
  ui.includeTableKeyValue(features, 'Award Type', filing.award_type)
  ui.includeTableKeyValue(features, 'Competition Type', filing.competition_type)
  ui.includeTableKeyValue(features, 'Reference Number', filing.reference_number)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderContractDates(filing) {
  const label = 'contract-dates'
  const title = 'Contract Dates'

  let features = []

  ui.includeTableKeyDate(features, 'Open Date', filing.open_date)
  ui.includeTableKeyDate(features, 'Close Date', filing.close_date)
  ui.includeTableKeyDate(features, 'Awarded Date', filing.awarded_date)
  ui.includeTableKeyDate(features, 'Report Date', filing.report_date)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderContractAmounts(filing) {
  const label = 'contract-amounts'
  const title = 'Contract Amounts'

  let features = []

  // HERE

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderContractDescription(filing) {
  const label = 'contract-description'
  const title = 'Contract Description'

  let features = []

  ui.includeTableKeyValue(features, 'Title', filing.title)
  ui.includeTableKeyValue(features, 'Overview', filing.overview)
  ui.includeTableKeyValue(features, 'Comments', filing.comments)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

export class Award extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/nz/procurement')
    super.setUniqueKeys(['notice_id'])
  }

  render() {
    const filing = this.state.filing
    const title = filing.title

    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light"
            href="https://www.mbie.govt.nz/cross-government-functions/new-zealand-government-procurement-and-property/open-data/"
            role="button"
          >
            Full Dataset
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderProcuringAgency(filing))
    accordionItems.push(renderContractActionDetails(filing))
    accordionItems.push(renderContractDates(filing))
    accordionItems.push(renderContractAmounts(filing))
    accordionItems.push(renderContractDescription(filing))
    let accordion = (
      <Accordion id="nz-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Award filing={filing} />
}
