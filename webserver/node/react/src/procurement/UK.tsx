import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './UK/renderFiling'

import * as util from '../utilities/util'

export class UKProcurement extends React.Component<any, any> {
  urlBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    const direction = this.props.isAgency ? '(as agency)' : '(as vendor)'
    const source = `UK ${direction}`
    this.urlBase = `${appConfig.apiURL}/${kProcurementPaths[source].get}`

    this.maxDescriptionLength = 256
    this.tableID = this.props.isAgency
      ? 'ti-table-uk-procurement-agency'
      : 'ti-table-uk-procurement'
    this.title = (
      <span>
        <img
          src="/logos/government_of_the_united_kingdom.svg"
          className="dropdown-flag"
        />
        UK Procurement Records {direction}
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? 'uk-procurement-agency'
      : 'uk-procurement'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Publication Date' },
      { title: 'Parties' },
      { title: 'Buyer' },
      { title: 'Description' },
      {
        title: 'Max Value',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, '£'),
      },
      { title: 'Entire Filing', visible: false },
    ]
  }

  filingMaxValue(filing): number {
    let value = undefined
    if (filing?.award?.value) {
      value = parseFloat(filing.award.value.amount)
    }
    if (filing?.tender?.minValue) {
      if (!value || filing.tender.minValue.amount > value) {
        value = parseFloat(filing.tender.minValue.amount)
      }
    }
    if (filing?.tender?.value) {
      if (!value || filing.tender.value.amount > value) {
        value = parseFloat(filing.tender.value.amount)
      }
    }
    return value
  }

  itemToRow(filing, filingIndex: number) {
    const publishedDate = filing.published_date
      ? util.dateWithoutTimeLex(filing.published_date)
      : ''

    let parties = []
    for (const key in filing.annotation.parties) {
      const annotation = filing.annotation.parties[key]
      if ('origText' in annotation) {
        parties.push(annotation.origText)
      }
    }
    const partiesStr = util.trimLabel(
      parties.join(', '),
      this.maxDescriptionLength
    )

    const buyer = util.coerce(filing.buyer?.name, '')

    const description = util.trimLabel(
      util.coerce(filing.tender?.description, ''),
      this.maxDescriptionLength
    )

    let maxValue = this.filingMaxValue(filing)
    if (!maxValue) {
      maxValue = 0
    }

    const entireFiling = JSON.stringify(filing)

    return [
      filingIndex,
      publishedDate,
      partiesStr,
      buyer,
      description,
      maxValue,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="uk_contract_filing"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
