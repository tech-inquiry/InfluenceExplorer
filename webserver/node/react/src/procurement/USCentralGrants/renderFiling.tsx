import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  AccordionItemWithLogo,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function usaSpendingAwardURL(uniqueKey) {
  const awardBase = 'https://www.usaspending.gov/award/'
  return `${awardBase}${uniqueKey}`
}

export class PrimeGrant extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/us/central/grant')
    super.setUniqueKeys(['unique_key'])
  }

  render() {
    const filing = this.state.filing

    function pushIfNonEmpty(array, item) {
      if (item) {
        array.push(item)
      }
    }

    function placeOfPerformanceLines() {
      let lines = []

      let cityLines = []
      pushIfNonEmpty(cityLines, filing.pop_city_name)
      pushIfNonEmpty(cityLines, filing.pop_state_name)
      pushIfNonEmpty(cityLines, filing.pop_zip_4)
      pushIfNonEmpty(cityLines, filing.pop_county_name)
      pushIfNonEmpty(cityLines, filing.pop_country_name)
      pushIfNonEmpty(lines, cityLines.join(', '))

      return lines.map((line) => <p className="ti-compact">{line.trim()}</p>)
    }

    function recipientLines() {
      let lines = []
      pushIfNonEmpty(lines, filing.recipient_address_line_1)
      pushIfNonEmpty(lines, filing.recipient_address_line_2)

      let cityLines = []
      pushIfNonEmpty(
        cityLines,
        util.coerce(
          filing.recipient_foreign_city_name,
          filing.recipient_city_name
        )
      )
      pushIfNonEmpty(
        cityLines,
        util.coerce(
          filing.recipient_foreign_province_name,
          filing.recipient_state_name
        )
      )
      pushIfNonEmpty(
        cityLines,
        util.coerce(
          filing.recipient_foreign_postal_code,
          filing.recipient_zip_code
        )
      )
      pushIfNonEmpty(cityLines, filing.recipient_country_name)
      pushIfNonEmpty(lines, cityLines.join(', '))

      return lines.map((line) => <p className="ti-compact">{line.trim()}</p>)
    }

    function renderRecipient() {
      const label = 'recipient'
      const title = 'Recipient'

      let features = []

      const annotation = filing.annotation.recipient

      const differentNames =
        util.coerceUpperCase(annotation.origText) != filing.recipient_name

      if (differentNames) {
        ui.includeTableKeyValue(
          features,
          'Recipient Name',
          filing.recipient_name
        )
      }
      ui.includeTableKeyValue(
        features,
        differentNames ? 'Recipient Name (raw)' : 'Recipient Name',
        util.coerceUpperCase(annotation.origText)
      )
      ui.includeTableKeyValue(features, 'Recipient UEI', filing.recipient_uei)
      ui.includeTableKeyValue(features, 'Recipient DUNS', filing.recipient_duns)

      const recipientAddress = recipientLines()
      if (recipientAddress.length) {
        ui.includeTableKeyValue(features, 'Recipient Address', recipientAddress)
      }

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderRecipientParent() {
      const label = 'recipient-parent'
      const title = 'Recipient Parent'

      let features = []

      const annotation = filing.annotation.recipientParent
      if (!annotation.origText) {
        return undefined
      }

      const differentNames =
        util.coerceUpperCase(annotation.origText) !=
        filing.recipient_parent_name

      if (differentNames) {
        ui.includeTableKeyValue(
          features,
          'Recipient Parent Name',
          filing.recipient_parent_name
        )
      }
      ui.includeTableKeyValue(
        features,
        differentNames
          ? 'Recipient Parent Name (raw)'
          : 'Recipient Parent Name',
        util.coerceUpperCase(annotation.origText)
      )
      ui.includeTableKeyValue(
        features,
        'Recipient Parent UEI',
        filing.recipient_parent_uei
      )
      ui.includeTableKeyValue(
        features,
        'Recipient Parent DUNS',
        filing.recipient_parent_duns
      )

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderOffice(awarding = true) {
      const annotation = awarding
        ? filing.annotation.awarding
        : filing.annotation.funding

      const titlePrefix = awarding ? 'Awarding' : 'Funding'
      const labelPrefix = awarding ? 'awarding' : 'funding'
      const title = `${titlePrefix} Office: ${annotation.office.toUpperCase()}`
      const label = `${labelPrefix}Office`

      const logoEntry = annotation.agencyLogo ? (
        <a href={util.getEntityURL(annotation.agencyNorm)}>
          <img
            className="ti-association-list-entity-logo"
            src={annotation.agencyLogo}
          />
        </a>
      ) : (
        <span style={{ fontSize: '1.5rem' }}>
          {annotation.agencyStylized.toUpperCase()}
        </span>
      )

      const features = [
        <ui.TableKeyValue
          k="Department"
          v={util.coerceUpperCase(annotation.department)}
        />,
        <ui.TableKeyValue
          k="Department Code"
          v={
            awarding ? filing.awarding_agency_code : filing.funding_agency_code
          }
        />,
        util.coerceUpperCase(annotation.agency) !=
        util.coerceUpperCase(annotation.department) ? (
          <ui.TableKeyValue
            k="Agency"
            v={util.coerceUpperCase(annotation.agency)}
          />
        ) : undefined,
        util.coerceUpperCase(annotation.agency) !=
        util.coerceUpperCase(annotation.department) ? (
          <ui.TableKeyValue
            k="Agency Code"
            v={
              awarding
                ? filing.awarding_sub_agency_code
                : filing.funding_sub_agency_code
            }
          />
        ) : undefined,

        <ui.TableKeyValue
          k="Office"
          v={util.coerceUpperCase(annotation.office)}
        />,
        <ui.TableKeyValue
          k="Office Code"
          v={
            awarding ? filing.awarding_office_code : filing.funding_office_code
          }
        />,
      ]

      return (
        <AccordionItemWithLogo
          label={label}
          title={title}
          features={features}
          logo={logoEntry}
        />
      )
    }

    function renderAmounts() {
      const label = 'amounts'
      const title = 'Amounts'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Total Obligated',
        `${util.numberWithCommas(filing.total_obligated)} USD`,
        true,
        filing.total_obligated
      )

      ui.includeTableKeyValue(
        features,
        'Federal Obligation',
        `${util.numberWithCommas(filing.federal_obligation)} USD`,
        true,
        filing.federal_obligation
      )

      ui.includeTableKeyValue(
        features,
        'Pragmatic Obligations',
        `${util.numberWithCommas(filing.pragmatic_obligations)} USD`,
        true,
        filing.pragmatic_obligations
      )

      ui.includeTableKeyValue(
        features,
        'Non-federal Funding',
        `${util.numberWithCommas(filing.non_federal_funding)} USD`,
        true,
        filing.non_federal_funding
      )

      ui.includeTableKeyValue(
        features,
        'Total Non-federal Funding',
        `${util.numberWithCommas(filing.total_non_federal_funding)} USD`,
        true,
        filing.total_non_federal_funding
      )

      ui.includeTableKeyValue(
        features,
        'Indirect Cost Federal Share',
        `${util.numberWithCommas(filing.indirect_cost_federal_share)} USD`,
        true,
        filing.indirect_cost_federal_share
      )

      ui.includeTableKeyValue(
        features,
        'Total Outlayed',
        `${util.numberWithCommas(filing.total_outlayed)} USD`,
        true,
        filing.total_outlayed
      )

      ui.includeTableKeyValue(
        features,
        'Face Value of Loan',
        `${util.numberWithCommas(filing.face_value_of_loan)} USD`,
        true,
        filing.face_value_of_loan
      )

      ui.includeTableKeyValue(
        features,
        'Total Face Value of Loan',
        `${util.numberWithCommas(filing.total_face_value_of_loan)} USD`,
        true,
        filing.total_face_value_of_loan
      )

      ui.includeTableKeyValue(
        features,
        'Original Loan Subsidy Cost',
        `${util.numberWithCommas(filing.original_loan_subsidy_cost)} USD`,
        true,
        filing.original_loan_subsidy_cost
      )

      ui.includeTableKeyValue(
        features,
        'Total Loan Subsidy Cost',
        `${util.numberWithCommas(filing.total_loan_subsidy_cost)} USD`,
        true,
        filing.total_loan_subsidy_cost
      )

      ui.includeTableKeyValue(
        features,
        'COVID-19 Obligation',
        `${util.numberWithCommas(filing.covid_obligated)} USD`,
        true,
        filing.covid_obligated
      )

      ui.includeTableKeyValue(
        features,
        'COVID-19 Outlayed Amount',
        `${util.numberWithCommas(filing.covid_outlayed)} USD`,
        true,
        filing.covid_outlayed
      )

      ui.includeTableKeyValue(
        features,
        'IIJA Obligation',
        `${util.numberWithCommas(filing.iija_obligated)} USD`,
        true,
        filing.iija_obligated
      )

      ui.includeTableKeyValue(
        features,
        'IIJA Outlayed Amount',
        `${util.numberWithCommas(filing.iija_outlayed)} USD`,
        true,
        filing.iija_outlayed
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderDates() {
      const label = 'dates'
      const title = 'Dates'

      let features = []

      ui.includeTableKeyDate(
        features,
        'Action Date',
        util.dateWithoutTime(new Date(filing.action_date))
      )
      if (!filing.action_date) {
        ui.includeTableKeyValue(
          features,
          'Action Fiscal Year',
          filing.action_fiscal_year
        )
      }

      ui.includeTableKeyDate(
        features,
        'Start Date',
        util.dateWithoutTime(new Date(filing.start_date))
      )
      ui.includeTableKeyDate(
        features,
        'End Date',
        util.dateWithoutTime(new Date(filing.end_date))
      )

      ui.includeTableKeyDate(
        features,
        'Initial Report Date',
        util.dateWithoutTime(new Date(filing.initial_report_date))
      )
      ui.includeTableKeyDate(
        features,
        'Last Modified Date',
        util.dateWithoutTime(new Date(filing.last_modified_date))
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderActionDetails() {
      const label = 'action-details'
      const title = 'Action Details'

      let features = []

      ui.includeTableKeyValue(features, 'ID FAIN', filing.id_fain)
      ui.includeTableKeyValue(
        features,
        'Modification Number',
        filing.mod_number
      )
      ui.includeTableKeyValue(features, 'Unique Key', filing.unique_key)

      ui.includeTableKeyValue(
        features,
        'Funding Opportunity Number',
        filing.funding_opportunity_number
      )

      ui.includeTableKeyValue(
        features,
        'Funding Opportunity Goals',
        filing.funding_opportunity_goals
      )

      ui.includeTableKeyValue(
        features,
        'Emergency Fund Codes',
        filing.emergency_fund_codes
      )

      ui.includeTableKeyValue(
        features,
        'Assistance Type Description',
        filing.assistance_type_description
      )

      ui.includeTableKeyValue(
        features,
        'Funds Indicator Description',
        filing.funds_indicator_description
      )

      ui.includeTableKeyValue(
        features,
        'Business Types Description',
        filing.business_types_description
      )

      ui.includeTableKeyValue(
        features,
        'Action Type Description',
        filing.action_type_description
      )

      ui.includeTableKeyValue(
        features,
        'Record Type Description',
        filing.record_type_description
      )

      ui.includeTableKeyValue(
        features,
        'Treasury Accounts',
        filing.treasury_accounts
      )

      ui.includeTableKeyValue(
        features,
        'Federal Accounts',
        filing.federal_accounts
      )

      ui.includeTableKeyValue(features, 'Object Classes', filing.object_classes)

      ui.includeTableKeyValue(
        features,
        'Program Activities',
        filing.program_activities
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderDescription() {
      const label = 'description'
      const title = 'Description'

      let features = []

      ui.includeTableKeyValue(features, 'CFDA Title', filing.cfda_title)
      ui.includeTableKeyValue(
        features,
        'Transaction Description',
        filing.transaction_description
      )
      if (
        filing.transaction_description != filing.base_transaction_description
      ) {
        ui.includeTableKeyValue(
          features,
          'Base Transaction Description',
          filing.base_transaction_description
        )
      }

      const placeOfPerformance = placeOfPerformanceLines()
      if (placeOfPerformance.length) {
        ui.includeTableKeyValue(
          features,
          'Place of Performance',
          placeOfPerformance
        )
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderHighlyPaidOfficers() {
      const label = 'highly-paid-officers'
      const title = 'Highly Paid Officers'

      let features = []

      function includeOfficer(index) {
        const nameKey = `officer_${index}_name`
        const amountKey = `officer_${index}_amount`
        if (filing[nameKey]) {
          ui.includeTableKeyValue(
            features,
            `Officer ${index}`,
            `${filing[nameKey]}: ${util.numberWithCommas(
              filing[amountKey]
            )} USD`
          )
        }
      }

      for (let i = 1; i <= 5; i += 1) {
        includeOfficer(i)
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    let title = []
    if (filing.cfda_title) {
      title.push(<p className="ti-compact">{filing.cfda_title}</p>)
    }

    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light"
            href={usaSpendingAwardURL(filing.unique_key)}
            role="button"
          >
            View in USASpending
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderRecipient())
    accordionItems.push(renderRecipientParent())
    accordionItems.push(renderOffice(true))
    accordionItems.push(renderOffice(false))
    accordionItems.push(renderAmounts())
    accordionItems.push(renderDates())
    accordionItems.push(renderActionDetails())
    accordionItems.push(renderDescription())
    accordionItems.push(renderHighlyPaidOfficers())
    let accordion = (
      <Accordion id="au-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export class SubGrant extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = ['prime_unique_key', 'sub_number', 'sub_name']
    super(props)
    super.setEndpoint('/api/us/central/subgrant')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing

    function pushIfNonEmpty(array, item) {
      if (item) {
        array.push(item)
      }
    }

    function primePlaceOfPerformanceLines() {
      let lines = []

      let cityLines = []
      pushIfNonEmpty(cityLines, filing.prime_pop_city_name)
      pushIfNonEmpty(cityLines, filing.prime_pop_state_name)
      pushIfNonEmpty(cityLines, filing.prime_pop_zip_code)
      pushIfNonEmpty(cityLines, filing.prime_pop_county_name)
      pushIfNonEmpty(cityLines, filing.prime_pop_country_name)
      pushIfNonEmpty(lines, cityLines.join(', '))

      return lines.map((line) => <p className="ti-compact">{line.trim()}</p>)
    }

    function subPlaceOfPerformanceLines() {
      let lines = []

      let cityLines = []
      pushIfNonEmpty(cityLines, filing.sub_pop_city_name)
      pushIfNonEmpty(cityLines, filing.sub_pop_state_name)
      pushIfNonEmpty(cityLines, filing.sub_pop_zip_code)
      pushIfNonEmpty(cityLines, filing.sub_pop_country_name)
      pushIfNonEmpty(lines, cityLines.join(', '))

      return lines.map((line) => <p className="ti-compact">{line.trim()}</p>)
    }

    function primeRecipientLines() {
      let lines = []
      pushIfNonEmpty(lines, filing.recipient_address_line_1)
      pushIfNonEmpty(lines, filing.recipient_address_line_2)

      let cityLines = []
      pushIfNonEmpty(
        cityLines,
        util.coerce(
          filing.recipient_foreign_city_name,
          filing.recipient_city_name
        )
      )
      pushIfNonEmpty(
        cityLines,
        util.coerce(
          filing.recipient_foreign_province_name,
          filing.recipient_state_name
        )
      )
      pushIfNonEmpty(
        cityLines,
        util.coerce(
          filing.recipient_foreign_postal_code,
          filing.recipient_zip_code
        )
      )
      pushIfNonEmpty(cityLines, filing.recipient_country_name)
      pushIfNonEmpty(lines, cityLines.join(', '))

      return lines.map((line) => <p className="ti-compact">{line.trim()}</p>)
    }

    function renderPrimeRecipient() {
      const label = 'prime'
      const title = 'Prime Recipient'

      let features = []

      const annotation = filing.annotation.recipient

      const differentNames =
        util.coerceUpperCase(annotation.origText) != filing.recipient_name

      if (differentNames) {
        ui.includeTableKeyValue(
          features,
          'Recipient Name',
          filing.recipient_name
        )
      }
      ui.includeTableKeyValue(
        features,
        differentNames ? 'Recipient Name (raw)' : 'Recipient Name',
        util.coerceUpperCase(annotation.origText)
      )
      ui.includeTableKeyValue(features, 'Recipient UEI', filing.recipient_uei)
      ui.includeTableKeyValue(features, 'Recipient DUNS', filing.recipient_duns)

      const recipientAddress = primeRecipientLines()
      if (recipientAddress.length) {
        ui.includeTableKeyValue(
          features,
          'Prime Recipient Address',
          recipientAddress
        )
      }

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderPrimeRecipientParent() {
      const label = 'prime-parent'
      const title = 'Prime Recipient Parent'

      let features = []

      const annotation = filing.annotation.recipientParent
      if (!annotation.origText) {
        return undefined
      }

      const differentNames =
        util.coerceUpperCase(annotation.origText) !=
        filing.recipient_parent_name

      if (differentNames) {
        ui.includeTableKeyValue(
          features,
          'Recipient Parent Name',
          filing.recipient_parent_name
        )
      }
      ui.includeTableKeyValue(
        features,
        differentNames
          ? 'Recipient Parent Name (raw)'
          : 'Recipient Parent Name',
        util.coerceUpperCase(annotation.origText)
      )
      ui.includeTableKeyValue(
        features,
        'Recipient Parent UEI',
        filing.recipient_parent_uei
      )
      ui.includeTableKeyValue(
        features,
        'Recipient Parent DUNS',
        filing.recipient_parent_duns
      )

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderSubcontractor() {
      const label = 'subcontractor'
      const title = 'Subcontractor'

      let features = []

      const annotation = filing.annotation.subcontractor
      if (!annotation.origText) {
        return undefined
      }

      ui.includeTableKeyValue(
        features,
        'Name',
        util.coerceUpperCase(annotation.origText)
      )
      ui.includeTableKeyValue(features, 'UEI', filing.sub_uei)
      ui.includeTableKeyValue(features, 'DUNS', filing.sub_duns)
      ui.includeTableKeyValue(features, 'Parent Name', filing.sub_parent_name)
      ui.includeTableKeyValue(features, 'Parent UEI', filing.sub_parent_uei)
      ui.includeTableKeyValue(features, 'Parent DUNS', filing.sub_parent_duns)

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderOffice(awarding = true) {
      const annotation = awarding
        ? filing.annotation.awarding
        : filing.annotation.funding

      const titlePrefix = awarding ? 'Awarding' : 'Funding'
      const labelPrefix = awarding ? 'awarding' : 'funding'
      const title = `${titlePrefix} Office: ${annotation.office.toUpperCase()}`
      const label = `${labelPrefix}Office`

      const logoEntry = annotation.agencyLogo ? (
        <a href={util.getEntityURL(annotation.agencyNorm)}>
          <img
            className="ti-association-list-entity-logo"
            src={annotation.agencyLogo}
          />
        </a>
      ) : (
        <span style={{ fontSize: '1.5rem' }}>
          {annotation.agencyStylized.toUpperCase()}
        </span>
      )

      const features = [
        <ui.TableKeyValue
          k="Department"
          v={util.coerceUpperCase(annotation.department)}
        />,
        <ui.TableKeyValue
          k="Department Code"
          v={
            awarding
              ? filing.prime_awarding_agency_code
              : filing.prime_funding_agency_code
          }
        />,
        util.coerceUpperCase(annotation.agency) !=
        util.coerceUpperCase(annotation.department) ? (
          <ui.TableKeyValue
            k="Agency"
            v={util.coerceUpperCase(annotation.agency)}
          />
        ) : undefined,
        util.coerceUpperCase(annotation.agency) !=
        util.coerceUpperCase(annotation.department) ? (
          <ui.TableKeyValue
            k="Agency Code"
            v={
              awarding
                ? filing.prime_awarding_sub_agency_code
                : filing.prime_funding_sub_agency_code
            }
          />
        ) : undefined,
        <ui.TableKeyValue
          k="Office"
          v={util.coerceUpperCase(annotation.office)}
        />,
        <ui.TableKeyValue
          k="Office Code"
          v={
            awarding
              ? filing.prime_awarding_office_code
              : filing.prime_funding_office_code
          }
        />,
      ]

      return (
        <AccordionItemWithLogo
          label={label}
          title={title}
          features={features}
          logo={logoEntry}
        />
      )
    }

    function renderAmounts() {
      const label = 'amounts'
      const title = 'Amounts'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Prime Amount',
        `${util.numberWithCommas(filing.prime_amount)} USD`,
        true,
        filing.prime_amount
      )

      ui.includeTableKeyValue(
        features,
        'Subaward Amount',
        `${util.numberWithCommas(filing.sub_amount)} USD`,
        true,
        filing.sub_amount
      )

      ui.includeTableKeyValue(
        features,
        'Total Outlayed',
        `${util.numberWithCommas(filing.prime_total_outlayed)} USD`,
        true,
        filing.prime_total_outlayed
      )

      ui.includeTableKeyValue(
        features,
        'COVID-19 Obligation',
        `${util.numberWithCommas(filing.prime_covid_obligated)} USD`,
        true,
        filing.prime_covid_obligated
      )

      ui.includeTableKeyValue(
        features,
        'COVID-19 Outlayed Amount',
        `${util.numberWithCommas(filing.prime_covid_outlayed)} USD`,
        true,
        filing.prime_covid_outlayed
      )

      ui.includeTableKeyValue(
        features,
        'IIJA Obligation',
        `${util.numberWithCommas(filing.prime_iija_obligated)} USD`,
        true,
        filing.prime_iija_obligated
      )

      ui.includeTableKeyValue(
        features,
        'IIJA Outlayed Amount',
        `${util.numberWithCommas(filing.prime_iija_outlayed)} USD`,
        true,
        filing.prime_iija_outlayed
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderDates() {
      const label = 'dates'
      const title = 'Dates'

      let features = []

      ui.includeTableKeyDate(
        features,
        'Prime Base Action Date',
        util.dateWithoutTime(new Date(filing.prime_base_action_date))
      )
      if (!filing.prime_base_action_date) {
        ui.includeTableKeyValue(
          features,
          'Prime Base Action Fiscal Year',
          filing.prime_base_action_fiscal_year
        )
      }

      ui.includeTableKeyDate(
        features,
        'Prime Latest Action Date',
        util.dateWithoutTime(new Date(filing.prime_latest_action_date))
      )
      if (!filing.prime_latest_action_date) {
        ui.includeTableKeyValue(
          features,
          'Prime Latest Action Fiscal Year',
          filing.prime_latest_action_fiscal_year
        )
      }

      ui.includeTableKeyDate(
        features,
        'Prime Start Date',
        util.dateWithoutTime(new Date(filing.prime_start_date))
      )
      ui.includeTableKeyDate(
        features,
        'Prime End Date',
        util.dateWithoutTime(new Date(filing.prime_end_date))
      )

      ui.includeTableKeyDate(
        features,
        'Sub Action Date',
        util.dateWithoutTime(new Date(filing.sub_action_date))
      )
      if (!filing.sub_action_date) {
        ui.includeTableKeyValue(
          features,
          'Sub Action Fiscal Year',
          filing.sub_action_fiscal_year
        )
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderActionDetails() {
      const label = 'action-details'
      const title = 'Action Details'

      let features = []

      ui.includeTableKeyValue(features, 'Prime FAIN', filing.prime_fain)
      ui.includeTableKeyValue(features, 'Subaward Number', filing.sub_number)
      ui.includeTableKeyValue(
        features,
        'Prime Unique Key',
        filing.prime_unique_key
      )

      ui.includeTableKeyValue(
        features,
        'Prime Emergency Fund Codes',
        filing.prime_emergency_fund_codes
      )

      ui.includeTableKeyValue(
        features,
        'Prime Business Types',
        filing.prime_business_types
      )

      ui.includeTableKeyValue(
        features,
        'Sub Business Types',
        filing.sub_business_types
      )

      ui.includeTableKeyValue(
        features,
        'Prime Object Classes',
        filing.prime_object_classes
      )

      ui.includeTableKeyValue(
        features,
        'Prime Program Activities',
        filing.prime_program_activities
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderDescription() {
      const label = 'description'
      const title = 'Description'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Prime CFDA Title',
        filing.prime_cfda_numbers_and_titles
      )
      ui.includeTableKeyValue(
        features,
        'Prime Transaction Description',
        filing.prime_base_transaction_description
      )
      ui.includeTableKeyValue(
        features,
        'Subaward Description',
        filing.sub_description
      )

      const primePlaceOfPerformance = primePlaceOfPerformanceLines()
      if (primePlaceOfPerformance.length) {
        ui.includeTableKeyValue(
          features,
          "Prime's Place of Performance",
          primePlaceOfPerformance
        )
      }

      const subPlaceOfPerformance = subPlaceOfPerformanceLines()
      if (subPlaceOfPerformance.length) {
        ui.includeTableKeyValue(
          features,
          "Sub's Place of Performance",
          subPlaceOfPerformance
        )
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderHighlyPaidOfficers() {
      const label = 'highly-paid-officers'
      const title = 'Highly Paid Officers'

      let features = []

      function includeOfficer(index) {
        const nameKey = `sub_officer_${index}_name`
        const amountKey = `sub_officer_${index}_amount`
        if (filing[nameKey]) {
          ui.includeTableKeyValue(
            features,
            `Officer ${index}`,
            `${filing[nameKey]}: ${util.numberWithCommas(
              filing[amountKey]
            )} USD`
          )
        }
      }

      for (let i = 1; i <= 5; i += 1) {
        includeOfficer(i)
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    let title = []
    if (filing.prime_cfda_numbers_and_titles) {
      title.push(
        <p className="ti-compact">{filing.prime_cfda_numbers_and_titles}</p>
      )
    }

    const header = (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light"
            href={usaSpendingAwardURL(filing.prime_unique_key)}
            role="button"
          >
            View in USASpending
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderPrimeRecipient())
    accordionItems.push(renderPrimeRecipientParent())
    accordionItems.push(renderSubcontractor())
    accordionItems.push(renderOffice(true))
    accordionItems.push(renderOffice(false))
    accordionItems.push(renderAmounts())
    accordionItems.push(renderDates())
    accordionItems.push(renderActionDetails())
    accordionItems.push(renderDescription())
    accordionItems.push(renderHighlyPaidOfficers())
    let accordion = (
      <Accordion id="au-procurement-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(item) {
  return item.source == 'prime' ? (
    <PrimeGrant filing={item.filing} />
  ) : (
    <SubGrant filing={item.filing} />
  )
}
