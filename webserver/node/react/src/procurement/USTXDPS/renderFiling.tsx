import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function renderValues(filing) {
  const label = 'contract-values'
  const title = 'Contract Values'

  let features = []

  function includeUSDValue(k, v) {
    if (v) {
      features.push(
        <ui.TableKeyValue k={k} v={'$' + `${util.numberWithCommas(v)}`} />
      )
    }
  }

  includeUSDValue('Maximum Value', filing.contract_maximum)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderDates(filing) {
  const label = 'contract-dates'
  const title = 'Contract Dates'

  let features = []

  ui.includeTableKeyDate(features, 'Start Date', filing.start_date)
  ui.includeTableKeyDate(features, 'End Date', filing.end_date)

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

function renderVendor(filing) {
  const label = 'vendor'

  const annotation = filing.annotation.vendor
  const title = `Vendor: ${annotation.origText.toUpperCase()}`

  const features = [
    <ui.TableKeyValue
      k="Vendor Name"
      v={util.coerceUpperCase(annotation.origText)}
    />,
  ]

  return (
    <AccordionItemWithAnnotation
      label={label}
      title={title}
      features={features}
      annotation={annotation}
    />
  )
}

function renderDescription(filing) {
  const label = 'description'
  const title = 'Description'

  let features = []

  ui.includeTableKeyValue(features, 'Description', filing.description)
  ui.includeTableKeyValue(
    features,
    'Contract ID',
    filing.contract.replace(/^0+/, ''),
    true,
    filing.contract
  )

  return (
    <SimpleAccordionItem
      tableClassName="table ti-key-value-table"
      key={title}
      label={label}
      title={title}
      features={features}
    />
  )
}

export function renderFiling(filing) {
  const maxDescriptionLength = 200

  const title = util.trimLabel(filing.description, maxDescriptionLength)

  let accordionItems = []
  accordionItems.push(renderVendor(filing))
  accordionItems.push(renderValues(filing))
  accordionItems.push(renderDates(filing))
  accordionItems.push(renderDescription(filing))
  let accordion = (
    <Accordion id="au-procurement-accordion" items={accordionItems} />
  )

  const pdfURL =
    'https://www.dps.texas.gov/sites/default/files/documents/iod/doingbusiness/docs/contractsover100k.pdf'
  const header = (
    <p className="card-text">
      <a href={pdfURL}>
        <button type="button" className="btn btn-secondary px-2 py-1">
          Full Dataset
        </button>
      </a>
    </p>
  )

  return (
    <div className="card text-start mb-2 border-0">
      <div className="card-body">
        <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
        {header}
        {accordion}
      </div>
    </div>
  )
}
