import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import * as util from '../utilities/util'

import { renderFiling } from './UATender/renderFiling'

const kLabel = 'ua-tender'
const kTableID = `ti-table-${kLabel}`
const kGovLogo = '/logos/government_of_ukraine.svg'

const kColumns = [
  { title: 'Index', visible: false },
  { title: 'Creation Date' },
  { title: 'ID' },
  { title: 'Suppliers' },
  { title: 'Buyer' },
  { title: 'Title' },
  { title: 'Description' },
  { title: 'Entire Filing', visible: false },
]

export class UATender extends React.Component<any, any> {
  urlBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)

    const direction = this.props.isAgency ? '(as agency)' : '(as vendor)'

    const titleString = `Ukrainian tenders ${direction}`
    this.title = (
      <span>
        <img src={kGovLogo} className="dropdown-flag" />
        {titleString}
      </span>
    )

    const source = `UA tender ${direction}`
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[source].get

    this.maxDescriptionLength = 256
    this.tableID = this.props.isAgency ? `${kTableID}-agency` : kTableID

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? `${kLabel}-agency`
      : kLabel

    this.columns = kColumns
  }

  itemToRow(item, itemIndex) {
    const tenderID = item.tender.data.tenderID
    const dateCreated = util.dateWithoutTimeLex(item.date_created)

    const suppliers = item.suppliers.join('; ')
    const buyers = item.buyers.join('; ')

    const title = util.coerce(item.tender.data.title, '')
    const description = util.coerce(item.tender.data.description, '')

    const entireItem = JSON.stringify(item)
    return [
      itemIndex,
      dateCreated,
      tenderID,
      suppliers,
      buyers,
      title,
      description,
      entireItem,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="ua_tender"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
