import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import * as util from '../utilities/util'

import { renderFiling } from './CanadaBuys/renderFiling'

export class CanadaBuys extends React.Component<any, any> {
  urlBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    const source = this.props.isAgency
      ? 'Canada Buys (as agency)'
      : 'Canada Buys (as vendor)'
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[source].get

    this.maxDescriptionLength = 256
    this.tableID = this.props.isAgency
      ? 'ti-table-canada-buys-agency-procurement'
      : 'ti-table-canada-buys-procurement'
    this.title = (
      <span>
        <img src="/logos/government_of_canada.svg" className="dropdown-flag" />
        Canada Buys procurement records{' '}
        {this.props.isAgency ? '(as agency)' : '(as vendor)'}
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? 'canada-buys-agency-procurement'
      : 'canada-buys-procurement'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Award Date' },
      { title: 'Vendor' },
      {
        title: 'Total Contract Value',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, 'CA$'),
      },
      { title: 'End Users' },
      { title: 'Title' },
      { title: 'Description' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex: number) {
    const awardDate = filing.contract_award_date
      ? util.dateWithoutTimeLex(filing.contract_award_date)
      : ''
    const vendor = util.coerce(filing.supplier_legal_name, '')
    const totalContractValue = util.coerce(filing.total_contract_value, '')
    const endUser = util.coerce(filing.end_user_entities_name, '')
    const title = util.coerce(filing.title, '')
    const description = util.coerce(
      filing.tender_description,
      util.coerce(filing.gsin_description, '')
    )
    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      awardDate,
      vendor,
      totalContractValue,
      endUser,
      title,
      description,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="ca_canada_buys"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
