import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kProcurementPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USCA/renderFiling'

import * as util from '../utilities/util'

export class USCAProcurement extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    const direction = this.props.isAgency ? '(as agency)' : '(as vendor)'
    const source = `US CA ${direction}`
    this.urlBase = appConfig.apiURL + '/' + kProcurementPaths[source].get

    this.tableID = this.props.isAgency
      ? 'ti-table-us-ca-procurement-agency'
      : 'ti-table-us-ca-procurement'
    this.title = (
      <span>
        <img src="/logos/state_of_california.svg" className="dropdown-flag" />
        California (US) Procurement Records {direction}
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : this.props.isAgency
      ? 'us-ca-procurement-agency'
      : 'us-ca-procurement'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Start Date' },
      { title: 'Vendor' },
      { title: 'Department' },
      {
        title: 'Total Amount',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
      },
      { title: 'First Item Title' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex: number) {
    const startDate = filing.start_date
      ? util.dateWithoutTimeLex(filing.start_date)
      : ''
    const vendor = util.coerceUpperCase(filing.supplier_name, '')
    const department = util.coerce(filing.department_name, '')
    const totalAmount = util.coerce(filing.grand_total, '')
    const title = util.coerce(filing.first_item_title, '')
    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      startDate,
      vendor,
      department,
      totalAmount,
      title,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL(): string {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
