import * as React from 'react'

import * as ui from './utilities/ui'
import * as util from './utilities/util'

function filingEntityLogo(annotation: any): any {
  if (!annotation) {
    return ''
  }
  return annotation.logo ? (
    <a href={util.getEntityURL(annotation.text)}>
      <img className="ti-association-list-entity-logo" src={annotation.logo} />
    </a>
  ) : (
    <a href={util.getEntityURL(annotation.text)}>
      <span style={{ fontSize: '1.5rem' }}>
        {util.coerceUpperCase(annotation.stylizedText, 'N/A')}
      </span>
    </a>
  )
}

export class AccordionItem extends React.Component<any, any> {
  headingId: string
  collapseId: string
  showChoice: string

  constructor(props) {
    super(props)
    this.headingId = `${this.props.label}-heading`
    this.collapseId = `${this.props.label}-collapse`
    this.showChoice = this.props.closed ? '' : 'show'
  }

  render() {
    return (
      <div className="accordion-item">
        <h2 className="accordion-header" id={this.headingId}>
          <button
            className={`accordion-button ${
              this.props.closed ? 'collapsed' : ''
            }`}
            type="button"
            data-bs-toggle="collapse"
            data-bs-target={`#${this.collapseId}`}
            aria-expanded={this.props.closed ? 'false' : 'true'}
            aria-controls={this.collapseId}
          >
            {this.props.title}
          </button>
        </h2>
        <div
          id={this.collapseId}
          className={`accordion-collapse collapse ${this.showChoice}`}
          aria-labelledby={this.headingId}
        >
          <div className="accordion-body">{this.props.body}</div>
        </div>
      </div>
    )
  }
}

export class SimpleAccordionItem extends React.Component<any, any> {
  render() {
    if (!(this.props.features && this.props.features.length)) {
      return undefined
    }

    const tableClassName = this.props.tableClassName
      ? this.props.tableClassName
      : 'table'
    const body = (
      <table className={tableClassName}>
        <tbody>{this.props.features}</tbody>
      </table>
    )
    return (
      <AccordionItem
        label={this.props.label}
        title={this.props.title}
        closed={this.props.closed}
        body={body}
      />
    )
  }
}

export class ItemWithLogo extends React.Component<any, any> {
  render() {
    if (!(this.props.features && this.props.features.length)) {
      return undefined
    }

    const tableClassName = util.coerce(this.props.tableClassName, 'table')
    return (
      <div className="container px-0">
        <div className="row flex-md-row-reverse align-items-center mx-0 px-0 py-3">
          <div className="col-12 col-md-4 align-items-center px-3 bg-light">
            <div className="text-center">{this.props.logo}</div>
          </div>
          <div className="col-12 col-md-8">
            <table className={tableClassName}>
              <tbody>{this.props.features}</tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}

export class AccordionItemWithLogo extends React.Component<any, any> {
  render() {
    if (!(this.props.features && this.props.features.length)) {
      return undefined
    }

    const body = (
      <ItemWithLogo
        tableClassName={this.props.tableClassName}
        logo={this.props.logo}
        features={this.props.features}
      />
    )
    return (
      <AccordionItem
        label={this.props.label}
        title={this.props.title}
        closed={this.props.closed}
        body={body}
      />
    )
  }
}

export class ItemWithAnnotation extends React.Component<any, any> {
  render() {
    return (
      <ItemWithLogo
        features={this.props.features}
        logo={filingEntityLogo(this.props.annotation)}
      />
    )
  }
}

export class AccordionItemWithAnnotation extends React.Component<any, any> {
  render() {
    return (
      <AccordionItemWithLogo
        label={this.props.label}
        title={this.props.title}
        closed={this.props.closed}
        features={this.props.features}
        logo={filingEntityLogo(this.props.annotation)}
      />
    )
  }
}

export class Accordion extends React.Component<any, any> {
  render() {
    return (
      <div className="accordion accordion-flush" id={this.props.id}>
        {this.props.items}
      </div>
    )
  }
}
