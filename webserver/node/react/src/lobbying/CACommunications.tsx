import * as React from 'react'

import { FilingFeed } from '../FilingFeed'

import { kLobbyingPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import {
  activityDescription,
  buildName,
  renderFiling,
} from './CACommunications/renderFiling'

import * as util from '../utilities/util'

export class CALobbyingCommunications extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL + '/' + kLobbyingPaths['CA communications'].get

    this.tableID = 'ti-table-ca-lobbying-communications'
    this.title = (
      <div>
        <p className="ti-compact">
          <span>
            <img
              src="/logos/government_of_canada.svg"
              className="dropdown-flag"
            />
            Canadian monthly lobbying communications
          </span>
        </p>
        <p className="ti-compact text-muted lh-1" style={{ fontSize: '80%' }}>
          <small>
            For a more focused look at Canadian lobbying records alongside
            freedom of information requests, please see the McMaster
            University-affiliated project{' '}
            <a href="https://thetechlobby.ca/our-team/">TheTechLobby</a>.
          </small>
        </p>
      </div>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : 'ca-lobbying-communications'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Date' },
      { title: 'Client Name' },
      { title: 'Registrant Name' },
      { title: 'Office Holder Name' },
      { title: 'Office Holder Title' },
      { title: 'Office Holder Inst.' },
      { title: 'Activity' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    const date = filing.date ? util.dateWithoutTimeLex(filing.date) : ''
    const clientName = filing.name ? filing.name : ''
    const registrantName = buildName(filing.registrant)
    const officeHolderName = buildName(filing.dpoh)
    const officeHolderTitle = filing.dpoh.title ? filing.dpoh.title : ''
    const officeHolderInst = filing.dpoh.institution
      ? filing.dpoh.institution
      : ''
    const activity = activityDescription(filing.activity)
    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      date,
      clientName,
      registrantName,
      officeHolderName,
      officeHolderTitle,
      officeHolderInst,
      activity,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
