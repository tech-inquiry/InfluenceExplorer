import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kLobbyingPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USCAContributions/renderFiling'

import * as util from '../utilities/util'

export class USCALobbyingContributions extends React.Component<any, any> {
  urlBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL + '/' + kLobbyingPaths['US CA contributions'].get

    this.maxDescriptionLength = 256
    this.tableID = 'ti-table-us-ca-lobbying-contributions'
    this.title = (
      <span>
        <img src="/logos/state_of_california.svg" className="dropdown-flag" />
        California campaign contributions
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : 'us-ca-lobbying-contributions'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Date Filed' },
      { title: 'Filer' },
      { title: 'Candidate' },
      { title: 'Employer' },
      { title: 'Contributor' },
      {
        title: 'Received',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
      },
      { title: 'Ballot Measure' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    function buildName(entityObj) {
      const nameKeys = ['prefix', 'first_name', 'last_name', 'suffix']
      let namePieces = []
      nameKeys.forEach(function (nameKey) {
        if (nameKey in entityObj && entityObj[nameKey]) {
          namePieces.push(entityObj[nameKey])
        }
      })
      if (namePieces.length) {
        return namePieces.join(' ')
      } else {
        return null
      }
    }

    const filedDate = filing.cover
      ? util.dateWithoutTimeLex(filing.cover['filed_date'])
      : ''

    let ballotMeasure = ''
    if (
      filing.cover &&
      filing.cover.ballot_measure &&
      filing.cover.ballot_measure.name
    ) {
      ballotMeasure = filing.cover.ballot_measure.name
    }

    let filersStr = ''
    if (filing.cover && filing.cover.filer) {
      filersStr = buildName(filing.cover.filer)
    }

    let candidatesStr = ''
    if (filing.cover && filing.cover.candidate) {
      candidatesStr = buildName(filing.cover.candidate)
    }

    let contributorStr = ''
    let employerStr = ''
    if (
      filing.campaign_contribution &&
      filing.campaign_contribution.contributor
    ) {
      const contributor = filing.campaign_contribution.contributor
      contributorStr = buildName(contributor)
      if (contributor.employer) {
        employerStr = contributor.employer
      }
    }

    let received = 0
    if (
      filing.campaign_contribution &&
      filing.campaign_contribution.amount &&
      'received' in filing.campaign_contribution.amount
    ) {
      received = filing.campaign_contribution.amount.received
    }

    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      filedDate,
      filersStr,
      candidatesStr,
      employerStr,
      contributorStr,
      received,
      ballotMeasure,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="us_calaccess_campaign_contrib"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
