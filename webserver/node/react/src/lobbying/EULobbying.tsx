import * as React from 'react'

import { FilingFeed } from '../FilingFeed'

import { kLobbyingPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './EULobbying/renderFiling'

import * as util from '../utilities/util'

const kNote = [undefined]

export class EULobbying extends React.Component<any, any> {
  urlBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    this.urlBase = appConfig.apiURL + '/' + kLobbyingPaths['EU Lobbying'].get

    this.maxDescriptionLength = 256
    this.tableID = 'ti-table-eu-lobbying'
    this.title = (
      <span>
        <img src="/logos/european_union.svg" className="dropdown-flag" />
        EU Lobbying Disclosures
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label ? this.props.label : 'eu-lobbying'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Last Update', visible: true },
      { title: 'Name', visible: true },
      { title: 'Goals', visible: true },
      { title: 'Proposals', visible: true },
      { title: 'Communications', visible: true },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    const maxDescLength = 300
    const lastUpdate = util.dateWithoutTimeLex(filing.last_update_date)
    const name = filing.name
    const goals = util.trimLabel(filing.goals, maxDescLength)
    const proposals = util.trimLabel(
      filing.legislative_proposals,
      maxDescLength
    )
    const communications = util.trimLabel(filing.communications, maxDescLength)
    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      lastUpdate,
      name,
      goals,
      proposals,
      communications,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="eu_lobbying"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        note={kNote}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
