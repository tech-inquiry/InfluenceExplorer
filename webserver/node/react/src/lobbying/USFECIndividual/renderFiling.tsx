import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function formatZipPlusFour(zip) {
  if (!zip) {
    return zip
  }
  if (/^\d+$/.test(zip) && zip.length == 9) {
    return `${zip.substring(0, 5)}-${zip.substring(5, 9)}`
  }
  return zip
}

function renderContributorAddress(obj) {
  if (!obj.city && !obj.state && !obj.zip) {
    return undefined
  }
  const address = {
    city: util.coerceUpperCase(obj.city),
    state: util.coerceUpperCase(obj.state),
    postalCode: formatZipPlusFour(obj.zip),
  }
  return ui.simpleAddress(address)
}

function renderCandidateAddress(obj) {
  if (
    !obj.candidate_city &&
    !obj.candidate_state &&
    !obj.candidate_zip &&
    !obj.candidate_street1 &&
    !obj.candidate_street2
  ) {
    return undefined
  }
  const address = {
    streetLines: [],
    city: util.coerceUpperCase(obj.candidate_city),
    state: util.coerceUpperCase(obj.candidate_state),
    postalCode: formatZipPlusFour(obj.candidate_zip),
  }
  if (obj.candidate_street1) {
    address.streetLines.push(util.coerceUpperCase(obj.candidate_street1))
  }
  if (obj.candidate_street2) {
    address.streetLines.push(util.coerceUpperCase(obj.candidate_street2))
  }
  return ui.simpleAddress(address)
}

function renderCommitteeAddress(obj) {
  if (
    !obj.committee_city &&
    !obj.committee_state &&
    !obj.committee_zip &&
    !obj.committee_street1 &&
    !obj.committee_street2
  ) {
    return undefined
  }
  const address = {
    streetLines: [],
    city: util.coerceUpperCase(obj.committee_city),
    state: util.coerceUpperCase(obj.committee_state),
    postalCode: formatZipPlusFour(obj.committee_zip),
  }
  if (obj.committee_street1) {
    address.streetLines.push(util.coerceUpperCase(obj.committee_street1))
  }
  if (obj.committee_street2) {
    address.streetLines.push(util.coerceUpperCase(obj.committee_street2))
  }
  return ui.simpleAddress(address)
}

export class FECIndividual extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = [
      'filer_id',
      'candidate_id',
      'fec_record_number',
      'cycle',
    ]
    super(props)
    super.setEndpoint('/api/us/central/lobbying/fec/individual')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing

    function renderTransaction() {
      const title = 'Transaction'
      const label = 'transaction'

      let features = []

      function electionType(indicator: string): string {
        if (indicator == 'P') {
          return 'Primary'
        } else if (indicator == 'G') {
          return 'General'
        } else {
          return indicator
        }
      }

      ui.includeTableKeyValue(
        features,
        'Election Type',
        electionType(filing.primary_general_indicator),
        true,
        filing.primary_general_indicator
      )
      ui.includeTableKeyDate(features, 'Date', filing.transaction_date)
      ui.includeTableKeyValue(
        features,
        'Amount',
        '$' + `${util.numberWithCommas(filing.transaction_amount)}`,
        true,
        filing.transaction_amount
      )

      ui.includeTableKeyValue(features, 'ID', filing.transaction_id)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderContributor() {
      const title = 'Contributor'
      const label = 'contributor'

      const contributorAnnotation = filing.annotation.contributor
      const employerAnnotation = filing.annotation.employer
      const annotation = contributorAnnotation.logo
        ? contributorAnnotation
        : employerAnnotation

      const features = [
        <ui.TableKeyValue
          k="Name"
          v={util.coerceUpperCase(contributorAnnotation.origText)}
        />,
        <ui.TableKeyValue
          k="Contributor Address"
          v={renderContributorAddress(filing)}
        />,
        <ui.TableKeyValue
          k="Employer"
          v={util.coerceUpperCase(employerAnnotation.origText)}
        />,
        <ui.TableKeyValue
          k="Occupation"
          v={util.coerceUpperCase(filing.occupation)}
        />,
      ]

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderRecipient() {
      const title = 'Recipient'
      const label = 'recpient'

      function officeType(office: string): string {
        if (office == 'h') {
          return 'U.S. House of Representatives'
        } else if (office == 's') {
          return 'U.S. Senate'
        } else {
          return util.coerceUpperCase(office)
        }
      }

      function incumbantStatus(status: string): string {
        if (status == 'C') {
          return 'as Challenger'
        } else if (status == 'I') {
          return 'as Incumbant'
        } else if (status == 'O') {
          return 'with no Incumbant'
        } else {
          return status
        }
      }

      function candidateStatus(status: string): string {
        if (status == 'C') {
          return 'Statutory candidate'
        } else if (status == 'F') {
          return 'Statutory candidate for future election'
        } else if (status == 'N') {
          return 'Not yet a statutory candidate'
        } else if (status == 'P') {
          return 'Statutory candidate in prior cycle'
        } else {
          return status
        }
      }

      function officeDescription(
        office: string,
        state: string,
        district: string,
        incumbant: string
      ): string {
        if (!office && !state && (district == '00' || !district)) {
          return undefined
        }

        let desc = 'Seeking '
        if (state) {
          desc += `${officeType(office)} seat in ${util.coerceUpperCase(state)}`

          if (district) {
            desc += ` District ${district}`
          }
        } else {
          desc += officeType(office)
        }
        desc += ` ${incumbantStatus(incumbant)}.`
        return desc
      }

      function partyAffiliation(affil: string): string {
        if (affil == 'REP') {
          return 'Republican'
        } else if (affil == 'DEM') {
          return 'Democratic'
        } else {
          return affil
        }
      }

      const annotation = filing.annotation.candidate.origText
        ? filing.annotation.candidate
        : filing.annotation.committee

      const committeeAnnotation = filing.annotation.committee
      const treasurerAnnotation = filing.annotation.treasurer
      const connectedOrgAnnotation = filing.annotation.connectedOrganization
      const features = [
        annotation.origText ? (
          <ui.TableKeyValue
            k="Candidate"
            v={
              annotation.url ? (
                <a href={annotation.url}>
                  {util.coerceUpperCase(annotation.origText)}
                </a>
              ) : (
                util.coerceUpperCase(annotation.origText)
              )
            }
          />
        ) : undefined,
        filing.candidate_id ? (
          <ui.TableKeyValue
            k="Candidate ID"
            v={
              <a
                href={`https://www.fec.gov/data/candidate/${filing.candidate_id}/`}
              >
                {filing.candidate_id}
              </a>
            }
          />
        ) : undefined,
        <ui.TableKeyValue
          k="Candidate Office"
          v={officeDescription(
            filing.candidate_office,
            filing.candidate_office_state,
            filing.candidate_office_district,
            filing.candidate_incumbant_status
          )}
        />,
        <ui.TableKeyValue
          k="Candidate Party"
          v={partyAffiliation(filing.candidate_party_affiliation)}
        />,
        <ui.TableKeyValue
          k="Candidate Status"
          v={candidateStatus(filing.candidate_status)}
        />,
        <ui.TableKeyValue
          k="Candidate Address"
          v={renderCandidateAddress(filing)}
        />,
        <ui.TableKeyValue
          k="Committee"
          v={
            committeeAnnotation.url ? (
              <a href={committeeAnnotation.url}>
                {util.coerceUpperCase(committeeAnnotation.origText)}
              </a>
            ) : (
              util.coerceUpperCase(committeeAnnotation.origText)
            )
          }
        />,
        <ui.TableKeyValue
          k="Committee ID"
          v={
            <a href={`https://www.fec.gov/data/committee/${filing.filer_id}/`}>
              {filing.filer_id}
            </a>
          }
        />,
        <ui.TableKeyValue
          k="Committee Address"
          v={renderCommitteeAddress(filing)}
        />,
        <ui.TableKeyValue
          k="Treasurer"
          v={
            treasurerAnnotation.url ? (
              <a href={treasurerAnnotation.url}>
                {util.coerceUpperCase(treasurerAnnotation.origText)}
              </a>
            ) : (
              util.coerceUpperCase(treasurerAnnotation.origText)
            )
          }
        />,
        connectedOrgAnnotation.origText &&
        connectedOrgAnnotation.origText != 'none' ? (
          <ui.TableKeyValue
            k="Connected Organization"
            v={
              connectedOrgAnnotation.url ? (
                <a href={connectedOrgAnnotation.url}>
                  {util.coerceUpperCase(connectedOrgAnnotation.origText)}
                </a>
              ) : (
                util.coerceUpperCase(connectedOrgAnnotation.origText)
              )
            }
          />
        ) : undefined,
      ]

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderMemo() {
      const title = 'Memo'
      const label = 'memo'

      let features = []

      ui.includeTableKeyValue(
        features,
        'Memo',
        util.coerceUpperCase(filing.memo_text)
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    const title =
      (filing.transaction_date
        ? `[${util.dateWithoutTimeLex(filing.transaction_date)}]: `
        : '') +
      (filing.contributor_name
        ? filing.contributor_name.toUpperCase()
        : 'N/A') +
      (filing.employer ? ' of ' + filing.employer.toUpperCase() : '')

    const header = (
      <p className="card-text">
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderTransaction())
    accordionItems.push(renderContributor())
    accordionItems.push(renderRecipient())
    accordionItems.push(renderMemo())
    let accordion = (
      <Accordion id="us-fec-individual-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <FECIndividual filing={filing} />
}
