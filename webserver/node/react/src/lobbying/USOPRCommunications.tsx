import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kLobbyingPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USOPRCommunications/renderFiling'

import * as util from '../utilities/util'

export class USOPRCommunications extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL + '/' + kLobbyingPaths['US OPR communications'].get

    this.tableID = 'ti-table-us-opr-communications'
    this.title = (
      <span>
        <img
          src="/logos/government_of_the_united_states.svg"
          className="dropdown-flag"
        />
        US federal lobbying filings (Senate Office of Public Records)
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label ? this.props.label : 'us-opr-communications'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Received', visible: true },
      { title: 'Year', visible: true },
      { title: 'Type', visible: true },
      { title: 'Registrant', visible: true },
      { title: 'Client', visible: true },
      {
        title: 'Amount',
        visible: true,
        render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
      },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    const received = util.dateWithoutTimeLex(filing.dt_posted)
    const year = filing.filing_year
    const type = filing.filing_type
    const registrantName = filing.registrant.name
    const clientName = filing.client.name
    const amount = filing.income
      ? filing.income
      : filing.expenses
      ? filing.expenses
      : ''
    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      received,
      year,
      type,
      registrantName,
      clientName,
      amount,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="us_opr_issues_filing"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
