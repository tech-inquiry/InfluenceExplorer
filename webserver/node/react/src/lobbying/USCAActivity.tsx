import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kLobbyingPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USCAActivity/renderFiling'

import * as util from '../utilities/util'

export class USCALobbyingActivity extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    this.urlBase = appConfig.apiURL + '/' + kLobbyingPaths['US CA activity'].get

    this.tableID = 'ti-table-us-ca-lobbying-activity'
    this.title = (
      <span>
        <img src="/logos/state_of_california.svg" className="dropdown-flag" />
        California lobbying activity
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label ? this.props.label : 'us-ca-lobbying-activity'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Date Filed' },
      { title: 'Filers' },
      { title: 'Employers' },
      { title: 'Activities' },
      {
        title: 'Fees',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
      },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    function buildName(entityObj) {
      const nameKeys = ['prefix', 'first_name', 'last_name', 'suffix']
      let namePieces = []
      nameKeys.forEach(function (nameKey) {
        if (nameKey in entityObj && entityObj[nameKey]) {
          namePieces.push(entityObj[nameKey])
        }
      })
      if (namePieces.length) {
        return namePieces.join(' ')
      } else {
        return null
      }
    }

    const filedDate = filing.cover
      ? util.dateWithoutTimeLex(filing.cover['date_filed'])
      : ''

    let filersStr = ''
    if (filing.cover?.filer) {
      filersStr = buildName(filing.cover.filer)
    }

    let employersStr = ''
    if (filing.payment?.employer) {
      employersStr = buildName(filing.payment.employer)
    }

    let activities = new Set()
    if (filing.cover?.activity_description) {
      activities.add(filing.cover.activity_description)
    }
    if (filing.payment?.activity_description) {
      activities.add(filing.payment.activity_description)
    }
    const activitiesStr = Array.from(activities).join('\n\n')

    let fees = 0
    if (filing.payment?.amount?.fees) {
      fees = Number(filing.payment.amount.fees)
    }

    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      filedDate,
      filersStr,
      employersStr,
      activitiesStr,
      fees,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="us_calaccess_lobby_disc_payment"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
