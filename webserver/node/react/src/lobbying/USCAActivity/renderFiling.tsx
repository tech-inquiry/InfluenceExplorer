import * as React from 'react'
import * as jQuery from 'jquery'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

const kLegInfoBaseURL =
  'https://leginfo.legislature.ca.gov/faces/billNavClient.xhtml'

function getYearFromFiling(filing): number {
  let year = -1
  if (filing.cover && filing.cover['period_begin']) {
    const periodBegin = new Date(filing.cover['period_begin'])
    year = periodBegin.getFullYear()
  }
  return year
}

function buildName(entityObj) {
  if (!entityObj) {
    return null
  }
  const nameKeys = ['prefix', 'first_name', 'last_name', 'suffix']
  let namePieces = []
  nameKeys.forEach(function (nameKey) {
    if (nameKey in entityObj && entityObj[nameKey]) {
      namePieces.push(entityObj[nameKey])
    }
  })
  if (namePieces.length) {
    return namePieces.join(' ')
  } else {
    return null
  }
}

function renderCalAccessLobbyingAddressFromObject(entity) {
  let address = {
    city: entity.city,
    state: entity.state,
    postalCode: entity.zipcode,
    phone: entity.phone,
    fax: entity.fax,
  }
  return <ui.Address address={address} />
}

export class Activity extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = ['filing_id', 'transaction_id', 'line_item']
    super(props)
    super.setEndpoint('/api/us/ca/lobbying/activity')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing
    const maxTitleLength = 200

    function insertURLs(
      parts: any[],
      year: number,
      billLabel: string,
      refList: any[],
      matchRE
    ) {
      const billNumbersRE = /[\d+\s*,*]+/

      const sessionURLComp =
        year % 2 ? `${year}${year + 1}` : `${year - 1}${year}`

      function legInfoURL(billType: string, billNumber: string): string {
        return (
          `${kLegInfoBaseURL}?bill_id=${sessionURLComp}0${billType}` +
          billNumber
        )
      }

      function replaceFunc(textMatch) {
        let items = []
        const billNumbersStr = textMatch.match(billNumbersRE)
        for (const billNumber of billNumbersStr[0].trim().split(/[, ]+/)) {
          if (!billNumber) {
            continue
          }
          const url = legInfoURL(billLabel, billNumber)
          const label = `${billLabel} ${billNumber}`
          items.push({ url: url, label: label })
        }
        return items
      }

      let newParts: any[] = []
      for (const part of parts) {
        if (typeof part == 'string') {
          let remaining = part
          let match = undefined
          while ((match = matchRE.exec(remaining))) {
            const matchBeg = match.index
            const matchEnd = match.index + match[0].length
            const preText = remaining.substring(0, matchBeg)
            const matchText = remaining.substring(matchBeg, matchEnd)
            const postText = remaining.substring(matchEnd)
            const items = replaceFunc(matchText)
            newParts.push(preText)
            for (const [i, item] of items.entries()) {
              refList.push(item)
              if (i > 0) {
                newParts.push(', ')
              }
              newParts.push(<a href={item.url}>{item.label}</a>)
            }
            if (matchText.endsWith(', ')) {
              remaining = ', ' + postText
            } else if (matchText.endsWith(' ')) {
              remaining = ' ' + postText
            } else if (matchText.endsWith(',')) {
              remaining = ',' + postText
            } else {
              remaining = postText
            }
          }
          newParts.push(remaining)
        } else {
          newParts.push(part)
        }
      }
      return newParts
    }

    function getBillLinkAnnotations(text: string, year: number) {
      const session = year % 2 ? `${year}-${year + 1}` : `${year - 1}-${year}`
      let parts = [text]

      let assemblyRefs = { bills: [], concurrentResolutions: [] }
      const assemblyRegex = /ab:? [\d+\s*,*]+/i
      parts = insertURLs(parts, year, 'AB', assemblyRefs.bills, assemblyRegex)

      const assemblyConcurrentResolutionsRegex = /acr:? [\d+\s*,*]+/i
      parts = insertURLs(
        parts,
        year,
        'ACR',
        assemblyRefs.concurrentResolutions,
        assemblyConcurrentResolutionsRegex
      )

      let senateRefs = { bills: [], concurrentResolutions: [] }
      const senateRegex = /sb:? [\d+\s*,*]+/i
      parts = insertURLs(parts, year, 'SB', senateRefs.bills, senateRegex)

      const senateConcurrentResolutionsRegex = /scr:? [\d+\s*,*]+/i
      parts = insertURLs(
        parts,
        year,
        'SCR',
        senateRefs.concurrentResolutions,
        senateConcurrentResolutionsRegex
      )

      return {
        description: parts,
        session: session,
        assembly: assemblyRefs,
        senate: senateRefs,
      }
    }

    function includeReferences(refSets, annotation) {
      for (const ref of annotation.assembly.bills) {
        refSets.assembly.bills.add(JSON.stringify(ref))
      }
      for (const ref of annotation.assembly.concurrentResolutions) {
        refSets.assembly.concurrentResolutions.add(JSON.stringify(ref))
      }
      for (const ref of annotation.senate.bills) {
        refSets.senate.bills.add(JSON.stringify(ref))
      }
      for (const ref of annotation.senate.concurrentResolutions) {
        refSets.senate.concurrentResolutions.add(JSON.stringify(ref))
      }
    }

    function renderCover(refSets) {
      const title = 'Cover Page'
      const label = 'coverPage'

      let features = []

      const cover = filing.cover
      const coverAnnotation = filing.annotation.cover
      const entityAnnotations = filing.annotation.entities

      if (!cover) {
        return undefined
      }

      const year = getYearFromFiling(filing)

      ui.includeTableKeyValue(features, 'Filing ID', filing.filing_id)
      let annotation = undefined
      if (cover.filer) {
        const filerName = buildName(cover.filer)
        if (filerName) {
          const entityAnnotation = entityAnnotations[filerName.toLowerCase()]
          annotation = entityAnnotation
          ui.includeTableKeyValue(features, 'Filer', filerName)
        }
      }
      ui.includeTableKeyValue(features, 'Filer ID', cover.filer_id)
      if (cover.firm) {
        ui.includeTableKeyValue(
          features,
          'Firm Address',
          renderCalAccessLobbyingAddressFromObject(cover.firm)
        )
      }

      const signer = cover.signer
      if (signer) {
        const signerName = buildName(signer)
        if (signerName) {
          const entityAnnotation = entityAnnotations[signerName.toLowerCase()]
          if (entityAnnotation && !annotation) {
            annotation = entityAnnotation
          }
          ui.includeTableKeyValue(features, 'Signer', signerName)
        }
        ui.includeTableKeyValue(features, 'Signer Title', signer.title)
        ui.includeTableKeyValue(features, 'Signer Location', signer.location)
        if (cover.signer_prn) {
          const signerNamePRN = buildName(cover.signer_prn)
          if (signerNamePRN && signerNamePRN != signerName) {
            const entityAnnotation =
              entityAnnotations[signerNamePRN.toLowerCase()]
            if (entityAnnotation && !annotation) {
              annotation = entityAnnotation
            }
            ui.includeTableKeyValue(features, 'Signer PRN', signerNamePRN)
          }
        }
        ui.includeTableKeyDate(features, 'Date Signed', signer.date)
      }

      ui.includeTableKeyDate(features, 'Date Filed', cover.date_filed)
      if (cover.period_begin && cover.period_end) {
        ui.includeTableKeyValue(
          features,
          'Period',
          `${util.dateWithoutTime(new Date(cover.period_begin))} -- ` +
            `${util.dateWithoutTime(new Date(cover.period_end))}`
        )
      } else {
        ui.includeTableKeyDate(features, 'Period Begin', cover.period_begin)
        ui.includeTableKeyDate(features, 'Period End', cover.period_end)
      }
      ui.includeTableKeyDate(
        features,
        'Cumulative Begin',
        cover.cumulative_begin
      )
      if (cover.activity_description) {
        const descAnnotation = getBillLinkAnnotations(
          cover.activity_description,
          year
        )
        ui.includeTableKeyValue(
          features,
          'Activity Description',
          descAnnotation.description
        )
        includeReferences(refSets, descAnnotation)
      }

      if (cover.memo) {
        if (cover.memo.text) {
          const descAnnotation = getBillLinkAnnotations(
            cover.activity_description,
            year
          )
          ui.includeTableKeyValue(
            features,
            'Memo Text',
            descAnnotation.description
          )
          includeReferences(refSets, descAnnotation)
        }
        ui.includeTableKeyValue(
          features,
          'Memo Form Type',
          cover.memo.form_type
        )
        ui.includeTableKeyValue(
          features,
          'Memo Reference',
          cover.memo.reference
        )
        ui.includeTableKeyValue(
          features,
          'Memo Line Item',
          cover.memo.line_item
        )
        ui.includeTableKeyValue(
          features,
          'Memo Amendment ID',
          cover.memo.amendment_id
        )
      }

      ui.includeTableKeyValue(features, 'Form Type', cover.form_type)
      ui.includeTableKeyValue(features, 'Sender ID', cover.sender_id)
      ui.includeTableKeyValue(features, 'Entity Code', cover.entity_code)
      ui.includeTableKeyValue(features, 'Amendment ID', cover.amendment_id)

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderPayment(refSets) {
      const title = 'Payment'
      const label = 'payment'

      let features = []

      const payment = filing.payment
      if (!payment) {
        return undefined
      }
      const paymentAnnotation = filing.annotation.payment
      const entityAnnotations = filing.annotation.entities

      const employerName = buildName(payment.employer)
      const year = getYearFromFiling(filing)

      let annotation = undefined

      if (payment.employer) {
        const employerName = buildName(payment.employer)
        if (employerName) {
          const entityAnnotation = entityAnnotations[employerName.toLowerCase()]
          ui.includeTableKeyValue(
            features,
            'Employer',
            util.coerceUpperCase(entityAnnotation.origText)
          )
          annotation = entityAnnotation
        }
        ui.includeTableKeyValue(
          features,
          'Employer Address',
          renderCalAccessLobbyingAddressFromObject(payment.employer)
        )
      }

      if (payment.activity_description) {
        const descAnnotation = getBillLinkAnnotations(
          payment.activity_description,
          year
        )
        ui.includeTableKeyValue(
          features,
          'Activity Description',
          descAnnotation.description
        )
        includeReferences(refSets, descAnnotation)
      }

      if (payment.amount) {
        ui.includeTableKeyValue(
          features,
          'Fees',
          '$' + util.numberWithCommas(payment.amount.fees),
          true,
          payment.amount.fees
        )
        ui.includeTableKeyValue(
          features,
          'Period Total',
          '$' + util.numberWithCommas(payment.amount.period_total),
          true,
          payment.amount.period_total
        )
        ui.includeTableKeyValue(
          features,
          'Cumulative Total',
          '$' + util.numberWithCommas(payment.amount.cumulative_total),
          true,
          payment.amount.cumulative_total
        )
      }

      if (payment.memo) {
        if (payment.memo.text) {
          const descAnnotation = getBillLinkAnnotations(payment.memo.text, year)
          ui.includeTableKeyValue(
            features,
            'Memo Text',
            descAnnotation.description
          )
          includeReferences(refSets, descAnnotation)
        }
        ui.includeTableKeyValue(
          features,
          'Memo Form Type',
          payment.memo.form_type
        )
        ui.includeTableKeyValue(
          features,
          'Memo Reference',
          payment.memo.reference
        )
        ui.includeTableKeyValue(
          features,
          'Memo Line Item',
          payment.memo.line_item
        )
        ui.includeTableKeyValue(
          features,
          'Memo Amendment ID',
          payment.memo.amendment_id
        )
      }

      ui.includeTableKeyValue(features, 'Form Type', payment.form_type)
      ui.includeTableKeyValue(features, 'Line Item', payment.line_item)
      if (payment.transaction) {
        ui.includeTableKeyValue(
          features,
          'Transaction ID',
          payment.transaction_id
        )
      }
      ui.includeTableKeyValue(features, 'Amendment ID', payment.amendment_id)

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderUnmatchedMemo(refSets, memo, index) {
      const title = `Unmatched Memo ${index + 1}`
      const label = `unmatchedMemo-${index + 1}`

      let features = []

      if (memo.text) {
        const year = getYearFromFiling(filing)
        const descAnnotation = getBillLinkAnnotations(memo.text, year)
        ui.includeTableKeyValue(features, 'Text', descAnnotation.description)
        includeReferences(refSets, descAnnotation)
      }
      ui.includeTableKeyValue(features, 'Form Type', memo.form_type)
      ui.includeTableKeyValue(features, 'Reference', memo.reference)
      ui.includeTableKeyValue(features, 'Line Item', memo.line_item)
      ui.includeTableKeyValue(features, 'Amendment', memo.amendment_id)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          closed={true}
          features={features}
        />
      )
    }

    function renderUnmatchedMemos(refSets) {
      const title = 'Unmatched Memos'
      const label = 'unmatchedMemos'

      let accordionItems = []
      for (const [i, memo] of filing.unmatched_memos.entries()) {
        accordionItems.push(renderUnmatchedMemo(refSets, memo, i))
      }
      if (!accordionItems.length) {
        return undefined
      }

      let accordion = (
        <Accordion
          id="us-ca-lobbying-activity-unmatched-accordion"
          items={accordionItems}
        />
      )

      return (
        <AccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          body={accordion}
        />
      )
    }

    function renderReferencedBills(refSets) {
      const title = 'Referenced Bills'
      const label = 'referencedBills'

      let features = []

      function addBillCategory(repType, billType, title) {
        const refs = Array.from(refSets[repType][billType]).map(function (
          item
        ) {
          return JSON.parse(item as string)
        })
        if (!refs.length) {
          return
        }
        ui.includeTableKeyValue(
          features,
          title,
          <ul style={{ paddingLeft: '1em' }}>
            {refs.map(function (item) {
              return (
                <li>
                  <a href={item.url}>{item.label}</a>
                </li>
              )
            })}
          </ul>
        )
      }

      addBillCategory('senate', 'bills', 'Senate Bills')
      addBillCategory(
        'senate',
        'concurrentResolutions',
        'Senate Concurrent Resolutions'
      )
      addBillCategory('assembly', 'bills', 'Assembly Bills')
      addBillCategory(
        'assembly',
        'concurrentResolutions',
        'Assembly Concurrent Resolutions'
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    const employersStr =
      filing.payment && filing.payment.employer
        ? buildName(filing.payment.employer)
        : 'N/A'
    const title = util.trimLabel(
      `${filing.filing_id}: ${employersStr}`,
      maxTitleLength
    )

    const header = (
      <p className="card-text">
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let refSets = {
      assembly: {
        bills: new Set<string>(),
        concurrentResolutions: new Set<string>(),
      },
      senate: {
        bills: new Set<string>(),
        concurrentResolutions: new Set<string>(),
      },
    }

    let accordionItems = []
    accordionItems.push(renderCover(refSets))
    accordionItems.push(renderPayment(refSets))
    accordionItems.push(renderUnmatchedMemos(refSets))
    accordionItems.push(renderReferencedBills(refSets))
    let accordion = (
      <Accordion
        id="us-ca-lobbying-activity-accordion"
        items={accordionItems}
      />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Activity filing={filing} />
}
