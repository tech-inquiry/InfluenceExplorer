import * as React from 'react'

import { FilingFeed } from '../FilingFeed'

import { kLobbyingPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { buildName, renderFiling } from './CARegistrations/renderFiling'

import * as ui from '../utilities/ui'
import * as util from '../utilities/util'

export class CALobbyingRegistrations extends React.Component<any, any> {
  urlBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL + '/' + kLobbyingPaths['CA registrations'].get

    this.maxDescriptionLength = 256
    this.tableID = 'ti-table-ca-lobbying-registrations'
    this.title = (
      <div>
        <p className="ti-compact">
          <span>
            <img
              src="/logos/government_of_canada.svg"
              className="dropdown-flag"
            />
            Canadian lobbying registrations
          </span>
        </p>
        <p className="ti-compact text-muted lh-1" style={{ fontSize: '80%' }}>
          <small>
            For a more focused look at Canadian lobbying records alongside
            freedom of information requests, please see the McMaster
            University-affiliated project{' '}
            <a href="https://thetechlobby.ca/our-team/">TheTechLobby</a>.
          </small>
        </p>
      </div>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : 'ca-lobbying-registrations'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Date' },
      { title: 'Client Name' },
      { title: 'Beneficiaries' },
      { title: 'Firm' },
      { title: 'Registrant Name' },
      { title: 'Registrant Position' },
      { title: 'Activity' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    function beneficiaryNames(filing) {
      let nameList = []
      for (const beneficiary of filing.beneficiaries) {
        const name = buildName(beneficiary)
        if (name) {
          nameList.push(name)
        }
      }
      return nameList.join(', ')
    }

    const date = filing.registration.postedDate
      ? util.dateWithoutTimeLex(filing.registration.postedDate)
      : util.dateWithoutTimeLex(filing.registration.startDate)
    const clientName = buildName(filing.client)
    const beneficiaries = beneficiaryNames(filing)
    const firm = filing.firm ? buildName(filing.firm) : ''
    const registrantName = buildName(filing.registrant)
    const registrantPosition = filing.registrant.position
      ? filing.registrant.position
      : ''
    const activity = filing.activity
      ? `${filing.activity.topics} (${filing.activity.category})`
      : ''
    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      date,
      clientName,
      beneficiaries,
      firm,
      registrantName,
      registrantPosition,
      activity,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
