import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function renderSupportedForums(forums): any[] {
  let items = []
  for (const forum of forums) {
    const name = forum.url ? (
      <a href={forum.url}>{forum.name}</a>
    ) : (
      <span>{forum.name}</span>
    )
    let comment = undefined
    if (forum.category && forum.classification) {
      comment = `${forum.category}, ${forum.classification}`
    } else if (forum.category) {
      comment = forum.category
    } else if (forum.classification) {
      comment = forum.classification
    }
    const entry = comment ? (
      <span>
        {name} ({comment})
      </span>
    ) : (
      name
    )
    items.push(<p className="ti-compact">{entry}</p>)
  }
  return items
}

function stringsToParagraphs(strings: string[]): any[] {
  let paraList = []
  if (strings) {
    for (const line of strings) {
      paraList.push(<p className="ti-compact">{line}</p>)
    }
  }
  return paraList
}

function listIsEmpty(items: string[]): boolean {
  if (items.length == 0) {
    return true
  }
  if (
    items.length == 1 &&
    (!items[0] || items[0] == 'none' || items[0] == 'None')
  ) {
    return true
  }
  return false
}

const countryMap = {
  'UNITED KINGDOM': 'United Kingdom',
  'UNITED STATES': 'United States',
}

function formAddress(obj: any): any {
  if (!obj) {
    return undefined
  }
  let address = {
    streetLines: [],
  }
  if (obj.address) {
    address['streetLines'] = [obj.address]
  }
  if (obj.postCode) {
    address['postalCode'] = obj.postCode
  }
  if (obj.city) {
    address['city'] = obj.city
  }
  if (obj.country) {
    let country = obj.country
    if (countryMap.hasOwnProperty(country)) {
      country = countryMap[country]
    }
    address['country'] = country
  }
  if (obj.phone) {
    address['phone'] = obj.phone.indicPhone
      ? obj.phone.indicPhone + obj.phone.phoneNumber
      : obj.phone.phoneNumber
  }
  return <ui.Address address={address} />
}

function yearToContributionParagraphs(year: any): string[] {
  let paraList = []
  if (!year.contributions || !year.contributions.length) {
    return paraList
  }

  const currency = util.coerce(year.costs?.['@currency'], '')
  for (const contribution of year.contributions) {
    const line = contribution.cost
      ? `${contribution.name} (${currency}${util.numberWithCommas(
          contribution.cost
        )})`
      : contribution.name
    paraList.push(<p className="ti-compact">{line}</p>)
  }
  return paraList
}

function yearToGrantParagraphs(year: any): string[] {
  let paraList = []
  if (!year.grants || !year.grants.length) {
    return paraList
  }

  const currency = util.coerce(year.costs?.['@currency'], '')
  for (const grant of year.grants) {
    const line = grant.cost
      ? `${grant.source} (${currency}${util.numberWithCommas(grant.cost)})`
      : grant.source
    paraList.push(<p className="ti-compact">{line}</p>)
  }
  return paraList
}

function yearToIntermediaryParagraphs(year: any): string[] {
  let paraList = []
  if (!year.intermediaries || !year.intermediaries.length) {
    return paraList
  }

  const currency = util.coerce(year.costs?.['@currency'], '')
  for (const intermediary of year.intermediaries) {
    let line = intermediary.name
    if (intermediary.representationCosts) {
      const repCosts = intermediary.representationCosts
      if (repCosts.range) {
        const lowerBound = currency + util.numberWithCommas(repCosts.range.min)
        const upperBound = currency + util.numberWithCommas(repCosts.range.max)
        if (
          repCosts.range.min != undefined &&
          repCosts.range.max != undefined
        ) {
          line = `${intermediary.name} (${lowerBound} to ${upperBound})`
        } else if (repCosts.range.min != undefined) {
          line = `${intermediary.name} (>= ${lowerBound})`
        } else if (repCosts.range.max != undefined) {
          line = `${intermediary.name} (<= ${upperBound})`
        }
      }
    }
    paraList.push(<p className="ti-compact">{line}</p>)
  }
  return paraList
}

function yearToClientParagraphs(year: any): string[] {
  let paraList = []
  if (!year.clients || !year.clients.length) {
    return paraList
  }

  const currency = util.coerce(year.totalAnnualRevenue?.['@currency'], '')
  for (const client of year.clients) {
    let line = client.name
    if (client.revenue) {
      const revenue = client.revenue
      if (revenue.range) {
        const lowerBound = currency + util.numberWithCommas(revenue.range.min)
        const upperBound = currency + util.numberWithCommas(revenue.range.max)
        if (revenue.range.min != undefined && revenue.range.max != undefined) {
          line = `${client.name} (${lowerBound} to ${upperBound})`
        } else if (revenue.range.min != undefined) {
          line = `${client.name} (>= ${lowerBound})`
        } else if (revenue.range.max != undefined) {
          line = `${client.name} (<= ${upperBound})`
        }
      }
    }
    if (client.proposal) {
      paraList.push(
        <p className="ti-compact">
          {line} for:
          <br />
          <pre>
            <code className="text-dark">{client.proposal}</code>
          </pre>
        </p>
      )
    } else {
      paraList.push(<p className="ti-compact">{line}</p>)
    }
  }
  return paraList
}

export class Filing extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/eu/central/lobbying')
    super.setUniqueKeys(['identification_code'])
  }

  render() {
    const filing = this.state.filing

    function renderLobbyingEntity() {
      const title = 'Lobbying Entity'
      const label = 'lobbying-entity'

      const annotation = filing.annotation.vendor

      const memberOfList = stringsToParagraphs(filing.structure.isMemberOf)
      const organisationList = stringsToParagraphs(
        filing.structure.organisationMembers
      )

      const features = [
        <ui.TableKeyValue k="Name" v={annotation.origText} />,
        <ui.TableKeyValue k="Entity Type" v={filing.entity_form} />,
        <ui.TableKeyValue k="Category" v={filing.registration_category} />,
        filing.website_url ? (
          <ui.TableKeyValue
            k="Website"
            v={
              <a href={util.prefixURL(filing.website_url)}>
                {filing.website_url}
              </a>
            }
          />
        ) : undefined,
        filing.head_office ? (
          <ui.TableKeyValue
            k="Head Office"
            v={formAddress(filing.head_office)}
          />
        ) : undefined,
        filing.eu_office ? (
          <ui.TableKeyValue k="EU Office" v={formAddress(filing.eu_office)} />
        ) : undefined,
        <ui.TableKeyValue k="Goals" v={filing.goals} />,
        filing.structure.isMemberOf ? (
          <ui.TableKeyValue k="Member Of" v={memberOfList} />
        ) : undefined,
        filing.structure.organisationMembers ? (
          <ui.TableKeyValue k="Associated Organizations" v={organisationList} />
        ) : undefined,
        filing.groupings ? (
          <ui.TableKeyValue k="Grouping" v={filing.groupings} />
        ) : undefined,
      ]

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderActivity() {
      const title = 'Activity'
      const label = 'activity'

      let features = []

      const proposalList = stringsToParagraphs(filing.legislative_proposals)
      const supportedList = renderSupportedForums(filing.supported_forums)
      const levelsList = stringsToParagraphs(filing.levels_of_interest)
      const interestList = stringsToParagraphs(filing.interests)

      ui.includeTableKeyValue(
        features,
        'Identification Code',
        <pre>
          <code className="text-dark">{filing.identification_code}</code>
        </pre>
      )
      ui.includeTableKeyValue(
        features,
        'Last Update',
        <pre>
          <code className="text-dark">
            {util.dateWithoutTimeLex(filing.last_update_date)}
          </code>
        </pre>,
        true,
        filing.last_update_date
      )
      ui.includeTableKeyValue(
        features,
        'Registration Date',
        <pre>
          <code className="text-dark">
            {util.dateWithoutTimeLex(filing.registration_date)}
          </code>
        </pre>,
        true,
        filing.registration_date
      )
      if (proposalList.length) {
        ui.includeTableKeyValue(features, 'Legislative Proposals', proposalList)
      }
      if (!listIsEmpty(supportedList)) {
        ui.includeTableKeyValue(
          features,
          'Supported Forums / Platforms',
          supportedList
        )
      }
      ui.includeTableKeyValue(
        features,
        'Communication Activities',
        filing.communications
      )
      ui.includeTableKeyValue(
        features,
        'Interest Represented',
        filing.interest_represented
      )
      if (filing.interests.length) {
        ui.includeTableKeyValue(features, 'Interests', interestList)
      }
      if (filing.levels_of_interest.length) {
        ui.includeTableKeyValue(features, 'Levels of Interest', levelsList)
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderFinancialData() {
      const title = 'Financial Data'
      const label = 'financial-data'

      let features = []

      ui.includeTableKeyValue(
        features,
        'New Organization',
        filing.financial_data.newOrganisation
      )
      if (filing.financial_data.closedYear) {
        const closedYear = filing.financial_data.closedYear
        ui.includeTableKeyValue(
          features,
          'Start of Closed Year',
          util.dateWithoutTimeLex(closedYear.startDate)
        )
        if (closedYear.totalBudget?.absoluteCost != undefined) {
          ui.includeTableKeyValue(
            features,
            'Total Budget',
            util.numberWithCommas(closedYear.totalBudget.absoluteCost)
          )
        }
        const fundingSourcesList = stringsToParagraphs(
          closedYear.fundingSources
        )
        if (fundingSourcesList.length) {
          ui.includeTableKeyValue(
            features,
            'Funding Sources',
            fundingSourcesList
          )
        }
        if (closedYear.totalAnnualRevenue?.range != undefined) {
          const annualRevenue = closedYear.totalAnnualRevenue
          const currency = annualRevenue['@currency']
          const lowerBound =
            currency + util.numberWithCommas(annualRevenue.range.min)
          const upperBound =
            currency + util.numberWithCommas(annualRevenue.range.max)
          let revenueLine = ''
          if (
            annualRevenue.range.min != undefined &&
            annualRevenue.range.max != undefined
          ) {
            revenueLine = `${lowerBound} to ${upperBound}`
          } else if (annualRevenue.range.min != undefined) {
            revenueLine = `>= ${lowerBound}`
          } else if (annualRevenue.range.max != undefined) {
            revenueLine = `<= ${upperBound}`
          } else {
            revenueLine = 'N/A'
          }
          ui.includeTableKeyValue(features, 'Annual Revenue', revenueLine)
        }
        if (closedYear.costs?.range) {
          const costs = closedYear.costs
          if (costs.range.min != undefined) {
            ui.includeTableKeyValue(
              features,
              'Closed Year Minimum Cost',
              costs['@currency'] + ' ' + util.numberWithCommas(costs.range.min)
            )
          }
          if (costs.range.max != undefined) {
            ui.includeTableKeyValue(
              features,
              'Closed Year Maximum Cost',
              costs['@currency'] + ' ' + util.numberWithCommas(costs.range.max)
            )
          }
        }
        const intermediaryLines = yearToIntermediaryParagraphs(closedYear)
        if (intermediaryLines.length) {
          ui.includeTableKeyValue(
            features,
            'Closed Year Intermediaries',
            intermediaryLines
          )
        }
        const clientLines = yearToClientParagraphs(closedYear)
        if (clientLines.length) {
          ui.includeTableKeyValue(features, 'Closed Year Clients', clientLines)
        }
        const grantLines = yearToGrantParagraphs(closedYear)
        if (grantLines.length) {
          ui.includeTableKeyValue(features, 'Closed Year Grants', grantLines)
        }
        const contributionLines = yearToContributionParagraphs(closedYear)
        if (contributionLines.length) {
          ui.includeTableKeyValue(
            features,
            'Closed Year Contributions',
            contributionLines
          )
        }
      }
      if (filing.financial_data.currentYear) {
        const currentYear = filing.financial_data.currentYear
        if (currentYear.totalBudget?.absoluteCost != undefined) {
          ui.includeTableKeyValue(
            features,
            'Total Budget of Current Year',
            util.numberWithCommas(currentYear.totalBudget.absoluteCost)
          )
        }
        if (currentYear.costs?.range) {
          const costs = currentYear.costs
          if (costs.range.min != undefined) {
            ui.includeTableKeyValue(
              features,
              'Current Year Minimum Cost',
              costs['@currency'] + ' ' + util.numberWithCommas(costs.range.min)
            )
          }
          if (costs.range.max != undefined) {
            ui.includeTableKeyValue(
              features,
              'Current Year Maximum Cost',
              costs['@currency'] + ' ' + util.numberWithCommas(costs.range.max)
            )
          }
        }
        const intermediaryLines = yearToIntermediaryParagraphs(currentYear)
        if (intermediaryLines.length) {
          ui.includeTableKeyValue(
            features,
            'Current Year Intermediaries',
            intermediaryLines
          )
        }
        const clientLines = yearToClientParagraphs(currentYear)
        if (clientLines.length) {
          ui.includeTableKeyValue(features, 'Current Year Clients', clientLines)
        }
        const grantLines = yearToGrantParagraphs(currentYear)
        if (grantLines.length) {
          ui.includeTableKeyValue(features, 'Current Year Grants', grantLines)
        }
        const contributionLines = yearToContributionParagraphs(currentYear)
        if (contributionLines.length) {
          ui.includeTableKeyValue(
            features,
            'Current Year Contributions',
            contributionLines
          )
        }
      }

      ui.includeTableKeyValue(
        features,
        'Complementary Information',
        filing.financial_data.complementaryInformation
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderMembers() {
      const title = 'Members'
      const label = 'members'

      let features = []

      ui.includeTableKeyValue(features, 'Total Members', filing.members.members)
      ui.includeTableKeyValue(
        features,
        'Number of 100% Members',
        filing.members.members100Percent
      )
      ui.includeTableKeyValue(
        features,
        'Number of 75% Members',
        filing.members.members75Percent
      )
      ui.includeTableKeyValue(
        features,
        'Number of 50% Members',
        filing.members.members50Percent
      )
      ui.includeTableKeyValue(
        features,
        'Number of 25% Members',
        filing.members.members25Percent
      )
      ui.includeTableKeyValue(
        features,
        'Equivalent number of Full-time Employees',
        filing.members.membersFTE
      )
      ui.includeTableKeyValue(
        features,
        'Member Information',
        filing.members.infoMembers
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    const title = `[${util.dateWithoutTimeLex(filing.last_update_date)}]: ${
      filing.name
    }`

    const datasetURL =
      'https://data.europa.eu/data/datasets/transparency-register?locale=en'
    const header = (
      <p className="card-text text-start">
        <div className="mb-2">
          <a className="btn btn-light" href={datasetURL} role="button">
            Full dataset
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderLobbyingEntity())
    accordionItems.push(renderActivity())
    accordionItems.push(renderFinancialData())
    accordionItems.push(renderMembers())
    let accordion = (
      <Accordion id="eu-lobbying-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-center mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Filing filing={filing} />
}
