import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

export function buildName(obj) {
  if (!obj) {
    return ''
  }
  let namePieces = []
  const nameKeys = ['name', 'firstName', 'lastName']
  for (const nameKey of nameKeys) {
    if (nameKey in obj) {
      namePieces.push(obj[nameKey].trim())
    }
  }
  return namePieces.join(' ').trim()
}

export function activityDescription(activity) {
  if (!activity) {
    return ''
  }

  if (
    activity.category &&
    activity.text &&
    activity.category != activity.text
  ) {
    return `${activity.category} (${activity.text})`
  } else if (activity.category) {
    return activity.category
  } else if (activity.text) {
    return activity.text
  } else {
    return ''
  }
}

export class Filing extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/ca/lobbying/communication')
    super.setUniqueKeys(['comm_id'])
  }

  render() {
    const filing = this.state.filing

    const datasetURL = 'https://lobbycanada.gc.ca/en/open-data/'

    function renderClient() {
      const label = 'client'

      const clientName = filing.name.toLowerCase()
      if (!(clientName in filing.annotation.entities)) {
        console.log(`Could not retrieve client annotation for "${clientName}".`)
        return undefined
      }
      const annotation = filing.annotation.entities[clientName]
      const title = `Client: ${annotation.text.toUpperCase()}`

      let features = []
      ui.includeTableKeyValue(
        features,
        'Name',
        util.coerceUpperCase(annotation.text)
      )
      ui.includeTableKeyValue(features, 'ID', filing.client_id)

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderRegistrant() {
      const label = 'registrant'

      const registrant = filing.registrant
      if (!registrant) {
        return undefined
      }
      const registrantName = buildName(registrant).toLowerCase()
      if (!(registrantName in filing.annotation.entities)) {
        console.log(
          `Could not retrieve registrant annotation for "${registrantName}".`
        )
      }
      const annotation = filing.annotation.entities[registrantName]
      const title = `Registrant: ${annotation.text.toUpperCase()}`

      let features = []
      ui.includeTableKeyValue(
        features,
        'Name',
        util.coerceUpperCase(annotation.text)
      )
      ui.includeTableKeyValue(features, 'ID', filing.client_id)

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderOfficeHolder() {
      const label = 'officeHolder'
      const title = 'Office Holder'

      let features = []

      const officeHolder = filing.dpoh
      if (!officeHolder) {
        return undefined
      }
      const holderName = buildName(officeHolder).toLowerCase()
      if (!(holderName in filing.annotation.entities)) {
        console.log(
          `Could not retrieve office holder annotation for "${holderName}".`
        )
        return undefined
      }
      const annotation = filing.annotation.entities[holderName]
      features.push(ui.renderEntityAnnotation(annotation, 'Name'))
      ui.includeTableKeyValue(features, 'Title', officeHolder.title)
      ui.includeTableKeyValue(features, 'Branch', officeHolder.branch)
      ui.includeTableKeyValue(features, 'Institution', officeHolder.institution)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderDates() {
      const label = 'dates'
      const title = 'Dates'

      let features = []

      ui.includeTableKeyDate(features, 'Date', filing.date)
      ui.includeTableKeyDate(
        features,
        'Submission Date',
        filing.submission_date
      )
      ui.includeTableKeyDate(features, 'Posted Date', filing.posted_date)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderActivity() {
      const label = 'activity'
      const title = 'Activity'

      let features = []

      ui.includeTableKeyValue(features, 'Communication ID', filing.comm_id)
      ui.includeTableKeyValue(features, 'Communication Type', filing.type)

      ui.includeTableKeyValue(
        features,
        'Description',
        activityDescription(filing.activity)
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    let title = filing.name
    const header = (
      <p className="card-text text-start">
        <div className="mb-2">
          <a className="btn btn-light" href={datasetURL} role="button">
            Full dataset
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderClient())
    accordionItems.push(renderRegistrant())
    accordionItems.push(renderOfficeHolder())
    accordionItems.push(renderDates())
    accordionItems.push(renderActivity())
    let accordion = (
      <Accordion
        id="ca-lobbying-communications-accordion"
        items={accordionItems}
      />
    )

    return (
      <div className="card text-center mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Filing filing={filing} />
}
