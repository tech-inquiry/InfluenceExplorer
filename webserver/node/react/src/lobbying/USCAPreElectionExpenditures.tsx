import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kLobbyingPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USCAPreElectionExpenditures/renderFiling'

import * as util from '../utilities/util'

export class USCAPreElectionExpenditures extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL +
      '/' +
      kLobbyingPaths['US CA pre-election expenditures'].get

    this.tableID = 'ti-table-us-ca-pre-election-expenditures'
    this.title = (
      <span>
        <img src="/logos/state_of_california.svg" className="dropdown-flag" />
        US CA Pre-Election Expenditures
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label
      ? this.props.label
      : 'us-ca-pre-election-expenditures'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Date Filed' },
      { title: 'Filer' },
      { title: 'Candidate' },
      { title: 'Description' },
      {
        title: 'Amount',
        render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
      },
      { title: 'Ballot Measure' },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    function buildName(entityObj) {
      const nameKeys = ['prefix', 'first_name', 'last_name', 'suffix']
      let namePieces = []
      nameKeys.forEach(function (nameKey) {
        if (nameKey in entityObj && entityObj[nameKey]) {
          namePieces.push(entityObj[nameKey])
        }
      })
      if (namePieces.length) {
        return namePieces.join(' ')
      } else {
        return null
      }
    }

    const filedDate = filing.cover
      ? util.dateWithoutTimeLex(filing.cover['filed_date'])
      : ''

    let filersStr = ''
    if (filing.cover && filing.cover.filer) {
      filersStr = buildName(filing.cover.filer)
    }

    let ballotMeasure = ''
    if (
      filing.cover &&
      filing.cover.ballot_measure &&
      filing.cover.ballot_measure.name
    ) {
      ballotMeasure = filing.cover.ballot_measure.name
    }

    let candidatesStr = ''
    if (filing.cover && filing.cover.candidate) {
      candidatesStr = buildName(filing.cover.candidate)
    }

    let description = ''
    if (
      filing.pre_election_expenditure &&
      filing.pre_election_expenditure.description
    ) {
      description = filing.pre_election_expenditure.description
    }

    let amount = 0
    if (
      filing.pre_election_expenditure &&
      'amount' in filing.pre_election_expenditure
    ) {
      amount = filing.pre_election_expenditure.amount
    }

    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      filedDate,
      filersStr,
      candidatesStr,
      description,
      amount,
      ballotMeasure,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
