import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kLobbyingPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USFECIndividual/renderFiling'

import * as util from '../utilities/util'

const kNote = undefined

export class USFECIndividual extends React.Component<any, any> {
  urlBase: string
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL + '/' + kLobbyingPaths['US FEC individual'].get

    this.tableID = 'ti-table-us-fec-individual'
    this.title = (
      <span>
        <img
          src="/logos/government_of_the_united_states.svg"
          className="dropdown-flag"
        />
        US federal campaign contributions (Federal Election Commission)
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label ? this.props.label : 'us-fec-individual'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Date', visible: true },
      {
        title: 'Amount',
        visible: true,
        render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
      },
      { title: 'Contributor', visible: true },
      { title: 'Employer', visible: true },
      { title: 'Occupation', visible: true },
      { title: 'Candidate', visible: true },
      { title: 'Committee', visible: true },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    const date = util.dateWithoutTimeLex(filing.transaction_date)
    const amount = Number(filing.transaction_amount)
    const candidate = util.coerceUpperCase(filing.candidate_name)
    const committee = util.coerceUpperCase(filing.committee_name)
    const contributor = util.coerceUpperCase(filing.contributor_name)
    const employer = util.coerceUpperCase(filing.employer)
    const occupation = util.coerceUpperCase(filing.occupation)
    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      date,
      amount,
      contributor,
      employer,
      occupation,
      candidate,
      committee,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="us_fec_indv"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        note={kNote}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
