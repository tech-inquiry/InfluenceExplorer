import * as React from 'react'

import { CALobbyingCommunications } from './CACommunications'
import { CALobbyingRegistrations } from './CARegistrations'
import { EULobbying } from './EULobbying'
import { USFECIndividual } from './USFECIndividual'
import { USOPRCommunications } from './USOPRCommunications'
import { USOPRContributions } from './USOPRContributions'
import { USCALobbyingActivity } from './USCAActivity'
import { USCALobbyingContributions } from './USCAContributions'
import { USCAPreElectionExpenditures } from './USCAPreElectionExpenditures'
import { kLobbyingPaths } from '../utilities/api'
import * as ui from '../utilities/ui'
import { coerce } from '../utilities/util'
import { appConfig } from '../utilities/constants'

class LobbyingRow extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = coerce(this.props.label, 'lobbying')
    this.sourceLabel = this.sourceLabel.bind(this)
    this.sourceModalId = this.sourceModalId.bind(this)
  }

  sourceLabel(source) {
    return `${this.label}-${source}`
  }

  sourceModalId(source) {
    return ui.modalId(this.sourceLabel(source))
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    const isEntity = profile != undefined
    const sources = this.props.sources

    const that = this

    const capImage = (
      <div
        className="mx-auto mb-0 ti-cap-img"
        style={{
          opacity: 1.0,
          height: '100px',
          maxWidth: '250px',
          backgroundImage: 'url("https://techinquiry.org/logos/influence.png")',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: '50% 50%',
          backgroundSize: 'contain',
        }}
      ></div>
    )

    const target = isEntity
      ? profile.entityAnnotations[info.query].stylizedText
      : info.query
    const footerText = (
      <>
        Tech Inquiry has political influence records for{' '}
        <span className="fst-italic">{target}</span> from{' '}
        <span className="fw-bold">
          {sources.length + ' '}
          {sources.length > 1 ? 'sources' : 'source'}
        </span>
        .
      </>
    )

    // We will place all of the modal objects up top.
    const enabledSources = [
      'CA communications',
      'CA registrations',
      'EU Lobbying',
      'US FEC individual',
      'US OPR communications',
      'US OPR contributions',
      'US CA activity',
      'US CA contributions',
      'US CA pre-election expenditures',
    ]

    function sourceToItem(source, index) {
      return ui.dropdownSource(
        source,
        index,
        enabledSources,
        kLobbyingPaths,
        that.sourceModalId
      )
    }

    return (
      <ui.RowHelper
        capImage={capImage}
        button={ui.getCardDropdownButton(
          'Political Influence',
          `${this.label}-dropdownMenuButton`,
          sources.map(sourceToItem)
        )}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

class LobbyingModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = coerce(this.props.label, 'lobbying')
    this.sourceLabel = this.sourceLabel.bind(this)
  }

  sourceLabel(source) {
    return `${this.label}-${source}`
  }

  render() {
    const info = this.props.info
    const isEntity = info.profile != undefined
    const sourceObjects = (
      <div>
        <CALobbyingCommunications
          label={this.sourceLabel('CA communications')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <CALobbyingRegistrations
          label={this.sourceLabel('CA registrations')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <EULobbying
          label={this.sourceLabel('EU Lobbying')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USFECIndividual
          label={this.sourceLabel('US FEC individual')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USOPRCommunications
          label={this.sourceLabel('US OPR communications')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USOPRContributions
          label={this.sourceLabel('US OPR contributions')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USCALobbyingActivity
          label={this.sourceLabel('US CA activity')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USCALobbyingContributions
          label={this.sourceLabel('US CA contributions')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
        <USCAPreElectionExpenditures
          label={this.sourceLabel('US CA pre-election expenditures')}
          query={info.query}
          isAgency={false}
          isEntity={isEntity}
          isTag={info.isTag}
        />
      </div>
    )

    return sourceObjects
  }
}

export function insertCardAndModal(
  info,
  sources,
  cards: any[],
  modals: any[]
): void {
  if (!sources.length) {
    return
  }
  cards.push(<LobbyingRow info={info} sources={sources} />)
  modals.push(<LobbyingModal info={info} />)
}
