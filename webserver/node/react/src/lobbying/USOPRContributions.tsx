import * as React from 'react'
import * as jQuery from 'jquery'

import { FilingFeed } from '../FilingFeed'

import { kLobbyingPaths } from '../utilities/api'
import { appConfig } from '../utilities/constants'
import { renderFiling } from './USOPRContributions/renderFiling'

import * as util from '../utilities/util'

const kNote = [
  <p className="ti-note lh-sm">
    Lobbying contribution data is sourced daily from the{' '}
    <a href="https://lda.senate.gov/api/">Rest API</a> for U.S. Senate Lobbying
    Disclosure Act Reports. While Tech Inquiry generally combines together the
    records across the various names corporation have lobbied through, our
    profiles for U.S. politicians typically do not have such extensive lists. As
    a result, we recommend that investigations into particular politicians be
    performed through <a href="https://www.opensecrets.org/">OpenSecrets</a>{' '}
    instead.
  </p>,
]

export class USOPRContributions extends React.Component<any, any> {
  urlBase: string
  maxDescriptionLength: number
  tableID: string
  label: string
  columns: any[]
  title: any

  constructor(props) {
    super(props)
    this.urlBase =
      appConfig.apiURL + '/' + kLobbyingPaths['US OPR contributions'].get

    this.maxDescriptionLength = 256
    this.tableID = 'ti-table-us-opr-contributions'
    this.title = (
      <span>
        <img
          src="/logos/government_of_the_united_states.svg"
          className="dropdown-flag"
        />
        US federal campaign contributions (Senate Office of Public Records)
      </span>
    )

    this.itemToRow = this.itemToRow.bind(this)
    this.itemsToTableData = this.itemsToTableData.bind(this)
    this.retrieveURL = this.retrieveURL.bind(this)

    this.label = this.props.label ? this.props.label : 'us-opr-contributions'

    this.columns = [
      { title: 'Index', visible: false },
      { title: 'Received', visible: true },
      { title: 'Period', visible: true },
      { title: 'Type', visible: true },
      { title: 'Registrant Name', visible: true },
      { title: 'Lobbyist', visible: true },
      { title: 'PACs', visible: true },
      { title: 'Payee', visible: true },
      {
        title: 'Amount',
        visible: true,
        render: jQuery.fn.dataTable.render.number(',', '.', 2, '$'),
      },
      { title: 'Entire Filing', visible: false },
    ]
  }

  itemToRow(filing, filingIndex) {
    const received = util.dateWithoutTimeLex(filing.dt_posted)
    const period = `${filing.filing_year}, ${filing.filing_period}`
    const type = filing.filing_type

    let payee = ''
    let amount = 0
    for (const contribution of filing.contribution_items) {
      amount += Number(contribution.amount)
      if (payee) {
        payee += ', '
      }
      payee += contribution.payee_name
    }
    payee = util.trimLabel(payee, this.maxDescriptionLength)

    const registrant = filing.registrant ? filing.registrant.name : ''
    const lobbyist = filing.lobbyist
      ? util.lobbyistNameFromObject(filing.lobbyist)
      : ''
    const pacs = filing.pacs.join(', ')

    const entireFiling = JSON.stringify(filing)
    return [
      filingIndex,
      received,
      period,
      type,
      registrant,
      lobbyist,
      pacs,
      payee,
      amount,
      entireFiling,
    ]
  }

  itemsToTableData(items) {
    return items.map(this.itemToRow)
  }

  retrieveURL() {
    return util.getRetrieveURL(
      this.urlBase,
      this.props.query,
      this.props.isAgency,
      this.props.isEntity,
      this.props.isTag
    )
  }

  render() {
    return (
      <FilingFeed
        tableName="us_opr_contributions_filing"
        title={this.title}
        label={this.label}
        tableID={this.tableID}
        note={kNote}
        apiURL={this.retrieveURL()}
        columns={this.columns}
        itemsToTableData={this.itemsToTableData}
        renderItem={renderFiling}
      />
    )
  }
}
