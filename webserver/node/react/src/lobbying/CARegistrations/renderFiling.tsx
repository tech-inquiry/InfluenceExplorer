import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

export function buildName(obj) {
  if (!obj) {
    return ''
  }
  let namePieces = []
  const nameKeys = ['name', 'firstName', 'lastName']
  for (const nameKey of nameKeys) {
    if (nameKey in obj) {
      namePieces.push(obj[nameKey])
    }
  }
  return namePieces.join(' ')
}

function buildAddress(obj) {
  let addressObj = {
    streetLines: [],
    city: obj.city,
    state: obj.state,
    postalCode: obj.postal,
    country: obj.country,
  }
  return <ui.Address address={addressObj} />
}

export class Filing extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/ca/lobbying/registration')
    super.setUniqueKeys(['registration_id'])
  }

  render() {
    const filing = this.state.filing
    const maxTitleLength = 200

    const datasetURL = 'https://lobbycanada.gc.ca/en/open-data/'

    function renderHeader() {
      const label = 'registrationMetadata'
      const title = 'Registration Metadata'

      let features = []

      const registration = filing.registration

      ui.includeTableKeyValue(
        features,
        'Registration ID',
        filing.registration_id
      )
      ui.includeTableKeyValue(
        features,
        'Registration Parent ID',
        registration.parentID
      )
      ui.includeTableKeyValue(
        features,
        'Registration Number',
        registration.number
      )
      ui.includeTableKeyValue(features, 'Registration Type', registration.type)
      ui.includeTableKeyValue(
        features,
        'Registration Version',
        registration.version
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderClient() {
      const label = 'client'
      const title = 'Client'

      let features = []

      let annotation = undefined

      const client = filing.client
      if (client) {
        annotation = filing.annotation.entities[buildName(client).toLowerCase()]
        ui.includeTableKeyValue(features, 'Name', buildName(client))
        ui.includeTableKeyValue(features, 'Primary ID', client.primaryNumber)
        ui.includeTableKeyValue(
          features,
          'Secondary ID',
          client.secondaryNumber
        )
        ui.includeTableKeyValue(
          features,
          'Phone',
          util.formatPhoneNumber(client.phone)
        )
        ui.includeTableKeyValue(
          features,
          'Fax',
          util.formatPhoneNumber(client.fax)
        )
        ui.includeTableKeyValue(features, 'Has Parents?', client.hasParents)
        ui.includeTableKeyValue(
          features,
          'Has Subsidiaries?',
          client.hasSubsidiaries
        )
        ui.includeTableKeyValue(features, 'Is Coalition?', client.isCoalition)
        ui.includeTableKeyValue(features, 'Is Controlled?', client.isControlled)
        ui.includeTableKeyValue(
          features,
          'Is Foreign Funded?',
          client.isForeignFunded
        )
        ui.includeTableKeyDate(features, 'Year End Date', client.yearEndDate)
      }

      const principalRep = filing.principal_representative
      if (principalRep) {
        // TODO(Jack Poulson): Add a principal rep. annotation?
        const principalRepName = buildName(principalRep)
        ui.includeTableKeyValue(
          features,
          "Principal Representative's Name",
          principalRepName
        )
        ui.includeTableKeyValue(
          features,
          "Principal Representative's Title",
          principalRep.position
        )
      }

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderBeneficiary(beneficiary, index) {
      const title = `Beneficiary ${index + 1}`
      const label = `beneficiary-${index + 1}`

      let features = []
      const annotation =
        filing.annotation.entities[buildName(beneficiary).toLowerCase()]
      if (annotation) {
        features.push(ui.renderEntityAnnotation(annotation, 'Name'))
      } else {
        ui.includeTableKeyValue(features, 'Name', buildName(beneficiary))
      }
      ui.includeTableKeyValue(features, 'Address', buildAddress(beneficiary))
      ui.includeTableKeyValue(features, 'Type', beneficiary.type)

      const body = (
        <table className="table ti-key-value-table">
          <tbody>{features}</tbody>
        </table>
      )

      return (
        <AccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          body={body}
        />
      )
    }

    function renderBeneficiaries() {
      const label = 'beneficiaries'
      const title = 'Beneficiaries'

      let features = []

      for (const [i, beneficiary] of filing.beneficiaries.entries()) {
        features.push(renderBeneficiary(beneficiary, i))
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderRegistrant() {
      const label = 'registrant'
      const title = 'Registrant'

      let features = []

      const firm = filing.firm
      const registrant = filing.registrant

      let annotation = undefined
      if (firm) {
        annotation = filing.annotation.entities[buildName(firm).toLowerCase()]
        if (annotation) {
          ui.includeTableKeyValue(
            features,
            'Consulting Firm',
            annotation.origText.toUpperCase()
          )
        } else {
          ui.includeTableKeyValue(features, 'Consulting Firm', buildName(firm))
        }
        ui.includeTableKeyValue(
          features,
          'Registrant Position at Consulting Firm',
          firm.registrantPosition
        )
        if (!registrant || registrant.address != firm.address) {
          ui.includeTableKeyValue(
            features,
            'Consulting Firm Address',
            firm.address
          )
        }
      }

      if (registrant) {
        const registrantAnnotation =
          filing.annotation.entities[buildName(registrant).toLowerCase()]
        if (registrantAnnotation) {
          ui.includeTableKeyValue(
            features,
            'Registrant Name',
            registrantAnnotation.origText.toUpperCase()
          )
          if (!annotation) {
            annotation = registrantAnnotation
          }
        }
        ui.includeTableKeyValue(
          features,
          'Registrant Address',
          registrant.address
        )
        ui.includeTableKeyValue(
          features,
          'Registrant Position',
          registrant.position
        )
        ui.includeTableKeyValue(
          features,
          'Registrant Phone',
          util.formatPhoneNumber(registrant.phone)
        )
        ui.includeTableKeyValue(
          features,
          'Registrant Fax',
          util.formatPhoneNumber(registrant.fax)
        )
        ui.includeTableKeyValue(
          features,
          'Registrant Account Number',
          registrant.accountNumber
        )
        ui.includeTableKeyValue(
          features,
          'Registrant Payment Contingent?',
          registrant.contingentPayment
        )
      }

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderDates() {
      const label = 'dates'
      const title = 'Dates'

      const registration = filing.registration
      if (!registration) {
        return undefined
      }

      let features = []

      ui.includeTableKeyDate(features, 'Start Date', registration.startDate)
      ui.includeTableKeyDate(features, 'End Date', registration.endDate)
      ui.includeTableKeyValue(features, 'Posted Date', registration.postedDate)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderActivity() {
      const label = 'activity'
      const title = 'Activity'

      const activity = filing.activity
      if (!activity) {
        return undefined
      }

      let features = []

      ui.includeTableKeyValue(features, 'Category', activity.category)
      ui.includeTableKeyValue(features, 'Topics', activity.topics)
      ui.includeTableKeyValue(features, 'Description', activity.description)

      const commTech = filing.comm_techniques
      if (commTech.length) {
        const techniques = (
          <ul style={{ paddingLeft: '1em' }}>
            {commTech.map(function (technique) {
              let description = technique.type
              if (technique.text) {
                description += ` (${technique.text})`
              }
              return <li>{description}</li>
            })}
          </ul>
        )
        ui.includeTableKeyValue(features, 'Techniques', techniques)
      }

      const commInst = filing.comm_institutions
      const fundInst = filing.funder_institutions
      if (commInst.length) {
        const list = (
          <ul style={{ paddingLeft: '1em' }}>
            {commInst.map(function (institution) {
              return <li>{institution}</li>
            })}
          </ul>
        )
        ui.includeTableKeyValue(
          features,
          'Institutions Communicated With',
          list
        )
      }
      if (fundInst.length) {
        const list = (
          <ul style={{ paddingLeft: '1em' }}>
            {fundInst.map(function (institution) {
              return (
                <li>
                  {`${institution.name} ` +
                    `(CA$${util.numberWithCommas(institution.amount)})`}
                </li>
              )
            })}
          </ul>
        )
        ui.includeTableKeyValue(features, 'Institutions Funded By', list)
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    const title = util.trimLabel(
      `${buildName(filing.registrant)}: ${buildName(filing.client)}`,
      maxTitleLength
    )
    const header = (
      <p className="card-text text-start">
        <div className="mb-2">
          <a className="btn btn-light" href={datasetURL} role="button">
            Full dataset
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderHeader())
    accordionItems.push(renderClient())
    accordionItems.push(renderBeneficiaries())
    accordionItems.push(renderRegistrant())
    accordionItems.push(renderDates())
    accordionItems.push(renderActivity())
    let accordion = (
      <Accordion
        id="ca-lobbying-registrations-accordion"
        items={accordionItems}
      />
    )

    return (
      <div className="card text-center mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Filing filing={filing} />
}
