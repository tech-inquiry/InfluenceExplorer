import * as React from 'react'

import { Accordion, AccordionItem, SimpleAccordionItem } from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function prettyPrintFilingPeriod(period) {
  if (period == 'mid_year') {
    return 'Mid-Year (Jan 1 - Jun 30)'
  } else if (period == 'year_end') {
    return 'Year-End (July 1 - Dec 31)'
  } else {
    return period
  }
}

function prettyPrintFilingType(type) {
  if (type == 'YY') {
    return 'Year-End Report'
  } else if (type == 'YA') {
    return 'Year-End Amendment'
  } else if (type == 'MM') {
    return 'Mid-Year Report'
  } else if (type == 'MA') {
    return 'Mid-Year Amendment'
  } else {
    return 'Report'
  }
}

function jsonURLFromUUID(uuid) {
  return `https://lda.senate.gov/api/v1/contributions/${uuid}`
}

function documentURLFromUUID(uuid) {
  return `https://lda.senate.gov/filings/public/contribution/${uuid}/print/`
}

// TODO(Jack Poulson): Avoid this being duplicated in USOPRCommunication
function renderUSLobbyingAddress(obj) {
  let address = {
    streetLines: [],
    city: obj.city,
    state: obj.state_display,
    postalCode: obj.zip,
    country: obj.country_display,
  }
  if (obj.address) {
    address.streetLines.push(obj.address)
  } else if (obj.address_1) {
    address.streetLines.push(obj.address_1)
    if (obj.address_2) {
      address.streetLines.push(obj.address_2)
    }
    if (obj.address_3) {
      address.streetLines.push(obj.address_3)
    }
    if (obj.address_4) {
      address.streetLines.push(obj.address_4)
    }
  }
  return <ui.Address address={address} />
}

export class Contributions extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/us/central/lobbying/opr/contributions')
    super.setUniqueKeys(['filing_uuid'])
  }

  render() {
    const filing = this.state.filing

    function renderFiler() {
      const title = 'Filer'
      const label = 'filer'

      let features = []

      ui.includeTableKeyValue(features, 'Filer Type', filing.filer_type_display)
      ui.includeTableKeyValue(
        features,
        'Filer Address',
        renderUSLobbyingAddress(filing)
      )
      ui.includeTableKeyValue(features, 'Contact Name', filing.contact_name)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderRegistrant() {
      const title = 'Registrant'
      const label = 'registrant'

      let features = []

      const registrant = filing.registrant
      const registrantAnnotation = filing.annotation.registrant
      features.push(ui.renderEntityAnnotation(registrantAnnotation, 'Name'))
      ui.includeTableKeyValue(
        features,
        'ID',
        registrant.url ? (
          <a href={registrant.url}>{registrant.id}</a>
        ) : (
          registrant.id
        )
      )
      ui.includeTableKeyValue(features, 'Description', registrant.description)

      ui.includeTableKeyValue(
        features,
        'Address',
        renderUSLobbyingAddress(registrant)
      )

      if (registrant.contact_name) {
        const contactAnnotation = filing.annotation.contact
        features.push(
          ui.renderEntityAnnotation(contactAnnotation, 'Point of Contact Name')
        )
      }
      ui.includeTableKeyValue(
        features,
        'Point of Contact Phone',
        registrant.contact_telephone
      )

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderLobbyist() {
      const title = 'Lobbyist'
      const label = 'lobbyist'

      const lobbyist = filing.lobbyist
      const lobbyistAnnotation = filing.annotation.lobbyist
      if (!(lobbyist || filing.filer_type == 'lobbyist')) {
        return undefined
      }

      let features = []

      if (lobbyistAnnotation.stylizedText) {
        features.push(ui.renderEntityAnnotation(lobbyistAnnotation, 'Name'))
      } else {
        ui.includeTableKeyValue(
          features,
          'Name',
          util.lobbyistNameFromObject(lobbyist)
        )
      }
      ui.includeTableKeyValue(features, 'ID', lobbyist.id)
      if (filing.filer_type == 'lobbyist') {
        ui.includeTableKeyValue(
          features,
          'Address',
          renderUSLobbyingAddress(filing)
        )
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderComments() {
      const title = 'Comments'
      const label = 'comments'

      let features = []

      ui.includeTableKeyValue(features, 'Comments', filing.comments)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderContributionItems() {
      const title = 'Contribution Items'
      const label = 'contributionItems'

      const items = filing.contribution_items
      const annotations = filing.annotation.contributions

      if (!items.length) {
        return undefined
      }

      let accordionItems = []
      for (const [i, item] of items.entries()) {
        const annotation = annotations[i]

        const itemTitlePrefix = item.date
          ? `[${util.dateWithoutTime(new Date(item.date))}] `
          : ''
        const itemTitleAmount = item.amount
          ? '$' + util.numberWithCommas(item.amount)
          : ''
        let itemTitle = undefined
        if (item.payee_name && item.honoree_name) {
          itemTitle =
            `${itemTitlePrefix}${itemTitleAmount} from ` +
            `${item.payee_name} to ${item.honoree_name}`
        } else if (item.payee_name) {
          itemTitle =
            `${itemTitlePrefix}${itemTitleAmount} from ` + `${item.payee_name}`
        } else if (item.honree_name) {
          itemTitle =
            `${itemTitlePrefix}${itemTitleAmount} to ` + `${item.honoree_name}`
        } else {
          itemTitle = `${itemTitlePrefix}${itemTitleAmount}`
        }

        const itemLabel = `contributionItem-${i + 1}`

        let features = []

        ui.includeTableKeyValue(features, 'Payee', item.payee_name)
        ui.includeTableKeyValue(features, 'Honoree', item.honoree_name)
        ui.includeTableKeyValue(
          features,
          'Amount',
          '$' + util.numberWithCommas(item.amount),
          true,
          item.amount
        )
        ui.includeTableKeyDate(features, 'Date', item.date)
        ui.includeTableKeyValue(features, 'Contributor', item.contributor_name)
        ui.includeTableKeyValue(
          features,
          'Contribution Type',
          item.contribution_type_display
        )

        accordionItems.push(
          <SimpleAccordionItem
            tableClassName="table ti-key-value-table"
            key={itemTitle}
            label={itemLabel}
            title={itemTitle}
            closed={true}
            features={features}
          />
        )
      }

      let accordion = (
        <Accordion
          id="us-opr-contribution-items-accordion"
          items={accordionItems}
        />
      )

      return (
        <AccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          body={accordion}
        />
      )
    }

    function renderPACs() {
      const title = 'PACS'
      const label = 'pacs'

      if (!filing.pacs.length) {
        return undefined
      }

      const body = (
        <ul
          style={{ listStyle: 'none', paddingLeft: '1em', textAlign: 'left' }}
        >
          {filing.pacs.map(function (pac, index) {
            return <li key={index}>{pac}</li>
          })}
        </ul>
      )

      return (
        <AccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          body={body}
        />
      )
    }

    const title =
      filing.filing_year +
      ' ' +
      prettyPrintFilingPeriod(filing.filing_period) +
      ' Lobbying ' +
      prettyPrintFilingType(filing.filing_type) +
      ` (Posted: ${util.dateWithoutTime(new Date(filing.dt_posted))})`

    const header = filing.filing_uuid ? (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light me-2"
            href={documentURLFromUUID(filing.filing_uuid)}
            role="button"
          >
            View Original
          </a>
          <a
            className="btn btn-light"
            href={jsonURLFromUUID(filing.filing_uuid)}
            role="button"
          >
            View JSON
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    ) : undefined

    let accordionItems = []
    accordionItems.push(renderFiler())
    accordionItems.push(renderRegistrant())
    accordionItems.push(renderLobbyist())
    accordionItems.push(renderPACs())
    accordionItems.push(renderComments())
    accordionItems.push(renderContributionItems())
    let accordion = (
      <Accordion id="us-opr-contributions-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2 text-start">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Contributions filing={filing} />
}
