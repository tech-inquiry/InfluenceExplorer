import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { appConfig } from '../../utilities/constants'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

// TODO(Jack Poulson): Finish converting to local logos.
// TODO(Jack Poulson): Move this into a JSON file.
const kGovernmentEntities = {
  '1': {
    text: 'Senate',
    logo: '/logos/united_states_senate.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Senate',
  },
  '2': {
    text: 'House of Representatives',
    logo: '/logos/united_states_house_of_representatives.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_House_of_Representatives',
  },
  '3': {
    text: 'Architect of the Capitol (AOC)',
  },
  '5': {
    text: 'Government Accountability Office (GAO)',
    logo: '/logos/us_government_accountability_office.svg',
    url: 'https://en.wikipedia.org/wiki/Government_Accountability_Office',
  },
  '6': {
    text: 'Government Printing Office (GPO)',
  },
  '7': {
    text: 'Library of Congress (LOC)',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/b/be/Logo_of_the_United_States_Library_of_Congress.svg',
    url: 'https://en.wikipedia.org/wiki/Library_of_Congress',
  },
  '8': {
    text: 'Congressional Budget Office (CBO)',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/1/15/Logo_of_the_United_States_Congressional_Budget_Office.png',
    url: 'https://en.wikipedia.org/wiki/Congressional_Budget_Office',
  },
  '9': {
    text: 'President of the U.S.',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/3/36/Seal_of_the_President_of_the_United_States.svg',
    url: 'https://en.wikipedia.org/wiki/President_of_the_United_States',
  },
  '10': {
    text: 'Vice President of the U.S.',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/6/6a/Seal_of_the_Vice_President_of_the_United_States.svg',
    url: 'https://en.wikipedia.org/wiki/Vice_President_of_the_United_States',
  },
  '11': {
    text: 'Executive Office of the President (EOP)',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/b/bc/Seal_of_the_Executive_Office_of_the_President_of_the_United_States_2014.svg',
    url: 'https://en.wikipedia.org/wiki/Executive_Office_of_the_President_of_the_United_States',
  },
  '12': {
    text: 'White House Office',
    logo: '/logos/eop_white_house_office.png',
    url: 'https://en.wikipedia.org/wiki/White_House_Office',
  },
  '13': {
    text: 'Office of the Vice President of the United States',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/6/6a/Seal_of_the_Vice_President_of_the_United_States.svg',
    url: 'https://en.wikipedia.org/wiki/Vice_President_of_the_United_States',
  },
  '14': {
    text: 'Council of Economic Advisers (CEA)',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/b/bc/Seal_of_the_Executive_Office_of_the_President_of_the_United_States_2014.svg',
    url: 'https://en.wikipedia.org/wiki/Council_of_Economic_Advisers',
  },
  '15': {
    text: 'Council on Environmental Quality (CEQ)',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/6/6f/US-CouncilOnEnvironmentalQuality-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Council_on_Environmental_Quality',
  },
  '16': {
    text: 'National Security Council',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/b/bc/Seal_of_the_Executive_Office_of_the_President_of_the_United_States_2014.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_National_Security_Council',
  },
  '17': {
    text: 'Office of Administration',
  },
  '18': {
    text: 'Office of Management & Budget (OMB)',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/7/73/US-OfficeOfManagementAndBudget-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Office_of_Management_and_Budget',
  },
  '19': {
    text: 'Office of National Drug Control Policy',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/6/61/US-ONDCP-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Office_of_National_Drug_Control_Policy',
  },
  '20': {
    text: 'Office of Policy Development',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/b/bc/Seal_of_the_Executive_Office_of_the_President_of_the_United_States_2014.svg',
    url: 'https://www.usgovernmentmanual.gov/Agency.aspx?EntityId=9QWs93j9/oI=&ParentEId=p0fnvDxExmY=&EType=/sbLHImeIYk=&AspxAutoDetectCookieSupport=1',
  },
  '21': {
    text: 'Office of Science & Technology Policy',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/7/73/US-OfficeOfManagementAndBudget-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Office_of_Science_and_Technology_Policy',
  },
  '22': {
    text: 'U.S. Trade Representative',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/7/73/US-OfficeOfManagementAndBudget-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Office_of_the_United_States_Trade_Representative',
  },
  '23': {
    text: 'United States Department of Agriculture',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/0/04/Logo_of_the_United_States_Department_of_Agriculture.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_Agriculture',
  },
  '24': {
    text: 'Department of Commerce',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/1/1a/Seal_of_the_United_States_Department_of_Commerce.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_Commerce',
  },
  '25': {
    text: 'Department of Defense',
    logo: '/logos/united_states_department_of_defense.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_Defense',
  },
  '26': {
    text: 'Office of the Secretary of Defense',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/e/e0/United_States_Department_of_Defense_Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Office_of_the_Secretary_of_Defense',
  },
  '27': {
    text: 'Joint Chiefs of Staff',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/d/d4/Joint_Chiefs_of_Staff_seal.svg',
    url: 'https://en.wikipedia.org/wiki/Joint_Chiefs_of_Staff',
  },
  '28': {
    text: 'Defense Department Field Activities',
  },
  '29': {
    text: 'Department of the Air Force',
    logo: '/logos/united_states_department_of_the_air_force.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Air_Force',
  },
  '30': {
    text: 'Department of the Army',
    logo: '/logos/united_states_department_of_the_army.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Army',
  },
  '31': {
    text: 'Department of the Navy',
    logo: '/logos/united_states_department_of_the_navy.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Navy',
  },
  '32': {
    text: 'Department of Education',
    logo: '/logos/united_states_department_of_education.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_Education',
  },
  '33': {
    text: 'Department of Energy',
    logo: '/logos/united_states_department_of_energy.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_Energy',
  },
  '34': {
    text: 'Department of Health & Human Services',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/7/79/Seal_of_the_United_States_Department_of_Health_and_Human_Services.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_Health_and_Human_Services',
  },
  '35': {
    text: 'Housing & Urban Development, Dept of (HUD)',
    logo: '/logos/united_states_department_of_housing_and_urban_development.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_Housing_and_Urban_Development',
  },
  '36': {
    text: 'Department of the Interior',
    logo: '/logos/united_states_department_of_the_interior.png',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_the_Interior',
  },
  '37': {
    text: 'Department of Justice',
    logo: '/logos/united_states_department_of_justice.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_Justice',
  },
  '38': {
    text: 'Department of Labor',
    logo: '/logos/united_states_department_of_labor.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_Labor',
  },
  '39': {
    text: 'Department of State',
    logo: '/logos/united_states_department_of_state.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_State',
  },
  '40': {
    text: 'Department of Transportation',
    logo: '/logos/united_states_department_of_transportation.svg',
    url: 'https://en.wikipedia.org/wiki/Department_of_Transportation',
  },
  '41': {
    text: 'Department of the Treasury',
    logo: '/logos/united_states_department_of_the_treasury.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_the_Treasury',
  },
  '42': {
    text: 'Department of Veterans Affairs',
    logo: '/logos/united_states_department_of_veterans_affairs.png',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_Veterans_Affairs',
  },
  '44': {
    text: 'Central Intelligence Agency',
    logo: '/logos/central_intelligence_agency.svg',
    url: 'https://en.wikipedia.org/wiki/Central_Intelligence_Agency',
  },
  '45': {
    text: 'Commodity Futures Trading Commission',
    logo: '/logos/commodity_futures_trading_commission.svg',
    url: 'https://en.wikipedia.org/wiki/Commodity_Futures_Trading_Commission',
  },
  '46': {
    text: 'Consumer Product Safety Commission',
    logo: '/logos/consumer_product_safety_commission.svg',
    url: 'https://en.wikipedia.org/wiki/U.S._Consumer_Product_Safety_Commission',
  },
  '47': {
    text: 'Corporation for National & Community Service ',
  },
  '48': {
    text: 'Defense Nuclear Facilities Safety Board',
  },
  '49': {
    text: 'Environmental Protection Agency',
    logo: '/logos/environmental_protection_agency.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Environmental_Protection_Agency',
  },
  '50': {
    text: 'Equal Employment Opportunity Commission',
    logo: '/logos/equal_employment_opportunity_commission.svg',
    url: 'https://en.wikipedia.org/wiki/Equal_Employment_Opportunity_Commission',
  },
  '51': {
    text: 'Export-Import Bank of the United States',
    logo: '/logos/export_import_bank_of_the_united_states.svg',
    url: 'https://en.wikipedia.org/wiki/Export%E2%80%93Import_Bank_of_the_United_States',
  },
  '52': {
    text: 'Farm Credit Administration',
  },
  '53': {
    text: 'Federal Communications Commission',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/c/c9/FCC_New_Logo.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Communications_Commission',
  },
  '54': {
    text: 'Federal Deposit Insurance Corporation',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/e/e6/US-FDIC-Logo.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Deposit_Insurance_Corporation',
  },
  '55': {
    text: 'Federal Election Commission',
  },
  '56': {
    text: 'Federal Emergency Management Agency',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/6/67/FEMA_logo.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Emergency_Management_Agency',
  },
  '57': {
    text: 'Federal Housing Finance Agency',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/1/12/Seal_of_the_United_States_Federal_Housing_Finance_Agency.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Housing_Finance_Agency',
  },
  '58': {
    text: 'Federal Labor Relations Authority',
  },
  '59': {
    text: 'Federal Maritime Commission',
  },
  '60': {
    text: 'Federal Mediation & Conciliation Service',
  },
  '62': {
    text: 'Federal Reserve System',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/1/1a/Seal_of_the_United_States_Federal_Reserve_System.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Reserve',
  },
  '63': {
    text: 'Federal Retirement Thrift Investment Board',
  },
  '64': {
    text: 'Federal Trade Commission',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/6/69/Seal_of_the_United_States_Federal_Trade_Commission.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Trade_Commission',
  },
  '65': {
    text: 'General Services Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/8/89/US-GeneralServicesAdministration-Logo.svg',
    url: 'https://en.wikipedia.org/wiki/General_Services_Administration',
  },
  '67': {
    text: 'Merit Systems Protection Board',
  },
  '68': {
    text: 'National Aeronautics & Space Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/e/e5/NASA_logo.svg',
    url: 'https://en.wikipedia.org/wiki/NASA',
  },
  '69': {
    text: 'National Archives & Records Administration',
  },
  '71': {
    text: 'National Credit Union Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/e/e0/NCUA_official_seal.svg',
    url: 'https://en.wikipedia.org/wiki/National_Credit_Union_Administration',
  },
  '72': {
    text: 'National Foundation on the Arts & Humanities',
  },
  '73': {
    text: 'National Labor Relations Board',
    logo: '/logos/nlrb.png',
    url: 'https://en.wikipedia.org/wiki/National_Labor_Relations_Board',
  },
  '74': {
    text: 'National Mediation Board',
    logo: '/logos/national_mediation_board.gif',
    url: 'https://en.wikipedia.org/wiki/National_Mediation_Board',
  },
  '75': {
    text: 'AMTRAK - Natl Railroad Passenger Corporation',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/e/eb/Amtrak_logo.svg',
    url: 'https://en.wikipedia.org/wiki/Amtrak',
  },
  '76': {
    text: 'National Science Foundation',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/7/7e/NSF_logo.png',
    url: 'https://en.wikipedia.org/wiki/National_Science_Foundation',
  },
  '77': {
    text: 'National Transportation Safety Board',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/1/16/Seal_of_the_United_States_National_Transportation_Safety_Board.svg',
    url: 'https://en.wikipedia.org/wiki/National_Transportation_Safety_Board',
  },
  '78': {
    text: 'Nuclear Regulatory Commission',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/b/b4/US-NuclearRegulatoryCommission-Logo.svg',
    url: 'https://en.wikipedia.org/wiki/Nuclear_Regulatory_Commission',
  },
  '81': {
    text: 'Office of Personnel Management',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/0/0a/Seal_of_the_United_States_Office_of_Personnel_Management.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Office_of_Personnel_Management',
  },
  '82': {
    text: 'Office of Special Counsel',
  },
  '85': {
    text: 'Pension Benefit Guaranty Corporation',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/f/fb/US-PensionBenefitGuarantyCorp-Logo.svg',
    url: 'https://en.wikipedia.org/wiki/Pension_Benefit_Guaranty_Corporation',
  },
  '87': {
    text: 'Railroad Retirement Board',
    logo: '/logos/railroad_retirement_board.png',
    url: 'https://en.wikipedia.org/wiki/Railroad_Retirement_Board',
  },
  '88': {
    text: 'Securities & Exchange Commission',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/1/1c/Seal_of_the_United_States_Securities_and_Exchange_Commission.svg',
    url: 'https://en.wikipedia.org/wiki/U.S._Securities_and_Exchange_Commission',
  },
  '90': {
    text: 'Small Business Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/2/25/U.S._Small_Business_Administration_logo.svg',
    url: 'https://en.wikipedia.org/wiki/Small_Business_Administration',
  },
  '91': {
    text: 'Social Security Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/2/26/US-SocialSecurityAdmin-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Social_Security_Administration',
  },
  '92': {
    text: 'Tennessee Valley Authority',
  },
  '93': {
    text: 'U.S. Trade & Development Agency',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/0/0f/Logo_USTDA_color_RGB.png',
    url: 'https://en.wikipedia.org/wiki/United_States_Trade_and_Development_Agency',
  },
  '95': {
    text: 'U.S. Commission on Civil Rights',
  },
  '98': {
    text: 'U.S. International Trade Commission',
    logo: '/logos/united_states_international_trade_commission.png',
    url: 'https://en.wikipedia.org/wiki/United_States_International_Trade_Commission',
  },
  '99': {
    text: 'U.S. Postal Service',
    logo: 'https://upload.wikimedia.org/wikipedia/en/3/37/United_States_Postal_Service_Logo.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Postal_Service',
  },
  '100': {
    text: 'Legal Services Corporation',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/2/20/LSC_logo_square.jpg',
    url: 'https://en.wikipedia.org/wiki/Legal_Services_Corporation',
  },
  '101': {
    text: 'Smithsonian Institution',
  },
  '105': {
    text: 'Bureau of the Census',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/8/85/Seal_of_the_United_States_Census_Bureau.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Census_Bureau',
  },
  '106': {
    text: 'Bureau of Economic Analysis',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/9/9a/Bea-final-logo-blue-backing.png',
    url: 'https://en.wikipedia.org/wiki/Bureau_of_Economic_Analysis',
  },
  '107': {
    text: 'Bureau of Industry and Security',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/0/03/US-DOC-BureauOfIndustryAndSecurity-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Bureau_of_Industry_and_Security',
  },
  '108': {
    text: 'Economic Development Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/3/37/Seal_of_the_United_States_Economic_Development_Administration.svg',
    url: 'https://en.wikipedia.org/wiki/Economic_Development_Administration',
  },
  '109': {
    text: 'International Trade Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/9/91/US-InternationalTradeAdministration-Logo.svg',
    url: 'https://en.wikipedia.org/wiki/International_Trade_Administration',
  },
  '111': {
    text: 'National Oceanic & Atmospheric Administration',
  },
  '112': {
    text: 'National Telecommunications & Information Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/5/59/US-NationalTelecommunicationsAndInformationAdministration-Logo.svg',
    url: 'https://en.wikipedia.org/wiki/National_Telecommunications_and_Information_Administration',
  },
  '113': {
    text: 'Patent & Trademark Office',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/7/75/Seal_of_the_United_States_Patent_and_Trademark_Office.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Patent_and_Trademark_Office',
  },
  '116': {
    text: 'Defense Advanced Research Projects Agency',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/c/ce/DARPA_Logo_2010.png',
    url: 'https://en.wikipedia.org/wiki/DARPA',
  },
  '117': {
    text: 'Defense Commissary Agency',
  },
  '118': {
    text: 'Defense Contract Audit Agency',
  },
  '119': {
    text: 'Defense Information Systems Agency',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/7/7b/US-DefenseInformationSystemsAgency-Logo.svg',
    url: 'https://en.wikipedia.org/wiki/Defense_Information_Systems_Agency',
  },
  '120': {
    text: 'Defense Intelligence Agency',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/5/56/Seal_of_the_U.S._Defense_Intelligence_Agency.svg',
    url: 'https://en.wikipedia.org/wiki/Defense_Intelligence_Agency',
  },
  '123': {
    text: 'Defense Logistics Agency',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/2/23/Seal_of_the_Defense_Logistics_Agency.png',
    url: 'https://en.wikipedia.org/wiki/Defense_Logistics_Agency',
  },
  '126': {
    text: 'National Geospatial Intelligence Agency',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/9/9f/US-NationalGeospatialIntelligenceAgency-2008Seal.svg',
    url: 'https://en.wikipedia.org/wiki/National_Geospatial-Intelligence_Agency',
  },
  '127': {
    text: 'National Security Agency',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/8/8d/Seal_of_the_U.S._National_Security_Agency.svg',
    url: 'https://en.wikipedia.org/wiki/National_Security_Agency',
  },
  '128': {
    text: 'On-Site Inspection Agency',
  },
  '130': {
    text: 'Administration on Aging',
  },
  '131': {
    text: 'Administration for Children & Families',
  },
  '132': {
    text: 'Agency for Healthcare Research & Quality',
  },
  '133': {
    text: 'Agency for Toxic Substances & Disease Registry',
  },
  '134': {
    text: 'Centers For Disease Control & Prevention',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/7/71/US_CDC_logo.svg',
    url: 'https://en.wikipedia.org/wiki/Centers_for_Disease_Control_and_Prevention',
  },
  '135': {
    text: 'Food & Drug Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/4/43/Logo_of_the_United_States_Food_and_Drug_Administration.svg',
    url: 'https://en.wikipedia.org/wiki/Food_and_Drug_Administration',
  },
  '136': {
    text: 'Centers For Medicare and Medicaid Services',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/2/2a/Centers_for_Medicare_and_Medicaid_Services_logo.svg',
    url: 'https://en.wikipedia.org/wiki/Centers_for_Medicare_%26_Medicaid_Services',
  },
  '137': {
    text: 'Health Resources & Services Administration',
    logo: '/logos/hhs_hrsa.png',
    url: 'https://en.wikipedia.org/wiki/Health_Resources_and_Services_Administration',
  },
  '138': {
    text: 'Indian Health Service',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/7/73/Indian_Health_Service_Logo.svg',
    url: 'https://en.wikipedia.org/wiki/Indian_Health_Service',
  },
  '139': {
    text: 'National Institutes of Health',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/c/c8/NIH_Master_Logo_Vertical_2Color.png',
    url: 'https://en.wikipedia.org/wiki/National_Institutes_of_Health',
  },
  '141': {
    text: 'Substance Abuse & Mental Health Services Administration',
    logo: '/logos/hhs_samhsa.png',
    url: 'https://www.samhsa.gov/',
  },
  '142': {
    text: 'U.S. Fish & Wildlife Service',
  },
  '143': {
    text: 'National Park Service',
  },
  '144': {
    text: 'U.S. Geological Survey',
  },
  '145': {
    text: 'Office of Surface Mining Reclamation & Enforcement',
  },
  '146': {
    text: 'Bureau of Indian Affairs',
  },
  '147': {
    text: 'Minerals Management Service',
  },
  '148': {
    text: 'Bureau of Land Management',
  },
  '149': {
    text: 'Bureau of Reclamation',
    logo: '/logos/interior_usbr-negate.png',
    url: 'https://en.wikipedia.org/wiki/United_States_Bureau_of_Reclamation',
  },
  '150': {
    text: 'Federal Bureau of Investigation',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/d/da/Seal_of_the_Federal_Bureau_of_Investigation.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Bureau_of_Investigation',
  },
  '151': {
    text: 'Bureau of Prisons',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/c/c7/Seal_of_the_Federal_Bureau_of_Prisons.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Bureau_of_Prisons',
  },
  '152': {
    text: 'U.S. Marshals Service',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/3/3f/Seal_of_the_United_States_Marshals_Service.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Marshals_Service',
  },
  '154': {
    text: 'Bureau of Citizenship & Immigration Services',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/a/a3/USCIS_logo_English.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Citizenship_and_Immigration_Services',
  },
  '155': {
    text: 'Drug Enforcement Administration',
    logo: '/logos/defense_dodea.png',
    url: 'https://en.wikipedia.org/wiki/Department_of_Defense_Education_Activity',
  },
  '156': {
    text: 'Office of Justice Program',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/8/81/US-OfficeOfJusticePrograms-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Office_of_Justice_Programs',
  },
  '157': {
    text: 'Bureau of Justice Assistance',
  },
  '158': {
    text: 'Bureau of Justice Statistics',
  },
  '159': {
    text: 'National Institute of Justice',
  },
  '160': {
    text: 'Office of Juvenile Justice & Delinquency Prevention',
  },
  '161': {
    text: 'Office for Victims of Crime',
  },
  '162': {
    text: 'Office on Violence Against Women',
  },
  '165': {
    text: 'Pension & Welfare Benefits Administration',
  },
  '167': {
    text: 'Occupational Safety & Health Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/4/44/US-OSHA-Logo.svg',
    url: 'https://en.wikipedia.org/wiki/Occupational_Safety_and_Health_Administration',
  },
  '168': {
    text: 'Mine Safety & Health Administration',
  },
  '169': {
    text: 'Bureau of Labor Statistics',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/7/76/Bureau_of_Labor_Statistics_logo.svg',
    url: 'https://en.wikipedia.org/wiki/Bureau_of_Labor_Statistics',
  },
  '170': {
    text: 'Veterans Employment & Training Service',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/c/c1/Seal_of_the_United_States_Department_of_Labor.svg',
    url: 'https://www.dol.gov/agencies/vets',
  },
  '171': {
    text: 'U.S. Coast Guard',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/e/e5/Mark_of_the_U.S._Coast_Guard.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Coast_Guard',
  },
  '172': {
    text: 'Federal Aviation Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/c/c8/Seal_of_the_United_States_Federal_Aviation_Administration.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Aviation_Administration',
  },
  '173': {
    text: 'Federal Highway Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/7/71/Logo_of_the_Federal_Highway_Administration.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Highway_Administration',
  },
  '174': {
    text: 'Federal Railroad Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/3/3e/Logo_of_the_United_States_Federal_Railroad_Administration.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Railroad_Administration',
  },
  '175': {
    text: 'National Highway Traffic Safety Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/3/31/Logo_of_the_United_States_National_Highway_Traffic_Safety_Administration.svg',
    url: 'https://en.wikipedia.org/wiki/National_Highway_Traffic_Safety_Administration',
  },
  '176': {
    text: 'Federal Transit Administration',
    logo: '/logos/transport_federal_transit_administration.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Transit_Administration',
  },
  '177': {
    text: 'Maritime Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/e/e6/US-MaritimeAdministration-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Maritime_Administration',
  },
  '178': {
    text: 'St. Lawrence Seaway Development Corporation',
  },
  '179': {
    text: 'Research & Special Programs Administration',
  },
  '180': {
    text: 'Bureau of Transportation Statistics',
  },
  '181': {
    text: 'Surface Transportation Board',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/a/a8/US-SurfaceTransportationBoard-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Surface_Transportation_Board',
  },
  '182': {
    text: 'Bureau of Alcohol Tobacco Firearms & Explosives',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/2/23/Seal_of_the_Bureau_of_Alcohol%2C_Tobacco%2C_Firearms_and_Explosives.svg',
    url: 'https://en.wikipedia.org/wiki/Bureau_of_Alcohol,_Tobacco,_Firearms_and_Explosives',
  },
  '183': {
    text: 'Office of the Comptroller of the Currency',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/a/a0/Seal_of_the_Office_of_the_Comptroller_of_the_Currency.svg',
    url: 'https://en.wikipedia.org/wiki/Office_of_the_Comptroller_of_the_Currency',
  },
  '184': {
    text: 'U.S. Customs & Border Protection',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/b/b3/Patch_of_the_U.S._Customs_and_Border_Protection.svg',
    url: 'https://en.wikipedia.org/wiki/U.S._Customs_and_Border_Protection',
  },
  '185': {
    text: 'Bureau of Engraving & Printing',
  },
  '188': {
    text: 'Internal Revenue Service',
    logo: '/logos/treasury_irs.png',
    url: 'https://en.wikipedia.org/wiki/Internal_Revenue_Service',
  },
  '189': {
    text: 'U.S. Mint',
  },
  '191': {
    text: 'U.S. Secret Service',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/b/b5/Logo_of_the_United_States_Secret_Service.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Secret_Service',
  },
  '193': {
    text: 'National Institute on Alcohol Abuse & Alcoholism',
  },
  '196': {
    text: 'U.S. Agency for International Development',
    logo: '/logos/united_states_agency_for_international_development.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Agency_for_International_Development',
  },
  '197': {
    text: 'Federal Energy Regulatory Commission',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/0/0d/Seal_of_the_United_States_Federal_Energy_Regulatory_Commission.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Energy_Regulatory_Commission',
  },
  '198': {
    text: 'National Economic Council',
    logo: '/logos/eop_white_house_office.png',
    url: 'https://en.wikipedia.org/wiki/National_Economic_Council_(United_States)',
  },
  '199': {
    text: 'U.S. Architectural & Transportation Barriers Compliance Board',
    logo: '/logos/us_access_board.jpeg',
    url: 'https://en.wikipedia.org/wiki/United_States_Access_Board',
  },
  '200': {
    text: 'U.S. Army Corps of Engineers',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/d/df/USACE.gif',
    url: 'https://en.wikipedia.org/wiki/United_States_Army_Corps_of_Engineers',
  },
  '201': {
    text: 'Department of Homeland Security',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/8/8a/Seal_of_the_United_States_Department_of_Homeland_Security.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Department_of_Homeland_Security',
  },
  '202': {
    text: 'Office of Faith-Based & Community Initiatives',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/b/bc/Seal_of_the_Executive_Office_of_the_President_of_the_United_States_2014.svg',
    url: 'https://en.wikipedia.org/wiki/White_House_Office_of_Faith-Based_and_Neighborhood_Partnerships',
  },
  '203': {
    text: 'Overseas Private Investment Corp',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/4/47/OPIC_logo2014_cmyk.png',
    url: 'https://en.wikipedia.org/wiki/OPIC',
  },
  '204': {
    text: 'Transportation Security Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/6/6b/Transportation_Security_Administration_logo.svg',
    url: 'https://en.wikipedia.org/wiki/Transportation_Security_Administration',
  },
  '206': {
    text: 'National Endowment for the Arts',
  },
  '207': {
    text: 'U.S. Copyright Office',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/a/a3/US-CopyrightOffice-Logo.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Copyright_Office',
  },
  '208': {
    text: 'National Indian Gaming Commission',
  },
  '209': {
    text: 'Institute of Museum and Library Services',
  },
  '210': {
    text: 'U.S. Marines',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/9/9f/Emblem_of_the_United_States_Marine_Corps.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Marine_Corps',
  },
  '212': {
    text: 'U.S. Forest Service',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/d/d0/Logo_of_the_United_States_Forest_Service.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Forest_Service',
  },
  '213': {
    text: 'Postal Regulatory Commission',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/8/81/U.S._Postal_Regulatory_Commission_Seal.jpg',
    url: 'https://en.wikipedia.org/wiki/Postal_Regulatory_Commission',
  },
  '214': {
    text: 'Advisory Council on Historic Preservation',
  },
  '215': {
    text: 'Director of National Intelligence',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/2/27/Seal_of_the_Office_of_the_Director_of_National_Intelligence.svg',
    url: 'https://en.wikipedia.org/wiki/Director_of_National_Intelligence#Office_of_the_Director_of_National_Intelligence',
  },
  '217': {
    text: 'Election Assistance Commission',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/3/3c/Seal_of_the_United_States_Election_Assistance_Commission.svg',
    url: 'https://en.wikipedia.org/wiki/Election_Assistance_Commission',
  },
  '218': {
    text: 'Federal Motor Carrier Safety Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/f/f5/US-FMCSA-Logo.svg',
    url: 'https://en.wikipedia.org/wiki/Federal_Motor_Carrier_Safety_Administration',
  },
  '219': {
    text: 'National Institute of Standards & Technology',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/e/ee/NIST_logo.svg',
    url: 'https://en.wikipedia.org/wiki/National_Institute_of_Standards_and_Technology',
  },
  '220': {
    text: 'U.S. Commission on International Religious Freedom',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/6/6a/Seal_of_the_United_States_Commission_on_International_Religious_Freedom.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_Commission_on_International_Religious_Freedom',
  },
  '221': {
    text: 'U.S. Chemical Safety & Hazard Investigation Board',
  },
  '222': {
    text: 'National Council on Disability',
  },
  '223': {
    text: 'National Endowment for the Humanities',
  },
  '225': {
    text: 'Alcohol & Tobacco Tax & Trade Bureau',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/4/4d/US-AlcoholAndTobaccoTaxAndTradeBureau-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Alcohol_and_Tobacco_Tax_and_Trade_Bureau',
  },
  '226': {
    text: 'Appalachian Regional Commission',
  },
  '227': {
    text: 'Architectural & Transportation Barriers Compliance Board',
    logo: '/logos/us_access_board.jpeg',
    url: 'https://en.wikipedia.org/wiki/United_States_Access_Board',
  },
  '230': {
    text: 'Defense Finance & Accounting Service',
  },
  '231': {
    text: 'Defense Security Cooperation Agency',
  },
  '232': {
    text: 'Employment & Training Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/c/c1/Seal_of_the_United_States_Department_of_Labor.svg',
    url: 'https://en.wikipedia.org/wiki/Employment_and_Training_Administration',
  },
  '233': {
    text: 'Federal Accounting Standards Advisory Board',
  },
  '235': {
    text: 'Financial Crimes Enforcement Network',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/c/c2/FinCEN.svg',
    url: 'https://en.wikipedia.org/wiki/Financial_Crimes_Enforcement_Network',
  },
  '236': {
    text: 'Defense Threat Reduction Agency',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/d/d7/US-DefenseThreatReductionAgency-Seal.svg',
    url: 'https://en.wikipedia.org/wiki/Defense_Threat_Reduction_Agency',
  },
  '237': {
    text: 'Financial Management Service',
  },
  '238': {
    text: 'Ginnie Mae',
  },
  '239': {
    text: 'Job Corps',
  },
  '242': {
    text: 'National Guard',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/2/28/Seal_of_the_United_States_National_Guard.svg',
    url: 'https://en.wikipedia.org/wiki/United_States_National_Guard',
  },
  '243': {
    text: 'National Institute of Mental Health',
  },
  '245': {
    text: 'Office of National AIDS Policy',
  },
  '247': {
    text: 'Pipeline & Hazardous Materials Safety Administration',
    logo: 'https://upload.wikimedia.org/wikipedia/commons/e/e3/USDOT_-_Pipeline_and_Hazardous_Materials_Safety_Administration_-_Logo.svg',
    url: 'https://en.wikipedia.org/wiki/Pipeline_and_Hazardous_Materials_Safety_Administration',
  },
  '248': {
    text: 'Risk Management Agency',
  },
  '249': {
    text: 'USA Freedom Corps',
  },
  '251': {
    text: 'U.S. Immigration & Customs Enforcement',
    logo: '/logos/united_states_immigration_and_customs_enforcement.svg',
    url: 'https://en.wikipedia.org/wiki/U.S._Immigration_and_Customs_Enforcement',
  },
  '252': {
    text: 'Voice of America',
  },
  '253': {
    text: 'Medicare Payment Advisory Commission',
  },
  '255': {
    text: 'Consumer Financial Protection Bureau',
    logo: '/logos/consumer_financial_protection_bureau.png',
    url: 'https://en.wikipedia.org/wiki/Consumer_Financial_Protection_Bureau',
  },
  '256': {
    text: 'Office of Technology Policy',
  },
}

function prettyPrintFilingPeriod(period) {
  if (period == 'first_quarter') {
    return 'First Quarter'
  } else if (period == 'second_quarter') {
    return 'Second Quarter'
  } else if (period == 'third_quarter') {
    return 'Third Quarter'
  } else if (period == 'fourth_quarter') {
    return 'Fourth Quarter'
  } else {
    return period
  }
}

function prettyPrintFilingType(type) {
  if (type == 'RR') {
    return 'Registration'
  } else if (type == 'RA') {
    return 'Registration Amendment'
  } else {
    return 'Report'
  }
}

function jsonURLFromUUID(uuid) {
  return `https://lda.senate.gov/api/v1/filings/${uuid}`
}

function documentURLFromUUID(uuid) {
  return `https://lda.senate.gov/filings/public/filing/${uuid}/print/`
}

// TODO(Jack Poulson): Avoid this being duplicated in USOPRContribution
function renderUSLobbyingAddress(obj) {
  let address = {
    streetLines: [],
    city: obj.city,
    state: obj.state_display,
    postalCode: obj.zip,
    country: obj.country_display,
  }
  if (obj.address) {
    address.streetLines.push(obj.address)
  } else if (obj.address_1) {
    address.streetLines.push(obj.address_1)
    if (obj.address_2) {
      address.streetLines.push(obj.address_2)
    }
    if (obj.address_3) {
      address.streetLines.push(obj.address_3)
    }
    if (obj.address_4) {
      address.streetLines.push(obj.address_4)
    }
  }
  return <ui.Address address={address} />
}

// Each session of Congress has lasted two years since the 77th session
// began in January of 1941. Bill numbers are reused between sessions,
// and so we index on the pairing of the session and bill name (e.g.,
// H.R. 6720 of the 116th session of Congress).
function sessionFromYear(year) {
  return 77 + Math.floor((year - 1941) / 2)
}

export class Issues extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/us/central/lobbying/opr/issues')
    super.setUniqueKeys(['filing_uuid'])
  }

  render() {
    const filing = this.state.filing

    function renderRegistrant() {
      const registrant = filing.registrant
      const registrantAnnotation = filing.annotation.registrant
      const contactAnnotation = filing.annotation.contact

      const title = `Registrant: ${registrant.name}`
      const label = 'registrant'

      const registrantID = registrant.id ? (
        registrant.url ? (
          <a href={registrant.url}>{registrant.id}</a>
        ) : (
          registrant.id
        )
      ) : undefined

      let pointOfContact = ''
      if (registrant.contact_name && registrant.contact_telephone) {
        pointOfContact = `${registrant.contact_name} (${registrant.contact_telephone})`
      } else if (registrant.contact_name) {
        pointOfContact = registrant.contact_name
      } else if (registrant.contact_telephone) {
        pointOfContact = registrant.contact_telephone
      }

      const features = [
        <ui.TableKeyValue
          k="Registrant Name"
          v={util.coerceUpperCase(registrantAnnotation.origText)}
        />,
        <ui.TableKeyValue k="Description" v={registrant.description} />,
        renderUSLobbyingAddress(registrant),
        <ui.TableKeyValue k="Point of Contact" v={pointOfContact} />,
        <ui.TableKeyValue k="Posted By" v={filing.posted_by_name} />,
        registrant.id ? (
          <ui.TableKeyValue k="ID" v={registrantID} />
        ) : undefined,
      ]

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={registrantAnnotation}
        />
      )
    }

    function renderClient() {
      const client = filing.client
      const annotation = filing.annotation.client

      const title = `Client: ${client.name}`
      const label = 'client'

      const clientID = client.id ? (
        client.url ? (
          <a href={client.url}>{client.id}</a>
        ) : (
          client.id
        )
      ) : undefined

      const features = [
        <ui.TableKeyValue
          k="Registrant Name"
          v={util.coerceUpperCase(annotation.origText)}
        />,
        <ui.TableKeyValue k="Description" v={client.description} />,
        <ui.TableKeyValue
          k="General Description"
          v={client.general_description}
        />,
        renderUSLobbyingAddress(client),
        client.id ? <ui.TableKeyValue k="ID" v={clientID} /> : undefined,
        <ui.TableKeyValue k="ID Relative to Registrant" v={client.client_id} />,
        client.client_government_entity ? (
          <ui.TableKeyValue
            k="Government Entity"
            v={[
              client.client_government_entity,
              <small className="text-muted">
                Senate OPR seems to inconsistently fill this field relative to
                whether the box is checked in the original document.
              </small>,
            ]}
          />
        ) : undefined,
      ]

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderAffiliatedOrganizations() {
      const affiliatedOrgs = filing.affiliated_organizations
      const orgAnnotations = filing.annotation.affiliatedOrgs

      const title = 'Affiliated Organizations'
      const label = 'affiliatedOrgs'

      let features = []

      for (const [i, org] of affiliatedOrgs.entries()) {
        const annotation = orgAnnotations[i]
        const orgFeatures = []

        orgFeatures.push(ui.renderEntityAnnotation(annotation, 'Name'))
        if (org.url) {
          ui.includeTableKeyValue(
            orgFeatures,
            'URL',
            <a href={util.prefixURL(org.url)}>{util.simplifyURL(org.url)}</a>
          )
        }
        orgFeatures.push(renderUSLobbyingAddress(org))

        ui.includeTableKeyValue(
          features,
          `Affiliated Org. ${i + 1}`,
          orgFeatures
        )
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderConvictionDisclosures() {
      const title = 'Conviction Disclosures'
      const label = 'convictionDisclosures'

      let features = []

      for (const [i, disclosure] of filing.conviction_disclosures.entries()) {
        const disclosureFeatures = []

        ui.includeTableKeyValue(
          disclosureFeatures,
          'Lobbyist Name',
          util.lobbyistNameFromObject(disclosure.lobbyist)
        )
        ui.includeTableKeyValue(
          disclosureFeatures,
          'Lobbyist ID',
          disclosure.lobbyist.id
        )
        ui.includeTableKeyValue(
          disclosureFeatures,
          'Description',
          disclosure.description
        )
        ui.includeTableKeyDate(disclosureFeatures, 'Date', disclosure.date)

        ui.includeTableKeyValue(
          features,
          `Disclosure ${i + 1}`,
          disclosureFeatures
        )
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function renderForeignEntities() {
      const title = 'Foreign Entities'
      const label = 'foreignEntities'

      let features = []

      for (const [i, entity] of filing.foreign_entities.entries()) {
        const entityFeatures = []

        ui.includeTableKeyValue(entityFeatures, 'Name', entity.name)
        entityFeatures.push(renderUSLobbyingAddress(entity))
        ui.includeTableKeyValue(
          entityFeatures,
          'Ownership Percentage',
          entity.ownership_percentage
        )
        ui.includeTableKeyValue(
          entityFeatures,
          'Contribution',
          entity.contribution
        )

        ui.includeTableKeyValue(features, `Entity ${i + 1}`, entityFeatures)
      }

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          features={features}
        />
      )
    }

    function getDescriptionAnnotation(description) {
      let senateRefs = {
        bills: [],
        resolutions: [],
        jointResolutions: [],
        concurrentResolutions: [],
      }
      let houseRefs = {
        bills: [],
        resolutions: [],
        jointResolutions: [],
        concurrentResolutions: [],
      }
      if (!description) {
        return {
          description: [],
          session: 'N/A',
          house: houseRefs,
          senate: senateRefs,
        }
      }

      // We default to assuming the session corresponds to the year of the
      // filing. But if a phrase of the form 'of 2017' appears, we compute the
      // session from the mentioned year. This handles cases such as those of
      //
      //   "H.R. 4477/S. 2135: Fix NICS Act of 2017" [filed in 2020].
      //
      let session = sessionFromYear(Number(filing.filing_year))
      const yearMatch = description.match(/of (\d{4})/)
      if (yearMatch) {
        session = sessionFromYear(Number(yearMatch[1]))
      }

      const billPrefix = `https://www.congress.gov/bill/${session}th-congress/`

      function insertURLs(parts: any[], refList: any[], matchRE, replaceFunc) {
        let newParts: any[] = []
        for (const part of parts) {
          if (typeof part == 'string') {
            let remaining = part
            let match = undefined
            while ((match = matchRE.exec(remaining))) {
              const matchBeg = match.index
              const matchEnd = match.index + match[0].length
              const preText = remaining.substring(0, matchBeg)
              const matchText = remaining.substring(matchBeg, matchEnd)
              const postText = remaining.substring(matchEnd)
              const item = replaceFunc(matchText)
              refList.push(item)
              newParts.push(preText)
              newParts.push(<a href={item.url}>{item.label}</a>)
              remaining = postText
            }
            newParts.push(remaining)
          } else {
            newParts.push(part)
          }
        }
        return newParts
      }

      let parts: any[] = [description]

      // Insert the Senate Concurrent Resolution links.
      parts = insertURLs(
        parts,
        senateRefs.concurrentResolutions,
        /\bs\.? ?con\.? ?res\.? *\d+/i,
        function (textMatch) {
          const urlLabel = 'senate-concurrent-resolution'
          const billLabel = 'S.Con.Res.'
          const billNumber = textMatch.match(/\d+/)[0]
          const url = `${billPrefix}${urlLabel}/${billNumber}`
          const label = `${billLabel} ${billNumber}`
          return { url: url, label: label }
        }
      )

      // Insert the Senate Joint Resolution links.
      parts = insertURLs(
        parts,
        senateRefs.jointResolutions,
        /\bs\.? ?j\.? ?res\.? *\d+/i,
        function (textMatch) {
          const urlLabel = 'senate-joint-resolution'
          const billLabel = 'S.J.Res.'
          const billNumber = textMatch.match(/\d+/)[0]
          const url = `${billPrefix}${urlLabel}/${billNumber}`
          const label = `${billLabel} ${billNumber}`
          return { url: url, label: label }
        }
      )

      // Insert the Senate Resolution links.
      parts = insertURLs(
        parts,
        senateRefs.resolutions,
        /\bs\.? ?res\.? *\d+/i,
        function (textMatch) {
          const urlLabel = 'senate-resolution'
          const billLabel = 'S.Res.'
          const billNumber = textMatch.match(/\d+/)[0]
          const url = `${billPrefix}${urlLabel}/${billNumber}`
          const label = `${billLabel} ${billNumber}`
          return { url: url, label: label }
        }
      )

      // Insert the Senate Bill links.
      // We would ideally use the following, but the negative look-behind
      // does not function in iOS.
      //  /(?<!u\.)\b(s\.?) *(\d+)/i;
      parts = insertURLs(
        parts,
        senateRefs.bills,
        /(?:u\.)?\bs\.? *\d+/i,
        function (textMatch) {
          if (textMatch.toLowerCase().startsWith('u')) {
            // We manually prevent matching 'u.s.' within the replace since we
            // cannot assume negative look-behind.
            return textMatch
          }
          const urlLabel = 'senate-bill'
          const billLabel = 'S.'
          const billNumber = textMatch.match(/\d+/)[0]
          const url = `${billPrefix}${urlLabel}/${billNumber}`
          const label = `${billLabel} ${billNumber}`
          return { url: url, label: label }
        }
      )

      // Insert the House Concurrent Resolution links.
      parts = insertURLs(
        parts,
        houseRefs.concurrentResolutions,
        /\bh\.? ?con\.? ?res\.? *\d+/i,
        function (textMatch) {
          const urlLabel = 'house-concurrent-resolution'
          const billLabel = 'H.Con.Res.'
          const billNumber = textMatch.match(/\d+/)[0]
          const url = `${billPrefix}${urlLabel}/${billNumber}`
          const label = `${billLabel} ${billNumber}`
          return { url: url, label: label }
        }
      )

      // Insert the House Joint Resolution links.
      parts = insertURLs(
        parts,
        houseRefs.jointResolutions,
        /\bh\.? ?j\.? ?res\.? *\d+/i,
        function (textMatch) {
          const urlLabel = 'house-joint-resolution'
          const billLabel = 'H.J.Res.'
          const billNumber = textMatch.match(/\d+/)[0]
          const url = `${billPrefix}${urlLabel}/${billNumber}`
          const label = `${billLabel} ${billNumber}`
          return { url: url, label: label }
        }
      )

      // Insert the House Resolution links.
      parts = insertURLs(
        parts,
        houseRefs.resolutions,
        /\bh\.? ?res\.? *\d+/i,
        function (textMatch) {
          const urlLabel = 'house-resolution'
          const billLabel = 'H.Res.'
          const billNumber = textMatch.match(/\d+/)[0]
          const url = `${billPrefix}${urlLabel}/${billNumber}`
          const label = `${billLabel} ${billNumber}`
          return { url: url, label: label }
        }
      )

      // Insert the House Bill links.
      parts = insertURLs(
        parts,
        houseRefs.bills,
        /\bh\.? ?r\.? *\d+/i,
        function (textMatch) {
          const urlLabel = 'house-bill'
          const billLabel = 'H.R.'
          const billNumber = textMatch.match(/\d+/)[0]
          const url = `${billPrefix}${urlLabel}/${billNumber}`
          const label = `${billLabel} ${billNumber}`
          return { url: url, label: label }
        }
      )

      return {
        description: parts,
        session: session,
        house: houseRefs,
        senate: senateRefs,
      }
    }

    function renderLobbyingActivities() {
      const title = 'Lobbying Activities'
      const label = 'lobbyingActivities'

      const areasOfCongress = {
        house: 'House',
        senate: 'Senate',
      }

      const billTypes = {
        bills: 'Bills',
        resolutions: 'Resolutions',
        jointResolutions: 'Joint Resolutions',
        concurrentResolutions: 'Concurrent Resolutions',
      }

      let billLinks = {
        house: {
          bills: new Set<string>(),
          resolutions: new Set<string>(),
          jointResolutions: new Set<string>(),
          concurrentResolutions: new Set<string>(),
        },
        senate: {
          bills: new Set<string>(),
          resolutions: new Set<string>(),
          jointResolutions: new Set<string>(),
          concurrentResolutions: new Set<string>(),
        },
      }

      function appendActivityBillLinks(annotation) {
        for (const areaOfCongress in areasOfCongress) {
          for (const billType in billTypes) {
            for (const item of annotation[areaOfCongress][billType]) {
              billLinks[areaOfCongress][billType].add(JSON.stringify(item))
            }
          }
        }
      }

      function includeBills(features, senateRefs, houseRefs) {
        for (const [billType, billTypeLabel] of Object.entries(billTypes)) {
          ui.includeTableKeyValue(
            features,
            `Senate ${billTypeLabel}`,
            renderBillList(senateRefs[billType])
          )
        }
        for (const [billType, billTypeLabel] of Object.entries(billTypes)) {
          ui.includeTableKeyValue(
            features,
            `House ${billTypeLabel}`,
            renderBillList(houseRefs[billType])
          )
        }
      }

      function includeLobbyists(features, activity, annotation) {
        for (const [i, lobbyist] of activity['lobbyists'].entries()) {
          if (!activity['lobbyists']) {
            continue
          }
          const lobbyistAnnotation = annotation['lobbyists'][i]
          let lobbyistFeatures = []
          if (lobbyistAnnotation.stylizedText) {
            lobbyistFeatures.push(
              ui.renderEntityAnnotation(lobbyistAnnotation, 'Name')
            )
          } else if (lobbyist.lobbyist) {
            ui.includeTableKeyValue(
              lobbyistFeatures,
              'Name',
              util.lobbyistNameFromObject(lobbyist.lobbyist)
            )
            ui.includeTableKeyValue(
              lobbyistFeatures,
              'ID',
              lobbyist.lobbyist.id
            )
          }
          ui.includeTableKeyValue(
            lobbyistFeatures,
            'Covered Position',
            lobbyist.covered_position
          )
          // TODO(Jack Poulson): Highlight with a background color?
          ui.includeTableKeyValue(lobbyistFeatures, 'New', lobbyist.new)

          ui.includeTableKeyValue(
            features,
            `Lobbyist ${i + 1}`,
            <table className="table ti-key-value-table">
              <tbody>{lobbyistFeatures}</tbody>
            </table>
          )
        }
      }

      function includeGovernmentEntities(features, activity) {
        for (const [i, entity] of activity.government_entities.entries()) {
          if (!entity) {
            continue
          }
          if (entity.id in kGovernmentEntities) {
            const entityProfile = kGovernmentEntities[entity.id]
            if ('url' in entityProfile) {
              if ('logo' in entityProfile) {
                const logo = entityProfile.logo.startsWith('/logos/')
                  ? util.concatenatePath(appConfig.root, entityProfile.logo)
                  : entityProfile.logo
                features.push(
                  <li>
                    <a href={entityProfile.url}>
                      <img
                        className="ti-us-gov-lobbying-entity-logo"
                        src={logo}
                      />
                      {entityProfile.text}
                    </a>
                  </li>
                )
              } else {
                features.push(
                  <li>
                    <a href={entityProfile.url}>{entityProfile.text}</a>
                  </li>
                )
              }
            } else {
              features.push(<li>{entityProfile.text}</li>)
            }
          } else {
            features.push(<li>{`${entity.id}: ${entity.name}`}</li>)
          }
        }
      }

      function renderBillList(items) {
        return items.length ? (
          <ul style={{ paddingLeft: '1em' }}>
            {items.map(function (item) {
              return (
                <li>
                  <a href={item.url}>{item.label}</a>
                </li>
              )
            })}
          </ul>
        ) : undefined
      }

      function billSetToList(billSet: Set<string>): any[] {
        return Array.from(billSet.values(), (item) => JSON.parse(item))
      }

      let accordionItems = []
      for (const [i, activity] of filing.lobbying_activities.entries()) {
        if (!activity) {
          continue
        }

        const activityTitle = `Area: ${activity.general_issue_code_display}`
        const activityLabel = `area-${i + 1}`
        let activityFeatures = []
        let activityAccordionItems = []

        const annotation = getDescriptionAnnotation(activity.description)
        appendActivityBillLinks(annotation)
        ui.includeTableKeyValue(
          activityFeatures,
          'Description',
          annotation.description
        )
        if (activity.foreign_entity_issues) {
          // TODO(Jack Poulson): Highlight with a particular color?
          ui.includeTableKeyValue(
            activityFeatures,
            'Foreign Entity Issues',
            activity.foreign_entity_issues
          )
        }

        // Add an accordion for the bills referenced in this activity
        let billFeatures = []
        const billTitle = 'Referenced Bills'
        const billLabel = `referencedBills-${i + 1}`
        includeBills(billFeatures, annotation.senate, annotation.house)
        activityAccordionItems.push(
          <SimpleAccordionItem
            tableClassName="table ti-key-value-table"
            key={billTitle}
            label={billLabel}
            title={billTitle}
            features={billFeatures}
          />
        )

        // Add an accordion for all the lobbyists
        let lobbyistFeatures = []
        const lobbyistTitle = 'Lobbyists'
        const lobbyistLabel = `lobbyists-${i + 1}`
        includeLobbyists(
          lobbyistFeatures,
          activity,
          filing.annotation.lobbyingActivities[i]
        )
        activityAccordionItems.push(
          <SimpleAccordionItem
            tableClassName="table ti-key-value-table"
            key={lobbyistTitle}
            label={lobbyistLabel}
            title={lobbyistTitle}
            features={lobbyistFeatures}
          />
        )

        // Add an accordion for all the government entities
        const govEntityTitle = 'Government Entities'
        const govEntityLabel = `govEntities-${i + 1}`
        let govEntityFeatures = []
        includeGovernmentEntities(govEntityFeatures, activity)
        if (govEntityFeatures.length) {
          const govEntityBody = (
            <ul
              style={{
                listStyle: 'none',
                paddingLeft: '1em',
                textAlign: 'left',
              }}
            >
              {govEntityFeatures}
            </ul>
          )
          activityAccordionItems.push(
            <AccordionItem
              tableClassName="table ti-key-value-table"
              key={govEntityTitle}
              label={govEntityLabel}
              title={govEntityTitle}
              body={govEntityBody}
            />
          )
        }

        const body = (
          <div>
            <table className="table ti-key-value-table">
              <tbody>{activityFeatures}</tbody>
            </table>
            <Accordion
              id={`us-opr-communications-activities-${i + 1}-accordion`}
              items={activityAccordionItems}
            />
          </div>
        )

        accordionItems.push(
          <AccordionItem
            tableClassName="table ti-key-value-table"
            key={activityTitle}
            label={activityLabel}
            title={activityTitle}
            closed={true}
            body={body}
          />
        )
      }

      // Add an accordion for all referenced bills.
      let billFeatures = []
      const billTitle = 'Referenced Bills'
      const billLabel = 'referencedBills'
      const senateRefs = {}
      const houseRefs = {}
      for (const billType in billTypes) {
        senateRefs[billType] = billSetToList(billLinks.senate[billType])
        houseRefs[billType] = billSetToList(billLinks.house[billType])
      }
      includeBills(billFeatures, senateRefs, houseRefs)
      accordionItems.push(
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={billTitle}
          label={billLabel}
          title={billTitle}
          closed={true}
          features={billFeatures}
        />
      )

      let accordion = (
        <Accordion
          id="us-opr-communications-activities-accordion"
          items={accordionItems}
        />
      )

      return (
        <AccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          body={accordion}
        />
      )
    }

    const title =
      filing.filing_year +
      ' ' +
      prettyPrintFilingPeriod(filing.filing_period) +
      ' Lobbying ' +
      prettyPrintFilingType(filing.filing_type) +
      ` (Posted: ${util.dateWithoutTime(new Date(filing.dt_posted))})`

    const header = filing.filing_uuid ? (
      <p className="card-text">
        <div className="mb-2">
          <a
            className="btn btn-light me-2"
            href={documentURLFromUUID(filing.filing_uuid)}
            role="button"
          >
            View Original
          </a>
          <a
            className="btn btn-light"
            href={jsonURLFromUUID(filing.filing_uuid)}
            role="button"
          >
            View JSON
          </a>
        </div>
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    ) : undefined

    let accordionItems = []
    accordionItems.push(renderClient())
    accordionItems.push(renderAffiliatedOrganizations())
    accordionItems.push(renderRegistrant())
    accordionItems.push(renderForeignEntities())
    accordionItems.push(renderConvictionDisclosures())
    accordionItems.push(renderLobbyingActivities())
    let accordion = (
      <Accordion id="us-opr-communications-accordion" items={accordionItems} />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Issues filing={filing} />
}
