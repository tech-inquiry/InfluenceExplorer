import * as React from 'react'

import {
  Accordion,
  AccordionItem,
  AccordionItemWithAnnotation,
  SimpleAccordionItem,
} from '../../Accordion'
import { FilingWithTags } from '../../FilingWithTags'
import * as ui from '../../utilities/ui'
import * as util from '../../utilities/util'

function buildName(entityObj): string {
  if (!entityObj) {
    return null
  }
  const nameKeys = ['prefix', 'first_name', 'last_name', 'suffix']
  let namePieces = []
  nameKeys.forEach(function (nameKey) {
    if (nameKey in entityObj && entityObj[nameKey]) {
      namePieces.push(entityObj[nameKey])
    }
  })
  if (namePieces.length) {
    return namePieces.join(' ')
  } else {
    return null
  }
}

function renderCalAccessLobbyingAddressFromObject(entity) {
  let address = {
    city: entity.city,
    state: entity.state,
    postalCode: entity.zipcode,
    phone: entity.phone,
    fax: entity.fax,
  }
  return <ui.Address address={address} />
}

export class Contributions extends FilingWithTags {
  constructor(props) {
    const uniqueKeys = ['filing_id', 'transaction_id', 'line_item']
    super(props)
    super.setEndpoint('/api/us/ca/lobbying/contributions')
    super.setUniqueKeys(uniqueKeys)
  }

  render() {
    const filing = this.state.filing
    const maxTitleLength = 200

    function renderCover() {
      const title = 'Cover Page'
      const label = 'coverPage'

      let features = []

      const cover = filing.cover
      const entityAnnotations = filing.annotation.entities

      let annotation = undefined

      if (cover.ballot_measure) {
        ui.includeTableKeyValue(
          features,
          'Ballot Measure',
          cover.ballot_measure.name
        )
        ui.includeTableKeyValue(
          features,
          'Ballot Measure Number',
          cover.ballot_measure.number_or_letter
        )
        ui.includeTableKeyValue(
          features,
          'Ballot Measure Jurisdiction',
          cover.ballot_measure.jurisdiction
        )
      }

      if (cover.filer) {
        const filerName = buildName(cover.filer)
        if (filerName) {
          const key = filerName.toLowerCase()
          if (key in entityAnnotations) {
            const entityAnnotation = entityAnnotations[key]
            ui.includeTableKeyValue(
              features,
              'Filer',
              util.coerceUpperCase(entityAnnotation.origText)
            )
            annotation = entityAnnotation
          } else {
            console.log(key)
          }
        }
      }
      if (cover.firm) {
        ui.includeTableKeyValue(
          features,
          'Firm Address',
          renderCalAccessLobbyingAddressFromObject(cover.firm)
        )
      }
      if (cover.filer_id) {
        ui.includeTableKeyValue(features, 'ID', cover.filer_id)
      } else if (cover.filer?.id) {
        ui.includeTableKeyValue(features, 'ID', cover.filer.id)
      }

      if (cover.treasurer) {
        const treasurerName = buildName(cover.treasurer)
        if (treasurerName) {
          const key = treasurerName.toLowerCase()
          if (key in entityAnnotations) {
            const entityAnnotation = entityAnnotations[key]
            ui.includeTableKeyValue(
              features,
              'Treasurer',
              util.coerceUpperCase(entityAnnotation.origText)
            )
            if (
              annotation == undefined ||
              (entityAnnotation.logo && !annotation.logo)
            ) {
              annotation = entityAnnotation
            }
          } else {
            console.log(key)
          }
        }
        ui.includeTableKeyValue(
          features,
          'Treasurer Address',
          renderCalAccessLobbyingAddressFromObject(cover.treasurer)
        )
      }

      if (cover.candidate) {
        const candidateName = buildName(cover.candidate)
        if (candidateName) {
          const key = candidateName.toLowerCase()
          if (key in entityAnnotations) {
            const entityAnnotation = entityAnnotations[key]
            ui.includeTableKeyValue(
              features,
              'Candidate',
              util.coerceUpperCase(entityAnnotation.origText)
            )
            if (
              annotation == undefined ||
              (entityAnnotation.logo && !annotation.logo)
            ) {
              annotation = entityAnnotation
            }
          } else {
            console.log(key)
          }
        }
        ui.includeTableKeyValue(
          features,
          'Candidate Address',
          renderCalAccessLobbyingAddressFromObject(cover.candidate)
        )
      }

      if (cover.office) {
        ui.includeTableKeyValue(features, 'Office Code', cover.office.code)
        ui.includeTableKeyValue(
          features,
          'Office Description',
          cover.office.description
        )
        ui.includeTableKeyValue(
          features,
          'Office District Number',
          cover.office.district_number
        )
        ui.includeTableKeyValue(
          features,
          'Office Jurisdiction Code',
          cover.office.jurisdiction_code
        )
        ui.includeTableKeyValue(
          features,
          'Office Jurisdiction Description',
          cover.office.jurisdiction_description
        )
        ui.includeTableKeyValue(
          features,
          'Office Sought or Held',
          cover.office.sought_or_held
        )
      }
      if (cover.yes_nos) {
        ui.includeTableKeyValue(
          features,
          'Controlled Committee?',
          cover.yes_nos.controlled_committee
        )
        ui.includeTableKeyValue(
          features,
          'Sponsored Committee?',
          cover.yes_nos.sponsored_committee
        )
      }
      ui.includeTableKeyValue(features, 'Committee Type', cover.committee_type)

      if (cover.signer) {
        const signerName = buildName(cover.signer)
        if (signerName) {
          const key = signerName.toLowerCase()
          if (key in entityAnnotations) {
            const entityAnnotation = entityAnnotations[key]
            ui.includeTableKeyValue(
              features,
              'Signer',
              util.coerceUpperCase(entityAnnotation.origText)
            )
            if (
              annotation == undefined ||
              (entityAnnotation.logo && !annotation.logo)
            ) {
              annotation = entityAnnotation
            }
          } else {
            console.log(key)
          }
        }
        ui.includeTableKeyDate(features, 'Signed Date', cover.signer.date)
        if (cover.signer_prn) {
          const signerPRNName = buildName(cover.signer_prn)
          if (signerPRNName && signerPRNName != signerName) {
            const key = signerPRNName.toLowerCase()
            if (key in entityAnnotations) {
              const entityAnnotation = entityAnnotations[key]
              ui.includeTableKeyValue(
                features,
                'Signer PRN',
                util.coerceUpperCase(entityAnnotation.origText)
              )
              if (
                annotation == undefined ||
                (entityAnnotation.logo && !annotation.logo)
              ) {
                annotation = entityAnnotation
              }
            } else {
              console.log(key)
            }
          }
        }
        ui.includeTableKeyValue(features, 'Signer Title', cover.signer.title)
        ui.includeTableKeyValue(
          features,
          'Signer Location',
          cover.signer.location
        )
      }

      ui.includeTableKeyDate(features, 'Date Filed', cover.filed_date)
      ui.includeTableKeyDate(features, 'Election Date', cover.election_date)
      if (cover.period_begin && cover.period_end) {
        ui.includeTableKeyValue(
          features,
          'Period',
          `${util.dateWithoutTime(new Date(cover.period_begin))} -- ` +
            util.dateWithoutTime(new Date(cover.period_end))
        )
      } else {
        ui.includeTableKeyDate(features, 'Period Begin', cover.period_begin)
        ui.includeTableKeyDate(features, 'Period End', cover.period_end)
      }
      ui.includeTableKeyDate(
        features,
        'Cumulative Begin',
        cover.cumulative_begin
      )

      if (cover.memo) {
        ui.includeTableKeyValue(features, 'Memo Text', cover.memo.text)
        ui.includeTableKeyValue(
          features,
          'Memo Form Type',
          cover.memo.form_type
        )
        ui.includeTableKeyValue(
          features,
          'Memo Reference',
          cover.memo.reference
        )
        ui.includeTableKeyValue(
          features,
          'Memo Line Item',
          cover.memo.line_item
        )
        ui.includeTableKeyValue(
          features,
          'Memo Amendment ID',
          cover.memo.amendment_id
        )
      }

      ui.includeTableKeyValue(features, 'Filing ID', cover.filing_id)
      ui.includeTableKeyValue(features, 'Form Type', cover.form_type)
      ui.includeTableKeyValue(features, 'Sender ID', cover.sender_id)
      ui.includeTableKeyValue(features, 'Entity Code', cover.entity_code)
      ui.includeTableKeyValue(features, 'Amendment ID', cover.amendment_id)
      ui.includeTableKeyValue(
        features,
        'Amendment Explanation',
        cover.amendment_explanation
      )

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderContribution() {
      const title = 'Campaign Contribution'
      const label = 'campaignContribution'

      const payment = filing.campaign_contribution
      const entityAnnotations = filing.annotation.entities

      let features = []

      ui.includeTableKeyValue(features, 'Description', payment.description)

      if (payment.support_or_oppose_code) {
        const suppOppCode = payment.support_or_oppose_code.toUpperCase()
        let suppOppDesc = undefined
        if (suppOppCode == 'S') {
          suppOppDesc = 'Support'
        } else if (suppOppCode == 'O') {
          suppOppDesc = 'Oppose'
        } else {
          suppOppDesc = `Invalid Code (${suppOppCode})`
        }
        ui.includeTableKeyValue(features, 'Support or Oppose', suppOppDesc)
      }

      let annotation = undefined

      if (payment.contributor) {
        const contributorName = buildName(payment.contributor)
        if (contributorName) {
          const key = contributorName.toLowerCase()
          if (key in entityAnnotations) {
            const entityAnnotation = entityAnnotations[key]
            ui.includeTableKeyValue(
              features,
              'Contributor',
              util.coerceUpperCase(entityAnnotation.origText)
            )
            annotation = entityAnnotation
          } else {
            console.log(key)
          }
        }
        ui.includeTableKeyValue(
          features,
          'Contributor Address',
          renderCalAccessLobbyingAddressFromObject(payment.contributor)
        )
        ui.includeTableKeyValue(
          features,
          'Contributor Occupation',
          payment.contributor.occupation
        )
        if (payment.contributor.employer) {
          const key = payment.contributor.employer.toLowerCase()
          if (key in entityAnnotations) {
            const entityAnnotation = entityAnnotations[key]
            ui.includeTableKeyValue(
              features,
              'Employer',
              util.coerceUpperCase(entityAnnotation.origText)
            )
            if (
              annotation == undefined ||
              (entityAnnotation.logo && !annotation.logo)
            ) {
              annotation = entityAnnotation
            }
          } else {
            console.log(key)
          }
        }
      }

      if (payment.intermediary) {
        const intermediaryName = buildName(payment.intermediary)
        if (intermediaryName) {
          const key = intermediaryName.toLowerCase()
          if (key in entityAnnotations) {
            const entityAnnotation = entityAnnotations[key]
            ui.includeTableKeyValue(
              features,
              'Intermediary',
              util.coerceUpperCase(entityAnnotation.origText)
            )
            if (
              annotation == undefined ||
              (entityAnnotation.logo && !annotation.logo)
            ) {
              annotation = entityAnnotation
            }
          } else {
            console.log(key)
          }
        }
        ui.includeTableKeyValue(
          features,
          'Intermediary Address',
          renderCalAccessLobbyingAddressFromObject(payment.intermediary)
        )
        ui.includeTableKeyValue(
          features,
          'Intermediary Occupation',
          payment.intermediary.occupation
        )
        if (payment.intermediary.employer) {
          const key = payment.intermediary.employer.toLowerCase()
          if (key in entityAnnotations) {
            const entityAnnotation = entityAnnotations[key]
            ui.includeTableKeyValue(
              features,
              "Intermediary's Employer",
              util.coerceUpperCase(entityAnnotation.origText)
            )
            if (
              annotation == undefined ||
              (entityAnnotation.logo && !annotation.logo)
            ) {
              annotation = entityAnnotation
            }
          } else {
            console.log(key)
          }
        }
      }

      if (payment.amount) {
        ui.includeTableKeyValue(
          features,
          'Received Amount',
          '$' + util.numberWithCommas(payment.amount.received),
          true,
          payment.amount.received
        )
        ui.includeTableKeyValue(
          features,
          'Period Total',
          '$' + util.numberWithCommas(payment.amount.period_total),
          true,
          payment.amount.period_total
        )
        ui.includeTableKeyValue(
          features,
          'Cumulative Total',
          '$' + util.numberWithCommas(payment.amount.cumulative_total),
          true,
          payment.amount.cumulative_total
        )
        ui.includeTableKeyValue(
          features,
          'Cumulative Year-to-Date',
          '$' + util.numberWithCommas(payment.amount.cumulative_year_to_date),
          true,
          payment.amount.cumulative_year_to_date
        )
        ui.includeTableKeyValue(
          features,
          'Cumulative Other',
          '$' + util.numberWithCommas(payment.amount.cumulative_other),
          true,
          payment.amount.cumulative_other
        )
      }

      ui.includeTableKeyDate(features, 'Date Received', payment.date_received)

      if (payment.memo) {
        ui.includeTableKeyValue(features, 'Memo Text', payment.memo.text)
        ui.includeTableKeyValue(
          features,
          'Memo Form Type',
          payment.memo.form_type
        )
        ui.includeTableKeyValue(
          features,
          'Memo Reference',
          payment.memo.reference
        )
        ui.includeTableKeyValue(
          features,
          'Memo Line Item',
          payment.memo.line_item
        )
        ui.includeTableKeyValue(
          features,
          'Memo Amendment ID',
          payment.memo.amendment_id
        )
      }

      ui.includeTableKeyValue(features, 'Form Type', payment.form_type)
      ui.includeTableKeyValue(features, 'Line Item', payment.line_item)
      if (payment.transaction) {
        ui.includeTableKeyValue(
          features,
          'Transaction ID',
          payment.transaction_id
        )
        ui.includeTableKeyValue(
          features,
          'Transaction Type',
          payment.transaction_type
        )
      }
      ui.includeTableKeyValue(features, 'Amendment ID', payment.amendment_id)

      return (
        <AccordionItemWithAnnotation
          label={label}
          title={title}
          features={features}
          annotation={annotation}
        />
      )
    }

    function renderUnmatchedMemo(memo, index) {
      const title = `Unmatched Memo ${index + 1}`
      const label = `unmatchedMemo-${index + 1}`

      let features = []

      ui.includeTableKeyValue(features, 'Text', memo.text)
      ui.includeTableKeyValue(features, 'Form Type', memo.form_type)
      ui.includeTableKeyValue(features, 'Reference', memo.reference)
      ui.includeTableKeyValue(features, 'Line Item', memo.line_item)
      ui.includeTableKeyValue(features, 'Amendment', memo.amendment_id)

      return (
        <SimpleAccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          closed={true}
          features={features}
        />
      )
    }

    function renderUnmatchedMemos() {
      const title = 'Unmatched Memos'
      const label = 'unmatchedMemos'

      let accordionItems = []
      for (const [i, memo] of filing.unmatched_memos.entries()) {
        accordionItems.push(renderUnmatchedMemo(memo, i))
      }
      if (!accordionItems.length) {
        return undefined
      }

      let accordion = (
        <Accordion
          id="us-ca-lobbying-activity-unmatched-accordion"
          items={accordionItems}
        />
      )

      return (
        <AccordionItem
          tableClassName="table ti-key-value-table"
          key={title}
          label={label}
          title={title}
          body={accordion}
        />
      )
    }

    const employersStr = util.coerce(
      filing.campaign_contribution?.contributor?.employer,
      'N/A'
    )
    const title = util.trimLabel(
      `${filing.filing_id}: ${employersStr}`,
      maxTitleLength
    )

    const header = (
      <p className="card-text">
        {super.getTagDisplay()}
        {super.getTagForm()}
      </p>
    )

    let accordionItems = []
    accordionItems.push(renderCover())
    accordionItems.push(renderContribution())
    accordionItems.push(renderUnmatchedMemos())
    let accordion = (
      <Accordion
        id="us-ca-lobbying-activity-accordion"
        items={accordionItems}
      />
    )

    return (
      <div className="card text-start mb-2 border-0">
        <div className="card-body">
          <h3 className="fw-bold lh-1 mb-2">{title}</h3>
          {header}
          {accordion}
        </div>
      </div>
    )
  }
}

export function renderFiling(filing) {
  return <Contributions filing={filing} />
}
