import * as React from 'react'

import { appConfig, maxInputLength } from '../utilities/constants'
import { DisplayArticle, getArticleTitle } from '../DisplayArticle'
import { DisplayBoard } from '../DisplayBoard'
import { DisplayHome } from '../DisplayHome'
import { DisplayHomeMobile } from '../DisplayHomeMobile'
import { DisplayOverview } from '../DisplayOverview'
import { DisplayUNICensorship } from '../DisplayUNICensorship'
import { TIPageContainer } from '../TIPageContainer'

const kLogoPrefix = 'https://techinquiry.org'

export class App extends React.Component<any, any> {
  isMobile: boolean
  homeContent: any
  overviewContent: any
  uniCensorshipContent: any

  constructor(props) {
    super(props)

    this.isMobile = window.innerWidth < 1024

    const loadingContent = <span style={{ fontSize: '150%' }}>Loading...</span>

    this.displayBoard = this.displayBoard.bind(this)
    this.displayHome = this.displayHome.bind(this)
    this.displayArticle = this.displayArticle.bind(this)
    this.displayOverview = this.displayOverview.bind(this)
    this.displayUNICensorship = this.displayUNICensorship.bind(this)

    this.setLoadingMessage = this.setLoadingMessage.bind(this)

    this.state = {
      content: undefined,
      loadingMessage: 'Loading...',
      footerContent: undefined,
    }
  }

  setLoadingMessage(text: string) {
    this.setState({ loadingMessage: text })
  }

  async componentDidMount() {
    this.homeContent = this.isMobile ? <DisplayHomeMobile /> : <DisplayHome />
    this.overviewContent = <DisplayOverview />
    this.uniCensorshipContent = <DisplayUNICensorship />

    // Bootstrap from the search parameters.
    const urlParams = new URLSearchParams(window.location.search)

    const articleKey = urlParams.get('article')
    const aboutType = urlParams.get('about')

    if (getArticleTitle(articleKey)) {
      this.displayArticle(articleKey)
    } else if (aboutType == 'board') {
      this.displayBoard()
    } else if (aboutType == 'overview') {
      this.displayOverview()
    } else if (aboutType == 'UNI-censorship') {
      this.displayUNICensorship()
    } else {
      this.displayHome()
    }
  }

  displayHome() {
    this.setState({
      content: this.homeContent,
      loadingMessage: undefined,
      footerContent: undefined,
    })
  }

  displayUNICensorship() {
    this.setState({
      content: this.uniCensorshipContent,
      loadingMessage: undefined,
      footerContent: undefined,
    })
  }

  displayArticle(articleKey: string) {
    const title = getArticleTitle(articleKey)
    if (title) {
      document.title = title
      const content = <DisplayArticle articleKey={articleKey} />
      this.setState({
        content: content,
        loadingMessage: undefined,
        footerContent: undefined,
      })
    }
  }

  displayOverview() {
    this.setState({
      content: this.overviewContent,
      loadingMessage: undefined,
      footerContent: undefined,
    })
  }

  displayBoard() {
    const content = <DisplayBoard />
    this.setState({
      content: content,
      loadingMessage: undefined,
      footerContent: undefined,
    })
  }

  render() {
    const {
      isMobile,
      state: { content, loadingMessage, footerContent },
    } = this

    return (
      <TIPageContainer
        {...{
          maxInputLength,
          isMobile,
        }}
      >
        <div>
          {content}
          {loadingMessage ? (
            <span style={{ fontSize: '150%' }}>{loadingMessage}</span>
          ) : undefined}
          {footerContent}
        </div>
      </TIPageContainer>
    )
  }
}
