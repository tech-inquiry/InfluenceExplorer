import * as React from 'react'

import { appConfig, maxInputLength } from '../utilities/constants'
import * as util from '../utilities/util'
import { EntityTitleCard } from '../EntityTitleCard'
import { FooterCard } from '../FooterCard'
import { sortAssociations } from '../AssociationsRow'
import { activeSources } from '../ActiveSources'
import { getCardsAndModals, getInitialCardsAndModals } from '../CardsAndModals'
import { TIPageContainer } from '../TIPageContainer'

const kLogoPrefix = 'https://techinquiry.org'

export class App extends React.Component<any, any> {
  isMobile: boolean
  entityAnnotations: any

  constructor(props) {
    super(props)

    this.isMobile = window.innerWidth < 1024

    const loadingContent = <span style={{ fontSize: '150%' }}>Loading...</span>

    this.entityAnnotations = undefined

    this.performSearch = this.performSearch.bind(this)
    this.displayEntity = this.displayEntity.bind(this)

    this.entityToItem = this.entityToItem.bind(this)
    this.setLoadingMessage = this.setLoadingMessage.bind(this)

    this.state = {
      isEntity: false,
      isTag: false,
      content: undefined,
      loadingMessage: 'Loading...',
      footerContent: undefined,
    }
  }

  setLoadingMessage(text: string) {
    this.setState({ loadingMessage: text })
  }

  async componentDidMount() {
    // Bootstrap from the search parameters.
    const urlParams = new URLSearchParams(window.location.search)
    const origEntity = util.decodeURLComponent(urlParams.get('entity'))
    const origQuery = util.decodeURLComponent(urlParams.get('text'))
    const origTagQuery = util.decodeURLComponent(urlParams.get('tagText'))
    const bookURL = util.decodeURLComponent(urlParams.get('bookURL'))

    const batchAll = urlParams.get('batch-all') ? true : false
    const batchNone = urlParams.get('batch-none') ? true : false
    let batchConfig = new Map<string, boolean>()
    batchConfig.set('all', batchAll)
    batchConfig.set('foreignAgents', !batchNone)
    batchConfig.set('laborRelations', !batchNone)
    batchConfig.set('lobbying', !batchNone)
    batchConfig.set('procurement', !batchNone)
    batchConfig.set('cables', !batchNone)
    batchConfig.set('leaks', !batchNone)
    batchConfig.set('securities', !batchNone)

    const development = urlParams.get('development') ? true : false

    if (origEntity) {
      this.displayEntity(origEntity, batchConfig, development)
    } else if (origQuery) {
      const isTag = false
      this.performSearch(origQuery, isTag, batchConfig, development)
    } else if (origTagQuery) {
      const isTag = true
      this.performSearch(origTagQuery, isTag, batchConfig, development)
    } else {
      console.log('Unknown query parameters.')
    }
  }

  async performSearch(
    query: string,
    isTag: boolean,
    batchConfig: Map<string, boolean>,
    development: boolean
  ) {
    if (!query) {
      return
    }

    this.setState({
      isEntity: false,
      isTag: isTag,
      content: undefined,
      footerContent: undefined,
    })

    const isEntity = false
    const blockConfig = new Map<string, boolean>()
    const sources = await activeSources(
      query,
      isEntity,
      isTag,
      batchConfig,
      blockConfig,
      this.setLoadingMessage
    )

    const [cards, modals] = await getCardsAndModals(
      query,
      sources,
      this.entityToItem,
      undefined,
      isTag,
      development
    )
    const packedCards = <div className="container px-3">{cards}</div>
    const footerCard = <FooterCard />
    const content = (
      <>
        {packedCards}
        {modals}
      </>
    )
    const footerContent = footerCard
    this.setState({
      content: content,
      footerContent: footerContent,
      loadingMessage: undefined,
    })
  }

  entityToItem(entity: string, index) {
    const logo = this.entityAnnotations[entity].hasOwnProperty('logoSmall')
      ? this.entityAnnotations[entity].logoSmall
      : this.entityAnnotations[entity].logo
    const entityStylized = this.entityAnnotations[entity].stylizedText
    const url = util.getEntityURL(entity)

    const logoEntry = (
      <a href={url}>
        <div className="text-center">
          <img
            className="ti-modal-list-entity-logo"
            loading="lazy"
            src={util.coerce(logo, util.defaultLogo)}
          />
        </div>
      </a>
    )

    return (
      <div key={entity} className="row align-items-center my-1">
        <div className="col-6 align-items-center py-2 px-5 bg-light">
          <div className="text-center">{logoEntry}</div>
        </div>
        <div className="col-6">
          <h3 className="fw-bold font-monospace mb-2">
            <a className="text-decoration-none" href={url}>
              {entityStylized}
            </a>
          </h3>
        </div>
      </div>
    )
  }

  async displayEntity(
    entity: string,
    batchConfig: Map<string, boolean>,
    development: boolean
  ) {
    entity = util.canonicalText(entity)
    this.setState({
      isEntity: true,
      content: undefined,
      footerContent: undefined,
    })

    this.setLoadingMessage('Retrieving entity profile...')
    const profile = await fetch(
      `${appConfig.apiURL}/entityProfile` +
        `?entity=${util.encodeURLComponent(entity)}`
    ).then((res) => res.json())
    this.entityAnnotations = profile.entityAnnotations

    // Set the document title.
    const selfAnnotation = profile.entityAnnotations[entity]
    document.title = selfAnnotation.stylizedText

    const isPerson = profile && profile.hasOwnProperty('isPerson')
    const blockFeeds = profile && profile.hasOwnProperty('blockFeeds')

    // Force the allowance of the cablegate and Teixeira leaks if there are
    // explicit feed categories.
    let allowLeaks = profile && profile.hasOwnProperty('allowLeaks')
    if (profile.hasOwnProperty('feeds')) {
      const feeds = profile.feeds
      if (feeds.hasOwnProperty('il')) {
        const ilFeeds = feeds.il
        if (ilFeeds.hasOwnProperty('leak')) {
          allowLeaks = true
        }
      }
      if (feeds.hasOwnProperty('us')) {
        const usFeeds = feeds.us
        if (usFeeds.hasOwnProperty('central')) {
          const centralFeeds = usFeeds.central
          if (centralFeeds.hasOwnProperty('cablegate')) {
            allowLeaks = true
          }
          if (centralFeeds.hasOwnProperty('teixeira')) {
            allowLeaks = true
          }
        }
      }
    }

    let blockConfig = new Map<string, boolean>()
    if (blockFeeds) {
      blockConfig.set('blockFeeds', blockFeeds)
    }
    if (isPerson) {
      blockConfig.set('isPerson', isPerson)
    }
    if (allowLeaks) {
      blockConfig.set('allowLeaks', allowLeaks)
    }

    const isEntity = true
    const isTag = false

    // So that we can extract the text from the top association if necessary
    // in the entity title card.
    sortAssociations(profile)

    // Because EntityTitleCard inherits from FilingWithTags, we manually fill
    // in a filing of the form { entity: entity }.
    const titleCard = (
      <EntityTitleCard
        entity={entity}
        profile={profile}
        filing={{ entity: entity }}
      />
    )
    const footerCard = <FooterCard />

    const [initialCards, initialModals] = getInitialCardsAndModals(
      entity,
      this.entityToItem,
      profile,
      isTag,
      development
    )
    let packedCards = <div className="container px-3">{initialCards}</div>
    let content = (
      <>
        {titleCard}
        {packedCards}
        {initialModals}
      </>
    )
    const footerContent = footerCard
    this.setState({ content: content, footerContent: footerCard })

    const sources = await activeSources(
      entity,
      isEntity,
      isTag,
      batchConfig,
      blockConfig,
      this.setLoadingMessage
    )

    const [cards, modals] = await getCardsAndModals(
      entity,
      sources,
      this.entityToItem,
      profile,
      isTag,
      development
    )
    packedCards = <div className="container px-3">{cards}</div>

    content = (
      <>
        {titleCard}
        {packedCards}
        {modals}
      </>
    )
    this.setState({ content: content, loadingMessage: undefined })
  }

  render() {
    const {
      isMobile,
      state: { content, loadingMessage, footerContent },
    } = this

    return (
      <TIPageContainer
        {...{
          maxInputLength,
          isMobile,
        }}
      >
        <div>
          {content}
          {loadingMessage ? (
            <div className="container text-center font-monospace mt-3">
              <p className="text-start">
                <div className="spinner-grow text-primary me-2" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
                <span style={{ fontSize: '150%' }}>{loadingMessage}</span>
              </p>
            </div>
          ) : undefined}
          {footerContent}
        </div>
      </TIPageContainer>
    )
  }
}
