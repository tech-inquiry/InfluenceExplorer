import * as React from 'react'

import { appConfig, maxInputLength } from '../utilities/constants'
import * as util from '../utilities/util'
import { FooterCard } from '../FooterCard'
import { activeLeakSources } from '../ActiveSources'
import { getLeakCardsAndModals } from '../CardsAndModals'
import { TIPageContainer } from '../TIPageContainer'

const kLogoPrefix = 'https://techinquiry.org'

export class App extends React.Component<any, any> {
  isMobile: boolean
  entityAnnotations: any

  constructor(props) {
    super(props)

    this.isMobile = window.innerWidth < 1024

    const loadingContent = <span style={{ fontSize: '150%' }}>Loading...</span>

    this.entityAnnotations = undefined

    this.performSearch = this.performSearch.bind(this)

    this.entityToItem = this.entityToItem.bind(this)
    this.setLoadingMessage = this.setLoadingMessage.bind(this)

    this.state = {
      content: undefined,
      loadingMessage: 'Loading...',
      footerContent: undefined,
    }
  }

  setLoadingMessage(text: string) {
    this.setState({ loadingMessage: text })
  }

  async componentDidMount() {
    // Bootstrap from the search parameters.
    const urlParams = new URLSearchParams(window.location.search)
    const origQuery = util.decodeURLComponent(urlParams.get('text'))

    const batchAll = urlParams.get('batch-all') ? true : false
    const batchNone = urlParams.get('batch-none') ? true : false
    let batchConfig = new Map<string, boolean>()
    batchConfig.set('all', batchAll)
    batchConfig.set('leaks', !batchNone)

    const development = urlParams.get('development') ? true : false

    if (origQuery) {
      this.performSearch(origQuery, batchConfig, development)
    } else {
      console.log('Unknown query parameters.')
    }
  }

  async performSearch(
    query: string,
    batchConfig: Map<string, boolean>,
    development: boolean
  ) {
    if (!query) {
      return
    }

    this.setState({
      content: undefined,
      footerContent: undefined,
    })

    const isEntity = false
    const isTag = false
    const blockConfig = new Map<string, boolean>()
    const sources = await activeLeakSources(
      query,
      isEntity,
      isTag,
      batchConfig,
      blockConfig,
      this.setLoadingMessage
    )

    const [cards, modals] = getLeakCardsAndModals(
      query,
      sources,
      this.entityToItem,
      undefined,
      isTag,
      development
    )
    const packedCards = <div className="container px-3">{cards}</div>
    const footerCard = <FooterCard />
    const content = (
      <>
        {packedCards}
        {modals}
      </>
    )
    const footerContent = footerCard
    this.setState({ content: content, footerContent: footerContent })
  }

  entityToItem(entity: string, index) {
    const logo = this.entityAnnotations[entity].hasOwnProperty('logoSmall')
      ? this.entityAnnotations[entity].logoSmall
      : this.entityAnnotations[entity].logo
    const entityStylized = this.entityAnnotations[entity].stylizedText
    const url = util.getEntityURL(entity)

    const logoEntry = (
      <a href={url}>
        <div className="text-center">
          <img
            className="ti-modal-list-entity-logo"
            loading="lazy"
            src={util.coerce(logo, util.defaultLogo)}
          />
        </div>
      </a>
    )

    return (
      <div key={entity} className="row align-items-center my-1">
        <div className="col-6 align-items-center py-2 px-5 bg-light">
          <div className="text-center">{logoEntry}</div>
        </div>
        <div className="col-6">
          <h3 className="fw-bold font-monospace mb-2">
            <a className="text-decoration-none" href={url}>
              {entityStylized}
            </a>
          </h3>
        </div>
      </div>
    )
  }

  render() {
    const {
      isMobile,
      state: { content, loadingMessage, footerContent },
    } = this

    return (
      <TIPageContainer
        {...{
          maxInputLength,
          isMobile,
        }}
      >
        <div>
          {content}
          {loadingMessage ? (
            <span style={{ fontSize: '150%' }}>{loadingMessage}</span>
          ) : undefined}
          {footerContent}
        </div>
      </TIPageContainer>
    )
  }
}
