import * as React from 'react'

import * as util from '../utilities/util'

const kRequestURL = '/requestEntityUpdate'

export default function RequestEntityUpdate() {
  const [summary, setSummary] = React.useState('')

  const urlParams = new URLSearchParams(window.location.search)
  const entity = urlParams.get('entity')
  const stylizedEntity = urlParams.get('stylizedEntity')
  if (urlParams.get('summary')) {
    setSummary(urlParams.get('summary'))
  }

  function submitRequest(event) {
    event.preventDefault()
    const data = { summary: summary }
    fetch('/api/addEntitySuggestion', {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        token: util.retrieveToken(),
        entity: entity,
        data: data,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.message === 'Invalid token') {
          util.dropToken()
          const redirectParams =
            `entity=${encodeURIComponent(entity)}` +
            `&stylizedEntity=${encodeURIComponent(stylizedEntity)}` +
            `&summary=${encodeURIComponent(summary)}`
          const url = `/login?redirectPath=${kRequestURL}&redirectParams=${encodeURIComponent(
            redirectParams
          )}`
          window.location.href = url
        } else if (data.message === 'success') {
          const url = `/?entity=${encodeURIComponent(entity)}`
          window.location.href = url
        } else {
          alert(data.message)
        }
      })
  }

  return (
    <>
      <form className="form-request-entity-update" onSubmit={submitRequest}>
        <label htmlFor="summary" className="sr-only">
          Summary of requested updates for {stylizedEntity}
          <textarea
            value={summary}
            placeholder="Request summary"
            rows={4}
            cols={80}
            onChange={(e) => setSummary(e.target.value)}
          />
        </label>
        <button className="btn btn-lg btn-primary btn-block" type="submit">
          Submit
        </button>
      </form>
    </>
  )
}
