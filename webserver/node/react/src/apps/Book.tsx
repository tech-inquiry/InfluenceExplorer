import * as React from 'react'

import { appConfig, maxInputLength } from '../utilities/constants'
import { decodeURLComponent, encodeURLComponent } from '../utilities/util'
import { DisplayBook } from '../DisplayBook'
import { TIPageContainer } from '../TIPageContainer'

const kLogoPrefix = 'https://techinquiry.org'

export class App extends React.Component<any, any> {
  isMobile: boolean

  constructor(props) {
    super(props)

    this.isMobile = window.innerWidth < 1024

    const loadingContent = <span style={{ fontSize: '150%' }}>Loading...</span>

    this.displayBook = this.displayBook.bind(this)

    this.setLoadingMessage = this.setLoadingMessage.bind(this)

    this.state = {
      content: undefined,
      loadingMessage: 'Loading...',
      footerContent: undefined,
    }
  }

  setLoadingMessage(text: string) {
    this.setState({ loadingMessage: text })
  }

  async componentDidMount() {
    // Bootstrap from the search parameters.
    const urlParams = new URLSearchParams(window.location.search)

    const book = decodeURLComponent(urlParams.get('book'))

    if (book) {
      this.displayBook(book)
    } else {
      console.log('Unknown query parameters.')
    }
  }

  async displayBook(tag: string) {
    this.setState({
      content: undefined,
      footerContent: undefined,
    })

    this.setLoadingMessage('Retrieving book info...')
    const book = await fetch(
      `${appConfig.apiURL}/book?tag=${encodeURLComponent(tag)}`
    ).then((res) => res.json())

    // Set the document title.
    document.title = book.title

    const content = <DisplayBook book={book} logoPrefix={kLogoPrefix} />

    this.setState({ content: content, loadingMessage: undefined })
  }

  render() {
    const {
      isMobile,
      state: { content, loadingMessage, footerContent },
    } = this

    return (
      <TIPageContainer
        {...{
          maxInputLength,
          isMobile,
        }}
      >
        <div>
          {content}
          {loadingMessage ? (
            <span style={{ fontSize: '150%' }}>{loadingMessage}</span>
          ) : undefined}
          {footerContent}
        </div>
      </TIPageContainer>
    )
  }
}
