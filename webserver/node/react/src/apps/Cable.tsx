import * as React from 'react'

import { appConfig, maxInputLength } from '../utilities/constants'
import * as renderFiling from '../leaks/cablegate/renderFiling'
import { TIPageContainer } from '../TIPageContainer'

export class App extends React.Component<any, any> {
  isMobile: boolean

  constructor(props) {
    super(props)

    this.isMobile = window.innerWidth < 1024

    const loadingContent = <span style={{ fontSize: '150%' }}>Loading...</span>

    this.displayCable = this.displayCable.bind(this)

    this.setLoadingMessage = this.setLoadingMessage.bind(this)

    this.state = {
      content: undefined,
      loadingMessage: 'Loading...',
      footerContent: undefined,
    }
  }

  setLoadingMessage(text: string) {
    this.setState({ loadingMessage: text })
  }

  async componentDidMount() {
    // Bootstrap from the search parameters.
    const urlParams = new URLSearchParams(window.location.search)
    const cableReferenceID = urlParams.get('cable_reference')

    if (cableReferenceID) {
      this.displayCable(cableReferenceID)
    } else {
      console.log('Unknown query parameters.')
    }
  }

  async displayCable(referenceID: string) {
    this.setState({
      content: undefined,
      footerContent: undefined,
    })

    this.setLoadingMessage('Retrieving cable info...')
    const result = await fetch(
      `${appConfig.apiURL}/us/central/cablegate/cable?reference_id=${referenceID}`
    ).then((res) => res.json())

    if (!result.filings.length) {
      this.setState({ content: 'No results.', loadingMessage: undefined })
      return
    }

    // Set the document title.
    const filing = result.filings[0]
    document.title = filing.reference_id

    const content = renderFiling.renderFiling(filing)

    this.setState({ content: content, loadingMessage: undefined })
  }

  render() {
    const {
      isMobile,
      state: { content, loadingMessage, footerContent },
    } = this

    return (
      <TIPageContainer
        {...{
          maxInputLength,
          isMobile,
        }}
      >
        <div>
          {content}
          {loadingMessage ? (
            <span style={{ fontSize: '150%' }}>{loadingMessage}</span>
          ) : undefined}
          {footerContent}
        </div>
      </TIPageContainer>
    )
  }
}
