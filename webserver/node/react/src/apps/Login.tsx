import * as React from 'react'

import * as util from '../utilities/util'

export default function Login() {
  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')

  const urlParams = new URLSearchParams(window.location.search)
  const redirectPath = urlParams.get('redirectPath')
  const redirectFile = urlParams.get('redirectFile')
  const redirectURL = redirectFile
    ? redirectPath + '/' + redirectFile
    : redirectPath
    ? redirectPath
    : '/'
  const redirectParams = urlParams.get('redirectParams')
  const isIsraelLeak = urlParams.get('isIsraelLeak')

  function formRedirectParams(token: string): string {
    return (redirectParams ? `${redirectParams}&` : '') + `token=${token}`
  }

  function formURL(token: string) {
    return `${redirectURL}?${formRedirectParams(token)}`
  }

  function submitUser(event) {
    event.preventDefault()
    fetch('/api/auth', {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({ username: username, password: password }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.message === 'success') {
          util.storeToken(data.token, username)
          window.location.href = formURL(data.token)
        } else {
          alert(data.message)
        }
      })
  }

  const existingToken = util.retrieveToken()
  if (existingToken) {
    fetch('/api/checkToken', {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({ token: existingToken }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.message === 'success') {
          window.location.replace(formURL(existingToken))
        } else {
          util.dropToken()
        }
      })
  }

  const israelLeaksNote = [
    <p className="form-graf mx-5 mb-1 font-weight-normal">
      Out of an abundance of journalistic care, Tech Inquiry is restricting
      access to the bulk of its searchable mirror of the Israeli Ministry of
      Justice and Ministry of Defense leaks to journalists and researchers.
    </p>,
    <p className="form-graf mx-5 mb-3 font-weight-normal">
      If you would like an account, please email our executive director, Jack
      Poulson, at <a href="mailto:jack@techinquiry.org">jack@techinquiry.org</a>
      .
    </p>,
  ]
  const defaultNote = [
    <p className="form-graf mx-5 mb-1 font-weight-normal">
      If you are a journalist or researcher interested in an account, please
      email our executive director, Jack Poulson, at{' '}
      <a href="mailto:jack@techinquiry.org">jack@techinquiry.org</a>.
    </p>,
    <p className="form-graf mx-5 mb-3 font-weight-normal">
      At the moment, beyond access to Israeli government email leaks, logged in
      users have the ability to apply their own tags to documents mirrored by
      Tech Inquiry.
    </p>,
  ]

  return (
    <>
      {isIsraelLeak ? israelLeaksNote : defaultNote}
      <form className="form-signin" onSubmit={submitUser}>
        <label htmlFor="username" className="sr-only">
          Username
        </label>
        <input
          type="username"
          id="username"
          className="form-control mb-3"
          value={username}
          placeholder="Username"
          onChange={(e) => setUsername(e.target.value)}
          required
          autoFocus
        />
        <label htmlFor="inputPassword" className="sr-only">
          Password
        </label>
        <input
          type="password"
          id="inputPassword"
          className="form-control mb-3"
          value={password}
          placeholder="Password"
          onChange={(e) => setPassword(e.target.value)}
          required
        />
        <button className="btn btn-lg btn-primary btn-block" type="submit">
          Sign in
        </button>
      </form>
    </>
  )
}
