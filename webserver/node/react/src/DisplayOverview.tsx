import * as React from 'react'

import { appConfig } from './utilities/constants'
import { generateQueryString, getEntityURL } from './utilities/util'

export class DisplayOverview extends React.Component<any, any> {
  constructor(props) {
    super(props)
  }

  render() {
    const boardURL =
      appConfig.explorerURL + generateQueryString({ about: 'board' })
    function renderEntityLink(entity: string, title: string) {
      return <a href={getEntityURL(entity)}>{title}</a>
    }

    function renderBoardLink(title: string) {
      return <a href={boardURL}>{title}</a>
    }

    return (
      <div className="card border-0">
        <div className="card-body mt-0">
          <p className="card-text ti-home mt-0">
            <strong>Tech Inquiry</strong> is a small nonprofit which maps out
            relationships between companies, nonprofits, and governments to
            better contextualize and investigate corporate influence. We are
            especially interested in networks relating to the{' '}
            <a href="https://theintercept.com/2022/04/22/anomaly-six-phone-tracking-zignal-surveillance-cia-nsa/">
              surveillance
            </a>{' '}
            and <a href={getEntityURL('elbit systems ltd')}>weapons</a>{' '}
            industries and are prolific requesters of{' '}
            <a href="https://theintercept.com/2020/10/21/google-cbp-border-contract-anduril/">
              contracts
            </a>{' '}
            and{' '}
            <a href="https://theintercept.com/2022/06/29/crypto-coinbase-tracer-ice/">
              emails
            </a>{' '}
            through the{' '}
            <a href="https://www.foia.gov/">Freedom of Information Act</a>. We
            have also exposed police{' '}
            <a href="https://www.vice.com/en/article/y3vq3w/inside-police-conference-washington-dc-sheriffs-association">
              corruption and misogyny
            </a>{' '}
            and the{' '}
            <a href="https://www.forbes.com/sites/thomasbrewster/2022/02/23/meet-the-secretive-surveillance-wizards-helping-the-fbi-and-ice-wiretap-facebook-and-google-users/?sh=6b3a0b953f0f">
              inner workings
            </a>{' '}
            of their social media, search engine, location-tracking, and private
            messaging surveillance.
          </p>
          <p className="card-text ti-home">
            On a daily basis we mirror and integrate procurement feeds from the
            governments of the US (including Florida state), UK, Australia,
            Canada, Israel, and the EU, as well as lobbying feeds from the US
            (including California state) and Canada. Beyond the live interface,
            all of our data and code is{' '}
            <a href="https://gitlab.com/tech-inquiry/InfluenceExplorer/">
              open source
            </a>
            , including recent entity{' '}
            <a href="https://techinquiry.org/data/entity_vectors.tsv">
              embeddings
            </a>{' '}
            and{' '}
            <a href="https://techinquiry.org/data/entity_labels.tsv">labels</a>,
            which you can download and then{' '}
            <a href="https://projector.tensorflow.org/">explore</a> in your
            browser.
          </p>
          <p className="text-center">
            <a href="https://projector.tensorflow.org/">
              <img
                src="https://techinquiry.org/images/BabelStreetProjection.png"
                className="img-fluid"
                style={{ height: '45em', width: 'auto' }}
              />
            </a>
          </p>
          <p className="card-text ti-home">
            Our most recent two reports are on the US government's aggregation
            of{' '}
            <a href="https://techinquiry.org/EasyAsPAI/">
              "Publicly Available Information"
            </a>{' '}
            and a deep dive into the impact on tech company tax rates of the{' '}
            <a href="https://techinquiry.org/DeathAndTaxes/">
              Tax Cuts and Jobs Act
            </a>
            .
          </p>
          <p className="card-text ti-home">
            Tech Inquiry is currently fiscally sponsored through the U.S.
            501(c)(3){' '}
            <a href="https://thesignalsnetwork.org">
              The Signals Network -- the whistleblowers foundation
            </a>
            , and you can donate to us tax-free through selecting the 'Tech
            Inquiry' option on what of their{' '}
            <a href="https://thesignalsnetwork.org/donate/">donation options</a>
            . For more information, please feel free to email
            info@techinquiry.org.
          </p>
          <p className="card-text ti-home">
            Tech Inquiry generally does not accept money from corporations -- we
            made exceptions for two small organizations,{' '}
            {renderEntityLink('the worker agency', 'The Worker Agency')} and{' '}
            <a href="https://metstrat.ca/">MetStrat</a> -- or from nonprofits
            associated with tech billionaires or from private foundations. Our
            most common support is via $50 monthly donations from individuals,
            through we have received two one-time $1000 donations from tech
            workers, a one-time $5000 donation from James Baker, and a one-time
            $10K donation from Aron Ahmadia (we make public any individual who
            donates more than $1000 in a single year).
          </p>
          <p className="card-text ti-home">
            We recently completed a research project with{' '}
            {renderEntityLink('uni global union', 'UNI Global Union')} on
            international procurement from cloud giants which was funded by{' '}
            {renderEntityLink(
              'friedrich-ebert-stiftung e.v.',
              'Friedrich-Ebert-Stiftung'
            )}
            . As a result of strict standards,{' '}
            {renderEntityLink('tech inquiry', 'Tech Inquiry')} has been
            primarily a volunteer effort. We hope to increase individual
            donations to the point where we can support our first full-time
            staff member.
          </p>
          <p className="card-text ti-home">
            Inquiries can be directed to:{' '}
            <a href="mailto:info@techinquiry.org">info@techinquiry.org</a> or to
            our Executive Director, Jack Poulson, at{' '}
            <a href="mailto:jack@techinquiry.org">jack@techinquiry.org</a>. You
            can also found contact information for our board{' '}
            {renderBoardLink('here')}.
          </p>
        </div>
      </div>
    )
  }
}
