import * as React from 'react'

import { RotatingImage } from './RotatingImage'
import * as ui from './utilities/ui'
import * as util from './utilities/util'

function getFromText(association) {
  if (!association.text) {
    if (Array.isArray(association.from.text)) {
      return association.from.text
    } else {
      return [association.from.text]
    }
  } else if (Array.isArray(association.text)) {
    return association.text
  } else {
    return [association.text]
  }
}

function getCitationURL(citation): string {
  return util.isString(citation) ? citation : citation.url
}

function formCitationEntry(citation): any {
  const nonBookCategories = [
    'article',
    'image',
    'journal',
    'report',
    'video',
    'webpage',
  ]

  if (util.isString(citation)) {
    return <a href={citation}>{citation}</a>
  } else if (citation['type'] == 'book') {
    // We make use of a rough analogue of Chicago-style for now.
    // TODO(Jack Poulson): Incorporate place of publication.
    const authorsString = util.getChicagoStyleAuthorsString(citation['authors'])
    const authorsEntry = authorsString
      ? util.endWithPunctuation(authorsString)
      : ''
    const titleEntry = util.endWithPunctuation(citation['title'])
    const year = citation['date'].split('.')[0]
    const url = citation['url']
    return (
      <p className="lh-sm my-0">
        <span style={{ fontSize: '1.05rem' }}>
          {authorsEntry} <span className="fst-italic">{titleEntry}</span>{' '}
          {citation['org']}, {year}.
        </span>
        <br />
        <a href={url}>{url}</a>
      </p>
    )
  } else if (citation['type'] == 'journal') {
    const authorsString = util.getChicagoStyleAuthorsString(citation['authors'])
    const authorsEntry = authorsString
      ? util.endWithPunctuation(authorsString) + ' '
      : ''
    const titleEntry = util.endWithPunctuation(citation['title'])
    const journalEntry = <span className="fst-italic">{citation['org']}</span>
    const volumeEntry = citation['volume']
      ? ` ${citation['volume']}`
      : undefined
    const numberEntry = `no. ${citation['number']}`
    const dateEntry = citation['date']
    const pages = citation['pages']
    const pagesEntry = pages ? `: ${pages}` : ''
    const url = citation['url']
    return (
      <p className="lh-sm my-0">
        <span style={{ fontSize: '1.05rem' }}>
          {authorsEntry}"{titleEntry}" {journalEntry}
          {volumeEntry}, {numberEntry} ({dateEntry}){pagesEntry}.
        </span>
        <br />
        <a href={url}>{url}</a>
      </p>
    )
  } else if (nonBookCategories.includes(citation.type)) {
    const authorsString = util.getChicagoStyleAuthorsString(citation['authors'])
    const authorsEntry = authorsString
      ? util.endWithPunctuation(authorsString)
      : ''
    const titleEntry = util.endWithPunctuation(citation['title'])
    const orgEntry = util.endWithPunctuation(citation['org'])
    const haveDate = citation['date']
    const dateField = haveDate ? 'date' : 'accessed'
    const dateStrPrefix = haveDate ? '' : 'Accessed: '
    const dateStr = util.getDateString(citation[dateField])
    const dateEntry = `${dateStrPrefix}${dateStr}`
    const url = citation['url']
    if (authorsEntry) {
      return (
        <p className="lh-sm my-0">
          <span style={{ fontSize: '1.05rem' }}>
            {authorsEntry} "{titleEntry}"{' '}
            <span className="fst-italic">{orgEntry}</span> {dateEntry}.
          </span>
          <br />
          <a href={url}>{url}</a>
        </p>
      )
    } else {
      return (
        <p className="lh-sm my-0">
          <span style={{ fontSize: '1.05rem' }}>
            "{titleEntry}" <span className="fst-italic">{orgEntry}</span>{' '}
            {dateEntry}.
          </span>
          <br />
          <a href={url}>{url}</a>
        </p>
      )
    }
  }
}

function weightCompare(assn1, assn2) {
  const fromWeight1 = 'weight' in assn1.from ? assn1.from.weight : 1
  const fromWeight2 = 'weight' in assn2.from ? assn2.from.weight : 1
  if (fromWeight1 > fromWeight2) {
    return -1
  }
  if (fromWeight2 > fromWeight1) {
    return 1
  }
  return 0
}

// Compare based upon timespans.
function timespanCompare(assn1, assn2) {
  if (assn1.timespan == undefined || assn2.timespan == undefined) {
    return 0
  }
  const ts1 = assn1.timespan
  const ts2 = assn2.timespan

  // Compare based upon timespan ends, if we can.
  const openEnded1 = ts1.end == undefined && ts1.start != undefined
  const openEnded2 = ts2.end == undefined && ts2.start != undefined
  if (openEnded1 && !openEnded2) {
    return -1
  }
  if (!openEnded1 && openEnded2) {
    return 1
  }

  const end1 = util.coerce(ts1.end, ts1.circa)
  const end2 = util.coerce(ts2.end, ts2.circa)
  if (end1 > end2) {
    return -1
  }
  if (end1 < end2) {
    return 1
  }

  return 0
}

// Compare based upon text.
function textCompare(assn1, assn2) {
  // TODO(Jack Poulson): Consider also comparing against further paragraphs.
  const from1 = getFromText(assn1)[0]
  const from2 = getFromText(assn2)[0]
  if (from1 < from2) {
    return -1
  }
  if (from2 < from1) {
    return 1
  }
  return 0
}

// Compare based upon name.
function nameCompare(assn1, assn2) {
  const name1 = assn1.from.name
  const name2 = assn2.from.name
  if (name1 < name2) {
    return -1
  }
  if (name2 < name1) {
    return 1
  }
  return 0
}

function associationCompare(assn1, assn2) {
  const weightResult = weightCompare(assn1, assn2)
  if (weightResult) {
    return weightResult
  }

  const timespanResult = timespanCompare(assn1, assn2)
  if (timespanResult) {
    return timespanResult
  }

  const textResult = textCompare(assn1, assn2)
  if (textResult) {
    return textResult
  }

  const nameResult = nameCompare(assn1, assn2)
  if (nameResult) {
    return nameResult
  }

  return 0
}

function groupCompare(assn1List, assn2List) {
  // We compare the first in each list.
  const assn1 = assn1List[0]
  const assn2 = assn2List[0]

  const timespanResult = timespanCompare(assn1, assn2)
  if (timespanResult) {
    return timespanResult
  }

  const weightResult = weightCompare(assn1, assn2)
  if (weightResult) {
    return weightResult
  }

  const textResult = textCompare(assn1, assn2)
  if (textResult) {
    return textResult
  }

  const nameResult = nameCompare(assn1, assn2)
  if (nameResult) {
    return nameResult
  }

  return 0
}

const kMiscellaneousKey = 'Miscellaneous'

function categorizeAssociations(associations) {
  // Detect the categories and their sizes.
  let counts = new Map<string, number>()
  for (const association of associations) {
    const category = association.from.category
    if (!category) {
      continue
    }
    if (counts.has(category)) {
      counts.set(category, counts.get(category) + 1)
    } else {
      counts.set(category, 1)
    }
  }

  // Build the association groups, handling the miscellaneous last.
  let groupMap = new Map<string, any[]>()
  let nameMap = new Map<string, any[]>()
  for (const association of associations) {
    let category = association.from.category
    if (category == undefined || counts[category] == 1) {
      category = kMiscellaneousKey
    }
    if (category == kMiscellaneousKey) {
      continue
    }

    if (!groupMap.has(category)) {
      groupMap.set(category, [])
    }
    const name = association.from.name
    // Try to push it into a pre-existing association.
    if (nameMap.has(name)) {
      // For now, assume at most two total locations can exist.
      const location = nameMap.get(name)[0]
      if (category == location.category) {
        // TODO(Jack Poulson): Remove duplicates.
        groupMap.get(location.category)[location.index].push(association)
        continue
      }
      // We have a name with (at least) two separate category tags, and so
      // we will insert in both places. The original motivation was Lebanese
      // general Michel Naim Aoun, who was both (disputed) prime minister and
      // then later president. The two positions are peers rather than in a
      // hierarchy.
    }
    const categoryIndex = groupMap.get(category).length
    nameMap.set(name, [{ category: category, index: categoryIndex }])
    groupMap.get(category).push([association])
  }
  for (const association of associations) {
    let category = association.from.category
    if (category == undefined || counts[category] == 1) {
      category = kMiscellaneousKey
    }
    if (category != kMiscellaneousKey) {
      continue
    }

    if (!groupMap.has(category)) {
      groupMap.set(category, [])
    }
    const name = association.from.name
    // Try to push it into a pre-existing association.
    if (nameMap.has(name)) {
      // TODO(Jack Poulson): Use the heaviest weighted category if multiple.
      // TODO(Jack Poulson): Remove duplicates.
      for (const location of nameMap.get(name)) {
        if (location.category == category) {
          groupMap.get(location.category)[location.index].push(association)
        }
      }
      continue
    } else {
      const categoryIndex = groupMap.get(category).length
      nameMap.set(name, [{ category: category, index: categoryIndex }])
    }
    groupMap.get(category).push([association])
  }

  // We want the categories to be sorted by maximum weight, with the
  // exception of placing the miscellaneous category last. We will assume the
  // associations had been pre-sorted before filting into categories, which
  // makes the non-miscellaneous categories already appear in sorted order.
  let groups = []
  for (const key of groupMap.keys()) {
    if (key != kMiscellaneousKey) {
      groups.push({ k: key, v: groupMap.get(key) })
    }
  }
  if (groupMap.has(kMiscellaneousKey)) {
    groups.push({
      k: kMiscellaneousKey,
      v: groupMap.get(kMiscellaneousKey),
    })
  }

  // Use the custom sort, which orders timespans first.
  for (let group of groups) {
    group.v.sort(groupCompare)
  }

  return groups
}

export function sortAssociations(profile) {
  if (profile.associations == undefined) {
    return
  }
  profile.associations.sort(associationCompare)
}

class AssociationsRow extends React.Component<any, any> {
  label: string
  rotatingImages: any[]
  maxImages: number

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'associations')

    this.maxImages = 10

    const info = this.props.info
    const profile = info.profile

    // Sort the associations.
    sortAssociations(profile)

    // Generate the list of hyperlinked images to show.
    this.rotatingImages = []
    for (const association of profile.associations) {
      if (this.rotatingImages.length >= this.maxImages) {
        break
      }
      const entity = util.canonicalText(association.from.name)
      const logo = profile.entityAnnotations[entity].logoSmall
      if (logo) {
        this.rotatingImages.push({
          image: logo,
          url: util.getEntityURL(entity),
        })
      }
    }
  }

  render() {
    const info = this.props.info
    const profile = info.profile

    const selfAnnotation = profile.entityAnnotations[info.query]

    const buttonTitle = 'Associations'

    const capImage = (
      <RotatingImage
        items={this.rotatingImages}
        opacity={1.0}
        development={info.development}
      />
    )

    const footerText = (
      <span>
        Click to view{' '}
        <span className="fw-bold">
          {profile.associations.length + ' manually tagged '}
          {profile.associations.length > 1 ? 'associations' : 'association'}
        </span>{' '}
        with <span className="fst-italic">{selfAnnotation.stylizedText}</span>
        {selfAnnotation.stylizedText.endsWith('.') ? '' : '.'}
      </span>
    )

    return (
      <ui.EntityRowCard
        label={this.label}
        buttonTitle={buttonTitle}
        capImage={capImage}
        footerText={footerText}
      />
    )
  }
}

class AssociationsModal extends React.Component<any, any> {
  label: string
  citationMap: Map<string, any>
  citationList: any[]

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'associations')

    const info = this.props.info
    const profile = info.profile

    // Sort the associations.
    sortAssociations(profile)

    // Make a map from links to citation number.
    this.citationMap = new Map<string, any>()
    this.citationList = []
    let citationURLs = new Set<string>()
    for (const association of profile.associations) {
      const citationList =
        association.citations != undefined
          ? association.citations
          : [association.citation]
      for (const citationItem of citationList) {
        if (citationItem == undefined) {
          continue
        }
        const citationSublist = Array.isArray(citationItem)
          ? citationItem
          : [citationItem]
        for (const citation of citationSublist) {
          const citationURL = getCitationURL(citation)
          if (citation && !citationURLs.has(citationURL)) {
            citationURLs.add(citationURL)

            this.citationList.push(citation)
            this.citationMap.set(citationURL, {
              index: this.citationList.length,
              accessed: association['accessed'],
            })
          }
        }
      }
    }

    this.formNote = this.formNote.bind(this)
    this.formNoteHelper = this.formNoteHelper.bind(this)
    this.associationGroupToItem = this.associationGroupToItem.bind(this)
    this.citationToItem = this.citationToItem.bind(this)
  }

  associationURLs(association) {
    return association.citations != undefined
      ? association.citations
      : association.citation
      ? [association.citation]
      : []
  }

  formNoteHelper(citations) {
    if (!citations.length) {
      return undefined
    }
    const that = this
    return (
      <code
        key="numbering-span"
        className="fw-bold font-monospace text-dark"
        style={{ fontSize: '1.5rem' }}
      >
        [
        {citations.map(function (citation, citationIndex) {
          const citationURL = getCitationURL(citation)
          const uniqueIndex = that.citationMap.get(citationURL)['index']
          return (
            <span key={citationIndex}>
              {citationIndex > 0 ? ', ' : undefined}
              <a className="text-decoration-none" href={citationURL}>
                {uniqueIndex}
              </a>
            </span>
          )
        })}
        ]
      </code>
    )
  }

  formNote(text, citations, index) {
    const that = this
    const pClasses = 'font-monospace lh-sm ti-compact'
    const pStyle = { fontSize: '1.5 rem' }

    let paragraphs = []
    if (text.length == 1) {
      paragraphs.push(text[0])
      paragraphs.push(that.formNoteHelper(citations))
    } else if (citations.length && Array.isArray(citations[0])) {
      for (const [itemIndex, item] of text.entries()) {
        paragraphs.push(item)
        paragraphs.push(that.formNoteHelper(citations[itemIndex]))
        paragraphs.push(<br />)
      }
    } else {
      for (const item of text) {
        paragraphs.push(item)
      }
      paragraphs.push(that.formNoteHelper(citations))
    }
    return (
      <div>
        {paragraphs.map(function (item, itemIndex) {
          const key = `${index}-${itemIndex}`
          return (
            <p key={key} className={pClasses} style={pStyle}>
              {item}
            </p>
          )
        })}
      </div>
    )
  }

  associationGroupToItem(associationGroup, index) {
    const info = this.props.info
    const profile = info.profile

    const selfAnnotation = profile.entityAnnotations[info.query]
    const toStylized = selfAnnotation.stylizedText

    // We grab metadata from the first association.
    const association = associationGroup[0]

    const fromName = util.canonicalText(association.from.name)
    const fromStylized = profile.entityAnnotations[fromName].stylizedText
    const logo = profile.entityAnnotations[fromName].logoSmall

    const url = util.getEntityURL(fromName)
    const logoEntry = (
      <a href={url}>
        <img
          className="ti-association-list-entity-logo"
          src={util.coerce(logo, util.defaultLogo)}
        />
      </a>
    )
    const fromNameEntry = (
      <a className="text-decoration-none" href={url}>
        {fromStylized}
      </a>
    )

    const that = this

    return (
      <div
        key={index}
        className="row flex-md-row-reverse align-items-center mx-0 px-0 py-3"
      >
        <div className="col-12 col-md-4 align-items-center px-3 bg-light">
          <div className="text-center">{logoEntry}</div>
        </div>
        <div className="col-12 col-md-8 font-monospace">
          <h4 className="fw-bold mb-2">{fromNameEntry}</h4>
          {associationGroup.map(function (assn, assnIndex) {
            return that.formNote(
              getFromText(assn),
              that.associationURLs(assn),
              assnIndex
            )
          })}
        </div>
      </div>
    )
  }

  citationToItem(citation) {
    if (!citation) {
      return undefined
    }
    const citationURL = getCitationURL(citation)
    const citationItem = this.citationMap.get(citationURL)

    const citationEntry = formCitationEntry(citation)

    return (
      <div
        key={citationItem['index']}
        className="row flex-md-row align-items-center mx-0 px-0 py-1 my-3"
      >
        <div className="col-1 fw-bold font-monospace">
          [{citationItem['index']}]
        </div>
        <div className="col-8 font-monospace overflow-auto">
          {citationEntry}
        </div>
        <div className="col-3 font-monospace">
          {citationItem['accessed']
            ? 'Accessed: ' + citationItem['accessed']
            : ''}
        </div>
      </div>
    )
  }

  render() {
    const info = this.props.info
    const profile = info.profile

    const modalTitle = 'Associations'

    const groups = categorizeAssociations(profile.associations)

    let associationContainer = undefined
    if (groups.length == 1 && groups[0].k == kMiscellaneousKey) {
      associationContainer = (
        <div key="association-container" className="container px-0">
          {groups[0].v.map(this.associationGroupToItem)}
        </div>
      )
    } else {
      const associationGroupToItem = this.associationGroupToItem
      associationContainer = (
        <div key="association-container" className="container px-0">
          {groups.map(function (group) {
            return (
              <div key={group.k} className="mb-3">
                <h1 className="border-bottom">{group.k}</h1>
                {group.v.map(associationGroupToItem)}
              </div>
            )
          })}
        </div>
      )
    }

    const citationContainer = this.citationList.length ? (
      <div key="citation-container" className="container mt-3 px-0">
        <h3 className="fw-bold mb-2">References</h3>
        {this.citationList.map(this.citationToItem)}
      </div>
    ) : undefined

    const modalBody = (
      <div>
        {associationContainer}
        {citationContainer}
      </div>
    )

    return (
      <ui.EntityRowModal
        label={this.label}
        title={modalTitle}
        body={modalBody}
        dialogExtraClasses="modal-fullscreen"
      />
    )
  }
}

export function insertCardAndModal(info, cards: any[], modals: any[]): void {
  const profile = info.profile
  if (!profile || !profile.associations || !profile.associations.length) {
    return
  }
  cards.push(<AssociationsRow info={info} />)
  modals.push(<AssociationsModal info={info} />)
}
