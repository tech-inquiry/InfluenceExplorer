import * as React from 'react'

export class DisplayUNICensorship extends React.Component<any, any> {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="card border-0">
        <div className="card-body ti-article mt-0">
          <h2>
            On the censorship of our report on government purchasing from
            Microsoft, Amazon, and Alphabet
          </h2>
          <p className="card-text ti-article mt-0">
            Jack Poulson, Executive Director, 2022-09-13
          </p>
          <p className="card-text ti-article mt-2">
            Tech Inquiry spent roughly the last year working on a 150 page
            report combining thousands of international government contracting
            records with bid protest decisions, reporting on (multi-)billion
            dollar classified contracts, and tax filings to better understand
            and contextualize the relationships tech giants have with the United
            States and its closest allies.{' '}
            <strong>
              We would love to share it with you, but both of the funders
              censored it, each for a different reason.
            </strong>
          </p>
          <p className="card-text ti-article mt-2">
            Tech Inquiry has always struggled with the tension between research
            and campaigning: We were originally founded by a group of tech
            whistleblowers who helped reveal their companies’ human rights
            violations – including drone warfare and suppression of dissent. But
            over time we developed an expertise in the fusion of government
            contracting data with broader public records. Campaigns often make
            use of our work, but we are deeply committed to objective analysis.
            Not just because this is what intellectual honesty demands, but
            because subjective analysis is easier to smear and dismiss.
          </p>
          <p className="card-text ti-article mt-2">
            Due to the incredible political influence of trillion dollar U.S.
            tech giants, Tech Inquiry has struggled to rely entirely on
            individual donations and has repeatedly been removed from -- or kept
            out of -- coalitions due to refusing to be managed by politically
            connected foundations. We were thus cautious but optimistic when an
            international network of unions (UNI Global Union) pitched us last
            year on documenting international government cloud contracts. While
            UNI would officially run the project, the funding would come from
            Friedrich-Ebert-Stiftung, a nonprofit arm of the Social Democratic
            Party of Germany (SPD).
          </p>
          <p className="card-text ti-article mt-2">
            UNI’s interest, as well as the official Request for Proposals,
            focused entirely on Amazon. Indeed, UNI is one of the{' '}
            <a href="https://makeamazonpay.com/">members</a> of the
            #MakeAmazonPay movement, which Tech Inquiry would happily endorse.
            But due to our experience with the importance of contextualizing
            critique of tech giants, we were able to increase the scope of the
            contract to a study of Amazon, Microsoft, and Alphabet. UNI agreed
            that broadening the focus would increase the quality of the study,
            even if it was evaluated purely under the metric of shedding light
            on Amazon.
          </p>
          <h3>The "perfect storm"</h3>
          <p className="card-text ti-article mt-0">
            In June of this year, Microsoft{' '}
            <a href="https://news.microsoft.com/2022/06/13/cwa-microsoft-announce-labor-neutrality-agreement/">
              announced
            </a>{' '}
            a labor neutrality agreement with the Communications Workers of
            America – which has undoubtedly become the most successful tech
            organizing union. According to the announcement,{' '}
            <em>
              “The foundation of the agreement is a commitment to mutual respect
              and open communication.”
            </em>
          </p>
          <p className="card-text ti-article mt-2">
            Just as Tech Inquiry was completing its year of work, our Executive
            Director received a call from a UNI official informing him that it
            would be <em>“a political problem”</em> if Microsoft ended up in the
            headline. Due to the neutrality agreement, Microsoft was now{' '}
            <em>“a friend of the labor movement”</em>. (A later phone call
            elaborated that CWA is an affiliate of UNI and helps pay UNI
            salaries. And that any critique of Microsoft could lead to one or
            more UNI employees being fired.)
          </p>
          <p className="card-text ti-article mt-2">
            Tech Inquiry firmly decided against protecting Microsoft by
            subverting its analysis and delivered its draft to UNI at the end of
            August. Both UNI and FES had demanded a pre-publication review and,
            after roughly a week, it was relayed by UNI that both organizations
            demanded that their funding be hidden from the final report unless
            it underwent modification. UNI’s demands were clear: either remove
            all material except the critique of Amazon or hide UNI’s funding.
            (It was suggested that Tech Inquiry wait six months before
            publishing its analysis of Microsoft and Google.)
          </p>
          <p className="card-text ti-article mt-2">
            FES’s concerns – as relayed by UNI -- were that Tech Inquiry’s
            report made reference to billion dollar cloud contracts with the{' '}
            <a href="https://www.fedscoop.com/cia-quietly-awards-billion-dollar-c2e-cloud-contract/">
              Central Intelligence Agency
            </a>{' '}
            and the{' '}
            <a href="https://www.theguardian.com/commentisfree/2021/oct/12/google-amazon-workers-condemn-project-nimbus-israeli-military-contract">
              Israel Defense Forces
            </a>
            . During the last year, Tech Inquiry’s research into Israeli
            procurement from American tech giants led to our discovery of
            Google’s training materials for its component of the $1.2 billion
            Nimbus contract with the Israeli government (which included explicit
            reference to the Israel Ministry of Defense). This discovery led to
            The Intercept{' '}
            <a href="https://theintercept.com/2022/07/24/google-israel-artificial-intelligence-project-nimbus/">
              elucidating
            </a>{' '}
            the scope of Google’s Nimbus contract. Ultimately Tech Inquiry was
            informed that both the Israel and U.S. offices of FES had demanded
            disassociation from the report.
          </p>
          <h3>
            Is objective analysis without a convenient political narrative
            valued?
          </h3>
          <p className="card-text ti-article mt-0">
            Tech Inquiry believes deeply in tech worker organizing as an
            accountability mechanism for trillion dollar American tech
            companies. Indeed, we worked with UNI precisely because we believe
            that grassroots worker power should be preferred to top-down
            management from billionaire-connected foundations.
          </p>
          <p className="card-text ti-article mt-2">
            But even the exposure of the most powerful tech labor union helping
            to suppress critique of a two trillion dollar company is often
            smeared as ‘anti-union’. As a result, Tech Inquiry has one less
            board member than it had before news broke. We have worked hard to
            stay supportive of tech labor organizing while remaining neutral on
            CWA itself.
          </p>
          <p className="card-text ti-article mt-2">
            The tech labor ecosystem is hotly divided on the question of whether
            it is acceptable to publicly critique even the problematic actions
            of major unions, and Tech Inquiry was forced to choose between
            subverting its tech accountability mission or being cut off from a
            large fraction of the tech labor movement. We chose not to hide
            either Microsoft’s secretive influence on labor unions or our
            biggest ever source of funding. (Our overall year-long contract with
            UNI and FES was for $49,000, and we have received a similar overall
            amount of individual donations.)
          </p>
          <p className="card-text ti-article mt-2">
            When the demands from both of our funders were combined, we would
            have been unable to publish our complete analysis of any of the
            three tech giants. We hope that the price Tech Inquiry is paying to
            expose this information – both financially and in terms of our
            relationships – is worth the clarity. The current redlines appear to
            be Microsoft,{' '}
            <a href="https://www.hrw.org/report/2021/04/27/threshold-crossed/israeli-authorities-and-crimes-apartheid-and-persecution">
              apartheid
            </a>
            , and understated critique. And pushback only increases as you
            combine the ingredients.
          </p>
          <p className="card-text ti-article mt-2">
            As it was explained to Tech Inquiry, the lesson learned is on{' '}
            <em>“the power of facts”</em>: UNI was unwilling to provide a
            statement of disavowal precisely because our report was objective.
            Refuting public records of the actions of a trillion dollar tech
            company would have revealed uncomfortable truths about the politics
            of neutrality agreements.
          </p>
          <p className="card-text ti-article mt-2">
            <em>Note:</em> In light of UNI's subsequent{' '}
            <a href="https://uniglobalunion.org/uni-statement-on-tech-inquiry-controversy/">
              public statement
            </a>{' '}
            that Tech Inquiry is now allowed to independently publish our
            report, we have uploaded the{' '}
            <a href="https://techinquiry.org/docs/InternationalCloud.pdf">
              final copy
            </a>{' '}
            of our report. The Intercept earlier{' '}
            <a href="https://theintercept.com/2022/09/07/microsoft-military-union-cwa/">
              published
            </a>{' '}
            a leaked version of our report.
          </p>
        </div>
      </div>
    )
  }
}
