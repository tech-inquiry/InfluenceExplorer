import { appConfig } from './constants'

export const kEmptyData =
  'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs='

export const defaultLogo = 'https://techinquiry.org/logos/question_mark.png'

export const kTokenLifeInHours = 3

export function storeToken(token: string, username: string) {
  localStorage.setItem('token-timestamp', JSON.stringify(Date.now()))
  localStorage.setItem('token', token)
  localStorage.setItem('username', username)
}

export function dropToken() {
  localStorage.removeItem('token')
  localStorage.removeItem('token-timestamp')
}

export function getMonthName(monthNumber: string): string {
  const monthMap = {
    '01': 'January',
    '02': 'February',
    '03': 'March',
    '04': 'April',
    '05': 'May',
    '06': 'June',
    '07': 'July',
    '08': 'August',
    '09': 'September',
    '10': 'October',
    '11': 'November',
    '12': 'December',
  }
  if (monthMap.hasOwnProperty(monthNumber)) {
    return monthMap[monthNumber]
  } else {
    console.log(`WARNING: Invalid month number string of ${monthNumber}.`)
    return monthNumber
  }
}

function stripLeadingZeroes(day: string): string {
  return day.replace(/^0+/, '')
}

// Take in a date of the form 2024.12.31, with a configurable separator beyond
// the assumed period.
export function getDateString(date: string, separator = '.'): string {
  if (!date) {
    return date
  }
  const dateParts = date.split(separator)
  const year = dateParts[0]
  if (dateParts.length == 3) {
    const month = getMonthName(dateParts[1])
    const day = stripLeadingZeroes(dateParts[2])
    return `${month} ${day}, ${year}`
  } else if (dateParts.length == 2) {
    const month = getMonthName(dateParts[1])
    return `${month} ${year}`
  } else {
    return year
  }
}

export function retrieveToken(
  tokenLifeInHours = kTokenLifeInHours,
  leewayInHours = 0.15
) {
  // Automatically drop the token if it isn't sufficiently fresh.
  // We add in 'leeway' browsing time to help prevent stale tokens after time
  // spent browsing a page prior to an API call.
  const tokenTimestamp = localStorage.getItem('token-timestamp')
  if (!tokenTimestamp) {
    dropToken()
    return undefined
  }
  const hour = 1000 * 60 * 60
  const fuseInHours = tokenLifeInHours - leewayInHours
  if (JSON.parse(tokenTimestamp) < Date.now() - fuseInHours * hour) {
    console.log(`Dropping token because timestamp ${tokenTimestamp} too old.`)
    dropToken()
    return undefined
  }

  return localStorage.getItem('token')
}

export function retrieveUsername(): string {
  return localStorage.getItem('username')
}

export function informalName(key: string, profile): string {
  if (!profile.name) {
    return key
  }

  const preferences = [
    profile.name.dba,
    profile.name.aka,
    profile.name.short,
    profile.name.full,
  ]
  for (const name of preferences) {
    if (name) {
      return name
    }
  }
}

export function getAuthorsString(authors: string[]): string {
  function formatName(author, lastThenFirst = true): string {
    if (Array.isArray(author)) {
      return author.join(' ')
    } else {
      return author.replace(/,/g, '')
    }
  }

  if (!authors) {
    return ''
  }
  if (authors.length == 1) {
    return formatName(authors[0])
  }
  if (authors.length == 2) {
    return formatName(authors[0]) + ' and ' + formatName(authors[1], false)
  }

  const lastAuthor = authors[authors.length - 1]
  return (
    authors
      .slice(0, authors.length - 1)
      .map((author) => formatName(author))
      .join(', ') +
    ', and ' +
    formatName(lastAuthor)
  )
}

export function getChicagoStyleAuthorsString(authors: string[]): string {
  function formatName(author, lastThenFirst = true): string {
    if (!lastThenFirst) {
      // We can simplify.
      if (Array.isArray(author)) {
        return author.join(' ')
      } else {
        return author.replace(/,/g, '')
      }
    }

    let lastName = undefined
    let firstName = undefined
    let suffix = undefined
    if (Array.isArray(author)) {
      firstName = author[0]
      if (author.length == 1) {
        return firstName
      }
      lastName = author[1]
      if (author.length == 3) {
        suffix = author[2]
      }
    } else {
      // This is a fall-back option, as we don't have a defined split between
      // first and last name. We will instead attempt to detect the last name.
      const namePieces = author.replace(/,/g, '').split(' ')
      if (namePieces.length == 1) {
        // This should be exceedingly rare.
        return namePieces[0]
      } else if (namePieces.length == 2) {
        firstName = namePieces[0]
        lastName = namePieces[1]
      } else {
        let lastLength = 1
        let suffixLength = 0
        if (
          namePieces[namePieces.length - 1] == 'Jr.' ||
          namePieces[namePieces.length - 1] == 'Sr.' ||
          namePieces[namePieces.length - 1] == 'II' ||
          namePieces[namePieces.length - 1] == 'III' ||
          namePieces[namePieces.length - 1] == 'IV'
        ) {
          suffixLength = 1
        }
        if (
          namePieces[namePieces.length - (lastLength + suffixLength + 1)] ==
            'de' ||
          namePieces[namePieces.length - (lastLength + suffixLength + 1)] ==
            'van'
        ) {
          lastLength += 1
        }
        firstName = namePieces
          .splice(0, namePieces.length - (lastLength + suffixLength))
          .join(' ')
        lastName = namePieces
          .splice(
            namePieces.length - (lastLength + suffixLength),
            namePieces.length - suffixLength
          )
          .join(' ')
        suffix = namePieces
          .splice(namePieces.length - suffixLength, namePieces.length)
          .join(' ')
      }
    }
    if (suffix) {
      return `${lastName}, ${firstName}, ${suffix}`
    } else {
      return `${lastName}, ${firstName}`
    }
  }

  if (!authors || authors == undefined || authors.length == 0) {
    return ''
  }

  if (authors.length == 1) {
    return formatName(authors[0])
  }
  if (authors.length == 2) {
    return formatName(authors[0]) + ' and ' + formatName(authors[1], false)
  }

  const lastAuthor = authors[authors.length - 1]
  return (
    authors
      .slice(0, authors.length - 1)
      .map((author) => formatName(author))
      .join(', ') +
    ', and ' +
    formatName(lastAuthor, false)
  )
}

export function endWithPunctuation(value: string): string {
  if (!value) {
    return ''
  }
  const endsWithPunctuation = value.endsWith('.') || value.endsWith('?')
  return endsWithPunctuation ? value : value + '.'
}

export function trimLabel(label: string, maxLength: number): string {
  if (label.length > maxLength) {
    label = label.slice(0, maxLength - 3) + '...'
  }
  return label
}

// Formats a number into a string with two digits after the period and a
// comma between each three signifant digits to the left of it.
export function numberWithCommas(x): string {
  if (isNaN(x) || x == undefined) {
    return ''
  } else {
    // Even if the input is loaded via TypeORM as a 'number', if the
    // column is nullable, we must still do a conversion to ensure the
    // member function 'toFixed' exists.
    return Number(x)
      .toFixed(2)
      .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }
}

export function amountWithCurrency(amount, currency: string): string {
  return numberWithCommas(amount) + ' ' + currency
}

// Formats a Date as string of form '[month] [date], [year]'.
export function dateWithoutTime(date: Date): string {
  if (date == null || isNaN(date.getTime())) {
    return 'N/A'
  }
  const month = new Intl.DateTimeFormat('en-US', { month: 'long' }).format(date)
  return `${month} ${date.getDate()}, ${date.getFullYear()}`
}

export function dateWithoutTimeLex(date: string): string {
  if (!date) {
    return ''
  }
  const splitTokens = ['T', 'Z', '+']
  let output = date
  for (const splitToken of splitTokens) {
    output = output.split(splitToken)[0]
  }
  return output
}

export function dateLex(date: string): string {
  if (!date) {
    return ''
  }
  const tokens = date.split('T')
  if (tokens.length >= 2) {
    return `${tokens[0]} ${tokens[1].substring(0, 8)}`
  } else if (tokens.length == 1) {
    return tokens[0]
  } else {
    return ''
  }
}

export function formatPhoneNumber(str: string): string {
  const cleaned = ('' + str).replace(/\D/g, '')
  if (cleaned.length < 10) {
    return str
  }
  const prefix =
    cleaned.length > 10 ? cleaned.substring(0, cleaned.length - 10) : ''
  const primary =
    cleaned.length > 10
      ? cleaned.substring(cleaned.length - 10, cleaned.length)
      : cleaned
  const match = primary.match(/^(\d{3})(\d{3})(\d{4})$/)
  if (match) {
    const primaryFormatted = `(${match[1]})${match[2]}-${match[3]}`
    if (prefix) {
      return `+${prefix} ${primaryFormatted}`
    } else {
      return primaryFormatted
    }
  }
  return str
}

export function prefixURL(url: string): string {
  if (!url) {
    return url
  }

  const urlLower = url.toLowerCase()
  if (urlLower.startsWith('http://') || urlLower.startsWith('https://')) {
    return url
  } else {
    // We do not know if the server supports https. If it does, it should
    // ideally redirect to https.
    return `http://${url}`
  }
}

export function simplifyURL(url: string): string {
  if (!url) {
    return url
  }

  let urlLower = url.toLowerCase()
  if (urlLower.startsWith('http://')) {
    url = url.substr(7)
  } else if (urlLower.startsWith('https://')) {
    url = url.substr(8)
  }

  urlLower = url.toLowerCase()
  if (urlLower.startsWith('www.')) {
    url = url.substr(4)
  }

  return url
}

export function concatenatePath(
  aPath: string,
  bPath: string,
  separator = '/'
): string {
  // We could instead detect the number of trailing separators and delete
  // them in one operation.
  while (aPath.endsWith(separator)) {
    aPath = aPath.slice(0, -1)
  }

  // We could instead detect the number of initial separators and delete
  // them in one operation.
  while (bPath.startsWith(separator)) {
    bPath = bPath.slice(1)
  }

  return aPath + separator + bPath
}

export function encodeURLComponent(component: string): string {
  if (!component) {
    return ''
  }
  return encodeURIComponent(component)
    .replace(/\./g, '%2E')
    .replace(/\//g, '%2F')
}

export function decodeURLComponent(component: string): string {
  if (!component) {
    return ''
  }
  // TODO(Jack Poulson): Add any decoders here.
  return component
}

export function getRetrieveURL(
  urlBase: string,
  query: string,
  isAgency: boolean,
  isEntity: boolean,
  isTag: boolean
): string {
  // TODO(Jack Poulson): Expand to support combined searches.

  let paramName = undefined
  if (isAgency) {
    paramName = 'agency'
  } else if (isEntity) {
    paramName = 'entity'
  } else if (isTag) {
    paramName = 'tagText'
  } else {
    paramName = 'text'
  }

  const token = retrieveToken()
  let queryStr = `${paramName}=${encodeURLComponent(query)}`
  if (token) {
    queryStr += `&token=${token}`
  }

  return `${urlBase}?${queryStr}`
}

export function canonicalText(text: string): string {
  if (!text) {
    return text
  }
  // TODO(Jack Poulson): Unescape HTML?
  return text.trim().toLowerCase().replace(/\s\s+/g, ' ')
}

export function generateQueryString(params: { [key: string]: string }) {
  // We add the guard parameter to prevent entities whose names end in a '.'
  // leading to the overall URL ending with a '.'. Due to the way that link
  // parsing works in many environments -- e.g., text messages or Twitter --
  // trailing periods get improperly truncated.
  params['guard'] = ''
  const joinedParams = Object.entries(params)
    .map(([k, v]) => `${encodeURLComponent(k)}=${encodeURLComponent(v)}`)
    .join('&')
  return '?' + joinedParams
}

export function changeDisplayedURLParameters(params: {
  [key: string]: string
}) {
  window.history.pushState(null, null, generateQueryString(params))
}

export function lobbyistNameFromObject(lobbyist: any): string {
  let pieces = []
  if (lobbyist.prefix_display) {
    pieces.push(lobbyist.prefix_display)
  }
  if (lobbyist.first_name) {
    pieces.push(lobbyist.first_name)
  }
  if (lobbyist.nickname) {
    pieces.push(lobbyist.nickname)
  }
  if (lobbyist.last_name) {
    pieces.push(lobbyist.last_name)
  }
  if (lobbyist.suffix_display) {
    pieces.push(lobbyist.suffix_display)
  }
  return pieces.join(' ')
}

export function isString(object): boolean {
  return typeof object == 'string' || object instanceof String
}

export function coerce(value: any, defaultValue: any): any {
  return value != undefined ? value : defaultValue
}

export function coerceUpperCase(value: string, defaultValue = ''): string {
  return value != undefined ? value.toUpperCase() : defaultValue.toUpperCase()
}

export function coerceLowerCase(value: string, defaultValue = ''): string {
  return value != undefined ? value.toLowerCase() : defaultValue.toLowerCase()
}

export function coerceArray(value: any): any[] {
  return Array.isArray(value) ? value : []
}

export const isDevelopmentBuild = () => {
  return process.env.NODE_ENV === 'development'
}

export const getEntityURL = (entity: string) => {
  return appConfig.explorerURL + generateQueryString({ entity: entity })
}

export const getBookURL = (label: string) => {
  return appConfig.explorerURL + generateQueryString({ book: label })
}

export const getSearchURL = (text: string) => {
  return appConfig.explorerURL + generateQueryString({ text: text })
}

export const getTagSearchURL = (text: string) => {
  return appConfig.explorerURL + generateQueryString({ tagText: text })
}

export const getSuggestions = (hint: string) => {
  const url =
    `${appConfig.apiURL}/entitySuggestions` +
    `?hint=${encodeURLComponent(hint)}`
  return fetch(url).then((res) => res.json())
}
