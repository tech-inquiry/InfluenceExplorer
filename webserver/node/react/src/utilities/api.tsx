export const kEUCountryCodes = [
  '1a',
  'al',
  'am',
  'ar',
  'at',
  'ba',
  'bd',
  'be',
  'bg',
  'bj',
  'bo',
  'by',
  'cg',
  'ch',
  'ci',
  'cm',
  'cn',
  'co',
  'cy',
  'cz',
  'de',
  'dk',
  'dz',
  'ec',
  'ee',
  'eg',
  'es',
  'et',
  'fi',
  'fr',
  'ge',
  'gp',
  'gr',
  'hr',
  'ht',
  'hu',
  'ie',
  'il',
  'in',
  'is',
  'it',
  'ke',
  'kg',
  'kn',
  'kz',
  'la',
  'lb',
  'li',
  'lt',
  'lu',
  'lv',
  'ma',
  'md',
  'me',
  'mg',
  'mk',
  'ml',
  'mt',
  'mw',
  'ni',
  'nl',
  'no',
  'np',
  'pl',
  'ps',
  'pt',
  'py',
  're',
  'ro',
  'rs',
  'ru',
  'se',
  'si',
  'sk',
  'sl',
  'sn',
  'td',
  'th',
  'tj',
  'tn',
  'tr',
  'tz',
  'ua',
  'ug',
  'uk',
  'us',
  'yt',
  'zm',
]

// Metadata for the procurement tabs for each supported EU country.
const kEUCountryProcurementTabs = {
  '1a': {
    logo: '/logos/government_of_the_republic_of_kosovo.svg',
    title: 'Kosovo (through EU)',
  },
  al: {
    logo: '/logos/government_of_the_republic_of_albania.svg',
    title: 'Albania (through EU)',
  },
  am: {
    logo: '/logos/government_of_the_republic_of_armenia.svg',
    title: 'Armenia (through EU)',
  },
  ar: {
    logo: '/logos/government_of_the_argentine_republic.svg',
    title: 'Argentina (through EU)',
  },
  at: {
    logo: '/logos/government_of_austria.svg',
    title: 'Austria',
  },
  ba: {
    logo: '/logos/government_of_bosnia_and_herzegovina.svg',
    title: 'Bosnia (through EU)',
  },
  bd: {
    logo: '/logos/government_of_the_peoples_republic_of_bangladesh.svg',
    title: 'Bangladesh (through EU)',
  },
  be: {
    logo: '/logos/government_of_the_kingdom_of_belgium.svg',
    title: 'Belgium',
  },
  bg: {
    logo: '/logos/government_of_the_republic_of_bulgaria.svg',
    title: 'Bulgaria',
  },
  bj: {
    logo: '/logos/government_of_the_republic_of_benin.svg',
    title: 'Benin (through EU)',
  },
  bo: {
    logo: '/logos/government_of_the_plurinational_state_of_bolivia.svg',
    title: 'Bolivia (through EU)',
  },
  by: {
    logo: '/logos/government_of_the_republic_of_belarus.svg',
    title: 'Belarus (through EU)',
  },
  cg: {
    logo: '/logos/government_of_the_republic_of_congo.svg',
    title: 'Congo (through EU)',
  },
  ch: {
    logo: '/logos/government_of_switzerland.svg',
    title: 'Switzerland',
  },
  ci: {
    logo: '/logos/government_of_the_republic_of_cote_divoire.svg',
    title: "Cote d'Ivoire (through EU)",
  },
  cm: {
    logo: '/logos/government_of_the_republic_of_cameroon.svg',
    title: 'Cameroon (through EU)',
  },
  cn: {
    logo: '/logos/government_of_the_peoples_republic_of_china.svg',
    title: "People's Republic of China (through EU)",
  },
  co: {
    logo: '/logos/government_of_the_republic_of_colombia.svg',
    title: 'Colombia (through EU)',
  },
  cy: {
    logo: '/logos/government_of_the_republic_of_cyprus.svg',
    title: 'Cyprus',
  },
  cz: {
    logo: '/logos/government_of_the_czech_republic.svg',
    title: 'Czech Republic',
  },
  de: {
    logo: '/logos/government_of_the_federal_republic_of_germany.svg',
    title: 'Germany',
  },
  dk: {
    logo: '/logos/government_of_denmark.svg',
    title: 'Denmark',
  },
  dz: {
    logo: '/logos/government_of_the_peoples_democratic_republic_of_algeria.svg',
    title: 'Algeria (through EU)',
  },
  ec: {
    logo: '/logos/government_of_the_republic_of_ecuador.svg',
    title: 'Ecuador (through EU)',
  },
  ee: {
    logo: '/logos/government_of_the_republic_of_estonia.svg',
    title: 'Estonia',
  },
  eg: {
    logo: '/logos/government_of_the_arab_republic_of_egypt.svg',
    title: 'Egypt (through EU)',
  },
  es: {
    logo: '/logos/government_of_the_kingdom_of_spain.svg',
    title: 'Spain',
  },
  et: {
    logo: '/logos/government_of_the_federal_democratic_republic_of_ethiopia.svg',
    title: 'Ethiopia (through EU)',
  },
  fi: {
    logo: '/logos/government_of_finland.svg',
    title: 'Finland',
  },
  fr: {
    logo: '/logos/government_of_france.svg',
    title: 'France',
  },
  ge: {
    logo: '/logos/government_of_georgia.svg',
    title: 'Georgia (through EU)',
  },
  gn: {
    logo: '/logos/government_of_the_republic_of_guinea.svg',
    title: 'Guinea (through EU)',
  },
  gp: {
    logo: '/logos/department_of_guadeloupe.svg',
    title: 'Guadeloupe (through EU)',
  },
  gr: {
    logo: '/logos/government_of_the_hellenic_republic.svg',
    title: 'Greece',
  },
  hr: {
    logo: '/logos/government_of_the_republic_of_croatia.svg',
    title: 'Croatia',
  },
  ht: {
    logo: '/logos/government_of_the_republic_of_haiti.svg',
    title: 'Haiti (through EU)',
  },
  hu: {
    logo: '/logos/government_of_hungary.svg',
    title: 'Hungary',
  },
  ie: {
    logo: '/logos/government_of_ireland.svg',
    title: 'Ireland',
  },
  il: {
    logo: '/logos/government_of_israel.svg',
    title: 'Israel (through EU)',
  },
  in: {
    logo: '/logos/government_of_the_republic_of_india.svg',
    title: 'India (through EU)',
  },
  is: {
    logo: '/logos/government_of_iceland.svg',
    title: 'Iceland',
  },
  it: {
    logo: '/logos/government_of_italy.svg',
    title: 'Italy',
  },
  ke: {
    logo: '/logos/government_of_the_republic_of_kenya.svg',
    title: 'Kenya (through EU)',
  },
  kg: {
    logo: '/logos/government_of_the_kyrgyz_republic.svg',
    title: 'Kyrgyz Republic (through EU)',
  },
  kn: {
    logo: '/logos/government_of_the_federation_of_saint_christopher_and_nevis.svg',
    title: 'Saint Kitts and Nevis (through EU)',
  },
  kz: {
    logo: '/logos/government_of_the_republic_of_kazakhstan.svg',
    title: 'Kazakhstan (through EU)',
  },
  la: {
    logo: '/logos/government_of_the_lao_peoples_democratic_republic.svg',
    title: 'Laos (through EU)',
  },
  lb: {
    logo: '/logos/government_of_the_republic_of_lebanon.svg',
    title: 'Lebanon (through EU)',
  },
  li: {
    logo: '/logos/government_of_the_principality_of_liechtenstein.svg',
    title: 'Liechtenstein (through EU)',
  },
  lt: {
    logo: '/logos/government_of_the_republic_of_lithuania.svg',
    title: 'Lithuania',
  },
  lu: {
    logo: '/logos/government_of_the_grand_duchy_of_luxembourg.svg',
    title: 'Luxembourg',
  },
  lv: {
    logo: '/logos/government_of_the_republic_of_latvia.svg',
    title: 'Latvia',
  },
  ma: {
    logo: '/logos/government_of_the_kingdom_of_morocco.svg',
    title: 'Morocco (through EU)',
  },
  md: {
    logo: '/logos/government_of_the_republic_of_moldova.svg',
    title: 'Moldova',
  },
  me: {
    logo: '/logos/government_of_montenegro.svg',
    title: 'Montenegro (through EU)',
  },
  mg: {
    logo: '/logos/government_of_the_republic_of_madagascar.svg',
    title: 'Madagascar (through EU)',
  },
  mk: {
    logo: '/logos/government_of_the_republic_of_north_macedonia.svg',
    title: 'Macedonia',
  },
  ml: {
    logo: '/logos/government_of_the_republic_of_mali.svg',
    title: 'Mali (through EU)',
  },
  mq: {
    logo: '/logos/department_of_martinique.svg',
    title: 'Martinique (through EU)',
  },
  mt: {
    logo: '/logos/government_of_the_republic_of_malta.svg',
    title: 'Malta',
  },
  mw: {
    logo: '/logos/government_of_the_republic_of_malawi.svg',
    title: 'Malawi (through EU)',
  },
  ni: {
    logo: '/logos/government_of_the_republic_of_nicaragua.svg',
    title: 'Nicaragua (through EU)',
  },
  nl: {
    logo: '/logos/government_of_the_netherlands.svg',
    title: 'Netherlands',
  },
  no: {
    logo: '/logos/government_of_norway.svg',
    title: 'Norway',
  },
  np: {
    logo: '/logos/government_of_the_federal_democratic_republic_of_nepal.svg',
    title: 'Nepal (through EU)',
  },
  pl: {
    logo: '/logos/government_of_poland.svg',
    title: 'Poland',
  },
  ps: {
    logo: '/logos/government_of_the_state_of_palestine.svg',
    title: 'Palestine (through EU)',
  },
  pt: {
    logo: '/logos/government_of_the_portuguese_republic.svg',
    title: 'Portugal',
  },
  py: {
    logo: '/logos/government_of_the_republic_of_paraguay.svg',
    title: 'Paraguay (through EU)',
  },
  re: {
    logo: '/logos/department_of_reunion.svg',
    title: 'Réunion (through EU)',
  },
  ro: {
    logo: '/logos/government_of_romania.svg',
    title: 'Romania',
  },
  rs: {
    logo: '/logos/government_of_the_republic_of_serbia.svg',
    title: 'Serbia',
  },
  ru: {
    logo: '/logos/government_of_the_russian_federation.svg',
    title: 'Russia (through EU)',
  },
  se: {
    logo: '/logos/government_of_sweden.svg',
    title: 'Sweden',
  },
  si: {
    logo: '/logos/government_of_the_republic_of_slovenia.svg',
    title: 'Slovenia',
  },
  sk: {
    logo: '/logos/government_of_the_slovak_republic.svg',
    title: 'Slovakia',
  },
  sl: {
    logo: '/logos/government_of_the_republic_of_sierra_leone.svg',
    title: 'Sierra Leone (through EU)',
  },
  sn: {
    logo: '/logos/government_of_the_republic_of_senegal.svg',
    title: 'Senegal (through EU)',
  },
  td: {
    logo: '/logos/government_of_the_republic_of_chad.svg',
    title: 'Chad (through EU)',
  },
  th: {
    logo: '/logos/government_of_the_kingdom_of_thailand.svg',
    title: 'Thailand (through EU)',
  },
  tj: {
    logo: '/logos/government_of_the_republic_of_tajikistan.svg',
    title: 'Tajikistan (through EU)',
  },
  tn: {
    logo: '/logos/government_of_the_republic_of_tunisia.svg',
    title: 'Tunisia (through EU)',
  },
  tr: {
    logo: '/logos/government_of_the_republic_of_turkey.svg',
    title: 'Turkey (through EU)',
  },
  tz: {
    logo: '/logos/government_of_the_united_republic_of_tanzania.svg',
    title: 'Tanzania (through EU)',
  },
  ua: {
    logo: '/logos/government_of_ukraine.svg',
    title: 'Ukraine (through EU)',
  },
  ug: {
    logo: '/logos/government_of_the_republic_of_uganda.svg',
    title: 'Uganda (through EU)',
  },
  uk: {
    logo: '/logos/government_of_the_united_kingdom.svg',
    title: 'United Kingdom (Pre-Brexit, through EU)',
  },
  us: {
    logo: '/logos/government_of_the_united_states.svg',
    title: 'United States (through EU)',
  },
  yt: {
    logo: '/logos/department_of_mayotte.svg',
    title: 'Mayotte (through EU)',
  },
  zm: {
    logo: '/logos/government_of_the_republic_of_zambia.svg',
    title: 'Zambia (through EU)',
  },
}

type EndPoint = {
  have: string
  get: string
  asAgency: boolean
  label: string
  logo: string
}

type SecuritiesEndPoint = {
  have: string
  getMeta: string
  getFilings: string
  asAgency: boolean
  label: string
  logo: string
}

// TODO(Jack Poulson): Collapse out the "as agency" variants after
// standardizing the APIs.
export let kProcurementPaths: { [key: string]: EndPoint } = {
  'AU (as vendor)': {
    have: 'au/procurement/have',
    get: 'au/procurement/get',
    asAgency: false,
    label: 'Australia (as vendor)',
    logo: '/logos/government_of_australia.svg',
  },
  'AU (as agency)': {
    have: 'au/procurement/have',
    get: 'au/procurement/get',
    asAgency: true,
    label: 'Australia (as agency)',
    logo: '/logos/government_of_australia.svg',
  },
  'CA proactive (as vendor)': {
    have: 'ca/procurement/proactive/have',
    get: 'ca/procurement/proactive/get',
    asAgency: false,
    label: 'Canadian proactive (as vendor)',
    logo: '/logos/government_of_canada.svg',
  },
  'CA proactive (as agency)': {
    have: 'ca/procurement/proactive/have',
    get: 'ca/procurement/proactive/get',
    asAgency: true,
    label: 'Canadian proactive (as agency)',
    logo: '/logos/government_of_canada.svg',
  },
  'CA PWSGC (as vendor)': {
    have: 'ca/procurement/pwsgc/have',
    get: 'ca/procurement/pwsgc/get',
    asAgency: false,
    label: 'Canadian Public Works (as vendor)',
    logo: '/logos/government_of_canada.svg',
  },
  'CA PWSGC (as agency)': {
    have: 'ca/procurement/pwsgc/have',
    get: 'ca/procurement/pwsgc/get',
    asAgency: true,
    label: 'Canadian Public Works (as agency)',
    logo: '/logos/government_of_canada.svg',
  },
  'Canada Buys (as vendor)': {
    have: 'ca/procurement/canadaBuys/have',
    get: 'ca/procurement/canadaBuys/get',
    asAgency: false,
    label: 'Canada Buys (as vendor)',
    logo: '/logos/government_of_canada.svg',
  },
  'Canada Buys (as agency)': {
    have: 'ca/procurement/canadaBuys/have',
    get: 'ca/procurement/canadaBuys/get',
    asAgency: true,
    label: 'Canada Buys (as agency)',
    logo: '/logos/government_of_canada.svg',
  },
  'IL Exempt (as vendor)': {
    have: 'il/procurement/exempt/have',
    get: 'il/procurement/exempt/get',
    asAgency: false,
    label: 'Israel Exempt (as vendor)',
    logo: '/logos/government_of_israel.svg',
  },
  'IL Exempt (as agency)': {
    have: 'il/procurement/exempt/have',
    get: 'il/procurement/exempt/get',
    asAgency: true,
    label: 'Israel Exempt (as agency)',
    logo: '/logos/government_of_israel.svg',
  },
  'IL Tender (as vendor)': {
    have: 'il/procurement/tender/have',
    get: 'il/procurement/tender/get',
    asAgency: false,
    label: 'Israel Tender (as vendor)',
    logo: '/logos/government_of_israel.svg',
  },
  'IL Tender (as agency)': {
    have: 'il/procurement/tender/have',
    get: 'il/procurement/tender/get',
    asAgency: true,
    label: 'Israel Tender (as agency)',
    logo: '/logos/government_of_israel.svg',
  },
  NZ: {
    have: 'nz/procurement/have',
    get: 'nz/procurement/get',
    asAgency: false,
    label: 'New Zealand',
    logo: '/logos/government_of_new_zealand.svg',
  },
  'UA tender (as vendor)': {
    have: 'ua/procurement/tender/have',
    get: 'ua/procurement/tender/get',
    asAgency: false,
    label: 'Ukraine tender (as vendor)',
    logo: '/logos/government_of_ukraine.svg',
  },
  'UA tender (as agency)': {
    have: 'ua/procurement/tender/have',
    get: 'ua/procurement/tender/get',
    asAgency: true,
    label: 'Ukraine tender (as agency)',
    logo: '/logos/government_of_ukraine.svg',
  },
  'UK (as vendor)': {
    have: 'uk/procurement/have',
    get: 'uk/procurement/get',
    asAgency: false,
    label: 'United Kingdom (as vendor)',
    logo: '/logos/government_of_the_united_kingdom.svg',
  },
  'UK (as agency)': {
    have: 'uk/procurement/have',
    get: 'uk/procurement/get',
    asAgency: true,
    label: 'United Kingdom (as agency)',
    logo: '/logos/government_of_the_united_kingdom.svg',
  },
  'US federal prime (as vendor)': {
    have: 'us/central/procurement/have',
    get: 'us/central/procurement/get',
    asAgency: false,
    label: 'US federal prime awards (as vendor)',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US federal prime (as agency)': {
    have: 'us/central/procurement/have',
    get: 'us/central/procurement/get',
    asAgency: true,
    label: 'US federal prime awards (as agency)',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US federal sub (as vendor)': {
    have: 'us/central/procurement/subaward/have',
    get: 'us/central/procurement/subaward/get',
    asAgency: false,
    label: 'US federal subawards (as vendor)',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US federal sub (as agency)': {
    have: 'us/central/procurement/subaward/have',
    get: 'us/central/procurement/subaward/get',
    asAgency: true,
    label: 'US federal subawards (as agency)',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US federal prime grants (as vendor)': {
    have: 'us/central/grant/have',
    get: 'us/central/grant/get',
    asAgency: false,
    label: 'US federal grants (as vendor)',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US federal prime grants (as agency)': {
    have: 'us/central/grant/have',
    get: 'us/central/grant/get',
    asAgency: true,
    label: 'US federal grants (as agency)',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US federal subgrants (as vendor)': {
    have: 'us/central/subgrant/have',
    get: 'us/central/subgrant/get',
    asAgency: false,
    label: 'US federal subgrants (as vendor)',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US federal subgrants (as agency)': {
    have: 'us/central/subgrant/have',
    get: 'us/central/subgrant/get',
    asAgency: true,
    label: 'US federal subgrants (as agency)',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US DOD announcements': {
    have: 'us/central/procurement/dod-announce/have',
    get: 'us/central/procurement/dod-announce/get',
    asAgency: false,
    label: 'US DoD announcements',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US federal opportunities': {
    have: 'us/central/procurement/opportunity/have',
    get: 'us/central/procurement/opportunity/get',
    asAgency: false,
    label: 'US contract opportunities',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US AZ': {
    have: 'us/az/procurement/have',
    get: 'us/az/procurement/get',
    asAgency: false,
    label: 'United States: Arizona',
    logo: '/logos/state_of_arizona.svg',
  },
  'US CA (as vendor)': {
    have: 'us/ca/procurement/have',
    get: 'us/ca/procurement/get',
    asAgency: false,
    label: 'California (as vendor)',
    logo: '/logos/state_of_california.svg',
  },
  'US CA (as agency)': {
    have: 'us/ca/procurement/have',
    get: 'us/ca/procurement/get',
    asAgency: true,
    label: 'California (as agency)',
    logo: '/logos/state_of_california.svg',
  },
  'US FL (as vendor)': {
    have: 'us/fl/procurement/have',
    get: 'us/fl/procurement/get',
    asAgency: false,
    label: 'Florida (as vendor)',
    logo: '/logos/state_of_florida.svg',
  },
  'US FL (as agency)': {
    have: 'us/fl/procurement/have',
    get: 'us/fl/procurement/get',
    asAgency: true,
    label: 'Florida (as agency)',
    logo: '/logos/state_of_florida.svg',
  },
  'US TX DPS (as agency)': {
    have: 'us/tx/dps/procurement/have',
    get: 'us/tx/dps/procurement/get',
    asAgency: true,
    label: 'United States: Texas DPS (as agency)',
    logo: '/logos/texas_department_of_public_safety.png',
  },
  'US TX DPS (as vendor)': {
    have: 'us/tx/dps/procurement/have',
    get: 'us/tx/dps/procurement/get',
    asAgency: false,
    label: 'United States: Texas DPS (as vendor)',
    logo: '/logos/texas_department_of_public_safety.png',
  },
  /*
  'US NY': {
    have: 'us/ny/procurement/have',
    get: 'us/ny/procurement/get'
    asAgency: false,
  }
  */
}
for (const countryCode of kEUCountryCodes) {
  kProcurementPaths[`EU ${countryCode.toUpperCase()}`] = {
    have: `eu/${countryCode}/procurement/have`,
    get: `eu/${countryCode}/procurement/get`,
    asAgency: false,
    label: kEUCountryProcurementTabs[countryCode].title,
    logo: kEUCountryProcurementTabs[countryCode].logo,
  }
  kProcurementPaths[`EU ${countryCode.toUpperCase()} (as agency)`] = {
    have: `eu/${countryCode}/procurement/have`,
    get: `eu/${countryCode}/procurement/get`,
    asAgency: true,
    label: kEUCountryProcurementTabs[countryCode].title + ' (as agency)',
    logo: kEUCountryProcurementTabs[countryCode].logo,
  }
}

export const kSearchProcurementKeys = Object.keys(kProcurementPaths)

export let kTagSearchProcurementKeys = [
  'AU (as vendor)',
  'CA proactive (as vendor)',
  'CA PWSGC (as vendor)',
  'Canada Buys (as vendor)',
  'IL Exempt (as vendor)',
  'IL Tender (as vendor)',
  'NZ',
  'UK (as vendor)',
  'US AZ',
  'US CA (as vendor)',
  'US DOD announcements',
  'US federal prime grants (as vendor)',
  'US federal opportunities',
  'US federal prime (as vendor)',
  'US federal sub (as vendor)',
  'US federal subgrants (as vendor)',
  'US FL (as vendor)',
]
for (const countryCode of kEUCountryCodes) {
  kTagSearchProcurementKeys.push(`EU ${countryCode.toUpperCase()}`)
}

export const kEntityProcurementKeys = Object.keys(kProcurementPaths)

kProcurementPaths['US federal (as agency)'] = {
  have: undefined,
  get: undefined,
  asAgency: true,
  label: 'United States (as agency)',
  logo: '/logos/government_of_the_united_states.svg',
}
kProcurementPaths['US federal (as vendor)'] = {
  have: undefined,
  get: undefined,
  asAgency: false,
  label: 'United States (as vendor)',
  logo: '/logos/government_of_the_united_states.svg',
}
kProcurementPaths['US federal grants (as agency)'] = {
  have: undefined,
  get: undefined,
  asAgency: true,
  label: 'United States grants (as agency)',
  logo: '/logos/government_of_the_united_states.svg',
}
kProcurementPaths['US federal grants (as vendor)'] = {
  have: undefined,
  get: undefined,
  asAgency: false,
  label: 'United States grants (as vendor)',
  logo: '/logos/government_of_the_united_states.svg',
}

export const kUSFederalAgencyProcurementSources = [
  'US federal prime (as agency)',
  'US federal sub (as agency)',
]
export const kUSFederalAgencyProcurementSource = 'US federal (as agency)'

export const kUSFederalVendorProcurementSources = [
  'US federal prime (as vendor)',
  'US federal sub (as vendor)',
]
export const kUSFederalVendorProcurementSource = 'US federal (as vendor)'

export const kUSFederalAgencyGrantSources = [
  'US federal prime grants (as agency)',
  'US federal subgrants (as agency)',
]
export const kUSFederalAgencyGrantSource = 'US federal grants (as agency)'

export const kUSFederalVendorGrantSources = [
  'US federal prime grants (as vendor)',
  'US federal subgrants (as vendor)',
]
export const kUSFederalVendorGrantSource = 'US federal grants (as vendor)'

export const kLobbyingPaths: { [key: string]: EndPoint } = {
  'CA communications': {
    have: 'ca/lobbying/communication/have',
    get: 'ca/lobbying/communication/get',
    asAgency: false,
    label: 'Canadian communications',
    logo: '/logos/government_of_canada.svg',
  },
  'CA registrations': {
    have: 'ca/lobbying/registration/have',
    get: 'ca/lobbying/registration/get',
    asAgency: false,
    label: 'Canadian registrations',
    logo: '/logos/government_of_canada.svg',
  },
  'EU Lobbying': {
    have: 'eu/central/lobbying/have',
    get: 'eu/central/lobbying/get',
    asAgency: false,
    label: 'EU Lobbying',
    logo: '/logos/european_union.svg',
  },
  'US FEC individual': {
    have: 'us/central/lobbying/fec/individual/have',
    get: 'us/central/lobbying/fec/individual/get',
    asAgency: false,
    label: 'US FEC individual contributions',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US OPR communications': {
    have: 'us/central/lobbying/opr/issues/have',
    get: 'us/central/lobbying/opr/issues/get',
    asAgency: false,
    label: 'US OPR communications',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US OPR contributions': {
    have: 'us/central/lobbying/opr/contributions/have',
    get: 'us/central/lobbying/opr/contributions/get',
    asAgency: false,
    label: 'US OPR contributions',
    logo: '/logos/government_of_the_united_states.svg',
  },
  'US CA activity': {
    have: 'us/ca/lobbying/activity/have',
    get: 'us/ca/lobbying/activity/get',
    asAgency: false,
    label: 'California activity',
    logo: '/logos/state_of_california.svg',
  },
  'US CA contributions': {
    have: 'us/ca/lobbying/contributions/have',
    get: 'us/ca/lobbying/contributions/get',
    asAgency: false,
    label: 'California contributions',
    logo: '/logos/state_of_california.svg',
  },
  'US CA pre-election expenditures': {
    have: 'us/ca/lobbying/pre-election-expenditure/have',
    get: 'us/ca/lobbying/pre-election-expenditure/get',
    asAgency: false,
    label: 'California pre-election expenditures',
    logo: '/logos/state_of_california.svg',
  },
}

export const kTagSearchLobbyingKeys = [
  'CA communications',
  'CA registrations',
  'EU Lobbying',
  'US CA activity',
  'US CA contributions',
  'US CA pre-election expenditures',
  'US FEC individual',
  'US OPR communications',
  'US OPR contributions',
]

export const kSecuritiesPaths: { [key: string]: SecuritiesEndPoint } = {
  'US federal securities': {
    have: 'us/central/haveSecurities',
    getMeta: 'us/central/securities/meta',
    getFilings: 'us/central/securities/filings',
    asAgency: false,
    label: 'US federal',
    logo: '/logos/government_of_the_united_states.svg',
  },
}
export const kEntitySecuritiesKeys = ['US federal securities']
export const kSearchSecuritiesKeys = []

export const kForeignAgentPaths: { [key: string]: EndPoint } = {
  'US foreign agents': {
    have: 'us/central/haveFARA',
    get: 'us/central/fara',
    asAgency: false,
    label: 'US foreign agents',
    logo: '/logos/government_of_the_united_states.svg',
  },
}
export const kEntityForeignAgentKeys = ['US foreign agents']
export const kSearchForeignAgentKeys = []

export const kLaborRelationsPaths: { [key: string]: EndPoint } = {
  'US federal labor relations': {
    have: 'us/central/haveLaborRelations',
    get: 'us/central/laborRelations',
    asAgency: false,
    label: 'US federal',
    logo: '/logos/government_of_the_united_states.svg',
  },
}

export const kLeakPaths: { [key: string]: EndPoint } = {
  Cablegate: {
    have: 'us/central/haveCablegate',
    get: 'us/central/cablegate',
    asAgency: false,
    label: 'Cablegate',
    logo: '/logos/united_states_department_of_state.svg',
  },
  'Israel Defense Forces': {
    have: 'il/leak/haveIDFEmail',
    get: 'il/leak/idfEmail',
    asAgency: false,
    label: 'Israel Defense Forces',
    logo: '/logos/israel_defense_forces.svg',
  },
  'Israel Ministry of Defense': {
    have: 'il/leak/haveDefenseEmail',
    get: 'il/leak/defenseEmail',
    asAgency: false,
    label: 'Israel Ministry of Defense',
    logo: '/logos/israel_ministry_of_defense.svg',
  },
  'Israel Ministry of Justice': {
    have: 'il/leak/haveJusticeEmail',
    get: 'il/leak/justiceEmail',
    asAgency: false,
    label: 'Israel Ministry of Justice',
    logo: '/logos/israel_ministry_of_justice.png',
  },
  Teixeira: {
    have: 'us/central/haveTeixeira',
    get: 'us/central/teixeira',
    asAgency: false,
    label: 'Teixeira',
    logo: '/logos/discord_inc.png',
  },
}

export const kTagSearchLeakKeys = [
  'Cablegate',
  'Israel Defense Forces',
  'Israel Ministry of Defense',
  'Israel Ministry of Justice',
  'Teixeira',
]
