import * as React from 'react'

import {
  coerce,
  dateWithoutTime,
  formatPhoneNumber,
  getEntityURL,
  prefixURL,
  simplifyURL,
} from './util'

function sanitizeLabel(label: string): string {
  return label.replace(/ /g, '-').replaceAll('(', '').replaceAll(')', '')
}

export function modalId(label: string): string {
  return `ti-modal-${sanitizeLabel(label)}`
}

export function modalCloseButtonId(label: string): string {
  return `ti-modal-close-button-${sanitizeLabel(label)}`
}

export class TableKeyValue extends React.Component<any, any> {
  render() {
    const guard = this.props.useGuard
      ? this.props.guard
      : this.props.vHTML || this.props.v
    return guard ? (
      <tr className="font-monospace">
        <th scope="row" className="text-end px-2">
          {this.props.k}
        </th>
        {this.props.vHTML ? (
          <td
            className="text-start px-2"
            dangerouslySetInnerHTML={{ __html: this.props.vHTML }}
          ></td>
        ) : (
          <td className="text-start">{this.props.v}</td>
        )}
      </tr>
    ) : undefined
  }
}

export function includeTableKeyValue(
  features,
  k,
  v,
  useGuard = undefined,
  guard = undefined
) {
  if (useGuard ? guard : v) {
    features.push(<TableKeyValue key={features.length} k={k} v={v} />)
  }
}

export function includeTableKeyHTML(
  features,
  k,
  v,
  useGuard = undefined,
  guard = undefined
) {
  if (useGuard ? guard : v) {
    features.push(<TableKeyValue key={features.length} k={k} vHTML={v} />)
  }
}

export class TableKeyDate extends React.Component<any, any> {
  render() {
    return (
      <TableKeyValue
        k={this.props.k}
        v={dateWithoutTime(new Date(this.props.v))}
        useGuard="true"
        guard={this.props.v}
      />
    )
  }
}

export function includeTableKeyDate(features, k, v) {
  if (v && !isNaN(new Date(v).getTime())) {
    features.push(<TableKeyDate key={features.length} k={k} v={v} />)
  }
}

function compactGraf(body: any): any {
  return <p className="ti-compact">{body}</p>
}

export class Address extends React.Component<any, any> {
  render() {
    const address = this.props.address
    function renderStreetAddress() {
      if (!('streetLines' in address)) {
        return undefined
      }
      let lines = []
      for (const line of address.streetLines) {
        if (line?.trim()) {
          lines.push(compactGraf(line.trim()))
        }
      }
      if (!lines.length) {
        return undefined
      }
      return <TableKeyValue key="street-address" k="Street Address" v={lines} />
    }
    return [
      renderStreetAddress(),
      <TableKeyValue key="city" k="City" v={address.city} />,
      <TableKeyValue key="state" k="State" v={address.state} />,
      <TableKeyValue
        key="postal-code"
        k="Postal Code"
        v={<code className="text-dark">{address.postalCode}</code>}
        useGuard={true}
        guard={address.postalCode}
      />,
      <TableKeyValue key="country" k="Country" v={address.country} />,
      <TableKeyValue
        key="email"
        k="Email"
        v={
          <a href={`mailto:${address.email}`}>
            <code className="text-dark">{address.email}</code>
          </a>
        }
        useGuard={true}
        guard={address.email}
      />,
      <TableKeyValue
        key="phone"
        k="Phone"
        v={
          <code className="text-dark">{formatPhoneNumber(address.phone)}</code>
        }
        useGuard={true}
        guard={address.phone}
      />,
      <TableKeyValue key="fax" k="Fax" v={formatPhoneNumber(address.fax)} />,
      <TableKeyValue
        key="url"
        k="URL"
        v={<a href={prefixURL(address.url)}>{simplifyURL(address.url)}</a>}
        useGuard={true}
        guard={address.url}
      />,
    ]
  }
}

export function simpleAddress(address) {
  let lines = []
  if ('streetLines' in address) {
    for (const line of address.streetLines) {
      if (line?.trim()) {
        lines.push(line.trim())
      }
    }
  }
  if (address.city && address.state && address.postalCode) {
    lines.push(`${address.city}, ${address.state} ${address.postalCode}`)
  } else if (address.city && address.state) {
    lines.push(`${address.city}, ${address.state}`)
  } else if (address.city && address.postalCode) {
    lines.push(`${address.city}, ${address.postalCode}`)
  } else if (address.state && address.postalCode) {
    lines.push(`${address.state} ${address.postalCode}`)
  } else if (address.state) {
    lines.push(address.state)
  } else if (address.postalCode) {
    lines.push(address.postalCode)
  }
  if (address.country) {
    lines.push(address.country)
  }
  if (address.email) {
    lines.push(<a href={`mailto:${address.email}`}>{address.email}</a>)
  }
  if (address.phone) {
    lines.push(`Phone: ${formatPhoneNumber(address.phone)}`)
  }
  if (address.fax) {
    lines.push(`Fax: ${formatPhoneNumber(address.fax)}`)
  }
  if (address.url) {
    lines.push(<a href={prefixURL(address.url)}>{simplifyURL(address.url)}</a>)
  }
  if (!lines.length) {
    return undefined
  }
  return lines.map((x) => compactGraf(x))
}

export class CloseButton extends React.Component<any, any> {
  render() {
    return (
      <button
        type="button"
        id={modalCloseButtonId(this.props.label)}
        className="btn-close btn-lg mr-2"
        style={{ fontSize: '2.0rem' }}
        data-bs-dismiss="modal"
        aria-label="Close"
      ></button>
    )
  }
}

export class SecondaryCloseButton extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = this.props.label ? this.props.label : 'Close'
  }
  render() {
    return (
      <button
        type="button"
        className="btn btn-secondary mr-3"
        style={{ fontSize: '1.75rem' }}
        data-bs-dismiss="modal"
      >
        {this.label}
      </button>
    )
  }
}

export class EntityRowModal extends React.Component<any, any> {
  render() {
    if (!this.props.title && !this.props.body) {
      return undefined
    }

    const dialogExtraClasses = coerce(
      this.props.dialogExtraClasses,
      'modal-xl modal-fullscreen-xl-down'
    )
    const dialogClasses = `modal-dialog ${dialogExtraClasses}`
    return (
      <div className="modal" id={modalId(this.props.label)} tabIndex={-1}>
        <div className={dialogClasses}>
          <div className="modal-content">
            <div className="modal-header">
              <h3 className="modal-title font-monospace">{this.props.title}</h3>
              <CloseButton label={this.props.label} />
            </div>
            <div className="modal-body mx-2 my-0">{this.props.body}</div>
            <div className="modal-footer">
              <SecondaryCloseButton />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export class RowHelper extends React.Component<any, any> {
  background: string

  constructor(props) {
    super(props)
    this.background = props.disableLightBackground ? '' : 'bg-light'
  }

  render() {
    const imgDivClasses = `col-12 col-md-4 align-items-center py-3 px-3 ${this.background}`
    return (
      <div className="row flex-md-row-reverse align-items-center py-4">
        <div className={imgDivClasses}>{this.props.capImage}</div>
        <div className="col-12 col-md-8 py-2">
          <div className="text-center">{this.props.button}</div>
          <div className="d-flex justify-content-center px-5">
            <p className="text-muted font-monospace text-start lh-sm">
              {this.props.footerText}
            </p>
          </div>
        </div>
      </div>
    )
  }
}

export class RowCardHelper extends React.Component<any, any> {
  background: string

  constructor(props) {
    super(props)
    this.background = props.disableLightBackground ? '' : 'bg-light'
  }

  render() {
    const cardClasses = 'card border-light my-1 me-5'
    const imgDivClasses = `col-12 col-md-4 align-items-center py-3 px-3 ${this.background}`

    const cardContent = [
      // TESTING pb-1 and mb-1
      <div className="card-body pb-1 mb-1">
        <h4 className="card-title pb-0 mb-0">{this.props.title}</h4>
      </div>,
      <div className="card-footer">{this.props.footerText}</div>,
    ]

    const card = this.props.url ? (
      <a href={this.props.url}>
        <div className={cardClasses} role="button">
          {cardContent}
        </div>
      </a>
    ) : (
      <div
        className={cardClasses}
        role="button"
        data-bs-toggle="modal"
        data-bs-target={'#' + modalId(this.props.label)}
      >
        {cardContent}
      </div>
    )

    return (
      <div className="row flex-md-row-reverse align-items-center py-4">
        <div className={imgDivClasses}>{this.props.capImage}</div>
        <div className="col-12 col-md-8 py-2">{card}</div>
      </div>
    )
  }
}

// TODO(Jack Poulson): Update the parameter names.
export class EntityRow extends React.Component<any, any> {
  render() {
    return (
      <RowCardHelper
        capImage={this.props.capImage}
        title={this.props.buttonTitle}
        label={this.props.label}
        url={this.props.buttonURL}
        footerText={this.props.footerText}
        disableLightBackground={this.props.disableLightBackground}
      />
    )
  }
}

// TODO(Jack Poulson): Deprecate this class.
export class EntityRowCard extends React.Component<any, any> {
  render() {
    return (
      <RowCardHelper
        capImage={this.props.capImage}
        title={this.props.buttonTitle}
        label={this.props.label}
        url={this.props.buttonURL}
        footerText={this.props.footerText}
        disableLightBackground={this.props.disableLightBackground}
      />
    )
  }
}

export function renderEntityAnnotation(annotation, title = 'ID') {
  function renderNameCorrection() {
    const nameCorrection =
      annotation.text.toLowerCase() != annotation.origText.toLowerCase()
    if (!nameCorrection) {
      return undefined
    }
    return (
      <p className="ti-compact font-monospace">
        <small className="text-muted">
          As written: "{annotation.origText}"
        </small>
      </p>
    )
  }

  const item = (
    <div>
      <a href={getEntityURL(annotation.text)}>
        {annotation.logo
          ? compactGraf(
              <img className="ti-filing-entity-logo" src={annotation.logo} />
            )
          : undefined}
        {compactGraf(annotation.stylizedText)}
      </a>
      {renderNameCorrection()}
    </div>
  )
  return <TableKeyValue k={title} v={item} />
}

export function getCapImage(
  imageURL,
  clickURL = undefined,
  height = '100px',
  maxWidth = '450px',
  opacity = 1.0
) {
  return (
    <div
      className="mx-auto ti-cap-img"
      style={{
        opacity: opacity,
        height: height,
        maxWidth: maxWidth,
        backgroundImage: `url(${imageURL})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: '50% 50%',
        backgroundSize: 'contain',
      }}
      onClick={function (evt) {
        if (clickURL) {
          window.location = clickURL
        }
      }}
    ></div>
  )
}

export function dropdownSource(
  source,
  index,
  enabledSources,
  sourceMap,
  idFunc
) {
  if (enabledSources.includes(source)) {
    const innerItem =
      sourceMap == undefined
        ? source
        : [
            sourceMap[source].logo ? (
              <img src={sourceMap[source].logo} className="dropdown-flag" />
            ) : undefined,
            sourceMap[source].label,
          ]
    return (
      <li key={index}>
        <a
          className="dropdown-item"
          href="#"
          data-bs-toggle="modal"
          data-bs-target={'#' + idFunc(source)}
        >
          {innerItem}
        </a>
      </li>
    )
  } else {
    return (
      <li key={index}>
        <a className="dropdown-item" href="#">
          {source}
        </a>
      </li>
    )
  }
}

// We have switched this to assuming a row format.
export function getCardDropdownButton(title, id, items) {
  return (
    <div className="dropdown">
      <button
        className="btn btn-secondary btn-lg ti-row-btn mt-0 px-3 py-3 mb-3 dropdown-toggle shadow"
        type="button"
        id={id}
        data-bs-toggle="dropdown"
        aria-expanded="false"
      >
        {title}
      </button>
      <ul className="dropdown-menu font-monospace" aria-labelledby={id}>
        {items}
      </ul>
    </div>
  )
}
