type AppConfig = {
  root: string
  explorerURL: string
  apiURL: string
}

const root = process.env.EXPLORER_HOSTNAME || 'https://techinquiry.org/'
const apiOrigin = process.env.API_ORIGIN || root
console.log(process.env.NODE_ENV)
//TODO: remove redundant constant
const explorerURL = root
export const appConfig: AppConfig = {
  root: root,
  explorerURL: explorerURL,
  apiURL: `${apiOrigin}api`,
}

export const autocompleteLabel = 'autocomplete'
export const maxInputLength = 150
export const DEBUG = true

export const routes = {
  submitEntity: '/submit-entity',
}
