import * as React from 'react'

function wrapURL(content, url) {
  if (url) {
    return <a href={url}>{content}</a>
  } else {
    return content
  }
}

export class SpinnerImage extends React.Component<any, any> {
  constructor(props) {
    super(props)
    this.state = { loaded: false }
  }

  render() {
    const spinnerWrapperClasses = this.props.spinnerWrapperClasses
    const spinnerWrapperStyle = this.props.spinnerWrapperStyle
    const spinnerClasses = this.props.spinnerClasses
      ? this.props.spinnerClasses
      : 'spinner-border'
    const spinnerStyle = this.props.spinnerStyle
    const img = (
      <img
        className={this.props.imgClasses}
        style={
          this.state.loaded
            ? this.props.imgStyle
            : { display: 'none', height: '0px', width: '0px' }
        }
        src={this.props.src}
        onLoad={() => this.setState({ loaded: true })}
      />
    )
    const wrappedImg = wrapURL(img, this.props.url)
    return (
      <div className={this.props.divClasses} style={this.props.divStyle}>
        {this.state.loaded ? null : (
          <div className={spinnerWrapperClasses} style={spinnerWrapperStyle}>
            <div className={spinnerClasses} style={spinnerStyle} role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        )}
        {wrappedImg}
      </div>
    )
  }
}
