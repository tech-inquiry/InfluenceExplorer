import * as React from 'react'

import { RotatingImage } from './RotatingImage'
import * as ui from './utilities/ui'
import * as util from './utilities/util'

class ChildrenRow extends React.Component<any, any> {
  label: string
  items: any[]

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'children')

    this.items = []
    const info = this.props.info
    const profile = info.profile
    if (profile && profile.hasOwnProperty('children')) {
      for (const child of profile.children) {
        const annotation = profile.entityAnnotations[child]
        if (annotation.logoSmall) {
          this.items.push({
            image: annotation.logoSmall,
            url: util.getEntityURL(child),
          })
        }
      }
    }
  }

  render() {
    const info = this.props.info
    const entity = info.query
    const profile = info.profile
    if (!profile || !profile.children || !profile.children.length) {
      return undefined
    }
    const selfAnnotation = profile.entityAnnotations[entity]

    const buttonTitle = 'Components'

    const capImage = (
      <RotatingImage
        items={this.items}
        opacity={1.0}
        development={info.development}
      />
    )

    const footerText = (
      <span>
        Click to view{' '}
        <span className="fw-bold">
          {profile.children.length + ' direct '}
          {profile.children.length > 1 ? 'children' : 'child'}
        </span>{' '}
        of <span className="fst-italic">{selfAnnotation.stylizedText}</span>
        {selfAnnotation.stylizedText.endsWith('.') ? '' : '.'}
      </span>
    )

    return (
      <ui.EntityRowCard
        label={this.label}
        buttonTitle={buttonTitle}
        capImage={capImage}
        footerText={footerText}
      />
    )
  }
}

class ChildrenModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'children')
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    if (!profile || !profile.children || !profile.children.length) {
      return undefined
    }

    const modalTitle = 'Child Entities'
    const modalBody = (
      <div className="container mx-2 my-1 px-0">
        {profile.children.map(info.entityToItem)}
      </div>
    )

    return (
      <ui.EntityRowModal
        label={this.label}
        title={modalTitle}
        body={modalBody}
        dialogExtraClasses="modal-lg modal-fullscreen-lg-down"
      />
    )
  }
}

export function insertCardAndModal(info, cards: any[], modals: any[]): void {
  const profile = info.profile
  if (!profile || !profile.children || !profile.children.length) {
    return
  }
  cards.push(<ChildrenRow info={info} />)
  modals.push(<ChildrenModal info={info} />)
}
