import * as React from 'react'

import { RotatingImage } from './RotatingImage'
import * as ui from './utilities/ui'
import * as util from './utilities/util'

function encodeBookURL(tag) {
  return `https://techinquiry.org?book=${tag}`
}

class BookRow extends React.Component<any, any> {
  label: string
  items: any[]

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'book')

    this.items = []
    if (this.props.info.profile && this.props.info.profile.book) {
      this.items = this.props.info.profile.book.map(function (book) {
        return { image: book.coverSmall, url: encodeBookURL(book.tag) }
      })
    }
  }

  render() {
    const entity = this.props.info.query
    const profile = this.props.info.profile
    if (!profile || !profile.book) {
      return undefined
    }

    const selfAnnotation = profile.entityAnnotations[entity]

    const buttonTitle = 'Books'

    const capImage = (
      <RotatingImage
        items={this.items}
        height="250px"
        opacity={1.0}
        isTall={true}
        shadow={true}
        development={this.props.info.development}
      />
    )

    const footerText = (
      <span>
        Tech Inquiry tagged{' '}
        <span className="fw-bold">
          {profile.book.length + ' '}
          {profile.book.length > 1 ? 'books' : 'book'}
        </span>{' '}
        related to{' '}
        <span className="fst-italic">{selfAnnotation.stylizedText}</span>
        {selfAnnotation.stylizedText.endsWith('.') ? ' ' : '. '}
        The recommendations are in the spirit of references to be read
        critically rather than as endorsements of the opinions contained
        therein.
      </span>
    )

    return (
      <ui.EntityRow
        label={this.label}
        buttonTitle={buttonTitle}
        capImage={capImage}
        footerText={footerText}
        disableLightBackground={true}
      />
    )
  }
}

class BookModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'book')
  }

  render() {
    const entity = this.props.info.query
    const profile = this.props.info.profile
    if (!profile || !profile.book) {
      return undefined
    }

    function bookToItem(book, index) {
      const coverImage = (
        <img className="ti-book-cover-list-item shadow-lg" src={book.cover} />
      )
      const coverEntry = book.url ? (
        <a href={encodeBookURL(book.tag)}>{coverImage}</a>
      ) : (
        coverImage
      )

      const authorsEntry = util.endWithPunctuation(
        util.getAuthorsString(book.authors)
      )

      return (
        <div
          key={index}
          className="row flex-md-row-reverse align-items-center py-3"
        >
          <div className="col-12 col-md-4 align-items-center px-3">
            <div className="text-center">{coverEntry}</div>
          </div>
          <div className="col-12 col-md-8">
            <h2 className="fw-bold font-monospace mb-2">
              <a
                className="text-decoration-none"
                href={encodeBookURL(book.tag)}
              >
                {book.title}
              </a>
            </h2>
            {book.subtitle ? (
              <h4 className="font-monospace mb-2">{book.subtitle}</h4>
            ) : undefined}
            <h5 className="font-monospace mb-3">
              {authorsEntry} <span className="fst-italic">{book.org}</span>,{' '}
              {book.date}
            </h5>
            <p className="font-monospace" style={{ fontSize: '125%' }}>
              {book.note ? book.note : ''}
            </p>
          </div>
        </div>
      )
    }

    const modalTitle = 'Select books (reverse-chronological)'
    const modalBody = (
      <div className="container px-3">{profile.book.map(bookToItem)}</div>
    )

    return (
      <ui.EntityRowModal
        label={this.label}
        title={modalTitle}
        body={modalBody}
        dialogExtraClasses="modal-fullscreen"
      />
    )
  }
}

export function insertCardAndModal(info, cards: any[], modals: any[]): void {
  const profile = info.profile
  if (!profile || !profile.book) {
    return
  }
  cards.push(<BookRow info={info} />)
  modals.push(<BookModal info={info} />)
}
