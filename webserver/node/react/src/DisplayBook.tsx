import * as React from 'react'

import { SpinnerImage } from './SpinnerImage'
import { createMasonry } from './Masonry'

import * as util from './utilities/util'

export class DisplayBook extends React.Component<any, any> {
  constructor(props) {
    super(props)

    this.bookEntityToItem = this.bookEntityToItem.bind(this)
  }

  bookEntityToItem(bookEntity) {
    const book = this.props.book
    const name = util.isString(bookEntity) ? bookEntity : bookEntity.name
    const annotation = book.entityAnnotations[name]

    const stylizedName = annotation.stylizedText
    const logoURL = annotation.logo
    const nameEntry = (
      <a className="text-decoration-none" href={util.getEntityURL(name)}>
        {stylizedName}
      </a>
    )
    const logoEntry = (
      <a href={util.getEntityURL(name)}>
        <img
          className="ti-association-list-entity-logo"
          src={util.coerce(logoURL, util.defaultLogo)}
        />
      </a>
    )
    const note = util.isString(bookEntity) ? undefined : bookEntity.note
    return (
      <div className="row flex-md-row-reverse align-items-center mx-0 px-0 py-3">
        <div className="col-12 col-md-4 align-items-center px-3 bg-light">
          <div className="text-center">{logoEntry}</div>
        </div>
        <div className="col-12 col-md-8">
          <h3 className="fw-bold mb-2">{nameEntry}</h3>
          <p className="lh-sm" style={{ fontSize: '1.75rem' }}>
            {note}
          </p>
        </div>
      </div>
    )
  }

  render() {
    const book = this.props.book
    const url = book.url

    const height = '28em'
    const divClasses = ''
    const divStyle = { height: height }
    const spinnerWrapperClasses =
      'd-flex align-items-center justify-content-center'
    const spinnerWrapperStyle = { height: height }
    const spinnerClasses = 'spinner-border'
    const spinnerStyle = {}
    const imgClasses = 'd-block mx-auto img-fluid shadow-lg ti-expand-on-hover'
    const imgStyle = {
      maxHeight: height,
      width: 'auto',
    }
    const coverEntry = (
      <SpinnerImage
        src={book.cover}
        url={url}
        divClasses={divClasses}
        divStyle={divStyle}
        spinnerWrapperClasses={spinnerWrapperClasses}
        spinnerWrapperStyle={spinnerWrapperStyle}
        spinnerClasses={spinnerClasses}
        spinnerStyle={spinnerStyle}
        imgClasses={imgClasses}
        imgStyle={imgStyle}
      />
    )

    const authorsEntry = util.endWithPunctuation(
      util.getAuthorsString(book.authors)
    )

    const bookTitleRow = (
      <div className="row flex-md-row-reverse align-items-center py-3 mb-5">
        <div className="col-12 col-md-4 align-items-center px-3">
          <div className="text-center">{coverEntry}</div>
        </div>
        <div className="col-12 col-md-8 align-items-start">
          <h1 className="fw-bold mb-2">
            <a className="text-decoration-none" href={url}>
              {book.title}
            </a>
          </h1>
          {book.subtitle ? (
            <h3 className="font-monospace mb-2">{book.subtitle}</h3>
          ) : undefined}
          <h4 className="font-monospace mb-3">
            {authorsEntry} <span className="fst-italic">{book.org}</span>,{' '}
            {book.date}
          </h4>
          {url ? (
            <h4 className="font-monospace mb-2">
              <a href={url}>{util.simplifyURL(url)}</a>
            </h4>
          ) : undefined}
          <p style={{ fontSize: '125%' }}>{book.note ? book.note : ''}</p>
        </div>
      </div>
    )

    const columnsCountBreakPoints = {
      225: 2,
      350: 3,
      475: 4,
      600: 5,
      725: 6,
      850: 7,
    }
    return (
      <>
        <div className="container px-3 mb-5">{bookTitleRow}</div>
        {book.annotatedSalienceMap != undefined
          ? createMasonry(
              book.annotatedSalienceMap,
              this.props.logoPrefix,
              60,
              80,
              columnsCountBreakPoints
            )
          : undefined}
        <div className="card mt-4">
          <div className="card-body">
            <h3 className="card-title">Tagged entities</h3>
            <div className="container px-3">
              {book.entities.map(this.bookEntityToItem)}
            </div>
          </div>
        </div>
      </>
    )
  }
}
