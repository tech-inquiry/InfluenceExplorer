import * as React from 'react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import { App } from './apps/Cable'

import '../css/style.css'

const root = createRoot(document.getElementById('ti-app'))
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
)
