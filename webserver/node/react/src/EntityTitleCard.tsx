import * as React from 'react'

import { FilingWithTags } from './FilingWithTags'
import { SpinnerImage } from './SpinnerImage'
import * as util from './utilities/util'

function wrapURL(content, url, extraGuard = true) {
  if (url && extraGuard) {
    return <a href={url}>{content}</a>
  } else {
    return content
  }
}

function getYear(date: string): string {
  if (!date) {
    return ''
  }
  return date.split('-')[0]
}

function pushSimpleIcon(iconList: any[], url: string, classString: string) {
  if (url) {
    iconList.push(
      <a className="ti-icon" key={url} href={url}>
        <i className={classString}></i>
      </a>
    )
  }
}

function pushTwitterIcon(iconList: any[], twitterObj) {
  if (twitterObj) {
    const twitter = twitterObj
    const account = Object.keys(twitter)[0]
    const accountURL = `https://twitter.com/${account}`
    pushSimpleIcon(iconList, accountURL, 'bi bi-twitter')
  }
}

function entityTitleImage(logo: string, url: string) {
  const height = '225px'
  const width = '350px'
  const divClasses = 'border'
  const divStyle = { height: height, width: width, backgroundColor: '#fafafa' }
  const spinnerWrapperClasses =
    'd-flex align-items-center justify-content-center'
  const spinnerWrapperStyle = { height: height, width: width }
  const spinnerClasses = 'spinner-border'
  const spinnerStyle = {}
  const imgClasses = 'd-block mx-auto img-fluid px-3 py-3 ti-expand-on-hover'
  const imgStyle = {
    height: height,
    width: width,
    objectFit: 'contain',
  }
  return (
    <SpinnerImage
      src={logo}
      url={url}
      divClasses={divClasses}
      divStyle={divStyle}
      spinnerWrapperClasses={spinnerWrapperClasses}
      spinnerWrapperStyle={spinnerWrapperStyle}
      spinnerClasses={spinnerClasses}
      spinnerStyle={spinnerStyle}
      imgClasses={imgClasses}
      imgStyle={imgStyle}
    />
  )
}

export class EntityTitleCard extends FilingWithTags {
  constructor(props) {
    super(props)
    super.setEndpoint('/api/entity')
    super.setUniqueKeys(['entity'])
  }

  render() {
    const entity = this.props.entity
    const profile = this.props.profile
    const selfAnnotation = profile.entityAnnotations[entity]

    const selfImage = entityTitleImage(
      util.coerce(selfAnnotation.logo, util.defaultLogo),
      selfAnnotation.url
    )

    let iconList = []
    if (profile.urls) {
      const urls = profile.urls
      pushSimpleIcon(iconList, urls.institutional, 'bi bi-link-45deg')
      pushSimpleIcon(iconList, urls.linkedin, 'bi bi-linkedin')
      pushTwitterIcon(iconList, profile.twitter)
      pushSimpleIcon(iconList, urls.facebook, 'bi bi-facebook')
      pushSimpleIcon(iconList, urls.vimeo, 'bi bi-vimeo')
      pushSimpleIcon(iconList, urls.youtube, 'bi bi-youtube')
      pushSimpleIcon(iconList, urls.github, 'bi bi-github')
      pushSimpleIcon(iconList, urls.wikipedia, 'fa-brands fa-wikipedia-w')
    }

    const icons = iconList.length ? (
      <div style={{ fontSize: '2.0em' }}>{iconList}</div>
    ) : undefined

    function requestUpdate(evt) {
      window.location.href =
        '/requestEntityUpdate?' +
        `entity=${encodeURIComponent(entity)}` +
        `&stylizedEntity=${encodeURIComponent(selfAnnotation.stylizedText)}`
    }

    const rightColumn = (
      <div className="col-lg-4">
        {selfAnnotation.logo || iconList.length ? (
          <div>
            <div className="d-md-flex justify-content-md-center">
              {selfImage}
            </div>
            <div className="d-grid gap-2 d-md-flex justify-content-md-center mt-3">
              {icons}
            </div>
          </div>
        ) : undefined}
      </div>
    )

    const selfStylizedText = wrapURL(
      selfAnnotation.stylizedText,
      selfAnnotation.url,
      !selfAnnotation.logo
    )
    const lifespanText = profile.lifespan ? (
      <span className="small">
        ({getYear(profile.lifespan.start)}-{getYear(profile.lifespan.end)})
      </span>
    ) : undefined
    let headline = []
    if (profile.headline) {
      const headlinePieces = util.isString(profile.headline)
        ? [profile.headline]
        : profile.headline
      for (const headlinePiece of headlinePieces) {
        headline.push(
          <p
            key={headline.length}
            className="lead font-monospace lh-sm"
            style={{ fontSize: '1.25rem' }}
            dangerouslySetInnerHTML={{ __html: headlinePiece }}
          ></p>
        )
      }
    } else if (profile.associations && profile.associations.length) {
      const topAssociation = profile.associations[0]
      const text = util.coerce(topAssociation.text, topAssociation.from.text)
      const minAssociationHeadlineLength = 25
      const textArray = util.isString(text) ? [text] : text
      if (textArray[0].length > minAssociationHeadlineLength) {
        for (const textEntry of textArray) {
          headline.push(
            <p
              className="lead font-monospace lh-sm"
              style={{ fontSize: '1.25rem' }}
            >
              {textEntry}
            </p>
          )
        }
      }
    }

    const rowClasses = 'row flex-lg-row-reverse g-5 py-2'

    const leftColumnClasses = headline.length
      ? 'col-lg-8 px-5'
      : 'col-lg-5 d-flex align-items-center px-5'

    const submitComments = (
      <button
        type="button"
        className="btn btn-light"
        style={{ maxWidth: '14em' }}
        onClick={requestUpdate}
      >
        Submit comments
      </button>
    )

    const leftColumn = (
      <div className={leftColumnClasses}>
        <div className="grid">
          <div className="row mb-3">
            <h1 className="display-6 fw-bold font-monospace lh-1">
              {selfStylizedText} {lifespanText}
            </h1>
          </div>
          <div className="row mb-2">
            {headline}
            {submitComments}
            {super.getTagDisplay()}
            {super.getTagForm()}
          </div>
        </div>
      </div>
    )

    return (
      <div className="container col-xxl-12 px-3 py-2">
        <div className={rowClasses}>
          {rightColumn}
          {leftColumn}
        </div>
      </div>
    )
  }
}
