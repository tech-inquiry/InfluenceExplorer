import * as React from 'react'

import { RotatingImage } from './RotatingImage'
import * as ui from './utilities/ui'
import * as util from './utilities/util'

class ParentsRow extends React.Component<any, any> {
  label: string
  items: any[]

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'parents')

    this.items = []
    const info = this.props.info
    const profile = info.profile
    if (profile && profile.hasOwnProperty('parents')) {
      for (const parent of profile.parents) {
        const annotation = profile.entityAnnotations[parent]
        if (annotation.logoSmall) {
          this.items.push({
            image: annotation.logoSmall,
            url: util.getEntityURL(parent),
          })
        }
      }
    }
  }

  render() {
    const info = this.props.info
    const entity = info.query
    const profile = info.profile
    if (!profile || !profile.parents || !profile.parents.length) {
      return undefined
    }

    const selfAnnotation = profile.entityAnnotations[entity]

    const buttonTitle = 'Controlling entities'

    const capImage = (
      <RotatingImage
        items={this.items}
        opacity={1.0}
        development={info.development}
      />
    )

    const footerText = (
      <span>
        Click to view{' '}
        <span className="fw-bold">
          {profile.parents.length + ' direct '}
          {profile.parents.length > 1 ? 'parents' : 'parent'}
        </span>{' '}
        of <span className="fst-italic">{selfAnnotation.stylizedText}</span>
        {selfAnnotation.stylizedText.endsWith('.') ? '' : '.'}
      </span>
    )

    return (
      <ui.EntityRowCard
        label={this.label}
        buttonTitle={buttonTitle}
        capImage={capImage}
        footerText={footerText}
      />
    )
  }
}

class ParentsModal extends React.Component<any, any> {
  label: string

  constructor(props) {
    super(props)
    this.label = util.coerce(this.props.label, 'parents')
  }

  render() {
    const info = this.props.info
    const profile = info.profile
    if (!profile || !profile.parents || !profile.parents.length) {
      return undefined
    }

    const modalTitle = 'Parent Entities'
    const modalBody = (
      <div className="container mx-2 my-1 px-0">
        {profile.parents.map(info.entityToItem)}
      </div>
    )

    return (
      <ui.EntityRowModal
        label={this.label}
        title={modalTitle}
        body={modalBody}
        dialogExtraClasses="modal-lg modal-fullscreen-lg-down"
      />
    )
  }
}

export function insertCardAndModal(info, cards: any[], modals: any[]): void {
  const profile = info.profile
  if (!profile || !profile.parents || !profile.parents.length) {
    return
  }
  cards.push(<ParentsRow info={info} />)
  modals.push(<ParentsModal info={info} />)
}
