import * as React from 'react'

import { appConfig, DEBUG } from './utilities/constants'
import { Accordion, AccordionItem } from './Accordion'
import { DataTable } from './DataTable'
import * as ui from './utilities/ui'
import * as util from './utilities/util'
import { coerce } from './utilities/util'

export class FilingFeed extends React.Component<any, any> {
  title: string
  label: string
  modal: any

  constructor(props) {
    super(props)
    this.state = {
      items: [],
      hitSearchCap: false,
      tableData: undefined,
      tableError: false,
      rowIndex: undefined,
      focusOnTable: true,
      justClicked: undefined,
      modalIsOpen: false,
      lastUpdate: undefined,
    }

    this.title = coerce(this.props.title, 'Filing Feed')

    this.moveToPrevItem = this.moveToPrevItem.bind(this)
    this.moveToNextItem = this.moveToNextItem.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handleKeyDown = this.handleKeyDown.bind(this)
    this.registerClickedRow = this.registerClickedRow.bind(this)

    this.retrieveItems = this.retrieveItems.bind(this)
    this.renderItemHandler = this.renderItemHandler.bind(this)
    this.dataTableHighlightFilter = this.dataTableHighlightFilter.bind(this)

    this.label = coerce(this.props.label, 'filing-feed')
    this.modal = undefined
  }

  moveToPrevItem() {
    if (this.state.rowIndex > 0) {
      const newIndex = this.state.rowIndex - 1
      this.setState({ rowIndex: newIndex })
    }
  }

  moveToNextItem() {
    if (this.state.rowIndex < this.state.items.length - 1) {
      const newIndex = this.state.rowIndex + 1
      this.setState({ rowIndex: newIndex })
    }
  }

  async retrieveItems() {
    try {
      let lastUpdate = undefined
      if (this.props.tableName) {
        const url = `/api/lastUpdate?tableName=${this.props.tableName}`
        const result = await fetch(url as string).then((res) => res.json())
        if (result != undefined && result.hasOwnProperty('last_update')) {
          lastUpdate = result['last_update']
        }
      }

      if (this.props.apiURLs) {
        // Simultaneously retrieve the feeds through their APIs.
        let promises = []
        for (const [source, url] of Object.entries(this.props.apiURLs)) {
          // TypeScript's type system cannot infer the type of url.
          promises.push(fetch(url as string).then((res) => res.json()))
        }
        const results = await Promise.all(promises)

        // Combine together the results.
        const sources = Object.keys(this.props.apiURLs)
        let items = []
        for (const [i, result] of results.entries()) {
          const source = sources[i]
          for (const filing of result.filings) {
            items.push({ source: source, filing: filing })
          }
        }

        const tableData = this.props.itemsToTableData(items)
        this.setState({
          tableError: false,
          items: items,
          tableData: tableData,
          lastUpdate: lastUpdate,
        })
      } else {
        const result = await fetch(this.props.apiURL).then((res) => res.json())
        const items = result.filings
        const tableData = this.props.itemsToTableData(items)
        this.setState({
          tableError: false,
          items: items,
          tableData: tableData,
          lastUpdate: lastUpdate,
        })
      }
    } catch (err) {
      console.log(err)
      this.setState({ tableError: true, items: [], tableData: undefined })
    }
  }

  dataTableHighlightFilter(rowIndex: number): boolean {
    const item = this.state.items[rowIndex]
    return this.props.highlightFilter(item)
  }

  renderItemHandler() {
    const rowIndex = this.state.rowIndex
    if (rowIndex < 0) {
      return undefined
    }
    if (this.props.actOnItem) {
      if (!this.state.justClicked) {
        return undefined
      }
    } else {
      if (this.state.focusOnTable) {
        return undefined
      }
    }

    const item = this.state.items[rowIndex]
    if (!item) {
      console.log(
        `Tried to access row ${rowIndex} of ${this.state.items.length}`
      )
      return undefined
    }

    if (this.props.actOnItem) {
      this.setState({ justClicked: false })
      return this.props.actOnItem(item)
    } else {
      return this.props.renderItem(item)
    }
  }

  async openModal(evt) {
    if (DEBUG) {
      console.log('Called openModal')
    }
    this.setState({ modalIsOpen: true })
    await this.retrieveItems()
  }

  closeModal(evt) {
    if (DEBUG) {
      console.log('Called closeModal')
    }
    this.setState({
      modalIsOpen: false,
      rowIndex: undefined,
      focusOnTable: true,
    })
  }

  handleKeyDown(evt) {
    if (evt.key == 'ArrowLeft') {
      // Move to the previous filing if the left arrow is pressed.
      this.moveToPrevItem()
    } else if (evt.key == 'ArrowRight') {
      // Move to the next filing if the right arrow is pressed.
      this.moveToNextItem()
    }
  }

  componentDidMount() {
    if (!this.modal) {
      const myModalId = ui.modalId(this.label)
      let modalElement = document.getElementById(myModalId)
      this.modal = modalElement
      if (this.modal) {
        modalElement.addEventListener('show.bs.modal', this.openModal)
        modalElement.addEventListener('hidden.bs.modal', this.closeModal)
        modalElement.addEventListener('keydown', this.handleKeyDown)
      }
    }
  }

  // DEBUG
  //componentWillUnmount() {
  //  if (this.modal) {
  //    this.modal.removeEventListener('show.bs.modal', this.openModal)
  //    this.modal.removeEventListener('hidden.bs.modal', this.closeModal)
  //    this.modal.removeEventListener('keydown', this.handleKeyDown)
  //  }
  //}

  registerClickedRow(rowIndex) {
    if (this.state.modalIsOpen && this.state.focusOnTable && rowIndex >= 0) {
      // The first column of the table has to point to the original item
      // index.
      const origRowIndex = this.state.tableData[rowIndex][0]
      if (this.props.actOnItem) {
        // We won't be rendering the item.
        this.setState({ justClicked: true, rowIndex: origRowIndex })
      } else {
        this.setState({ rowIndex: origRowIndex, focusOnTable: false })
      }
    }
  }

  render() {
    let that = this

    let dataTable = this.state.modalIsOpen ? (
      <DataTable
        tableID={this.props.tableID}
        tableData={this.state.tableData}
        tableError={this.state.tableError}
        columns={this.props.columns}
        registerClickedRow={this.registerClickedRow}
        highlightFilter={
          this.props.highlightFilter ? this.dataTableHighlightFilter : undefined
        }
        style={{
          display: this.state.focusOnTable ? 'inline' : 'none',
        }}
      />
    ) : undefined

    const prevButton = (
      <button
        type="button"
        className="btn btn-secondary mx-2"
        style={{
          fontSize: '1.75rem',
          display: this.state.focusOnTable ? 'none' : 'inline',
        }}
        onClick={this.moveToPrevItem}
      >
        Previous
      </button>
    )

    const nextButton = (
      <button
        type="button"
        className="btn btn-secondary mx-2"
        style={{
          fontSize: '1.75rem',
          display: this.state.focusOnTable ? 'none' : 'inline',
        }}
        onClick={this.moveToNextItem}
      >
        Next
      </button>
    )

    const backButton = (
      <button
        type="button"
        className="btn btn-secondary mx-2"
        style={{
          fontSize: '1.75rem',
          display: this.state.focusOnTable ? 'none' : 'inline',
        }}
        onClick={function (evt) {
          that.setState({ focusOnTable: true })
        }}
      >
        Back to table
      </button>
    )

    const note = this.state.focusOnTable ? this.props.note : undefined

    const dateOptions = {
      timeZone: 'UTC',
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    } as const
    const dateFormatter = new Intl.DateTimeFormat('en-US', dateOptions)
    const lastUpdate = this.state.lastUpdate ? (
      <h5>
        Last update: {dateFormatter.format(new Date(this.state.lastUpdate))}
      </h5>
    ) : undefined

    return (
      <div className="modal" id={ui.modalId(this.label)} tabIndex={-1}>
        <div className="modal-dialog modal-fullscreen">
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">{this.title}</h4>
              <ui.CloseButton label={this.label} />
            </div>
            <div className="modal-body mx-3 my-0">
              {this.state.modalIsOpen
                ? [lastUpdate, note, dataTable, this.renderItemHandler()]
                : undefined}
            </div>
            <div className="modal-footer">
              {this.state.rowIndex > 0 ? prevButton : undefined}
              {this.state.rowIndex < this.state.items.length - 1
                ? nextButton
                : undefined}
              {this.state.modalIsOpen ? backButton : undefined}
              <ui.SecondaryCloseButton />
            </div>
          </div>
        </div>
      </div>
    )
  }
}
