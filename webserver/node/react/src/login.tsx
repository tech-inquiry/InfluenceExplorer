import * as React from 'react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import Login from './apps/Login'

import '../css/signin.css'

const root = createRoot(document.getElementById('login-app'))
root.render(
  <BrowserRouter>
    <Login />
  </BrowserRouter>
)
