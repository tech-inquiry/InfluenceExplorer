import * as api from './utilities/api'
import { appConfig } from './utilities/constants'
import {
  encodeURLComponent,
  getRetrieveURL,
  retrieveToken,
} from './utilities/util'

function buildAPIURL(
  endpoint: string,
  query: string,
  asAgency: boolean,
  isEntity: boolean,
  isTag: boolean
) {
  const urlBase = `${appConfig.apiURL}/${endpoint}`
  return getRetrieveURL(urlBase, query, asAgency, isEntity, isTag)
}

export async function activeSources(
  query: string,
  isEntity: boolean,
  isTag: boolean,
  batchConfig: Map<string, boolean>,
  blockConfig: Map<string, boolean>,
  setLoadingMessage
) {
  let peeks = []
  let promises = []

  async function testForRecords(keys, paths, configKey, disabled = false) {
    if (disabled) {
      for (const key of keys) {
        peeks.push(false)
      }
      return
    }
    for (const key of keys) {
      const config = paths[key]
      const asAgency = config['asAgency']
      const url = buildAPIURL(config.have, query, asAgency, isEntity, isTag)
      let promise = fetch(url).then((res) => res.json())
      if (batchConfig.get('all') || batchConfig.get(configKey)) {
        promises.push(promise)
      } else {
        setLoadingMessage(
          `Testing if records exist for ${configKey} feed ${key}...`
        )
        peeks.push(await promise)
      }
    }
    if (batchConfig.get(configKey)) {
      setLoadingMessage(`Testing if records exist in each ${configKey} feed...`)
      peeks = peeks.concat(await Promise.all(promises))
      promises = []
    }
  }

  const blockFeeds = blockConfig.has('blockFeeds')
  const isPerson = blockConfig.has('isPerson')
  const allowLeaks = blockConfig.has('allowLeaks')

  const procurementKeys = isEntity
    ? api.kEntityProcurementKeys
    : isTag
    ? api.kTagSearchProcurementKeys
    : api.kSearchProcurementKeys
  const lobbyingKeys = isTag
    ? api.kTagSearchLobbyingKeys
    : Object.keys(api.kLobbyingPaths)
  const leakKeys = isTag ? api.kTagSearchLeakKeys : Object.keys(api.kLeakPaths)
  const securitiesKeys = isEntity
    ? api.kEntitySecuritiesKeys
    : isTag
    ? []
    : api.kSearchSecuritiesKeys
  const foreignAgentKeys = isEntity
    ? api.kEntityForeignAgentKeys
    : isTag
    ? []
    : api.kSearchForeignAgentKeys
  const laborRelationsKeys = isTag ? [] : Object.keys(api.kLaborRelationsPaths)

  await testForRecords(
    procurementKeys,
    api.kProcurementPaths,
    'procurement',
    blockFeeds || isPerson
  )
  await testForRecords(lobbyingKeys, api.kLobbyingPaths, 'lobbying', blockFeeds)
  await testForRecords(
    leakKeys,
    api.kLeakPaths,
    'leaks',
    blockFeeds && !allowLeaks
  )
  await testForRecords(
    securitiesKeys,
    api.kSecuritiesPaths,
    'securities',
    blockFeeds || isPerson
  )
  await testForRecords(
    foreignAgentKeys,
    api.kForeignAgentPaths,
    'foreignAgents',
    blockFeeds
  )
  await testForRecords(
    laborRelationsKeys,
    api.kLaborRelationsPaths,
    'laborRelations',
    blockFeeds || isPerson
  )

  if (promises.length) {
    setLoadingMessage('Testing if records exist in each feed...')
    peeks = await Promise.all(promises)
  }
  setLoadingMessage('Retrieved list of non-empty feeds...')

  let sourceOffset = 0

  function testPeeks(sourceList, keys) {
    for (const key of keys) {
      if (peeks[sourceOffset]) {
        sourceList.push(key)
      }
      sourceOffset += 1
    }
  }

  let sources = {
    procurement: [],
    lobbying: [],
    leaks: [],
    securities: [],
    foreignAgents: [],
    laborRelations: [],
  }
  if (!isEntity && !isTag) {
    sources['webtext'] = true
  }
  if (!isEntity) {
    sources['entity'] = true
  }

  // Combine the US federal procurement sources.
  let haveUSFederalAgency = false
  let haveUSFederalVendor = false
  let haveUSFederalGrantAgency = false
  let haveUSFederalGrantVendor = false
  for (const key of procurementKeys) {
    if (peeks[sourceOffset]) {
      if (api.kUSFederalAgencyProcurementSources.includes(key)) {
        if (!haveUSFederalAgency) {
          sources.procurement.push(api.kUSFederalAgencyProcurementSource)
          haveUSFederalAgency = true
        }
      } else if (api.kUSFederalVendorProcurementSources.includes(key)) {
        if (!haveUSFederalVendor) {
          sources.procurement.push(api.kUSFederalVendorProcurementSource)
          haveUSFederalVendor = true
        }
      } else if (api.kUSFederalAgencyGrantSources.includes(key)) {
        if (!haveUSFederalGrantAgency) {
          sources.procurement.push(api.kUSFederalAgencyGrantSource)
          haveUSFederalGrantAgency = true
        }
      } else if (api.kUSFederalVendorGrantSources.includes(key)) {
        if (!haveUSFederalGrantVendor) {
          sources.procurement.push(api.kUSFederalVendorGrantSource)
          haveUSFederalGrantVendor = true
        }
      } else {
        sources.procurement.push(key)
      }
    }
    sourceOffset += 1
  }

  testPeeks(sources.lobbying, lobbyingKeys)
  testPeeks(sources.leaks, leakKeys)
  testPeeks(sources.securities, securitiesKeys)
  testPeeks(sources.foreignAgents, foreignAgentKeys)
  testPeeks(sources.laborRelations, laborRelationsKeys)

  setLoadingMessage('Displaying active feeds...')

  return sources
}

export async function activeLeakSources(
  query: string,
  isEntity: boolean,
  isTag: boolean,
  batchConfig: Map<string, boolean>,
  blockConfig: Map<string, boolean>,
  setLoadingMessage
) {
  let peeks = []
  let promises = []

  async function testForRecords(keys, paths, configKey, disabled = false) {
    if (disabled) {
      for (const key of keys) {
        peeks.push(false)
      }
      return
    }
    for (const key of keys) {
      const config = paths[key]
      const asAgency = config['asAgency']
      const url = buildAPIURL(config.have, query, asAgency, isEntity, isTag)
      let promise = fetch(url).then((res) => res.json())
      if (batchConfig.get('all') || batchConfig.get(configKey)) {
        promises.push(promise)
      } else {
        setLoadingMessage(
          `Testing if records exist for ${configKey} feed ${key}...`
        )
        peeks.push(await promise)
      }
    }
    if (batchConfig.get(configKey)) {
      setLoadingMessage(`Testing if records exist in each ${configKey} feed...`)
      peeks = peeks.concat(await Promise.all(promises))
      promises = []
    }
  }

  const blockFeeds = blockConfig.has('blockFeeds')
  const isPerson = blockConfig.has('isPerson')

  await testForRecords(
    Object.keys(api.kLeakPaths),
    api.kLeakPaths,
    'leaks',
    blockFeeds
  )

  if (promises.length) {
    setLoadingMessage('Testing if records exist in each feed...')
    peeks = await Promise.all(promises)
  }
  setLoadingMessage('Retrieved list of non-empty feeds...')

  let sourceOffset = 0

  function testPeeks(sourceList, keys) {
    for (const key of keys) {
      if (peeks[sourceOffset]) {
        sourceList.push(key)
      }
      sourceOffset += 1
    }
  }

  let sources = {
    leaks: [],
  }

  testPeeks(sources.leaks, Object.keys(api.kLeakPaths))

  setLoadingMessage('Displaying active feeds...')

  return sources
}
