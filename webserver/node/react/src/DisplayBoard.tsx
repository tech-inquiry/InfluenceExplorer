import * as React from 'react'

export class DisplayBoard extends React.Component<any, any> {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        <div className="card text-center mb-3">
          <div className="card-body">
            <h3 className="card-title">Board of Directors</h3>
          </div>
        </div>
        <div className="card" style={{ flexDirection: 'row' }}>
          <a href="https://libraryfreedom.org/team/alison-macrina/">
            <img
              src="https://techinquiry.org/logos/alison_macrina.jpg"
              className="card-img-top ti-governance-picture"
              alt="Alison Macrina"
            />
          </a>
          <div className="card-body">
            <h4 className="card-title">
              Alison Macrina (Director){' '}
              <a href="https://twitter.com/flexlibris" className="mx-1">
                <i className="bi bi-twitter"></i>
              </a>
            </h4>
            <p className="card-text">
              Alison is an activist librarian and the director of Library
              Freedom Project. Alison started LFP in 2015 to organize and build
              community with other librarians who are dedicated to library
              values of privacy, intellectual freedom, social responsibility,
              and the public good.
            </p>
          </div>
        </div>
        <div className="card" style={{ flexDirection: 'row' }}>
          <a href="https://collectiveaction.tech/author/js/">
            <img
              src="https://techinquiry.org/logos/js_tan.jpg"
              className="card-img-top ti-governance-picture"
              alt="JS Tan"
            />
          </a>
          <div className="card-body">
            <h4 className="card-title">
              JS Tan (Director){' '}
              <a href="https://twitter.com/organizejs" className="mx-1">
                <i className="bi bi-twitter"></i>
              </a>
            </h4>
            <p className="card-text">
              JS is a former tech worker and writes about tech, labor & China as
              a maintainer of{' '}
              <a href="https://collectiveaction.tech/author/js/">
                Collective Actions in Tech
              </a>
              .
            </p>
          </div>
        </div>
        <div className="card" style={{ flexDirection: 'row' }}>
          <a href="http://shaunagm.net/">
            <img
              src="https://techinquiry.org/images/ShaunaGordonMcKeon-75pct.jpg"
              className="card-img-top ti-governance-picture"
              alt="Shauna Gordon-McKeon"
            />
          </a>
          <div className="card-body">
            <h4 className="card-title">
              Shauna Gordon-McKeon, she/her{' '}
              <a href="https://twitter.com/shauna_gm" className="mx-1">
                <i className="bi bi-twitter"></i>
              </a>
            </h4>
            <p className="card-text">
              Shauna Gordon-McKeon is a freelance technology consultant with a
              background in open source project management and Python
              programming. Her current focus is on improving both the governance
              of technology and governance technologies. She was Tech Inquiry's
              original President of the Board and largely designed the
              organization's governance structure.
            </p>
          </div>
        </div>
        <div className="card" style={{ flexDirection: 'row' }}>
          <a href="https://www.icarussalon.com/">
            <img
              src="https://techinquiry.org/logos/SherryWong.jpg"
              className="card-img-top ti-governance-picture"
              alt="Şerife (Sherry) Wong"
            />
          </a>
          <div className="card-body">
            <h4 className="card-title">
              Şerife (Sherry) Wong, she/her (Director){' '}
              <a href="https://twitter.com/sherrying" className="mx-1">
                <i className="bi bi-twitter"></i>
              </a>
            </h4>
            <p className="card-text">
              Şerife is a Turkish-Hawaiian artist working on AI governance. She
              is currently an affiliate at O'Neil Risk Consulting and
              Algorithmic Auditing and an affiliate research scientist at Kidd
              Lab, UC Berkeley, serves on the board of directors for Gray Area
              and Tech Inquiry, and is the culture and AI governance lead at the
              Tech Diplomacy Network.
            </p>
          </div>
        </div>
        <div className="card" style={{ flexDirection: 'row' }}>
          <a href="https://www.humanityinaction.org/person/yonatan-miller/">
            <img
              src="https://techinquiry.org/logos/yonatan_miller.jpg"
              className="card-img-top ti-governance-picture"
              alt="Yonatan Miller"
            />
          </a>
          <div className="card-body">
            <h4 className="card-title">
              Yonatan Miller (Director){' '}
              <a href="https://twitter.com/shushugah" className="mx-1">
                <i className="bi bi-twitter"></i>
              </a>
            </h4>
            <p className="card-text">
              Yonatan Miller is a tech worker and trade union organizer from New
              York who considers Berlin home. He recently graduated from a
              Master’s Programme in Labour Policies and Globalisation, where he
              furthered his knowledge of the organizing challenges of global
              solidarity in the tech sector. He co-founded the Berlin Tech
              Workers Coalition, and he is an activist within the Berlin Vs
              Amazon alliance. He does not believe there are shortcuts to
              organizing. When he is not busy fighting for justice, he can be
              found tweeting raccoon memes and sipping coffee on his twitter
              handle <a href="https://twitter.com/shushugah">@shushugah</a>.
              Tech Won’t Save Us, Organize!
            </p>
          </div>
        </div>
        <div className="card" style={{ flexDirection: 'row' }}>
          <img
            src="https://techinquiry.org/images/JackPoulson-70pct.jpg"
            className="card-img-top ti-governance-picture"
            alt="Jack Poulson"
          />
          <div className="card-body">
            <h4 className="card-title">
              Jack Poulson, he/him (Executive Director){' '}
              <a href="mailto:jack@techinquiry.org">jack@techinquiry.org</a>
            </h4>
            <p className="card-text">
              Jack Poulson is the Executive Director of Tech Inquiry. He spends
              the bulk of his time investigating relationships between
              corporations, governments, and NGOs (with a focus on the
              surveillance and weapons industries). He founded Tech Inquiry in
              2019. If you are interested in sending him a secure tip, consider
              messaging him from a purely personal device on Signal at
              @poulson.01 with disappearing messages set to at most one week.
            </p>
          </div>
        </div>

        <div className="card text-center mb-3">
          <div className="card-body">
            <h3 className="card-title">Board of Directors - In Memoriam</h3>
          </div>
        </div>
        <div className="card" style={{ flexDirection: 'row' }}>
          <a href="http://www.uncomputing.org/">
            <img
              src="https://techinquiry.org/logos/david_golumbia.jpg"
              className="card-img-top ti-governance-picture"
              alt="David Golumbia"
            />
          </a>
          <div className="card-body">
            <h4 className="card-title">
              David Golumbia (Former Director){' '}
              <a href="https://twitter.com/dgolumbia" className="mx-1">
                <i className="bi bi-twitter"></i>
              </a>
            </h4>
            <p className="card-text">
              David Golumbia taught in the English department and the Media,
              Art, and Text PhD program at Virginia Commonwealth University. In
              2016, he published the book{' '}
              <a href="https://www.upress.umn.edu/book-division/books/the-politics-of-bitcoin">
                The Politics of Bitcoin: Software as Right-Wing Extremism
              </a>
              .
            </p>
          </div>
        </div>
      </div>
    )
  }
}
