import * as React from 'react'

import { Icon, LatLngExpression } from 'leaflet'
import {
  MapContainer,
  LayersControl,
  LayerGroup,
  TileLayer,
  useMap,
  Marker,
  Popup,
} from 'react-leaflet'
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch'

import { createMasonry, createMasonryBooks } from './Masonry'
import * as util from './utilities/util'

import * as kAirForceBases from '../data/airForceBases'
import * as kArmyBases from '../data/armyBases'
import * as kBorderPatrolSectors from '../data/borderPatrolSectors'
import * as kBorderPatrolStations from '../data/borderPatrolStations'
import * as kDEAOffices from '../data/deaOffices'
import * as kEmbassies from '../data/embassies'
import * as kEROFieldOffices from '../data/eroFieldOffices'
import * as kFBIFieldOffices from '../data/fbiFieldOffices'
import * as kHSIFieldOffices from '../data/hsiFieldOffices'
import * as kMarineBases from '../data/marineBases'
import * as kNavalBases from '../data/navalBases'
import * as kSpaceForceBases from '../data/spaceForceBases'

import * as kAndurilASTs from '../data/andurilASTs'
import * as kElbitIFTs from '../data/elbitIFTs'
import * as kGDITRVSSs from '../data/gditRVSSs'

import * as kHomeNotices from '../data/homeNotices'

const kLogoPrefix = 'https://techinquiry.org'

const kEPICOffice = {
  EPIC: {
    latLong: [31.830139420237373, -106.37450514665363],
    entity: 'el paso intelligence center',
    title: 'El Paso Intelligence Center',
    addressLines: [
      '11339 SSG Sims Street',
      'El Paso, Texas  79918',
      'Phone: (915) 760-2000',
    ],
  },
}

function iconFromLogo(url, size = 30, aspectRatio = 1) {
  return new Icon({
    iconUrl: url,
    iconRetinaUrl: url,
    iconAnchor: null,
    popupAnchor: [-0, -0],
    shadowUrl: null,
    shadowSize: null,
    shadowAnchor: null,
    iconSize: [size, size * aspectRatio],
  })
}

const stationLogos = {
  'Big Bend': '/logos/big_bend_border_patrol_sector.png',
  Blaine: '/logos/blaine_border_patrol_sector.png',
  Buffalo: '/logos/buffalo_border_patrol_sector.png',
  'Del Rio': '/logos/del_rio_border_patrol_sector.png',
  Detroit: '/logos/detroit_border_patrol_sector.png',
  'El Centro': '/logos/el_centro_border_patrol_sector.png',
  'El Paso': '/logos/el_paso_border_patrol_sector.png',
  'Grand Forks': '/logos/grand_forks_border_patrol_sector.png',
  Havre: '/logos/havre_border_patrol_sector.png',
  Houlton: '/logos/houlton_border_patrol_sector.png',
  Laredo: '/logos/laredo_border_patrol_sector.png',
  Miami: '/logos/miami_border_patrol_sector.png',
  'New Orleans': '/logos/new_orleans_border_patrol_sector.png',
  'Rio Grande Valley': '/logos/rio_grande_valley_border_sector.png',
  'San Diego': '/logos/san_diego_border_patrol_sector.png',
  Spokane: '/logos/spokane_border_patrol_sector.png',
  Swanton: '/logos/swanton_border_patrol_sector.png',
  Tucson: '/logos/tucson_border_patrol_sector.png',
  Yuma: '/logos/yuma_border_patrol_sector.png',
}

function borderPatrolStationToMarker(station, icon) {
  return (
    <Marker
      key={station['title']}
      position={station['latLong'] as LatLngExpression}
      icon={icon}
    >
      <Popup>
        <a href={util.getEntityURL(station.entity)}>{station['title']}</a>
        {station['addressLines'].map(function (line) {
          return (
            <>
              <br />
              {line}
            </>
          )
        })}
      </Popup>
    </Marker>
  )
}

const astIcon = iconFromLogo(
  kLogoPrefix + '/logos/anduril_standard_range_sentry_tower_top.webp'
)
function astToMarker(tower) {
  return (
    <Marker
      key={tower['title']}
      position={tower['latLong'] as LatLngExpression}
      icon={astIcon}
    >
      <Popup>
        <a href={util.getEntityURL('anduril standard range sentry tower')}>
          {tower['title']}
        </a>
        {tower['addressLines'].map(function (line) {
          return (
            <>
              <br />
              {line}
            </>
          )
        })}
      </Popup>
    </Marker>
  )
}

const rvssIcon = iconFromLogo(kLogoPrefix + '/logos/gdit_rvss_icon.png')
function rvssToMarker(tower) {
  return (
    <Marker
      key={tower['title']}
      position={tower['latLong'] as LatLngExpression}
      icon={rvssIcon}
    >
      <Popup>
        <a
          href={util.getEntityURL(
            'general dynamics remote video surveillance system'
          )}
        >
          {tower['title']}
        </a>
        {tower['addressLines'].map(function (line) {
          return (
            <>
              <br />
              {line}
            </>
          )
        })}
      </Popup>
    </Marker>
  )
}

const iftIcon = iconFromLogo(
  kLogoPrefix + '/logos/elbit_integrated_fixed_tower_icon.png'
)

function iftToMarker(tower) {
  return (
    <Marker
      key={tower['title']}
      position={tower['latLong'] as LatLngExpression}
      icon={iftIcon}
    >
      <Popup>
        <a href={util.getEntityURL('elbit integrated fixed tower')}>
          {tower['title']}
        </a>
        {tower['addressLines'].map(function (line) {
          return (
            <>
              <br />
              {line}
            </>
          )
        })}
      </Popup>
    </Marker>
  )
}

function officeToMarker(office, icon) {
  return (
    <Marker
      key={office['title']}
      position={office['latLong'] as LatLngExpression}
      icon={icon}
    >
      <Popup>
        <a href={util.getEntityURL(office['entity'])}>{office['title']}</a>
        {office['controller'] ? (
          <>
            <br />
            <em>{office['controller']}</em>
          </>
        ) : undefined}
        {office['addressLines'].map(function (line) {
          return (
            <>
              <br />
              {line}
            </>
          )
        })}
      </Popup>
    </Marker>
  )
}

function getStationMarkers(stationName) {
  const icon = iconFromLogo(kLogoPrefix + stationLogos[stationName], 25)
  function stationToMarker(station) {
    return borderPatrolStationToMarker(station, icon)
  }
  return Object.values(kBorderPatrolStations.data[stationName]).map(
    stationToMarker
  )
}

let stationMarkers = {}
for (const stationName in kBorderPatrolStations.data) {
  stationMarkers[stationName] = getStationMarkers(stationName)
}

const initialZoomLevel = 2

function getMarkers(offices, logo, size = 30, aspectRatio = 1) {
  const icon = iconFromLogo(kLogoPrefix + logo, size, aspectRatio)
  function customOfficeToMarker(office) {
    return officeToMarker(office, icon)
  }
  return Object.values(offices).map(customOfficeToMarker)
}

const MapContent = () => {
  const andurilASTMarkers = Object.values(kAndurilASTs.data).map(astToMarker)
  const andurilASTLayer = (
    <LayersControl.Overlay name="Anduril Border Surveillance Towers">
      <LayerGroup>{andurilASTMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const gditRVSSMarkers = Object.values(kGDITRVSSs.data).map(rvssToMarker)
  const gditRVSSLayer = (
    <LayersControl.Overlay name="GDIT Border Surveillance Towers">
      <LayerGroup>{gditRVSSMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const elbitIFTMarkers = Object.values(kElbitIFTs.data).map(iftToMarker)
  const elbitIFTLayer = (
    <LayersControl.Overlay name="Elbit Border Surveillance Towers">
      <LayerGroup>{elbitIFTMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const borderPatrolSectorMarkers = getMarkers(
    kBorderPatrolSectors.data,
    '/logos/united_states_border_patrol_icon.png'
  )
  const borderPatrolSectorsLayer = (
    <LayersControl.Overlay checked name="Border Patrol Sectors">
      <LayerGroup>{borderPatrolSectorMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const borderPatrolStationsLayer = (
    <LayersControl.Overlay name="Border Patrol Stations">
      <LayerGroup>{Object.values(stationMarkers)}</LayerGroup>
    </LayersControl.Overlay>
  )

  const armyBaseMarkers = getMarkers(
    kArmyBases.data,
    '/logos/united_states_department_of_the_army_icon.png'
  )
  const armyBaseLayer = (
    <LayersControl.Overlay checked name="Army Bases (in development)">
      <LayerGroup>{armyBaseMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const airForceBaseMarkers = getMarkers(
    kAirForceBases.data,
    '/logos/united_states_department_of_the_air_force_icon.png'
  )
  const airForceBaseLayer = (
    <LayersControl.Overlay checked name="Air Force Bases">
      <LayerGroup>{airForceBaseMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const spaceForceBaseMarkers = getMarkers(
    kSpaceForceBases.data,
    '/logos/united_states_space_force_icon.png'
  )
  const spaceForceBaseLayer = (
    <LayersControl.Overlay checked name="Space Force Bases">
      <LayerGroup>{spaceForceBaseMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const marineBaseMarkers = getMarkers(
    kMarineBases.data,
    '/logos/united_states_marine_corps_icon.webp'
  )
  const marineBaseLayer = (
    <LayersControl.Overlay checked name="Marine Corps Bases">
      <LayerGroup>{marineBaseMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const navalBaseMarkers = getMarkers(
    kNavalBases.data,
    '/logos/united_states_department_of_the_navy_icon.png'
  )
  const navalBaseLayer = (
    <LayersControl.Overlay checked name="Naval Bases">
      <LayerGroup>{navalBaseMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const embassyMarkers = getMarkers(
    kEmbassies.data,
    '/logos/united_states_embassy_icon.png'
  )
  const embassyLayer = (
    <LayersControl.Overlay checked name="Embassies">
      <LayerGroup>{embassyMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const fbiFieldOfficeMarkers = getMarkers(
    kFBIFieldOffices.data,
    '/logos/federal_bureau_of_investigation_icon.png'
  )
  const fbiFieldOfficesLayer = (
    <LayersControl.Overlay checked name="FBI Field Offices">
      <LayerGroup>{fbiFieldOfficeMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const iceEROFieldOfficeMarkers = getMarkers(
    kEROFieldOffices.data,
    '/logos/ice_enforcement_and_removal_operations_icon.png',
    24,
    1.425
  )
  const iceEROFieldOfficesLayer = (
    <LayersControl.Overlay checked name="ICE ERO Field Offices">
      <LayerGroup>{iceEROFieldOfficeMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const iceHSIFieldOfficeMarkers = getMarkers(
    kHSIFieldOffices.data,
    '/logos/ice_homeland_security_investigations_icon.png'
  )
  const iceHSIFieldOfficesLayer = (
    <LayersControl.Overlay checked name="ICE HSI Field Offices">
      <LayerGroup>{iceHSIFieldOfficeMarkers}</LayerGroup>
    </LayersControl.Overlay>
  )

  const deaOfficeMarkers = getMarkers(
    kDEAOffices.data,
    '/logos/drug_enforcement_administration_icon.png'
  )
  const epicOfficeMarker = getMarkers(
    kEPICOffice,
    '/logos/el_paso_intelligence_center.png'
  )
  const deaOfficesLayer = (
    <LayersControl.Overlay checked name="DEA Offices">
      <LayerGroup>
        {deaOfficeMarkers}
        {epicOfficeMarker}
      </LayerGroup>
    </LayersControl.Overlay>
  )

  return (
    <LayersControl position="topright">
      {embassyLayer}
      {navalBaseLayer}
      {marineBaseLayer}
      {airForceBaseLayer}
      {armyBaseLayer}
      {spaceForceBaseLayer}
      {andurilASTLayer}
      {gditRVSSLayer}
      {elbitIFTLayer}
      {borderPatrolStationsLayer}
      {borderPatrolSectorsLayer}
      {fbiFieldOfficesLayer}
      {iceEROFieldOfficesLayer}
      {iceHSIFieldOfficesLayer}
      {deaOfficesLayer}
    </LayersControl>
  )
}

const SearchField = () => {
  const provider = new OpenStreetMapProvider({})

  // @ts-ignore
  const searchControl = new GeoSearchControl({
    provider: provider,
  })

  let map = useMap()
  React.useEffect(() => {
    map.addControl(searchControl)
    return () => {
      map.removeControl(searchControl)
    }
  }, [])

  return null
}

function generateInstallationMap() {
  const mapCenter = [8.427397695562613, 11.894309201280857]
  return (
    <MapContainer center={mapCenter} zoom={initialZoomLevel} minZoom={2}>
      <TileLayer
        attribution=""
        url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
      />
      <MapContent />
      <SearchField />
    </MapContainer>
  )
}

const breakpointColumnsObj = {
  default: 4,
  1500: 3,
  1100: 2,
  900: 1,
}

const masonryEntities = {
  'Defense Tech': [
    { name: 'shield ai, inc.', logo: '/logos/shield_ai_inc.png' },
    { name: 'hawkeye 360, inc.', logo: '/logos/hawkeye_360_inc.png' },
    {
      name: 'anduril industries, inc.',
      logo: '/logos/anduril_industries_inc.png',
    },
    {
      name: 'rebellion defense, inc.',
      logo: '/logos/rebellion_defense_inc.png',
    },
    {
      name: 'palantir technologies, inc.',
      logo: '/logos/palantir_technologies_inc.png',
    },
    { name: 'epirus, inc.', logo: '/logos/epirus_inc.png' },
    { name: 'scale ai, inc.', logo: '/logos/scale_ai_inc.png' },
    {
      name: 'applied intuition, inc.',
      logo: '/logos/applied_intuition_inc.svg',
    },
    { name: 'saildrone, inc.', logo: '/logos/saildrone_inc.svg' },
    { name: 'chaos industries, inc.', logo: '/logos/chaos_industries_inc.png' },
    { name: 'helsing gmbh', logo: '/logos/helsing_gmbh.png' },
  ],
  'Big Tech': [
    { name: 'apple inc.', logo: '/logos/apple_inc.svg' },
    {
      name: 'microsoft corporation',
      logo: '/logos/microsoft_corporation.svg',
    },
    { name: 'google, llc', logo: '/logos/google_llc.svg' },
    { name: 'amazon.com, inc.', logo: '/logos/amazon_com_inc.svg' },
    {
      name: 'meta platforms, inc.',
      logo: '/logos/meta_platforms_inc.svg',
    },
    { name: 'oracle corporation', logo: '/logos/oracle_corporation.png' },
  ],
  'Offensive Cyber and Wiretapping': [
    {
      name: 'nso group technologies ltd.',
      logo: '/logos/nso_group_technologies_ltd.png',
    },
    { name: 'quadream ltd', logo: '/logos/quadream_ltd.png' },
    { name: 'boldend, inc.', logo: '/logos/boldend_inc.png' },
    {
      name: 'blackhorse solutions, inc.',
      logo: '/logos/blackhorse_solutions_inc.png',
    },
    { name: 'rayzone group ltd', logo: '/logos/rayzone_group_ltd.png' },
    { name: 'saito tech ltd', logo: '/logos/saito_tech_ltd.webp' },
    { name: 'pen-link, ltd.', logo: '/logos/penlink_ltd.png' },
    { name: 'trovicor fz llc', logo: '/logos/trovicor_fz_llc.png' },
    {
      name: 'cognyte software ltd.',
      logo: '/logos/cognyte_software_ltd.svg',
    },
  ],
  'Weapons Manufacturers': [
    {
      name: 'lockheed martin corporation',
      logo: '/logos/lockheed_martin_corporation.svg',
    },
    {
      name: 'raytheon technologies corporation',
      logo: '/logos/rtx_corporation.svg',
    },
    {
      name: 'northrop grumman corporation',
      logo: '/logos/northrop_grumman_corporation.png',
    },
    { name: 'the boeing company', logo: '/logos/the_boeing_company.svg' },
    { name: 'elbit systems ltd', logo: '/logos/elbit_systems_ltd.svg' },
    {
      name: 'rafael advanced defense systems ltd',
      logo: '/logos/rafael_advanced_defense_systems_ltd.svg',
    },
    {
      name: 'israel aerospace industries ltd',
      logo: '/logos/israel_aerospace_industries_ltd.svg',
    },
    { name: 'rheinmetall ag', logo: '/logos/rheinmetall_ag.svg' },
    { name: 'saab ab', logo: '/logos/saab_ab.svg' },
  ],
  'U.S. Intelligence Agencies': [
    {
      name: 'national reconnaissance office',
      logo: '/logos/national_reconnaissance_office.svg',
    },
    {
      name: 'defense intelligence agency',
      logo: '/logos/defense_intelligence_agency.svg',
    },
    {
      name: 'central intelligence agency',
      logo: '/logos/central_intelligence_agency.svg',
    },
    {
      name: 'united states national geospatial-intelligence agency',
      logo: '/logos/united_states_national_geospatial-intelligence_agency.svg',
    },
    {
      name: 'national security agency',
      logo: '/logos/national_security_agency.svg',
    },
    {
      name: 'federal bureau of investigation',
      logo: '/logos/federal_bureau_of_investigation.svg',
    },
    {
      name: 'dea office of national security intelligence',
      logo: '/logos/drug_enforcement_administration.svg',
    },
    {
      name: 'united states treasury office of intelligence and analysis',
      logo: '/logos/united_states_treasury_office_of_intelligence_and_analysis.png',
    },
    {
      name: 'united states army intelligence and security command',
      logo: '/logos/united_states_army_intelligence_and_security_command.png',
    },
    {
      name: 'office of naval intelligence',
      logo: '/logos/office_of_naval_intelligence.png',
    },
  ],
  'U.S. Special Operations': [
    {
      name: 'joint special operations command',
      logo: '/logos/joint_special_operations_command.svg',
    },
    {
      name: '1st special forces operational detachment\u2013delta',
      logo: '/logos/1st_special_forces_operational_detachment-delta.svg',
    },
    {
      name: 'naval special warfare development group',
      logo: '/logos/naval_special_warfare_development_group.svg',
    },
    {
      name: 'special activities center',
      logo: '/logos/special_activities_center.webp',
    },
    {
      name: 'united states army intelligence support activity',
      logo: '/logos/united_states_army_intelligence_support_activity.png',
    },
    {
      name: '160th special operations aviation regiment (airborne)',
      logo: '/logos/160th_special_operations_aviation_regiment_airborne.svg',
    },
    {
      name: '24th special tactics squadron',
      logo: '/logos/24th_special_tactics_squadron.png',
    },
    {
      name: '1st special forces group (airborne)',
      logo: '/logos/1st_special_forces_group_airborne.svg',
    },
    {
      name: '3rd special forces group (airborne)',
      logo: '/logos/3rd_special_forces_group_airborne.svg',
    },
    {
      name: '5th special forces group (airborne)',
      logo: '/logos/5th_special_forces_group_airborne.png',
    },
    {
      name: '7th special forces group (airborne)',
      logo: '/logos/7th_special_forces_group_airborne.png',
    },
    {
      name: '10th special forces group (airborne)',
      logo: '/logos/10th_special_forces_group_airborne.svg',
    },
    { name: '75th ranger regiment', logo: '/logos/75th_ranger_regiment.svg' },
  ],
  'Defense & Intelligence Venture Capital': [
    {
      name: 'lux capital',
      logo: '/logos/lux_capital.svg',
    },
    {
      name: 'in-q-tel, inc.',
      logo: '/logos/in-q-tel_inc.svg',
    },
    {
      name: 'red cell partners, llc',
      logo: '/logos/red_cell_partners_llc.png',
    },
    {
      name: 'founders fund',
      logo: '/logos/founders_fund.png',
    },
    {
      name: 'eight partners vc, llc',
      logo: '/logos/eight_partners_vc_llc.png',
    },
    {
      name: 'c5 capital limited',
      logo: '/logos/c5_capital_limited.svg',
    },
    {
      name: 'paladin capital group',
      logo: '/logos/paladin_capital_group.png',
    },
    {
      name: 'perot jain, l.p.',
      logo: '/logos/perot_jain_lp.png',
    },
    {
      name: 'shield capital, llc',
      logo: '/logos/shield_capital.svg',
    },
    {
      name: 'ah capital management, llc',
      logo: '/logos/ah_capital_management_llc.svg',
    },
    {
      name: 'general catalyst',
      logo: '/logos/general_catalyst.svg',
    },
  ],
  'Defense & Intelligence Consulting Firms': [
    {
      name: 'beacon global strategies llc',
      logo: '/logos/beacon_global_strategies_llc.png',
    },
    {
      name: 'hakluyt & company limited',
      logo: '/logos/haklyut_and_company_limited.png',
    },
    {
      name: 'westexec advisors, llc',
      logo: '/logos/westexec_advisors_llc.png',
    },
    {
      name: 'pallas advisors',
      logo: '/logos/pallas_advisors.png',
    },
    {
      name: 'macro advisory partners llp',
      logo: '/logos/macro_advisory_partners_llp.png',
    },
    {
      name: 'mclarty associates llc',
      logo: '/logos/mclarty_associates_llc.png',
    },
    {
      name: 'the cohen group',
      logo: '/logos/the_cohen_group.png',
    },
    {
      name: 'the scowcroft group, inc.',
      logo: '/logos/the_scowcroft_group_inc.png',
    },
    {
      name: 'asia group (the) llc',
      logo: '/logos/asia_group_the_llc.svg',
    },
    {
      name: 'kissinger associates, inc.',
      logo: '/logos/henry_kissinger.png',
    },
  ],
}

const masonryBooks = {
  'Some recommended books': [
    {
      name: 'pasztor-when-the-pentagon-was-for-sale',
      logo: '/bookCoverSmalls/when_the_pentagon_was_for_sale.png',
    },
    {
      name: 'lewis-spy-capitalism',
      logo: '/bookCoverSmalls/spy_capitalism.jpg',
    },
    {
      name: 'stares-the-militarization-of-space',
      logo: '/bookCoverSmalls/the_militarization_of_space.jpg',
    },
    {
      name: 'campbell-the-unsinkable-aircraft-carrier',
      logo: '/bookCoverSmalls/the_unsinkable_aircraft_carrier.png',
    },
    { name: 'coll-ghost-wars', logo: '/bookCoverSmalls/ghost_wars.png' },
    { name: 'coll-directorate-s', logo: '/bookCoverSmalls/directorate_s.png' },
    {
      name: 'coll-the-achilles-trap',
      logo: '/bookCoverSmalls/the_achilles_trap.jpg',
    },
    {
      name: 'grimes-the-history-of-big-safari',
      logo: '/bookCoverSmalls/the_history_of_big_safari.jpg',
    },
    { name: 'whittle-predator', logo: '/bookCoverSmalls/predator.jpg' },
    {
      name: 'michel-eyes-in-the-sky',
      logo: '/bookCoverSmalls/eyes_in_the_sky.png',
    },
    {
      name: 'weinberger-the-imagineers-of-war',
      logo: '/bookCoverSmalls/the_imagineers_of_war.png',
    },
    {
      name: 'weiner-legacy-of-ashes',
      logo: '/bookCoverSmalls/legacy_of_ashes.png',
    },
    {
      name: 'prados-safe-for-democracy',
      logo: '/bookCoverSmalls/safe_for_democracy.jpg',
    },
    {
      name: 'simpson-inside-the-green-berets',
      logo: '/bookCoverSmalls/inside_the_green_berets.jpg',
    },
    {
      name: 'naylor-relentless-strike',
      logo: '/bookCoverSmalls/relentless_strike.png',
    },
    {
      name: 'raviv-melman-spies-against-armageddon',
      logo: '/bookCoverSmalls/spies_against_armageddon.jpg',
    },
    {
      name: 'darshan-leitner-katz-harpoon',
      logo: '/bookCoverSmalls/harpoon.webp',
    },
    {
      name: 'tau-means-of-control',
      logo: '/bookCoverSmalls/means_of_control.jpg',
    },
  ],
}

const masonryContent = createMasonry(masonryEntities, kLogoPrefix, 75, 90)
const masonryBookContent = createMasonryBooks(
  masonryBooks,
  kLogoPrefix,
  250,
  100
)

export class DisplayHome extends React.Component<any, any> {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <>
        <div className="card border-none my-5">
          <div className="card-body">
            <h1 className="card-title fw-bold">
              Tech Inquiry simplifies public records analysis for the
              surveillance and weapons industries.
            </h1>
            <p className="card-text" style={{ fontSize: '1.6rem' }}>
              Curated feeds across thousands of organizations can be found
              through the search bar in the top-right, but representative
              examples across several categories of interest are listed below.
            </p>
          </div>
        </div>

        {masonryContent}
        {masonryBookContent}

        <div className="card border-primary my-3">
          <div className="card-body">
            <h1 className="card-title fw-bold">
              U.S. National Security Procurement and Cables
            </h1>
            <p className="card-text" style={{ fontSize: '1.4rem' }}>
              Below is an interactive map of U.S. embassies, naval bases, FBI
              field offices, ICE ERO/HSI offices, all DEA regional/division
              offices, and border patrol sectors offices. (Army bases are still
              in progress, and far more content and connections will be added
              over the next year.) Clicking on a particular icon links you to
              Tech Inquiry's data feed of procurement records and/or historical
              diplomatic cables associated with the particular location.
            </p>
            <p className="card-text" style={{ fontSize: '1.4rem' }}>
              The locations of surveillance towers along the U.S.-Mexico border
              were sourced from the EFF's March 2023{' '}
              <a href="https://www.eff.org/deeplinks/2023/03/cbp-expanding-its-surveillance-tower-program-us-mexico-border-and-were-mapping-it">
                mapping project
              </a>
              . Border patrol procurement ceased being broken down by regional
              office and was centralized into the main{' '}
              <a href={util.getEntityURL('united states border patrol')}>
                U.S. Border Patrol
              </a>{' '}
              office years ago.
            </p>
            <p className="card-text" style={{ fontSize: '1.4rem' }}>
              Cf. the map of{' '}
              <a href="https://worldbeyondwar.org/no-bases/">
                U.S. military bases
              </a>{' '}
              developed by World BEYOND War.
            </p>
            <div
              className="mt-2 mx-auto"
              style={{ height: '60vh', width: '95%' }}
            >
              {generateInstallationMap()}
            </div>
          </div>
        </div>
        <div className="card border-primary my-3">
          <div className="card-body">
            <h1 className="card-title fw-bold">Recommended Reporting</h1>
            <div className="container-fluid mt-3 px-1 py-1">
              {kHomeNotices.data.map(function (notice) {
                return (
                  <div
                    className="row align-items-start g-5 py-1"
                    style={{ fontSize: '1.4rem' }}
                  >
                    <div className="col-2">
                      <strong>{notice.date}</strong>
                    </div>
                    <div className="col-10">{notice.item}</div>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      </>
    )
  }
}
