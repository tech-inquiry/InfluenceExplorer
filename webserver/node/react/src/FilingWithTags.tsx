import * as React from 'react'

import * as util from './utilities/util'

function filterTags(tags, tagObj) {
  if (!tags) {
    return []
  }
  let filteredTags = []
  for (const item of tags) {
    if (
      item.tag == tagObj.tag &&
      item.is_entity == tagObj.is_entity &&
      item.username == tagObj.username
    ) {
      continue
    }
    filteredTags.push(item)
  }
  return filteredTags
}

export class FilingWithTags extends React.Component<any, any> {
  fetchTagsEndpoint: string
  addTagEndpoint: string
  deleteTagEndpoint: string
  uniqueKeys: string[]
  showTagFormButton: any
  username: string

  constructor(props) {
    super(props)

    const filing = this.props.filing
    this.state = {
      filing: filing,
      showTagForm: false,
      tagText: '',
      tags: [],
    }

    this.uniqueKeys = ['id']

    this.fetchTags = this.fetchTags.bind(this)
    this.tagExists = this.tagExists.bind(this)
    this.submitTag = this.submitTag.bind(this)
    this.handleTagInputPress = this.handleTagInputPress.bind(this)
    this.addTag = this.addTag.bind(this)
    this.deleteTag = this.deleteTag.bind(this)
    this.deleteTagLocally = this.deleteTagLocally.bind(this)

    this.tagToItem = this.tagToItem.bind(this)
    this.getTagDisplay = this.getTagDisplay.bind(this)
    this.getTagForm = this.getTagForm.bind(this)

    this.setEndpoint = this.setEndpoint.bind(this)
    this.setEndpoints = this.setEndpoints.bind(this)
    this.setUniqueKeys = this.setUniqueKeys.bind(this)
    this.getUniqueData = this.getUniqueData.bind(this)

    const that = this
    this.showTagFormButton = (
      <button
        className="btn btn-light"
        onClick={() => that.setState({ showTagForm: true })}
      >
        <span className="bi bi-plus plus-btn"></span>
      </button>
    )
  }

  setEndpoints(
    fetchTagsEndpoint: string,
    addTagEndpoint: string,
    deleteTagEndpoint: string
  ) {
    this.fetchTagsEndpoint = fetchTagsEndpoint
    this.addTagEndpoint = addTagEndpoint
    this.deleteTagEndpoint = deleteTagEndpoint
  }

  setEndpoint(endpoint: string) {
    this.fetchTagsEndpoint = `${endpoint}/getTags`
    this.addTagEndpoint = `${endpoint}/addTag`
    this.deleteTagEndpoint = `${endpoint}/deleteTag`
  }

  setUniqueKeys(uniqueKeys: string[]) {
    this.uniqueKeys = uniqueKeys
  }

  getUniqueData(filing: any) {
    let uniqueData = {}
    for (const uniqueKey of this.uniqueKeys) {
      uniqueData[uniqueKey] = String(filing[uniqueKey])
    }
    return uniqueData
  }

  async componentDidMount() {
    const uniqueData = this.getUniqueData(this.state.filing)
    const tags = await this.fetchTags(uniqueData)
    if (tags.length) {
      this.setState({ tags: tags })
    }
  }

  async componentDidUpdate(prevProps) {
    // This is apparently required due to the parent, FilingFeed, only
    // passing in filing as a property.
    if (prevProps.filing != this.props.filing) {
      const filing = this.props.filing

      const uniqueData = this.getUniqueData(filing)

      const tags = await this.fetchTags(uniqueData)
      this.setState({
        filing: filing,
        tagText: '',
        tags: tags,
      })
    }
  }

  async fetchTags(uniqueData) {
    if (!this.fetchTagsEndpoint) {
      console.log('this.fetchTagsEndpoint was undefined in fetchTags')
      return []
    }

    this.username = util.retrieveUsername()

    const token = util.retrieveToken()
    const result = await fetch(this.fetchTagsEndpoint, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        token: token,
        uniqueData: uniqueData,
      }),
    })
    const data = await result.json()

    if (data.message == 'success') {
      return data.tags
    } else {
      if (data.message == 'Invalid token') {
        util.dropToken()
      }
      return []
    }
  }

  tagExists(tagObj): boolean {
    // Linear time is okay.
    for (const tag of this.state.tags) {
      if (
        tag.tag == tagObj.tag &&
        tag.is_entity == tagObj.is_entity &&
        tag.username == tagObj.username
      ) {
        return true
      }
    }

    return false
  }

  addTag(tagObj) {
    let tags = this.state.tags
    tags.push({
      tag: tagObj.tag,
      is_entity: tagObj.is_entity,
      username: this.username,
    })

    this.setState({
      showTagForm: false,
      tagText: '',
      tags: tags,
    })
  }

  submitTag(event) {
    event.preventDefault()
    const tag = this.state.tagText
    const filing = this.state.filing
    const token = util.retrieveToken()
    if (!token) {
      alert('Cannot submit a tag without a token.')
    }

    if (
      this.tagExists({ tag: tag, is_entity: false, username: this.username })
    ) {
      this.setState({
        showTagForm: false,
        tagText: '',
      })
      return
    }

    const uniqueData = this.getUniqueData(filing)

    fetch(this.addTagEndpoint, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        token: token,
        uniqueData: uniqueData,
        tag: tag,
        isEntity: false,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.message == 'success') {
          this.addTag({ tag: tag, is_entity: false })
        } else {
          if (data.message == 'Invalid token') {
            util.dropToken()
          }
          this.setState({ showTagForm: false, tagText: '' })
          alert(data.message)
        }
      })
  }

  deleteTagLocally(tagObj) {
    const filteredTags = filterTags(this.state.tags, tagObj)
    this.setState({ tags: filteredTags })
  }

  deleteTag(tagObj) {
    const filing = this.state.filing
    const token = util.retrieveToken()
    if (!token) {
      alert('Cannot delete a tag without a token.')
    }

    const uniqueData = this.getUniqueData(filing)

    const that = this
    fetch(this.deleteTagEndpoint, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        token: token,
        uniqueData: uniqueData,
        tag: tagObj.tag,
        isEntity: tagObj.is_entity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.message == 'success') {
          that.deleteTagLocally(tagObj)
        } else {
          if (data.message == 'Invalid token') {
            util.dropToken()
          }
          alert(data.message)
        }
      })
  }

  tagToItem(tagObj) {
    const that = this
    return (
      <button className="btn btn-light">
        <a className="me-2" href={util.getTagSearchURL(tagObj.tag)}>
          #{tagObj.tag}
        </a>
        {tagObj.username == this.username ? (
          <span
            onClick={() => that.deleteTag(tagObj)}
            className="bi bi-trash trash-btn float-end"
            aria-hidden="true"
          ></span>
        ) : undefined}
      </button>
    )
  }

  handleTagInputPress(evt) {
    if (evt.key == 'Enter') {
      this.submitTag(evt)
    }
  }

  getTagForm() {
    const token = util.retrieveToken()

    return token && this.state.showTagForm ? (
      <div className="input-group">
        <div className="input-group-prepend">
          <div className="input-group-text">
            <i className="bi bi-plus me-2"></i>
          </div>
        </div>
        <input
          type="text"
          className="form-control-sm"
          style={{ width: '250px' }}
          id="tag"
          placeholder="a-new-tag-here"
          value={this.state.tagText}
          onChange={(e) => this.setState({ tagText: e.target.value })}
          onKeyDown={this.handleTagInputPress}
          maxLength={64}
        />
      </div>
    ) : undefined
  }

  getTagDisplay() {
    const tags = this.state.tags
    const token = util.retrieveToken()
    if (!tags.length && !token) {
      return undefined
    }

    return tags.sort((a, b) => a.tag.localeCompare(b.tag)) ? (
      <div>
        <span className="mx-2">Tags:</span>
        {tags.map(this.tagToItem)}
        {token ? this.showTagFormButton : undefined}
      </div>
    ) : undefined
  }

  render() {
    return undefined
  }
}
