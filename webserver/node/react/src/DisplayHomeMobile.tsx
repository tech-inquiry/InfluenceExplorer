import * as React from 'react'

import { EntityAutocompleteDisplayMobile } from './EntityAutocomplete'

export class DisplayHomeMobile extends React.Component<any, any> {
  maxInputLength: number

  constructor(props) {
    super(props)
    this.maxInputLength = 150
  }

  render() {
    return (
      <EntityAutocompleteDisplayMobile maxInputLength={this.maxInputLength} />
    )
  }
}
