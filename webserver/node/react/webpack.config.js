const webpack = require('webpack')
const CompressionPlugin = require('compression-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')

const path = require('path')
module.exports = (env) => {
  return {
    devServer: {
      static: {
        directory: path.join(__dirname, '../public'),
      },
      devMiddleware: {
        writeToDisk: true,
      },
      compress: true,
      port: 9000,
    },
    entry: {
      cable: './src/cable.tsx',
      login: './src/login.tsx',
      requestEntityUpdate: './src/requestEntityUpdate.tsx',
      main: './src/main.tsx',
      leaks: './src/leaks.tsx',
      homepage: './src/homepage.tsx',
      book: './src/book.tsx',
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
      filename: '[name]-bundle.js',
      path: path.resolve(__dirname, '../public/dist'),
    },
    optimization: {
      minimize: true,
      minimizer: [new TerserPlugin()],
    },
    module: {
      rules: [
        {
          test: /\.(?:js|mjs|cjs)$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [['@babel/preset-env', { targets: 'defaults' }]],
            },
          },
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
      ],
    },
    plugins: [
      new webpack.ProvidePlugin({
        React: 'react',
      }),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production'),
        },
        'process.env.EXPLORER_HOSTNAME': JSON.stringify(env.EXPLORER_HOSTNAME),
        'process.env.API_ORIGIN': JSON.stringify(env.API_ORIGIN),
      }),
      new CompressionPlugin({
        algorithm: 'gzip',
        test: /\.js$|\.css$/,
      }),
    ],
  }
}
