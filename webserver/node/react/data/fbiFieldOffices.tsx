import * as React from 'react'

export const data = {
  'FBI Albany': {
    latLong: [42.634136757444, -73.77267964743614],
    entity: 'fbi albany',
    title: 'FBI Albany',
    addressLines: [
      '200 McCarty Avenue',
      'Albany, NY 12209',
      'Phone: (518) 465-7551',
    ],
  },
  'FBI Albuquerque': {
    latLong: [35.12804094754232, -106.61282612348796],
    entity: 'fbi albuquerque',
    title: 'FBI Albuquerque',
    addressLines: [
      '4200 Luecking Park Avenue NE',
      'Albuquerque, NM 87107',
      'Phone: (505) 889-1300',
    ],
  },
  'FBI Anchorage': {
    latLong: [61.216805043787886, -149.8824887744861],
    entity: 'fbi anchorage',
    title: 'FBI Anchorage',
    addressLines: [
      '101 East Sixth Avenue',
      'Anchorage, AK 99501',
      'Phone: (907) 276-4441',
    ],
  },
  'FBI Atlanta': {
    latLong: [33.87218364623264, -84.26744144135913],
    entity: 'fbi atlanta',
    title: 'FBI Atlanta',
    addressLines: [
      '3000 Flowers Road S',
      'Atlanta, GA 30341',
      'Phone: (770) 216-3000',
    ],
  },
  'FBI Baltimore': {
    latLong: [39.32674992202007, -76.75215240537474],
    entity: 'fbi baltimore',
    title: 'FBI Baltimore',
    addressLines: [
      '2600 Lord Baltimore Drive',
      'Baltimore, MD 21244',
      'Phone: (410) 265-8080',
    ],
  },
  'FBI Birmingham': {
    latLong: [33.52320572913621, -86.81646260579724],
    entity: 'fbi birmingham',
    title: 'FBI Birmingham',
    addressLines: [
      '1000 18th Street North',
      'Birmingham, AL 35203',
      'Phone: (205) 326-6166',
    ],
  },
  'FBI Boston': {
    latLong: [42.397157555877385, -71.03875003396882],
    entity: 'fbi boston',
    title: 'FBI Boston',
    addressLines: [
      '201 Maple Street',
      'Chelsea, MA 02150',
      'Phone: (857) 386-2000',
    ],
  },
  'FBI Buffalo': {
    latLong: [42.88747237114748, -78.87995182288655],
    entity: 'fbi buffalo',
    title: 'FBI Buffalo',
    addressLines: [
      'One FBI Plaza',
      'Buffalo, NY 14202',
      'Phone: (716) 856-7800',
    ],
  },
  'FBI Charlotte': {
    latLong: [35.140221119584616, -80.9196504042323],
    entity: 'fbi charlotte',
    title: 'FBI Charlotte',
    addressLines: [
      '7915 Microsoft Way',
      'Charlotte, NC 28273',
      'Phone: (704) 672-6100',
    ],
  },
  'FBI Chicago': {
    latLong: [41.86551766103679, -87.67992563272026],
    entity: 'fbi chicago',
    title: 'FBI Chicago',
    addressLines: [
      '2111 W. Roosevelt Road',
      'Chicago, IL 60608',
      'Phone: (312) 421-6700',
    ],
  },
  'FBI Cincinnati': {
    latLong: [39.199397128822426, -84.37211478759704],
    entity: 'fbi cincinnati',
    title: 'FBI Cincinnati',
    addressLines: [
      '2012 Ronald Reagan Drive',
      'Cincinnati, OH 45236',
      'Phone: (513) 421-4310',
    ],
  },
  'FBI Cleveland': {
    latLong: [41.508828105612714, -81.68663380520118],
    entity: 'fbi cleveland',
    title: 'FBI Cleveland',
    addressLines: [
      '1501 Lakeside Avenue',
      'Cleveland, OH 44114',
      'Phone: (216) 522-1400',
    ],
  },
  'FBI Columbia': {
    latLong: [34.06002672899491, -81.12780062355594],
    entity: 'fbi columbia',
    title: 'FBI Columbia',
    addressLines: [
      '151 Westpark Boulevard',
      'Columbia, SC 29210',
      'Phone: (803) 551-4200',
    ],
  },
  'FBI Dallas': {
    latLong: [32.8551774895594, -96.88950297550117],
    entity: 'fbi dallas',
    title: 'FBI Dallas',
    addressLines: [
      'One Justice Way',
      'Dallas, TX 75220',
      'Phone: (972) 559-5000',
    ],
  },
  'FBI Denver': {
    latLong: [39.76614331197744, -104.89610063283503],
    entity: 'fbi denver',
    title: 'FBI Denver',
    addressLines: [
      '8000 East 36th Avenue',
      'Denver, CO 80238',
      'Phone: (303) 629-7171',
    ],
  },
  'FBI Detroit': {
    latLong: [42.33114338833194, -83.05321100513383],
    entity: 'fbi detroit',
    title: 'FBI Detroit',
    addressLines: [
      '477 Michigan Ave., 26th Floor',
      'Detroit, MI 48226',
      'Phone: (313) 965-2323',
    ],
  },
  'FBI El Paso': {
    latLong: [31.815513315666134, -106.53689348812479],
    entity: 'fbi el paso',
    title: 'FBI El Paso',
    addressLines: [
      'El Paso Federal Justice Center',
      '660 South Mesa Hills Drive',
      'El Paso, TX 79912',
      'Phone: (915) 832-5000',
    ],
  },
  'FBI Honolulu': {
    latLong: [21.32368406338233, -158.06803347592228],
    entity: 'fbi honolulu',
    title: 'FBI Honolulu',
    addressLines: [
      '91-1300 Enterprise Street',
      'Kapolei, HI 96707',
      'Phone: (808) 566-4300',
    ],
  },
  'FBI Houston': {
    latLong: [29.83670640170238, -95.48251704163059],
    entity: 'fbi houston',
    title: 'FBI Houston',
    addressLines: [
      '1 Justice Park Drive',
      'Houston, TX 77092',
      'Phone: (713) 693-5000',
    ],
  },
  'FBI Indianapolis': {
    latLong: [39.91536897152464, -86.06203842311963],
    entity: 'fbi indianapolis',
    title: 'FBI Indianapolis',
    addressLines: [
      '8825 Nelson B Klein Pkwy',
      'Indianapolis, IN 46250',
      'Phone: (317) 595-4000',
    ],
  },
  'FBI Jackson': {
    latLong: [32.3990682737224, -90.18718400435762],
    entity: 'fbi jackson',
    title: 'FBI Jackson',
    addressLines: [
      '1220 Echelon Parkway',
      'Jackson, MS 39213',
      'Phone: (601) 948-5000',
    ],
  },
  'FBI Jacksonville': {
    latLong: [30.249016000602474, -81.52275316309039],
    entity: 'fbi jacksonville',
    title: 'FBI Jacksonville',
    addressLines: [
      '6061 Gate Parkway',
      'Jacksonville, FL 32256',
      'Phone: (904) 248-7000',
    ],
  },
  'FBI Kansas City': {
    latLong: [39.09751362442021, -94.59412580539251],
    entity: 'fbi kansas city',
    title: 'FBI Kansas City',
    addressLines: [
      '1300 Summit Street',
      'Kansas City, MO 64105',
      'Phone: (816) 512-8200',
    ],
  },
  'FBI Knoxville': {
    latLong: [35.951539587359704, -84.02607502342592],
    entity: 'fbi knoxville',
    title: 'FBI Knoxville',
    addressLines: [
      '1501 Dowell Springs Boulevard',
      'Knoxville, TN 37909',
      'Phone: (865) 544-0751',
    ],
  },
  'FBI Las Vegas': {
    latLong: [36.19422618908655, -115.16380047534527],
    entity: 'fbi las vegas',
    title: 'FBI Las Vegas',
    addressLines: [
      '1787 West Lake Mead Boulevard',
      'Las Vegas, NV 89106-2135',
      'Phone: (702) 385-1281',
    ],
  },
  'FBI Little Rock': {
    latLong: [34.74199207290502, -92.39935953454969],
    entity: 'fbi little rock',
    title: 'FBI Little Rock',
    addressLines: [
      '24 Shackleford West Boulevard',
      'Little Rock, AR 72211-3755',
      'Phone: (501) 221-9100',
    ],
  },
  'FBI Los Angeles': {
    latLong: [34.05672598438745, -118.44874057544675],
    entity: 'fbi los angeles',
    title: 'FBI Los Angeles',
    addressLines: [
      '11000 Wilshire Boulevard',
      'Suite 1700',
      'Los Angeles, CA 90024',
      'Phone: (310) 477-6565',
    ],
  },
  'FBI Louisville': {
    latLong: [38.22083549713753, -85.5292835316689],
    entity: 'fbi louisville',
    title: 'FBI Louisville',
    addressLines: [
      '12401 Sycamore Station Place',
      'Louisville, KY 40299-6198',
      'Phone: (502) 263-6000',
    ],
  },
  'FBI Memphis': {
    latLong: [35.13324229855625, -89.86357437684954],
    entity: 'fbi memphis',
    title: 'FBI Memphis',
    addressLines: [
      '225 North Humphreys Boulevard',
      'Suite 3000',
      'Memphis, TN 38120',
      'Phone: (901) 747-4300',
    ],
  },
  'FBI Miami': {
    latLong: [25.990257025816014, -80.33796900697824],
    entity: 'fbi miami',
    title: 'FBI Miami',
    addressLines: [
      '2030 SW 145th Avenue',
      'Miramar, FL 33027',
      'Phone: (754) 703-2000',
    ],
  },
  'FBI Milwaukee': {
    latLong: [42.980336620112496, -87.86065573265715],
    entity: 'fbi milwaukee',
    title: 'FBI Milwaukee',
    addressLines: [
      '3600 S. Lake Drive',
      'St. Francis, WI 53235',
      'Phone: (414) 276-4684',
    ],
  },
  'FBI Minneapolis': {
    latLong: [45.07163122495639, -93.30142677606528],
    entity: 'fbi minneapolis',
    title: 'FBI Minneapolis',
    addressLines: [
      '1501 Freeway Boulevard',
      'Brooklyn Center, MN 55430',
      'Phone: (763) 569-8000',
    ],
  },
  'FBI Mobile': {
    latLong: [30.696308049035377, -88.04158140598155],
    entity: 'fbi mobile',
    title: 'FBI Mobile',
    addressLines: [
      '200 North Royal Street',
      'Mobile, AL 36602',
      'Phone: (251) 438-3674',
    ],
  },
  'FBI New Haven': {
    latLong: [41.30900483038694, -72.92069500521744],
    entity: 'fbi new haven',
    title: 'FBI New Haven',
    addressLines: [
      '600 State Street',
      'New Haven, CT 06511',
      'Phone: (203) 777-6311',
    ],
  },
  'FBI New Orleans': {
    latLong: [30.030953941339373, -90.0427191882319],
    entity: 'fbi new orleans',
    title: 'FBI New Orleans',
    addressLines: [
      '2901 Leon C. Simon Boulevard',
      'New Orleans, LA 70126',
      'Phone: (504) 816-3000',
    ],
  },
  'FBI New York City': {
    latLong: [40.71549195378481, -74.00424738064496],
    entity: 'fbi new york city',
    title: 'FBI New York City',
    addressLines: [
      '26 Federal Plaza, 23rd Floor',
      'New York, NY 10278-0004',
      'Phone: (212) 384-1000',
    ],
  },
  'FBI Newark': {
    latLong: [40.7383515713896, -74.16463817642708],
    entity: 'fbi newark',
    title: 'FBI Newark',
    addressLines: [
      'Claremont Tower',
      '11 Centre Place',
      'Newark, NJ 07102',
      'Phone: (973) 792-3000',
    ],
  },
  'FBI Norfolk': {
    latLong: [36.732013864955356, -76.22176680557078],
    entity: 'fbi norfolk',
    title: 'FBI Norfolk',
    addressLines: [
      '509 Resource Row',
      'Chesapeake, VA 23320',
      'Phone: (757) 455-0100',
    ],
  },
  'FBI Oklahoma City': {
    latLong: [35.6113581949966, -97.57708217681561],
    entity: 'fbi oklahoma city',
    title: 'FBI Oklahoma City',
    addressLines: [
      '3301 West Memorial Road',
      'Oklahoma City, OK 73134-7098',
      'Phone: (405) 290-7770',
    ],
  },
  'FBI Omaha': {
    latLong: [41.215349324547375, -96.10177740522504],
    entity: 'fbi omaha',
    title: 'FBI Omaha',
    addressLines: [
      '4411 South 121st Court',
      'Omaha, NE 68137-2112',
      'Phone: (402) 493-8688',
    ],
  },
  'FBI Philadelphia': {
    latLong: [39.952645822595876, -75.15084390532576],
    entity: 'fbi philadelphia',
    title: 'FBI Philadelphia',
    addressLines: [
      'William J. Green, Jr. Building',
      '600 Arch Street, 8th Floor',
      'Philadelphia, PA 19106',
      'Phone: (215) 418-4000',
    ],
  },
  'FBI Phoenix': {
    latLong: [33.68277377319446, -112.06428870430021],
    entity: 'fbi phoenix',
    title: 'FBI Phoenix',
    addressLines: [
      '21711 N. 7th Street',
      'Phoenix, AZ 85024',
      'Phone: (623) 466-1999',
    ],
  },
  'FBI Pittsburgh': {
    latLong: [40.422544939121835, -79.9590609052886],
    entity: 'fbi pittsburgh',
    title: 'FBI Pittsburgh',
    addressLines: [
      '3311 East Carson Street',
      'Pittsburgh, PA 15203',
      'Phone: (412) 432-4000',
    ],
  },
  'FBI Portland': {
    latLong: [45.578214390052445, -122.57005133369371],
    entity: 'fbi portland',
    title: 'FBI Portland',
    addressLines: [
      '9109 NE Cascades Parkway',
      'Portland, OR 97220',
      'Phone: (503) 224-4181',
    ],
  },
  'FBI Richmond': {
    latLong: [37.641437072019265, -77.48481580550337],
    entity: 'fbi richmond',
    title: 'FBI Richmond',
    addressLines: [
      '1970 East Parham Road',
      'Richmond, VA 23228',
      'Phone: (804) 261-1044',
    ],
  },
  'FBI Salt Lake City': {
    latLong: [40.77533047016561, -112.02185626293303],
    entity: 'fbi richmond',
    title: 'FBI Richmond',
    addressLines: [
      '5425 West Amelia Earhart Drive',
      'Salt Lake City, UT 84116',
      'Phone: (801) 579-1400',
    ],
  },
  'FBI Sacramento': {
    latLong: [38.793415135824645, -121.30334926295033],
    entity: 'fbi sacramento',
    title: 'FBI Sacramento',
    addressLines: [
      '2001 Freedom Way',
      'Roseville, CA 95678',
      'Phone: (916) 746-7000',
    ],
  },
  'FBI San Antonio': {
    latLong: [29.5705408123835, -98.59708690605086],
    entity: 'fbi san antonio',
    title: 'FBI San Antonio',
    addressLines: [
      '5740 University Heights Blvd.',
      'San Antonio, TX 78249',
      'Phone: (210) 225-6741',
    ],
  },
  'FBI San Diego': {
    latLong: [32.897303715714706, -117.21469017700292],
    entity: 'fbi san diego',
    title: 'FBI San Diego',
    addressLines: [
      '10385 Vista Sorrento Parkway',
      'San Diego, CA 92121',
      'Phone: (858) 320-1800',
    ],
  },
  'FBI San Francisco': {
    latLong: [37.78194149235847, -122.41809680410225],
    entity: 'fbi san francisco',
    title: 'FBI San Francisco',
    addressLines: [
      '450 Golden Gate Avenue, 13th Floor',
      'San Francisco, CA 94102-9523',
      'Phone: (415) 553-7400',
    ],
  },
  'FBI San Juan': {
    latLong: [18.42317214802511, -66.06423326988771],
    entity: 'fbi san juan',
    title: 'FBI San Juan',
    addressLines: [
      '140 Carlos Chardon Avenue',
      'Hato Rey, PR 00918',
      'Phone: (787) 987-6500',
    ],
  },
  'FBI Seattle': {
    latLong: [47.606681934436, -122.3345364329966],
    entity: 'fbi seattle',
    title: 'FBI Seattle',
    addressLines: [
      '1110 3rd Avenue',
      'Seattle, WA 98101-2904',
      'Phone: (206) 622-0460',
    ],
  },
  'FBI Springfield': {
    latLong: [39.76430866568247, -89.64265284766797],
    entity: 'fbi springfield',
    title: 'FBI Springfield',
    addressLines: [
      '900 East Linton Avenue',
      'Springfield, IL 62703',
      'Phone: (217) 522-9675',
    ],
  },
  'FBI St. Louis': {
    latLong: [38.629774653108235, -90.21326653426485],
    entity: 'fbi st. louis',
    title: 'FBI St. Louis',
    addressLines: [
      '2222 Market Street',
      'St. Louis, MO 63103',
      'Phone: (314) 589-2500',
    ],
  },
  'FBI Tampa': {
    latLong: [27.949216063357635, -82.53943544847398],
    entity: 'fbi tampa',
    title: 'FBI Tampa',
    addressLines: [
      '5525 West Gray Street',
      'Tampa, FL 33609',
      'Phone: (813) 253-1000',
    ],
  },
  'FBI Washington': {
    latLong: [38.897752071361055, -77.01559297657164],
    entity: 'fbi washington',
    title: 'FBI Washington',
    addressLines: [
      '601 4th Street NW',
      'Washington, DC 20535',
      'Phone: (202) 278-2000',
    ],
  },
  'Redstone Arsenal': {
    latLong: [34.68570019356632, -86.64950022750727],
    entity: 'redstone arsenal',
    title: 'Redstone Arsenal',
    controller: 'U.S. Army',
    addressLines: [
      'Redstone Arsenal, AL'
    ]
  }
}
