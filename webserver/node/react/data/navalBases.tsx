import * as React from 'react'

export const data = {
  'Armed Forces Experimental Training Activity Camp Peary': {
    latLong: [37.30753810325468, -76.66238712272725],
    entity: 'armed forces experimental training activity camp peary',
    title: 'AFETA Camp Peary / \"The Farm\"',
    controller: 'Department of Defense',
    addressLines: [
      'Williamsburg, VA 23185'
    ]
  },
  'Camp Lemonnier, Djibouti': {
    latLong: [11.540662840271038, 43.15835441292781],
    entity: 'camp lemonnier, djibouti',
    title: 'Camp Lemonnier, Djibouti',
    controller: 'Navy Region Europe, Africa, Central',
    addressLines: [
      'PSC 831 Box 0040',
      'FPO AE 09363-0001',
    ],
  },
  'Christchurch International Airport': {
    latLong: [-43.48728717272058, 172.53851402213002],
    entity: 'christchurch international airport limited',
    title: 'Christchurch International Airport',
    controller: 'Royal New Zealand Air Force',
    addressLines: [
      '30 Durey Road, Harewood, Christchurch 8053'
    ],
  },
  'Fleet Activities Chinhae': {
    latLong: [35.155601971788776, 128.65422726068127],
    entity: 'commander, fleet activities chinhae',
    title: 'Fleet Activities Chinhae',
    controller: 'Naval Forces Korea',
    addressLines: [
      'PSC 479',
      'FPO AP 96269-1100',
      'Phone: 001-82-55-540-5310',
    ],
  },
  'Fleet Activities Okinawa': {
    latLong: [26.331315165599346, 127.752914075284],
    entity: 'fleet activities okinawa',
    title: 'Fleet Activities Okinawa',
    controller: 'Navy Region Japan',
    addressLines: [
      'PSC 480 Box 1100',
      'FPO AP',
      'Okinawa, Japan 96370-1100',
    ],
  },
  'Fleet Activities Sasebo': {
    latLong: [33.1616054, 129.71869572300048],
    entity: 'united states fleet activities sasebo',
    title: 'Fleet Activities Sasebo',
    controller: 'Navy Region Japan',
    addressLines: [
      'PSC 476 Box 1',
      'FPO AP',
      'Sasebo, Japan 96322',
    ],
  },
  'Fleet Activities Yokosuka': {
    latLong: [35.28435207576819, 139.6662647844459],
    entity: 'united states fleet activities yokosuka',
    title: 'Fleet Activities Yokosuka',
    controller: 'Navy Region Japan',
    addressLines: [
      'Kanagawa Prefecture',
      'Yokosuka City',
      '1 Banchi Tomarimachi, Japan',
    ],
  },
  'Harvey Point Defense Testing Activity': {
    latLong: [36.098457785938166, -76.32778441425347],
    entity: 'harvey point defense testing activity',
    title: 'Harvey Point Defense Testing Activity',
    controller: 'Department of Defense',
    addressLines: [
      '2835 Harvey Point Rd',
      'Hertford, NC 27944'
    ]
  },
  'Joint Base Anacostia-Bolling': {
    latLong: [38.84358995942755, -77.01220026930892],
    entity: 'joint base anacostia-bolling',
    title: 'Joint Base Anacostia-Bolling',
    controller: 'Naval District Washington',
    addressLines: [
      '20 MacDill Blvd SE, Washington, DC 20032',
    ],
  },
  'Joint Base Andrews': {
    latLong: [38.80172128979321, -76.87610627637922],
    entity: 'joint base andrews',
    title: 'Joint Base Andrews',
    controller: 'Naval District of Washington',
    addressLines: [
      'Joint Base Andrews, MD 20762'
    ],
  },
  'Joint Base Charleston': {
    latLong: [32.89097148929066, -80.06918456113281],
    entity: 'joint base charleston',
    title: 'Joint Base Charleston',
    controller: 'Navy Region Southeast',
    addressLines: [
      '104 E. Simpson Street',
      'Building 500',
      'SC 29404',
      'Phone: (843) 963-4406',
    ],
  },
  'Naval Academy': {
    latLong: [38.981257244403004, -76.48548090725085],
    entity: 'united states naval academy',
    title: 'U.S. Naval Academy',
    addressLines: ['290 Buchanan Rd', 'Annapolis, MD 21402'],
  },
  'Naval Air Facility Atsugi': {
    latLong: [35.45820798982594, 139.44786230979736],
    entity: 'naval air facility atsugi',
    title: 'Naval Air Facility Atsugi',
    controller: 'Navy Region Japan',
    addressLines: ['PSC 477 BOX 9', 'FPO AP 96306-1209'],
  },
  'Naval Air Facility El Centro': {
    latLong: [32.811460497373865, -115.67230378325928],
    entity: 'naval air facility el centro',
    title: 'Naval Air Facility El Centro',
    controller: 'Navy Region Southwest',
    addressLines: [
      'El Centro, CA 92243-5001',
      'Phone: (760) 339-2673',
    ],
  },
  'Naval Air Facility Misawa': {
    latLong: [40.69894747348223, 141.3745809253998],
    entity: 'misawa air base',
    title: 'Naval Air Facility Misawa (Misawa Air Base)',
    controller: 'Navy Region Japan',
    addressLines: [
      'Unit 5048 APO AP ',
      'Misawa, Japan 96319-5000',
    ],
  },
  'Naval Air Station Corpus Christi': {
    latLong: [27.69358547540177, -97.2891781770355],
    entity: 'naval air station corpus christi',
    title: 'Naval Air Station Corpus Christi',
    controller: 'Navy Region Southeast',
    addressLines: [
      '10651 E Street',
      'Bldg H-100, Suite 5042',
      'Corpus Christi, TX 78419',
      'Phone: (361) 961-2674/3420',
    ],
  },
  'Naval Air Station Fallon': {
    latLong: [39.42043815649015, -118.72420920198255],
    entity: 'naval air station fallon',
    title: 'Naval Air Station Fallon',
    controller: 'Navy Region Southwest',
    addressLines: [
      '4755 Pasture Road Fallon',
      'Fallon, NV 89496',
      'Phone: (775) 426-2880',
    ],
  },
  'Naval Air Station Jacksonville': {
    latLong: [30.220007017408175, -81.68501659581777],
    entity: 'naval air station jacksonville',
    title: 'Naval Air Station Jacksonville',
    controller: 'Navy Region Southeast',
    addressLines: [
      'Jacksonville, FL 32212',
      'Phone: (904) 542-2338/2339',
    ],
  },
  'Naval Air Station Joint Reserve Base Fort Worth': {
    latLong: [32.76514342630478, -97.42119932486992],
    entity: 'naval air station joint reserve base fort worth',
    title: 'Naval Air Station Joint Reserve Base Fort Worth',
    controller: 'Navy Region Southeast',
    addressLines: [
      '1510 Chennault Ave',
      'Fort Worth, TX 76113',
      'Phone: (817) 782-7815',
    ],
  },
  'Naval Air Station Joint Reserve Base New Orleans': {
    latLong: [29.82335891394819, -90.00879378671289],
    entity: 'naval air station joint reserve base new orleans',
    title: 'Naval Air Station Joint Reserve Base New Orleans',
    controller: 'Navy Region Southeast',
    addressLines: [
      '400 Russell Ave',
      'New Orleans, LA 70143',
      'Phone: (504) 678-3260',
    ],
  },
  'Naval Air Station Key West': {
    latLong: [24.577081184599695, -81.70004358189702],
    entity: 'naval air station key west',
    title: 'Naval Air Station Key West',
    controller: 'Navy Region Southeast',
    addressLines: [
      'P.O. Box 9001',
      'Key West, FL 33040',
      'Phone: (305) 293-2425',
    ],
  },
  'Naval Air Station Kingsville': {
    latLong: [27.49530781111326, -97.81871134332069],
    entity: 'naval air station kingsville',
    title: 'Naval Air Station Kingsville',
    controller: 'Navy Region Southeast',
    addressLines: [
      '554 McCain Street',
      'Suite 214',
      'Kingsville, TX 78363',
      'Phone: (361) 516-6146',
    ],
  },
  'Naval Air Station Lemoore': {
    latLong: [36.256030643303326, -119.90367872386611],
    entity: 'naval air station lemoore',
    title: 'Naval Air Station Lemoore',
    controller: 'Navy Region Southwest',
    addressLines: [
      '700 Avenger Ave.',
      'Lemoore, CA 93246',
      'Phone: (559) 998-3393',
    ],
  },
  'Naval Air Station Meridian': {
    latLong: [32.5559380818014, -88.55456737067381],
    entity: 'naval air station meridian',
    title: 'Naval Air Station Meridian',
    controller: 'Navy Region Southeast',
    addressLines: [
      '255 Rosenbaum Ave',
      'Meridian, MS 39309-5003',
      'Phone: (601) 679-2602',
    ],
  },
  'Naval Air Station Oceana': {
    latLong: [36.81238115129853, -76.02479287479426],
    entity: 'naval air station oceana',
    title: 'Naval Air Station Oceana',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '1750 Tomcat Blvd.',
      'Virginia Beach, VA 23460',
    ],
  },
  'Naval Air Station Patuxent River': {
    latLong: [38.27484053474952, -76.4457694013979],
    entity: 'naval air station patuxent river',
    title: 'Naval Air Station Patuxent River',
    controller: 'Naval District Washington',
    addressLines: [
      '22268 Cedar Point Road Building 409',
      'Patuxent River MD 20670-1154',
      'Phone: (301) 757-6748',
    ],
  },
  'Naval Air Station Pensacola': {
    latLong: [30.35025794356411, -87.29136446495272],
    entity: 'naval air station pensacola',
    title: 'Naval Air Station Pensacola',
    controller: 'Navy Region Southeast',
    addressLines: [
      '150 Hase Road',
      'Suite A',
      'Pensacola, FL 32508-1051',
      'Phone: (850) 452-4436',
    ],
  },
  'Naval Air Station Sigonella': {
    latLong: [37.40815118413565, 14.911225536487253],
    entity: 'naval air station sigonella',
    title: 'Naval Air Station Sigonella',
    controller: 'Navy Region Europe, Africa, Central',
    addressLines: [
      'PSC 812 BOX 1000',
      'FPO AE 09627-1000',
    ],
  },
  'Naval Air Station Whidbey Island': {
    latLong: [48.34108652851902, -122.67226880960065],
    entity: 'naval air station whidbey island',
    title: 'Naval Air Station Whidbey Island',
    controller: 'Navy Region Northwest',
    addressLines: [
      '2853 Langley Boulevard',
      'Oak Harbor, WA 98278-5101',
      'Phone: (360) 257-2271',
    ],
  },
  'Naval Air Station Whiting Field': {
    latLong: [30.7143337346701, -87.01788613046158],
    entity: 'naval air station whiting field',
    title: 'Naval Air Station Whiting Field',
    controller: 'Navy Region Southeast',
    addressLines: [
      '7550 USS Essex St',
      'Milton, FL 32570',
      'Phone: (850) 623-7341',
    ],
  },
  'Naval Air Weapons Station China Lake': {
    latLong: [35.6551699503946, -117.6574293514538],
    entity: 'naval air weapons station china lake',
    title: 'Naval Air Weapons Station China Lake',
    controller: 'Navy Region Southwest',
    addressLines: [
      '1 Administration Circle',
      'China Lake, CA 93555',
      'Phone: (760) 939-1693',
    ],
  },
  'Naval Amphibious Base Little Creek': {
    latLong: [36.91639133441057, -76.1913410931749],
    entity: 'joint expeditionary base little creek-fort story',
    title: 'Naval Amphibious Base Little Creek (JEB Little Creek-Fort Story)',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '2600 Tarawa Court, Ste 100',
      'Virginia Beach, VA 23459-3297',
    ],
  },
  'Naval Base Coronado': {
    latLong: [32.67175767287566, -117.16533803776875],
    entity: 'naval base coronado',
    title: 'Naval Base Coronado',
    controller: 'Navy Region Southwest',
    addressLines: [
      '3455 Senn Road',
      'Coronado, CA 92136',
      'Phone: (619) 545-8167',
    ],
  },
  'Naval Base Guam': {
    latLong: [13.415144964610102, 144.67330238902852],
    entity: 'andersen air force base',
    title: 'Naval Base Guam',
    addressLines: ['Joint Region Marianas', 'Santa Rita, Guam'],
  },
  'Naval Base Kitsap': {
    latLong: [47.562495112864745, -122.6499310469919],
    entity: 'naval base kitsap',
    title: 'Naval Base Kitsap',
    controller: 'Navy Region Northwest',
    addressLines: [
      '120 South Dewey Street',
      'Bremerton, WA 98314-5020',
      'Phone: (360) 396-6111',
    ],
  },
  'Naval Base Point Loma': {
    latLong: [32.683822104027065, -117.23883957999418],
    entity: 'naval base point loma',
    title: 'Naval Base Point Loma',
    controller: 'Navy Region Southwest',
    addressLines: [
      '140 Sylvester Road',
      'San Diego, CA 92106-3251',
      'Phone: (619) 553-0090',
    ],
  },
  'Naval Base San Diego': {
    latLong: [32.68480662012992, -117.12995117580577],
    entity: 'naval base san diego',
    title: 'Naval Base San Diego',
    controller: 'Navy Region Southwest',
    addressLines: [
      '3455 Senn Road, Bldg 72',
      'San Diego, CA 92136-5084',
    ],
  },
  'Naval Base Subic Bay': {
    latLong: [14.82448215856446, 120.21119343731895],
    entity: 'naval base subic bay',
    title: 'Naval Base Subic Bay',
    controller: 'Philippines Navy',
    addressLines: [
      'Subic Bay Freeport Zone', 'Zambales, Philippines'
    ],
  },
  'Naval Base Ventura County': {
    latLong: [34.11274074937068, -119.10555157332153],
    entity: 'naval base ventura county',
    title: 'Naval Base Ventura County',
    controller: 'Navy Region Southwest',
    addressLines: [
      '311 Main Road Bldg. 1',
      'Point Mugu, CA 93042-5033',
      'Phone: (805) 982-4711',
    ],
  },
  'Naval Comm. Station Harold E. Holt': {
    latLong: [-21.81631941603769, 114.16563793084563],
    entity: 'naval communication station harold e. holt',
    title: 'Naval Comm. Station Harold E. Holt',
    addressLines: ['North West Cape WA 6707, Australia'],
  },
  'Naval Construction Battalion Center Gulfport': {
    latLong: [30.373782454232124, -89.12112196992898],
    entity: 'naval construction battalion center gulfport',
    title: 'Naval Construction Battalion Center Gulfport',
    controller: 'Navy Region Southeast',
    addressLines: [
      '4902 Marvin Shields Blvd',
      'Gulfport, MS 39501',
      'Phone: (228) 871-2555',
    ],
  },
  'Naval Magazine Indian Island': {
    latLong: [48.03363121068213, -122.72878301339018],
    entity: 'naval magazine indian island',
    title: 'Naval Magazine Indian Island',
    controller: 'Navy Region Northwest',
    addressLines: [
      '100 Indian Island Road',
      'Port Hadlock, WA 98339',
      'Phone: (360) 867-8525',
    ],
  },
  'Naval Operations Support Center Rock Island': {
    latLong: [41.5172792134375, -90.54422433397525],
    entity: 'rock island arsenal',
    title: 'NAVOPTSCEN Rock Island (Rock Island Arsenal)',
    controller: 'IMCOM',
    addressLines: [
      'Rock Island, IL 61201'
    ]
  },
  'Naval Postgraduate School': {
    latLong: [36.59693392687027, -121.87413430435709],
    entity: 'naval postgraduate school',
    title: 'Naval Postgraduate School',
    addressLines: ['1 University Circle', 'Monterey, CA 93943'],
  },
  'Naval Station Everett': {
    latLong: [47.989953831270135, -122.21851711766638],
    entity: 'naval station everett',
    title: 'Naval Station Everett',
    controller: 'Navy Region Northwest',
    addressLines: [
      '2000 West Marine View Drive',
      'Everett, WA  98207-5001',
      'Phone: (425) 304-3366',
    ],
  },
  'Naval Station Great Lakes': {
    latLong: [42.31208697277338, -87.8348136358936],
    entity: 'naval station great lakes',
    title: 'Naval Station Great Lakes',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '2601E Paul Jones Street',
      'Great Lakes, IL 60088',
    ],
  },
  'Naval Station Guantanamo Bay': {
    latLong: [19.903080686504712, -75.09663003350005],
    entity: 'naval station guantanamo bay',
    title: 'Naval Station Guantanamo Bay',
    controller: 'Navy Region Southeast',
    addressLines: [
      'PSC 1005 Box 25',
      'FPO, AA 34009',
      'Phone: (757) 458-4502',
    ],
  },
  'Naval Station Mayport': {
    latLong: [30.37741180528287, -81.41030843853238],
    entity: 'naval station mayport',
    title: 'Naval Station Mayport',
    controller: 'Navy Region Southeast',
    addressLines: [
      'Box 280032',
      'Mayport, FL 32228',
      'Phone: (904) 270-5226',
    ],
  },
  'Naval Station Newport': {
    latLong: [41.51683269596761, -71.31961606426293],
    entity: 'naval station newport',
    title: 'Naval Station Newport',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '690 Peary Street',
      'Newport, Rhode Island 02841',
      'Phone: (401) 841-3456',
    ],
  },
  'Naval Station Norfolk': {
    latLong: [36.948138749763075, -76.30905655155466],
    entity: 'naval station norfolk',
    title: 'Naval Station Norfolk',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '1530 Gilbert Street',
      'Suite 2000',
      'Norfolk, VA 23511',
      'Phone: (757) 322-2576',
    ],
  },
  'Naval Station Pearl Harbor': {
    latLong: [21.361232067732335, -157.94376275604313],
    entity: 'joint base pearl harbor-hickam',
    title: 'Naval Station Pearl Harbor',
    controller: 'Navy Region Hawaii',
    addressLines: [
      '850 Ticonderoga St',
      'JBPHH, HI 96860-5101',
    ],
  },
  'Naval Station Rota': {
    latLong: [36.63738714699194, -6.346561816566716],
    entity: 'naval station rota',
    title: 'Naval Station Rota',
    controller: 'Navy Region Europe, Africa, Central',
    addressLines: [
      'PSC 819 Box 1',
      'Rota, Spain 09645',
    ],
  },
  'Naval Submarine Base Kings Bay': {
    latLong: [30.785523966572452, -81.56896216888858],
    entity: 'naval submarine base kings bay',
    title: 'Naval Submarine Base Kings Bay',
    controller: 'Navy Region Southeast',
    addressLines: [
      '1063 USS Tennessee Avenue',
      'Kings Bay, GA 31547',
      'Phone: (912) 573-4718',
    ],
  },
  'Naval Submarine Base New London': {
    latLong: [41.388450756094976, -72.0860038532098],
    entity: 'naval submarine base new london',
    title: 'Naval Submarine Base New London',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      'Box 00',
      'Groton, CT 06349-5000',
      'Phone: (860) 694-5980',
    ],
  },
  'Naval Support Activity Annapolis': {
    latLong: [38.98621154200764, -76.46581859237796],
    entity: 'naval support activity annapolis',
    title: 'Naval Support Activity Annapolis',
    controller: 'Naval District Washington',
    addressLines: [
      '58 Bennion Road',
      'Annapolis, MD 21402',
      'Phone: (410) 293-1000',
    ],
  },
  'Naval Support Activity Bethesda': {
    latLong: [39.00402012266021, -77.09637130912363],
    entity: 'naval support activity bethesda',
    title: 'Naval Support Activity Bethesda',
    controller: 'Naval District Washington',
    addressLines: [
      '102 Wood Road',
      'Bethesda, Maryland 20889',
      'Phone: (301) 295-6246',
    ],
  },
  'Naval Support Activity Bahrain': {
    latLong: [26.209012468491288, 50.6114683240537],
    entity: 'naval support activity bahrain',
    title: 'Naval Support Activity Bahrain',
    controller: 'Navy Region Europe, Africa, Central',
    addressLines: [
      'PSC 851 Box 10',
      'FPO AE, 09834-2800',
    ],
  },
  'Naval Support Activity Crane': {
    latLong: [38.859690210910614, -86.84962499831694],
    entity: 'naval support activity crane',
    title: 'Naval Support Activity Crane',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '300 Hwy 361 Code N1',
      'Bldg 3219',
      'Crane, IN 47522',
      'Phone: (812) 854-3965',
    ],
  },
  'Naval Support Activity Hampton Roads': {
    latLong: [36.92280123090542, -76.30162932605131],
    entity: 'naval support activity hampton roads',
    title: 'Naval Support Activity Hampton Roads',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '7918 Blandy Rd',
      'Suite 100',
      'Norfolk, VA 23551-2419',
      'Phone: (757) 836-1484',
    ],
  },
  'Naval Support Activity Lakehurst': {
    latLong: [40.03786727251134, -74.58847557429723],
    entity: 'naval support activity lakehurst',
    title: 'Naval Support Activity Lakehurst (JB MDL)',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      'County Rd 547',
      'Lakehurst, NJ 08753',
      'Phone: (732) 323-1439/2693',
    ],
  },
  'Naval Support Activity Mechanicsburg': {
    latLong: [40.228421844670834, -76.99174287730858],
    entity: 'naval support activity mechanicsburg',
    title: 'Naval Support Activity Mechanicsburg',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '5450 Carlisle Pike',
      'Bldg. 306C',
      'Mechanicsburg, PA 17055',
    ],
  },
  'Naval Support Activity Mid-South': {
    latLong: [35.33602659596654, -89.86847079977365],
    entity: 'naval support activity mid-south',
    title: 'Naval Support Activity Mid-South',
    controller: 'Navy Region Southeast',
    addressLines: [
      '5722 Integrity Drive',
      'Millington, TN 38054',
      'Phone: (901) 874-7421',
    ],
  },
  'Naval Support Activity Monterey': {
    latLong: [36.596619221813135, -121.87421229562],
    entity: 'naval support activity monterey',
    title: 'Naval Support Activity Monterey',
    controller: 'Navy Region Southwest',
    addressLines: [
      '271 Stone Road',
      'Monterey, CA 93943-5000',
      'Phone: (831) 656-6233',
    ],
  },
  'Naval Support Activity Naples': {
    latLong: [40.98984561686801, 14.24834255262787],
    entity: 'naval support activity naples',
    title: 'Naval Support Activity Naples',
    controller: 'Navy Region Europe, Africa, Central',
    addressLines: [
      'PSC 817 Box 1',
      'FPO AE ',
      'Naples, Italy 09622-0001',
    ],
  },
  'Naval Support Activity Orlando': {
    latLong: [28.584396248993187, -81.19749912305194],
    entity: 'naval support activity orlando',
    title: 'Naval Support Activity Orlando',
    controller: 'Navy Region Southeast',
    addressLines: [
      '12350 Research Parkway',
      'Orlando, FL 32826',
      'Phone: (407) 380-8372',
    ],
  },
  'Naval Support Activity Panama City': {
    latLong: [30.179282113505568, -85.75973875512473],
    entity: 'naval support activity panama city',
    title: 'Naval Support Activity Panama City',
    controller: 'Navy Region Southeast',
    addressLines: [
      '101 Vernon Avenue',
      'Panama City Beach, FL 32407',
      'Phone: (850) 230-7717',
    ],
  },
  'Naval Support Activity Saratoga Springs': {
    latLong: [43.07811250369985, -73.81991030060938],
    entity: 'naval support activity saratoga springs',
    title: 'Naval Support Activity Saratoga Springs',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '19 J.F. King Drive',
      'Saratoga Springs, NY 12866',
      'Phone: (518) 886-0200',
    ],
  },
  'Naval Support Activity Souda Bay': {
    latLong: [35.517302918401725, 24.123870926633927],
    entity: 'naval support activity souda bay',
    title: 'Naval Support Activity Souda Bay',
    controller: 'Navy Region Europe, Africa, Central',
    addressLines: [
      'PSC 814 Box 1',
      'FPO AE, 09266',
    ],
  },
  'Naval Support Activity South Potomac': {
    latLong: [38.32080622738815, -77.03590326818465],
    entity: 'naval support activity south potomac',
    title: 'Naval Support Activity South Potomac',
    controller: 'Naval District Washington',
    addressLines: [
      '6509 Sampson Rd. Suite 217',
      'Dahlgren, VA  22448-5108',
      'Phone: (540) 653-8153',
    ],
  },
  'Naval Support Activity Washington': {
    latLong: [38.874481471178655, -76.99508422335927],
    entity: 'naval support activity washington',
    title: 'Naval Support Activity Washington',
    controller: 'Naval District Washington',
    addressLines: [
      '1411 Parsons Ave SE',
      'Suite 323',
      'Washington, DC 20374-5001',
    ],
  },
  'Naval Support Facility Beaufort': {
    latLong: [32.388055520300064, -80.68430719669392],
    entity: 'naval support facility beaufort',
    title: 'Naval Support Facility Beaufort',
    controller: 'Navy Region Southeast',
    addressLines: [
      '1 Pinckney Blvd',
      'Beaufort, SC 29902',
      'Phone: (843) 228-5200',
    ],
  },
  'Naval Support Facility Deveselu': {
    latLong: [44.077281021674025, 24.41421570461313],
    entity: 'naval support facility deveselu',
    title: 'Naval Support Facility Deveselu',
    controller: 'Navy Region Europe, Africa, Central',
    addressLines: [
      'NSF Deveselu Navy Exchange',
      'UM 01871, Deveselu 235200, Romania',
    ],
  },
  'Naval Support Facility Diego Garcia': {
    latLong: [-7.312619304492377, 72.412516859965],
    entity: 'naval support facility diego garcia',
    title: 'Naval Support Facility Diego Garcia',
    controller: 'Navy Region Japan',
    addressLines: ['PSC 466 Box 2', 'FPO AP 96595'],
  },
  'Naval Support Facility Redzikowo': {
    latLong: [54.4799219244104, 17.101165691686088],
    entity: 'naval support facility redzikowo',
    title: 'Naval Support Facility Redzikowo',
    controller: 'Navy Region Europe, Africa, Central',
    addressLines: [
      'PSC 826 Box 1',
      'FPO, AE 09761',
    ],
  },
  'Naval Support Facility Thurmont': {
    latLong: [39.647535338667524, -77.46589566140022],
    entity: 'naval support facility thurmont',
    title: 'Naval Support Facility Thurmont',
    controller: 'Naval District Washington',
    addressLines: [
      'P.O. Box 1000',
      'Thurmont, MD 21788-5001',
    ],
  },
  'Naval Weapons Station Earle': {
    latLong: [40.26496640237434, -74.16129805328714],
    entity: 'naval weapons station earle',
    title: 'Naval Weapons Station Earle',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '201 Route 34 South',
      'Colts Neck, NJ 07722',
      'Phone: (732) 866-2171',
    ],
  },
  'Naval Weapons Station Seal Beach': {
    latLong: [33.749767704430916, -118.08842348838313],
    entity: 'naval weapons station seal beach',
    title: 'Naval Weapons Station Seal Beach',
    controller: 'Navy Region Southwest',
    addressLines: [
      '800 Seal Beach Boulevard, B-204',
      'Seal Beach, California 90740',
      'Phone: (562) 626-7215',
    ],
  },
  'Naval Weapons Station Yorktown': {
    latLong: [37.235339184490634, -76.54879350905905],
    entity: 'naval weapons station yorktown',
    title: 'Naval Weapons Station Yorktown',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '160 Main Rd',
      'Yorktown, VA 23691-0160',
      'Phone: (757) 887-4939',
    ],
  },
  'Norfolk Naval Shipyard': {
    latLong: [36.81122201433893, -76.30142503135895],
    entity: 'norfolk naval shipyard',
    title: 'Norfolk Naval Shipyard',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      'Portsmouth, Virginia 23709-5000',
      'Phone: (757) 396-9550',
    ],
  },
  'Pacific Missile Range Facility Barking Sands': {
    latLong: [22.037356821367798, -159.75667237370897],
    entity: 'pacific missile range facility barking sands',
    title: 'Pacific Missile Range Facility Barking Sands',
    controller: 'Navy Region Hawaii',
    addressLines: ['PO Box 128', 'Kekaha, HI 96752'],
  },
  'Portsmouth Naval Shipyard': {
    latLong: [43.08033641201101, -70.73991745609591],
    entity: 'portsmouth naval shipyard',
    title: 'Portsmouth Naval Shipyard',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      'Kittery, ME 03904',
      'Phone: (207) 438-1000',
    ],
  },
  'Puget Sound Naval Shipyard': {
    latLong: [47.558346183329405, -122.64391165835127],
    entity: 'puget sound naval shipyard & intermediate maintenance facility',
    title: 'Puget Sound Naval Shipyard',
    controller: 'Naval Sea Systems Command',
    addressLines: [
      '1400 Farragut St.',
      'Bremerton, WA 98314',
    ],
  },
  'Singapore Area Coordinator': {
    latLong: [1.4624956615157605, 103.83129790867407],
    entity: 'singapore area coordinator',
    title: 'Singapore Area Coordinator',
    controller: 'Navy Region Japan',
    addressLines: [
      'PSA Sembawang Terminal',
      'Building 7-4 Deptford Road',
      'Singapore 759657',
    ],
  },
  'Surface Combat Systems Center Wallops Island': {
    latLong: [37.93220913975689, -75.47941500739374],
    entity: 'surface combat systems center wallops island',
    title: 'Surface Combat Systems Center Wallops Island',
    controller: 'Navy Region Mid-Atlantic',
    addressLines: [
      '30 Battle Group Way',
      'Wallops Island, VA 23337-5000',
      'Phone: (757) 824-1692',
    ],
  },
}
