import * as React from 'react'

export const data = {
  'Big Bend': {
    'Alpine Station': {
      latLong: [30.350463033131376, -103.70206314638125],
      entity: 'big bend border patrol sector',
      title: 'Border Patrol Alpine Station',
      addressLines: [
        '3003 West Highway 90',
        'Alpine, Texas 79830',
        'Phone: (432) 837-6100',
      ],
    },
    'Fort Stockton Station': {
      latLong: [30.90208736333613, -102.88489407468765],
      entity: 'big bend border patrol sector',
      title: 'Border Patrol Fort Stockton Station',
      addressLines: [
        '1801 Front Street',
        'Post Office Box 607',
        'Fort Stockton, Texas 79735',
        'Phone: (432) 336-2468',
      ],
    },
    'Marfa Station': {
      latLong: [30.304641600726228, -104.01888595303373],
      entity: 'big bend border patrol sector',
      title: 'Border Patrol Marfa Station',
      addressLines: [
        '717 South Highland',
        'Post Office Box "I"',
        'Marfa, Texas 79843',
        'Phone: (432) 729-5600',
      ],
    },
    'Presidio Station': {
      latLong: [29.570722494467528, -104.37069211883191],
      entity: 'big bend border patrol sector',
      title: 'Border Patrol Presidio Station',
      addressLines: [
        'Highways 170 and 67',
        'Post Office Box 929',
        'Presidio, Texas 79845',
        'Phone: (432) 229-3330',
      ],
    },
    'Sanderson Station': {
      latLong: [30.148828972117155, -102.42296753289627],
      entity: 'big bend border patrol sector',
      title: 'Border Patrol Sanderson Station',
      addressLines: [
        'Highway 90 West',
        'Post Office Box 628',
        'Sanderson, Texas 79848',
        'Phone: (432) 345-2972',
      ],
    },
    'Sierra Blanca Station': {
      latLong: [31.17168676547951, -105.3374606893043],
      entity: 'big bend border patrol sector',
      title: 'Border Patrol Sierra Blanca Station',
      addressLines: [
        '900 Aztec Drive',
        'Post Office Box 8',
        'Sierra Blanca, Texas 79851',
        'Phone: (915) 369-4000',
      ],
    },
    'Van Horn Station': {
      latLong: [31.03466423966395, -104.84474444450485],
      entity: 'big bend border patrol sector',
      title: 'Border Patrol Van Horn Station',
      addressLines: [
        '500 Laurel Street',
        'Post Office Box 368',
        'Van Horn, Texas 79855',
        'Phone: (432) 283-3100',
      ],
    },
  },
  Blaine: {
    'Bellingham Station': {
      latLong: [48.821580800442696, -122.55927665318184],
      entity: 'blaine border patrol sector',
      title: 'Border Patrol Bellingham Station',
      addressLines: [
        '1431 Sunset Avenue',
        'Ferndale, WA 98248-8914',
        'Phone: (360) 380-0408',
      ],
    },
    'Blaine Station': {
      latLong: [48.99437109984104, -122.73139510608709],
      entity: 'blaine border patrol sector',
      title: 'Border Patrol Blaine Station',
      addressLines: [
        '1580 H Street',
        'Blaine, WA 98230-9114',
        'Phone:(360) 332-1721',
      ],
    },
    'Port Angeles Station': {
      latLong: [48.10694044665338, -123.40574540735521],
      entity: 'blaine border patrol sector',
      title: 'Border Patrol Port Angeles Station',
      addressLines: [
        '110 South Penn Street',
        'Port Angeles, WA 98362',
        'Phone: (360) 565-7300',
      ],
    },
    'Sumas Station': {
      latLong: [48.98996685331573, -122.28435196085104],
      entity: 'blaine border patrol sector',
      title: 'Border Patrol Sumas Station',
      addressLines: [
        '9648 Garrison Road',
        'Sumas, WA 98295',
        'Phone: (360) 988-5520',
      ],
    },
  },
  Buffalo: {
    'Buffalo Station': {
      latLong: [42.99464436365909, -78.85987415561932],
      entity: 'buffalo border patrol sector',
      title: 'Border Patrol Buffalo Station',
      addressLines: [
        '600 Colvin Woods Parkway',
        'Tonawanda, NY 14150',
        'Phone: (716) 862-7000',
      ],
    },
    'Erie Station': {
      latLong: [42.01182132564199, -80.22456003918923],
      entity: 'buffalo border patrol sector',
      title: 'Border Patrol Erie Station',
      addressLines: [
        '7851 Traut Drive',
        'Fairview, PA 16415',
        'Phone: (814) 474-4700',
      ],
    },
    'Niagara Falls Station': {
      latLong: [43.130747993592095, -79.0412696341514],
      entity: 'buffalo border patrol sector',
      title: 'Border Patrol Niagara Falls Station',
      addressLines: [
        '1708 Lafayette Avenue',
        'Niagara Falls, NY 14305',
        'Phone: (716) 285-6444',
      ],
    },
    'Oswego Station': {
      latLong: [43.46258259916383, -76.5075002956615],
      entity: 'buffalo border patrol sector',
      title: 'Border Patrol Oswego Station',
      addressLines: [
        '19 East Schuyler Street',
        'Oswego, NY 13126',
        'Phone: (315) 342-7017',
      ],
    },
    'Rochester Station': {
      latLong: [43.24810523598395, -77.60489541757184],
      entity: 'buffalo border patrol sector',
      title: 'Border Patrol Rochester Station',
      addressLines: [
        '171 Pattonwood Drive',
        'Rochester, NY 14617',
        'Phone: (585) 266-2802',
      ],
    },
    'Wellesley Island Station': {
      latLong: [44.33027654190712, -75.93527886436212],
      entity: 'buffalo border patrol sector',
      title: 'Border Patrol Wellesley Island Station',
      addressLines: [
        '45764 Landon Road',
        'Wellesley Island, NY 13640',
        'Phone: (315) 482-7556',
      ],
    },
  },
  'Del Rio': {
    'Abilene Station': {
      latLong: [32.40128264468131, -99.7464874316743],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Abilene Station',
      addressLines: [
        '1945 Industrial Blvd',
        'Abilene, Texas 79602',
        'Phone: (325) 698-5552',
      ],
    },
    'Brackettville Station': {
      latLong: [29.315269424130786, -100.42626280408993],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Brackettville Station',
      addressLines: [
        '802 W. Spring St.',
        'Brackettville, TX 78832',
        'Phone: (830) 563-6000',
      ],
    },
    'Carrizo Springs Station': {
      latLong: [28.532976750304062, -99.82537446052727],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Carrizo Springs Station',
      addressLines: [
        '1868 Hwy 85 East',
        'Carrizo Springs, Texas 78834',
        'Phone: (830) 876-1453',
      ],
    },
    'Comstock Station': {
      latLong: [29.642021747655473, -101.13467730407842],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Comstock Station',
      addressLines: [
        '27685 Highway 90 West',
        'Comstock, Texas 78837',
        'Phone: (432) 292-4600',
      ],
    },
    'Del Rio Station': {
      latLong: [29.371239022504326, -100.87085764641613],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Del Rio Station',
      addressLines: [
        '2300 Highway 90 East',
        'Del Rio, Texas 78840',
        'Phone: (830) 778-3000',
      ],
    },
    'Eagle Pass Station': {
      latLong: [28.735261366565574, -100.4971340932715],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Eagle Pass Station',
      addressLines: [
        '2285 Del Rio Blvd.',
        'Eagle Pass, TX 78852',
        'Phone: (830) 758-4000',
      ],
    },
    'Eagle Pass South Station': {
      latLong: [28.67183818419802, -100.45988725259487],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Eagle Pass South Station',
      addressLines: [
        '4156 El Indio Highway',
        'Eagle Pass, TX 78852',
        'Phone: (830) 752-3300',
      ],
    },
    'Rocksprings Station': {
      latLong: [30.01593728960274, -100.21348146153727],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Rocksprings Station',
      addressLines: [
        '605 W. Main Street',
        'P.O. Box 576',
        'Rocksprings, Texas 78880',
        'Phone: (830) 683-2255',
      ],
    },
    'San Angelo Station': {
      latLong: [31.36511611822438, -100.50209436168761],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol San Angelo Station',
      addressLines: [
        '8210 Hangar Road',
        'San Angelo, TX 76904',
        'Phone: (325) 949-0139',
      ],
    },
    'Uvalde Station': {
      latLong: [29.213745239439117, -99.7518004464216],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Uvalde Station',
      addressLines: [
        '#30 Industrial Park',
        'Uvalde, Texas 78801',
        'Phone: (830) 486-7600',
      ],
    },
  },
  Detroit: {
    Detroit: {
      latLong: [42.367538371077316, -82.96635743236911],
      entity: 'detroit border patrol sector',
      title: 'Border Patrol Detroit Station',
      addressLines: [
        '11700 E. Jefferson Avenue',
        'Detroit, MI 48214',
        'P.O. Box 44975',
        'Phone: (313) 926-4700',
      ],
    },
    Gibraltar: {
      latLong: [42.09375186456822, -83.20166414090009],
      entity: 'detroit border patrol sector',
      title: 'Border Patrol Gibraltar Station',
      addressLines: [
        '14801 Middle Gibraltar Rd.',
        'Gibraltar, MI 48173',
        'Phone: (734) 676-2972',
      ],
    },
    Marysville: {
      latLong: [42.881511409345805, -82.48568860350737],
      entity: 'detroit border patrol sector',
      title: 'Border Patrol Marysville Station',
      addressLines: [
        '2600 Wills Road',
        'Marysville, MI 48040',
        'Phone: (810) 989-5056',
      ],
    },
    'Sandusky Bay': {
      latLong: [41.512509540154326, -82.85818661275187],
      entity: 'detroit border patrol sector',
      title: 'Border Patrol Sandusky Bay Station',
      addressLines: [
        '709 SE Catawba Road',
        'Port Clinton, OH 43452',
        'Phone: (419) 732-4400',
      ],
    },
    'Sault Sainte Marie': {
      latLong: [46.49936131845848, -84.34455713935773],
      entity: 'detroit border patrol sector',
      title: 'Border Patrol Sault Sainte Marie Station',
      addressLines: [
        '208 Bingham Ave',
        'Sault Ste. Marie, MI 49783',
        'Phone: (906) 632-3383',
      ],
    },
  },
  'El Centro': {
    'Calexico Station': {
      latLong: [32.678513294867294, -115.48003912686582],
      entity: 'el centro border patrol sector',
      title: 'Border Patrol Calexico Station',
      addressLines: [
        '1150 Birch Street',
        'Calexico, CA 92231',
        'Phone: (760) 768-7000',
      ],
    },
    'El Centro Station': {
      latLong: [32.8248495081239, -115.55408251760934],
      entity: 'el centro border patrol sector',
      title: 'Border Patrol El Centro Station',
      addressLines: [
        '221 West Aten Road',
        'Imperial, CA 92251',
        'Phone: (760) 337-7100',
      ],
    },
    'Indio Station': {
      latLong: [33.71648152411393, -116.20311655259992],
      entity: 'el centro border patrol sector',
      title: 'Border Patrol Indio Station',
      addressLines: [
        '45-620 Commerce Street',
        'Indio, CA 92201',
        'Phone: (760) 347-3658',
      ],
    },
  },
  'El Paso': {
    'Alamogordo Station': {
      latLong: [32.865355885189864, -105.96808434628637],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Alamogordo Station',
      addressLines: [
        '1997 Highway 54 South',
        'Alamogordo, NM 88310-7377',
        'Phone: (575) 812-6840',
      ],
    },
    'Clint Station': {
      latLong: [31.58064477171607, -106.22905193481907],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Clint Station',
      addressLines: [
        '13400 Alameda Ave',
        'Clint, Texas 79836',
        'Phone: (915) 872-4100',
      ],
    },
    'Deming Station': {
      latLong: [32.244120628978564, -107.72396655353444],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Deming Station',
      addressLines: [
        '3300 J Street',
        'Deming, NM 88030-7146',
        'Phone: (575) 544-6100',
      ],
    },
    'El Paso Station': {
      latLong: [31.872610007518823, -106.44269297516068],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol El Paso Station',
      addressLines: [
        '9201 Gateway South Boulevard',
        'El Paso, TX 79924',
        'Phone: (915) 585-1924',
      ],
    },
    'Fort Hancock Station': {
      latLong: [31.27646526217969, -105.85194446717603],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Fort Hancock Station',
      addressLines: [
        'Fort Hancock Station',
        '828 South HWY 1088',
        'Fort Hancock, TX 79839',
        'Phone: (915) 769-1700',
      ],
    },
    'Las Cruces Station': {
      latLong: [32.34204279515303, -106.77302748313735],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Las Cruces Station',
      addressLines: [
        'Las Cruces Station',
        '3120 North Main',
        'Las Cruces, NM 88001-1162',
        'Phone: (575) 528-6600',
      ],
    },
    'Lordsburg Station': {
      latLong: [32.348216979048104, -108.7453300980536],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Lordsburg Station',
      addressLines: [
        '26 Pipeline Road',
        'Lordsburg, NM 88045-1231',
        'Phone: (575) 542-6000',
      ],
    },
    'Santa Teresa Station': {
      latLong: [31.820702890292985, -106.7004970616704],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Santa Teresa Station',
      addressLines: [
        '1005 NM Highway 9',
        'Santa Teresa, NM 88008',
        'Phone: (575) 874-6800',
      ],
    },
    'Truth or Consequences Station': {
      latLong: [33.1973839258252, -107.25289125381214],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Truth or Consequences Station',
      addressLines: [
        'Truth or Consequences Station',
        'Mile Post 82, I-25 North / Box 3310',
        'Truth or Consequences, New Mexico 87901',
        'Phone: (575) 744-5235',
      ],
    },
    'Ysleta Station': {
      latLong: [31.711361469052605, -106.28485574071837],
      entity: 'del rio border patrol sector',
      title: 'Border Patrol Ysleta Station',
      addressLines: [
        'Ysleta Station',
        '12245 Pine Springs Drive',
        'El Paso, TX 79936-7828',
        'Phone: (915) 435-1100',
      ],
    },
  },
  'Grand Forks': {
    Bottineau: {
      latLong: [48.821234391118416, -100.43824120318892],
      entity: 'grand forks border patrol sector',
      title: 'Border Patrol Bottineau Station',
      addressLines: [
        '1235 11th Street East',
        'P.O. Box 6',
        'Bottineau, ND 58318',
        'Phone: (701) 228-3179',
      ],
    },
    Duluth: {
      latLong: [46.83439339011033, -92.1560733321353],
      entity: 'grand forks border patrol sector',
      title: 'Border Patrol Duluth Station',
      addressLines: [
        '4431 Endeavor Drive',
        'Duluth, MN 55811',
        'Phone: (218) 720-5465',
      ],
    },
    'Grand Marais': {
      latLong: [47.75402542426602, -90.32487479041794],
      entity: 'grand forks border patrol sector',
      title: 'Border Patrol Grand Marais Station',
      addressLines: [
        '518 E. Hwy 61',
        'Grand Marais, MN 55604',
        'Phone: (218) 387-1770',
      ],
    },
    'International Falls': {
      latLong: [48.60236591677625, -93.3882244692414],
      entity: 'grand forks border patrol sector',
      title: 'Border Patrol International Falls Station',
      addressLines: [
        '312 Highway 11 East',
        'International Falls, MN 56649',
        'Phone: (218) 285-3423',
      ],
    },
    Pembina: {
      latLong: [48.9628809227097, -97.25231611003815],
      entity: 'grand forks border patrol sector',
      title: 'Border Patrol Pembina Station',
      addressLines: [
        '388 Division Street',
        'Pembina, ND 58271',
        'Phone: (701) 825-6640',
      ],
    },
    Portal: {
      latLong: [48.998603549132184, -102.55513510317888],
      entity: 'grand forks border patrol sector',
      title: 'Border Patrol Portal Station',
      addressLines: [
        '500 2nd Avenue East',
        'Portal, ND 58772',
        'Phone: (701) 926-7570',
      ],
    },
    Warroad: {
      latLong: [48.8986714487898, -95.31946061667684],
      entity: 'grand forks border patrol sector',
      title: 'Border Patrol Warroad Station',
      addressLines: [
        '502 State Avenue South, Highway 11',
        'PO Box 24',
        'Warroad, MN 56763',
        'Phone: (218) 386-1802',
      ],
    },
  },
  Havre: {
    Havre: {
      latLong: [48.5500512669392, -109.70831191060962],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol Havre Station',
      addressLines: [
        '345 16th Avenue W',
        'PO Box 1629',
        'Havre, MT 59501',
        'Phone: (406) 395-6100',
      ],
    },
    Malta: {
      latLong: [48.36301508449287, -107.88048414554294],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol Malta Station',
      addressLines: [
        '47152 U.S. Highway 2',
        'P.O. Box 36',
        'Malta, MT 59538',
        'Phone: (406) 654-2711/2944',
      ],
    },
    Plentywood: {
      latLong: [48.782112540698094, -104.5709662571603],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol Plentywood Station',
      addressLines: [
        '31 Highway 16 N',
        'P.O. Box 434',
        'Plentywood, MT 59254',
        'Phone: (406) 765-1852',
      ],
    },
    Scobey: {
      latLong: [48.99840015946226, -105.4088483896866],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol Scobey Station',
      addressLines: [
        '131 C HWY 5 E',
        'P.O. Box 820',
        'Scobey, MT 59263',
        'Phone: (406) 487-2621',
      ],
    },
    'St. Mary': {
      latLong: [48.99696590764108, -113.3804050997401],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol St. Mary Station',
      addressLines: [
        '4999 US Highway 89',
        'Port of Piegan',
        'Babb, MT 59411',
        'Phone: (406) 732-5982',
      ],
    },
    Sweetgrass: {
      latLong: [48.88566262611497, -111.89429387434937],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol Sweetgrass Station',
      addressLines: [
        'P.O. Box 100',
        '37 Nine Mile Road',
        'Sunburst, MT 59482',
        'Phone: (406) 937-8800',
      ],
    },
  },
  Houlton: {
    Calais: {
      latLong: [45.11783395422017, -67.34841421873531],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol Calais Station',
      addressLines: [
        '67 Park Road',
        'Baring, Maine 04694',
        'Phone: (207) 454-3613',
      ],
    },
    'Fort Fairfield': {
      latLong: [46.783652450776685, -67.81921138742574],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol Fort Fairfield Station',
      addressLines: [
        '200 Limestone Road',
        'Fort Fairfield, Maine 04742',
        'Phone: (207) 472-5041',
      ],
    },
    Houlton: {
      latLong: [46.165888153993045, -67.84008724566391],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol Houlton Station',
      addressLines: [
        '591 North Street',
        'Houlton, Maine 04730',
        'Phone: (207) 532-9061',
      ],
    },
    Jackman: {
      latLong: [45.631710599010155, -70.19424674476602],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol Jackman Station',
      addressLines: [
        '229 Long Pond Road',
        'Jackman, Maine 04945',
        'Phone: (207) 668-3151',
      ],
    },
    Rangeley: {
      latLong: [44.978158796246795, -70.62694708805533],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol Rangeley Station',
      addressLines: [
        '224 Stratton Road',
        'Dallas Plantation, Maine 04970',
        'Phone: (207) 864-5356',
      ],
    },
    'Van Buren': {
      latLong: [47.15089415647728, -67.9296956744462],
      entity: 'houlton border patrol sector',
      title: 'Border Patrol Van Buren Station',
      addressLines: [
        '9 Main Street',
        'Van Buren, Maine 04785',
        'Phone: (207) 868-3900',
      ],
    },
  },
  Laredo: {
    'Cotulla Station': {
      latLong: [28.487704524575488, -99.22579001761072],
      entity: 'laredo border patrol sector',
      title: 'Border Patrol Cotulla Station',
      addressLines: [
        '3423 Interstate Highway 35',
        'Cotulla, Texas 78014',
        'Phone: (210) 242-1600',
      ],
    },
    'Dallas Station': {
      latLong: [32.81509534063035, -97.13066907512435],
      entity: 'laredo border patrol sector',
      title: 'Border Patrol Dallas Station',
      addressLines: [
        '2800 South Pipeline Road',
        'Euless, Texas 76040',
        'Phone: (817) 571-2146',
      ],
    },
    'Freer Station': {
      latLong: [27.880529526440856, -98.58039700413876],
      entity: 'laredo border patrol sector',
      title: 'Border Patrol Freer Station',
      addressLines: [
        '5087 Highway 44',
        'Freer, Texas 78357',
        'Phone: (361) 698-5400',
      ],
    },
    'Hebbronville Station': {
      latLong: [27.313172403851773, -98.66826321640227],
      entity: 'laredo border patrol sector',
      title: 'Border Patrol Hebbronville Station',
      addressLines: [
        '34 East Highway 359',
        'Hebbronville, Texas 78361',
        'Phone: (361) 886-1700',
      ],
    },
    'Laredo North Station': {
      latLong: [27.614736102766862, -99.47704574647571],
      entity: 'laredo border patrol sector',
      title: 'Border Patrol Laredo North Station',
      addressLines: [
        '11119 McPherson Road',
        'Laredo, Texas 78045',
        'Phone: (956) 764-3800',
      ],
    },
    'Laredo South Station': {
      latLong: [27.58780343911433, -99.49551126182025],
      entity: 'laredo border patrol sector',
      title: 'Border Patrol Laredo South Station',
      addressLines: [
        '9001 San Dario Avenue',
        'Laredo, Texas 78045',
        'Phone: (956) 764-3600',
      ],
    },
    'Laredo West Station': {
      latLong: [27.701629472504845, -99.74248217530875],
      entity: 'laredo border patrol sector',
      title: 'Border Patrol Laredo West Station',
      addressLines: [
        'Colombia Port of Entry',
        '202 State Highway 255',
        'Laredo, Texas 78045',
        'Phone: (956) 417-2100',
      ],
    },
    'San Antonio Station': {
      latLong: [29.480284451153846, -98.59755810590707],
      entity: 'laredo border patrol sector',
      title: 'Border Patrol San Antonio Station',
      addressLines: [
        '5000 N.W. Industrial Drive',
        'San Antonio, Texas 78238',
        'Phone: (210) 521-7926',
      ],
    },
    'Zapata Station': {
      latLong: [26.895621781532025, -99.26516547533487],
      entity: 'laredo border patrol sector',
      title: 'Border Patrol Zapata Station',
      addressLines: [
        '105 Kennedy Street',
        'Zapata, Texas 78076',
        'Phone: (956) 519-5600',
      ],
    },
  },
  Miami: {
    'Dania Beach Station': {
      latLong: [26.0720748169822, -80.13094062271645],
      entity: 'miami border patrol sector',
      title: 'Border Patrol Dania Beach Station',
      addressLines: [
        '1800 NE 7th Avenue',
        'Dania Beach, Florida 33004',
        'Phone: (954) 496-8300',
      ],
    },
    'Jacksonville Station': {
      latLong: [30.427147083619104, -81.65240932978811],
      entity: 'miami border patrol sector',
      title: 'Border Patrol Jacksonville Station',
      addressLines: [
        '489 Dundas Drive',
        'Jacksonville, Florida 32218',
        'Phone: (904) 714-0225',
      ],
    },
    'Marathon Station': {
      latLong: [24.712965433415647, -81.09015846627973],
      entity: 'miami border patrol sector',
      title: 'Border Patrol Marathon Station',
      addressLines: [
        '3770 Oversees Highway',
        'Fat Deer Key',
        'Marathon, Florida 33050',
        'Phone: (305) 289-0942',
      ],
    },
    'Orlando Station': {
      latLong: [28.41233147485261, -81.35309249864731],
      entity: 'miami border patrol sector',
      title: 'Border Patrol Orlando Station',
      addressLines: [
        '2348 Trade Port Drive',
        'Orlando, Florida 32824',
        'Phone: (407) 967-4000',
      ],
    },
    'Tampa Station': {
      latLong: [27.979449804591734, -82.37849691822908],
      entity: 'miami border patrol sector',
      title: 'Border Patrol Tampa Station',
      addressLines: [
        '3811 Corporex Park Drive',
        'Tampa, Florida 33619',
        'Phone: (813) 623-5101',
      ],
    },
    'West Palm Beach Station': {
      latLong: [26.78877926243, -80.0537673484781],
      entity: 'miami border patrol sector',
      title: 'Border Patrol West Palm Beach Station',
      addressLines: [
        '3301 Lake Shore Drive',
        'Riviera Beach, Florida 33404',
        'Phone: (561) 848-6161',
      ],
    },
  },
  'New Orleans': {
    'Baton Rouge Station': {
      latLong: [30.422143883566118, -91.05310056065771],
      entity: 'new orleans border patrol sector',
      title: 'Border Patrol Baton Rouge Station',
      addressLines: [
        '11655 Southfork Avenue',
        'Baton Rouge, LA 70816',
        'Phone: (225) 298-5501',
      ],
    },
    'Gulfport Station': {
      latLong: [30.437562024514996, -89.05640616155158],
      entity: 'new orleans border patrol sector',
      title: 'Border Patrol Gulfport Station',
      addressLines: [
        '10400 Larkin-Smith Drive',
        'Gulfport, MS 39503',
        'Phone: (228) 896-0884',
      ],
    },
    'Lake Charles Station': {
      latLong: [30.21581759811509, -93.24974996414994],
      entity: 'new orleans border patrol sector',
      title: 'Border Patrol Lake Charles Station',
      addressLines: [
        '152 Marine Street',
        'Lake Charles, LA 70601-5612',
        'Phone: (337) 721-3400',
      ],
    },
    'Mobile Station': {
      latLong: [30.599164539711385, -88.15665368760416],
      entity: 'new orleans border patrol sector',
      title: 'Border Patrol Mobile Station',
      addressLines: [
        '4425 Demetropolis Road',
        'Mobile, AL 36619',
        'Phone: (251) 660-1445',
      ],
    },
    'New Orleans Station': {
      latLong: [29.9525665338122, -90.0374179844306],
      entity: 'new orleans border patrol sector',
      title: 'Border Patrol New Orleans Station',
      addressLines: [
        '3819 Patterson Road',
        'New Orleans, LA 70114',
        'Phone: (504) 376-2830',
      ],
    },
  },
  'Rio Grande Valley': {
    'Brownsville Station': {
      latLong: [26.021253245878153, -97.50773400234685],
      entity: 'rio grande valley border patrol sector',
      title: 'Border Patrol Brownsville Station',
      addressLines: [
        '940 N. FM 511',
        'Olmito, Texas 78575',
        'Phone: (956) 983-1100',
      ],
    },
    'Corpus Christi Station': {
      latLong: [27.779514476049233, -97.54364567530614],
      entity: 'rio grande valley border patrol sector',
      title: 'Border Patrol Corpus Christi Station',
      addressLines: [
        '9149 State Highway 44',
        'Corpus Christi, Texas 78406',
        'Phone: (361) 879-4300',
      ],
    },
    'Falfurrias Station': {
      latLong: [27.205138176383564, -98.15646417532497],
      entity: 'rio grande valley border patrol sector',
      title: 'Border Patrol Falfurrias Station',
      addressLines: [
        '933 County Road 300',
        'Falfurrias, Texas 78355',
        'Phone: (361) 325-7000',
      ],
    },
    'Fort Brown Station': {
      latLong: [25.89181453930988, -97.47612387536631],
      entity: 'rio grande valley border patrol sector',
      title: 'Border Patrol Fort Brown Station',
      addressLines: [
        '3305 S. Expressway 83',
        'Brownsville, Texas 78521',
        'Phone: (956) 983-7100',
      ],
    },
    'Harlingen Station': {
      latLong: [26.16276526630758, -97.68769452535795],
      entity: 'rio grande valley border patrol sector',
      title: 'Border Patrol Harlingen Station',
      addressLines: [
        '3902 S. Expressway 77',
        'Harlingen, Texas 78552',
        'Phone: (956) 366-3000',
      ],
    },
    'Kingsville Station': {
      latLong: [27.49723630987911, -97.83834637531541],
      entity: 'rio grande valley border patrol sector',
      title: 'Border Patrol Kingsville Station',
      addressLines: [
        '2422 E. Senator Carlos Truan Blvd.',
        'Kingsville, Texas 78363',
        'Phone: (361) 595-8700',
      ],
    },
    'McAllen Station': {
      latLong: [26.152991377020378, -98.26522900419413],
      entity: 'rio grande valley border patrol sector',
      title: 'Border Patrol McAllen Station',
      addressLines: [
        '3000 West Military Highway',
        'McAllen, TX 78503',
        'Phone: (956) 217-3700',
      ],
    },
    'Rio Grande City Station': {
      latLong: [26.30837859448535, -98.65104563302523],
      entity: 'rio grande valley border patrol sector',
      title: 'Border Patrol Rio Grande City Station',
      addressLines: [
        '730 Border Patrol Lane',
        'Rio Grande City, Texas 78582',
        'Phone: (956) 487-1044',
      ],
    },
    'Weslaco Station': {
      latLong: [26.17238417876614, -97.97656027535761],
      entity: 'rio grande valley border patrol sector',
      title: 'Border Patrol Weslaco Station',
      addressLines: [
        '1501 E. Expressway 83',
        'Weslaco, Texas 78559',
        'Phone: (956) 647-8800',
      ],
    },
  },
  'San Diego': {
    'Boulevard Station': {
      latLong: [32.682421127448904, -116.29190808505665],
      entity: 'san diego border patrol sector',
      title: 'Border Patrol Boulevard Station',
      addressLines: [
        '2463 Ribbonwood Rd.',
        'Boulevard, CA 91905',
        'Phone: (619) 766-3000',
      ],
    },
    'Brown Field Station': {
      latLong: [32.54910960842828, -116.976875927284],
      entity: 'san diego border patrol sector',
      title: 'Border Patrol Brown Field Station',
      addressLines: [
        '7560 Britannia Ct.',
        'San Diego, California 92154',
        'Phone: (619) 671-1800',
      ],
    },
    'Campo Station': {
      latLong: [32.71934295301038, -116.44862953279981],
      entity: 'san diego border patrol sector',
      title: 'Border Patrol Campo Station',
      addressLines: [
        '32355 Old Highway 80',
        'Pine Valley, California 91962',
        'Phone: (619) 938-8700',
      ],
    },
    'Chula Vista Station': {
      latLong: [32.56520525244656, -117.05241520070628],
      entity: 'san diego border patrol sector',
      title: 'Border Patrol Chula Vista Station',
      addressLines: [
        'USBP Chula Vista Station',
        'P.O. BOX 210038',
        'Chula Vista, CA  91921-0038',
        'Phone: (619) 498-9700',
      ],
    },
    'El Cajon Station': {
      latLong: [32.828194871968435, -116.96390561559247],
      entity: 'san diego border patrol sector',
      title: 'Border Patrol El Cajon Station',
      addressLines: [
        '8404 N. Magnolia Avenue, Suite I',
        'Santee, California 92071',
        'Phone: (619) 596-6000',
      ],
    },
    'Imperial Beach Station': {
      latLong: [32.56636673766222, -117.10100870194205],
      entity: 'san diego border patrol sector',
      title: 'Border Patrol Imperial Beach Station',
      addressLines: [
        '1802 Saturn Blvd.',
        'San Diego, California 92154',
        'Phone: (619) 628-2900',
      ],
    },
    'Newton-Azrak Station': {
      latLong: [33.54575971910041, -117.18667133933425],
      entity: 'san diego border patrol sector',
      title: 'Border Patrol Newton-Azrak Station',
      addressLines: [
        '25762 Madison Avenue',
        'Murrieta, California 92562',
        'Phone: (951) 816-3000',
      ],
    },
    'San Clemente Station': {
      latLong: [33.35227580638089, -117.5240001462672],
      entity: 'san diego border patrol sector',
      title: 'Border Patrol San Clemente Station',
      addressLines: [
        'San Onofre - Weigh Scales',
        'Mile Marker 67 I-5',
        'San Clemente, California 92674',
        'Phone: (760) 430-7029',
      ],
    },
  },
  Spokane: {
    'Bonners Ferry': {
      latLong: [48.73054449935356, -116.30204440319406],
      entity: 'spokane border patrol sector',
      title: 'Border Patrol Bonners Ferry Station',
      addressLines: [
        '112 Moon Shadow Road',
        'Bonners Ferry, ID 83805',
        'Phone : (208) 267-2734',
      ],
    },
    Colville: {
      latLong: [48.56105097290935, -117.92472280320364],
      entity: 'spokane border patrol sector',
      title: 'Border Patrol Colville Station',
      addressLines: [
        '200 Buena Vista Drive',
        'Colville, WA 99114',
        'Phone : (509) 684-6272',
      ],
    },
    Curlew: {
      latLong: [48.879624178943196, -118.60127965730557],
      entity: 'spokane border patrol sector',
      title: 'Border Patrol Curlew Station',
      addressLines: [
        'P.O. Box 444',
        '#5 Forest Lane',
        'Curlew, WA 99118',
        'Phone: (509) 779-4376',
      ],
    },
    Eureka: {
      latLong: [48.99598614089212, -115.05802354497443],
      entity: 'spokane border patrol sector',
      title: 'Border Patrol Eureka Station',
      addressLines: [
        '7695 Airport Road',
        'Eureka, MT 59917',
        'Phone : (406) 889-9081',
      ],
    },
    'Metaline Falls': {
      latLong: [48.85085498702793, -117.39115447594699],
      entity: 'spokane border patrol sector',
      title: 'Border Patrol Metaline Falls Station',
      addressLines: [
        'P.O. Box 22',
        '105 Hwy 31',
        'Metaline, WA 99152',
        'Phone : (509) 446-1037',
      ],
    },
    Oroville: {
      latLong: [48.98671466104128, -119.4607394364046],
      entity: 'spokane border patrol sector',
      title: 'Border Patrol Oroville Station',
      addressLines: [
        'P.O. Box 99',
        '21 Shirley Road',
        'Oroville, WA 98844',
        'Phone : (509) 476-3622',
      ],
    },
    Whitefish: {
      latLong: [48.40984250658219, -114.3635598584649],
      entity: 'spokane border patrol sector',
      title: 'Border Patrol Whitefish Station',
      addressLines: [
        '1335 Hwy 93 West',
        'Whitefish, MT 59937',
        'Phone: (406) 862-2561',
      ],
    },
  },
  Swanton: {
    'Beecher Falls': {
      latLong: [45.00282545853701, -71.54891719184111],
      entity: 'swanton border patrol sector',
      title: 'Border Patrol Beecher Falls Station',
      addressLines: [
        '288 VT Route 114',
        'Canaan, VT 05903',
        'Phone: (802) 266-3035',
      ],
    },
    Burke: {
      latLong: [44.89085374286358, -74.23780791364855],
      entity: 'swanton border patrol sector',
      title: 'Border Patrol Burke Station',
      addressLines: [
        '4525 State Route 11',
        'Malone, NY 12953',
        'Phone: (518) 483-5941',
      ],
    },
    Champlain: {
      latLong: [44.958379189132046, -73.46025522918886],
      entity: 'swanton border patrol sector',
      title: 'Border Patrol Champlain Station',
      addressLines: [
        '1969 Ridge Road',
        'Champlain, NY 12919',
        'Phone: (518) 298-2531',
      ],
    },
    Massena: {
      latLong: [44.95489372429496, -74.82419435709298],
      entity: 'swanton border patrol sector',
      title: 'Border Patrol Massena Station',
      addressLines: [
        '135 Trippany Road',
        'Massena, New York 13662',
        'Phone: (315) 769-2342',
      ],
    },
    Newport: {
      latLong: [44.92509508552499, -72.18441495288654],
      entity: 'swanton border patrol sector',
      title: 'Border Patrol Newport Station',
      addressLines: [
        '373 Citizens Road',
        'Derby, VT 05829',
        'Phone: (802) 334-1333',
      ],
    },
    Ogdensburg: {
      latLong: [44.697254411769016, -75.49727721805375],
      entity: 'swanton border patrol sector',
      title: 'Border Patrol Ogdensburg Station',
      addressLines: [
        '127 North Water Street',
        'Ogdensburg, NY 13669',
        'Phone: (315) 393-1150',
      ],
    },
    Richford: {
      latLong: [44.975898603155855, -72.68706312040119],
      entity: 'swanton border patrol sector',
      title: 'Border Patrol Richford Station',
      addressLines: [
        '1668 St. Albans Road',
        'Richford, VT 05476',
        'Phone: (802) 848-3434',
      ],
    },
    Swanton: {
      latLong: [44.931623282392884, -73.08365222565438],
      entity: 'swanton border patrol sector',
      title: 'Border Patrol Swanton Station',
      addressLines: [
        '104 Raven Drive',
        'Swanton, VT 05488',
        'Phone: (802) 868-3220',
      ],
    },
  },
  Tucson: {
    'Ajo Station': {
      latLong: [32.27397049725815, -112.73972959718867],
      entity: 'tucson border patrol sector',
      title: 'Border Patrol Ajo Station',
      addressLines: [
        '850 North Highway 85',
        'Why, AZ 85321-9634',
        'Phone: (520) 387-7002',
      ],
    },
    'Brian A. Terry Station': {
      latLong: [31.37748312895231, -109.92875273399676],
      entity: 'tucson border patrol sector',
      title: 'Border Patrol Brian A. Terry Station',
      addressLines: [
        '2136 South Naco Highway',
        'Bisbee, AZ 85603',
        'Phone: (520) 432-5121',
      ],
    },
    'Case Grande Station': {
      latLong: [32.87677844308366, -111.69000456788103],
      entity: 'tucson border patrol sector',
      title: 'Border Patrol Case Grande Station',
      addressLines: [
        '396 Camino Mercado',
        'Casa Grande, AZ 85122',
        'Phone: (520) 836-7812',
      ],
    },
    'Douglas Station': {
      latLong: [31.352002890933857, -109.63187217322934],
      entity: 'tucson border patrol sector',
      title: 'Border Patrol Douglas Station',
      addressLines: [
        '1608 S. Kings Highway',
        'Douglas, AZ 85607',
        'Phone: (520) 805-6900',
      ],
    },
    'Nogales Station': {
      latLong: [31.354225836259577, -110.96571812887682],
      entity: 'tucson border patrol sector',
      title: 'Border Patrol Nogales Station',
      addressLines: [
        '1500 West La Quinta Road',
        'Nogales, AZ 85621',
        'Phone: (520) 761-2400',
      ],
    },
    'Sonoita Station': {
      latLong: [31.67874824692188, -110.65746410089147],
      entity: 'tucson border patrol sector',
      title: 'Border Patrol Sonoita Station',
      addressLines: [
        '3225 Highway 82',
        'Sonoita, AZ 85637',
        'Phone: (520) 455-5051',
      ],
    },
    'Three Points Station': {
      latLong: [32.07582063801897, -111.31707941748114],
      entity: 'tucson border patrol sector',
      title: 'Border Patrol Three Points Station',
      addressLines: [
        '16435 W. Ajo Way',
        'Tucson, Arizona 85736',
        'Phone: (520) 883-4302',
      ],
    },
    'Tucson Station': {
      latLong: [32.1939690314366, -110.89368935724882],
      entity: 'tucson border patrol sector',
      title: 'Border Patrol Tucson Station',
      addressLines: [
        '2430 S. Swan Road',
        'Tucson, AZ 85711',
        'Phone: (520) 514-4700',
      ],
    },
    'Willcox Station': {
      latLong: [32.246303321878436, -109.82579294631043],
      entity: 'tucson border patrol sector',
      title: 'Border Patrol Willcox Station',
      addressLines: [
        '200 South Rex Allen Jr. Road',
        'Willcox, Arizona 85643',
        'Phone: (520) 384-7200',
      ],
    },
  },
  Yuma: {
    'Blythe Station': {
      latLong: [33.610033905335854, -114.70907492591739],
      entity: 'yuma border patrol sector',
      title: 'Border Patrol Blythe Station',
      addressLines: [
        '16870 W. Hobson Way',
        'Blythe, CA 92226',
        'Phone: (760) 921-7000',
      ],
    },
    'Wellton Station': {
      latLong: [32.66702947412265, -114.10988026983516],
      entity: 'yuma border patrol sector',
      title: 'Border Patrol Wellton Station',
      addressLines: [
        '10888 Avenue 31E',
        'Wellton, AZ 85356',
        'Phone: (928) 785-9364',
      ],
    },
    'Yuma Station': {
      latLong: [32.65250314008074, -114.63217637513068],
      entity: 'yuma border patrol sector',
      title: 'Border Patrol Yuma Station',
      addressLines: [
        '4151 S. Avenue A',
        'Yuma, AZ 85365',
        'Phone: (928) 341-2800',
      ],
    },
  },
}
