import * as React from 'react'

export const data = {
  'ICE ERO Atlanta': {
    latLong: [33.74961343353164, -84.3973474455124],
    entity: 'ice ero atlanta',
    title: 'ICE ERO Atlanta',
    addressLines: [
      '180 Ted Turner Dr. SW',
      'Suite 522',
      'Atlanta, GA 30303',
      'Phone: (404) 893-1290',
    ],
  },
  'ICE ERO Baltimore': {
    latLong: [39.288161028488204, -76.61672436882624],
    entity: 'ice ero baltimore',
    title: 'ICE ERO Baltimore',
    addressLines: [
      '31 Hopkins Plaza',
      '6th Floor',
      'Baltimore, MD 21201',
      'United States',
      'Phone: (410) 637-4000',
    ],
  },
  'ICE ERO Batavia': {
    latLong: [43.020182567022324, -78.20233809038425],
    entity: 'ice ero batavia',
    title: 'ICE ERO Batavia',
    addressLines: [
      '4250 Federal Drive',
      'Batavia, NY 14020',
      'Phone: (585) 344-6500',
    ],
  },
  'ICE ERO Boston': {
    latLong: [42.482648727222774, -71.20898706280533],
    entity: 'ice ero boston',
    title: 'ICE ERO Boston',
    addressLines: [
      '1000 District Avenue',
      'Burlington, MA 01803',
      'Phone: (781) 359-7500',
    ],
  },
  'ICE ERO Buffalo': {
    latLong: [42.890965966392, -78.87697087443574],
    entity: 'ice ero buffalo',
    title: 'ICE ERO Buffalo',
    addressLines: [
      '250 Delaware Avenue, Floor 7',
      'Buffalo, NY 14202',
      'Phone: (716) 464-5800',
    ],
  },
  'ICE ERO Chicago': {
    latLong: [41.87538263837195, -87.6310700805282],
    entity: 'ice ero chicago',
    title: 'ICE ERO Chicago',
    addressLines: [
      '101 W Ida B Wells Drive',
      'Suite 4000',
      'Chicago, IL 60605',
      'Phone: (312) 347-2400',
    ],
  },
  'ICE ERO Dallas': {
    latLong: [32.82375688147957, -96.87218946335041],
    entity: 'ice ero dallas',
    title: 'ICE ERO Dallas',
    addressLines: [
      '8101 N. Stemmons Frwy',
      'Dallas, TX 75247',
      'Phone: (972) 367-2200',
    ],
  },
  'ICE ERO Denver': {
    latLong: [39.601187369854586, -104.84506565868082],
    entity: 'ice ero denver',
    title: 'ICE ERO Denver',
    addressLines: [
      '12445 E. Caley Avenue',
      'Centennial, CO 80111',
      'Phone: (720) 873-2899',
    ],
  },
  'ICE ERO Detroit': {
    latLong: [42.3429298286484, -83.01000654441133],
    entity: 'ice ero detroit',
    title: 'ICE ERO Detroit',
    addressLines: [
      '333 Mt. Elliott St',
      'Detroit, MI 48207',
      'Phone: (313) 568-6049',
    ],
  },
  'ICE ERO El Paso': {
    latLong: [31.80485603754899, -106.29112860666613],
    entity: 'ice ero el paso',
    title: 'ICE ERO El Paso',
    addressLines: [
      '11541 Montana Ave',
      'Suite E',
      'El Paso, TX 79936',
      'Phone: (915) 225-1901',
    ],
  },
  'ICE ERO Houston': {
    latLong: [29.937616942395408, -95.4066321812804],
    entity: 'ice ero houston',
    title: 'ICE ERO Houston',
    addressLines: [
      '126 Northpoint Drive',
      'Houston, TX 77060',
      'Phone: (281) 774-4816',
    ],
  },
  'ICE ERO Los Angeles': {
    latLong: [34.05376105080079, -118.23972958107882],
    entity: 'ice ero los angeles',
    title: 'ICE ERO Los Angeles',
    addressLines: [
      '300 North Los Angeles St.',
      'Room 7631',
      'Los Angeles, CA 90012',
      'Phone: (213) 830-7911',
    ],
  },
  'ICE ERO Miami': {
    latLong: [26.109884785533474, -80.25620956365574],
    entity: 'ice ero miami',
    title: 'ICE ERO Miami',
    addressLines: [
      '865 SW 78th Avenue',
      'Suite 101',
      'Plantation, FL 33324',
      'Phone: (954) 236-4900',
    ],
  },
  'ICE ERO New Orleans': {
    latLong: [29.95064639360769, -90.07689085305468],
    entity: 'ice ero new orleans',
    title: 'ICE ERO New Orleans',
    addressLines: [
      '1250 Poydras',
      'Suite 325',
      'New Orleans, LA 70113',
      'Phone: (504) 599-7800',
    ],
  },
  'ICE ERO New York City': {
    latLong: [40.71528575281864, -74.00411627457021],
    entity: 'ice ero new york city',
    title: 'ICE ERO New York City',
    addressLines: [
      '26 Federal Plaza',
      '9th Floor, Suite 9-110',
      'New York, NY 10278',
      'Phone: (212) 436-9315',
    ],
  },
  'ICE ERO Newark': {
    latLong: [40.73011646660293, -74.17424496291255],
    entity: 'ice ero newark',
    title: 'ICE ERO Newark',
    addressLines: [
      '970 Broad St. 11th Floor',
      'Newark, NJ 07102',
      'Phone: (973) 776-3274',
    ],
  },
  'ICE ERO Philadelphia': {
    latLong: [39.95389096767238, -75.15313355682474],
    entity: 'ice ero philadelphia',
    title: 'ICE ERO Philadelphia',
    addressLines: [
      '114 North 8th Street',
      'Philadelphia, PA 19107',
      'Phone: (215) 656-7164',
    ],
  },
  'ICE ERO Phoenix': {
    latLong: [33.470852948087696, -112.0731931749739],
    entity: 'ice ero phoenix',
    title: 'ICE ERO Phoenix',
    addressLines: [
      '2035 N. Central Avenue',
      'Phoenix, AZ 85004',
      'Phone: (602) 257-5900',
    ],
  },
  'ICE ERO Saint Paul': {
    latLong: [44.89421437116252, -93.19489520927809],
    entity: 'ice ero saint paul',
    title: 'ICE ERO Saint Paul',
    addressLines: [
      '1 Federal Drive',
      'Suite 1601',
      'Fort Snelling, MN 55111',
      'Phone: (612) 843-8600',
    ],
  },
  'ICE ERO Salt Lake City': {
    latLong: [40.70629320455767, -111.94618816291398],
    entity: 'ice ero salt lake city',
    title: 'ICE ERO Salt Lake City',
    addressLines: [
      '2975 Decker Lake Drive',
      'Suite 100',
      'West Valley City, UT 84119-6096',
      'Phone: (801) 886-7400',
    ],
  },
  'ICE ERO San Antonio': {
    latLong: [29.517399577314, -98.43663796350872],
    entity: 'ice ero san antonio',
    title: 'ICE ERO San Antonio',
    addressLines: [
      '1777 NE Loop 410',
      'Floor 15',
      'San Antonio, TX 78217',
      'Phone: (210) 283-4750',
    ],
  },
  'ICE ERO San Diego': {
    latLong: [32.71435720055896, -117.16471799219185],
    entity: 'ice ero san diego',
    title: 'ICE ERO San Diego',
    addressLines: [
      '880 Front Street',
      '#2242',
      'San Diego, CA 92101',
      'Phone: (619) 436-0410',
    ],
  },
  'ICE ERO San Francisco': {
    latLong: [37.79623266041419, -122.40123139570304],
    entity: 'ice ero san francisco',
    title: 'ICE ERO San Francisco',
    addressLines: [
      '630 Sansome Street',
      'Rm 590',
      'San Francisco, CA 94111',
      'Phone: (415) 844-5512',
    ],
  },
  'ICE ERO Seattle': {
    latLong: [47.4904142865364, -122.29141171036281],
    entity: 'ice ero seattle',
    title: 'ICE ERO Seattle',
    addressLines: [
      '12500 Tukwila International Boulevard',
      'Seattle, WA 98168',
      'Phone: (206) 277-2000',
    ],
  },
  'ICE ERO Washington, DC': {
    latLong: [38.91149567502299, -77.45865576179722],
    entity: 'ice ero washington, dc',
    title: 'ICE ERO Washington, DC',
    addressLines: [
      '14797 Murdock Street',
      'Chantilly, VA 20151',
      'Phone: (703) 633-2100',
    ],
  },
}
