import * as React from 'react'

export const data = {
  'Big Bend Border Patrol Sector': {
    latLong: [30.30357300058833, -104.02371210548938],
    entity: 'big bend border patrol sector',
    title: 'Big Bend Border Patrol Sector',
    addressLines: [
      'P.O. Box I',
      'Marfa, TX 79843',
      'Phone: (432) 729-5200',
    ],
  },
  'Blaine Border Patrol Sector': {
    latLong: [48.97644596578675, -122.72262563506901],
    entity: 'blaine border patrol sector',
    title: 'Blaine Border Patrol Sector',
    addressLines: [
      "2410 Nature's Path Way",
      'Blaine, WA 98230',
      'Phone: (360) 332-9200',
    ],
  },
  'Buffalo Border Patrol Sector': {
    latLong: [43.03476363968433, -78.97779739125053],
    entity: 'buffalo border patrol sector',
    title: 'Buffalo Border Patrol Sector',
    addressLines: [
      '201 Lang Blvd.',
      'Grand Island, NY 14072',
      'Phone: (716) 774-7200',
    ],
  },
  'Del Rio Border Patrol Sector': {
    latLong: [29.392624606321885, -100.89912866873938],
    entity: 'del rio border patrol sector',
    title: 'Del Rio Border Patrol Sector',
    addressLines: [
      '2401 Dodson Avenue',
      'Del Rio, TX 78840',
      'Phone: (830) 778-7000',
    ],
  },
  'Detroit Border Patrol Sector': {
    latLong: [42.62073735601456, -82.83602436919647],
    entity: 'detroit border patrol sector',
    title: 'Detroit Border Patrol Sector',
    addressLines: [
      '26000 South Street',
      'Building 1516',
      'Selfridge ANGB, MI 48045',
      'Phone: (586) 239-2160',
    ],
  },
  'El Centro Border Patrol Sector': {
    latLong: [32.824893274876594, -115.55476612681498],
    entity: 'el centro border patrol sector',
    title: 'El Centro Border Patrol Sector',
    addressLines: [
      '211 West Aten Road',
      'Imperial, California 92251',
      'Phone: (760) 335-5700',
    ],
  },
  'El Paso Border Patrol Sector': {
    latLong: [31.793907253189694, -106.37019495878877],
    entity: 'el paso border patrol sector',
    title: 'El Paso Border Patrol Sector',
    addressLines: [
      '8901 Montana Avenue',
      'El Paso, TX 79925-1212',
      'Phone: (915) 834-8350',
    ],
  },
  'Grand Forks Border Patrol Sector': {
    latLong: [47.9358656588873, -97.19498625347482],
    entity: 'grand forks border patrol sector',
    title: 'Grand Forks Border Patrol Sector',
    addressLines: [
      '1816 17th Street NE',
      'Grand Forks, ND 58203',
      'Phone: (701) 772-3056',
    ],
  },
  'Havre Border Patrol Sector': {
    latLong: [48.528172666105604, -109.6809770620235],
    entity: 'havre border patrol sector',
    title: 'Havre Border Patrol Sector',
    addressLines: [
      '2605 5th Ave. SE',
      'Havre, MT 59501',
      'Phone: (406) 262-5600',
    ],
  },
  'Houlton Border Patrol Sector': {
    latLong: [46.08877717565214, -67.84105960453233],
    entity: 'houlton border patrol sector',
    title: 'Houlton Border Patrol Sector',
    addressLines: ['96 Calais Rd', 'Hodgdon ME 04730', 'Phone: (207) 532-6521'],
  },
  'Laredo Border Patrol Sector': {
    latLong: [27.566496681538343, -99.50005372890715],
    entity: 'laredo border patrol sector',
    title: 'Laredo Border Patrol Sector',
    addressLines: [
      '207 W. Del Mar Blvd.',
      'Laredo, TX 78041',
      'Phone: (956) 764-3200',
    ],
  },
  'Miami Border Patrol Sector': {
    latLong: [26.007181831768033, -80.35377500568367],
    entity: 'miami border patrol sector',
    title: 'Miami Border Patrol Sector',
    addressLines: [
      '15720 Pines Boulevard',
      'Pembroke Pines, FL 33027',
      'Phone: (954) 965-6300',
    ],
  },
  'New Orleans Border Patrol Sector': {
    latLong: [29.92488501607076, -90.01626890844696],
    entity: 'new orleans border patrol sector',
    title: 'New Orleans Border Patrol Sector',
    addressLines: [
      'P.O. Box 6218',
      'New Orleans, LA 70174-6218',
      'Phone: (504) 376-2800',
    ],
  },
  'Ramsey Border Patrol Sector': {
    latLong: [18.50081864522199, -67.13862776339985],
    entity: 'ramsey border patrol sector',
    title: 'Ramsey Border Patrol Sector',
    addressLines: [
      '723 Belt St Aguadilla',
      'PR 00603',
      'Phone: (787) 890-4747',
    ],
  },
  'Rio Grande Valley Border Patrol Sector': {
    latLong: [26.258859353910765, -98.16414168664215],
    entity: 'rio grande valley border patrol sector',
    title: 'Rio Grande Valley Border Patrol Sector',
    addressLines: [
      '4400 South Expressway 281',
      'Edinburg, Texas',
      'Phone: (956) 289-4800',
    ],
  },
  'San Diego Border Patrol Sector': {
    latLong: [32.65583873951683, -116.96123420537037],
    entity: 'san diego border patrol sector',
    title: 'San Diego Border Patrol Sector',
    addressLines: [
      '2411 Boswell Rd.',
      'Chula Vista, CA 91914-3519',
      'Phone: (619) 216-4000',
    ],
  },
  'Spokane Border Patrol Sector': {
    latLong: [47.75530953610772, -117.39456857557578],
    entity: 'spokane border patrol sector',
    title: 'Spokane Border Patrol Sector',
    addressLines: [
      '10710 N. Newport Highway',
      'Spokane, Washington 99218',
      'Phone: (509) 353-2747',
    ],
  },
  'Swanton Border Patrol Sector': {
    latLong: [44.911749157862246, -73.10924050461588],
    entity: 'swanton border patrol sector',
    title: 'Swanton Border Patrol Sector',
    addressLines: [
      '155 Grand Ave',
      'Swanton, VT 05488',
      'Phone: 1-800-247-2434',
    ],
  },
  'Tucson Border Patrol Sector': {
    latLong: [32.19396973246213, -110.8936924053945],
    entity: 'tucson border patrol sector',
    title: 'Tucson Border Patrol Sector',
    addressLines: [
      '2430 S. Swan Road',
      'Tucson, AZ 85711',
      'Phone: (520) 748-3000',
    ],
  },
  'Yuma Border Patrol Sector': {
    latLong: [32.654394473722625, -114.63194033105178],
    entity: 'yuma border patrol sector',
    title: 'Yuma Border Patrol Sector',
    addressLines: ['4035 S. Ave. A', 'Yuma AZ 85365', 'Phone: (928) 341-6500'],
  },
}
