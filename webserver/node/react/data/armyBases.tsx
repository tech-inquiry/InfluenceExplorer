import * as React from 'react'

export const data = {
  'Aberdeen Proving Ground': {
    latLong: [39.46870651457396, -76.13005466690208],
    entity: 'aberdeen proving ground',
    title: 'Aberdeen Proving Ground',
    controller: 'IMCOM',
    addressLines: [
      'Aberdeen Proving Ground, MD'
    ]
  },
  'Anniston Army Depot': {
    latLong: [33.62415510698187, -85.96754538908446],
    entity: 'anniston army depot',
    title: 'Anniston Army Depot',
    controller: 'TACOM',
    addressLines: [
      '7 Frankford Ave', 'Anniston, AL 36201'
    ]
  },
  'Blue Grass Army Depot': {
    latLong: [37.67971294076381, -84.25326659821401],
    entity: 'blue grass army depot',
    title: 'Blue Grass Army Depot',
    controller: 'Joint Munitions Command',
    addressLines: [
      'Richmond, KY 40475'
    ]
  },
  'Camp Ashland': {
    latLong: [41.07256472573475, -96.33978613336849],
    entity: 'camp ashland',
    title: 'Camp Ashland',
    controller: 'NE Army National Guard',
    addressLines: [
      'Ashland, NE 68003'
    ],
  },
  'Camp Atterbury': {
    latLong: [39.35919770330471, -86.02429855398336],
    entity: 'camp atterbury',
    title: 'Camp Atterbury',
    controller: 'IN National Guard',
    addressLines: [
      'Franklin, IN 46131'
    ],
  },
  'Camp Blanding Joint Training Center': {
    latLong: [29.961585640046735, -81.9733172790896],
    entity: 'camp blanding joint training center',
    title: 'Camp Blanding Joint Training Center',
    controller: 'FL National Guard',
    addressLines: [
      'Starke, FL 32091'
    ],
  },
  'Camp Butner Training Center': {
    latLong: [36.18274973471141, -78.79338370281846],
    entity: 'camp butner training center',
    title: 'Camp Butner Training Center',
    controller: 'NY National Guard',
    addressLines: [
      '539 Roberts Chapel Rd', 'Stem, NC 27581'
    ],
  },
  'Camp Carroll': {
    latLong: [35.99258924746457, 128.41067724604972],
    entity: 'camp carroll',
    title: 'Camp Carroll',
    controller: 'USAG Daegu',
    addressLines: [
      'Waegwan, South Korea'
    ],
  },
  'Camp Chamberlain': {
    latLong: [44.351821958169616, -69.79486696209077],
    entity: 'camp chamberlain',
    title: 'Camp Chamberlain',
    controller: 'ME Army National Guard',
    addressLines: [
      '23 Blue Star Ave', 'Augusta, ME 04330'
    ],
  },
  'Camp Curtis Guild': {
    latLong: [42.52881637766607, -71.07722783337795],
    entity: 'camp curtis guild',
    title: 'Camp Curtis Guild',
    controller: 'MA Army National Guard',
    addressLines: [
      'Reading, MA 01867'
    ],
  },
  'Camp Devens': {
    latLong: [42.53778444589455, -71.61571842141105],
    entity: 'camp devens',
    title: 'Camp Devens',
    controller: 'IMCOM',
    addressLines: [
      'Devens, MA 01434'
    ],
  },
  'Camp Dodge': {
    latLong: [41.69780987206447, -93.71533475791662],
    entity: 'camp dodge',
    title: 'Camp Dodge',
    controller: 'IA Army National Guard',
    addressLines: [
      'Johnston, IA 50131'
    ]
  },
  'Camp Edwards': {
    latLong: [41.6541891857062, -70.53809858866374],
    entity: 'camp edwards',
    title: 'Camp Edwards (JB Cape Cod)',
    controller: 'MA National Guard',
    addressLines: [
      'Mashpee, MA'
    ]
  },
  'Camp Ethan Allen Training Site': {
    latLong: [44.48074159963403, -72.95018109629609],
    entity: 'camp ethan allen training site',
    title: 'Camp Ethan Allen Training Site',
    controller: 'VT Army National Guard',
    addressLines: [
      'Jericho, VT 05465'
    ]
  },
  'Camp Fogarty Training Site': {
    latLong: [41.62000800674609, -71.49456267091043],
    entity: 'camp fogarty training site',
    title: 'Camp Fogarty Training Site',
    controller: 'RI Army Natioanl Guard',
    addressLines: [
      'East Greenwich, RI 02818'
    ]
  },
  'Camp Frank D. Merrill': {
    latLong: [34.62743070269926, -84.10421534653892],
    entity: 'camp frank d. merrill',
    title: 'Camp Frank D. Merrill',
    controller: 'IMCOM',
    addressLines: [
      'Dahlonega, GA 30533'
    ],
  },
  'Camp George': {
    latLong: [35.848718957737304, 128.5939293237146],
    entity: 'camp george',
    title: 'Camp George',
    controller: 'USAG Daegu',
    addressLines: [
      'Nam-gu, Daegu, South Korea'
    ],
  },
  'Camp Grafton Training Center': {
    latLong: [48.055114570415974, -98.90335610414793],
    entity: 'camp grafton training center',
    title: 'Camp Grafton Training Center',
    controller: 'ND Army National Guard',
    addressLines: [
      'Devils Lake, ND 58301'
    ],
  },
  'Camp Grayling': {
    latLong: [44.630462910902736, -84.77752250445486],
    entity: 'camp grayling',
    title: 'Camp Grayling',
    controller: 'MI Army National Guard',
    addressLines: [
      'Grayling, MI 49738'
    ],
  },
  'Camp Gruber Training Center': {
    latLong: [35.67179120645927, -95.20268279563058],
    entity: 'camp gruber training center',
    title: 'Camp Gruber Training Center',
    controller: 'OK Army National Guard',
    addressLines: [
      'Braggs, OK 74423'
    ],
  },
  'Camp Henry': {
    latLong: [35.85081434507122, 128.60156372031398],
    entity: 'camp henry',
    title: 'Camp Henry',
    controller: 'USAG Daegu',
    addressLines: [
      'Nam-gu, Daegu, South Korea'
    ],
  },
  'Camp James A. Garfield Joint Military Training Center': {
    latLong: [41.16811247542931, -81.08535459661647],
    entity: 'camp james a. garfield joint military training center',
    title: 'Camp James A. Garfield Joint Military Training Center',
    controller: 'OH Army National Guard',
    addressLines: [
      'Ravenna, OH 44266'
    ]
  },
  'Camp Johnson, Vermont': {
    latLong: [44.50044134492174, -73.162993370106],
    entity: 'camp johnson, vermont',
    title: 'Camp Johnson, Vermont',
    controller: 'VT Army National Guard',
    addressLines: [
      'Colchester, VT 05446'
    ]
  },
  'Camp Keyes': {
    latLong: [44.31746580367242, -69.79136885544673],
    entity: 'camp keyes',
    title: 'Camp Keyes',
    controller: 'ME Army National Guard',
    addressLines: [
      'Augusta, ME 04330'
    ],
  },
  'Camp Kościuszko': {
    latLong: [52.40848197203611, 16.895038096797492],
    entity: 'camp kościuszko',
    title: 'Camp Kościuszko',
    controller: 'IMCOM Europe',
    addressLines: [
      '60-810 Poznań, Poland'
    ]
  },
  'Camp Lemonnier, Djibouti': {
    latLong: [11.540662840271038, 43.15835441292781],
    entity: 'camp lemonnier, djibouti',
    title: 'Camp Lemonnier, Djibouti',
    addressLines: [
      'PSC 831 Box 0040',
      'FPO AE 09363-0001',
    ],
  },
  'Camp Mackall': {
    latLong: [35.03533783218236, -79.49703268004019],
    entity: 'camp mackall',
    title: 'Camp Mackall (Fort Liberty)',
    controller: 'Fort Liberty',
    addressLines: [
      'Hoffman, NC'
    ]
  },
  'Camp McCain Training Center': {
    latLong: [33.698034267096446, -89.7127276037301],
    entity: 'camp mccain training center',
    title: 'Camp McCain Training Center',
    controller: 'MS Army National Guard',
    addressLines: [
      'Grenada, MS 38901'
    ]
  },
  'Camp Navajo': {
    latLong: [35.230639908350696, -111.82172902874696],
    entity: 'camp navajo',
    title: 'Camp Navajo',
    controller: 'AZ Army National Guard',
    addressLines: [
      '1 Hughes Ave', 'Bellemont, AZ 86015'
    ]
  },
  'Camp Nett': {
    latLong: [41.33343604102048, -72.18551287756529],
    entity: 'camp nett',
    title: 'Camp Nett',
    controller: 'CT National Guard',
    addressLines: [
      '38 Smith Street',
      'Niantic, CT 06357',
      'Phone: (860) 739-1699'
    ]
  },
  'Camp Ouallam': {
    latLong: [14.3127546424808, 2.064330827329125],
    entity: 'camp ouallam',
    title: 'Camp Ouallam',
    controller: 'SOCAFRICA',
    addressLines: [
      'Ouallam, Niger'
    ]
  },
  'Camp Panzer Kaserne': {
    latLong: [48.685751753511056, 9.043575864155638],
    entity: 'camp panzer kaserne',
    title: 'Camp Panzer Kaserne',
    controller: 'USAG Stuttgart',
    addressLines: [
      'Panzer Strasse',
      'Boeblingen-Stuttgart, 71032'
    ]
  },
  'Camp Perry Joint Training Center': {
    latLong: [41.532236788087786, -83.0181588793082],
    entity: 'camp perry joint training center',
    title: 'Camp Perry Joint Training Center',
    controller: 'OH Army National Guard',
    addressLines: [
      'Port Clinton, OH 43452'
    ]
  },
  'Camp Rapid': {
    latLong: [44.081200120869745, -103.26640198356698],
    entity: 'camp rapid',
    title: 'Camp Rapid',
    controller: 'SD Army National Guard',
    addressLines: [
      'Rapid City, SD 57702'
    ]
  },
  'Camp Rilea Armed Forces Training Center': {
    latLong: [46.122481163763034, -123.93396356929341],
    entity: 'camp rilea armed forces training center',
    title: 'Camp Rilea Armed Forces Training Center',
    controller: 'OR Army National Guard',
    addressLines: [
      'Warrenton, OR 97146'
    ]
  },
  'Camp Ripley Training Center': {
    latLong: [46.09308493396267, -94.35958956309543],
    entity: 'camp ripley training center',
    title: 'Camp Ripley Training Center',
    controller: 'MN National Guard',
    addressLines: [
      '15000 Highway 115',
      'Little Falls, MN 56345-4173',
      'Phone: 320.632.7000'
    ]
  },
  'Camp Roberts': {
    latLong: [35.79834781277171, -120.74390464481174],
    entity: 'camp roberts',
    title: 'Camp Roberts',
    controller: 'CA Army National Guard',
    addressLines: [
      'San Miguel, CA 93451'
    ],
  },
  'Camp San Luis Obispo': {
    latLong: [35.32411838348817, -120.72940254948321],
    entity: 'camp san luis obispo',
    title: 'Camp San Luis Obispo',
    controller: 'CA Army National Guard',
    addressLines: [
      'San Luis Obispo, CA 93405'
    ],
  },
  'Camp Santiago Joint Training Center': {
    latLong: [18.003832516631746, -66.29439240965233],
    entity: 'camp santiago joint training center',
    title: 'Camp Santiago Joint Training Center',
    controller: 'PR National Guard',
    addressLines: [
      'Salinas 00751, Puerto Rico'
    ]
  },
  'Camp Shelby Joint Forces Training Center': {
    latLong: [31.19841853383025, -89.19237769885257],
    entity: 'camp shelby joint forces training center',
    title: 'Camp Shelby Joint Forces Training Center',
    controller: 'MS Army National Guard',
    addressLines: [
      'Lee Ave', 'Hattiesburg, MS 39401'
    ]
  },
  'Camp Sherman Joint Training Center': {
    latLong: [39.357653271517414, -82.96464088447375],
    entity: 'camp sherman joint training center',
    title: 'Camp Sherman Joint Training Center',
    controller: 'OH Army National Guard',
    addressLines: [
      '2154 Narrows Rd', 'Chillicothe, OH 45601'
    ]
  },
  'Camp Simba': {
    latLong: [-2.15398314610, 40.8968660278],
    entity: 'camp simba',
    title: 'Camp Simba',
    controller: 'CSL Manda Bay',
    addressLines: [
      'Hindi Magogoni, Kenya'
    ],
  },
  'Camp Smith': {
    latLong: [41.30183175106823, -73.94059728133469],
    entity: 'camp smith',
    title: 'Camp Smith',
    controller: 'NY Army National Guard',
    addressLines: [
      '11 Bear Mountain Bridge Rd', 'Cortlandt, NY 10567, United States'
    ],
  },
  'Camp Swift Training Center': {
    latLong: [30.222250476431157, -97.31424270209862],
    entity: 'camp smith training center',
    title: 'Camp Smith Training Center',
    controller: 'TX Army National Guard',
    addressLines: [
      'Bastrop, TX 78602'
    ]
  },
  'Camp Varnum': {
    latLong: [41.44602439250841, -71.43530718998875],
    entity: 'camp varnum',
    title: 'Camp Varnum',
    controller: 'RI Army National Guard',
    addressLines: [
      'Narragansett, RI 02882'
    ]
  },
  'Camp W. G. Williams': {
    latLong: [40.435168420112426, -111.92625800116308],
    entity: 'camp w. g. williams',
    title: 'Camp W. G. Williams',
    controller: 'UT Army National Guard',
    addressLines: [
      'Bluffdale, UT 84045'
    ]
  },
  'Camp Walker': {
    latLong: [35.838340975245806, 128.58970912727142],
    entity: 'camp walker',
    title: 'Camp Walker',
    controller: 'USAG Daegu',
    addressLines: [
      'Nam-gu, Daegu, South Korea'
    ],
  },
  'Camp Zama': {
    'latLong': [35.48960177128652, 139.3957177065062],
    'entity': 'camp zama',
    'title': 'Camp Zama',
    controller: 'IMCOM Pacific',
    'addressLines': [
      'Zama, Kanagawa 252-0027, Japan'
    ]
  },
  'Carlisle Barracks': {
    latLong: [40.20978158971501, -77.17693073468905],
    entity: 'carlisle barracks',
    title: 'Carlisle Barracks',
    controller: 'IMCOM',
    addressLines: [
      '22 Ashburn Dr', 'Carlisle, PA 17013'
    ]
  },
  'Caserma Ederle': {
    latLong: [45.54223228226459, 11.57936888024121],
    entity: 'caserma ederle',
    title: 'Caserma Ederle',
    controller: 'USAG Italy',
    addressLines: [
      '36100 Vicenza VI, Italy'
    ]
  },
  'Combat Capabilities Development Command Soldier Center': {
    latLong: [42.28825962925112, -71.36337558921458],
    entity: 'combat capabilities development command soldier center',
    title: 'DEVCOM Soldier Center',
    controller: 'DEVCOM',
    addressLines: [
      'Natick, MA 01760'
    ]
  },
  'Command Post TANGO': {
    latLong: [37.42615272333173, 127.05930215443084],
    entity: 'command post theater air naval ground operations',
    title: 'Command Post TANGO',
    controller: 'IMCOM Pacific',
    addressLines: [
      'Gyeonggi-do, South Korea'
    ],
  },
  'Dagger Complex': {
    'latLong': [49.84390580882167, 8.584377796604988],
    'entity': 'dagger complex',
    'title': 'Dagger Complex',
    controller: 'USAG Wiesbaden',
    'addressLines': [
      'Eberstädter Weg 4370', '64347 Griesheim, Germany'
    ]
  },
  'Darby Military Community': {
    latLong: [43.6441650556214, 10.352413887438273],
    entity: 'darby military community',
    title: 'Darby Military Community',
    controller: 'USAG Italy',
    addressLines: [
      'Via Vecchia Livornese, 788', '56122 Pisa PI, Italy'
    ]
  },
  'Dugway Proving Ground': {
    latLong: [40.229791409372076, -112.75123180967353],
    entity: 'dugway proving ground',
    title: 'Dugway Proving Ground',
    controller: 'ATEC',
    addressLines: [
      'Dugway, UT 84022'
    ]
  },
  'Fifth Regiment Armory': {
    latLong: [39.30342298202449, -76.62222597599367],
    entity: 'fifth regiment armory',
    title: 'Fifth Regiment Armory',
    controller: 'MD National Guard',
    addressLines: [
      '29th Division Street',
      'Baltimore, MD 21201-2288'
    ],
  },
  'Fort A.P. Hill': {
    latLong: [38.072022105804415, -77.32149380968029],
    entity: 'fort a.p. hill',
    title: 'Fort A.P. Hill',
    controller: 'IMCOM Sustainment',
    addressLines: [
      'Fort A.P. Hill, VA 22427'
    ],
  },
  'Fort Allen Training Center': {
    latLong: [18.005522727819486, -66.49556062543056],
    entity: 'fort allen training center',
    title: 'Fort Allen Training Center',
    controller: 'PR Army National Guard',
    addressLines: [
      'Rd 149', 'Juana Diaz 00795, Puerto Rico'
    ]
  },
  'Fort Belvoir': {
    latLong: [38.71465715344062, -77.14737463510292],
    entity: 'fort belvoir',
    title: 'Fort Belvoir',
    controller: 'IMCOM Sustainment',
    addressLines: [
      'Fort Belvoir, VA 22060'
    ],
  },
  'Fort Bliss': {
    latLong: [31.81149451258156, -106.42154427109266],
    entity: 'fort bliss',
    title: 'Fort Bliss',
    controller: 'Army Forces Command',
    addressLines: [
      'Fort Bliss, TX 79906'
    ]
  },
  'Fort Bragg': {
    latLong: [35.13533231553799, -78.99467452788797],
    entity: 'fort bragg',
    title: 'Fort Bragg',
    addressLines: [
      'Fort Bragg, NC 28310'
    ],
  },
  'Fort Buchanan': {
    latLong: [18.419359792161924, -66.12538630711458],
    entity: 'fort buchanan',
    title: 'Fort Buchanan',
    addressLines: [
      'Guaynabo 00934, Puerto Rico'
    ]
  },
  'Fort Campbell': {
    latLong: [36.65828303180332, -87.47880345953598],
    entity: 'fort campbell',
    title: 'Fort Campbell',
    addressLines: [
      'Fort Campbell, KY 42223'
    ],
  },
  'Fort Carson': {
    latLong: [38.716495723941485, -104.77082668110643],
    entity: 'fort carson',
    title: 'Fort Carson',
    controller: 'IMCOM',
    addressLines: [
      'Fort Carson, CO 80913'
    ],
  },
  'Fort Cavazos': {
    latLong: [31.15465463108803, -97.76993767947081],
    entity: 'fort cavazos',
    title: 'Fort Cavazos',
    addressLines: [
      'Fort Cavazos, TX 76544'
    ],
  },
  'Fort Chaffee Joint Maneuver Training Center': {
    latLong: [35.3044684294877, -94.29586196434492],
    entity: 'fort chaffee joint maneuver training center',
    title: 'Fort Chaffee Joint Maneuver Training Center',
    controller: 'AR Army National Guard',
    addressLines: [
      'Fort Smith, AR 72916'
    ],
  },
  'Fort Custer Training Center': {
    latLong: [42.32950832451423, -85.2886459042362],
    entity: 'fort custer training center',
    title: 'Fort Custer',
    controller: 'MI Army National Guard',
    addressLines: [
      'Augusta, MI 49012'
    ],
  },
  'Fort Detrick': {
    latLong: [39.435447547566916, -77.42766745352809],
    entity: 'fort detrick',
    title: 'Fort Detrick',
    controller: 'IMCOM Sustainment',
    addressLines: [
      '810 Schreider St, Fort Detrick, MD 21702'
    ],
  },
  'Fort Drum': {
    latLong: [44.05049390698787, -75.76768711991657],
    entity: 'fort drum',
    title: 'Fort Drum',
    controller: 'IMCOM',
    addressLines: [
      'Fort Drum, NY 13603'
    ],
  },
  'Fort Eustis': {
    latLong: [37.15743343886894, -76.58379200041631],
    entity: 'joint base langley-eustis',
    title: 'Fort Eustis (JB Langley-Eustis)',
    addressLines: ['Newport News, VA 23604'],
  },
  'Fort George G. Meade': {
    latLong: [39.106853966059575, -76.74657949963331],
    entity: 'fort george g. meade',
    title: 'Fort George G. Meade',
    controller: 'IMCOM Sustainment',
    addressLines: [
      'Fort Meade, MD 20755'
    ],
  },
  'Fort Gordon': {
    latLong: [33.41884222813086, -82.14476564276363],
    entity: 'fort gordon',
    title: 'Fort Gordon',
    controller: 'IMCOM',
    addressLines: [
      'Augusta, GA 30905'
    ],
  },
  'Fort Greely': {
    latLong: [63.901476675076374, -145.7181744410102],
    entity: 'fort greely',
    title: 'Fort Greely',
    controller: 'USAG Alaska',
    addressLines: [
      'Southeast Fairbanks Census Area, AK'
    ],
  },
  'Fort Gregg-Adams': {
    latLong: [37.24058301345361, -77.33792759085813],
    entity: 'fort gregg-adams',
    title: 'Fort Gregg-Adams',
    controller: 'IMCOM',
    addressLines: [
      'Fort Gregg-Adams, VA 23801'
    ],
  },
  'Fort Hamilton': {
    latLong: [40.61059901965437, -74.02839746211305],
    entity: 'fort hamilton',
    title: 'Fort Hamilton',
    controller: 'IMCOM',
    addressLines: [
      '114 White Ave', 'Brooklyn, NY 11252'
    ],
  },
  'Fort Huachuca': {
    latLong: [31.554925768198512, -110.34698696173585],
    entity: 'fort huachuca',
    title: 'Fort Huachuca',
    controller: 'IMCOM',
    addressLines: [
      'Sierra Vista, AZ 85613'
    ],
  },
  'Fort Hunter Liggett': {
    latLong: [36.00370670506831, -121.23134786737734],
    entity: 'fort hunter liggett',
    title: 'Fort Hunter Liggett',
    controller: 'IMCOM',
    addressLines: [
      '238 California Avenue', 'Jolon, CA 93928'
    ],
  },
  'Fort Indiantown Gap': {
    latLong: [40.442557307393294, -76.59883249500403],
    entity: 'fort indiantown gap',
    title: 'Fort Indiantown Gap',
    controller: 'PA National Guard',
    addressLines: [
      'Annville, PA 17003'
    ],
  },
  'Fort Irwin National Training Center': {
    latLong: [35.26173740440129, -116.68818188523794],
    entity: 'fort irwin national training center',
    title: 'Fort Irwin National Training Center',
    controller: 'Army Forces Command',
    addressLines: [
      'Fort Irwin, CA 92310'
    ],
  },
  'Fort Jackson': {
    latLong: [34.01700974287178, -80.93842805489898],
    entity: 'fort jackson',
    title: 'Fort Jackson',
    controller: 'TRADOC',
    addressLines: [
      'Columbia, SC 29207'
    ]
  },
  'Fort Johnson': {
    latLong: [31.05924164185576, -93.21642504370803],
    entity: 'fort johnson',
    title: 'Fort Johnson',
    controller: 'IMCOM',
    addressLines: [
      'Fort Polk South, LA 71459'
    ]
  },
  'Fort Knox': {
    latLong: [37.88822014100673, -85.96406656939234],
    entity: 'fort knox',
    title: 'Fort Knox',
    controller: 'IMCOM',
    addressLines: [
      'Fort Knox, KY 40121'
    ],
  },
  'Fort Leavenworth': {
    latLong: [39.358221128550674, -94.91967119046907],
    entity: 'fort leavenworth',
    title: 'Fort Leavenworth',
    controller: 'IMCOM',
    addressLines: [
      'Fort Leavenworth, KS 66048'
    ],
  },
  'Fort Leonard Wood': {
    latLong: [37.71888819647011, -92.1520050671626],
    entity: 'fort leonard wood',
    title: 'Fort Leonard Wood',
    addressLines: [
      'Plato, MO 65552'
    ],
  },
  'Fort McClellan': {
    latLong: [33.7237336767989, -85.7852977650809],
    entity: 'fort mcclellan',
    title: 'Fort McClellan',
    controller: 'AL Army National Guard',
    addressLines: [
      '1023 Fort McClellan', 'Anniston, AL 36205'
    ],
  },
  'Fort Moore': {
    latLong: [32.352249926603704, -84.96893443086775],
    entity: 'fort moore',
    title: 'Fort Moore',
    controller: 'IMCOM',
    addressLines: [
      'Fort Moore, GA 31905'
    ],
  },
  'Fort Nathanael Greene': {
    latLong: [41.381461989398396, -71.48730852754882],
    entity: 'fort nathanael greene',
    title: 'Fort Nathanael Greene',
    controller: 'U.S. Army Reserves',
    addressLines: [
      'Narragansett, RI 02882'
    ]
  },
  'Fort Novosel': {
    latLong: [31.337941670701262, -85.71471274748832],
    entity: 'fort novosel',
    title: 'Fort Novosel',
    controller: 'IMCOM',
    addressLines: [
      'Fort Novosel, AL 36362'
    ]
  },
  'Fort Richardson': {
    latLong: [61.252961485682135, -149.79424181990015],
    entity: 'joint base elmendorf-richardson',
    title: 'Fort Richardson (JB Elmendorf-Richardson)',
    controller: 'IMCOM Pacific',
    addressLines: ['Anchorage, AK 99506'],
  },
  'Fort Riley': {
    latLong: [39.17318484133229, -96.80344795360979],
    entity: 'fort riley',
    title: 'Fort Riley',
    controller: 'IMCOM',
    addressLines: [
      'Fort Riley, KS 66442'
    ]
  },
  'Fort Shafter': {
    latLong: [21.345061248497736, -157.88387411323288],
    entity: 'fort shafter',
    title: 'Fort Shafter',
    controller: 'USAG Hawaii',
    addressLines: ['Honolulu, HI 96819']
  },
  'Fort Sill': {
    latLong: [34.659187572504095, -98.41795042508295],
    entity: 'fort sill',
    title: 'Fort Sill',
    controller: 'IMCOM',
    addressLines: ['Fort Sill, OK 73503']
  },
  'Fort Stewart': {
    latLong: [31.871007065660457, -81.60895460592285],
    entity: 'fort stewart',
    title: 'Fort Stewart',
    controller: 'IMCOM',
    addressLines: ['Fort Stewart, GA 31315']
  },
  'Fort Wainwright': {
    latLong: [64.83007167747905, -147.62893421220633],
    entity: 'fort wainwright',
    title: 'Fort Wainwright',
    controller: 'USAG Alaska',
    addressLines: [
      'Fort Wainwright, AK 99703'
    ],
  },
  'Fort William Henry Harrison': {
    latLong: [46.617736870110605, -112.09992840040279],
    entity: 'fort william henry harrison',
    title: 'Fort William Henry Harrison',
    controller: 'MT National Guard',
    addressLines: [
      'Helena, MT 59602'
    ]
  },
  'Gowen Field': {
    latLong: [43.561414424685026, -116.23056211170419],
    entity: 'gowen field',
    title: 'Gowen Field (Boise Airport)',
    controller: 'State of Idaho',
    addressLines: [
      'Boise, ID 83705'
    ],
  },
  'Great Plains Joint Training Center': {
    latLong: [38.73134449912542, -97.74124380293703],
    entity: 'great plains joint training center',
    title: 'Great Plains Joint Training Center',
    controller: 'KS National Guard',
    addressLines: [
      'Salina, KS 67401'
    ]
  },
  'Harrisburg Military Post': {
    latLong: [40.27781044181132, -76.87471794599146],
    entity: 'harrisburg military post',
    title: 'Harrisburg Military Post',
    controller: 'PA National Guard',
    addressLines: [
      'Harrisburg, PA 17103'
    ]
  },
  'Hawthorne Army Depot': {
    latLong: [38.54747842105602, -118.65172485306351],
    entity: 'hawthorne army depot',
    title: 'Hawthorne Army Depot',
    controller: 'Joint Munitions Command',
    addressLines: [
      'Walker Lake, NV 89415'
    ]
  },
  'Hunter Army Airfield': {
    latLong: [32.01121336216964, -81.14582222084752],
    entity: 'hunter army airfield',
    title: 'Hunter Army Airfield (Fort Stewart)',
    controller: 'IMCOM',
    addressLines: [
      'Savannah, GA 31409'
    ],
  },
  'Iowa Army Ammunition Plant': {
    latLong: [40.82573643083652, -91.26552551693185],
    entity: 'iowa army ammunition plant',
    title: 'Iowa Army Ammunition Plant',
    controller: 'Joint Munitions Command',
    addressLines: [
      'Middletown, IA 52638'
    ]
  },
  'Joint Base Lewis-McChord': {
    latLong: [47.13170643375651, -122.49220073129321],
    entity: 'joint base lewis-mcchord',
    title: 'Joint Base Lewis-McChord',
    addressLines: ['Joint Base Lewis-McChord, WA'],
  },
  'Joint Base McGuire-Dix-Lakehurst': {
    latLong: [40.04055377626638, -74.5843282046994],
    entity: 'joint base mcguire-dix-lakehurst',
    title: 'Joint Base McGuire-Dix-Lakehurst',
    addressLines: [
      '3021 McGuire Blvd',
      'McGuire AFB, NJ 08641',
    ],
  },
  'Joint Base Myer-Henderson Hall': {
    latLong: [38.87230296985544, -77.07896111085194],
    entity: 'joint base myer-henderson hall',
    title: 'Joint Base Myer-Henderson Hall',
    controller: 'IMCOM Sustainment',
    addressLines: ['Fort Myer, VA 22211']
  },
  'Joint Forces Training Base - Los Alamitos': {
    latLong: [33.79665740495186, -118.0545248930933],
    entity: 'joint forces training base - los alamitos',
    title: 'Joint Forces Training Base - Los Alamitos',
    controller: 'CA National Guard',
    addressLines: ['Los Alamitos, CA 90720'],
  },
  'Kelley Barracks': {
    latLong: [48.723238682595685, 9.175151022860662],
    entity: 'kelley barracks',
    title: 'Kelley Barracks',
    controller: 'USAG Stuttgart',
    addressLines: [
      'Plieninger Strasse 289', 'Mohringen-Stuttgart, 70567'
    ]
  },
  'Letterkenny Army Depot': {
    latLong: [39.996330809355975, -77.63196358300995],
    entity: 'letterkenny army depot',
    title: 'Letterkenny Army Depot',
    controller: 'Aviation & Missile Command',
    addressLines: [
      'Chambersburg, PA 17201'
    ]
  },
  'Louisiana National Guard Training Center Pineville': {
    latLong: [31.372895877804368, -92.3967813196292],
    entity: 'louisiana national guard training center pineville',
    title: 'Louisiana National Guard Training Center Pineville',
    controller: 'LA National Guard',
    addressLines: [
      'Pineville, LA 71360'
    ]
  },
  'McAlester Army Ammunition Plant': {
    latLong: [34.83879855838077, -95.83969270760447],
    entity: 'mcalester army ammunition plant',
    title: 'McAlester Army Ammunition Plant',
    controller: 'Joint Munitions Command',
    addressLines: [
      'McAlester, OK 74501'
    ]
  },
  'McEntire Joint National Guard Base': {
    latLong: [33.93943948459946, -80.8013173038501],
    entity: 'mcentire joint national guard base',
    title: 'McEntire Joint National Guard Base',
    controller: 'SC NG',
    addressLines: [
      'Hopkins, SC 29061'
    ]
  },
  'Muscatatuck Urban Training Center': {
    latLong: [39.04951469983345, -85.52869310352716],
    entity: 'muscatatuck urban training center',
    title: 'Muscatatuck Urban Training Center',
    controller: 'IN National Guard',
    addressLines: [
      'Butlerville, IN 47223'
    ]
  },
  'Parks Reserve Forces Training Area': {
    latLong: [37.7157035461547, -121.90791174515809],
    entity: 'parks reserve forces training area',
    title: 'Parks Reserve Forces Training Area',
    controller: 'IMCOM',
    addressLines: [
      'Dublin, CA 94568'
    ]
  },
  'Patch Barracks': {
    latLong: [48.7359090160413, 9.081116701911066],
    entity: 'patch barracks',
    title: 'Patch Barracks',
    controller: 'USAG Stuttgart',
    addressLines: [
      'Kurmaucker Strasse', 'Vaihingen-Stuttgart, 70376'
    ]
  },
  'Picatinny Arsenal': {
    latLong: [40.93760729278052, -74.56530010995131],
    entity: 'picatinny arsenal',
    title: 'Picatinny Arsenal',
    controller: 'IMCOM',
    addressLines: [
      'Rockaway Township, NJ'
    ]
  },
  'Pine Bluff Arsenal': {
    latLong: [34.29622909107035, -92.06860839572754],
    entity: 'pine bluff arsenal',
    title: 'Pine Bluff Arsenal',
    controller: 'Army Materiel Command',
    addressLines: [
      'White Hall, AR 71602'
    ]
  },
  'Pōhakuloa Training Area': {
    latLong: [19.75711410028179, -155.53567299682814],
    entity: 'pōhakuloa training area',
    title: 'Pōhakuloa Training Area',
    controller: 'IMCOM Pacific',
    addressLines: ['Mauna Kea Loop', 'Waimea, HI 96743']
  },
  'PRANG Army Aviation Support Facility': {
    latLong: [18.456808028817715, -66.09832581852878],
    entity: 'fernando luis ribas dominicci airport',
    title: 'PRARNG Aviation Support Facility (Isla Grande Airport)',
    controller: 'PR Army National Guard',
    addressLines: [
      'San Juan, 00907, Puerto Rico'
    ]
  },
  'Presidio of Monterey': {
    latLong: [36.6053056214929, -121.91171151902013],
    entity: 'presidio of monterey',
    title: 'Presidio of Monterey',
    controller: 'IMCOM',
    addressLines: [
      'Monterey, CA 93944'
    ]
  },
  'Pueblo Chemical Depot': {
    latLong: [38.276788542428925, -104.34239155337562],
    entity: 'pueblo chemical depot',
    title: 'Pueblo Chemical Depot',
    controller: 'Army Chemical Materials Activity',
    addressLines: [
      '45825 Highway 96 East',
      'Pueblo, CO 81006'
    ]
  },
  'Raven Rock Mountain Complex': {
    latLong: [39.734253484447045, -77.41991406061297],
    entity: 'raven rock mountain complex',
    title: 'Raven Rock Mountain Complex',
    controller: 'U.S. Department of Defense',
    addressLines: [
      'Liberty Township, PA 17320'
    ]
  },
  'Red River Army Depot': {
    latLong: [33.436398657884595, -94.2961806906286],
    entity: 'red river army depot',
    title: 'Red River Army Depot',
    controller: 'Army Materiel Command',
    addressLines: [
      '100 Armory Rd',
      'New Boston, TX 75570'
    ]
  },
  'Redstone Arsenal': {
    latLong: [34.68570019356632, -86.64950022750727],
    entity: 'redstone arsenal',
    title: 'Redstone Arsenal',
    controller: 'IMCOM',
    addressLines: [
      'Redstone Arsenal, AL'
    ]
  },
  'RI ARNG Army Aviation Support Facility': {
    latLong: [41.59510058985122, -71.42130002895436],
    entity: 'rhode island army national guard army aviation support faciliy',
    title: 'RI ARNG Army Aviation Support Facility',
    controller: 'Quonset Point ANGS',
    addressLines: [
      'North Kingstown, RI 02852' 
    ]
  },
  'Robinson Barracks': {
    latLong: [48.82026612542359, 9.190408588793582],
    entity: 'robinson barracks',
    title: 'Robinson Barracks',
    controller: 'USAG Stuttgart',
    addressLines: [
      'Heidlochstraße', '70376 Stuttgart-Burgholzhof'
    ]
  },
  'Robinson Maneuver Training Center': {
    latLong: [34.8237275751993, -92.28324298195308],
    entity: 'robinson maneuver training center',
    title: 'Robinson Maneuver Training Center',
    controller: 'AR Army National Guard',
    addressLines: [
      'North Little Rock, AR 72199'
    ],
  },
  'Rock Island Arsenal': {
    latLong: [41.5172792134375, -90.54422433397525],
    entity: 'rock island arsenal',
    title: 'Rock Island Arsenal',
    controller: 'IMCOM',
    addressLines: [
      'Rock Island, IL 61201'
    ]
  },
  'Schofield Barracks': {
    latLong: [21.49293359645888, -158.0587918691636],
    entity: 'schofield barracks',
    title: 'Schofield Barracks',
    controller: 'USAG Hawaii',
    addressLines: ['Wahiawa, HI 96786']
  },
  'Sierra Army Depot': {
    latLong: [40.147340928804724, -120.1275681275417],
    entity: 'sierra army depot',
    title: 'Sierra Army Depot',
    controller: 'IMCOM',
    addressLines: ['Herlong, CA 96113']
  },
  'Site 512': {
    latLong: [30.9950860871879, 34.487135232229754],
    entity: 'site 512',
    title: 'Site 512',
    addressLines: ['Mt. Har Qeren, Israel']
  },
  'Site 883 Life Support Area': {
    latLong: [30.9811793138774, 34.7642710959132],
    entity: 'mashabim air base',
    title: 'Site 883 Life Support Area (Mashabim Air Base)',
    controller: 'U.S. Army and IDF',
    addressLines: [
      'Tlalim, Israel' 
    ]
  },
  'Stuttgart Army Airfield': {
    latLong: [48.68185253679313, 9.199637925484266],
    entity: 'stuttgart airport',
    title: 'Stuttgart Army Airfield',
    controller: 'USAG Stuttgart',
    addressLines: [
      'Stuttgart Airport', '70629 Filderstadt, Germany'
    ]
  },
  'Tobyhanna Army Depot': {
    latLong: [41.18592403330883, -75.42916118391746],
    entity: 'tobyhanna army depot',
    title: 'Tobyhanna Army Depot',
    controller: 'IMCOM',
    addressLines: [
      'Coolbaugh Township, PA 18466'
    ]
  },
  'Tooele Army Depot': {
    latLong: [40.499290771508846, -112.33426409545952],
    entity: 'tooele army depot',
    title: 'Tooele Army Depot',
    controller: 'Joint Munitions Command',
    addressLines: [
      'Tooele, UT 84074'
    ]
  },
  'USAG Ansbach': {
    'latLong': [49.31322336609552, 10.644772946745881],
    'entity': 'u.s. army garrison ansbach',
    'title': 'USAG Ansbach',
    controller: 'IMCOM Europe',
    'addressLines': [
      'Ansbach, Germany'
    ]
  },
  'USAG Bavaria-Garmisch': {
    'latLong': [47.48365523183373, 11.062372134336226],
    'entity': 'u.s. army garrison bavaria - garmisch',
    'title': 'USAG Bavaria-Garmisch',
    controller: 'IMCOM Europe',
    'addressLines': [
      '82467 Garmisch-Partenkirchen, Germany'
    ]
  },
  'USAG Bavaria-Grafenwoehr': {
    'latLong': [49.71484676711955, 11.914038922374399],
    'entity': 'u.s. army garrison bavaria - grafenwoehr',
    'title': 'USAG Bavaria-Grafenwoehr',
    controller: 'IMCOM Europe',
    'addressLines': [
      '92655 Grafenwöhr, Germany'
    ]
  },
  'USAG Bavaria-Hohenfels': {
    'latLong': [49.225769975344754, 11.829674829809253],
    'entity': 'u.s. army garrison bavaria - hohenfels',
    'title': 'USAG Bavaria-Hohenfels',
    controller: 'IMCOM Europe',
    'addressLines': [
      '92366 Hohenfels, Germany'
    ]
  },
  'USAG Bavaria-Vilseck': {
    'latLong': [49.64648279827734, 11.869691184314581],
    'entity': 'u.s. army garrison bavaria - vilseck',
    'title': 'USAG Bavaria-Vilseck',
    controller: 'IMCOM Europe',
    'addressLines': [
      'Grafenwöhr, Germany'
    ]
  },
  'USAG Benelux': {
    'latLong': [50.49716492126092, 3.9786507192877165],
    'entity': 'u.s. army garrison benelux',
    'title': 'USAG Benelux (NATO SHAPE)',
    controller: 'IMCOM Europe',
    'addressLines': [
      '7010 Mons, Belgium'
    ]
  },
  'USAG Humphreys': {
    'latLong': [36.95825652208051, 127.02473770777337],
    'entity': 'u.s. army garrison humphreys',
    'title': 'USAG Humphreys',
    controller: 'IMCOM Pacific',
    'addressLines': [
      'Gyeonggi-do, South Korea'
    ]
  },
  'USAG Kwajalein Atoll': {
    'latLong': [8.72066517675677, 167.73137966450116],
    'entity': 'u.s. army garrison kwajalein atoll',
    'title': 'USAG Kwajalein Atoll',
    controller: 'IMCOM Pacific',
    'addressLines': [
      'Kwajalein Atoll, Marshall Islands'
    ]
  },
  'USAG Okinawa': {
    'latLong': [26.384404782766996, 127.73789189808467],
    'entity': 'u.s. army garrison okinawa',
    'title': 'USAG Okinawa',
    controller: 'IMCOM Pacific',
    'addressLines': [
      'Nakagami District', 'Okinawa 904-0304, Japan'
    ]
  },
  'USAG Rheinland-Pfalz': {
    'latLong': [49.44245606155357, 7.805867841800824],
    'entity': 'u.s. army garrison rheinland-pfalz',
    'title': 'USAG Rheinland-Pfalz',
    controller: 'IMCOM Europe',
    'addressLines': [
      '67657 Kaiserslautern, Germany'
    ]
  },
  'USAG Wiesbaden': {
    'latLong': [50.04260532206873, 8.326326174329926],
    'entity': 'u.s. army garrison wiesbaden',
    'title': 'USAG Wiesbaden',
    controller: 'IMCOM Europe',
    'addressLines': [
      '65205 Wiesbaden, Germany'
    ]
  },
  'USAG Yongsan-Casey': {
    'latLong': [37.91820971656798, 127.05797852237248],
    'entity': 'u.s. army garrison yongsan-casey',
    'title': 'USAG Yongsan-Casey',
    controller: 'IMCOM Pacific',
    'addressLines': [
      'Gyeonggi-do, South Korea'
    ]
  },
  'Watervliet Arsenal': {
    latLong: [42.7194090142213, -73.70711518936987],
    entity: 'watervliet arsenal',
    title: 'Watervliet Arsenal',
    controller: 'IMCOM',
    addressLines: ['1 Buffington St', 'Watervliet, NY 12189']
  },
  'Wheeler Army Airfield': {
    latLong: [21.481169957774277, -158.03883254546477],
    entity: 'wheeler army airfield',
    title: 'Wheeler Army Airfield',
    controller: 'USAG Hawaii',
    addressLines: ['BLDG 107', '745 Wright Ave', 'Wahiawa HI 96786']
  },
  'White Sands Missile Range': {
    latLong: [33.23643315418895, -106.35894806159747],
    entity: 'white sands missile range',
    title: 'White Sands Missile Range',
    controller: 'Test & Evaluation Command',
    addressLines: ['White Sands Missile Range, NM']
  },
  'Yakima Training Center': {
    latLong: [46.672982124108685, -120.45976532537331],
    entity: 'yakima training center',
    title: 'Yakima Training Center',
    controller: 'IMCOM',
    addressLines: [
      '968-1198 Firing Center Rd', 'Yakima, WA 98901'
    ]
  },
  'Yuma Proving Ground': {
    latLong: [32.85952216189724, -114.43960662319847],
    entity: 'yuma proving ground',
    title: 'Yuma Proving Ground',
    controller: 'ATEC',
    addressLines: [
      '301 C St', 'Yuma, AZ 85365'
    ]
  }
}
