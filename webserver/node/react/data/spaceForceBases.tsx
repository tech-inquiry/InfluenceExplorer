import * as React from 'react'

export const data = {
  'Buckley Space Force Base': {
    latLong: [39.70665317689733, -104.75441284010273],
    entity: 'buckley space force base',
    title: 'Buckley Space Force Base',
    controller: 'Space Base Delta 2',
    addressLines: [ 'Aurora, CO'],
  },
  'Cheyenne Mountain Space Force Station': {
    latLong: [38.744140088699986, -104.84590078430043],
    entity: 'cheyenne mountain space force station',
    title: 'Cheyenne Mountain Space Force Station',
    controller: 'Space Base Delta 1',
    addressLines: ['Colorado Springs, CO 80906'],
  },
  'Joint Base Cape Cod': {
    latLong: [41.663927615035064, -70.54728651564584],
    entity: 'joint base cape cod',
    title: 'Joint Base Cape Cod',
    controller: 'Space Base Delta 2',
    addressLines: ['Connery Ave', 'Buzzards Bay, MA 02542'],
  },
  'Kaena Point Space Force Station': {
    latLong: [21.54673605698693, -158.23979783559204],
    entity: 'kaena point space force station',
    title: 'Kaena Point Space Force Station',
    controller: 'Space Base Delta 1',
    addressLines: ['HI-93', 'Waialua, HI 96791'],
  },
  'Los Angeles Air Force Base': {
    latLong: [33.919169578387, -118.38109759848486],
    entity: 'los angeles air force base',
    title: 'Los Angeles Air Force Base',
    controller: 'Space Systems Command',
    addressLines: ['200 N Douglas St', 'El Segundo, CA 90245'],
  },
  'New Boston Space Force Station': {
    latLong: [42.94643002374208, -71.6281072413597],
    entity: 'new boston space force station',
    title: 'New Boston Space Force Station',
    controller: 'Space Base Delta 1',
    addressLines: ['New Boston, NH 03070'],
  },
  'Patrick Space Force Base': {
    latLong: [28.23549694564631, -80.60789829040458],
    entity: 'patrick space force base',
    title: 'Patrick Space Force Base',
    controller: 'Space Launch Delta 45',
    addressLines: ['Florida 32925'],
  },
  'Peterson and Schriever Space Force Base': {
    latLong: [38.82571476753706, -104.69956318431511],
    entity: 'peterson and schriever space force base',
    title: 'Peterson and Schriever Space Force Base',
    controller: 'Space Base Delta 1',
    addressLines: ['Colorado Springs, CO'],
  },
  'Pituffik Space Base': {
    latLong: [76.53113606237633, -68.70692248689058],
    entity: 'pituffik space base',
    title: 'Pituffik Space Base',
    controller: 'Space Base Delta 1',
    addressLines: ['Pituffik, Greenland'],
  },
  'Royal Air Force Oakhanger': {
    latLong: [51.11438137819342, -0.8931755698751774],
    entity: 'royal air force oakhanger',
    title: 'Royal Air Force Oakhanger',
    controller: '23rd Space Operations Squadron',
    addressLines: ['Bordon GU35 9JA', 'United Kingdom'],
  },
  'Vandenberg Space Force Base': {
    latLong: [34.75260733293764, -120.53978901095138],
    entity: 'vandenberg space force base',
    title: 'Vandenberg Space Force Base',
    controller: 'Space Launch Delta 30',
    addressLines: [
      '6 California Blvd',
      'Vandenberg Space Force Base, CA 93437',
    ],
  }
}
