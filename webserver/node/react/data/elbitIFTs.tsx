import * as React from 'react'

export const data = {
  'CAG-0096 [Tower003]': {
    latLong: [32.13328, -112.15897],
    title: 'CAG-0096 [Tower003]',
    addressLines: [
      'South of Maish Vaya, AZ',
      'Casa Grande Station',
      'Tucson Sector',
    ],
  },
  'CAG-Tower-1/CA-CAG-0430 [Tower004]': {
    latLong: [31.572975, -111.685136],
    title: 'CAG-Tower-1/CA-CAG-0430 [Tower004]',
    addressLines: [
      'East of San Miguel, AZ',
      'Casa Grande Station',
      'Tucson Sector',
    ],
  },
  'TCA-AJO-003 [Tower066]': {
    latLong: [31.9780523, -112.9995376],
    title: 'TCA-AJO-003 [Tower066]',
    addressLines: ['East of Lukeville, AZ', 'Ajo Station', 'Tucson Sector'],
  },
  'TCA-AJO-004 [Tower067]': {
    latLong: [32.20080234, -112.8947267],
    title: 'TCA-AJO-004 [Tower067]',
    addressLines: ['Southwest of Why, AZ', 'Ajo Station', 'Tucson Sector'],
  },
  'TCA-AJO-0446 [Tower070]': {
    latLong: [31.767324, -112.2581983],
    title: 'TCA-AJO-0446 [Tower070]',
    addressLines: ["Tohono O'odham, AZ", 'Ajo Station', 'Tucson Sector'],
  },
  'TCA-AJO-0448 [Tower071]': {
    latLong: [31.77220149, -112.4195815],
    title: 'TCA-AJO-0448 [Tower071]',
    addressLines: ["Tohono O'odham, AZ", 'Ajo Station', 'Tucson Sector'],
  },
  'TCA-AJO-170 [Tower072]': {
    latLong: [32.09544373, -112.7964141],
    title: 'TCA-AJO-170 [Tower072]',
    addressLines: ['South of Why, AZ', 'Ajo Station', 'Tucson Sector'],
  },
  'TCA-AJO-302 [Tower073]': {
    latLong: [32.130124, -113.085372],
    title: 'TCA-AJO-302 [Tower073]',
    addressLines: ['Boundary Camp, AZ', 'Ajo Station', 'Tucson Sector'],
  },
  'TCA-AJO-310 [Tower074]': {
    latLong: [31.8935224, -112.745851],
    title: 'TCA-AJO-310 [Tower074]',
    addressLines: ['East of Lukeville, AZ', 'Ajo Station', 'Tucson Sector'],
  },
  'TCA-AJO-0450 [Tower075]': {
    latLong: [31.80844, -112.54179],
    title: 'TCA-AJO-0450 [Tower075]',
    addressLines: ['East of Ali-Chuk, AZ', 'Ajo Station', 'Tucson Sector'],
  },
  'TCA-CAG-0432 [Tower076]': {
    latLong: [31.5833069, -111.7706895],
    title: 'TCA-CAG-0432 [Tower076]',
    addressLines: ['San Miguel, AZ', 'Casa Grande Station', 'Tucson Sector'],
  },
  'TCA-CAG-0434/CAG-Tower-2 [Tower077]': {
    latLong: [31.618347, -111.839197],
    title: 'TCA-CAG-0434/CAG-Tower-2 [Tower077]',
    addressLines: [
      'West of San Miguel, AZ',
      'Casa Grande Station',
      'Tucson Sector',
    ],
  },
  'TCA-CAG-0438 [Tower078]': {
    latLong: [31.65101903, -111.9903441],
    title: 'TCA-CAG-0438 [Tower078]',
    addressLines: [
      "Tohono O'odham, AZ",
      'Casa Grande Station',
      'Tucson Sector',
    ],
  },
  'TCA-CAG-0440 [Tower079]': {
    latLong: [31.67311045, -112.049583],
    title: 'TCA-CAG-0440 [Tower079]',
    addressLines: [
      "Tohono O'odham, AZ",
      'Casa Grande Station',
      'Tucson Sector',
    ],
  },
  'TCA-CAG-0442 [Tower080]': {
    latLong: [31.73492687, -112.1146996],
    title: 'TCA-CAG-0442 [Tower080]',
    addressLines: [
      "Tohono O'odham, AZ",
      'Casa Grande Station',
      'Tucson Sector',
    ],
  },
  'TCA-CAG-0444 [Tower081]': {
    latLong: [31.62072449, -111.9047341],
    title: 'TCA-CAG-0444 [Tower081]',
    addressLines: [
      "Tohono O'odham, AZ",
      'Casa Grande Station',
      'Tucson Sector',
    ],
  },
  'TCA-DGL-0364 [Tower082]': {
    latLong: [31.357467, -109.428583],
    title: 'TCA-DGL-0364 [Tower082]',
    addressLines: ['East of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'TCA-DGL-0366 [Tower083]': {
    latLong: [31.3399, -109.383217],
    title: 'TCA-DGL-0366 [Tower083]',
    addressLines: ['East of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'TCA-DGL-0368 [Tower084]': {
    latLong: [31.34625, -109.30995],
    title: 'TCA-DGL-0368 [Tower084]',
    addressLines: ['East of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'TCA-DGL-0372 [Tower085]': {
    latLong: [31.379017, -109.2101],
    title: 'TCA-DGL-0372 [Tower085]',
    addressLines: ['East of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'TCA-DGL-0374 [Tower086]': {
    latLong: [31.38525, -109.1607],
    title: 'TCA-DGL-0374 [Tower086]',
    addressLines: ['East of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'TCA-DGL-0384 [Tower087]': {
    latLong: [31.44059, -109.47396],
    title: 'TCA-DGL-0384 [Tower087]',
    addressLines: ['North of Douglas', 'Douglas Station', 'Tucson Sector'],
  },
  'TCA-DGL-0388 [Tower088]': {
    latLong: [31.34302002, -109.1229485],
    title: 'TCA-DGL-0388 [Tower088]',
    addressLines: ['East of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'TCA-DGL-0390 [Tower089]': {
    latLong: [31.35516, -109.12614],
    title: 'TCA-DGL-0390 [Tower089]',
    addressLines: ['East of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'TCA-DGL-0396 [Tower090]': {
    latLong: [31.3377, -109.41082],
    title: 'TCA-DGL-0396 [Tower090]',
    addressLines: ['East of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'TCA-DGL-0428 [Tower091]': {
    latLong: [31.35726, -109.2668],
    title: 'TCA-DGL-0428 [Tower091]',
    addressLines: ['East of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'TCA-NGL-0046 [Tower097]': {
    latLong: [31.354483, -111.056933],
    title: 'TCA-NGL-0046 [Tower097]',
    addressLines: ['West of Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'TCA-NGL-0047 [Tower098]': {
    latLong: [31.39167, -111.0538],
    title: 'TCA-NGL-0047 [Tower098]',
    addressLines: ['West of Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'TCA-NGL-0049 [Tower099]': {
    latLong: [31.35317887, -111.0347527],
    title: 'TCA-NGL-0049 [Tower099]',
    addressLines: ['West of Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'TCA-NGL-0052 [Tower100]': {
    latLong: [31.358533, -110.87435],
    title: 'TCA-NGL-0052 [Tower100]',
    addressLines: ['East of Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'TCA-NGL-0054 [Tower101]': {
    latLong: [31.35229, -110.77514],
    title: 'TCA-NGL-0054 [Tower101]',
    addressLines: ['East of Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'TCA-NGL-0285 [Tower102]': {
    latLong: [31.410269, -110.824808],
    title: 'TCA-NGL-0285 [Tower102]',
    addressLines: ['East of Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'TCA-NGL-0316 [Tower103]': {
    latLong: [31.64716109, -111.0666001],
    title: 'TCA-NGL-0316 [Tower103]',
    addressLines: ['North of Tubac, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'TCA-SON-056 [Tower109]': {
    latLong: [31.356372, -110.652059],
    title: 'TCA-SON-056 [Tower109]',
    addressLines: [
      'Northwest of Lochiel, AZ',
      'Sonoita Station',
      'Tucson Sector',
    ],
  },
  'TCA-SON-0057 [Tower110]': {
    latLong: [31.351367, -110.55935],
    title: 'TCA-SON-0057 [Tower110]',
    addressLines: ['East of Lochiel, AZ', 'Sonoita Station', 'Tucson Sector'],
  },
  'TCA-SON-0058 [Tower111]': {
    latLong: [31.398682, -110.432175],
    title: 'TCA-SON-0058 [Tower111]',
    addressLines: ['West of Hereford, AZ', 'Sonoita Station', 'Tucson Sector'],
  },
  'TCA-SON-0059 [Tower112]': {
    latLong: [31.37875, -110.402367],
    title: 'TCA-SON-0059 [Tower112]',
    addressLines: ['East of Lochiel, AZ', 'Sonoita Station', 'Tucson Sector'],
  },
  'TCA-SON-0060 [Tower113]': {
    latLong: [31.373367, -110.354367],
    title: 'TCA-SON-0060 [Tower113]',
    addressLines: [
      'West of Miracle Valley, AZ',
      'Sonoita Station',
      'Tucson Sector',
    ],
  },
  'TCA-SON-0061 [Tower114]': {
    latLong: [31.35915, -110.30267],
    title: 'TCA-SON-0061 [Tower114]',
    addressLines: [
      'West of Miracle Valley, AZ',
      'Sonoita Station',
      'Tucson Sector',
    ],
  },
  'TCA-SON-0062 [Tower115]': {
    latLong: [31.34989697, -110.2856119],
    title: 'TCA-SON-0062 [Tower115]',
    addressLines: [
      'Coronado Peak Trailhead, AZ',
      'Sonoita Station',
      'Tucson Sector',
    ],
  },
  'TCA-SON-0314 [Tower116]': {
    latLong: [31.3417071, -110.6952233],
    title: 'TCA-SON-0314 [Tower116]',
    addressLines: ['East of Nogales, AZ', 'Sonoita Station', 'Tucson Sector'],
  },
  'TCA-TUS-036 [Tower117]': {
    latLong: [31.4845991, -111.3773244],
    title: 'TCA-TUS-036 [Tower117]',
    addressLines: ['South of Arivaca, AZ', 'Tucson Station', 'Tucson Sector'],
  },
  'TCA-TUS-041 [Tower118]': {
    latLong: [31.52529686, -111.2892159],
    title: 'TCA-TUS-041 [Tower118]',
    addressLines: ['South of Arivaca, AZ', 'Tucson Station', 'Tucson Sector'],
  },
  'TCA-TUS-290 [Tower119]': {
    latLong: [31.59329664, -111.349464],
    title: 'TCA-TUS-290 [Tower119]',
    addressLines: ['Arivaca, AZ', 'Tucson Station', 'Tucson Sector'],
  },
  'TCA-AJO-301 [Tower187]': {
    latLong: [31.881062, -112.815131],
    title: 'TCA-AJO-301 [Tower187]',
    addressLines: [
      'Lukeville/Sonoyta Crossing, AZ',
      'Ajo Station',
      'Tucson Sector',
    ],
  },
  'IFT [Tower195]': {
    latLong: [31.64621656, -111.5003347],
    title: 'IFT [Tower195]',
    addressLines: [
      'Buenos Aires National Wildlife Refuge, AZ',
      'Tucson Station',
      'Tucson Sector',
    ],
  },
  'IFT [Tower233]': {
    latLong: [31.36338469, -109.6829957],
    title: 'IFT [Tower233]',
    addressLines: ['Cochise College, AZ', 'Douglas Station', 'Tucson Sector'],
  },
}
