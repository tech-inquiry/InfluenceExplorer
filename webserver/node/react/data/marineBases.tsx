import * as React from 'react'

export const data = {
  'Camp Courtney': {
    latLong: [26.39014325459057, 127.85968626939173],
    entity: 'camp courtney',
    title: 'Camp Courtney (MCBC Smedley D. Butler)',
    controller: 'MCI Pacific', 
    'addressLines': [
      '941 Tengan',
      'Uruma, Okinawa 904-2202, Japan'
    ]
  },
  'Camp Foster': {
    latLong: [26.298567816445132, 127.76965720944897],
    entity: 'camp foster',
    title: 'Camp Foster (MCBC Smedley D. Butler)',
    controller: 'MCI Pacific',
    addressLines: [
      'Aniya, Ginowan',
      'Okinawa 901-2200, Japan'
    ]
  },
  'Camp Gonsalves': {
    latLong: [26.684857467798746, 128.23254033184537],
    entity: 'camp gonsalves',
    title: 'Camp Gonsalves (MCBC Smedley D. Butler)',
    controller: 'MCI Pacific',
    addressLines: [
      'Higashi, Kunigami District',
      'Okinawa 905-1201, Japan'
    ]
  },
  'Camp H. M. Smith': {
    latLong: [21.385718977448622, -157.90854707143131],
    entity: 'camp h. m. smith',
    title: 'Camp H. M. Smith',
    controller: 'MCB Hawaii',
    addressLines: ['Halawa, HI 96701']
  },
  'Camp Hansen': {
    latLong: [26.461858243229536, 127.91415585821692],
    entity: 'camp hansen',
    title: 'Camp Hansen (MCBC Smedley D. Butler)',
    controller: 'MCI Pacific',
    addressLines: [
      'Kunigami District',
      'Okinawa 904-1201, Japan'
    ]
  },
  'Camp Kinser': {
    latLong: [26.254882518049037, 127.70508722662028],
    entity: 'camp kinser',
    title: 'Camp Kinser (MCBC Smedley D. Butler)',
    controller: 'MCI Pacific',
    addressLines: [
      'Yafuso, Urasoe',
      'Okinawa 901-2127, Japan'
    ]
  },
  'Camp McTureous': {
    latLong: [26.3807776504011, 127.84513465814342],
    entity: 'camp mctureous',
    title: 'Camp McTureous (MCBC Smedley D. Butler)',
    controller: 'MCI Pacific',
    addressLines: [
      'Uruma, Okinawa 904-2204, Japan'
    ]
  },
  'Camp Mujuk': {
    latLong: [35.960312020797794, 129.425806690897],
    entity: 'camp mujuk',
    title: 'Camp Mujuk',
    controller: 'MCI Pacific',
    addressLines: [
      'Pohang-si',
      'Gyeongsangbuk-do, South Korea'
    ]
  },
  'Camp Panzer Kaserne': {
    latLong: [48.685751753511056, 9.043575864155638],
    entity: 'camp panzer kaserne',
    title: 'Camp Panzer Kaserne (USAG Stuttgart)',
    controller: 'MARFORCEUR/AF',
    addressLines: [
      'Panzer Strasse',
      'Boeblingen-Stuttgart, 71032'
    ]
  },
  'Camp Schwab': {
    latLong: [26.52478371366478, 128.04062133982214],
    entity: 'camp schwab',
    title: 'Camp Schwab (MCBC Smedley D. Butler)',
    controller: 'MCI Pacific',
    addressLines: [
      '1985 Henoko, Nago',
      'Okinawa 905-2171, Japan'
    ]
  },
  'Combined Arms Training Center Camp Fuji': {
    latLong: [35.32105798842141, 138.8662007955729],
    entity: 'combined arms training center camp fuji',
    title: 'CATC Camp Fuji (MCBC Smedley D. Butler)',
    controller: 'MCI Pacific',
    addressLines: [
      'Nakabata, Gotemba',
      'Shizuoka 412-0006, Japan'
    ]
  },
  'Joint Base Myer-Henderson Hall': {
    latLong: [38.87230296985544, -77.07896111085194],
    entity: 'joint base myer-henderson hall',
    title: 'Joint Base Myer-Henderson Hall',
    controller: 'MCI East',
    addressLines: ['Fort Myer, VA 22211']
  },
  'Marine Barracks Washington, D.C.': {
    latLong: [38.87991992373194, -76.9943144465549],
    entity: 'marine barracks washington, d.c.',
    title: 'Marine Barracks Washington',
    controller: 'MCI East',
    addressLines: ['8th St SE, Washington, DC 20003']
  },
  'Marine Corps Air Ground Combat Center': {
    latLong: [34.2407126014105, -116.06282702195726],
    entity: 'marine corps air ground combat center',
    title: 'Marine Corps Air Ground Combat Center',
    controller: 'MCI West',
    addressLines: [
      'Twentynine Palms, CA 92277'
    ]
  },
  'Marine Corps Air Station Beaufort': {
    latLong: [32.475579082336814, -80.71464453277542],
    entity: 'marine corps air station beaufort',
    title: 'Marine Corps Air Station Beaufort',
    controller: 'MCI East',
    addressLines: [
      'Geiger Blvd, Beaufort, SC 29904'
    ]
  },
  'Marine Corps Air Station Cherry Point': {
    latLong: [34.90322739139199, -76.89635902011253],
    entity: 'marine corps air station cherry point',
    title: 'Marine Corps Air Station Cherry Point',
    controller: 'MCI East',
    addressLines: [
      'MCAS Cherry Point, NC 28533'
    ]
  },
  'Marine Corps Air Station Futenma': {
    latLong: [26.272538718102258, 127.7550207246484],
    entity: 'marine corps air station futenma',
    title: 'Marine Corps Air Station Futenma',
    controller: 'MCI Pacific',
    addressLines: [
      'Ginowan, Okinawa 901-2211, Japan'
    ]
  },
  'Marine Corps Air Station Iwakuni': {
    latLong: [34.14287219161101, 132.22519422497146],
    entity: 'marine corps air station iwakuni',
    title: 'Marine Corps Air Station Iwakuni',
    controller: 'MCI Pacific',
    addressLines: [
      'Japan, 〒740-8555 Yamaguchi',
      'Iwakuni, Misumimachi, 2 Chome, 官有地'
    ]
  },
  'Marine Corps Air Station Miramar': {
    latLong: [32.87450918949354, -117.13784278160672],
    entity: 'marine corps air station miramar',
    title: 'Marine Corps Air Station Miramar',
    controller: 'MCI West',
    addressLines: [
      'Miramar Way, San Diego, CA 92145'
    ]
  },
  'Marine Corps Air Station New River': {
    latLong: [34.72777628862227, -77.47091901589994],
    entity: 'marine corps air station new river',
    title: 'Marine Corps Air Station New River',
    controller: 'MCI East',
    addressLines: [
      'Curtis Rd, Jacksonville, NC 28540'
    ]
  },
  'Marine Corps Air Station Yuma': {
    latLong: [32.65577076708793, -114.58177347839666],
    entity: 'marine corps air station yuma',
    title: 'Marine Corps Air Station Yuma',
    controller: 'MCI West',
    addressLines: ['Yuma, AZ 85365']
  },
  'Marine Corps Base Camp Blaz': {
    latLong: [13.563307011344802, 144.8327428258439],
    entity: 'marine corps base camp blaz',
    title: 'Marine Corps Base Camp Blaz',
    controller: 'MCI Pacific',
    addressLines: ['Dededo, Guam']
  },
  'Marine Corps Base Camp Lejeune': {
    latLong: [34.58360073548368, -77.36039895078221],
    entity: 'marine corps base camp lejeune',
    title: 'Marine Corps Base Camp Lejeune',
    controller: 'MCI East',
    addressLines: ['North Carolina 28547']
  },
  'Marine Corps Base Camp Pendleton': {
    latLong: [33.214741127483165, -117.38715055156487],
    entity: 'marine corps base camp pendleton',
    title: 'Marine Corps Base Camp Pendleton',
    controller: 'MCI West',
    addressLines: [
      '20250 Vandegrift Blvd, Oceanside, CA 92058'
    ]
  },
  'Marine Corps Base Camp Smedley D. Butler': {
    latLong: [26.30823962841211, 127.76683809644979],
    entity: 'marine corps base camp smedley d. butler',
    title: 'Marine Corps Base Camp Smedley D. Butler',
    controller: 'MCI Pacific',
    addressLines: [
      'Omura, Chatan',
      'Nakagami District',
      'Okinawa 904-0107, Japan'
    ]
  },
  'Marine Corps Base Hawaii': {
    latLong: [21.444580887976258, -157.75198406334732],
    entity: 'marine corps base hawaii',
    title: 'Marine Corps Base Hawaii',
    controller: 'MCI Pacific',
    addressLines: ['MCBH, HI 96863']
  },
  'Marine Corps Base Quantico': {
    latLong: [38.51957475163969, -77.36702690794517],
    entity: 'marine corps base quantico',
    title: 'Marine Corps Base Quantico',
    controller: 'MCICOM',
    addressLines: ['Quantico, VA']
  },
  'Marine Corps Logistics Base Albany': {
    latLong: [31.5507902321527, -84.06383685261923],
    entity: 'marine corps logistics base albany',
    title: 'Marine Corps Logistics Base Albany',
    controller: 'MCI East',
    addressLines: [
      '814 Radford Blvd Suite 20306, Albany, GA 31704'
    ]
  },
  'Marine Corps Logistics Base Barstow': {
    latLong: [34.87696432459484, -116.95389678032812],
    entity: 'marine corps logistics base barstow',
    title: 'Marine Corps Logistics Base Barstow',
    controller: 'MCI West',
    addressLines: [
      'Joseph L Boll Ave, Barstow, CA 92311'
    ]
  },
  'Marine Corps Mountain Warfare Training Center': {
    latLong: [38.3574870888294, -119.51402134061584],
    entity: 'marine corps mountain warfare training center',
    title: 'Marine Corps Mountain Warfare Training Center',
    controller: 'MCI West',
    addressLines: [
      'MWTC HC83, Bridgeport, CA 93517'
    ]
  },
  'Marine Corps Recruit Depot San Diego': {
    latLong: [32.74166983878236, -117.19781680730094],
    entity: 'marine corps recruit depot san diego',
    title: 'Marine Corps Recruit Depot San Diego',
    controller: 'MCI West',
    addressLines: [
      '38990 Midway Ave',
      'San Diego, CA 92140'
    ]
  },
  'Marine Corps Support Facility Blount Island': {
    latLong: [30.406105968019865, -81.52194328306778],
    entity: 'marine corps support facility blount island',
    title: 'MCSF Blount Island',
    controller: 'MCI East',
    addressLines: [
      '5880 Channel View Blvd', 'Jacksonville, FL 32226'
    ]
  },
  'Marine Corps Support Facility New Orleans': {
    latLong: [29.949415551951628, -90.03678681956517],
    entity: 'marine corps support facility new orleans',
    title: 'MCSF New Orleans',
    controller: 'MCI East',
    addressLines: [
      '2000 Opelousas Ave', 'New Orleans, LA 70114'
    ]
  },
  'Robertson Barracks': {
    latLong: [-12.43745788323409, 130.9761486209739],
    entity: 'robertson barracks',
    title: 'Robertson Barracks',
    controller: 'MCI Pacific',
    addressLines: [
      'Suvla Bay Rd',
      'Palmerston City NT 0831, Australia'
    ]
  }
}
