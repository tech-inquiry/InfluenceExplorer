import * as React from 'react'

export const data = [
  {
    date: '2025-03-11',
    item: (
      <a href="https://defensescoop.com/2025/03/11/trump-nominates-michael-obadal-army-undersecretary-anduril/">
        Trump nominates Anduril executive, former special operations officer to be Army undersecretary 
      </a>
    ),
  },
  {
    date: '2025-03-11',
    item: (
      <a href="https://www.theguardian.com/us-news/2025/mar/11/usaid-staff-documents">
        USAid employees told to destroy classified documents, email shows
      </a>
    ),
  },
  {
    date: '2025-03-10',
    item: (
      <a href="https://www.nytimes.com/2025/03/10/technology/eric-schmidt-relativity-space.html">
        Eric Schmidt joins Relativity Space, a rocket start-up, as C.E.O.
      </a>
    ),
  },
  {
    date: '2025-03-10',
    item: (
      <a href="https://www.thecrimson.com/article/2025/3/10/naftali-bennett-hbs-talk/">
        At Harvard talk, former Israeli PM joked he would give exploding pagers to protesters
      </a>
    ),
  },
  {
    date: '2025-03-09',
    item: (
      <a href="https://apnews.com/article/columbia-university-mahmoud-khalil-ice-15014bcbb921f21a9f704d5acdcae7a8">
        ICE arrests Palestinian activist who helped lead Columbia University protests, his lawyer says
      </a>
    ),
  },
  {
    date: '2025-03-07',
    item: (
      <a href="https://apnews.com/article/columbia-university-protests-antisemitism-palestine-israel-9c209ce040e4b60d2702b40b9c2fb321">
        Trump administration cancels $400M in grants and contracts with Columbia University
      </a>
    ),
  },
  {
    date: '2025-03-05',
    item: (
      <a href="https://jackpoulson.substack.com/p/white-house-situation-room-is-depending">
        White House Situation Room is depending on live Belgian corporate intel feed to respond to terrorist hijackings
      </a>
    ),
  },
  {
    date: '2025-03-04',
    item: (
      <a href="https://jackpoulson.substack.com/p/where-data-journalism-meets-information">
        Where data journalism meets information warfare against China
      </a>
    ),
  },
  {
    date: '2025-02-28',
    item: (
      <a href="https://www.wsj.com/world/americas/mexico-cartel-us-drone-boss-capture-a0c8e429">
        Secret U.S. drone program helped capture Mexican cartel bosses
      </a>
    ),
  },
  {
    date: '2025-02-28',
    item: (
      <a href="https://fedscoop.com/gsa-tells-agencies-to-terminate-contracts-with-top-10-consulting-firms/">
         GSA tells agencies to terminate contracts with top-10 consulting firms 
      </a>
    ),
  },
  {
    date: '2025-02-26',
    item: (
      <a href="https://www.theguardian.com/media/2025/feb/26/jeff-bezos-washington-post-opinion">
        Bezos directs Washington Post opinion pages to promote ‘personal liberties and free markets’
      </a>
    ),
  },
  {
    date: '2025-02-25',
    item: (
      <a href="https://www.dropsitenews.com/p/israel-launches-major-air-assault">
        Israel launches major air assault on Damascus 
      </a>
    ),
  },
  {
    date: '2025-02-23',
    item: (
      <a href="https://archive.ph/7bywD">
        Trump’s frustration with generals resulted in an unconventional pick
      </a>
    ),
  },
  {
    date: '2025-02-21',
    item: (
      <a href="https://www.bbc.com/news/articles/cgj54eq4vejo">
        Apple pulls data protection tool after UK government security row
      </a>
    ),
  },
  {
    date: '2025-02-21',
    item: (
      <a href="https://archive.ph/xmCVR">
        These are the SpaceX engineers already working inside the FAA
      </a>
    ),
  },
  {
    date: '2025-02-19',
    item: (
      <a href="https://archive.ph/wx89A">
        Clearview AI’s CEO resigns As facial recognition company focuses on Trump 'opportunities'
      </a>
    ),
  },
  {
    date: '2025-02-19',
    item: (
      <a href="https://archive.ph/AynuH">
        Pro-Palestinian protesters, far-right Jewish group clash in ultra-Orthodox Brooklyn neighborhood
      </a>
    ),
  },
  {
    date: '2025-02-14',
    item: (
      <a href="https://www.lemonde.fr/en/international/article/2025/02/14/united-arab-emirates-break-arab-front-against-trump-s-gaza-plan_6738156_4.html">
        United Arab Emirates break Arab front against Trump's Gaza plan
      </a>
    ),
  },
  {
    date: '2025-02-14',
    item: (
      <a href="https://www.theguardian.com/us-news/2025/feb/14/white-house-ap-ban-oval-office">
        White House bans AP journalists from Oval Office amid continued Gulf dispute
      </a>
    ),
  },
  {
    date: '2025-02-13',
    item: (
      <a href="https://www.dropsitenews.com/p/elon-musk-armored-tesla-forecast-400-million-state-department-contracts">
        'Armored Tesla' (was) forecast to win $400 million State Department contract after Trump's election
      </a>
    ),
  },
  {
    date: '2025-02-12',
    item: (
      <a href="https://www.forbes.com/sites/davidjeans/2025/02/12/pentagon-doge-elon-musk/">
        A mad scramble inside the Pentagon ahead of DOGE's arrival
      </a>
    ),
  },
  {
    date: '2025-02-07',
    item: (
      <a href="https://jackpoulson.substack.com/p/cia-adjacent-vc-community-convenes">
        CIA-adjacent VC community convenes in Boston during ice storm
      </a>
    ),
  },
  {
    date: '2025-02-07',
     item: (
      <a href="https://www.theguardian.com/us-news/2025/feb/07/israel-weapons-sale-gaza">
        US approves $7.4bn sale of more weapons to Israel used to ravage Gaza
      </a>
    ),
  },
  {
    date: '2025-02-05',
    item: (
      <a href="https://jackpoulson.substack.com/p/bibis-edgelords">
        Bibi's edgelords
      </a>
    ),
  },
  {
    date: '2025-02-04',
    item: (
      <a href="https://www.wsj.com/politics/national-security/the-cia-is-about-to-get-a-trump-makeover-16fc0cbf">
        CIA offers buyout to entire workforce as part of Trump makeover
      </a>
    ),
  },
  {
    date: '2025-02-04',
    item: (
      <a href="https://sf.gazetteer.co/judge-tosses-sf-lawsuit-that-spurred-streisand-effect-for-tech-execs-arrest">
        Judge tosses SF lawsuit that spurred Streisand Effect for tech exec’s arrest
      </a>
    ),
  },
  {
    date: '2025-02-03',
    item: (
      <a href="https://www.404media.co/senator-hawley-proposes-jail-time-for-people-who-download-deepseek/">
        Senator Hawley proposes jail time for people who download DeepSeek
      </a>
    ),
  },
  {
    date: '2025-02-03',
    item: (
      <a href="https://apnews.com/article/trump-tiktok-wealth-fund-saudi-arabia-b71c92cb0deb1eb39352cd1cd4db4b98">
        Trump orders creation of US sovereign wealth fund, says it could own part of TikTok
      </a>
    ),
  },
  {
    date: '2025-02-03',
    item: (
      <a href="https://www.theguardian.com/us-news/2025/feb/03/usaid-closed-trump-musk">
        Marco Rubio appoints himself head of USAid as workers locked out of office
      </a>
    ),
  },
  {
    date: '2025-02-02',
    item: (
      <a href="https://www.wired.com/story/elon-musk-government-young-engineers/">
        The young, inexperienced engineers aiding Elon Musk’s government takeover
      </a>
    ),
  },
  {
    date: '2025-02-02',
    item: (
      <a href="https://www.theguardian.com/us-news/2025/feb/02/usaid-officials-put-on-leave-musk-doge">
        Senior USAid officials put on leave after denying access to Musk’s Doge team
      </a>
    ),
  },
  {
    date: '2025-01-31',
    item: (
      <a href="https://www.reuters.com/world/any-forced-halt-unrwas-work-would-jeopardise-gaza-ceasefire-agency-says-2025-01-31/">
        UNRWA carries on aid work despite Israeli ban, hostilities
      </a>
    ),
  },
  {
    date: '2025-01-31',
    item: (
      <a href="https://www.politico.com/news/2025/01/31/trump-usaid-shutdown-state-department-00201760">
        Trump is wreaking havoc at USAID. Is the goal to shut it down? 
      </a>
    ),
  },
  {
    date: '2025-01-30',
    item: (
      <a href="https://www.businessinsider.com/trump-fbi-director-kash-patel-financial-disclosure-2025-1">
        Trump FBI pick Kash Patel made more than $2.6 million from consulting, paid speeches, and books last year
      </a>
    ),
  },
  {
    date: '2025-01-30',
    item: (
      <a href="https://www.dropsitenews.com/p/israel-palestine-dox-new-york-facial-recognition-ai">
        “Operation Wrath of Zion" aims to dox and deport pro-Palestinian protestors in New York City
      </a>
    ),
  },
  {
    date: '2025-01-30',
    item: (
      <a href="https://www.theguardian.com/us-news/2025/jan/29/aircraft-crash-ronald-reagan-airport-virginia">
        Rescuers search Washington DC river after plane collides with military helicopter
      </a>
    ),
  },
  {
    date: '2025-01-29',
    item: (
      <a href="https://www.timesofisrael.com/employee-at-canadian-pro-israel-media-watchdog-charged-for-anti-palestinian-graffiti/">
        Employee at Canadian pro-Israel media watchdog charged for anti-Palestinian graffiti
      </a>
    ),
  },
  {
    date: '2025-01-28',
    item: (
      <a href="https://jackpoulson.substack.com/p/microsofts-internal-whitepaper-for">
        Microsoft's whitepaper for the U.S. Army's 'fighting google' and integration with Maven through Project Convergence 
      </a>
    ),
  },
  {
    date: '2025-01-27',
    item: (
      <a href="https://www.theguardian.com/world/2025/jan/27/israel-gaza-ceasefire-qatar-civilian-hostage-palestinians-statement">
        Tens of thousands of Palestinians return to north Gaza as Israel opens checkpoints
      </a>
    ),
  },
  {
    date: '2025-01-26',
    item: (
      <a href="https://www.theguardian.com/us-news/2025/jan/26/trump-resumes-sending-2000-pound-bombs-to-israel-undoing-biden-pause">
        Trump’s Gaza proposal rejected by allies and condemned as ethnic cleansing plan
      </a>
    ),
  },
  {
    date: '2025-01-26',
    item: (
      <a href="https://jackpoulson.substack.com/p/gaza-checkpoint-shell-company-outs">
        Gaza checkpoint shell company outs itself as led by former CIA paramilitary chief
      </a>
    ),
  },
  {
    date: '2025-01-25',
    item: (
      <a href="https://jackpoulson.substack.com/p/day-7-of-gaza-ceasefire-us-manned">
        Day 7 of Gaza ceasefire: U.S.-manned Netzarim corridor vehicle checkpoint set to open today
      </a>
    ),
  },
  {
    date: '2025-01-25',
    item: (
      <a href="https://electronicintifada.net/content/eis-ali-abunimah-arrested-switzerland/50333">
        [Journalist] Ali Abunimah arrested in Switzerland
      </a>
    ),
  },
  {
    date: '2025-01-24',
    item: (
      <a href="https://jackpoulson.substack.com/p/gaza-checkpoint-contractor-is-shell">
        Gaza checkpoint contractor is shell company of Wyoming-based 'generational wealth management' firm
      </a>
    ),
  },
  {
    date: '2025-01-23',
    item: (
      <a href="https://sf.gazetteer.co/a-former-tech-ceo-is-on-a-crusade-to-get-the-record-of-his-arrest-removed-from-the-internet">
        A former tech CEO is on a crusade to get the record of his arrest removed from the internet
      </a>
    ),
  },
  {
    date: '2025-01-23',
    item: (
      <a href="https://www.dropsitenews.com/p/microsoft-azure-israel-top-customer-ai-cloud">
        The Israeli military is one of Microsoft's top AI customers, leaked documents reveal
      </a>
    ),
  },
  {
    date: '2025-01-21',
    item: (
      <a href="https://www.defenseone.com/policy/2025/01/trump-orders-military-plan-seal-borders/402360/">
        Trump orders military to plan to ‘seal the borders’ 
      </a>
    ),
  },
  {
    date: '2025-01-21',
    item: (
      <a href="https://www.cnn.com/2025/01/21/tech/openai-oracle-softbank-trump-ai-investment/index.html">
         Trump announces a $500 billion AI infrastructure investment in the US 
      </a>
    ),
  },
  {
    date: '2025-01-21',
    item: (
      <a href="https://www.theguardian.com/us-news/2025/jan/21/trump-un-elise-stefanik-israel">
        Trump UN nominee backs Israeli claims of biblical rights to West Bank
      </a>
    ),
  },
  {
    date: '2025-01-20',
    item: (
      <a href="https://www.businessinsider.com/chinese-spies-silicon-valley-tech-companies-freaking-out-espionage-employees-2025-1">
        Silicon Valley's new Red Scare
      </a>
    ),
  },
  {
    date: '2025-01-20',
    item: (
      <a href="https://jackpoulson.substack.com/p/from-cia-station-chief-to-ai-chatbots">
        From CIA station chief to AI chatbots to Trump-adjacent lobbying
      </a>
    ),
  },
  {
    date: '2025-01-19',
    item: (
      <a href="https://www.dropsitenews.com/p/hamas-will-uphold-ceasefire-deal-israel-gaza-exclusive-interview">
        Hamas intends to uphold agreement, senior official tells Drop Site
      </a>
    ),
  },
  {
    date: '2025-01-16',
    item: (
      <a href="https://newrepublic.com/post/190350/antony-blinken-journalist-questions-gaza">
        Antony Blinken kicks out journalist for asking questions about Gaza
      </a>
    ),
  },
  {
    date: '2025-01-16',
    item: (
      <a href="https://www.rollingstone.com/politics/politics-features/john-ratcliffe-trump-cia-clients-ai-defense-contractors-1235238009/">
        Trump’s CIA nominee discloses clients
      </a>
    ),
  },
  {
    date: '2025-01-14',
    item: (
      <a href="https://www.aljazeera.com/news/liveblog/2025/1/15/live-israel-launches-fierce-strikes-on-gaza-as-ceasefire-deal-moves-closer">
        Hamas approves proposal for Gaza ceasefire deal with Israel
      </a>
    ),
  },
  {
    date: '2025-01-13',
    item: (
      <a href="https://www.washingtonpost.com/politics/2025/01/13/andreessen-tech-industry-trump-administration-doge/">
        [Marc Andreessen is] helping shape the Trump administration
      </a>
    ),
  },
  {
    date: '2025-01-10',
    item: (
      <a href="https://www.politico.eu/article/poland-general-fired-after-missing-anti-tank-mines-were-found-in-ikea/">
        Polish general fired after missing anti-tank mines were found in IKEA 
      </a>
    ),
  },
  {
    date: '2025-01-09',
    item: (
      <a href="https://www.wired.com/story/gravy-location-data-app-leak-rtb/">
        Candy Crush, Tinder, MyFitnessPal: See the thousands of apps hijacked to spy on your location
      </a>
    ),
  },
  {
    date: '2025-01-09',
    item: (
      <a href="https://www.nytimes.com/2025/01/09/us/politics/icc-sanctions-house-israel.html">
        House passes bill to sanction I.C.C. officials for Israeli prosecutions
      </a>
    ),
  },
  {
    date: '2025-01-09',
    item: (
      <a href="https://www.theguardian.com/technology/2025/jan/09/google-microsoft-donate-trump-inaugural-fund">
        Google and Microsoft donate $1m each to Trump’s inaugural fund
      </a>
    ),
  },
  {
    date: '2025-01-09',
    item: (
      <a href="https://www.forbes.com/sites/sarahemerson/2025/01/09/eric-schmidts-new-secret-project-is-an-ai-video-platform-called-hooglee/">
        Eric Schmidt's new secret project is an AI video platform called 'Hooglee'
      </a>
    ),
  },
  {
    date: '2025-01-02',
    item: (
      <a href="https://www.military.com/daily-news/2025/01/02/man-connected-tesla-cybertruck-explosion-trump-hotel-was-active-duty-green-beret.html">
        Active-duty Special Forces soldier connected to Trump Hotel Tesla truck explosion
      </a>
    ),
  },
  {
    date: '2024-12-30',
    item: (
      <a href="https://www.reuters.com/world/biden-announces-25-billion-fresh-military-aid-ukraine-2024-12-30/">
        U.S. announces $5.9 billion in military and budget aid to Ukraine
      </a>
    ),
  },
  {
    date: '2024-12-29',
    item: (
      <a href="https://www.timesofisrael.com/foreign-ministry-to-receive-massive-budget-for-public-diplomacy-abroad/amp/">
        [Israeli] Foreign Ministry to receive massive budget for public diplomacy abroad
      </a>
    ),
  },
  {
    date: '2024-12-27',
    item: (
      <a href="https://www.spiegel.de/netzwelt/web/volkswagen-konzern-datenleck-wir-wissen-wo-dein-auto-steht-a-e12d33d0-97bc-493c-96d1-aa5892861027">
        [Location data and owner information from 800,000 electric cars were unprotected on the Internet.]
      </a>
    ),
  },
  {
    date: '2024-12-26',
    item: (
      <a href="https://www.theguardian.com/world/2024/dec/26/five-palestinian-journalists-killed-israeli-strike-gaza">
        Five Palestinian journalists killed in Israeli strike on van in Gaza
      </a>
    ),
  },
  {
    date: '2024-12-24',
    item: (
      <a href="https://www.wsj.com/business/trump-inauguration-sponsors-large-company-donations-a3cc1b92">
        The big companies funding Trump’s inauguration despite denouncing Jan. 6
      </a>
    ),
  },
  {
    date: '2024-12-22',
    item: (
      <a href="https://www.kenklippenstein.com/p/missing-congresswoman-transferred">
        Missing Congresswoman transferred home deed in July
      </a>
    ),
  },
  {
    date: '2024-12-20',
    item: (
      <a href="https://apnews.com/article/syria-united-states-austin-tice-assad-de71dc4920d4677fcfb2d31a2b6e6e54">
        US drops $10M terrorism bounty offered for capture of Syrian rebel leader who ousted Assad
      </a>
    ),
  },
  {
    date: '2024-12-20',
    item: (
      <a href="https://www.bloomberg.com/news/newsletters/2024-12-20/israeli-spyware-maker-bought-by-us-private-equity-in-rare-move">
        Israeli spyware maker bought by US private equity in rare move
      </a>
    ),
  },
  {
    date: '2024-12-19',
    item: (
      <a href="https://www.courthousenews.com/federal-judge-blocks-california-law-preventing-reporting-of-sealed-arrest-records/">
        Federal judge blocks California law preventing reporting of sealed arrest records
      </a>
    ),
  },
  {
    date: '2024-12-19',
    item: (
      <a href="https://www.haaretz.com/israel-news/2024-12-18/ty-article-magazine/.premium/idf-soldiers-expose-arbitrary-killings-and-rampant-lawlessness-in-gazas-netzarim-corridor/00000193-da7f-de86-a9f3-fefff2e50000">
        'No civilians. Everyone's a terrorist': IDF soldiers expose arbitrary killings and rampant lawlessness in Gaza's Netzarim corridor
      </a>
    ),
  },
  {
    date: '2024-12-17',
    item: (
      <a href="https://apnews.com/article/syria-war-assad-israel-news-17-december-2024-4d7f0b35e08d5e853c776193272fb2d6">
        Netanyahu says Israeli troops will occupy a buffer zone inside Syria for the foreseeable future
      </a>
    ),
  },
  {
    date: '2024-12-15',
    item: (
      <a href="https://verify-sy.com/en/details/10562/Did-CNN-Fabricate-the-Story-of--Freeing-a-Prisoner-from-a-Secret-Jail--">
        Did CNN fabricate the story of "freeing a prisoner from a secret jail"?
      </a>
    ),
  },
  {
    date: '2024-12-13',
    item: (
      <a href="https://www.npr.org/2024/12/13/nx-s1-5155962/mckinsey-purdue-opioid-prosecution-doj">
        McKinsey & Company to pay $650 million for role in opioid crisis
      </a>
    ),
  },
  {
    date: '2024-12-10',
    item: (
      <a href="https://www.politico.com/live-updates/2024/12/10/congress/trump-picks-china-hawk-helberg-to-be-top-state-department-economic-policy-official-00193683">
        Trump picks China hawk to be top State Department economic policy official
      </a>
    ),
  },
  {
    date: '2024-12-08',
    item: (
      <a href="https://www.bbc.com/news/articles/ce313jn453zo">
        Who are the rebels in Syria?
      </a>
    ),
  },
  {
    date: '2024-12-07',
    item: (
      <a href="https://www.theguardian.com/us-news/2024/dec/07/campaign-spending-crypto-tech-influence">
        The tech bosses who poured $394.1m into US election - and how they compared to Elon Musk
      </a>
    ),
  },
  {
    date: '2024-12-06',
    item: (
      <a href="https://www.bbc.com/news/articles/cn4x2epppego">
        Romanian court annuls result of presidential election first round
      </a>
    ),
  },
  {
    date: '2024-12-05',
    item: (
      <a href="https://www.reuters.com/world/us/trump-appoints-former-paypal-coo-david-sacks-ai-crypto-czar-2024-12-06/">
        Trump appoints former PayPal COO David Sacks as AI and crypto czar
      </a>
    ),
  },
  {
    date: '2024-12-03',
    item: (
      <a href="https://www.404media.co/ftc-bans-location-data-company-that-powers-the-surveillance-ecosystem/">
        FTC bans location data company that powers the surveillance ecosystem
      </a>
    ),
  },
  {
    date: '2024-12-03',
    item: (
      <a href="https://apnews.com/article/south-korea-yoon-martial-law-997c22ac93f6a9bece68454597e577c1">
        South Korean president declares emergency martial law, accusing opposition of anti-state activities
      </a>
    ),
  },
  {
    date: '2024-12-02',
    item: (
      <a href="https://apnews.com/article/2000-mules-film-dinesh-dsouza-apology-91d6c3c80e6c56e89684a12111f92319">
        Creator of ‘2000 Mules’ apologizes to Georgia man falsely accused of ballot fraud in the film
      </a>
    ),
  },
  {
    date: '2024-12-02',
    item: (
      <a href="https://stanforddaily.com/2024/12/02/jeff-hancock-court-declaration/">
        Stanford misinformation expert accused of using AI to fabricate court statement
      </a>
    ),
  },
  {
    date: '2024-12-02',
    item: (
      <a href="https://www.mediapart.fr/en/journal/international/021224/hidden-links-between-giant-investigative-journalism-and-us-government">
        The hidden links between a giant of investigative journalism and the US government
      </a>
    ),
  },
  {
    date: '2024-11-27',
    item: (
      <a href="https://www.reuters.com/business/energy/exxon-lobbyist-investigated-over-hack-and-leak-environmentalist-emails-sources-2024-11-27/">
        Exxon lobbyist investigated over hack-and-leak of environmentalist emails, sources say
      </a>
    ),
  },
  {
    date: '2024-11-25',
    item: (
      <a href="https://www.theguardian.com/world/2024/nov/25/killing-of-journalists-in-israeli-strike-could-be-a-war-legal-experts-say">
        Israel used US weapons [from Boeing and Woodward] in strike that killed journalists
      </a>
    ),
  },
  {
    date: '2024-11-21',
    item: (
      <a href="https://www.nytimes.com/2024/11/21/us/politics/pam-bondi-trump-ag-lobbyist.html">
        As a lobbyist, Bondi had clients including Amazon, G.M., Uber and Qatar
      </a>
    ),
  },
  {
    date: '2024-11-21',
    item: (
      <a href="https://europeanconservative.com/articles/news/german-journalist-could-face-prison-for-mocking-government-officials/">
        German journalist could face prison for mocking government officials
      </a>
    ),
  },
  {
    date: '2024-11-21',
    item: (
      <a href="https://www.theguardian.com/world/2024/nov/21/icc-issues-arrest-warrant-for-benjamin-netanyahu-israel">
        ICC issues arrest warrant for Benjamin Netanyahu for alleged Gaza war crimes
      </a>
    ),
  },
  {
    date: '2024-11-20',
    item: (
      <a href="https://apnews.com/article/ukraine-russia-war-land-mines-civilians-biden-76fb21ce12f70aa965d3d7fbccb35c9c">
        The US is sending antipersonnel land mines to Ukraine.
      </a>
    ),
  },
  {
    date: '2024-11-20',
    item: (
      <a href="https://www.bbc.com/news/articles/c4g704g051go">
        Ukraine fires UK-supplied Storm Shadow missiles at Russia for first time
      </a>
    ),
  },
  {
    date: '2024-11-12',
    item: (
      <a href="https://www.404media.co/email/f459caa7-1a58-4f31-a9ba-3cb53a5046a4/">
        'FYI. A warrant isn’t needed': Secret Service says you agreed to be tracked with location data
      </a>
    ),
  },
  {
    date: '2024-11-07',
    item: (
      <a href="https://www.businesswire.com/news/home/20241107699415/en/Anthropic-and-Palantir-Partner-to-Bring-Claude-AI-Models-to-AWS-for-U.S.-Government-Intelligence-and-Defense-Operations">
         Anthropic and Palantir partner to bring Claude AI models to AWS for U.S. Government intelligence and defense operations 
      </a>
    ),
  },
  {
    date: '2024-11-06',
    item: (
      <a href="https://open.substack.com/pub/jackpoulson/p/the-quiet-integration-of-project">
        The quiet integration of 'Project Maven' and the Pentagon's cloud with 'Minotaur' maritime border surveillance
      </a>
    ),
  },
  {
    date: '2024-11-05',
    item: (
      <a href="https://open.substack.com/pub/jackpoulson/p/pentagon-confirms-erasure-of-project">
        Pentagon confirms erasure of Project Maven-related contract records
      </a>
    ),
  },
  {
    date: '2024-11-02',
    item: (
      <a href="https://www.theguardian.com/us-news/2024/nov/02/microsoft-workers-fired-gaza-vigil">
        Microsoft workers fired over Gaza vigil say company ‘crumbled under pressure’
      </a>
    ),
  },
  {
    date: '2024-11-01',
    item: (
      <a href="https://federalnewsnetwork.com/cloud-computing/2024/11/nsa-goes-live-with-hybrid-compute-initiative/">
        NSA goes live with ‘Hybrid Compute Initiative’
      </a>
    ),
  },
  {
    date: '2024-10-31',
    item: (
      <a href="https://www.wired.com/story/trump-cia-venezuela-maduro-regime-change-plot/">
        The untold story of Trump's failed attempt to overthrow Venezuela's president
      </a>
    ),
  },
  {
    date: '2024-10-26',
    item: (
      <a href="https://open.substack.com/pub/jackpoulson/p/142-million-ai-drone-warfare-contract">
        $142 million AI drone warfare contract from 'Project Maven' was silently erased from public record
      </a>
    ),
  },
  {
    date: '2024-10-25',
    item: (
      <a href="https://www.theguardian.com/world/live/2024/oct/25/israel-lebanon-gaza-hamas-hezbollah-middle-east-latest">
        Israeli military says it it carrying out strikes on military targets in Iran
      </a>
    ),
  },
  {
    date: '2024-10-25',
    item: (
      <a href="https://theintercept.com/2024/10/25/africom-microsoft-openai-military/">
        Pentagon purchased OpenAI tools for military operations across Africa 
      </a>
    ),
  },
  {
    date: '2024-10-24',
    item: (
      <a href="https://open.substack.com/pub/jackpoulson/p/ftc-deleted-public-evidence-of-its">
        FTC deleted public evidence of its investigation into phone-tracking data broker after being asked for comment
      </a>
    ),
  },
  {
    date: '2024-10-23',
    item: (
      <a href="https://krebsonsecurity.com/2024/10/the-global-surveillance-free-for-all-in-mobile-ad-data/#more-69216">
        The global surveillance free-for-all in mobile ad data
      </a>
    ),
  },
  {
    date: '2024-10-21',
    item: (
      <a href="https://open.substack.com/pub/jackpoulson/p/florida-geared-up-to-surveil-haitian">
        Florida geared up to surveil Haitian migrants' phones from planes using Israeli 'StingRay'
      </a>
    ),
  },
  {
    date: '2024-10-21',
    item: (
      <a href="https://theintercept.com/2024/10/21/instagram-israel-palestine-censorship-sjp/">
        Meta’s Israel policy chief tried to suppress pro-Palestinian Instagram posts 
      </a>
    ),
  },
  {
    date: '2024-10-21',
    item: (
      <a href="https://open.substack.com/pub/jackpoulson/p/ftc-opened-investigation-into-commercial">
        FTC opened investigation into commercial phone-tracking firm Venntel
      </a>
    ),
  },
  {
    date: '2024-10-19',
    item: (
      <a href="https://www.kenklippenstein.com/p/israel-preps-for-strike-on-iran-top">
        Israel preps for strike on Iran, top secret leak reveals
      </a>
    ),
  },
  {
    date: '2024-10-17',
    item: (
      <a href="https://electronicintifada.net/blogs/ali-abunimah/uk-police-raid-home-seize-devices-eis-asa-winstanley">
        UK police raid home, seize devices of [journalist] Asa Winstanley
      </a>
    ),
  },
  {
    date: '2024-10-15',
    item: (
      <a href="https://www.spytalk.co/p/cia-had-role-in-israeli-assasssination">
        CIA had role in Israeli assasssination try in Lebanon—report
      </a>
    ),
  },
  {
    date: '2024-10-14',
    item: (
      <a href="https://www.nbcnews.com/news/world/israel-gaza-hamas-hospital-bombing-rcna175237">
        Israeli strike on Gaza hospital ignites devastating fire
      </a>
    ),
  },
  {
    date: '2024-10-14',
    item: (
      <a href="https://www.theguardian.com/world/2024/oct/14/usaid-gasa-aid-meetings-sde-teiman">
        US officials attend Gaza aid meetings on site of Israeli prison accused of ‘horrific’ torture
      </a>
    ),
  },
  {
    date: '2024-10-13',
    item: (
      <a href="https://www.politico.eu/article/40-countries-condemn-israel-attacks-un-peacekeepers-lebanon/">
         40 countries ‘strongly condemn’ Israeli attacks on UN peacekeepers 
      </a>
    ),
  },
  {
    date: '2024-10-11',
    item: (
      <a href="https://www.theguardian.com/world/2024/oct/11/us-made-munition-used-in-israeli-strike-on-central-beirut-shrapnel-shows">
        US-made munition used in Israeli strike on central Beirut, shrapnel shows
      </a>
    ),
  },
  {
    date: '2024-10-11',
    item: (
      <a href="https://thegrayzone.com/2024/10/11/israeli-jails-grayzones-jeremy-loffredo/">
        Israel jails Grayzone’s Jeremy Loffredo, releases him pending investigation
      </a>
    ),
  },
  {
    date: '2024-10-10',
    item: (
      <a href="https://open.substack.com/pub/jackpoulson/p/israeli-firm-taught-us-police-how">
        Israeli firm taught U.S. police how to "target" BLM, Antifa, and Jan 6 protestors through social media
      </a>
    ),
  },
  {
    date: '2024-10-10',
    item: (
      <a href="https://www.ynetnews.com/article/ryjxg00b1je">
        [Israel arrests independent American journalist]
      </a>
    ),
  },
  {
    date: '2024-10-05',
    item: (
      <a href="https://www.wsj.com/tech/cybersecurity/u-s-wiretap-systems-targeted-in-china-linked-hack-327fc63b">
        U.S. wiretap systems targeted in China-linked hack
      </a>
    ),
  },
  {
    date: '2024-10-04',
    item: (
      <a href="https://www.propublica.org/article/israel-gaza-america-biden-administration-weapons-bombs-state-department">
        Inside the State Department’s weapons pipeline to Israel 
      </a>
    ),
  },
  {
    date: '2024-10-04',
    item: (
      <a href="https://apnews.com/article/keith-alexander-ironnet-cybersecurity-nsa-bankruptcy-eddd67f3a1b312face21c29c59400e05">
        Collapse of national security elites’ cyber firm leaves bitter wake
      </a>
    ),
  },
  {
    date: '2024-10-04',
    item: (
      <a href="https://www.reuters.com/world/middle-east/us-military-strikes-houthi-tagets-yemen-official-says-2024-10-04/">
        US military strikes 15 Houthi targets in Yemen
      </a>
    ),
  },
  {
    date: '2024-10-04',
    item: (
      <a href="https://nymag.com/intelligencer/article/samantha-power-usaid-gaza-israel-hamas.html">
         The Price of Power: America’s chief humanitarian official rose to fame by speaking out against atrocities. Now she’s trapped by one. 
      </a>
    ),
  },
  {
    date: '2024-10-03',
    item: (
      <a href="https://www.reuters.com/world/britain-agrees-chagos-island-sovereignty-deal-with-mauritius-2024-10-03/">
        UK cedes Chagos Island sovereignty to Mauritius, retains Diego Garcia airbase
      </a>
    ),
  },
  {
    date: '2024-10-01',
    item: (
      <a href="https://www.bbc.com/news/articles/cj9jzv4deg9o">
        Fear and uncertainty in Lebanon as Israel invades
      </a>
    ),
  },
  {
    date: '2024-10-01',
    item: (
      <a href="https://www.theguardian.com/world/2024/oct/01/iran-imminent-ballistic-missile-attack-israel-us-warns">
        Iran launches waves of missiles at Israel hours after US warning
      </a>
    ),
  },
  {
    date: '2024-09-29',
    item: (
      <a href="https://www.theguardian.com/world/live/2024/sep/29/middle-east-crisis-live-israel-bombardment-lebanon-hezbollah-hassan-nasrallah-latest">
        Israeli air force launches strikes on Yemen
      </a>
    ),
  },
  {
    date: '2024-09-27',
    item: (
      <a href="https://www.cnn.com/2024/09/27/middleeast/israel-pager-attack-hezbollah-lebanon-invs-intl/index.html">
        Israel concealed explosives inside batteries of pagers sold to Hezbollah, Lebanese officials say 
      </a>
    ),
  },
  {
    date: '2024-09-26',
    item: (
      <a href="https://www.kenklippenstein.com/p/twitter-banned-me">
        [Twitter banned journalist Ken Klippenstein for publishing Vance dossier]
      </a>
    ),
  },
  {
    date: '2024-09-25',
    item: (
      <a href="https://www.bloomberg.com/news/articles/2024-09-25/us-probing-tech-firms-sap-carahsoft-for-potential-price-fixing">
        US investigating SAP, Carahsoft for potential price-fixing
      </a>
    ),
  },
  {
    date: '2024-09-24',
    item: (
      <a href="https://www.propublica.org/article/gaza-palestine-israel-blocked-humanitarian-aid-blinken">
        Israel deliberately blocked humanitarian aid to Gaza, two government bodies concluded. Antony Blinken rejected them. 
      </a>
    ),
  },
  {
    date: '2024-09-23',
    item: (
      <a href="https://www.theguardian.com/world/2024/sep/23/israel-lebanon-strikes-evacuation-hezbollah">
        Israeli strikes kill 492 in heaviest daily toll in Lebanon since 1975-90 civil war
      </a>
    ),
  },
  {
    date: '2024-09-21',
    item: (
      <a href="https://apnews.com/article/israel-palestinians-al-jazeera-gaza-war-hamas-4abdb2969e39e7ad99dfbf9caa7bb32c">
        Israel raids and shuts down Al Jazeera’s bureau in Ramallah in the West Bank
      </a>
    ),
  },
  {
    date: '2024-09-19',
    item: (
      <a href="https://www.theguardian.com/world/live/2024/sep/19/lebanon-blasts-impossible-to-know-if-walkie-talkies-used-by-hezbollah-were-from-our-company-says-japanese-firm-live">
        Israeli warplanes target Lebanon as Iran and Hezbollah threaten retaliation for deadly blasts
      </a>
    ),
  },
  {
    date: '2024-09-18',
    item: (
      <a href="https://jackpoulson.substack.com/p/company-behind-walkie-talkie-devices">
        Brand behind walkie-talkie devices transformed into bombs also supplies U.S. military
      </a>
    ),
  },
  {
    date: '2024-09-18',
    item: (
      <a href="https://apnews.com/live/lebanon-syria-pagers-hezbollah-updates">
        Second wave of deadly device explosions injures at least 300 in Lebanon
      </a>
    ),
  },
  {
    date: '2024-09-18',
    item: (
      <a href="https://www.cbsnews.com/news/hezbollah-pagers-explode-israel-taiwan-hungary-gold-apollo-bac-consulting/">
        Hezbollah's exploding pagers may have been made by a Hungarian company. Here's what we know so far.
      </a>
    ),
  },
  {
    date: '2024-09-17',
    item: (
      <a href="https://www.reuters.com/world/middle-east/israel-planted-explosives-hezbollahs-taiwan-made-pagers-sources-say-2024-09-18/">
        Israel planted explosives in Hezbollah's Taiwan-made pagers, sources say
      </a>
    ),
  },
  {
    date: '2024-09-17',
    item: (
     <a href="https://www.aljazeera.com/news/2024/9/17/dozens-of-hezbollah-members-wounded-after-pagers-explode-in-lebanon">
       Lebanon’s health minister says 8 killed, 2,750 wounded by exploding pagers
     </a>
   ),
  },
  {
    date: '2024-09-16',
    item: (
     <a href="https://apnews.com/article/jewish-chronicle-fabricated-articles-israel-hamas-gaza-d6b82c43b7ee400c78ac1901cd92c7ce">
       Columnists resign from the Jewish Chronicle over allegations Gaza articles were fabricated
     </a>
   ),
  },
  {
    date: '2024-09-13',
    item: (
      <a href="https://english.elpais.com/international/2024-09-13/owner-of-company-that-spied-on-assange-for-the-cia-was-collaborating-with-spains-secret-service.html">
        Owner of company that spied on Assange for the CIA was collaborating with Spain’s secret service
      </a>
    ),
  },
  {
    date: '2024-09-04',
    item: (
      <a href="https://www.prweek.com/article/1887170/edelman-beefs-public-affairs-expertise-nikki-haley-max-baucus">
        [Nikki Haley joins PR firm Edelman as vice chair]
      </a>
    ),
  },
  {
    date: '2024-08-30',
    item: (
      <a href="https://www.wsj.com/articles/rtx-to-pay-200-million-over-export-control-violations-46d653b1">
        [Raytheon] to pay $200 million over export control violations
      </a>
    ),
  },
  {
    date: '2024-08-29',
    item: (
      <a href="https://www.theguardian.com/world/article/2024/aug/29/israel-airstrike-aid-convoy-gaza">
        Israeli military launches fatal airstrike on humanitarian aid convoy in Gaza
      </a>
    ),
  },
  {
    date: '2024-08-28',
    item: (
      <a href="https://www.wsj.com/world/who-is-pavel-durov-telegram-founder-9b43eb5a">
        [French and UAE intelligence reportedly jointly hacked the iPhone of Telegram founder Pavel Durov in 2017]
      </a>
    ),
  },
  {
    date: '2024-08-26',
    item: (
      <a href="https://www.ontheditch.com/third-flight-transported-idf-weapons/">
        Third flight transported IDF weapons over Ireland
      </a>
    ),
  },
  {
    date: '2024-08-25',
    item: (
      <a href="https://apnews.com/article/france-russia-telegram-paris-durov-arrest-63cd8e5663c6b6f3404745866d662954">
        French authorities arrest Telegram CEO Pavel Durov at a Paris airport, French media report
      </a>
    ),
  },
  {
    date: '2024-08-17',
    item: (
      <a href="https://open.substack.com/pub/jackpoulson/p/leaked-israeli-docs-reveal-efforts">
        Leaked Israeli docs reveal efforts to evade foreign agent lobbying law
      </a>
    ),
  },
  {
    date: '2024-08-13',
    item: (
      <a href="https://www.haaretz.com/israel-news/2024-08-13/ty-article-magazine/.premium/idf-uses-gazan-civilians-as-human-shields-to-inspect-potentially-booby-trapped-tunnels/00000191-4c84-d7fd-a7f5-7db6b99e0000">
        Israeli Army uses Palestinian civilians to inspect potentially booby-trapped tunnels in Gaza
      </a>
    ),
  },
  {
    date: '2024-08-13',
    item: (
      <a href="https://www.nytimes.com/2024/08/13/us/politics/hunter-biden-ukrainian-company.html">
        Hunter Biden sought State Department help for Ukrainian company
      </a>
    ),
  },
  {
    date: '2024-08-08',
    item: (
      <a href="https://www.timesofisrael.com/minister-black-cube-ceo-discussed-probing-students-for-justice-in-palestine-report/">
        [Israeli] Minister, Black Cube CEO discussed probing Students for Justice in Palestine
      </a>
    ),
  },
  {
    date: '2024-08-05',
    item: (
      <a href="https://apnews.com/article/google-antitrust-search-engine-verdict-apple-319a61f20fb11510097845a30abaefd8">
        Google illegally maintains monopoly over internet search, judge rules
      </a>
    ),
  },
  {
    date: '2024-08-03',
    item: (
      <a href="https://www.timesofisrael.com/liveblog_entry/josh-shapiro-seeks-to-downplay-his-time-as-idf-volunteer-after-college-op-ed-resurfaces/">
        Josh Shapiro seeks to downplay his time as IDF volunteer after college op-ed resurfaces
      </a>
    ),
  },
  {
    date: '2024-07-30',
    item: (
      <a href="https://thegrayzone.com/2024/07/30/grayzone-pr-fiasco-us-govt-regime-change-leaked-emails/">
        The Grayzone caused ‘biggest PR fiasco in history’ for US govt regime change arm, leaked emails reveal
      </a>
    ),
  },
  {
    date: '2024-07-29',
    item: (
      <a href="https://www.haaretz.com/israel-news/2024-07-29/ty-article/.premium/idf-commanders-gave-order-to-blow-up-rafah-reservoir-army-suspects-breach-of-intl-law/00000190-fd90-d5ef-a5fe-ff9ec3ea0000">
        Israeli Army commanders gave order to blow up Rafah reservoir. IDF suspects breach of int'l law
      </a>
    ),
  },
  {
    date: '2024-07-28',
    item: (
      <a href="https://open.substack.com/pub/jackpoulson/p/tiktok-to-ban-some-criticisms-of">
        TikTok to ban some criticisms of Zionism following pressure from NGO backed by former Israeli intelligence officials
      </a>
    ),
  },
  {
    date: '2024-07-19',
    item: (
      <a href="https://apnews.com/article/icj-court-israel-palestinians-settlements-2d5178500c0410341b252335859f2316">
        Top UN court says Israel’s presence in occupied Palestinian territories is illegal and should end
      </a>
    ),
  },
  {
    date: '2024-07-19',
    item: (
      <a href="https://www.nbcnews.com/news/world/israel-hamas-war-houthi-rebels-drone-strike-tel-aviv-us-embassy-office-rcna161846">
        Drone strike near U.S. Embassy office in Tel Aviv leaves one dead, Yemen's Houthi rebels claim attack
      </a>
    ),
  },
  {
    date: '2024-07-17',
    item: (
      <a href="https://www.reuters.com/world/us/ex-white-house-official-indicted-acting-south-korea-agent-2024-07-16/">
        Former White House official accused of acting as South Korea agent
      </a>
    ),
  },
  {
    date: '2024-07-12',
    item: (
      <a href="https://www.france24.com/en/middle-east/20240712-fogbow-a-us-firm-with-military-links-eyes-maritime-plan-for-gaza-aid">
        Fogbow, a US firm with military links, eyes maritime plan for Gaza aid
      </a>
    ),
  },
  {
    date: '2024-07-11',
    item: (
      <a href="https://open.substack.com/pub/jackpoulson/p/pro-israel-group-censoring-social">
        Pro-Israel group censoring social media led by former Israeli intelligence officers 
      </a>
    ),
  },
  {
    date: '2024-07-10',
    item: (
      <a href="https://www.theguardian.com/world/article/2024/jul/10/first-f-16-jets-heading-to-ukraine-after-months-of-training-and-negotiations">
        First F-16 jets heading to Ukraine after months of training and negotiations
      </a>
    ),
  },
  {
    date: '2024-07-08',
    item: (
      <a href="https://www.nytimes.com/2024/07/08/business/sullivan-cromwell-israel-protests.html">
        A Wall Street law firm wants to define consequences of Israel protests
      </a>
    ),
  },
  {
    date: '2024-07-08',
    item: (
      <a href="https://www.theguardian.com/us-news/article/2024/jul/08/biden-wurd-radio-interview-questions">
        Radio host resigns after admitting Biden aides gave her questions for interview
      </a>
    ),
  },
  {
    date: '2024-07-07',
    item: (
      <a href="https://www.haaretz.com/israel-news/2024-07-07/ty-article-magazine/.premium/idf-ordered-hannibal-directive-on-october-7-to-prevent-hamas-taking-soldiers-captive/00000190-89a2-d776-a3b1-fdbe45520000">
        IDF ordered Hannibal directive on October 7 to prevent Hamas taking soldiers captive
      </a>
    ),
  },
  {
    date: '2024-07-05',
    item: (
      <a href="https://apnews.com/article/uk-elections-2024-result-labour-starmer-exit-sunak-e94f379ea893ec17711fd82cec03b603">
        Starmer vows ‘government of service’ for national renewal as he takes power after Labour landslide
      </a>
    ),
  },
  {
    date: '2024-06-25',
    item: (
      <a href="https://www.aljazeera.com/news/2024/6/25/julian-assange-is-free-wikileaks-founder-freed-in-deal-with-us">
        ‘Julian Assange is free’: Wikileaks founder freed in deal with US
      </a>
    ),
  },
  {
    date: '2024-06-24',
    item: (
      <a href="https://www.theguardian.com/world/article/2024/jun/24/israel-fund-us-university-protest-gaza-antisemitism">
        Israeli documents show expansive government effort to shape US discourse around Gaza war
      </a>
    ),
  },
  {
    date: '2024-06-20',
    item: (
      <a href="https://www.washingtonbabylondc.com/p/the-life-of-the-party-former-fbi">
        The Life of the Party: Former FBI official Anthony Ferrante wins friends and influences people by throwing the best bashes in town
      </a>
    ),
  },
  {
    date: '2024-06-18',
    item: (
      <a href="https://www.jta.org/2024/06/18/united-states/adl-faces-wikipedia-ban-over-reliability-concerns-on-israel-antisemitism">
        ADL faces Wikipedia ban over reliability concerns on Israel, antisemitism
      </a>
    ),
  },
  {
    date: '2024-06-14',
    item: (
      <a href="https://www.reuters.com/investigates/special-report/usa-covid-propaganda/">
        Pentagon ran secret anti-vax campaign to undermine China during pandemic 
      </a>
    ),
  },
  {
    date: '2024-06-11',
    item: (
      <a href="https://www.washingtonpost.com/national-security/2024/06/10/azov-brigade-ukraine-us-weapons/">
        U.S. lifts weapons ban on Ukrainian military unit
      </a>
    ),
  },
  {
    date: '2024-06-10',
    item: (
      <a href="https://jackpoulson.substack.com/p/the-australian-ad-tech-firm-secretly">
        The Australian ad-tech firm secretly fueling "psychological warfare" against U.S. university students
      </a>
    ),
  },
  {
    date: '2024-06-06',
    item: (
      <a href="https://www.forbes.com/sites/sarahemerson/2024/06/06/eric-schmidt-is-secretly-testing-ai-military-drones-in-a-wealthy-silicon-valley-suburb/">
        Eric Schmidt is secretly testing AI military drones in a wealthy Silicon Valley suburb
      </a>
    ),
  },
  {
    date: '2024-06-04',
    item: (
      <a href="https://apnews.com/article/columbia-law-review-israel-article-backlash-da2f924cddec4593b4f17b8baf500969">
        After publishing an article critical of Israel, Columbia Law Review’s website is shut down by board
      </a>
    ),
  },
  {
    date: '2024-06-04',
    item: (
      <a href="https://www.npr.org/2024/05/31/1197959218/fbi-phone-company-anom">
        How the FBI's fake cell phone company put criminals into real jail cells
      </a>
    ),
  },
  {
    date: '2024-06-01',
    item: (
      <a href="https://abcnews.go.com/International/wireStory/hezbollah-fighters-shoot-israeli-drone-southern-lebanon-110737840">
        Hezbollah fighters shoot down an Israeli drone in Lebanon and fire rockets at an Israeli base
      </a>
    ),
  },
  {
    date: '2024-05-30',
    item: (
      <a href="https://www.haaretz.com/israel-news/2024-05-30/ty-article/.premium/how-israel-nixed-haaretzs-report-into-alleged-mossad-extortion-of-hague-prosecutor/0000018f-c608-d801-a3ef-ff08cf810000">
        How Israeli security nixed Haaretz's report into alleged Mossad extortion of international court prosecutor
      </a>
    ),
  },
  {
    date: '2024-05-30',
    item: (
      <a href="https://www.theguardian.com/us-news/article/2024/may/30/two-more-us-officials-resign-over-biden-administrations-position-on-gaza-war">
       Two more US officials resign over Biden administration’s position on Gaza war 
      </a>
    ),
  },
  {
    date: '2024-05-29',
    item: (
      <a href="https://www.washingtonpost.com/world/2024/05/29/rafah-strike-us-munition-israel/">
        Experts say Israel used U.S.-made bomb in deadly Rafah strike
      </a>
    ),
  },
  {
    date: '2024-05-28',
    item: (
      <a href="https://www.theguardian.com/world/article/2024/may/28/israeli-spy-chief-icc-prosecutor-war-crimes-inquiry">
        Israeli spy chief ‘threatened’ ICC prosecutor over war crimes inquiry
      </a>
    ),
  },
  {
    date: '2024-05-27',
    item: (
      <a href="https://www.telegraph.co.uk/world-news/2024/05/26/at-least-20-palestinians-killed-as-air-strikes-hit-rafah/">
         Israel admits bombing Rafah camp as civilians ‘burnt alive’ in melting tents 
      </a>
    ),
  },
  {
    date: '2024-05-26',
    item: (
      <a href="https://www.ft.com/content/e2c327e4-31e9-4208-afcc-095c0a4ed762">
        US expected to lift ban on sale of offensive weapons to Saudi Arabia
      </a>
    ),
  },
  {
    date: '2024-05-24',
    item: (
      <a href="https://apnews.com/article/israel-gaza-palestinians-court-ceasefire-01d093d21a1eadaa31af5708cf1cbf38">
        Top UN court orders Israel to halt military offensive in Rafah, though Israel is unlikely to comply
      </a>
    ),
  },
  {
    date: '2024-05-21',
    item: (
      <a href="https://www.ft.com/content/6700a246-e0cd-49d8-b5ef-d2379e86290f">
        Biden administration signals it will support push to sanction ICC 
      </a>
    ),
  },
  {
    date: '2024-05-21',
    item: (
      <a href="https://apnews.com/article/live-transmission-israel-associated-press-57e8f662907334ba3599156276381190">
       Israeli communications minister orders return of seized camera equipment to AP
      </a>
    ),
  },
  {
    date: '2024-05-21',
    item: (
      <a href="https://www.theguardian.com/world/article/2024/may/21/israeli-soldiers-and-police-tipping-off-groups-that-attack-gaza-aid-trucks">
        Israeli soldiers and police tipping off groups that attack Gaza aid trucks  
      </a>
    ),
  },
  {
    date: '2024-05-16',
    item: (
      <a href="https://www.washingtonpost.com/nation/2024/05/16/business-leaders-chat-group-eric-adams-columbia-protesters/">
        Business titans privately urged NYC mayor to use police on Columbia protesters, chats show 
      </a>
    ),
  },
  {
    date: '2024-05-13',
    item: (
      <a href="https://www.washingtonpost.com/national-security/2024/05/13/harrison-mann-army-resigns-israel-gaza/">
        Army officer resigns in protest of U.S. support for Israel in Gaza
      </a>
    ),
  },
  {
    date: '2024-05-10',
    item: (
      <a href="https://jackpoulson.substack.com/p/scsp-ai-expo-2024">
        U.S. tech industry gathers to organize against China, while defending its obscured role in ongoing slaughter of Gazans
      </a>
    ),
  },
  {
    date: '2024-05-10',
    item: (
      <a href="https://apnews.com/article/mit-arizona-pennsylvania-campus-protests-encampment-police-7d9cd0a1f4ac7eaca41b38de798a2217">
        Police arrest dozens as they break up pro-Palestinian protests at several US universities
      </a>
    ),
  },
  {
    date: '2024-05-06',
    item: (
      <a href="https://jackpoulson.substack.com/p/reuters-sinks-to-new-low-and-doesnt">
        Reuters sinks to new low and doesn't even bother Googling for its "exclusive" on Wozniak's Privateer buying Orbital Insight
      </a>
    ),
  },
  {
    date: '2024-05-05',
    item: (
      <a href="https://apnews.com/article/israel-aljazeera-hamas-gaza-war-eba9416aea82f505ab908ee60d1de5e4">
        Israel orders Al Jazeera to close its local operation and seizes some of its equipment
      </a>
    ),
  },
  {
    date: '2024-05-02',
    item: (
      <a href="https://jackpoulson.substack.com/p/microsoft-and-google-have-been-working">
        Microsoft and Google have been working closely with Israel's Computer Services Directorate for years, in the shadow of flashier military intelligence units
      </a>
    ),
  },
  {
    date: '2024-05-02',
    item: (
      <a href="https://thegrayzone.com/2024/05/02/columbia-crackdown-university-nypd/">
        Columbia crackdown led by university prof doubling as NYPD spook
      </a>
    ),
  },
  {
    date: '2024-04-26',
    item: (
      <a href="https://jackpoulson.substack.com/p/rayzone-group-borderint">
        Israeli competitor to Palantir is proposing "BORDERINT" traveler risk assessments
      </a>
    ),
  },
  {
    date: '2024-04-25',
    item: (
      <a href="https://jackpoulson.substack.com/p/california-based-covert-surveillance">
        California-based covert surveillance firm attempted to criminalize [independent journalist] and unmask [their] sources, court filings reveal
      </a>
    ),
  },
  {
    date: '2024-04-20',
    item: (
      <a href="https://www.axios.com/2024/04/20/us-israel-sanctions-idf-west-bank">
        U.S. expected to sanction IDF unit for human rights violations in West Bank
      </a>
    ),
  },
  {
    date: '2024-04-20',
    item: (
      <a href="https://jackpoulson.substack.com/p/google-promoting-founder-of-google">
        Google promoting founder of Google Israel R&D to global research chief days after firing 28 workers for protesting cloud contract with Israeli military
      </a>
    ),
  },
  {
    date: '2024-04-19',
    item: (
      <a href="https://www.wsj.com/world/biden-weighs-more-than-1-billion-in-new-arms-for-israel-8b8607e4">
        Biden weighs more than $1 billion in new arms for Israel
      </a>
    ),
  },
  {
    date: '2024-04-17',
    item: (
      <a href="https://www.kenklippenstein.com/p/leaked-cables-show-biden-admin-opposes">
        Leaked cables show Biden admin opposes Palestinian statehood
      </a>
    ),
  },
  {
    date: '2024-04-16',
    item: (
      <a href="https://www.wired.com/story/google-no-tech-for-apartheid-project-nimbus-israel-gaza-protest/">
        Google workers protest cloud contract with Israel's government
      </a>
    ),
  },
  {
    date: '2024-04-15',
    item: (
      <a href="https://www.levernews.com/the-defense-industrys-inside-man-in-the-pacific-arms-race/">
        The defense industry’s inside man in the Pacific arms race
      </a>
    ),
  },
  {
    date: '2024-04-11',
    item: (
      <a href="https://www.telegraph.co.uk/global-health/terror-and-security/israel-hamas-war-gaza-idf-aid-strike-world-central-kitchen/">
         Top IDF commander in aid strike wanted to block humanitarian supplies into Gaza 
      </a>
    ),
  },
  {
    date: '2024-04-10',
    item: (
      <a href="https://jackpoulson.substack.com/p/your-ai-is-your-rifle">
        "Your AI is Your Rifle": Leak sheds light on ecosystem behind Pentagon's AI adoption
      </a>
    ),
  },
  {
    date: '2024-04-05',
    item: (
      <a href="https://www.theguardian.com/world/2024/apr/05/top-israeli-spy-chief-exposes-his-true-identity-in-online-security-lapse">
        Top Israeli spy chief exposes his true identity in online security lapse
      </a>
    ),
  },
  {
    date: '2024-04-03',
    item: (
      <a href="https://www.972mag.com/lavender-ai-israeli-army-gaza/">
        ‘Lavender’: The AI machine directing Israel’s bombing spree in Gaza
      </a>
    ),
  },
  {
    date: '2024-04-02',
    item: (
      <a href="https://www.cbsnews.com/news/central-world-kitchen-aid-workers-killed-airstrike-gaza/">
        7 World Central Kitchen aid workers killed by Israeli airstrike in Gaza
      </a>
    ),
  },
  {
    date: '2024-04-01',
    item: (
      <a href="https://www.cnn.com/2024/04/01/politics/biden-administration-f15-fighter-jets-israel/index.html">
         Biden administration set to greenlight $18 billion sale of F-15 fighter jets to Israel 
      </a>
    ),
  },
  {
    date: '2024-03-28',
    item: (
      <a href="https://www.wired.com/story/jeffrey-epstein-island-visitors-data-broker-leak/">
        [Adtech location-tracking data broker Near declares bankruptcy and reforms as Azira]
      </a>
    ),
  },
  {
    date: '2024-03-24',
    item: (
      <a href="https://www.haaretz.com/israel-news/2024-03-24/ty-article-magazine/.premium/were-not-only-here-to-fuck-hamas-how-israeli-militarism-took-over-online-dating/0000018e-60aa-d27e-a7af-7aeec5b00000">
        'We're not only here to fuck Hamas': How Israeli militarism took over online dating
      </a>
    ),
  },
  {
    date: '2024-03-23',
    item: (
      <a href="https://jackpoulson.substack.com/p/exclusive-us-army-simulated-exploiting">
        U.S. Army simulated exploiting The Taipei Times, Buddhist monks, and surgeons through online propaganda in context of Chinese invasion of Taiwan
      </a>
    ),
  },
  {
    date: '2024-03-22',
    item: (
      <a href="https://www.forbes.com/sites/davidjeans/2024/03/22/china-hawk-rep-mike-gallagher-is-taking-a-job-at-palantir/?sh=3e3646355148">
        China hawk Rep. Mike Gallagher is taking a job at Palantir
      </a>
    ),
  },
  {
    date: '2024-03-20',
    item: (
      <a href="https://jackpoulson.substack.com/p/where-the-us-is-training-soldiers">
        Where the U.S. is training soldiers to spy on China in the age of cellphones and facial recognition
      </a>
    ),
  },
  {
    date: '2024-03-19',
    item: (
      <a href="https://www.dsca.mil/press-media/major-arms-sales/bahrain-m1a2-abrams-main-battle-tanks">
        [Approval of $2.2 billion contract to sell Abrams tanks to Bahrain]
      </a>
    ),
  },
  {
    date: '2024-03-18',
    item: (
      <a href="https://jackpoulson.substack.com/p/brief-google-received-cloud-contract">
        Google received cloud contract supporting U.S. Special Operations Forces
      </a>
    ),
  },
  {
    date: '2024-03-14',
    item: (
      <a href="https://www.reuters.com/world/us/trump-launched-cia-covert-influence-operation-against-china-2024-03-14/">
        Trump launched CIA covert influence operation against China
      </a>
    ),
  },
  {
    date: '2024-03-14',
    item: (
      <a href="https://jackpoulson.substack.com/p/brief-pentagons-new-ai-chief-is-former">
        Pentagon's new AI chief is former Google Trust & Safety exec who previously helped guide U.S. special operations
      </a>
    ),
  },
  {
    date: '2024-03-12',
    item: (
      <a href="https://www.wired.com/story/hpsci-us-protests-section-702-presentation/">
        US lawmaker cited NYC protests in a defense of warrantless spying
      </a>
    ),
  },
  {
    date: '2024-03-06',
    item: (
      <a href="https://www.washingtonpost.com/national-security/2024/03/06/us-weapons-israel-gaza/">
        U.S. floods arms into Israel despite mounting alarm over war’s conduct
      </a>
    ),
  },
  {
    date: '2024-03-05',
    item: (
      <a href="https://theintercept.com/2024/03/05/israel-rafah-humanitarian-aid-us-cable/">
         Leaked U.S. Cable: Israeli invasion of Rafah would have “catastrophic humanitarian consequences” 
      </a>
    ),
  },
  {
    date: '2024-03-05',
    item: (
      <a href="https://jackpoulson.substack.com/p/26-billion-ai-powered-robotics-company">
        $2.6 billion AI-powered robotics company hired two separate law firms to suppress independent reporting
      </a>
    ),
  },
  {
    date: '2024-03-01',
    item: (
      <a href="https://theintercept.com/2024/03/01/cnn-christiane-amanpour-israel-gaza-coverage/">
         In internal meeting, Christiane Amanpour confronts CNN brass about “double standards” on Israel coverage 
      </a>
    ),
  },
  {
    date: '2024-02-28',
    item: (
      <a href="https://www.wired.com/story/how-pentagon-learned-targeted-ads-to-find-targets-and-vladimir-putin/">
       How the Pentagon learned to use targeted ads to find its targets—and Vladimir Putin
      </a>
    ),
  },
  {
    date: '2024-02-26',
    item: (
      <a href="https://prospect.org/world/2024-02-26-davos-in-the-desert-on-the-coast/">
        Davos in the desert, on the coast
      </a>
    ),
  },
  {
    date: '2024-02-24',
    item: (
      <a href="https://www.spytalk.co/p/israels-gospel-targeting-system-proves">
        Israel’s ‘Gospel’ targeting system proves lethal to Gaza civilians, too
      </a>
    ),
  },
  {
    date: '2024-02-20',
    item: (
      <a href="https://harpers.org/archive/2024/03/the-pentagons-silicon-valley-problem-andrew-cockburn/">
        The Pentagon's Silicon Valley problem
      </a>
    ),
  },
  {
    date: '2024-02-20',
    item: (
      <a href="https://www.timesofisrael.com/us-vetoes-security-council-resolution-demanding-immediate-ceasefire-in-gaza-war/">
        US vetoes Security Council resolution demanding immediate ceasefire in Gaza war
      </a>
    ),
  },
  {
    date: '2024-02-15',
    item: (
      <a href="https://thelogic.co/briefing/rcmp-still-buying-personal-data-from-private-sources-with-inadequate-checks-privacy-commissioner/">
        RCMP still buying personal data from private sources with inadequate checks: Privacy commissioner
      </a>
    ),
  },
  {
    date: '2024-02-15',
    item: (
      <a href="https://jackpoulson.substack.com/p/admist-uncertain-merger-with-wozs">
        Admist uncertain merger with Woz's Privateer Space, Orbital Insight CEO Kevin O'Brien bails to cryptocurrency surveillance firm Chainalysis
      </a>
    ),
  },
  {
    date: '2024-02-06',
    item: (
      <a href="https://www.washingtonpost.com/investigations/2024/02/06/mattis-advised-uae-yemen-war/">
        Mattis secretly advised Arab monarch on Yemen war, records show
      </a>
    ),
  },
  {
    date: '2024-02-03',
    item: (
      <a href="https://www.reuters.com/world/us-launches-retaliatory-strikes-iraq-syria-nearly-40-reported-killed-2024-02-03/">
        U.S. launches strikes in Iraq, Syria, nearly 40 reported killed
      </a>
    ),
  },
  {
    date: '2024-01-31',
    item: (
      <a href="https://www.washingtonpost.com/national-security/2024/01/31/samantha-power-usaid-confronted-gaza/">
         USAID's Samantha Power, genocide scholar, confronted by staff on Gaza
      </a>
    ),
  },
  {
    date: '2024-01-29',
    item: (
      <a href="https://www.politico.com/news/2024/01/29/jordan-drone-iran-biden-00138363">
         Enemy drone evaded detection by trailing US drone landing at Jordan base 
      </a>
    ),
  },
  {
    date: '2024-01-28',
    item: (
      <a href="https://www.nytimes.com/2024/01/28/us/politics/nancy-pelosi-fbi-russia-gaza-protesters.html">
         Pelosi Wants F.B.I. to Investigate Pro-Palestinian Protesters
      </a>
    ),
  },
  {
    date: '2024-01-26',
    item: (
      <a href="https://www.timesofisrael.com/the-hagues-decision-means-israel-is-now-in-the-dock-for-genocide/">
        The Hague’s decision means Israel is now in the dock for genocide
      </a>
    ),
  },
  {
    date: '2024-01-25',
    item: (
      <a href="https://www.bloomberg.com/news/articles/2024-01-25/dell-micron-backed-a-group-criticizing-chinese-rivals">
        Dell, Micron Backed a Group Raising Alarms on Rivals’ China Ties
      </a>
    ),
  },
  {
    date: '2024-01-23',
    item: (
      <a href="https://www.forbes.com/sites/sarahemerson/2024/01/23/eric-schmidts-secret-white-stork-project-aims-to-build-ai-combat-drones/">
        Eric Schmidt's Secret 'White Stork' Project Aims to Build AI Combat Drones
      </a>
    ),
  },
  {
    date: '2024-01-15',
    item: (
      <a href="https://jackpoulson.substack.com/p/anomaly-detection-firm-tied-to-numerous">
        Anomaly detection firm tied to numerous former CIA officials affiliated with Beacon Global accidentally published its confidential data brokerage and persona management agreements
      </a>
    ),
  },
  {
    date: '2024-01-12',
    item: (
      <a href="https://jackpoulson.substack.com/p/california-judiciary-purchased-chatgpt">
        California Judiciary purchased ChatGPT Plus licenses
      </a>
    ),
  },
  {
    date: '2024-01-10',
    item: (
      <a href="https://theintercept.com/2024/01/10/israel-disinformation-social-media-iron-truth/">
         Israeli Group Claims It’s Using Big Tech Back Channels to Censor “Inflammatory” Wartime Content
      </a>
    ),
  },
  {
    date: '2024-01-09',
    item: (
      <a href="https://www.forbes.com/sites/sarahemerson/2024/01/09/ex-google-ceo-eric-schmidt-is-working-on-a-secret-military-drone-project/?sh=1bfdefa86c6b">
        Ex-Google CEO Eric Schmidt Is Working On A Secret Military Drone Project
      </a>
    ),
  },
  {
    date: '2024-01-09',
    item: (
      <a href="https://www.ftc.gov/news-events/news/press-releases/2024/01/ftc-order-prohibits-data-broker-x-mode-social-outlogic-selling-sensitive-location-data"> 
        FTC Order Prohibits Data Broker X-Mode Social and Outlogic from Selling Sensitive Location Data 
      </a>
    ),
  },
  {
    date: '2024-01-06',
    item: (
      <a href="https://jackpoulson.substack.com/p/gig-work-surveillance-firm-settles">
        Gig-work surveillance firm settles case against alleged whistleblower
      </a>
    ),
  },
  {
    date: '2024-01-04',
    item: (
      <a href="https://theintercept.com/2024/01/04/cnn-israel-gaza-idf-reporting/">
         CNN Runs Gaza Coverage Past Jerusalem Team Operating Under Shadow of IDF Censor 
      </a>
    ),
  },
  {
    date: '2024-01-02',
    item: (
      <a href="https://www.reuters.com/world/middle-east/explosion-southern-beirut-suburb-dahiyeh-two-security-sources-2024-01-02/">
        Israeli drone kills senior Hamas official in Beirut, security sources say
      </a>
    ),
  },
  {
    date: '2023-12-29',
    item: (
      <a href="https://www.timesofisrael.com/liveblog_entry/skipping-congressional-review-blinken-okays-emergency-sale-of-m107-shells-to-israel/">
        Skipping congressional review, Blinken okays emergency sale of M107 shells to Israel
      </a>
    ),
  },
  {
    date: '2023-12-23',
    item: (
      <a href="https://www.washingtonpost.com/investigations/interactive/2023/israel-war-destruction-gaza-record-pace/">
        Israel has waged one of this century’s most destructive wars in Gaza
      </a>
    ),
  },
  {
    date: '2023-12-17',
    item: (
      <a href="https://substack.com/home/post/p-139712620">
        CEO of Gig-work Surveillance Firm Resigns
      </a>
    ),
  },
  {
    date: '2023-12-10',
    item: (
      <a href="https://www.washingtonpost.com/world/2023/12/10/india-the-disinfo-lab-discredit-critics/">
        Covert Indian operation seeks to discredit Modi's critics in the U.S.
      </a>
    ),
  },
  {
    date: '2023-12-09',
    item: (
      <a href="https://www.reuters.com/world/state-dept-asks-congress-approve-sale-israeli-tank-rounds-amid-growing-rights-2023-12-08/">
        Biden administration presses Congress to approve tank shells for Israel's war in Gaza
      </a>
    ),
  },
  {
    date: '2023-12-07',
    item: (
      <a href="https://jackpoulson.substack.com/p/inside-the-pro-israel-information">
        Inside the Pro-Israel Information War
      </a>
    ),
  },
  {
    date: '2023-12-04',
    item: (
      <a href="https://jackpoulson.substack.com/p/leaked-details-of-new-collaboration">
        Leaked details of new collaboration between U.S. and Australian intelligence, by way of former Google CEO Eric Schmidt
      </a>
    ),
  },
  {
    date: '2023-11-30',
    item: (
      <a href="https://jackpoulson.substack.com/p/brief-norwegian-firm-whose-execs">
        Brief: Norwegian firm whose execs sold Jay-Z a failed music streaming platform agreed to merge with special ops phone-tracking firm
      </a>
    ),
  },
  {
    date: '2023-11-30',
    item: (
      <a href="https://www.972mag.com/mass-assassination-factory-israel-calculated-bombing-gaza/">
        ‘A mass assassination factory’: Inside Israel’s calculated bombing of Gaza
      </a>
    ),
  },
  {
    date: '2023-11-29',
    item: (
      <a href="https://www.rollingstone.com/politics/politics-news/henry-kissinger-war-criminal-dead-1234804748/">
        Henry Kissinger, War Criminal Beloved by America’s Ruling Class, Finally Dies
      </a>
    ),
  },
  {
    date: '2023-11-20',
    item: (
      <a href="https://jackpoulson.substack.com/p/fraudulent-dmca-takedown-submitted">
        Fraudulent DMCA Takedown Submitted to Hide Details of Felony Domestic Violence Arrest of Premise CEO Delwin Maury Blackman
      </a>
    ),
  },
  {
    date: '2023-11-17',
    item: (
      <a href="https://jackpoulson.substack.com/p/the-nsf-is-using-clearview-ai-to">
        The NSF is using Clearview AI to investigate grant applicants with common names
      </a>
    ),
  },
  {
    date: '2023-11-09',
    item: (
      <a href="https://www.forbes.com/sites/sarahemerson/2023/11/09/former-google-ceo-eric-schmidt-futures-philanthropic-mess/">
        Inside Former Google CEO Eric Schmidt's $1 Billion Philanthropic Mess
      </a>
    ),
  },
  {
    date: '2023-11-09',
    item: (
      <a href="https://www.vox.com/world-politics/2023/11/9/23953714/biden-campaign-alumni-want-gaza-ceasefire-state-department-dissent-memo">
        More than 500 Biden campaign alumni want a Gaza ceasefire
      </a>
    ),
  },
  {
    date: '2023-11-09',
    item: (
      <a href="https://www.theatlantic.com/politics/archive/2023/11/peter-thiel-2024-election-politics-investing-life-views/675946/">
        Peter Thiel Is Taking a Break From Democracy
      </a>
    ),
  },
  {
    date: '2023-11-08',
    item: (
      <a href="https://jackpoulson.substack.com/p/campaign-to-stop-killer-robots-fired">
        Campaign to Stop Killer Robots fired its government relations manager to protect relationship with Austria, says former employee
      </a>
    ),
  },
  {
    date: '2023-11-04',
    item: (
      <a href="https://www.vox.com/world-politics/2023/11/4/23945628/israel-hamas-war-gaza-ceasefire-history-explained">
        Will an Israel-Hamas ceasefire happen? The reasons and roadblocks, explained.
      </a>
    ),
  },
  {
    date: '2023-10-23',
    item: (
      <a href="https://www.forbes.com/sites/davidjeans/2023/10/23/eric-schmidt-michelle-ritter-steel-perlot/">
        Former Google CEO Launched A $100 Million Company With His Girlfriend. It’s Not Going Well
      </a>
    ),
  },
  {
    date: '2023-10-21',
    item: (
      <a href="https://jackpoulson.substack.com/p/how-steve-wozniak-and-indonesias">
        How Steve Wozniak and Indonesia's CIA collided
      </a>
    ),
  },
  {
    date: '2023-10',
    item: (
      <a href="https://thecapitolforum.com/google_antitrust_trial_2023/">
        US v Google Trial Transcripts
      </a>
    ),
  },
  {
    date: '2023-10-19',
    item: (
      <a href="https://www.businessinsider.com/peter-thiel-fbi-informant-charles-johnson-johnathan-buma-chs-genius-2023-10">
        Tech billionaire Peter Thiel was an FBI informant
      </a>
    ),
  },
  {
    date: '2023-10-18',
    item: (
      <a href="https://jackpoulson.substack.com/p/lockheed-is-now-tracking-phones-and">
        Lockheed is now tracking phones and walkie-talkies from space, and the UAE military is allegedly a "strong" customer
      </a>
    ),
  },
  {
    date: '2023-10-16',
    item: (
      <a href="https://jackpoulson.substack.com/p/exclusive-google-backed-startups">
        Google-backed startup's main income from Indonesian intelligence tracking West Papuans, former employee says
      </a>
    ),
  },
  {
    date: '2023-10-15',
    item: (
      <a href="https://pacificsun.com/gaza-bay-area-protest/">
        An eye for an eye in Gaza? Bay Area youth and politicians respond
      </a>
    ),
  },
  {
    date: '2023-10-12',
    item: (
      <a href="https://www.politico.eu/article/france-gerald-darmanin-aims-to-ban-all-pro-palestine-protests/">
        France orders ban on all pro-Palestinian protests
      </a>
    ),
  },
  {
    date: '2023-10-10',
    item: (
      <a href="https://www.vice.com/en/article/4a378p/five-of-tim-ballards-alleged-victims-have-filed-a-lawsuit-against-him">
        Five of Tim Ballard’s Alleged Victims Have Filed a Lawsuit Against Him
      </a>
    ),
  },
  {
    date: '2023-10-10',
    item: (
      <a href="https://www.bloomberg.com/news/articles/2023-10-10/boeing-sped-1-000-smart-bombs-to-israel-after-the-hamas-attacks#xj4y7vzkg">
        Boeing Sped 1,000 Smart Bombs to Israel After the Hamas Attacks
      </a>
    ),
  },
  {
    date: '2023-10-09',
    item: (
      <a href="https://www.vox.com/2023/10/9/23910159/israel-gaza-siege-palestinians-hamas-humanitarian-crisis">
        What a “complete siege” of Gaza will mean for Palestinians
      </a>
    ),
  },
  {
    date: '2023-10-04',
    item: (
      <a href="https://www.forbes.com/sites/thomasbrewster/2023/10/04/iphone-app-promises-to-identify-human-traffickers-critics-say-it-endangers-victims/">
        A Hollywood-Backed Nonprofit’s App Promises To Identify Sex Traffickers. But Critics Say It Endangers Survivors
      </a>
    ),
  },
  {
    date: '2023-10-02',
    item: (
      <a href="https://www.theguardian.com/world/2023/oct/02/scandinavian-spy-drama-the-intelligence-chief-who-came-under-state-surveillance">
        Scandinavian spy drama: the intelligence chief who came under state surveillance
      </a>
    ),
  },
  {
    date: '2023-10-01',
    item: (
      <a href="https://jackpoulson.substack.com/p/exclusive-google-backed-intelligence">
        Google-backed intelligence contractor is avoiding bankruptcy by attempting to merge with Steve Wozniak's satellite company
      </a>
    ),
  },
  {
    date: '2023-09-28',
    item: (
      <a href="https://jackpoulson.substack.com/p/head-of-cia-osint-emphasizes-centrality">
        Head of CIA OSINT emphasizes centrality of Twitter and Telegram surveillance
      </a>
    ),
  },
  {
    date: '2023-09-27',
    item: (
      <a href="https://www.defense.gov/News/Releases/Release/Article/3540283/dod-to-establish-ai-battle-labs-in-eucom-indopacom/">
         DOD to Establish AI Battle Labs in EUCOM, INDOPACOM 
      </a>
    ),
  },
  {
    date: '2023-09-25',
    item: (
      <a href="https://jackpoulson.substack.com/p/exclusive-british-government-funded">
        British government funded a plan for international censorship of critiques of NATO
      </a>
    ),
  },
  {
    date: '2023-09-25',
    item: (
      <a href="https://balkaninsight.com/2023/09/25/who-benefits-inside-the-eus-fight-over-scanning-for-child-sex-content/">
        ‘Who Benefits?’ Inside the EU’s Fight over Scanning for Child Sex Content
      </a>
    ),
  },
  {
    date: '2023-09-25',
    item: (
      <a href="https://themessenger.com/business/chain-bridge-spac-iqt-christopher-darby-inqtel-cia">
        A CIA-Linked Investment Fund Is Falling Apart, Leaving a Trail of Questions
      </a>
    ),
  },
  {
    date: '2023-09-22',
    item: (
      <a href="https://jackpoulson.substack.com/p/fort-meades-new-contract-monitoring">
        Fort Meade's new contract monitoring Twitter
      </a>
    ),
  },
  {
    date: '2023-09-21',
    item: (
      <a href="https://www.cbc.ca/news/politics/sikh-nijjar-india-canada-trudeau-modi-1.6974607">
        Canada has Indian diplomats' communications in bombshell murder probe: sources
      </a>
    ),
  },
  {
    date: '2023-09-21',
    item: (
      <a href="https://www.timesofisrael.com/liveblog_entry/netanyahu-meets-ex-google-ceo-schmidt-asks-him-to-serve-on-ai-advisory-team/">
        Netanyahu meets ex-Google CEO Schmidt, asks him to serve on AI advisory team
      </a>
    ),
  },
  {
    date: '2023-09-21',
    item: (
      <a href="https://www.france24.com/en/france/20230920-french-journalist-released-after-two-day-arrest-for-reporting-on-egypt-spy-operation">
        French police release journalist arrested for reporting on alleged France-Egypt spy operation
      </a>
    ),
  },
  {
    date: '2023-09-19',
    item: (
      <a href="https://jackpoulson.substack.com/p/surveillance-firm-backed-by-google">
        Surveillance firm backed by Google and CIA allegedly defaulted on $370,000 per month rent
      </a>
    ),
  },
  {
    date: '2023-09-18',
    item: (
      <a href="https://jackpoulson.substack.com/p/covert-gigwork-surveillance-platform">
        Covert gigwork surveillance platform attempts to claw back disclosures of its intelligence clients
      </a>
    ),
  },
  {
    date: '2023-09-18',
    item: (
      <a href="https://www.404media.co/inside-shadowdragon-ice-babycenter-pregnancy-fortnite-black-planet/">
        Inside ShadowDragon, The Tool That Lets ICE Monitor Pregnancy Tracking Sites and Fortnite Players
      </a>
    ),
  },
  {
    date: '2023-09-18',
    item: (
      <a href="https://www.forbes.com/sites/thomasbrewster/2023/09/18/droidish-ai-drone-swarms-pentagon/">
        The U.S. military hopes AI drone swarms will be able to work together to carry out offensive missions with little human input. A language called Droidish might be the key.
      </a>
    ),
  },
  {
    date: '2023-09-17',
    item: (
      <a href="https://theintercept.com/2023/09/17/pakistan-ukraine-arms-imf/">
         U.S. Helped Pakistan Get IMF Bailout With Secret Arms Deal for Ukraine, Leaked Documents Reveal 
      </a>
    ),
  },
  {
    date: '2023-09-15',
    item: (
      <a href="https://jackpoulson.substack.com/p/the-pentagon-is-in-the-process-of">
        The Pentagon is moving its whistleblower-hunting system to Palantir's cloud
      </a>
    ),
  },
  {
    date: '2023-09-14',
    item: (
      <a href="https://jackpoulson.substack.com/p/the-covert-gig-work-surveillance">
        The Covert Gig-Work Surveillance CEO Arrested for Felony Domestic Violence
      </a>
    ),
  },
  {
    date: '2023-09-13',
    item: (
      <a href="https://jackpoulson.substack.com/p/exclusive-inside-trident-spectre">
        Inside 'Trident Spectre', the yearly tech experimentation program for Navy SEALs
      </a>
    ),
  },
  {
    date: '2023-09-12',
    item: (
      <a href="https://www.404media.co/customs-and-border-protection-stop-buying-location-data/">
        Customs and Border Protection Says It Will Stop Buying Smartphone Location Data
      </a>
    ),
  },
  {
    date: '2023-09-09',
    item: (
      <a href="https://jackpoulson.substack.com/p/transparency-international-defends">
        Transparency International defends funding from Western spy agencies — but won't say which
      </a>
    ),
  },
  {
    date: '2023-09-08',
    item: (
      <a href="https://www.bloomberg.com/news/articles/2023-09-08/google-ex-ceo-eric-schmidt-influences-ai-policy-with-27-billion-fortune">
        Google's Former CEO Is Leveraging His $27 Billion Fortune to Shape AI Policy
      </a>
    ),
  },
  {
    date: '2023-09-07',
    item: (
      <a href="https://www.crikey.com.au/2023/09/07/clearview-ai-australian-federal-police-meetings/">
        Australia’s top police met with Clearview AI after it was slammed for breaking nation’s privacy law
      </a>
    ),
  },
  {
    date: '2023-09-07',
    item: (
      <a href="https://www.nytimes.com/2023/09/07/nyregion/hillary-clinton-columbia-university.html">
        Professor Hillary Clinton Goes Back to School
      </a>
    ),
  },
  {
    date: '2023-09-06',
    item: (
      <a href="https://theintercept.com/2023/09/06/anne-neuberger-nsa-cybersecurity/">
        Top Biden Cyber Official Accused of Workplace Misconduct at NSA in 2014 — and Again at White House Last Year 
      </a>
    ),
  },
  {
    date: '2023-09-04',
    item: (
      <a href="https://www.opendemocracy.net/en/palantir-mi6-spymaster-cabinet-office-contract-brexit-john-sawers/">
        Spy tech firm won £27m government deal after introduction from ex-MI6 boss
      </a>
    ),
  },
  {
    date: '2023-09-01',
    item: (
      <a href="https://jackpoulson.substack.com/p/premise-data-confirms-secret-military">
        Premise Data confirms secret military and intelligence contracts in court filing
      </a>
    ),
  },
  {
    date: '2023-08-27',
    item: (
      <a href="https://jackpoulson.substack.com/p/pollster-for-niger-coup-support-is">
        Pollster for Niger coup support is a surveillance platform for U.S. Special Operations Forces 
      </a>
    ),
  },
  {
    date: '2023-08-24',
    item: (
      <a href="https://jackpoulson.substack.com/p/us-army-criminal-investigation-division">
        U.S. Army Criminal Investigation Division purchased more Clearview
        AI licenses
      </a>
    ),
  },
  {
    date: '2023-08-24',
    item: (
      <a href="https://www.leefang.com/p/nikki-haleys-sudden-wealth-rooted">
        Nikki Haley's Sudden Wealth Rooted in Weapons Industry, Pro-War Advocacy Network
      </a>
    ),
  },
  {
    date: '2023-08-24',
    item: (
      <a href="https://www.404media.co/ai-surveillance-tool-dhs-cbp-sentiment-emotion-fivecast/">
        The A.I. Surveillance Tool DHS Uses to Detect ‘Sentiment and
        Emotion’
      </a>
    ),
  },
  {
    date: '2023-08-22',
    item: (
      <a href="https://www.404media.co/the-secret-weapon-hackers-can-use-to-dox-nearly-anyone-in-america-for-15-tlo-usinfosearch-transunion/">
        The Secret Weapon Hackers Can Use to Dox Nearly Anyone in America
        for $15
      </a>
    ),
  },
  {
    date: '2023-08-21',
    item: (
      <a href="https://jackpoulson.substack.com/p/thiel-backed-startup-began-contracting">
        Thiel-backed startup began contracting on "off-grid" facial
        recognition with U.S. Air Force
      </a>
    ),
  }
]
