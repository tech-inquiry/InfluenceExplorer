import * as React from 'react'

export const data = {
  'RVSS [Tower001]': {
    latLong: [25.893899, -97.504345],
    title: 'BRP Customs B&M [Tower001]',
    addressLines: [
      'Brownsville, TX',
      'Brownsville Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower002]': {
    latLong: [25.968075, -97.599319],
    title: 'BRP Mulberry [Tower002]',
    addressLines: [
      'South of San Pedro, TX',
      'Brownsville Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower005]': {
    latLong: [29.42519, -101.04079],
    title: 'DRT-DRS-02 Upper Weir2 [Tower005]',
    addressLines: [
      'South of Amistad Dam, TX',
      'Del Rio Station',
      'Del Rio Sector',
    ],
  },
  'RVSS [Tower006]': {
    latLong: [29.32899, -100.93011],
    title: 'DRT-DRS-06 Lower Weir Dam2 [Tower006]',
    addressLines: ['Del Rio, TX', 'Del Rio Station', 'Del Rio Sector'],
  },
  'RVSS [Tower007]': {
    latLong: [29.32991, -100.92446],
    title: 'DRT-DRS-07 POE Bridge [Tower007]',
    addressLines: ['Del Rio, TX', 'Del Rio Station', 'Del Rio Sector'],
  },
  'RVSS [Tower008]': {
    latLong: [29.32952, -100.9239],
    title: 'DRT-DRS-08 POE Bridge Down River2 [Tower008]',
    addressLines: ['Del Rio, TX', 'Del Rio Station', 'Del Rio Sector'],
  },
  'RVSS [Tower010]': {
    latLong: [31.78232, -106.52797],
    title: 'EPT-EPS-01 Monument One2 [Tower010]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower011]': {
    latLong: [31.77575, -106.5217],
    title: 'EPT-EPS-02 Asarco West2 [Tower011]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower012]': {
    latLong: [31.7731, -106.51704],
    title: 'EPT-EPS-03 Asarco East2 [Tower012]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower013]': {
    latLong: [31.76177, -106.50954],
    title: 'EPT-EPS-04 Hacienda Restaurant2 [Tower013]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower014]': {
    latLong: [31.76044, -106.50529],
    title: 'EPT-EPS-05 Portfirio Diaz2 [Tower014]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower015]': {
    latLong: [31.749191, -106.486257],
    title: 'EPT-EPS-07 PDN Bridge2 [Tower015]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower016]': {
    latLong: [31.75021, -106.47878],
    title: 'EPT-EPS-08 Park Street2 [Tower016]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower017]': {
    latLong: [31.75982, -106.4678],
    title: 'EPT-EPS-09 2nd Street2 [Tower017]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower018]': {
    latLong: [31.76239, -106.44494],
    title: 'EPT-EPS-11 Boone Street2 [Tower018]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower019]': {
    latLong: [31.75643, -106.435719],
    title: 'EPT-EPS-12 Modesto Gomez2 [Tower019]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower020]': {
    latLong: [31.75447, -106.4216],
    title: 'EPT-EPS-13 Fonseca [Tower020]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower021]': {
    latLong: [31.74516, -106.40219],
    title: 'EPT-EPS-14 Ascarate [Tower021]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower023]': {
    latLong: [31.82065, -106.69981],
    title: 'EPT-STN-03 [Tower023]',
    addressLines: ['New Mexico', 'Santa Teresa Station', 'El Paso Sector'],
  },
  'RVSS [Tower024]': {
    latLong: [31.72748, -106.37595],
    title: 'EPT-YST-01 Midway [Tower024]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower025]': {
    latLong: [31.71138, -106.36686],
    title: 'EPT-YST-02 Yarborough [Tower025]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower026]': {
    latLong: [31.70263, -106.35506],
    title: 'EPT-YST-03 Levarios [Tower026]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower027]': {
    latLong: [31.6876, -106.34303],
    title: 'EPT-YST-04 Padres [Tower027]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower028]': {
    latLong: [31.67235444, -106.3362643],
    title: 'EPT-YST-05 POE2 [Tower028]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower029]': {
    latLong: [31.65425, -106.32555],
    title: 'EPT-YST-06 Headgates [Tower029]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower031]': {
    latLong: [25.873929, -97.376473],
    title: 'FTB Alaska Rd [Tower031]',
    addressLines: [
      'South Point, TX',
      'Fort Brown Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower032]': {
    latLong: [25.865524, -97.410568],
    title: 'FTB East of Sable Palm Road [Tower032]',
    addressLines: [
      'Brownsville, TX',
      'Fort Brown Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower033]': {
    latLong: [25.862211, -97.435097],
    title: 'FTB Pig Pens [Tower033]',
    addressLines: [
      'Brownsville, TX',
      'Fort Brown Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower034]': {
    latLong: [25.86332957, -97.44296572],
    title: 'FTB Pig Pens [Tower034]',
    addressLines: [
      'Brownsville, TX',
      'Fort Brown Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower035]': {
    latLong: [31.333932, -110.935302],
    title: 'Golf [Tower035]',
    addressLines: ['Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower036]': {
    latLong: [31.333078, -110.938893],
    title: 'Hotel [Tower036]',
    addressLines: ['Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower037]': {
    latLong: [26.043999, -97.712914],
    title: 'HRL Cantu Road [Tower037]',
    addressLines: [
      'Carricitos, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower038]': {
    latLong: [26.05416892, -97.78475156],
    title: 'HRL Galveston Bend [Tower038]',
    addressLines: [
      'West of Venadito, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower039]': {
    latLong: [26.066868, -97.891175],
    title: 'HRL Rio Rico Road [Tower039]',
    addressLines: [
      'Relampago, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower040]': {
    latLong: [26.039206, -97.676857],
    title: 'HRL Wells Bros Canal [Tower040]',
    addressLines: [
      'La Paloma, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower041]': {
    latLong: [31.33278915, -110.9474364],
    title: 'Kilo [Tower041]',
    addressLines: ['Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower042]': {
    latLong: [26.24190701, -98.505177],
    title: 'MCS Banworth Canal  [Tower042]',
    addressLines: [
      'South of La Joya, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower043]': {
    latLong: [26.259194, -98.573397],
    title: 'MCS GF Military Area [Tower043]',
    addressLines: [
      'Sullivan City, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower044]': {
    latLong: [26.179, -98.357],
    title: 'MCS Inspiration Canal  [Tower044]',
    addressLines: [
      'South of Mission, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower045]': {
    latLong: [26.23179963, -98.46079992],
    title: 'MCS Irrigation District #6 [Tower045]',
    addressLines: [
      'South of La Joya, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower046]': {
    latLong: [26.11817688, -98.26334295],
    title: 'MCS MacPump  [Tower046]',
    addressLines: [
      'Hidalgo, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower047]': {
    latLong: [26.155542, -98.328514],
    title: 'RVSS [Tower047]',
    addressLines: [
      'South of Madero, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower048]': {
    latLong: [26.225998, -98.439438],
    title: 'MCS Penitas Pump [Tower048]',
    addressLines: [
      'Penitas, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower049]': {
    latLong: [26.25233173, -98.52771421],
    title: 'MCS South Sam Fordyce [Tower049]',
    addressLines: [
      'La Joya, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower050]': {
    latLong: [26.1937045, -98.38664536],
    title: 'MCS Twin Bridges  [Tower050]',
    addressLines: [
      'South of Palmview South, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower051]': {
    latLong: [31.333368, -110.971173],
    title: 'November [Tower051]',
    addressLines: ['Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower052]': {
    latLong: [26.363573, -98.836094],
    title: 'RGC Azteca [Tower052]',
    addressLines: [
      'South of Rio Grande City, TX',
      'Rio Grande City Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower053]': {
    latLong: [26.538074, -99.141229],
    title: 'RGC Inside Mustang Gate Chapeno [Tower053]',
    addressLines: [
      'Roma, TX',
      'Rio Grande City Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower054]': {
    latLong: [26.298388, -98.724513],
    title: 'RGC La Casita Main [Tower054]',
    addressLines: [
      'South of Rio Grande City, TX',
      'Rio Grande City Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower055]': {
    latLong: [26.280707, -98.619185],
    title: 'RGC N of Silvertanks Military  [Tower055]',
    addressLines: [
      'Southeast of Rio Grande City, TX',
      'Rio Grande City Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower056]': {
    latLong: [26.34256, -98.741084],
    title: 'RGC Near Blas Chapas La Puerta  [Tower056]',
    addressLines: [
      'Rio Grande City, TX',
      'Rio Grande City Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower057]': {
    latLong: [26.405872, -99.07565],
    title: 'RGC NW of Horse Corrals Fronton [Tower057]',
    addressLines: [
      'Roma, TX',
      'Rio Grande City Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower058]': {
    latLong: [26.399997, -99.01317],
    title: 'RGC Rock Crossing [Tower058]',
    addressLines: [
      'Roma, TX',
      'Rio Grande City Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower092]': {
    latLong: [31.346183, -109.476383],
    title: 'TCA-DGL-0557 [Tower092]',
    addressLines: ['Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower093]': {
    latLong: [31.362745, -109.738764],
    title: 'TCA-DGL-0565 [Tower093]',
    addressLines: ['West of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower094]': {
    latLong: [31.346315, -109.807646],
    title: 'TCA-NCO-0525 [Tower094]',
    addressLines: [
      'East of Bisbee, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower095]': {
    latLong: [31.358338, -109.834806],
    title: 'TCA-NCO-0529 [Tower095]',
    addressLines: [
      'East of Bisbee, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower096]': {
    latLong: [31.346134, -109.76952],
    title: 'TCA-NCO-0567 [Tower096]',
    addressLines: [
      'West of Douglas, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower104]': {
    latLong: [31.334148, -110.99119],
    title: 'TCA-NGL-0505 [Tower104]',
    addressLines: ['West of Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower105]': {
    latLong: [31.342336, -111.007227],
    title: 'TCA-NGL-0507 [Tower105]',
    addressLines: ['West of Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower106]': {
    latLong: [31.347261, -111.017606],
    title: 'TCA-NGL-0509 [Tower106]',
    addressLines: ['West of Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower107]': {
    latLong: [31.34166, -110.89374],
    title: 'TCA-NGL-0511 [Tower107]',
    addressLines: ['East of Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower108]': {
    latLong: [31.333115, -110.982029],
    title: 'TCA-NGL-0555 [Tower108]',
    addressLines: ['West of Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower120]': {
    latLong: [28.70220083, -100.5061136],
    title: 'RVSS [Tower120]',
    addressLines: ['Eagle Pass, TX', 'Eagle Pass Station', 'Del Rio Sector'],
  },
  'RVSS [Tower121]': {
    latLong: [28.71868, -100.505465],
    title: 'RVSS [Tower121]',
    addressLines: ['Eagle Pass, TX', 'Eagle Pass Station', 'Del Rio Sector'],
  },
  'RVSS [Tower122]': {
    latLong: [28.66334937, -100.4992489],
    title: 'RVSS [Tower122]',
    addressLines: ['South of Eagle Pass, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower123]': {
    latLong: [28.67144067, -100.4961575],
    title: 'RVSS [Tower123]',
    addressLines: ['South of Eagle Pass, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower124]': {
    latLong: [28.73905791, -100.505616],
    title: 'RVSS [Tower124]',
    addressLines: ['Eagle Pass, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower125]': {
    latLong: [28.73291055, -100.4998943],
    title: 'RVSS [Tower125]',
    addressLines: ['Eagle Pass, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower126]': {
    latLong: [28.65990315, -100.4860346],
    title: 'RVSS [Tower126]',
    addressLines: ['South of Eagle Pass, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower127]': {
    latLong: [28.94162911, -100.6440181],
    title: 'RVSS [Tower127]',
    addressLines: ['Quemado, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower128]': {
    latLong: [28.62974323, -100.4449047],
    title: 'RVSS [Tower128]',
    addressLines: ['Rosita South, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower129]': {
    latLong: [28.60604414, -100.4326645],
    title: 'RVSS [Tower129]',
    addressLines: ['Rosita South, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower130]': {
    latLong: [32.6509357, -115.7216788],
    title: 'BP Hill [Tower130]',
    addressLines: [
      'West of Calexico, CA',
      'Calexico Station',
      'El Centro Sector',
    ],
  },
  'RVSS [Tower131]': {
    latLong: [32.64735237, -115.7606917],
    title: 'RVSS [Tower131]',
    addressLines: [
      'West of Calexico, CA',
      'Calexico Station',
      'El Centro Sector',
    ],
  },
  'RVSS [Tower132]': {
    latLong: [32.65271006, -115.6837788],
    title: 'RVSS [Tower132]',
    addressLines: [
      'West of Calexico, CA',
      'Calexico Station',
      'El Centro Sector',
    ],
  },
  'RVSS [Tower133]': {
    latLong: [32.66110974, -115.5593189],
    title: 'RVSS [Tower133]',
    addressLines: ['Calexico West, CA', 'Calexico Station', 'El Centro Sector'],
  },
  'RVSS [Tower134]': {
    latLong: [32.65880467, -115.5905213],
    title: 'RVSS [Tower134]',
    addressLines: ['Calexico West, CA', 'Calexico Station', 'El Centro Sector'],
  },
  'RVSS [Tower135]': {
    latLong: [32.6762328, -115.3510591],
    title: 'RVSS [Tower135]',
    addressLines: [
      'East of Calexico, CA',
      'Calexico Station',
      'El Centro Sector',
    ],
  },
  'RVSS [Tower136]': {
    latLong: [32.7058022, -115.1249271],
    title: 'RVSS [Tower136]',
    addressLines: [
      'East of Calexico, CA',
      'Calexico Station',
      'El Centro Sector',
    ],
  },
  'RVSS [Tower137]': {
    latLong: [32.7059224, -115.0304962],
    title: 'RVSS [Tower137]',
    addressLines: [
      'East of Calexico, CA',
      'Calexico Station',
      'El Centro Sector',
    ],
  },
  'RVSS [Tower138]': {
    latLong: [32.664163, -115.507997],
    title: 'RVSS [Tower138]',
    addressLines: ['Calexico, CA', 'Calexico Station', 'El Centro Sector'],
  },
  'RVSS [Tower139]': {
    latLong: [31.806637, -107.697075],
    title: 'RVSS [Tower139]',
    addressLines: ['West of Columbus, NM', 'Deming Station', 'El Paso Sector'],
  },
  'RVSS [Tower140]': {
    latLong: [31.791842, -107.808213],
    title: 'RVSS [Tower140]',
    addressLines: ['West of Columbus, NM', 'Deming Station', 'El Paso Sector'],
  },
  'RVSS [Tower142]': {
    latLong: [27.70610286, -99.74319186],
    title: 'RVSS [Tower142]',
    addressLines: [
      'Laredo crossing, TX',
      'Laredo West Station',
      'Laredo Sector',
    ],
  },
  'RVSS [Tower143]': {
    latLong: [27.33532877, -99.50508374],
    title: 'RVSS [Tower143]',
    addressLines: ['El Cenizo, TX', 'Laredo South Station', 'Laredo Sector'],
  },
  'RVSS [Tower144]': {
    latLong: [27.36467796, -99.50276284],
    title: 'RVSS [Tower144]',
    addressLines: ['Rio Bravo, TX', 'Laredo South Station', 'Laredo Sector'],
  },
  'RVSS [Tower147]': {
    latLong: [27.4665155, -99.48237222],
    title: 'Orphanage [Tower147]',
    addressLines: ['Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower148]': {
    latLong: [27.48375066, -99.47861093],
    title: 'RVSS [Tower148]',
    addressLines: ['Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower149]': {
    latLong: [27.49539103, -99.48709262],
    title: 'Slaughter [Tower149]',
    addressLines: ['Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower150]': {
    latLong: [27.49848638, -99.49211659],
    title: 'Tex Mex PR [Tower150]',
    addressLines: ['Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower151]': {
    latLong: [27.54044174, -99.52124214],
    title: 'RVSS [Tower151]',
    addressLines: ['Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower152]': {
    latLong: [27.57722728, -99.52432977],
    title: 'Fletcha Lane [Tower152]',
    addressLines: ['Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower154]': {
    latLong: [27.64485557, -99.62229447],
    title: 'RVSS [Tower154]',
    addressLines: ['South of Rancho Penitas West, TX', 'Laredo Sector'],
  },
  'RVSS [Tower155]': {
    latLong: [27.37239105, -99.49669606],
    title: 'RVSS [Tower155]',
    addressLines: ['North of Rio Bravo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower157]': {
    latLong: [27.604275, -99.58221],
    title: 'RVSS [Tower157]',
    addressLines: ['Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower158]': {
    latLong: [27.66865375, -99.69228711],
    title: 'RVSS [Tower158]',
    addressLines: ['West of Rancho Penitas West, TX', 'Laredo Sector'],
  },
  'RVSS [Tower161]': {
    latLong: [27.64271372, -99.60106133],
    title: 'JW Nixon [Tower161]',
    addressLines: ['South of Rancho Penitas West, TX', 'Laredo Sector'],
  },
  'RVSS [Tower162]': {
    latLong: [27.84976658, -99.87995234],
    title: 'RVSS [Tower162]',
    addressLines: ['North of Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower163]': {
    latLong: [27.79615226, -99.84906419],
    title: 'RVSS [Tower163]',
    addressLines: ['North of Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower166]': {
    latLong: [27.388715, -99.49176],
    title: 'RVSS [Tower166]',
    addressLines: ['North of Rio Bravo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower167]': {
    latLong: [27.778128, -99.817353],
    title: 'RVSS [Tower167]',
    addressLines: ['North of Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower168]': {
    latLong: [26.37161, -98.900581],
    title: 'RVSS [Tower168]',
    addressLines: [
      'West of Rio Grande City, TX',
      'Rio Grande City Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower174]': {
    latLong: [32.54370456, -117.0193295],
    title: 'RVSS [Tower174]',
    addressLines: [
      'San Ysidro East, CA',
      'Imperial Beach Station',
      'San Diego Sector',
    ],
  },
  'RVSS [Tower175]': {
    latLong: [32.54325707, -117.022721],
    title: 'RVSS [Tower175]',
    addressLines: [
      'San Ysidro East, CA',
      'Imperial Beach Station',
      'San Diego Sector',
    ],
  },
  'RVSS [Tower176]': {
    latLong: [32.5428547, -117.0270978],
    title: 'RVSS [Tower176]',
    addressLines: [
      'San Ysidro East, CA',
      'Imperial Beach Station',
      'San Diego Sector',
    ],
  },
  'RVSS [Tower178]': {
    latLong: [32.54689247, -116.9799696],
    title: 'RVSS [Tower178]',
    addressLines: ['Borderlands Park, CA', 'San Diego Sector'],
  },
  'RVSS [Tower179]': {
    latLong: [32.5488247, -116.9562754],
    title: 'RVSS [Tower179]',
    addressLines: ['San Diego border fence, CA', 'San Diego Sector'],
  },
  'RVSS [Tower180]': {
    latLong: [32.55008859, -116.9408883],
    title: 'RVSS [Tower180]',
    addressLines: ['Otay Mesa, CA', 'San Diego Sector'],
  },
  'RVSS [Tower181]': {
    latLong: [32.54586438, -116.9921673],
    title: 'RVSS [Tower181]',
    addressLines: ['Otay Mesa, CA', 'San Diego Sector'],
  },
  'RVSS [Tower182]': {
    latLong: [32.54503495, -117.0033441],
    title: 'RVSS [Tower182]',
    addressLines: ['Otay Mesa, CA', 'San Diego Sector'],
  },
  'RVSS [Tower183]': {
    latLong: [32.54499017, -117.0084789],
    title: 'RVSS [Tower183]',
    addressLines: ['Otay Mesa, CA', 'San Diego Sector'],
  },
  'RVSS [Tower184]': {
    latLong: [32.54773858, -116.969912],
    title: 'RVSS [Tower184]',
    addressLines: ['Otay Mesa, CA', 'San Diego Sector'],
  },
  'RVSS [Tower188]': {
    latLong: [31.334166, -109.948725],
    title: 'TCA-NCO-POE NACO [Tower188]',
    addressLines: [
      'Naco, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower189]': {
    latLong: [31.340182, -109.954401],
    title: 'TCA-NCO-WILSON ROAD [Tower189]',
    addressLines: [
      'West Naco, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower190]': {
    latLong: [31.345038, -109.961294],
    title: 'TCA-NCO-NW OF FENCE [Tower190]',
    addressLines: [
      'West Naco, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower191]': {
    latLong: [31.334047, -109.526488],
    title: 'DGL-17-Jefferson [Tower191]',
    addressLines: ['Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower192]': {
    latLong: [31.334176, -109.558965],
    title: 'DGL-POE-15-Dgl [Tower192]',
    addressLines: ['Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower193]': {
    latLong: [31.334186, -109.541211],
    title: 'DGL-16-Dolores [Tower193]',
    addressLines: ['Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower194]': {
    latLong: [31.345074, -109.59062],
    title: 'DGL-24-SlagPit [Tower194]',
    addressLines: ['West Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower196]': {
    latLong: [31.339143, -109.937202],
    title: 'TCA-NCO-ALVAREZ & RR [Tower196]',
    addressLines: [
      'Naco, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower197]': {
    latLong: [31.346996, -110.231528],
    title: 'TCA-NCO-MONTEZUMA RANCH [Tower197]',
    addressLines: [
      'Near Coronado Peak, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower198]': {
    latLong: [31.352811, -109.600927],
    title: 'DGL-19-Vance [Tower198]',
    addressLines: ['West of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower199]': {
    latLong: [32.74998973, -114.8662909],
    title: 'YUM-YUS-C40 [Tower199]',
    addressLines: ['California', 'Blythe Station', 'Yuma Sector'],
  },
  'RVSS [Tower200]': {
    latLong: [32.52144771, -114.8002526],
    title: 'YUM-YUS-C11/12 [Tower200]',
    addressLines: ['South of Gasden, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower201]': {
    latLong: [32.61815, -114.784309],
    title: 'YUM-YUS-C17 [Tower201]',
    addressLines: ['Yuma, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower202]': {
    latLong: [32.66289783, -114.7474486],
    title: 'YUM-YUS-C18/19 [Tower202]',
    addressLines: ['Yuma, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower203]': {
    latLong: [32.687151, -114.734063],
    title: 'YUM-YUS-C21/22 [Tower203]',
    addressLines: ['Yuma, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower204]': {
    latLong: [32.48470493, -114.7802133],
    title: 'YUM-YUS-C06/07 [Tower204]',
    addressLines: ['South of Gasden, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower205]': {
    latLong: [31.34635631, -110.0711846],
    title: 'TCA-NCO-SO MILL [Tower205]',
    addressLines: [
      'West of Naco, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower206]': {
    latLong: [31.35611625, -110.1170828],
    title: 'TCA-NCO-BLM PLATEAU [Tower206]',
    addressLines: [
      'East of San Pedro River, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower207]': {
    latLong: [31.33413112, -109.4670844],
    title: 'RVSS [Tower207]',
    addressLines: ['Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower208]': {
    latLong: [26.1528155, -98.3529541],
    title: 'RVSS [Tower208]',
    addressLines: [
      'Mission, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower210]': {
    latLong: [32.539363, -117.076464],
    title: 'RVSS [Tower210]',
    addressLines: ['Tijuana River Valley, CA', 'San Diego Sector'],
  },
  'RVSS [Tower211]': {
    latLong: [32.54370456, -117.013895],
    title: 'RVSS [Tower211]',
    addressLines: ['Otay Mesa, CA', 'San Diego Sector'],
  },
  'RVSS [Tower212]': {
    latLong: [31.33308, -110.92816],
    title: 'Foxtrot [Tower212]',
    addressLines: ['Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower213]': {
    latLong: [31.334811, -110.933093],
    title: 'Foxtrot [Tower213]',
    addressLines: ['Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower214]': {
    latLong: [26.09868098, -98.27464311],
    title: 'MCS Hidalgo POE [Tower214]',
    addressLines: [
      'Hidalgo, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower215]': {
    latLong: [26.07248358, -98.08753168],
    title: 'WSL Donna Canal [Tower215]',
    addressLines: [
      'West of Donna, TX',
      'Weslaco Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower216]': {
    latLong: [26.0793336, -98.22273239],
    title: "WSL Dyer's Farms [Tower216]",
    addressLines: [
      'South of Hidalgo, TX',
      'Weslaco Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower217]': {
    latLong: [26.08748616, -98.20286927],
    title: 'WSL Pharr POE South [Tower217]',
    addressLines: [
      'East of Hidalgo, TX',
      'Weslaco Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower218]': {
    latLong: [26.05099524, -98.04052697],
    title: 'WSL Retamal South [Tower218]',
    addressLines: ['Donna, TX', 'Weslaco Station', 'Rio Grande Valley Sector'],
  },
  'RVSS [Tower219]': {
    latLong: [26.08219846, -98.15253662],
    title: 'WSL Santa Ana Refuge [Tower219]',
    addressLines: [
      'South of Pharr, TX',
      'Weslaco Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower220]': {
    latLong: [26.08600248, -98.2642481],
    title: 'WSL South Settling Basin [Tower220]',
    addressLines: [
      'Hidalgo, TX',
      'Weslaco Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower221]': {
    latLong: [26.073023, -98.114593],
    title: 'WSL South Tower Rd [Tower221]',
    addressLines: [
      'West of Donna, TX',
      'Weslaco Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower222]': {
    latLong: [26.06376916, -97.95418677],
    title: 'WSL Whiskey Tree [Tower222]',
    addressLines: [
      'Progreso Lakes, TX',
      'Weslaco Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower223]': {
    latLong: [32.49377941, -114.8112426],
    title: 'YUM-YUS-0533 [Tower223]',
    addressLines: ['South of Gasden, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower224]': {
    latLong: [32.71951, -114.717329],
    title: 'YUM-YUS-0535 [Tower224]',
    addressLines: ['West of Yuma, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower225]': {
    latLong: [32.4451, -114.6483],
    title: 'YUM-YUS-0539 [Tower225]',
    addressLines: ['South of Yuma, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower226]': {
    latLong: [32.4194, -114.5623],
    title: 'YUM-YUS-0543 [Tower226]',
    addressLines: ['South of Yuma, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower227]': {
    latLong: [32.718904, -114.726754],
    title: 'YUM-YUS-0571 [Tower227]',
    addressLines: ['Andrade, CA', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower228]': {
    latLong: [32.715303, -114.793673],
    title: 'YUM-YUS-0573 [Tower228]',
    addressLines: ['West of Andrade, CA', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower229]': {
    latLong: [32.4583374365, -114.69166308525],
    title: 'YUM-YUS-0575 [Tower229]',
    addressLines: ['South of Yuma, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower230]': {
    latLong: [32.46863109, -114.7257633],
    title: 'YUM-YUS-0577 [Tower230]',
    addressLines: ['South of Yuma, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower232]': {
    latLong: [31.75237, -106.49245],
    title: 'EPT-EPS-06 Water Treatment Plant2 [Tower232]',
    addressLines: ['El Paso, TX', 'El Paso Station', 'El Paso Sector'],
  },
  'RVSS [Tower241]': {
    latLong: [27.01559768, -97.79381892],
    title: 'Sarita Checkpoint [Tower241]',
    addressLines: [
      'Sarita Checkpoint, TX',
      'Kingsville Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower261]': {
    latLong: [32.4818, -114.7672],
    title: 'YUM-YUS-C05 [Tower261]',
    addressLines: ['South of Gasden, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower262]': {
    latLong: [32.48822, -114.78769],
    title: 'YUM-YUS-C08 [Tower262]',
    addressLines: ['South of Gasden, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower263]': {
    latLong: [32.49335, -114.79662],
    title: 'YUM-YUS-C09/10 [Tower263]',
    addressLines: ['South of Gasden, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower264]': {
    latLong: [32.55452378, -114.7877653],
    title: 'YUM-YUS-C13/14 [Tower264]',
    addressLines: ['Gasden, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower265]': {
    latLong: [32.59603, -114.79309],
    title: 'YUM-YUS-C16 [Tower265]',
    addressLines: ['North of Gasden, AZ', 'Yuma Station', 'Yuma Sector'],
  },
  'RVSS [Tower266]': {
    latLong: [31.76475, -112.30455],
    title: 'Ajo-Papago Farms [Tower266]',
    addressLines: ['South of Tatk Kam Vo, AZ', 'Ajo Station', 'Tucson Sector'],
  },
  'RVSS [Tower267]': {
    latLong: [31.33758, -110.95282],
    title: 'Lima [Tower267]',
    addressLines: ['Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower268]': {
    latLong: [31.33446, -110.95888],
    title: 'Mike [Tower268]',
    addressLines: ['Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower269]': {
    latLong: [31.33816, -110.95331],
    title: 'Lima [Tower269]',
    addressLines: ['Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower270]': {
    latLong: [31.35016, -109.89028],
    title: 'TCA-NCO-BISBEE JUNCTION [Tower270]',
    addressLines: [
      'East of Naco, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower271]': {
    latLong: [31.34443553, -109.9067038],
    title: 'TCA-NCO-BJ & TANK [Tower271]',
    addressLines: [
      'East of Naco, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower272]': {
    latLong: [31.33949, -109.92131],
    title: 'TCA-NCO-SO OF GORE RANCH [Tower272]',
    addressLines: [
      'East of Naco, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower273]': {
    latLong: [31.35833, -110.19104],
    title: 'TCA-NCO-STAR RANCH [Tower273]',
    addressLines: [
      'Miracle Valley, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower274]': {
    latLong: [31.36809, -110.00373],
    title: 'TCA-NCO-STATE WINDMILL [Tower274]',
    addressLines: [
      'West of Naco, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower275]': {
    latLong: [31.3427, -110.02166],
    title: 'TCA-NCO-TANK 5 [Tower275]',
    addressLines: [
      'West of Naco, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower276]': {
    latLong: [31.33491, -109.57399],
    title: 'Dgl-18-RosePlant [Tower276]',
    addressLines: ['West Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower277]': {
    latLong: [31.35038, -109.63228],
    title: 'Dgl-20-KingsStation [Tower277]',
    addressLines: ['West of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower278]': {
    latLong: [31.34192, -109.57903],
    title: 'Dgl-PD-21-Warehouse [Tower278]',
    addressLines: ['West Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower279]': {
    latLong: [31.33429, -109.69652],
    title: 'Dgl-22-Central [Tower279]',
    addressLines: ['West of Douglas, AZ', 'Douglas Station', 'Tucson Sector'],
  },
  'RVSS [Tower280]': {
    latLong: [25.89092699, -97.46897163],
    title: '3E3 and Levee [Tower280]',
    addressLines: [
      'Brownsville, TX',
      'Fort Brown Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower281]': {
    latLong: [25.89272276, -97.46078997],
    title: 'RVSS [Tower281]',
    addressLines: [
      'Brownsville, TX',
      'Fort Brown Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower282]': {
    latLong: [26.058231, -97.792842],
    title: "RGV-HRL-ADAM'S GARDEN [Tower282]",
    addressLines: [
      'East of Bluetown-Iglesia Antigua, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower283]': {
    latLong: [26.062558, -97.843536],
    title: 'RGV-HRL-ANACUA ROAD [Tower283]',
    addressLines: [
      'Santa-Maria, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower284]': {
    latLong: [26.037231, -97.720792],
    title: 'RGV-HRL-CANTU ROAD [Tower284]',
    addressLines: [
      'Los Indios, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower285]': {
    latLong: [26.044347, -97.701572],
    title: 'RGV-HRL-LANDRUMS [Tower285]',
    addressLines: [
      'Carricitos, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower286]': {
    latLong: [26.043267, -97.755483],
    title: 'RGV-HRL-SAN BENITO PUMP [Tower286]',
    addressLines: [
      'Los Indios, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower287]': {
    latLong: [26.067856, -97.815231],
    title: 'RGV-HRL-THREE HOUSE ROAD [Tower287]',
    addressLines: [
      'Bluetown-Iglesia Antigua, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower288]': {
    latLong: [26.01703379, -97.63500833],
    title: 'Monks Bend [Tower288]',
    addressLines: [
      'Encantada-Ranchito, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower289]': {
    latLong: [26.00764275, -97.61741477],
    title: 'IBC Road [Tower289]',
    addressLines: [
      'Encantada-Ranchito, TX',
      'Harlingen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower290]': {
    latLong: [25.935267, -97.543279],
    title: 'Colonia Galaxia [Tower290]',
    addressLines: [
      'East of Los Compadres, TX',
      'Brownsville Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower291]': {
    latLong: [25.91983247, -97.52923841],
    title: 'PUB [Tower291]',
    addressLines: [
      'Northwest of Brownsville, TX',
      'Brownsville Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower292]': {
    latLong: [25.889307, -97.509706],
    title: 'Amigoland [Tower292]',
    addressLines: [
      'Brownsville, TX',
      'Brownsville Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower293]': {
    latLong: [25.89540493, -97.49554702],
    title: 'Customs/Gateway [Tower293]',
    addressLines: [
      'Brownsville, TX',
      'Brownsville Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower294]': {
    latLong: [25.887677, -97.479243],
    title: 'Lincoln Park [Tower294]',
    addressLines: [
      'Brownsville, TX',
      'Brownsville Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower296]': {
    latLong: [28.81976296, -100.5430614],
    title: 'RVSS [Tower296]',
    addressLines: [
      'South of Radar Base, TX',
      'Del Rio Station',
      'Del Rio Sector',
    ],
  },
  'RVSS [Tower297]': {
    latLong: [28.8479168, -100.5717615],
    title: 'RVSS [Tower297]',
    addressLines: [
      'West of Radar Base, TX',
      'Del Rio Station',
      'Del Rio Sector',
    ],
  },
  'RVSS [Tower298]': {
    latLong: [27.75555257, -99.77277589],
    title: 'RVSS [Tower298]',
    addressLines: ['North of Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower300]': {
    latLong: [27.6190161, -99.5764028],
    title: 'RVSS [Tower300]',
    addressLines: ['Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower301]': {
    latLong: [27.6067163, -99.5668052],
    title: 'RVSS [Tower301]',
    addressLines: ['Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower302]': {
    latLong: [27.7516905, -99.7501832],
    title: 'RVSS [Tower302]',
    addressLines: ['North of Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower304]': {
    latLong: [27.6400206, -99.6285611],
    title: 'RVSS [Tower304]',
    addressLines: ['South of Rancho Penitas West, TX', 'Laredo Sector'],
  },
  'RVSS [Tower305]': {
    latLong: [28.7807182, -100.5279747],
    title: 'RVSS [Tower305]',
    addressLines: ['West of Elm Creek, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower306]': {
    latLong: [27.4120553, -99.486464],
    title: 'Hernandez Ranch [Tower306]',
    addressLines: ['Laredo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower307]': {
    latLong: [28.71044659, -100.5072063],
    title: 'RVSS [Tower307]',
    addressLines: ['Eagle Pass, TX', 'Eagle Pass Station', 'Del Rio Sector'],
  },
  'RVSS [Tower308]': {
    latLong: [28.9604448, -100.642186],
    title: 'RVSS [Tower308]',
    addressLines: ['Quemado, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower309]': {
    latLong: [28.9987995, -100.6452011],
    title: 'RVSS [Tower309]',
    addressLines: ['North of Quemado, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower317]': {
    latLong: [28.58405438, -100.3942715],
    title: 'RVSS [Tower317]',
    addressLines: ['South of Rosita, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower318]': {
    latLong: [28.68556401, -100.4990367],
    title: 'RVSS [Tower318]',
    addressLines: ['South of Eagle Pass, TX', 'Del Rio Sector'],
  },
  'RVSS [Tower320]': {
    latLong: [27.173396, -99.424722],
    title: 'RVSS [Tower320]',
    addressLines: ['El Capullo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower321]': {
    latLong: [27.091923, -99.427176],
    title: 'RVSS [Tower321]',
    addressLines: ['El Cuellareno, TX', 'Laredo Sector'],
  },
  'RVSS [Tower324]': {
    latLong: [26.521491, -99.115745],
    title: 'N of Bench Landing, Salineno [Tower324]',
    addressLines: [
      'Salineno, TX',
      'Rio Grande City Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower337]': {
    latLong: [27.64901125, -99.65737697],
    title: 'RVSS [Tower337]',
    addressLines: ['West of Rancho Penitas West, TX', 'Laredo Sector'],
  },
  'RVSS [Tower340]': {
    latLong: [25.88358327, -97.45036292],
    title: 'RVSS [Tower340]',
    addressLines: [
      'Brownsville, TX',
      'Fort Brown Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower345]': {
    latLong: [32.72955609, -115.9824254],
    title: 'RVSS [Tower345]',
    addressLines: ['Arizona', 'El Centro Sector'],
  },
  'RVSS [Tower350]': {
    latLong: [27.210858, -99.430978],
    title: 'RVSS [Tower350]',
    addressLines: ['El Capullo, TX', 'Laredo Sector'],
  },
  'RVSS [Tower353]': {
    latLong: [31.3330892, -110.9416587],
    title: 'Juliet [Tower353]',
    addressLines: ['Nogales, AZ', 'Nogales Station', 'Tucson Sector'],
  },
  'RVSS [Tower355]': {
    latLong: [25.945132, -97.559556],
    title: 'BRP Cindy Stone [Tower355]',
    addressLines: [
      'Los Compadres, TX',
      'Brownsville Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower356]': {
    latLong: [26.13394012, -98.28833057],
    title: 'MCS Floodway [Tower356]',
    addressLines: [
      'Granjeno, TX',
      'McAllen Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower358]': {
    latLong: [31.34033, -110.16005],
    title: 'TCA-NCO-APACHE SKY [Tower358]',
    addressLines: [
      'Miracle Valley, AZ',
      'Brian A. Terry (Naco) Station',
      'Tucson Sector',
    ],
  },
  'RVSS [Tower359]': {
    latLong: [26.414417, -99.034435],
    title: 'NE of Oysterbed, Roma [Tower359]',
    addressLines: [
      'Roma, TX',
      'Rio Grande City Station',
      'Rio Grande Valley Sector',
    ],
  },
  'RVSS [Tower363]': {
    latLong: [32.64675792, -115.8786396],
    title: 'RVSS [Tower363]',
    addressLines: ['Arizona', 'El Centro Sector'],
  },
  'RVSS [Tower364]': {
    latLong: [32.67528, -115.882788],
    title: 'RVSS [Tower364]',
    addressLines: ['Arizona', 'El Centro Sector'],
  },
}
