import * as React from 'react'

export const data = {
  'DEA Africa Region': {
    latLong: [-34.076814486584226, 18.429675267049785],
    entity: 'dea africa region',
    title: 'DEA Africa Region',
    addressLines: ['2 Reddam Ave', 'Westlake 7945', 'Phone: +27 (21) 702-7300'],
  },
  'DEA Andean Region': {
    latLong: [4.63721435229, -74.095327923],
    entity: 'dea andean region',
    title: 'DEA Andean Region',
    addressLines: [
      'Carrera 45 No. 24B-27',
      'Bogotá, D.C. Colombia',
      'Phone: (+57) (601) 275-2000',
    ],
  },
  'DEA Asia Pacific Region': {
    latLong: [13.7361560489, 100.547461218],
    entity: 'dea asia pacific region',
    title: 'DEA Asia Pacific Region',
    addressLines: [
      '120/22 Wireless Road',
      'Bangkok, Thailand 10330',
      'Phone: +66-2-205-4987',
    ],
  },
  'DEA Atlanta Division': {
    latLong: [33.75331707327848, -84.39587683307782],
    entity: 'dea atlanta division',
    title: 'DEA Atlanta Division',
    addressLines: [
      '75 Ted Turner Drive, SW',
      'Room 800',
      'Atlanta, GA 30303-3311',
      'Phone: (404) 893-7000',
    ],
  },
  'DEA Aviation Division': {
    latLong: [32.98451697358258, -97.31339286022323],
    entity: 'dea aviation division',
    title: 'DEA Aviation Division',
    addressLines: [
      'Aviation Operations Center',
      '13901 Aviator Way',
      'Fort Worth, TX 76177',
      'Phone: (817) 890-1000',
    ],
  },
  'DEA Caribbean Division': {
    latLong: [18.41581164645457, -66.10651460492777],
    entity: 'dea caribbean division',
    title: 'DEA Caribbean Division',
    addressLines: [
      'Metro Office Park, Millennium Park Plaza',
      'San Juan, PR',
      'Phone: (571) 362-4700',
    ],
  },
  'DEA Chicago Division': {
    latLong: [41.8784543727116, -87.629889021778],
    entity: 'dea chicago division',
    title: 'DEA Chicago Division',
    addressLines: [
      '230 S. Dearborn Street',
      'Ste. 1200',
      'Chicago, IL 60604-1745',
      'Phone: (312) 353-7875',
    ],
  },
  'DEA Dallas Division': {
    latLong: [32.86044963446932, -96.89483687560369],
    entity: 'dea dallas division',
    title: 'DEA Dallas Division',
    addressLines: [
      '0160 Technology Boulevard, East',
      'Dallas, TX 75220',
      'Phone: (214) 366-6900',
    ],
  },
  'DEA Detroit Division': {
    latLong: [42.33048658831723, -83.05249553954373],
    entity: 'dea detroit division',
    title: 'DEA Detroit Division',
    addressLines: [
      '431 Howard Street',
      'Detroit, MI 48226',
      'Phone: (313) 234-4000',
    ],
  },
  'DEA El Paso Division': {
    latLong: [31.815429539465864, -106.53689967979092],
    entity: 'dea el paso division',
    title: 'DEA El Paso Division',
    addressLines: [
      '660 Mesa Hills Drive',
      'Suite 2000',
      'El Paso, TX 79912',
      'Phone: (915) 832-6000',
    ],
  },
  'DEA Europe Region': {
    latLong: [50.8442449467, 4.36697092778],
    entity: 'dea europe region',
    title: 'DEA Europe Region',
    addressLines: [
      'Zinnerstraat – 13 – Rue Zinner',
      'B-1000 Brussels, Belgium',
    ],
  },
  'DEA Houston Division': {
    latLong: [29.7534704966048, -95.4546925935261],
    entity: 'dea houston division',
    title: 'DEA Houston Division',
    addressLines: [
      '1433 West Loop South',
      'Suite 600',
      'Houston, TX 77027-9506',
      'Phone: (713) 693-3000',
    ],
  },
  'DEA Los Angeles Division': {
    latLong: [34.05286890656478, -118.23903148659457],
    entity: 'dea los angeles division',
    title: 'DEA Los Angeles Division',
    addressLines: [
      '255 East Temple Street',
      '17th Floor',
      'Los Angeles, CA 90012',
      'Phone: (213) 621-6700',
    ],
  },
  'DEA Miami Division': {
    latLong: [26.090967890480567, -80.36576778691756],
    entity: 'dea miami division',
    title: 'DEA Miami Division',
    addressLines: [
      '2100 North Commerce Parkway',
      'Miami, FL 33326',
      'Phone: (571) 362-3364',
    ],
  },
  'DEA Middle East Region': {
    latLong: [39.90933827249, 32.804012263],
    entity: 'dea middle east region',
    title: 'DEA Middle East Region',
    addressLines: [
      '1480 Sokak No. 1',
      'Cukurambar Mahallesi',
      '06530 Çankaya',
      'Ankara - Türkiye',
      'Phone: (90-312) 294-0000',
    ],
  },
  'DEA New England Division': {
    latLong: [42.36131204406728, -71.05938723938363],
    entity: 'dea new england division',
    title: 'DEA New England Division',
    addressLines: [
      '15 New Sudbury St',
      'Room E-400',
      'Boston, MA 02203',
      'Phone: (617) 557-2100',
    ],
  },
  'DEA New Jersey Division': {
    latLong: [40.73598638941622, -74.16713910404987],
    entity: 'dea new jersey division',
    title: 'DEA New Jersey Division',
    addressLines: [
      '80 Mulberry Street',
      '2nd Floor',
      'Newark, NJ 07102-4206',
      'Phone: (571) 776-1263',
    ],
  },
  'DEA New Orleans Division': {
    latLong: [30.01833644413014, -90.15555917572436],
    entity: 'dea new orleans division',
    title: 'DEA New Orleans Division',
    addressLines: [
      '3838 N. Causeway Blvd.',
      'Suite 1800, Three Lakeway Center',
      'New Orleans, LA',
      'Phone: (504) 840-1100',
    ],
  },
  'DEA New York Division': {
    latLong: [40.74411978343475, -74.00776844709246],
    entity: 'dea new york division',
    title: 'DEA New York Division',
    addressLines: [
      '99 10th Avenue',
      'New York, NY 10011',
      'Phone: (212) 337-3900',
    ],
  },
  'DEA Omaha Division': {
    latLong: [41.33882756053598, -96.02612703285267],
    entity: 'dea omaha division',
    title: 'DEA Omaha Division',
    addressLines: [
      '7300 World Communications Drive',
      'Omaha, NE 68122',
      'Phone: (402) 965-3600',
    ],
  },
  'DEA Philadelphia Division': {
    latLong: [39.95269516935665, -75.15083317525632],
    entity: 'dea philadelphia division',
    title: 'DEA Philadelphia Division',
    addressLines: [
      'Wm. J. Green Fed. Bldg.',
      '600 Arch Street, Room 10224',
      'Philadelphia, PA 19106',
      'Phone: (215) 861-3474',
    ],
  },
  'DEA Phoenix Division': {
    latLong: [33.41694145401052, -112.0058212399967],
    entity: 'dea phoenix division',
    title: 'DEA Phoenix Division',
    addressLines: [
      '3439 East University Drive',
      'Phoenix, AZ 85034',
      'Phone: (571) 362-5600',
    ],
  },
  'DEA Rocky Mountain Division': {
    latLong: [39.58679337767175, -104.84667410411168],
    entity: 'dea rocky mountain division',
    title: 'DEA Rocky Mountain Division',
    addressLines: [
      '12154 East Easter Avenue',
      'Centennial',
      'Denver, CO 80112',
      'Phone: (720) 895-4040',
    ],
  },
  'DEA San Diego Division': {
    latLong: [32.82437474020264, -117.12372895781421],
    entity: 'dea san diego division',
    title: 'DEA San Diego Division',
    addressLines: [
      '450 Golden Gate Ave.',
      'P.O. Box 36035',
      'San Francisco, CA 94102',
      'Phone: (415) 436-7900',
    ],
  },
  'DEA Seattle Division': {
    latLong: [47.60230490962616, -122.32742292143969],
    entity: 'dea seattle division',
    title: 'DEA Seattle Division',
    addressLines: [
      '300 5th Avenue',
      'Seattle, WA 98104',
      'Phone: (206) 553-5443',
    ],
  },
  'DEA Southern Cone Region': {
    latLong: [-12.10062193582, -76.968923947],
    entity: 'dea southern cone region',
    title: 'DEA Southern Cone Region',
    addressLines: [
      'Avenida La Encalada cdra. 17 s/n',
      'Surco, Lima 33, Peru',
      'Phone: (51-1) 618-2000',
    ],
  },
  'DEA St. Louis Division': {
    latLong: [38.62601501039302, -90.20578668637091],
    entity: 'dea st. louis division',
    title: 'DEA St. Louis Division',
    addressLines: [
      '317 South 16th Street',
      'St. Louis, MO 63103',
      'Phone: (314) 538-4600',
    ],
  },
  'DEA Washington, DC Division': {
    latLong: [38.90171802693321, -77.02272513972972],
    entity: 'dea washington, dc division',
    title: 'DEA Washington, DC Division',
    addressLines: [
      '800 K Street, N.W',
      'Suite 500',
      'Washington, DC 20001',
      'Phone: (202) 305-8500',
    ],
  },
}
