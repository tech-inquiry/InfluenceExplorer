import * as React from 'react'

export const data = {
  'EPT-STN-01 Monument 3 [Tower022]': {
    latLong: [31.7894, -106.583],
    title: 'EPT-STN-01 Monument 3 [Tower022]',
    addressLines: ['Sunland Park, TX', 'El Paso Sector'],
  },
  'AST [Tower059]': {
    latLong: [32.60424, -116.754769],
    title: 'SDC-BRF-004 Hell’s Canyon [Tower059]',
    addressLines: ['Dulzura, CA', 'Brown Field Station', 'San Diego Sector'],
  },
  'AST [Tower060]': {
    latLong: [32.58383, -116.64016],
    title: 'SDC-CAO-03/SDC-BRF-006 West High Point [Tower060]',
    addressLines: ['Tecate, CA', 'Brown Field Station', 'San Diego Sector'],
  },
  'AST [Tower061]': {
    latLong: [32.598807, -116.816583],
    title: 'SDC-BRF-003 85 Highpoint [Tower061]',
    addressLines: [
      'Otay Mountain Wilderness, CA',
      'Brown Field Station',
      'San Diego Sector',
    ],
  },
  'AST [Tower062]': {
    latLong: [32.5761, -116.76275],
    title: 'SDC-BRF-005 Perch [Tower062]',
    addressLines: [
      'West of Tecate, CA',
      'Brown Field Station',
      'San Diego Sector',
    ],
  },
  'AST [Tower063]': {
    latLong: [32.60058452, -116.4624704],
    title: 'SDC-CAO-02 CLEEF Water Tank [Tower063]',
    addressLines: ['Campo, CA', 'Campo Station', 'San Diego Sector'],
  },
  'AST [Tower064]': {
    latLong: [32.61222, -116.38788],
    title: 'SDC-CAO-02 Insurance Cut [Tower064]',
    addressLines: ['Campo, CA', 'Campo Station', 'San Diego Sector'],
  },
  'AST [Tower065]': {
    latLong: [32.60592, -116.32872],
    title: 'SDC-CAO-04 Browns [Tower065]',
    addressLines: ['Tierra del Sol, CA', 'Campo Station', 'San Diego Sector'],
  },
  'AST [Tower141]': {
    latLong: [31.83457002, -107.9842738],
    title: 'AST [Tower141]',
    addressLines: ['South of Corrizalillo Spring, NM', 'El Paso Sector'],
  },
  'AST [Tower145]': {
    latLong: [27.43839467, -99.49489774],
    title: 'AST [Tower145]',
    addressLines: [
      'Santa Rita, Texas',
      'Laredo South Station',
      'Laredo Sector',
    ],
  },
  'AST [Tower146]': {
    latLong: [27.44323108, -99.49496134],
    title: 'AST [Tower146]',
    addressLines: [
      'Santa Rita, Texas',
      'Laredo South Station',
      'Laredo Sector',
    ],
  },
  'AST [Tower153]': {
    latLong: [27.58769904, -99.53285115],
    title: 'AST [Tower153]',
    addressLines: ['Laredo, Texas', 'Laredo Sector'],
  },
  'AST [Tower159]': {
    latLong: [27.5784016, -99.52713789],
    title: 'AST [Tower159]',
    addressLines: ['Laredo, Texas', 'Laredo Sector'],
  },
  'AST [Tower160]': {
    latLong: [27.59850239, -99.53589311],
    title: 'AST [Tower160]',
    addressLines: ['World Trade Bridge', 'Laredo, Texas', 'Laredo Sector'],
  },
  'AST [Tower164]': {
    latLong: [27.543382, -99.521493],
    title: 'AST [Tower164]',
    addressLines: ['Laredo, Texas', 'Laredo Sector'],
  },
  'AST [Tower165]': {
    latLong: [27.498792, -99.495434],
    title: 'AST [Tower165]',
    addressLines: ['Laredo, Texas', 'Laredo Sector'],
  },
  'AST [Tower169]': {
    latLong: [32.58946085, -116.5014533],
    title: 'AST [Tower169]',
    addressLines: ['Campo, CA', 'Campo Station', 'San Diego Sector'],
  },
  'AST [Tower170]': {
    latLong: [32.58900088, -116.4787371],
    title: 'SDC-CAO-01 Patriots Point [Tower170]',
    addressLines: ['Campo, CA', 'Campo Station', 'San Diego Sector'],
  },
  'AST [Tower171]': {
    latLong: [32.59194663, -116.4485007],
    title: 'AST [Tower171]',
    addressLines: ['Campo, CA', 'Campo Station', 'San Diego Sector'],
  },
  'AST [Tower173]': {
    latLong: [32.53484498, -117.1227076],
    title: 'AST [Tower173]',
    addressLines: [
      'Friendship Park, CA',
      'Imperial Beach Station',
      'San Diego Sector',
    ],
  },
  'AST [Tower177]': {
    latLong: [32.59636541, -116.8240978],
    title: 'AST [Tower177]',
    addressLines: [
      'San Ysidro Mountains, CA',
      'Imperial Beach Station',
      'San Diego Sector',
    ],
  },
  'AST [Tower185]': {
    latLong: [32.5759447, -116.893215],
    title: 'AST [Tower185]',
    addressLines: ['Otay Mesa, CA', 'San Diego Sector'],
  },
  'AST [Tower186]': {
    latLong: [32.5629507, -116.8253666],
    title: 'AST [Tower186]',
    addressLines: ['Otay Mesa, CA', 'San Diego Sector'],
  },
  'AST [Tower209]': {
    latLong: [32.6782021, -115.7433199],
    title: 'AST [Tower209]',
    addressLines: ['East of Calexico', 'El Centro Sector'],
  },
  'AST [Tower231]': {
    latLong: [32.5573732, -116.8660364],
    title: 'AST [Tower231]',
    addressLines: ['Otay Mesa, CA', 'San Diego Sector'],
  },
  'AST [Tower248]': {
    latLong: [32.97876961, -117.2717837],
    title: 'AST [Tower248]',
    addressLines: ['Del Mar, CA', 'San Diego Sector'],
  },
  'AST [Tower247]': {
    latLong: [29.54631124, -104.3131375],
    title: 'AST [Tower247]',
    addressLines: ['Presidio, TX', 'Big Bend Sector'],
  },
  'AST [Tower249]': {
    latLong: [29.55581653, -104.3839503],
    title: 'AST [Tower249]',
    addressLines: ['Presidio, TX', 'Big Bend Sector'],
  },
  'AST [Tower250]': {
    latLong: [29.52127778, -104.2870526],
    title: 'AST [Tower250]',
    addressLines: ['Presidio, TX', 'Big Bend Sector'],
  },
  'AST [Tower254]': {
    latLong: [32.58900803, -116.5405064],
    title: 'AST [Tower254]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower255]': {
    latLong: [32.58319476, -116.5683184],
    title: 'AST [Tower255]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower256]': {
    latLong: [32.62062348, -116.1643297],
    title: 'AST [Tower256]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower257]': {
    latLong: [32.63242686, -116.1391549],
    title: 'AST [Tower257]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower258]': {
    latLong: [32.65532666, -116.8160578],
    title: 'AST [Tower258]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower259]': {
    latLong: [32.59193778, -116.5836086],
    title: 'AST [Tower249]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower260]': {
    latLong: [29.59704093, -104.4236337],
    title: 'AST [Tower260]',
    addressLines: ['Texas', 'Big Bend Sector'],
  },
  'AST [Tower295]': {
    latLong: [29.61729103, -104.4464363],
    title: 'AST [Tower295]',
    addressLines: ['Texas', 'Big Bend Sector'],
  },
  'AST [Tower299]': {
    latLong: [27.7190415, -99.7318405],
    title: 'AST [Tower299]',
    addressLines: ['Texas', 'Laredo Sector'],
  },
  'AST [Tower303]': {
    latLong: [27.7687411, -99.8030606],
    title: 'AST [Tower303]',
    addressLines: ['Texas', 'Laredo Sector'],
  },
  'AST [Tower310]': {
    latLong: [28.820482, -100.524148],
    title: 'AST [Tower310]',
    addressLines: ['Texas', 'Del Rio Sector'],
  },
  'AST [Tower312]': {
    latLong: [27.69869732, -99.71548747],
    title: 'AST [Tower312]',
    addressLines: ['Texas', 'Laredo West Station', 'Laredo Sector'],
  },
  'AST [Tower311]': {
    latLong: [30.63967093, -104.6233205],
    title: 'AST [Tower311]',
    addressLines: ['Texas', 'Big Bend Sector'],
  },
  'AST [Tower313]': {
    latLong: [27.76816496, -99.77745617],
    title: 'AST [Tower313]',
    addressLines: ['Texas', 'Laredo West Station', 'Laredo Sector'],
  },
  'AST [Tower314]': {
    latLong: [27.7464807, -99.7512188],
    title: 'AST [Tower314]',
    addressLines: ['Texas', 'Laredo West Station', 'Laredo Sector'],
  },
  'AST [Tower315]': {
    latLong: [27.78086616, -99.80869537],
    title: 'AST [Tower315]',
    addressLines: ['Texas', 'Laredo West Station', 'Laredo Sector'],
  },
  'AST [Tower316]': {
    latLong: [28.576579, -100.352045],
    title: 'AST [Tower316]',
    addressLines: ['Texas', 'Del Rio Sector'],
  },
  'AST [Tower319]': {
    latLong: [28.544799, -100.326491],
    title: 'AST [Tower319]',
    addressLines: ['Texas', 'Del Rio Sector'],
  },
  'AST [Tower322]': {
    latLong: [27.035797, -99.420102],
    title: 'AST [Tower322]',
    addressLines: ['Texas', 'Laredo Sector'],
  },
  'AST [Tower323]': {
    latLong: [32.61946865, -116.2262831],
    title: 'AST [Tower323]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower325]': {
    latLong: [32.6410104, -116.281439],
    title: 'AST [Tower325]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower326]': {
    latLong: [32.6564404, -116.3046796],
    title: 'AST [Tower326]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower327]': {
    latLong: [27.8050885, -99.8594631],
    title: 'AST [Tower327]',
    addressLines: ['Texas', 'Laredo Sector'],
  },
  'AST [Tower328]': {
    latLong: [27.81668263, -99.86962303],
    title: 'AST [Tower328]',
    addressLines: ['Texas', 'Laredo Sector'],
  },
  'AST [Tower336]': {
    latLong: [27.73775069, -99.78075018],
    title: 'AST [Tower336]',
    addressLines: ['Texas', 'Laredo Sector'],
  },
  'AST [Tower338]': {
    latLong: [32.585426, -116.588679],
    title: 'AST [Tower338]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower339]': {
    latLong: [32.6302453, -116.8494138],
    title: 'AST [Tower339]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower342]': {
    latLong: [32.599569, -116.609662],
    title: 'AST [Tower342]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower343]': {
    latLong: [28.498065, -100.332276],
    title: 'AST [Tower343]',
    addressLines: ['Texas', 'Del Rio Sector'],
  },
  'AST [Tower344]': {
    latLong: [28.436158, -100.334965],
    title: 'AST [Tower344]',
    addressLines: ['Texas', 'Del Rio Sector'],
  },
  'AST [Tower347]': {
    latLong: [27.666728, -99.686104],
    title: 'AST [Tower347]',
    addressLines: ['Texas', 'Laredo Sector'],
  },
  'AST [Tower349]': {
    latLong: [32.5748048, -116.8418574],
    title: 'AST [Tower349]',
    addressLines: ['Otay Mesa, CA', 'San Diego Sector'],
  },
  'AST [Tower351]': {
    latLong: [32.61410324, -116.3561017],
    title: 'AST [Tower351]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower352]': {
    latLong: [32.6098603, -116.4488605],
    title: 'AST [Tower352]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower357]': {
    latLong: [27.284904, -99.488132],
    title: 'AST [Tower357]',
    addressLines: ['Texas', 'Laredo Sector'],
  },
  'AST [Tower361]': {
    latLong: [32.677827, -115.789081],
    title: 'AST [Tower361]',
    addressLines: ['El Centro Sector'],
  },
  'AST [Tower362]': {
    latLong: [32.65829781, -115.8439193],
    title: 'AST [Tower362]',
    addressLines: ['El Centro Sector'],
  },
  'AST [Tower365]': {
    latLong: [32.68527123, -115.9149854],
    title: 'AST [Tower365]',
    addressLines: ['El Centro Sector'],
  },
  'AST [Tower366]': {
    latLong: [32.69990593, -115.9356494],
    title: 'AST [Tower366]',
    addressLines: ['El Centro Sector'],
  },
  'AST [Tower367]': {
    latLong: [32.61178378, -116.2114782],
    title: 'AST [Tower367]',
    addressLines: ['San Diego Sector'],
  },
  'AST [Tower368]': {
    latLong: [32.59199715, -116.5173044],
    title: 'AST [Tower368]',
    addressLines: ['San Diego Sector'],
  },
}
