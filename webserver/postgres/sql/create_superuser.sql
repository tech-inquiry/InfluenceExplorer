CREATE DATABASE contracts_and_lobbying;
GRANT ALL PRIVILEGES ON DATABASE contracts_and_lobbying TO tech_inquiry;
ALTER USER tech_inquiry SUPERUSER;

\c contracts_and_lobbying;
CREATE EXTENSION btree_gin;
