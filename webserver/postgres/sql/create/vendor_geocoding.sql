CREATE TABLE vendor_geocoding(
  id SERIAL PRIMARY KEY,
  vendor VARCHAR(255),
  street_address VARCHAR(255),
  city VARCHAR(127),
  state VARCHAR(63),
  zipcode VARCHAR(63),
  country VARCHAR(127) ,
  phone_number VARCHAR(63),
  fax_number VARCHAR(63),
  congressional_district VARCHAR(63),
  longitude FLOAT,
  latitude FLOAT,
  signed_date TIMESTAMP);

CREATE UNIQUE INDEX vendor_geocoding_vendor_index ON vendor_geocoding (vendor);
