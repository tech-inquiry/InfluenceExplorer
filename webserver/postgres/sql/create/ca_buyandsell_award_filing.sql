CREATE TABLE ca_buyandsell_award_filing(
  id SERIAL,
  title VARCHAR(511),
  reference_number VARCHAR(63),
  solicitation_number VARCHAR(127),
  contract_sequence_number INT,
  contract_number VARCHAR(127),
  publishing_status VARCHAR(63),
  award_date DATE,
  publication_date DATE,
  amendment_date DATE,
  gsin_unspsc TEXT,
  contract_award_procedure VARCHAR(127),
  tendering_procedure VARCHAR(63),
  procurement_entity VARCHAR(255),
  end_user_entity VARCHAR(127),
  customer_info VARCHAR(511),
  description TEXT,
  supplier_info TEXT,
  currency VARCHAR(63),
  contract_value VARCHAR(63),
  CONSTRAINT ca_pspc_filing_unique UNIQUE (
    reference_number, solicitation_number, contract_sequence_number,
    contract_number)
);

CREATE INDEX ca_buyandsell_award_filing_supplier_index ON
  ca_buyandsell_award_filing (LOWER(supplier_info));

CREATE INDEX ca_buyandsell_award_filing_search_index ON
  ca_buyandsell_award_filing USING GIN (
    to_tsvector('english',
      coalesce(lower(title), '') || ' ' ||
      coalesce(lower(reference_number), '') || ' ' ||
      coalesce(lower(solicitation_number), '') || ' ' ||
      coalesce(contract_sequence_number::TEXT, '') || ' ' ||
      coalesce(lower(contract_number), '') || ' ' ||
      coalesce(lower(publishing_status), '') || ' ' ||
      coalesce(lower(gsin_unspsc), '') || ' ' ||
      coalesce(lower(contract_award_procedure), '') || ' ' ||
      coalesce(lower(tendering_procedure), '') || ' ' ||
      coalesce(lower(procurement_entity), '') || ' ' ||
      coalesce(lower(end_user_entity), '') || ' ' ||
      coalesce(lower(customer_info), '') || ' ' ||
      coalesce(lower(description), '') || ' ' ||
      coalesce(lower(supplier_info), '')
    )
  );
