#!/bin/bash
set -e

cd /usr/src/webserver/postgres

# Set up a user.
echo "Creating Postgres user ${INF_EXP_POSTGRES_USER} with pass ${INF_EXP_POSTGRES_PASSWORD}"
psql -U postgres -c \
  "CREATE USER ${INF_EXP_POSTGRES_USER} WITH password '${INF_EXP_POSTGRES_PASSWORD}'"

psql -U postgres -c "CREATE DATABASE ${INF_EXP_POSTGRES_DATABASE};"
psql -U postgres -c \
  "GRANT ALL PRIVILEGES ON DATABASE ${INF_EXP_POSTGRES_DATABASE} TO ${INF_EXP_POSTGRES_USER};"
psql -U postgres -c "ALTER USER ${INF_EXP_POSTGRES_USER} SUPERUSER";
psql -U postgres -c \
  "CREATE TABLE vendor_suggestion (id SERIAL, username VARCHAR(63), date DATE, vendor VARCHAR(255), data jsonb);"

psql -U postgres -c \
  "CREATE TABLE table_updates (id SERIAL, table_name TEXT, last_update DATE, UNIQUE (table_name));"
